﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;

public partial class RetirementReports : System.Web.UI.Page
{
    static String ConnString = "";
    static string StrFormula = "";
    public string str1 = "";
    static string PanNoReq = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LastDay_CurrentMonth(string StrRetirementDate)
    {
        //int SectorID = 1;
        //DataSet ds1 = DBHandler.GetResults("Get_SalPayYear", SalFinYear); --> This coding is current Month display from Get_SalPayYear
        DataTable dt = DBHandler.GetResult("get_LastDate", StrRetirementDate);
        //DataTable dt = ds1.Tables[0]; DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonth = dt;
        List<CenterbySector> listCenter = new List<CenterbySector>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CenterbySector objst = new CenterbySector();
                objst.LastDay_CurrentMonth = Convert.ToString(dtSalMonth.Rows[0]["LastDay_CurrentMonth"]);
                listCenter.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listCenter);

    }

    public class CenterbySector
    {
        public string LastDay_CurrentMonth { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(string StrRetirementDate,string StrMonthYr, string FormName, string ReportName, string ReportType) 
    {
      
        

            String Msg = "";            
        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        string RptFieldParameter1 = StrRetirementDate;
        string RptFieldParameter2 = StrMonthYr;
        int RptFieldParameter3 = UserID;
        DataTable dtcr = DBHandler.GetResult("Load_EmployeeRetirementList", RptFieldParameter1, RptFieldParameter2, RptFieldParameter3);
            StrFormula = "";
        
        

        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }
    }
    
