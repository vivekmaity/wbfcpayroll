﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;

public partial class CenterMaster : System.Web.UI.Page
{
    public static string hdncenId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload()
    {
        DataTable dt = DBHandler.GetResult("show_cdropdown");
        return dt;
    }
   

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void PopulateGrid()
    {
        try
        {

            DataTable dt = DBHandler.GetResult("Get_Center");
            tbl.DataSource = dt;
            tbl.DataBind();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_centerbyID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                hdncenId = dtResult.Rows[0]["CenterId"].ToString();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlsecname")).SelectedValue = dtResult.Rows[0]["SECID"].ToString();

                    
                }



            }
            else if (flag == 2)
            {
                DataTable dt = new DataTable();
                dt = DBHandler.GetResult("DELETE_CenterBYID", ID);
                if (dt.Rows.Count > 0) {
                    string Respo = dt.Rows[0]["Status"].ToString();
                    if (Respo == "1")
                    {
                        dv.ChangeMode(FormViewMode.Insert);
                        PopulateGrid();
                        dv.DataBind();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                    }
                    else if (Respo == "2")
                    {
                        dv.ChangeMode(FormViewMode.Insert);
                        PopulateGrid();
                        dv.DataBind();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Not Deleted.')</script>");
                    }
                    else
                    {
                        dv.ChangeMode(FormViewMode.Insert);
                        PopulateGrid();
                        dv.DataBind();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Not Authorized for Deleted.')</script>");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                TextBox txtcc = (TextBox)dv.FindControl("txtcc");
                TextBox txtcn = (TextBox)dv.FindControl("txtcn");
                
                DropDownList ddlsecname = (DropDownList)dv.FindControl("ddlsecname");

                TextBox txtadrs = (TextBox)dv.FindControl("txtadrs");
                TextBox txtphn = (TextBox)dv.FindControl("txtphn");
                TextBox txtfax = (TextBox)dv.FindControl("txtfax");
                TextBox txthdo = (TextBox)dv.FindControl("txthdo");
                

               

                //TextBox txtDOR = (TextBox)dv.FindControl("txtDOR");
                // DateTime dt1 = DateTime.ParseExact(txtDOR.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                DBHandler.Execute("INSERT_Center",
                    txtcc.Text.Equals("") ? DBNull.Value : (object)(txtcc.Text),
                    txtcn.Text.Equals("") ? DBNull.Value : (object)(txtcn.Text),

                    ddlsecname.SelectedValue.Equals("") ? DBNull.Value : (object)Convert.ToInt32(ddlsecname.SelectedValue),
                    txtadrs.Text.Equals("") ? DBNull.Value : (object)(txtadrs.Text),
                    txtphn.Text.Equals("") ? DBNull.Value : (object)(txtphn.Text),
                    txtfax.Text.Equals("") ? DBNull.Value : (object)(txtfax.Text),
                    txthdo.Text.Equals("") ? DBNull.Value : (object)(txthdo.Text),
                    Session[SiteConstants.SSN_INT_USER_ID]);
                  

                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {

                TextBox txtcc = (TextBox)dv.FindControl("txtcc");
                TextBox txtcn = (TextBox)dv.FindControl("txtcn");

                DropDownList ddlsecname = (DropDownList)dv.FindControl("ddlsecname");

                TextBox txtadrs = (TextBox)dv.FindControl("txtadrs");
                TextBox txtphn = (TextBox)dv.FindControl("txtphn");
                TextBox txtfax = (TextBox)dv.FindControl("txtfax");
                TextBox txthdo = (TextBox)dv.FindControl("txthdo");


                // DateTime dt1 = DateTime.ParseExact(txtDOR.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                string ID = hdncenId;
                    //((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("UPDATE_Center", ID,
                    txtcc.Text.Equals("") ? DBNull.Value : (object)(txtcc.Text),
                    txtcn.Text.Equals("") ? DBNull.Value : (object)(txtcn.Text),

                    ddlsecname.SelectedValue.Equals("") ? DBNull.Value : (object)Convert.ToInt32(ddlsecname.SelectedValue),
                    txtadrs.Text.Equals("") ? DBNull.Value : (object)(txtadrs.Text),
                    txtphn.Text.Equals("") ? DBNull.Value : (object)(txtphn.Text),
                    txtfax.Text.Equals("") ? DBNull.Value : (object)(txtfax.Text),
                    txthdo.Text.Equals("") ? DBNull.Value : (object)(txthdo.Text),
                    Session[SiteConstants.SSN_INT_USER_ID]);

                    cmdSave.Text = "Create";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tbl.PageIndex = e.NewPageIndex;
        PopulateGrid();
    }
}