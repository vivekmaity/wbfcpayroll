﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="PayScheduleHistory.aspx.cs" Inherits="PayScheduleHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        //This is on Page Load for hide and show Gridview Image
        $(function () {

                    var E = "{SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                    var options = {};
                    options.url = "PayScheduleHistory.aspx/GetSalMonth";
                    options.type = "POST";
                    options.data = E;                   
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listSalMonth) {
                        var t = jQuery.parseJSON(listSalMonth.d);
                        //alert(JSON.stringify(t));
                        //var PayMon = t[0]["PayMonths"];
                        //var PayMonID = t[0]["PayMonthsID"];
                        var a = t.length;
                        $("#ddlPaymonthType").empty();
                        //$('#txtPayMonth').val(PayMon);
                        //$('#hdnSalMonthID').val(PayMonID);
                        if (a >= 0) {
                            $("#ddlPaymonthType").append("<option value=''>Select Sal Month</option>")
                            $.each(t, function (key, value) {
                                $("#ddlPaymonthType").append($("<option></option>").val(value.SalMonthID).html(value.SalMonth));
                            });
                        }
                        //if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                        //    document.getElementById("ddlLocation").disabled = false;

                        //}

                    };
                    options.error = function () { alert("Error in retrieving Location!"); };
                    $.ajax(options);
               
            

        });



        //This is for 'Location' Binding on the Base of 'Sector'
        var ParameterTypes = "P";
        var ParameterTypes1 = "D";
        $(document).ready(function () {

        });
        $(document).ready(function () {
            $('#ddlBoth').hide();
            $('#ddlEmpType').hide();
            $('#LempType').hide();

        });

        //This is for RadioButton checked or unchecked
        $(document).ready(function () {
            $('#rdPDF').click(function () {
                if (document.getElementById('rdPDF').checked) {
                    ParameterTypes = "P";
                }
            });

            $('#rdExcel').click(function () {
                if (document.getElementById('rdExcel').checked) {
                    ParameterTypes = "E";
                }
            });
            $('#RadBoth').click(function () {
                if (document.getElementById('RadBoth').checked) {
                    document.getElementById("RadBDeduction").checked = false;
                    document.getElementById("RadBLoan").checked = false;
                    $('#ddlBoth').show();
                    $('#ddlSched').hide();
                    $('#ddlEmpType').show();
                    $('#LempType').show();
                    ParameterTypes1 = "B";
                }
            });
        });
        //This is for 'Location' Binding on the Base of 'Sector'
        $(document).ready(function () {
            $("#ddlSector").change(function () {      
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        var options = {};
                        options.url = "PayScheduleHistory.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;                   
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            //alert(JSON.stringify(t));
                            //var PayMon = t[0]["PayMonths"];
                            //var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                            $("#ddlPaymonthType").empty();
                            //$('#txtPayMonth').val(PayMon);
                            //$('#hdnSalMonthID').val(PayMonID);
                            if (a >= 0) {
                                $("#ddlPaymonthType").append("<option value=''>Select Sal Month</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlPaymonthType").append($("<option></option>").val(value.SalMonthID).html(value.SalMonth));
                                });
                            }
                            //if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                            //    document.getElementById("ddlLocation").disabled = false;

                            //}

                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }

                }
                else {
                    //$('#txtPayMonth').val('');
                    $("#ddlPaymonthType").empty();
                    $("#ddlPaymonthType").append("<option value=''>Select sal month</option>")
                    $("#ddlPaymonthType").val(0);
                }

            });

        });

        $(document).ready(function () {

            $("#ddlSector").change(function () {
                if ($("#ddlSector").val() != '' & $("#ddlLocation").val() != '') {
                    document.getElementById("ddlLocation").disabled = false;

                    var s = $('#ddlSector').val();
                    if (s != 0) {
                        if ($("#ddlSector").val() != "Please select") {
                            var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                            var options = {};
                            options.url = "PaySlipReportHist.aspx/GetLocation";
                            options.type = "POST";
                            options.data = E;
                            options.dataType = "json";
                            options.contentType = "application/json";
                            options.success = function (listLocation) {
                                var t = jQuery.parseJSON(listLocation.d);
                                //alert(JSON.stringify(t));
                                //var PayMon = t[0]["PayMonths"];
                                //var PayMonID = t[0]["PayMonthsID"];
                                var a = t.length;
                                $("#ddlLocation").empty();
                                //$('#txtPayMonth').val(PayMon);
                                //$('#hdnSalMonthID').val(PayMonID);192317
                                if (a >= 0) {
                                    $("#ddlLocation").append("<option value='0'>Select Location</option>")
                                    $.each(t, function (key, value) {
                                        $("#ddlLocation").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                                    });
                                }
                                //if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                                //    document.getElementById("ddlLocation").disabled = false;

                                //}

                            };
                            options.error = function () { alert("Error in retrieving Location!"); };
                            $.ajax(options);
                        }

                    }
                    else {
                        //$('#txtPayMonth').val('');
                        $("#ddlLocation").empty();
                        $("#ddlLocation").append("<option value=''>Select Location</option>")
                        $("#ddlLocation").val(0);
                    }

                }
                //else {
                //    $("#ddlLocation").val(0);
                //    document.getElementById("ddlLocation").disabled = true;

                //}

            });
        });
        $(document).ready(function () {

            $("#ddlReportType").change(function () {
                if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '')
                {
                    document.getElementById("ddlLocation").disabled = false;
                }
                else
                {
                    $("#ddlLocation").val(0);
                    document.getElementById("ddlLocation").disabled = true;
                }
            });
        });

        $(document).ready(function () {
            $("#txtAccFinYear").blur(function () {

                var E = "{SalFinYear: '" + $('#txtAccFinYear').val() + "'}";
                        var options = {};
                        options.url = "PayScheduleHistory.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;                   
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            var a = t.length;
                            $("#ddlPaymonthType").empty();
                            if (a >= 0) {
                                $("#ddlPaymonthType").append("<option value=''>Select Sal Month</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlPaymonthType").append($("<option></option>").val(value.SalMonthID).html(value.SalMonth));
                                });
                            }
                            
                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    
            });
        });
       

        function opentab() {
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            var StrFinYr = '';
            var StrSecID;
            var StrSalMonth;
            var StrSalMonthID;
            var StrCenID;
            var StrSchedule;
            var StrEmpType;
            var SalMonthID;
            //alert();
            //var ReportType = $("#ddlReportType").val();
            var StrReportTyp = '';
            StrReportTyp = $('#ddlReportType').val();
            StrSecID = $("#ddlSector").val();
            StrSalMonthID = $('#ddlPaymonthType').val();
            //StrSalMonth = $('#txtPayMonth').val();
            SalMonthID = $('#ddlPaymonthType').val();
            StrCenID = $('#ddlLocation').val();
            StrSchedule = $('#ddlSched').val();
            StrEmpType = $('#ddlEmpType').val();
            SalFinYear = $("#txtSalFinYear").val();
            FormName = "PayScheduleHistory.aspx";
            ReportName = "Pay_Schedule_History";
            if (StrSchedule == 1) {
                ReportName = "Pay_Schedule_ITAX_History";

            }
             else if  (StrSchedule == 5) {
                ReportName = "Pay_Schedule_PTAX_History";

            }
            else
                {
                ReportName = "Pay_Schedule_Mice_History";
            }
            
            
            
           
            if (ParameterTypes1 == "L") {
                ReportName = "Pay_Schedule_Loan_History";

            }
            if (ParameterTypes1 == "B") {
                StrSchedule = $('#ddlBoth').val();
                ReportName = "Pay_Schedule_History_COOPPFCPF";
                if (StrSchedule == 3) {
                    ReportName = "AdminAllsector_COOP_hist";
                }
                else {
                    ReportName = "AdminAllsector_CPF_all_PF_state_hist";
                }
                //alert(StrSchedule);
            }
            if (ParameterTypes == "P") {

                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";
                if (confirm("Do you want to view the report ?")) {
                    confirm_value.value = "Yes";
            $(".loading-overlay").show();
            var E = '';
            E = "{StrSalMonthID:" + StrSalMonthID + ",StrSchedule:" + StrSchedule + ",StrEmpType:'" + StrEmpType + "',SalFinYear:'" + SalFinYear + "', FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
            //alert(E);
            $.ajax({

                type: "POST",
                url: "PayScheduleHistory.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    $(".loading-overlay").hide();
                    window.open("ReportView1.aspx?");
                }
            });
            
                } else {
                    confirm_value.value = "No";

                }
            }
            else {

                E = "{StrSalMonthID:" + StrSalMonthID + ",StrSchedule:" + StrSchedule + ",SalFinYear:'" + SalFinYear + "',StrEmpType:'" + StrEmpType + "',ReportName:'" + ReportName + "'}";
                //alert(E);
                $.ajax({

                    type: "POST",
                    url: "PayScheduleHistory.aspx/Count_Excel",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        //alert(JSON.stringify(jsmsg))
                        if (jsmsg > 0) {
                            //alert("hi");
                            window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "&StrSalMonthID=" + StrSalMonthID + "&StrSchedule=" + StrSchedule + "&SalFinYear=" + SalFinYear + "&StrEmpType=" + StrEmpType);
                            
                        } else {
                            alert("No Records found");
                        }
                    }
                });
                
               

            }


        }
                
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function deduction() {
            if (document.getElementById('RadBDeduction').checked) {
                document.getElementById("RadBLoan").checked = false;
                document.getElementById("RadBoth").checked = false;
                ParameterTypes1 = "D";
                $('#ddlBoth').hide();
                $('#ddlSched').show();
                $('#ddlEmpType').hide();
                $('#LempType').hide();
                $(document).ready(function () {
                    $.ajax({

                        type: "POST",
                        url: "PayScheduleHistory.aspx/GetDeduction",
                        data: {},

                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlSched").empty();
                            $("#ddlSched").append("<option value=''>Select Schedule Deduction</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlSched").append($("<option></option>").val(value.EDID).html(value.EDName));
                            });
                        },
                        error: function (data) {
                            //alert("error found");
                        }
                    });
                });
            }

            else {
                document.getElementById("RadBLoan").checked = true;
            }
        }
        function loan() {

            if (document.getElementById('RadBLoan').checked) {
                document.getElementById("RadBDeduction").checked = false;
                document.getElementById("RadBoth").checked = false;
                ParameterTypes1 = "L";
                $('#ddlBoth').hide();
                $('#ddlSched').show();
                $('#ddlEmpType').hide();
                $('#LempType').hide();

                $(document).ready(function () {
                    $.ajax({
                        type: "POST",
                        url: "PayScheduleHistory.aspx/GetLoan",
                        data: {},
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlSched").empty();
                            $("#ddlSched").append("<option value=''>Select Schedule Loan</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlSched").append($("<option></option>").val(value.EDID).html(value.EDName));
                            });
                        },
                        error: function (data) {
                        }
                    });
                });

            }


            else {
                document.getElementById("RadBDeduction").checked = true;
            }
        }
</script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>         
							<h2>Pay Schedule History</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  " bgcolor="#669999"  >
                    <tr>
                        <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" bgcolor="White" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" bgcolor="White" >
                                        <asp:DropDownList ID="ddlPaymonthType" Width="200px" Height="28px" runat="server"  CssClass="textbox"  Enabled="true"
                                            DataTextField="SalMonth" DataValueField="SalMonthID" AppendDataBoundItems="true" autocomplete="off">
                                            <asp:ListItem Text="Select Salary Month" Selected="True" Value="0"></asp:ListItem>
                                        </asp:DropDownList>

                                        </td>
                                       
                                         
                                       <asp:HiddenField ID="hdnSalMonthID" runat="server"  />
                                   <%-- </tr>
                                    <tr>--%>
                                                    </td>
                                        <td class="style22">
                                        </td>
                                       <tr>
                                       <td style="padding:5px;" class="style16"><span class="headFont">Schedule Type&nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" class="style20">
                                            <asp:RadioButton ID="RadBDeduction" CssClass="headFont" runat="server" GroupName="rd" 
                                                onchange ='deduction();' Checked ="false" ClientIDMode="Static" AutoPostBack="false"
                                                Text="Deduction"></asp:RadioButton>
                                            <asp:RadioButton ID="RadBLoan" CssClass="headFont" runat="server" AutoPostBack="false" GroupName="rd" 
                                               onchange ='loan();' Checked ="false" ClientIDMode="Static" Text="Loan"></asp:RadioButton>
                                            <asp:RadioButton ID="RadBoth" CssClass="headFont" runat="server" AutoPostBack="false" GroupName="rd" 
                                                Checked ="false" ClientIDMode="Static" Text="Both"></asp:RadioButton>
                                        </td>
                                        <td style="padding:5px;" class="style18" ><span class="headFont">Select Schedule &nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;">:</td>
                                          <td class="style22" >
                                         <asp:DropDownList ID="ddlSched" Width="217px" Height="25px"  runat="server" DataValueField="EDID"
                                                DataTextField="EDName"  Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static" autocomplete="off" >
                                           <asp:ListItem Text="Select Schedule" Selected="True" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlBoth" Width="217px" Height="25px"  runat="server" DataValueField="EDID"
                                                DataTextField="EDName"  Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static" autocomplete="off" >
                                            <asp:ListItem Value="0">Select Report Type</asp:ListItem>
                                            <asp:ListItem Value="1"> PF</asp:ListItem>
                                            <asp:ListItem Value="2"> CPF</asp:ListItem>
                                            <asp:ListItem Value="3"> CO-OP</asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                           <td style="padding:5px;" class="style18" ><span class="headFont"> <asp:Label id="LempType" runat="server" Text="Emplyee Type :" ></asp:Label> &nbsp;&nbsp;</span></td>
                                        
                                          <td class="style22" >
                                              <asp:DropDownList ID="ddlEmpType" Width="217px" Height="25px"  runat="server" DataValueField="EDID"
                                                DataTextField="EDName"  Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static" >
                                            <asp:ListItem Value="N">Select Report Type</asp:ListItem>
                                            <asp:ListItem Value="K"> WBFC</asp:ListItem>
                                            <asp:ListItem Value="S"> State</asp:ListItem>
                                            <asp:ListItem Value="C"> Central</asp:ListItem>
                                            <%--<asp:ListItem Value="A"> ALL</asp:ListItem>--%>
                                            </asp:DropDownList>
                                              </td>
                                           <td style="padding: 5px; vertical-align: top;">
                                            <asp:RadioButton ID="rdPDF" Checked="true" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="PDF" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdExcel" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="Excel" Checked="false" />
                                        </td>
                                           
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" bgcolor="White" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;" /> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                        
                                    </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

