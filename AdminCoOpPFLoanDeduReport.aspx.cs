﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;             
using System.Web.Script.Services;    
using System.Globalization;
using System.Web.Script;              
using System.Web.Script.Serialization;                
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class AdminCoOpPFLoanDeduReport : System.Web.UI.Page
{
    public string str1 = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string GetSalMonth(int SecID, string SalFinYear)
    {
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID, SalFinYear);
        DataTable dt = ds1.Tables[0];
        DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonthID = ds1.Tables[0];

        List<MaxSalMonth> listSalMonth = new List<MaxSalMonth>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MaxSalMonth objst = new MaxSalMonth();

                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                objst.PayMonthsID = Convert.ToInt32(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                listSalMonth.Insert(i, objst);
            }
        }
                 
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class MaxSalMonth
    {
        public string PayMonths { get; set; }
        public int PayMonthsID { get; set; }
    }
                   
    [WebMethod]
    public static void Report_Paravalue(string SalaryFinYear, int SalMonth, string EmpType, string EmpStatus, int DeduEdid, int LoanEdid, int UserId, int ReportID)
    {
        UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (ReportID == 1)
        {
            DataTable dtcr = DBHandler.GetResult("Get_PaySchedule_ALL_COOP", SalaryFinYear, SalMonth, EmpType, EmpStatus, DeduEdid, LoanEdid, UserId);
        }
        else {
            DataTable dtcr = DBHandler.GetResult("Get_CPF_gPF_all", SalaryFinYear, SalMonth, EmpType, EmpStatus, DeduEdid, LoanEdid, UserId);
        }

      }

}