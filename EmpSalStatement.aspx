﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always"  CodeFile="EmpSalStatement.aspx.cs" Inherits="EmpSalStatement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            var SalFinYear = $("#txtSalFinYear").val();
            $("#txtSalFinalYear").val(SalFinYear);
            $("#hdnSalFinYear").val(SalFinYear);
            if (document.getElementById('ChkAllEMP').checked) {
                document.getElementById("ddlEmployee").disabled = true;
                $('#ddlEmployee').val('');
                document.getElementById("ddlCenter").disabled = true;
                $('#ddlCenter').val('');


            }

            else {
                document.getElementById("ddlEmployee").disabled = false;
                document.getElementById("ddlCenter").disabled = false;

            }

        })       

        $(document).ready(function () {

            $("#ddlSector").change(function () {
                if ($("#ddlSector").val() != "Please select") {
                    var E = "{SecID: '" + $('#ddlSector').val() + "'}";
                    var options = {};
                    options.url = "EmpSalStatement.aspx/GetSecByCent";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listSecbycent) {
                        var t = jQuery.parseJSON(listSecbycent.d);
                        var a = t.length;
                        $("#ddlCenter").empty();
                        if (a >= 0) {
                            $("#ddlCenter").append("<option value=''>Select Center</option>")
                            $.each(t, function (key, value) {
                                $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        }
                    };
                    options.error = function () { alert("Error in retrieving Center!"); };
                    $.ajax(options);
                }

            });
        });

        $(document).ready(function () {

            $("#ddlCenter").change(function () {
                if ($("#ddlCenter").val() != "Please select") {
                    var E = "{CenID: '" + $('#ddlCenter').val() + "'}";
                    var options = {};
                    options.url = "EmpSalStatement.aspx/GetEmpnameByCent";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listSecbycent) {
                        var t = jQuery.parseJSON(listSecbycent.d);
                        var a = t.length;
                        $("#ddlEmployee").empty();
                        if (a >= 0) {
                            $("#ddlEmployee").append("<option value=''>Select Employee</option>")
                            $.each(t, function (key, value) {
                                $("#ddlEmployee").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        }
                    };
                    options.error = function () { alert("Error in retrieving Center!"); };
                    $.ajax(options);
                }

            });
        }); 

       

       
        function checkemply() {
            if (document.getElementById('ChkAllEMP').checked) {
                document.getElementById("ddlEmployee").disabled = true;
                $('#ddlEmployee').val('');
                document.getElementById("ddlCenter").disabled = true;
                $('#ddlCenter').val('');


            }

            else {
                document.getElementById("ddlEmployee").disabled = false;
                document.getElementById("ddlCenter").disabled = false;
                
            }
        }

       
       

        function beforeSave() {
            $("#form1").validate();

        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }


        function PrintOpentab() {
            var StrSecID = '';
            var StrCenID = '';
            var StrEmpID = '';
            var ChkAllEmp = '';
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            var StrFinYr = '';
            var StrReportTyp = '';
            StrReportTyp = $('#ddlReportType').val();
            //alert(StrReportTyp);
            if (StrReportTyp == '0') {
                alert("please select Report Type");
                $('#ddlReportType').focus();
                return false;

            }
            if (StrReportTyp == '1') {
                ReportName = "Emp_salary_statement_details";

            }
            if (StrReportTyp == '2') {
                ReportName = "Employee_salary_statement_details";

            }
            FormName = "EmpSalStatement.aspx";
            
            ReportType = "EmpSalStatement";
            //alert($('#ddlSector').val());
            if ($('#ddlSector').val() == '0') {
                alert("Please select Sector");
                $('#ddlSector').focus();
                return false;
            }
            if ($('#txtSalFinalYear').val() == '') {
                alert("Current Salary Financial year can not be null");
                $('#txtSalFinalYear').focus();
                return false;
            }

            if (document.getElementById('ChkAllEMP').checked) {
                ChkAllEmp = "Y";
            }
            else {
                ChkAllEmp = "N";
                //if ($('#ddlCenter').val() == '') {
                //    alert("please select Location");
                //    $('#ddlCenter').focus();
                //    return false;
                //}

                if ($('#ddlEmployee').val() == '') {
                    alert("please select Employee");
                    $('#ddlEmployee').focus();
                    return false;
                }
                if ($('#ddlReportType').val() == '') {
                    alert("please select Report Type");
                    $('#ddlReportType').focus();
                    return false;
                }

            }

             StrSecID = $('#ddlSector').val();
              StrCenID = $('#ddlCenter').val();
              StrEmpID = $('#ddlEmployee').val();
             StrFinYr =$("#txtSalFinalYear").val();

             if (StrCenID == '')
             {
                 StrCenID = 0;
             }

             if (StrEmpID == '') {
                 StrEmpID = 0;
             }
             $(".loading-overlay").show();
            var E = '';
            E = "{StrSecID:" + StrSecID + ",StrCenID:" + StrCenID + ",StrEmpID:" + StrEmpID + ",ChkAllEmp:'" + ChkAllEmp + "',StrFinYr:'" + StrFinYr + "', FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                        
            $.ajax({

                type: "POST",
                url: "EmpSalStatement.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    $(".loading-overlay").hide();
                    window.open("ReportView1.aspx?");
                }
            });
        }


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style type="text/css">
        .style7
        {
            height: 31px;
        }
        .style16
        {
            height: 31px;
            width: 129px;
        }
        .style17
        {
            width: 129px;
        }
        .style18
        {
            width: 97px;
        }
        .style19
        {
            width: 341px;
        }
        .style20
        {
            height: 31px;
            width: 341px;
        }
        .style21
        {
            height: 16px;
            width: 129px;
        }
        .style22
        {
            height: 16px;
        }
        .style23
        {
            height: 16px;
            width: 341px;
        }
        .style24
        {
            width: 97px;
            height: 16px;
        }
        .auto-style1 {
            width: 219px;
        }
        .auto-style2 {
            width: 219px;
            height: 16px;
        }
    </style>
</asp:Content>
          
<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Employee Salary Statement</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;"  ><span class="headFont">Current Salary Financial Year &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left"  >
                                        <asp:TextBox ID="txtSalFinalYear" autocomplete="off" Width="209px" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>
                                            
                                                                                            
                                       <td style="padding:5px;" ><span class="headFont">Location &nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;" class="style22">:</td>
                                        <td class="style22" >
                                        <asp:DropDownList ID="ddlCenter" autocomplete="off" Width="217px" Height="25px" runat="server" DataValueField="CenterID"
                                                DataTextField="CenterName" Enabled="True" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static">
                                                <asp:ListItem Text="Select Center" Selected="True" Value="0"></asp:ListItem>
                                            </asp:DropDownList>

                                            <%--<asp:CheckBox ID="ChkCenterAll" Checked ="true" bgcolor="#CCFF66" AutoPostBack="false" onchange ='checkcenter();'
                                                CssClass="headFont"  ClientIDMode="Static" runat="server"  Text="All"></asp:CheckBox>--%>
                                            <%--<asp:CheckBox ID="ChkCenterWise" Checked="true" CssClass="headFont" ClientIDMode="Static" runat="server" Text="Group Center"></asp:CheckBox>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                        <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span></td>
                                       
                                       <td style="padding:5px;" >:</td>
                                        
                                        
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployee" autocomplete="off" Width="218px" runat="server" Height="25px" CssClass="textbox"
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static">
                                                <asp:ListItem Text="(Select Employee)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:CheckBox ID="ChkAllEMP" Checked ="true" AutoPostBack="false" onchange ='checkemply();'
                                                CssClass="headFont"  ClientIDMode="Static" runat="server"  Text="All EMPLOYEE"></asp:CheckBox>
                                        </td>

                                        <td style="padding:5px;" class="style3" bgcolor="White"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;" bgcolor="White">:</td>
                                        
                                            <td class="style22" >
                                        <asp:DropDownList ID="ddlReportType" autocomplete="off" Width="217px" Height="25px" CssClass="textbox"  Enabled="True" runat="server">
                                            <asp:ListItem Value="0">(Select Report Type)</asp:ListItem>
                                            
                                            <asp:ListItem Value="1">Salary Statement Summary</asp:ListItem>
                                            <%--<asp:ListItem Value="2"> salrary statement  details</asp:ListItem>--%>
                                           <%-- <asp:ListItem Value="1">Pay Slip</asp:ListItem>
                                            <asp:ListItem Value="2">Pay Summary</asp:ListItem>
                                            <asp:ListItem Value="3">Pay Summary Location Wise</asp:ListItem>--%>
                                             <%--<asp:ListItem Value="4">Pay Register</asp:ListItem>--%>
                                        </asp:DropDownList>   
                                        </td>

                                    <%--<td style="padding:5px;" class="style21"><span class="headFont">Sector&nbsp;&nbsp;</span> </td>
                                    <td style="padding:5px;" class="style22">:</td>
                                    <td style="padding:5px;" align="left" class="style23" >
                                    <asp:DropDownList ID="ddlSector1" Width="200px" Height="25px" runat="server" DataValueField="SectorID"
                                                DataTextField="SectorName" Enabled="false" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static">
                                                <asp:ListItem Text="Select Sector" Selected="True" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:CheckBox ID="ChkSectorAll" Checked ="true" AutoPostBack="false" onchange ='checksector();'
                                                CssClass="headFont"  ClientIDMode="Static" runat="server"  Text="All"></asp:CheckBox>
                                                                                            
                                            <asp:CheckBox ID="ChkSectorWise" CssClass="headFont" Checked="true" runat="server" Text="Group Sector"></asp:CheckBox>
                                        </td>--%>
                                        
                                        </td>
                                        
                                    </tr>
                                    <%--</tr>--%>

                                    <%--<td style="padding:5px;" class="style21"><span class="headFont">Employee Type&nbsp;&nbsp;</span> </td>
                                    <td style="padding:5px;" class="style22">:</td>
                                    <td style="padding:5px;" align="left" class="style23" >
                                        <asp:DropDownList ID="ddlEmpType" Width="200px" Height="25px" CssClass="textbox" Enabled="false" runat="server">
                                            <asp:ListItem Value="0">Select Employee Type</asp:ListItem>
                                            <asp:ListItem Value="1">CMDA</asp:ListItem>
                                            <asp:ListItem Value="2">State</asp:ListItem>
                                            <asp:ListItem Value="3">Central</asp:ListItem>
                                        </asp:DropDownList>

                                            <asp:CheckBox ID="ChkEmpTypeAll" Checked ="true" CssClass="headFont" 
                                                runat="server" AutoPostBack ="false" onchange ='checkemptype();' Text="All"></asp:CheckBox>
                                        </td>--%>
                                        <%--<td style="padding:5px;" class="style24"><span class="headFont">Status &nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;" class="style22">:</td>
                                        <td class="style22" >
                                        <asp:DropDownList ID="ddlStatus" Width="217px" Height="25px" CssClass="textbox" Enabled="false" runat="server">
                                            <asp:ListItem Value="0">Select Status</asp:ListItem>
                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                            <asp:ListItem Value="2">Suspended</asp:ListItem>
                                            <asp:ListItem Value="3">Re-Employed</asp:ListItem>
                                        </asp:DropDownList>

                                            <asp:CheckBox ID="ChkStatusAll" Checked ="true" CssClass="headFont" 
                                                runat="server" AutoPostBack="false" Text="All" 
                                                onchange ='checkstatus();'></asp:CheckBox>

                                        </td>--%>
                                       
                                        <%--<td class="style22">
                                        <asp:HiddenField ID="hdnSalFinYear" runat="server"  />
                                                 
                                        </td>
                                        <td class="style22">
                                        </td>
                                       <tr>
                                       <td style="padding:5px;" class="style16"><span class="headFont">Schedule Type&nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" class="style20">
                                            <asp:RadioButton ID="RadBDeduction" CssClass="headFont" runat="server" 
                                                onchange ='deduction();' Checked ="false" ClientIDMode="Static" AutoPostBack="false"
                                                Text="Deduction"></asp:RadioButton>
                                            <asp:RadioButton ID="RadBLoan" CssClass="headFont" runat="server" AutoPostBack="false" 
                                               onchange ='loan();' Checked ="false" ClientIDMode="Static" Text="Loan"></asp:RadioButton>
                                        </td>
                                        <td style="padding:5px;" class="style18" ><span class="headFont">Select Schedule &nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;">:</td>
                                          <td class="style22" >
                                         <asp:DropDownList ID="ddlSched" Width="217px" Height="25px"  runat="server" DataValueField="EDID"
                                                DataTextField="EDName"  Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static" >
                                           <asp:ListItem Text="Select Schedule" Selected="True" Value=""></asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                       <td class="style7">
                                        <asp:HiddenField ID="HiddenField2" runat="server"  />
                                        </td>
                                       <td class="style7">
                                        
                                                 
                                        </td>
                                       
                                    </tr> --%>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    
                                    <%--<td style="padding:5px;" class="style1"><span class="headFont">* indicates Mandatory Field &nbsp;&nbsp;</span> </td>
                                    <td style="padding:5px;">&nbsp;</td>--%>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:450px;">
                                        <asp:Button ID="cmdShow" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="PrintOpentab(); return false;" 
                                            BorderColor="#669999"/> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Reset"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdReset_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                   <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                        </div>    
                                   
                                    </td>
                                   
                                </tr>
                                
                                </table>
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

