﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class VendorMaster : System.Web.UI.Page
{
    public static string hdnvendorId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                
                PopulateGrid();
            }
                 }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DuplicatePannumber_Checkings(string panno)
    {    
        string htmlContent = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Get_VendorALLBYCompanyPan", panno);
            if (dt.Rows.Count > 0)
            {
                 htmlContent = "Available".ToJSON();                
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;    
    }
    
    protected DataTable drpload(string str)
    {
        try
        {
            if (str == "Bank")
            {
                DataTable dt = DBHandler.GetResult("Load_Bank");
                return dt;
            }
            else
            {
                return null;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlBank = (DropDownList)sender;
            LoadBranch(ddlBank.SelectedValue);//PopulateGridOnBankSelection();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlBranch = (DropDownList)sender;
            LoadIFSCMICR(ddlBranch.SelectedValue);//PopulateGridOnBankSelection();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    private void LoadIFSCMICR(string BranchID)
    {
        try
        {
            TextBox txtIFSC = (TextBox)dv.FindControl("txtIFSC");
            TextBox txtMICR = (TextBox)dv.FindControl("txtMICR");
            TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");

             txtBankCode.Enabled = true;

             DataTable dt = DBHandler.GetResult("Get_BankandBranch", BranchID);
                 txtMICR.Text = dt.Rows[0]["MICR"].ToString();
                 txtIFSC.Text = dt.Rows[0]["IFSC"].ToString();
             
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }



    private void LoadBranch(string BankID)
    {
        try
        {
            DropDownList ddlBranch = (DropDownList)dv.FindControl("ddlBranch");
            //TextBox txtIFSC = (TextBox)dv.FindControl("txtIFSC");
            //TextBox txtMICR = (TextBox)dv.FindControl("txtMICR");
            //TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");

            //txtBankCode.Enabled = true;
            
         //   string finID = BankID;
            DataTable dt = DBHandler.GetResult("Get_BranchbyBankID", BankID.Equals("") ? DBNull.Value : (object)BankID);
            //txtMICR.Text = dt.Rows[0]["MICR"].ToString();
            //txtIFSC.Text = dt.Rows[0]["IFSC"].ToString();
            if (dt.Rows.Count > 0)
            {
                
                ddlBranch.Items.Clear();
                ddlBranch.DataSource = dt;
                ddlBranch.DataTextField = "Branch";
                ddlBranch.DataValueField = "BranchID";
                ddlBranch.Items.Add(new ListItem("(Select Branch)", ""));
                ddlBranch.DataBind();

                
            }
            else
            {
                ddlBranch.Items.Clear();
                ddlBranch.Items.Add(new ListItem("(Select Branch)", ""));
                ddlBranch.DataBind();
            }
            
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    //[WebMethod]
    //public static string BindBranch(int BankID)
    //{
    //    DataSet ds = DBHandler.GetResults("Get_BranchbyBankID", BankID);
    //    DataTable dt = ds.Tables[0];

    //    List<BranchbyBank> listbranch = new List<BranchbyBank>();

    //    if (dt.Rows.Count > 0)
    //    {
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            BranchbyBank objst = new BranchbyBank();

    //            objst.BranchID = Convert.ToInt32(dt.Rows[i]["BranchID"]);
    //            objst.Branch = Convert.ToString(dt.Rows[i]["Branch"]);

    //            listbranch.Insert(i, objst);
    //        }
    //    }


    //    JavaScriptSerializer jscript = new JavaScriptSerializer();
    //    return jscript.Serialize(listbranch);

    //}
    //public class BranchbyBank
    //{
    //    public int BranchID { get; set; }
    //    public string Branch { get; set; }
    //}

   



    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {

            string htmlContent = "";

            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                

                TextBox txtVendorName = (TextBox)dv.FindControl("txtVendorName");
                TextBox txtAddress = (TextBox)dv.FindControl("txtAddress");
                TextBox txtCompanyName = (TextBox)dv.FindControl("txtCompanyName");
                TextBox txtPanNo = (TextBox)dv.FindControl("txtPanNo");
                TextBox txtEmail = (TextBox)dv.FindControl("txtEmail");
                TextBox txtPhone = (TextBox)dv.FindControl("txtPhone");
                DropDownList ddlBranch = (DropDownList)dv.FindControl("ddlBranch");
                TextBox txtBranchID = (TextBox)dv.FindControl("txtBranchID");
                TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                DropDownList ddlActiveFlag = (DropDownList)dv.FindControl("ddlActiveFlag");


         
                

               // string id = txtBranchID.Text;
                DataTable dt=DBHandler.GetResult("INSERT_VendorMast", txtVendorName.Text, txtAddress.Text, txtCompanyName.Text,
                    txtPanNo.Text, txtEmail.Text, txtPhone.Text, ddlBranch.SelectedValue.Equals("") ? DBNull.Value : (object)ddlBranch.SelectedValue,
                    ddlActiveFlag.SelectedValue.Equals("") ? DBNull.Value : (object)ddlActiveFlag.SelectedValue,
                    txtBankCode.Text,
                    HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]
                    );


                if (dt.Rows.Count > 0)
                {
                    string Val = dt.Rows[0]["Value"].ToString();
                    if (Val == "D")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This PanNo Already Exist.Kindly Enter Proper PanNo')</script>");
                        txtPanNo.Focus();
                        txtPanNo.Text = "";
                        return;
                    }
                }
                 
                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Vendor Saved Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {
                //DropDownList ddlBank = (DropDownList)dv.FindControl("ddlBank");
                //TextBox txtBranch = (TextBox)dv.FindControl("txtBranch");
                //TextBox txtIFSC = (TextBox)dv.FindControl("txtIFSC");
                //TextBox txtMICR = (TextBox)dv.FindControl("txtMICR");

                TextBox txtVendorName = (TextBox)dv.FindControl("txtVendorName");
                TextBox txtAddress = (TextBox)dv.FindControl("txtAddress");
                TextBox txtCompanyName = (TextBox)dv.FindControl("txtCompanyName");
                TextBox txtPanNo = (TextBox)dv.FindControl("txtPanNo");
                TextBox txtEmail = (TextBox)dv.FindControl("txtEmail");
                TextBox txtPhone = (TextBox)dv.FindControl("txtPhone");
                DropDownList ddlBranch = (DropDownList)dv.FindControl("ddlBranch");
                TextBox txtBranchID = (TextBox)dv.FindControl("txtBranchID");
                TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                DropDownList ddlActiveFlag = (DropDownList)dv.FindControl("ddlActiveFlag");


                int ID = Convert.ToInt32(hdnvendorId);

                DataTable dt = DBHandler.GetResult("Update_VendorMast",ID,
                    txtVendorName.Text, txtAddress.Text, txtCompanyName.Text,
                    txtPanNo.Text, txtEmail.Text, txtPhone.Text, ddlBranch.SelectedValue.Equals("") ? DBNull.Value : (object)ddlBranch.SelectedValue,
                    ddlActiveFlag.SelectedValue.Equals("") ? DBNull.Value : (object)ddlActiveFlag.SelectedValue,
                    txtBankCode.Text,
                    HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    //ID, ddlBank.SelectedValue, txtBranch.Text,
                   // txtIFSC.Text, txtMICR.Text, Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Vendor Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");

        }
        
    }
    

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dtstr = DBHandler.GetResult("Get_Vendor");
            tbl.DataSource = dtstr;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //[WebMethod]
    //public static string GetIFSCandMICR(int BranchID)
    //{
    //    DataSet ds = DBHandler.GetResults("Get_IFSCandMICR", BranchID);
    //    DataTable dt = ds.Tables[0];

    //    List<IfscMicr> listIfscMicr = new List<IfscMicr>();

    //    if (dt.Rows.Count > 0)
    //    {
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            IfscMicr objst = new IfscMicr();

    //            objst.IFSC = Convert.ToString(dt.Rows[i]["IFSC"]);
    //            objst.MICR = Convert.ToString(dt.Rows[i]["MICR"]);

    //            listIfscMicr.Insert(i, objst);
    //        }
    //    }


    //    JavaScriptSerializer jscript = new JavaScriptSerializer();
    //    return jscript.Serialize(listIfscMicr);

    //}
    //public class IfscMicr
    //{
    //    public string IFSC { get; set; }
    //    public string MICR { get; set; }
    //}


    //private void PopulateGridOnBankSelection()
    //{
    //    try
    //    {
    //        DropDownList ddlbank = (DropDownList)dv.FindControl("ddlBank");
    //        string BankID = ddlbank.SelectedValue.ToString();
    //        string BankName = ddlbank.SelectedItem.Text;
    //        DataSet ds = DBHandler.GetResults("Get_BranchbyBankID", BankID);
    //        DataTable dt = ds.Tables[0];
    //        if (dt.Rows.Count > 0)
    //        {
    //            dt.Columns.Add("BankName");
    //            for (int i = 0; i < dt.Rows.Count; i++)
    //            {
    //                dt.Rows[i]["BankName"] = BankName;
    //            }
    //            tbl.DataSource = dt;
    //            tbl.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    //protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    //tbl.PageIndex = e.NewPageIndex;
    //    PopulateGridOnBankSelection();
    //    if (!IsPostBack)
    //    {
    //        // PopulateGrid();
    //    }
    //}
    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
                
            {
                
                dv.ChangeMode(FormViewMode.Edit);
                hdnvendorId = e.CommandArgument.ToString();
                
         
                  UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                txtBankCode.Enabled = true;
                cmdSave.CommandName = "Edit";
               
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
                
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_VendorByID", ID);
                dv.DataSource = dtResult;               
                dv.DataBind();
                

                string BranchID = dtResult.Rows[0]["BranchID"].ToString();
                hdnvendorId = dtResult.Rows[0]["VendorID"].ToString();
                OnSearchBindBankandBranch(BranchID);

                {
                    ((DropDownList)dv.FindControl("ddlActiveFlag")).SelectedValue = dtResult.Rows[0]["ActiveFlag"].ToString();
                }

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_BankByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Vendor Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void OnSearchBindBankandBranch(string BranchID)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dtBank = DBHandler.GetResult("Load_Bank");
                if (dtBank.Rows.Count > 0)
                {
                    DropDownList ddlBank = (DropDownList)dv.FindControl("ddlBank");
                    ddlBank.DataSource = dtBank;
                    ddlBank.DataTextField = "BankName";
                    ddlBank.DataValueField = "BankID";
                    ddlBank.DataBind();
                }

                DataTable dt = DBHandler.GetResult("Get_BankandBranch", BranchID);
                if (dt.Rows.Count > 0)
                {
                    ((DropDownList)dv.FindControl("ddlBank")).SelectedValue = dt.Rows[0]["BankID"].ToString();

                    DataTable dtBranch = DBHandler.GetResult("Get_BranchbyBankID", dt.Rows[0]["BankID"].ToString());
                    if (dtBranch.Rows.Count > 0)
                    {
                        DropDownList ddlBranch = (DropDownList)dv.FindControl("ddlBranch");
                        ddlBranch.DataSource = dtBranch;
                        ddlBranch.DataTextField = "Branch";
                        ddlBranch.DataValueField = "BranchID";
                        ddlBranch.DataBind();
                    }
                    ((DropDownList)dv.FindControl("ddlBranch")).SelectedValue = BranchID;

                    TextBox txtIFSC = (TextBox)dv.FindControl("txtIFSC");
                    TextBox txtMICR = (TextBox)dv.FindControl("txtMICR");
                    txtIFSC.Text = dt.Rows[0]["IFSC"].ToString();
                    txtMICR.Text = dt.Rows[0]["MICR"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void txtVendorName_TextChanged(object sender, EventArgs e)
    {
        TextBox txt = (TextBox)dv.FindControl("txtVendorName");
        string value = txt.Text;
        DataTable dtVendor = DBHandler.GetResult("Vendor_textBOx_autoComplete", value);
        if (dtVendor.Rows.Count > 0)
        {
            tbl.DataSource = dtVendor;
            tbl.DataBind();
        }
    }

}






