﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="HistoryGeneration.aspx.cs" Inherits="HistoryGeneration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <script>
        $(document).ready(function () {
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        var options = {};
                        options.url = "HistoryGeneration.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            //alert(JSON.stringify(t));
                            //var PayMon = t[0]["PayMonths"];
                            //var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                            $("#ddlPaymonthType").empty();
                            //$('#txtPayMonth').val(PayMon);
                            //$('#hdnSalMonthID').val(PayMonID);
                            if (a >= 0) {
                                $("#ddlPaymonthType").append("<option value=''>Select Salary Month</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlPaymonthType").append($("<option></option>").val(value.SalMonthID).html(value.SalMonth));
                                });
                            }
                            //if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                            //    document.getElementById("ddlLocation").disabled = false;

                            //}

                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }

                }
                else {
                    //$('#txtPayMonth').val('');
                    $("#ddlPaymonthType").empty();
                    $("#ddlPaymonthType").append("<option value=''>Select Salary Month</option>")
                    $("#ddlPaymonthType").val(0);
                }

            });

        });

        function opentab() {
            var SectorID;
            var SalMonth;
            var CurrMonth;
            SectorID = $("#ddlSector").val();
            SalMonth = $('#ddlPaymonthType').val();  //$('#hdnSalMonthID').val();
            CurrMonth = $('#ddlPaymonthType').find('option:selected').text(); //$('#txtPayMonth').val();

            if (SalMonth == "") {
                alert("Please Select Sector");
                $('#ddlSector').focus();
                return false;

            }

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";

            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Process History For The Month " + CurrMonth + " ?")) {
                confirm_value.value = "Yes";
                $(".loading-overlay").show();
                var E = "{SectorID:" + SectorID + ",SalMonth:" + SalMonth + "}";
                $.ajax({
                    type: "POST",
                    url: "HistoryGeneration.aspx/Report_Paravalue",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                        $(".loading-overlay").hide();
                        var jsmsg = JSON.parse(msg.d);
                        alert(jsmsg);

                    }

                });
            } else {
                confirm_value.value = "No";

            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>History Generation</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" bgcolor="White" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" bgcolor="White" >
                                        <asp:DropDownList ID="ddlPaymonthType" Width="200px" Height="28px" runat="server"  CssClass="textbox"  Enabled="true"
                                            DataTextField="SalMonth" DataValueField="SalMonthID" AppendDataBoundItems="true" autocomplete="off">
                                            <asp:ListItem Text="Select Salary Month" Selected="True" Value="0"></asp:ListItem>
                                        </asp:DropDownList>

                                        </td>
                                    </tr>
                                </table>

                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="History Generate"   
                                        Width="120px" CssClass="Btnclassname" OnClientClick="opentab(); return false;"/> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname"  OnClientClick='javascript: return unvalidate();'/>
                                         <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">History Generate...</div>
                                        </div>
                                        </div>
                                    </div>       
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                                     
                     
                  
                   
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

