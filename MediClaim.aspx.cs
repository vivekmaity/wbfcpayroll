﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MediClaim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GetEmployeeNameAutoComplete(string EmpName)
    {
        try
        {
            List<string> result = new List<string>();
            DataTable dt = DBHandler.GetResult("GetEmployeeNameAutoComplete", EmpName);
            foreach (DataRow dtRow in dt.Rows)
            {
                result.Add(string.Format("{0}|{1}", dtRow["EmpName"].ToString().Trim(), dtRow["EmpId"].ToString().Trim()));
            }
            return result.ToArray();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveMediclaimDetails(MediCliamDetails param)
    {
        object ObjData=new object();
        try
        {
            string ConString = ""; 
            ConString = DataAccess.DBHandler.GetConnectionString();
            SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            db.Open();
            transaction = db.BeginTransaction();
            SqlDataReader dr;
            try {
                try {
                    var dateTime = DateTime.ParseExact(param.MediClaimStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var outputDate = dateTime.ToString("yyyy-MM-dd");

                    int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    cmd = new SqlCommand("Exec Save_MediclaimDetails '"+param.LoanId+"','"+
                        param.EmployeeId + "','" + param.MediclaimNo + "','" + outputDate + "','" + param.MediClaimAmount + "','" + param.InstallMentNO + "','" +
                        param.SlabInsatallNoIst+"','"+param.SlabInsatallAmountIst+"','"+param.SlabInsatallNoScnd+"','"+param.SlabInsatallAmountScnd+"','"+
                        param.AccFinYear + "','" + UserId + "'", db, transaction);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                         ObjData = new {
                            Message=dr["Message"].ToString(),
                            MessageCode = Convert.ToInt32(dr["MessageCode"].ToString()),
                            Status="Success"
                        };
                    }
                    dr.Close();
                    transaction.Commit();
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    ObjData = new
                    {
                        Message = sqlError.Message,
                        MessageCode = "",
                        Status = "Fail"
                    };
                }
                db.Close();
            }
            catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return ObjData.ToJSON();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetdataForUpdate(int EmployeeId, string FinYear)
    {
        object dataObj = new object();
        try
        {
            List<string> result = new List<string>();
            DataTable dt = DBHandler.GetResult("Get_MediclaimDetails", EmployeeId, FinYear);
            if (dt.Rows.Count > 0)
            {
                dataObj = new
                {
                    LoanID = dt.Rows[0]["LoanID"],
                    EmployeeID = dt.Rows[0]["EmployeeID"],
                    EmpName = dt.Rows[0]["EmpName"],
                    LoanAmount = dt.Rows[0]["LoanAmount"],
                    LoanDeducStartDate = dt.Rows[0]["LoanDeducStartDate"],
                    TotLoanInstall = dt.Rows[0]["TotLoanInstall"],
                    CurLoanInstall = dt.Rows[0]["CurLoanInstall"],
                    LoanAmountPaid = dt.Rows[0]["LoanAmountPaid"],
                    MediclaimNo = dt.Rows[0]["MediclaimNo"],
                    FinYear = dt.Rows[0]["FinYear"],
                    inst_no1 = dt.Rows[0]["inst_no1"],
                    inst_amt1 = dt.Rows[0]["inst_amt1"],
                    inst_no2 = dt.Rows[0]["inst_no2"],
                    inst_amt2 = dt.Rows[0]["inst_amt2"]
                };
            }
            else {
                dataObj = null;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dataObj.ToJSON();
    }
    
}
public class MediCliamDetails
{
    public int? LoanId { get; set; }
    public int EmployeeId { get; set; }
    public string MediclaimNo { get; set; }
    public string MediClaimStartDate { get; set; }
    public decimal MediClaimAmount { get; set; }
    public int InstallMentNO { get; set; }
    public int SlabInsatallNoIst { get; set; }
    public decimal SlabInsatallAmountIst { get; set; }
    public int SlabInsatallNoScnd { get; set; }
    public int SlabInsatallAmountScnd { get; set; }
    public string AccFinYear { get; set; }
}