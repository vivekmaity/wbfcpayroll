﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="VendorBillMaster.aspx.cs" Inherits="VendorBillMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">

    <link href="css/Gridstyle.css" rel="stylesheet" />
    <link href="css/pensionstyle.css" rel="stylesheet" />
    <script src="js/VendorBill.js"></script>
    

    <style type="text/css">  
        .ui-menu .ui-menu-item a {
            color: #3366cc;
            /*background-color:#ADD8E6;*/
        }     
         .er1
          {
            color:red;
            }
         #autoContainerVendorNameSearch {
            display: block;
            position: relative;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Vendor Bill Master</h2>
                    </section>

                    <asp:HiddenField ID="hdnVendorID" runat="server" Value="" ClientIDMode="Static" />


                    <div class="content-overlay" id='overlay' style="z-index: 49;">
                        <div class="wrapper-outer">
                            <div class="wrapper-inner">
                                <div class="loadContent">
                                    <div>
                                        <div style="float: right; width: 49%;" id="Close" class="close-content" >
                                            <a href="javascript:void(0);" id="A1" style="color: red;">
                                                <img src="images/cross.png" alt="Close" title="Close"  style="Height: 18px; Width: 20px;" /></a>
                                                <%--<asp:Button ID="cmdClose" runat="server" Text="close"  Width="100px"
                                                                            CssClass="Btnclassname" />--%>
                                                
                                        </div>

                                    </div>
                                    <br />
                                    <div id="Div1" runat="server">
                                        <table width="100%" style="border: solid 2px lightblue; height: auto;">
                                            <tr>
                                                <td style="padding: 3px;">
                                                    <table align="center" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div id="tabs-1" style="float: left; width: 100%; height: auto;">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td colspan="4" style="padding: 3.5px; background: #deedf7;" class="style1">
                                                                                <span class="headFont" style="font-weight: bold;"><font size="3px">Vendor Master:</font></span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:FormView ID="FormView1" runat="server" Width="99%" AutoGenerateRows="False"
                                                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                                                    <InsertItemTemplate>
                                                                                        <table align="center" width="100%">
                                                                                            
                                                                                            <tr>                                                                                               

                                                                                                <td style="padding: 5px;">
                                                                                                    <span class="headFont">Pan &nbsp;&nbsp;</span><span class="require">*</span>
                                                                                                </td>
                                                                                                <td style="padding: 5px;">:
                                                                                                </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtPanNo" autocomplete="off" onkeyup="validationPANonkeypress()"  ClientIDMode="Static" runat="server" CssClass="textbox UpperCase"
                                                                                                        MaxLength="10"></asp:TextBox>
                                                                                                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                                                                                      ControlToValidate="txtPanNo" runat="server" ErrorMessage="Invalid PAN" ForeColor="Red" 
                                                                                                         ValidationExpression="^[\w]{3}(p|P|c|C|h|H|f|F|a|A|t|T|b|B|l|L|j|J|g|G)[\w][\d]{4}[\w]$"></asp:RegularExpressionValidator>--%>
                                                                                                </td>

                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Vendor Name&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtVendorName" ClientIDMode="Static" Enabled="false" runat="server" autocomplete="off" CssClass="textbox UpperCase" onkeyup="CopyfromVendornametoCompanyname()"
                                                                                                        MaxLength="400"></asp:TextBox>
                                                                                                    <div id="autoContainerSearch"></div>
                                                                                                </td>
                                                                                                <td style="padding: 5px;">
                                                                                                    <span class="headFont">Company Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                                                </td>
                                                                                                <td style="padding: 5px;">:
                                                                                                </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtCmpName" ClientIDMode="Static" Enabled="false" runat="server" autocomplete="off" CssClass="textbox UpperCase" 
                                                                                                        MaxLength="250"></asp:TextBox>
                                                                                                </td>


                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Address &nbsp;&nbsp;</span></td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtAddress" ClientIDMode="Static" runat="server" Height="30px" width="235%" autocomplete="off" TextMode="MultiLine" CssClass="textbox UpperCase"></asp:TextBox>

                                                                                                </td>

                                                                                            </tr>

                                                                                            <tr>

                                                                                                <td style="padding: 5px;">
                                                                                                    <span class="headFont">Email &nbsp;&nbsp;</span>
                                                                                                </td>
                                                                                                <td style="padding: 5px;">:
                                                                                                </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtEmail" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                                                        MaxLength="100"></asp:TextBox>
                                                                                                </td>


                                                                                                <td style="padding: 5px;">
                                                                                                    <span class="headFont">Mobile No &nbsp;&nbsp;</span>
                                                                                                </td>
                                                                                                <td style="padding: 5px;">:
                                                                                                </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtPhone" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" 
                                                                                                        MaxLength="10"></asp:TextBox>
                                                                                                </td>

                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td colspan="6">
                                                                                                    <hr style="border: solid 1px lightblue" />
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                                <td style="padding: 5px;">: </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:DropDownList ID="ddlBank" autocomplete="off" Width="220px" Height="23px" runat="server" DataSource='<%#drpload("Bank") %>'
                                                                                                        DataValueField="BankID" AutoPostBack="false" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                                        <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                                                    </asp:DropDownList>

                                                                                                </td>

                                                                                                <td style="padding: 5px;"><span class="headFont">Branch Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                                        <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                    <%--<asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server" DataValueField="BranchID" DataTextField="Branch" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                        <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtBranchID" ClientIDMode="Static" runat="server" Width="210px" Visible="false" CssClass="textbox"></asp:TextBox>--%>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">IFSC Code&nbsp;&nbsp;</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtIFSC" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                                <td style="padding: 5px;"><span class="headFont">MICR No&nbsp;&nbsp;</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtMICR" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Bank Account No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtBankCode" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" MaxLength="20"
                                                                                                        onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="6">
                                                                                                    <hr style="border: solid 1px lightblue" />
                                                                                                </td>

                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Active Flag&nbsp;&nbsp;</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:DropDownList ID="ddlActiveFlag" Width="220px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                                        <%--<asp:ListItem Text="(Select)" Selected="True" Value=""></asp:ListItem>--%>
                                                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>

                                                                                        </table>
                                                                                    </InsertItemTemplate>

                                                                                    <EditItemTemplate>
                                                                                        <table align="center" width="100%">
                                                                                            <tr>                                                                                               
                                                                                                <td style="padding: 5px;"><span class="headFont">Pan &nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                                                <td style="padding: 5px;">: </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtPanNo" ClientIDMode="Static" onkeyup="validationPANonkeypress()" runat="server" CssClass="textbox" Text='<%# Eval("CompanyPAN") %>'
                                                                                                        MaxLength="10" autocomplete="off"></asp:TextBox>
                                                                                                </td>

                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Vendor Name&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtVendorName" ClientIDMode="Static" runat="server" CssClass="textbox" onkeyup="CopyfromVendornametoCompanyname()"
                                                                                                        MaxLength="400" Text='<%# Eval("VendorName") %>' autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                                 <td style="padding: 5px;"><span class="headFont">Company Name&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                                                <td style="padding: 5px;">: </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtCmpName" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyName") %>'
                                                                                                        MaxLength="250" autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Address &nbsp;&nbsp;</span></td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtAddress" ClientIDMode="Static" runat="server" Height="30px" Width="235px" TextMode="MultiLine" CssClass="textbox" Text='<%# Eval("VendorAddress") %>' autocomplete="off"></asp:TextBox>

                                                                                                </td>

                                                                                            </tr>                                                                                           

                                                                                            <tr>

                                                                                                <td style="padding: 5px;"><span class="headFont">Email &nbsp;&nbsp;</span></td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtEmail" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyEmail") %>'
                                                                                                        MaxLength="100" autocomplete="off"></asp:TextBox>
                                                                                                </td>

                                                                                                <td style="padding: 5px;"><span class="headFont">Mobile No &nbsp;&nbsp;</span></td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtPhone" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyMobile") %>'
                                                                                                        MaxLength="10" autocomplete="off"></asp:TextBox>
                                                                                                </td>

                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td colspan="6">
                                                                                                    <hr style="border: solid 1px lightblue" />
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                                <td style="padding: 5px;">: </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:DropDownList ID="ddlBank" Width="220px" Height="23px" runat="server" DataSource='<%#drpload("Bank") %>' DataValueField="BankID" AutoPostBack="false" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                                        <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </td>

                                                                                                <td style="padding: 5px;"><span class="headFont">Branch Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <%--<asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server" DataSource='<%#drpload("Branch") %>' DataValueField="BranchID" DataTextField="Branch" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                            </asp:DropDownList> --%>
                                                                                                    <asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server" DataValueField="BranchID" DataTextField="Branch" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                                        <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">IFSC Code&nbsp;&nbsp;</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtIFSC" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                                <td style="padding: 5px;"><span class="headFont">MICR No&nbsp;&nbsp;</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtMICR" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Bank Account No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:TextBox ID="txtBankCode" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" Text='<%# Eval("AccountNo") %>'
                                                                                                        MaxLength="20" autocomplete="off"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="6">
                                                                                                    <hr style="border: solid 1px lightblue" />
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td style="padding: 5px;"><span class="headFont">Active Flag&nbsp;&nbsp;</span> </td>
                                                                                                <td style="padding: 5px;">:</td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <asp:DropDownList ID="ddlActiveFlag" Width="220px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                                        <%--<asp:ListItem Text="(Select)" Selected="True" Value=""></asp:ListItem>--%>
                                                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>

                                                                                    </EditItemTemplate>

                                                                                    <FooterTemplate>
                                                                                        <table align="center" width="100%">
                                                                                            <tr>
                                                                                                <td colspan="4">
                                                                                                    <hr style="border: solid 1px lightblue" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="padding: 5px;">
                                                                                                    <span class="require">*</span> indicates Mandatory Field
                                                                                                </td>
                                                                                                <td style="padding: 5px;">&nbsp;
                                                                                                </td>
                                                                                                <td style="padding: 5px;" align="left">
                                                                                                    <div style="float: left; margin-left: 200px;">
                                                                                                        <asp:Button ID="cmdPopUpSave" runat="server" Text="Create" CommandName="Add" Width="100px" 
                                                                                                            CssClass="Btnclassname DefaultButton" />
                                                                                                        <asp:Button ID="cmdPopUpCancel" runat="server" Text="Refresh" Width="100px" CssClass="Btnclassname DefaultButton " />

                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:FormView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <hr style="border: solid 1px lightblue" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>


                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="tabBillMast" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Location&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtLocationCd" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" Width="50px" 
                                                                        MaxLength="4"></asp:TextBox>
                                                                    <asp:TextBox ID="txtLocationDet" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="false" CssClass="textbox" Width="600px"
                                                                        MaxLength="100"></asp:TextBox>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bill No&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBillNo" ClientIDMode="Static" runat="server" CssClass="textbox"  MaxLength="500" autocomplete="off"></asp:TextBox>
                                                                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                                                    ControlToValidate="txtBillNo" runat="server" ErrorMessage="Invalid PAN" ForeColor="Red" 
                                                                                                         ValidationExpression="@"^[a-zA-Z0-9-_/.#$%^*()}{:;+=|!,?~@&<>'"" ]*$"></asp:RegularExpressionValidator>--%>

                                                                </td>

                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bill Date&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBillDate" ClientIDMode="Static" runat="server" CssClass="textbox" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Vendor Name&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                               <%-- <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlVendorName" Width="220px" Height="23px" runat="server" DataSource='<%#drpload("Vendor") %>'
                                                                        DataValueField="VendorId" AutoPostBack="false" DataTextField="VendorName" AppendDataBoundItems="true" ClientIDMode="Static" CssClass="UpperCase">
                                                                        <asp:ListItem Text="(Select Vendor)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:Button ID="cmdForm" runat="server" Text="Go To Vendor Master" Width="200px"
                                                                            CssClass="Btnclassname DefaultButton" />
                                                                    </td>--%>


                                                                <td style="padding: 5px;" align="left" colspan="7">
                                                                    <asp:TextBox ID="txtVendorNameSearch" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="400" Width="665px" autocomplete="off"></asp:TextBox>
                                                                    <asp:TextBox ID="hdnVendorNameSearch" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="8" Width="5px" style="display:none" autocomplete="off"></asp:TextBox>
                                                                    <asp:Button ID="cmdForm" runat="server" Text="Go To Vendor Master" Width="200px"
                                                                        CssClass="Btnclassname DefaultButton" />
                                                                    <div id="autoContainerVendorNameSearch">
                                                                    </div>
                                                                </td>


                                                            </tr>


                                                             <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>                                                           
                                                          <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Company Name&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                           <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtCompanyName" ClientIDMode="Static" runat="server" Enabled="false" CssClass="textbox"
                                                                        MaxLength="100" autocomplete="off"></asp:TextBox>&nbsp;&nbsp;

                                                              <span class="headFont">Company Pan&nbsp;&nbsp;:&nbsp;&nbsp;</span>
                                                               <asp:TextBox ID="txtCompanyPan" ClientIDMode="Static" runat="server" Enabled="false" CssClass="textbox"
                                                                        MaxLength="100" autocomplete="off"></asp:TextBox>
                                                           </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                                 
                                                                <tr>
                                                                    <td style="padding: 5px;"><span class="headFont">Vendor Bill No&nbsp;&nbsp;</span></td>
                                                                    <td style="padding: 5px;">: </td>
                                                                    <td style="padding: 5px;" align="left">
                                                                        <asp:TextBox ID="txtVendorBillNo" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="500" autocomplete="off"></asp:TextBox>
                                                                       
                                                                    </td>

                                                                </tr>

                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Vendor Bill Date&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtVendorBillDt" ClientIDMode="Static" runat="server" CssClass="textbox" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                        </table>
                                                    </InsertItemTemplate>
                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Location&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtLocationCd" ClientIDMode="Static" runat="server" CssClass="textbox" Width="50px"
                                                                        MaxLength="4" autocomplete="off"></asp:TextBox>
                                                                    <asp:TextBox ID="txtLocationDet" ClientIDMode="Static" runat="server" Enabled="false" CssClass="textbox" Width="500px"
                                                                        MaxLength="100" autocomplete="off"></asp:TextBox>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bill No&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBillNo" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bill Date&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBillDate" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Vendor Name&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <%--<td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlVendorName" Width="200px" Height="23px" runat="server" DataSource='<%#drpload("Vendor") %>'
                                                                        DataValueField="VendorId" AutoPostBack="true" DataTextField="VendorName" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                        <asp:ListItem Text="(Select Vendor)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </td>--%>
                                                                 <td style="padding: 5px;" align="left" colspan="7">
                                    <asp:TextBox ID="txtVendorNameSearch" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="400" Width="665px"></asp:TextBox>
                                    <asp:TextBox ID="hdnVendorNameSearch" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="8" Width="5px" Style="display: none;"></asp:TextBox>
                                                                       
                                    <div id="autoContainerVendorNameSearch">
                                    </div>
                                </td>
                                                        </tr>



                                                            <tr>
                                                                
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtCompanyName" ClientIDMode="Static" runat="server" Enabled="false" CssClass="textbox"
                                                                        MaxLength="100" autocomplete="off"></asp:TextBox>
                                                                </td>

                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtCompanyPan" ClientIDMode="Static" runat="server" Enabled="false" CssClass="textbox"
                                                                        MaxLength="100" autocomplete="off"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Vendor Bill No&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtVendorBillNo" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                                </td>

                                                            </tr>


                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Vendor Bill Date&nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtVendorBillDt" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                </td>

                                                            </tr>


                                                            <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="require">*</span> indicates Mandatory Field
                                                                </td>
                                                                <td style="padding: 5px;">&nbsp;
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" Width="100px"
                                                                            CssClass="Btnclassname DefaultButton" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Refresh" Width="100px" CssClass="Btnclassname DefaultButton" />                                                                        

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div style="overflow-y: scroll; height:500px; width: 100%">
                                                    <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                                                        AutoGenerateColumns="false" DataKeyNames="VendorBillID"
                                                        AllowPaging="false" PageSize="10" OnRowCommand="tbl_RowCommand"
                                                        CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
                                                        <AlternatingRowStyle BackColor="#FFFACD" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last"
                                                            Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <HeaderStyle />
                                                                <ItemTemplate>
                                                                    <div id="worktype">
                                                                        <a href="javascript:void(0);" id="btnEdit" itemid='<%# Eval("VendorBillID") %>'>
                                                                            <img src="img/Edit.jpg" alt="edit" title="Edit" style="Height: 15px; Width: 15px;" />
                                                                        </a>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <HeaderStyle />
                                                                <ItemTemplate>

                                                                    <div id="del">
                                                                        <a href="javascript:void(0);" id="btnDelete" itemid='<%# Eval("VendorBillID") %>'>
                                                                            <img src="img/Delete.gif" alt="delete" title="Delete" style="Height: 15px; Width: 15px;" />
                                                                        </a>
                                                                    </div>


                                                                    <%-- <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                        OnClientClick='return Delete();' CommandArgument='<%# Eval("VendorBillID") %>' />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="VendorBillID" Visible="false" ItemStyle-CssClass="labelCaption"
                                                                HeaderStyle-CssClass="TableHeader" HeaderText="VendorBillID" />

                                                            <asp:BoundField DataField="CenterID"
                                                                HeaderText="Loction Code" />
                                                            <asp:BoundField DataField="VendorName"
                                                                HeaderText="Vendor Name " />
                                                            <asp:BoundField DataField="BillNo"
                                                                HeaderText="Bill NO" />
                                                            <asp:BoundField DataField="BillDate"
                                                                HeaderText="Bill Date" />
                                                            <asp:BoundField DataField="VendorBillNo"
                                                                HeaderText="Vendor BillNo" />
                                                            <asp:BoundField DataField="VendorBillDate"
                                                                HeaderText="Vendor BillDate" />
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                            </td>
                                        </tr>


                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </a>

</asp:Content>


