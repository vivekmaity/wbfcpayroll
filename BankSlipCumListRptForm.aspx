﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BankSlipCumListRptForm.aspx.cs" Inherits="BankSlipCumListRptForm" %>

<%@ Register TagPrefix="art" TagName="TransferEmpLPC" Src="TransferEmpDetail.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript"> 

        
        //This is for 'Location' Binding on the Base of 'Sector'
        $(document).ready(function () {
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        var options = {};
                        options.url = "BankSlipCumListRptForm.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;                             
                        options.dataType = "json";                                   
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            var PayMon = t[0]["PayMonths"];
                            var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                           
                            $('#txtPayMonth').val(PayMon);
                            $('#hdnSalMonthID').val(PayMonID);
 
                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }

                }
                else {
                    $('#txtPayMonth').val('');
                   
                }

            });

        });

        function PrintOpentab() {

            var SectorID;
            var SalMonthID;
            var SalMonth;
            var jsmsg = "";
            var panmsg = "";
            var ReportType = $("#ddlReportType").val();
            SectorID = $("#ddlSector").val();
            SalMonthID = $('#hdnSalMonthID').val();
            SalMonth = $('#txtPayMonth').val();
            if (SectorID == "0") {
                alert("Please select Sector");
                $('#ddlSector').focus();
                return false;
            }

            if (SalMonthID == "") {
                alert("Please Select Sector");
                $('#ddlSector').focus();
                return false;
            }

            if (ReportType == "0") {
                alert("Please select Report Type");
                $('#ddlReportType').focus();
                return false;
            }



            var Q = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "', curmonth: '" + $('#txtPayMonth').val() + "'}";

            $.ajax({
                type: "POST",
                url: "BankSlipCumListRptForm.aspx/PanNo_Required1",
                data: Q,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg1) {
                    panmsg = JSON.parse(msg1.d);
                    if (panmsg != 'Pan No') {
                        alert(panmsg);
                        return false;
                    }
                    else {
                        var confirm_value = document.createElement("INPUT");
                        confirm_value.type = "hidden";

                        confirm_value.name = "confirm_value";
                        if (confirm("You can't provide any input after this process  for the month " + SalMonth + ". Do You Want to Continue ?")) {
                            confirm_value.value = "Yes";
                            document.forms[0].appendChild(confirm_value);

                            //*************************************************************************************   //get_employeeSectorStatus   for return 0 either 1
                            var F = "{SectorID:" + SectorID + "}";
                            $.ajax({
                                type: "POST",
                                url: "BankSlipCumListRptForm.aspx/Check_LPCforBankList",
                                data: F,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    data = JSON.parse(D.d); //alert(data);
                                    if (data == "NotGenerate") {
                                        var result = confirm("Sorry ! You Can not Generate Bank List.");
                                        if (result == true) {
                                            var K = "{SectorID:'" + SectorID + "'}";
                                            $.ajax({
                                                type: "POST",
                                                url: "BankSlipCumListRptForm.aspx/Show_LPCforBankList",
                                                data: K,
                                                dataType: "json",
                                                contentType: "application/json; charset=utf-8",
                                                success: function (D) {
                                                    // data = JSON.parse(D.d); //alert(data);
                                                    $('#mainDiv').html(D.d);
                                                    $("#lblHeading").text('Employee LPC Problem');
                                                    $("#overlay").show();
                                                    $("#dialog").fadeIn(300);
                                                }
                                            });
                                        }
                                    }
                                    else
                                    {
                                        var G = "{SectorID:'" + SectorID + "'}";
                                        $.ajax({
                                            type: "POST",
                                            url: "BankSlipCumListRptForm.aspx/Get_EmployeeSectorStatus",
                                            data: G,
                                            dataType: "json",
                                            contentType: "application/json; charset=utf-8",
                                            success: function (D) {
                                                data = JSON.parse(D.d); //alert(data);
                                                if (data == "NotAvail") {
                                                    var M = "{SectorID:'" + SectorID + "'}";
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "BankSlipCumListRptForm.aspx/Show_EmployeeSectorStatus",
                                                        data: M,
                                                        dataType: "json",
                                                        contentType: "application/json; charset=utf-8",
                                                        success: function (D) {
                                                            // data = JSON.parse(D.d); //alert(data);
                                                            $('#mainDiv').html(D.d);
                                                            $("#lblHeading").text('Location not Found Problem');
                                                            $("#overlay").show();
                                                            $("#dialog").fadeIn(300);
                                                        }
                                                    });
                                                }
                                                else
                                                {
                                                    //var M = "{SectorID:" + SectorID + ", SalMonthID:'" + SalMonthID + "'}";
                                                    //$.ajax({
                                                    //    type: "POST",
                                                    //    url: "BankSlipCumListRptForm.aspx/Generate_PFmoreorLess",
                                                    //    data: M,
                                                    //    dataType: "json",
                                                    //    contentType: "application/json; charset=utf-8",
                                                    //    success: function (D) {
                                                    //        data = JSON.parse(D.d); 
                                                    //        if (data == "GenerateReport")
                                                    //        {
                                                                var E = "{SectorID:" + SectorID + ", SalMonthID:" + SalMonthID + "}";
                                                                //alert(E);
                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "BankSlipCumListRptForm.aspx/EncodeValue",
                                                                    data: E,
                                                                    dataType: "json",
                                                                    contentType: "application/json; charset=utf-8",
                                                                    success: function (msg) {
                                                                        jsmsg = JSON.parse(msg.d);
                                                                        alert(jsmsg);

                                                                        var popUp = window.open("BankSlipCumListReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&SalMonth=" + SalMonth + "");
                                                                        return (popup) ? false : true;
                                                                    }

                                                                });
                                                            //}
                                                            //else
                                                            //{
                                                                
                                                            //    var M = "{SectorID:'" + SectorID + "', SalMonthID: '" + $('#hdnSalMonthID').val() + "'}";
                                                            //    //alert(M);
                                                            //    $.ajax({
                                                            //        type: "POST",
                                                            //        url: "BankSlipCumListRptForm.aspx/Show_PFmoreorLess",
                                                            //        data: M,
                                                            //        dataType: "json",
                                                            //        contentType: "application/json; charset=utf-8",
                                                            //        success: function (D) {
                                                            //            $('#mainDiv').html(D.d);
                                                            //            $("#lblHeading").text('PF Problem');
                                                            //            $("#overlay").show();
                                                            //            $("#dialog").fadeIn(300);
                                                            //        }
                                                            //    });
                                                            //}
                                                       // }
                                                    //});
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        else
                        {
                            confirm_value.value = "No";
                        }
                    }
                }

            });
            }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        $(document).ready(function () {
            $('#Close a').on('click', function () {
                $("#overlay").hide();
            });
        });




    </script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Bank Slip Cum List</h2>						
						</section>


                    <div class="content-overlay" id='overlay'>
                            <div class="wrapper-outer">
                                <div class="wrapper-inner">
                                    <div class="loadContent">
                                        <div id="Close" class="close-content"><a href="javascript:void(0);" id="prev" style="color: red;">Close</a></div>
                                        <div style="float: left;">
                                            <h1 style="font-family: Verdana; font-size: 18px; font-weight: bold;">
                                                <asp:Label ID="lblHeading" runat="server" ClientIDMode="Static" Text="" autocomplete="off"></asp:Label>
                                            </h1>
                                        </div>
                                        <table cellpadding="5" style="border: solid 1px lightblue; padding-bottom: 10px; width: 650px; overflow: scroll;" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table align="center" cellpadding="5">
                                                        <tr>
                                                            <td>
                                                                <div class='divCaption' id='divCaption'></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div id="mainDiv" runat="server" clientidmode="Static" style="width: 650px; height: 440px; overflow: scroll;">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" >
                                        <asp:TextBox ID="txtPayMonth" ClientIDMode="Static" autocomplete="off" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>

                                        </td>
                                        <td style="padding:5px;" class="style3"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                        <asp:DropDownList ID="ddlReportType" Width="200px" Height="28px"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">(Select Report Type)</asp:ListItem>
                                            <asp:ListItem Value="1">Bank Slip Cum List</asp:ListItem>
                                        </asp:DropDownList>
 
                                        </td>
 
                                       <asp:HiddenField ID="hdnSalMonthID" runat="server"  />
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="PrintOpentab();return false;"/> 
                                       
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

