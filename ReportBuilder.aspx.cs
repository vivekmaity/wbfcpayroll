﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class ReportBuilder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //int ReportID = 0;
        //string ParmFlag = string.Empty;
        try
        {
            if (!IsPostBack)
            {
                //mainDiv.InnerHtml = GetGridControlContent("~/GridviewTemplete.ascx", ReportID, ParmFlag);
                PopulateDropDown();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void PopulateDropDown()
    {
        try
        {
                DataTable dt = DBHandler.GetResult("Load_ReportName");
                ddlReportName.Items.Clear();
                if (dt.Rows.Count > 0)
                {
                    ddlReportName.Items.Insert(0, new ListItem("(Select Report Name)", "0"));
                    ddlReportName.DataSource = dt;
                    ddlReportName.DataTextField = "ReportHeader";
                    ddlReportName.DataValueField = "ReportID";
                    ddlReportName.DataBind();
                }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string AjaxGetGridCtrl(int ReportID, string ParmFlag)
    {
        return GetGridControlContent("~/GridviewTemplete.ascx", ReportID, ParmFlag);
    }

    private static String GetGridControlContent(String controlLocation, int ReportID, string ParmFlag)
    {
        var page = new Page();

        var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

        userControl.ReportID = ReportID;
        userControl.Parmflag = ParmFlag;

        page.Controls.Add(userControl);
        String htmlContent = "";

        using (var textWriter = new StringWriter())
        {
            HttpContext.Current.Server.Execute(page, textWriter, false);
            htmlContent = textWriter.ToString();
        }
        return htmlContent;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ShowDetails(int ReportID)
    {
        string JSONVal = "";
        try
        {
            DataSet dsRepQry = DBHandler.GetResults("returnReportQuery", ReportID);
            DataTable dtRepQry = dsRepQry.Tables[0];

            if (dtRepQry.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                if (dtRepQry.Rows.Count > 0)
                {
                    foreach (DataRow r in dtRepQry.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRepQry.Columns)
                        {
                            row.Add(col.ColumnName, r[col]);
                        }
                        rows.Add(row);
                    }
                }
                JSONVal = rows.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ShowButtonForReport(int ReportID)
    {
        string JSONVal = "";
        try
        {
            DataSet dsRepQry = DBHandler.GetResults("Get_ReportQuery", ReportID);
            DataTable dtRepQry = dsRepQry.Tables[0];
            if (dtRepQry.Rows.Count > 0)
            {
                string ParamValue = dtRepQry.Rows[0]["ParameterFlag"].ToString();
                JSONVal = ParamValue.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }
}