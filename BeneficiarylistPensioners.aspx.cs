﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class BeneficiarylistPensioners : System.Web.UI.Page
{
    public static string hdnpenMonthID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulatePensionMonth();
        }
    }


    protected void PopulatePensionMonth()
    {
        try
        {
            var FinYear = HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString();
            DataTable dt = DBHandler.GetResult("Get_PensionMonth", FinYear);
            if(dt.Rows.Count>0)
            {
             txtPayMonth.Text=dt.Rows[0]["PenMonth"].ToString();
           //  hdnPenMonthID.Value = dt.Rows[0]["PensionMonthID"].ToString();
                hdnpenMonthID= dt.Rows[0]["PensionMonthID"].ToString();
            }
           
        }
        catch (Exception exx)
        {
            throw new Exception(exx.Message);
        }
    }

    [WebMethod]
    //public static string Report_Paravalue(string PenMonthid)
    //{
    //    try
    //    {
    //        DataSet ds1 = DBHandler.GetResults("PensionProcess", PenMonthid, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
    //        String Msg = "Salary Generation has been Completed Successfully";
    //        JavaScriptSerializer jscript = new JavaScriptSerializer();
    //        return jscript.Serialize(Msg);
    //    }
    //    catch (Exception exx)
    //    {
    //        throw new Exception(exx.Message);
    //    }
    //}

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(string PenMonthid, string FormName, string ReportName, string ReportType)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        String Msg = "";
        string StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        //DataTable DTCheckReportVal1 = DBHandler.GetResult("LoadReport_CalCu_Pension", StrCaseFileNo, EmployeeID, UserID);
        //StrFormula = "{tmp_Pensionslip_monthly.inserted_By}=" + UserID + "";
        StrFormula = "{MST_PenPensioner.AcceptFlag}=1 and {MST_PenPensioner.Checkedflag}=1";
             
        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

        //DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Update_RptParameterValue", UserID, FormName, ReportName, ReportType, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9, Parameter10);

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }

}