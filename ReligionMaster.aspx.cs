﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;

public partial class ClassificationMaster : System.Web.UI.Page
{
    public static string hdnReligionID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    //protected DataTable drpload()
    //{
    //    DataTable dt = DBHandler.GetResult("show_cdropdown");
    //    return dt;
    //}
   

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void PopulateGrid()
    {
        try
        {

            DataTable dt = DBHandler.GetResult("Get_Religion");
            tbl.DataSource = dt;
            tbl.DataBind();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_ReligionbyID", ID);
                //Session["ReligionID"] = dtResult.Rows[0]["ReligionID"].ToString();
                dv.DataSource = dtResult;
                dv.DataBind();
                hdnReligionID= dtResult.Rows[0]["ReligionID"].ToString();



            }
            else if (flag == 2)
            {
                DBHandler.Execute("DELETE_ReligionbyID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                //TextBox txtrc = (TextBox)dv.FindControl("txtrc");
                TextBox txtr = (TextBox)dv.FindControl("txtr");
                

               

                //TextBox txtDOR = (TextBox)dv.FindControl("txtDOR");
                // DateTime dt1 = DateTime.ParseExact(txtDOR.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                DBHandler.Execute("INSERT_Religion",
                     DBNull.Value,
                     txtr.Text.Equals("") ? DBNull.Value : (object)(txtr.Text),
                     Session[SiteConstants.SSN_INT_USER_ID]);
                  

                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {

                //TextBox txtrc = (TextBox)dv.FindControl("txtrc");
                TextBox txtr = (TextBox)dv.FindControl("txtr");



                // DateTime dt1 = DateTime.ParseExact(txtDOR.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                string ID = hdnReligionID;

                 //   ((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("UPDATE_Religion", ID,
                     DBNull.Value,
                      txtr.Text.Equals("") ? DBNull.Value : (object)(txtr.Text),
                  
                     Session[SiteConstants.SSN_INT_USER_ID]);

                    cmdSave.Text = "Create";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tbl.PageIndex = e.NewPageIndex;
        PopulateGrid();
    }
}