﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;


    public partial class _Default : System.Web.UI.Page
    {
        string SecID = string.Empty;
        string flag = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session[SiteConstants.SSN_INT_USER_ID] == null)
                {
                    Response.Redirect("Default.aspx", false);
                }
                mainDiv.InnerHtml = GetGridControlContent("~/TransferEmpDetail.ascx", SecID, flag);
                if (!IsPostBack)
                {
                    if (Session[SiteConstants.SSN_INT_USER_ID] != null)
                    {
                        PopulateGrid();
                    }
                    PopulateGrids();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected void PopulateGrid()
        {
            try
            {
                DataTable dtsector = DBHandler.GetResult("Get_SectorPermission", Session[SiteConstants.SSN_INT_USER_ID]);
                if (dtsector.Rows.Count > 0)
                {
                    grdViewOrders.DataSource = dtsector;
                    grdViewOrders.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void PopulateGrids()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Name");
                DataRow dtrow = dt.NewRow();
                dtrow["Name"] = "Designation Not Set";
                dt.Rows.Add(dtrow);

                dtrow = dt.NewRow();
                dtrow["Name"] = "Location Not Set";
                dt.Rows.Add(dtrow);

                dtrow = dt.NewRow();
                dtrow["Name"] = "Bank Details Not Set";
                dt.Rows.Add(dtrow);

                grvOther.DataSource = dt;
                grvOther.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void grdViewOrders_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    GridView grvOther = (GridView)e.Row.FindControl("grvOther");
            //    DataTable dt = new DataTable();
            //    dt.Columns.Add("Name");
            //    DataRow dtrow = dt.NewRow();
            //    dtrow["Name"] = "Designation Not Set";
            //    dt.Rows.Add(dtrow);

            //    dtrow = dt.NewRow();
            //    dtrow["Name"] = "Location Not Set";
            //    dt.Rows.Add(dtrow);

            //    dtrow = dt.NewRow();
            //    dtrow["Name"] = "Bank Details Not Set";
            //    dt.Rows.Add(dtrow);

            //    grvOther.DataSource = dt;
            //    grvOther.DataBind();
            //}
        }

        [WebMethod]
        public static string AjaxGetGridCtrl(string SecID, string flag)
        {
            return GetGridControlContent("~/TransferEmpDetail.ascx", SecID, flag);
        }

        private static String GetGridControlContent(String controlLocation, string SecID, string flag)
        {
            var page = new Page();

            var userControl = (TransferEmpDetail)page.LoadControl(controlLocation);

            userControl.SecID = SecID;
            userControl.flag = flag;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string AcceptEmployee(string ArrayListEmp, string ArrayListSec, string ArrayListCen)
        {
            string JSONVal = ""; string result = string.Empty;
            try
            {
                string[] listEmp = ArrayListEmp.Split(',');
                string[] listSec = ArrayListSec.Split(',');
                string[] listCen = ArrayListCen.Split(',');

                for (int i = 0; i < listEmp.Length && i < listSec.Length && i < listCen.Length; i++)
                {
                  string EmpNo = listEmp[i].ToString();
                  string SecID = listSec[i].ToString();
                  string CenID = listCen[i].ToString();

                  DataTable dt = DBHandler.GetResult("Update_ApplyEmpMasterforLPC", EmpNo);
                  if (dt.Rows.Count > 0)
                  {
                      string value = dt.Rows[0]["Result"].ToString();
                      if(value =="Updated")
                            result = "success";
                      else
                          result = "fail";
                  }
                }

                

                JSONVal = result.ToJSON();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return JSONVal;
        }
    }

