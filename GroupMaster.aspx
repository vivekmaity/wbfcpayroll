﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="GroupMaster.aspx.cs" Inherits="GroupMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            $("#txtGroupCode").rules("add", { required: true, messages: { required: "Please enter Group Code"} });
            $("#txtGroupName").rules("add", { required: true, messages: { required: "Please enter Group Name"} });
        }           
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Class  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Class Code &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtGroupCode" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="1"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Class Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtGroupName" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 10px;"><span class="headFont">Class Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtGroupCode" Text='<%# Eval("GroupCode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="1" Enabled="false" autocomplete="off"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("GroupID") %>' />
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Class Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtGroupName" Text='<%# Eval("Group") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" autocomplete="off"></asp:TextBox>

                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                                <td style="padding: 5px;">&nbsp;</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"
                                                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();'
                                                                            OnClick="cmdSave_Click" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                                                            Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                                    DataKeyNames="GroupCode" OnRowCommand="tbl_RowCommand" AllowPaging="true" PageSize="10"
                                                    CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                                     <AlternatingRowStyle BackColor="#FFFACD" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Edit">
                                                            <HeaderStyle  />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                    runat="server" ID="btnEdit" CommandArgument='<%# Eval("GroupID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Delete">
                                                            <HeaderStyle  />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                    OnClientClick='return Delete();'
                                                                    CommandArgument='<%# Eval("GroupID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="GroupID" Visible="false"  HeaderText="Class ID" />
                                                        <asp:BoundField DataField="GroupCode"  HeaderText="Class Code" ItemStyle-HorizontalAlign="Center"/>
                                                        <asp:BoundField DataField="Group"  HeaderText="Class Name" />
                                                    </Columns>
                                                </asp:GridView>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

