﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="SalaryGeneration.aspx.cs" Inherits="SalaryGeneration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        //This is for 'Location' Binding on the Base of 'Sector'
                        
        $(document).ready(function () {
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();      
                if (s != 0) {       
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        var options = {};
                        options.url = "SalaryGeneration.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;                
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                        
                            var PayMon = t[0]["PayMonths"];
                            var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                            $('#txtPayMonth').val(PayMon);
                            $('#hdnSalMonthID').val(PayMonID);
                           
                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }
                   
                }
                
            });

        });


        function opentab() {
            var SectorID;
            var SalMonth;
            var CurrMonth;
            SectorID = $("#ddlSector").val();
            SalMonth = $('#hdnSalMonthID').val();
            CurrMonth = $('#txtPayMonth').val();
           
            if (SalMonth == "") {
                alert("Please Select Sector");
                $('#ddlSector').focus();
                return false;
                
            }

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Process Salary For The Month " + CurrMonth + " ?")) {
                confirm_value.value = "Yes";
            $(".loading-overlay").show();
            var E = "{SectorID:" + SectorID + ",SalMonth:" + SalMonth + "}";
            $.ajax({
                type: "POST",
                url: "SalaryGeneration.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    
                    $(".loading-overlay").hide();
                    var jsmsg = JSON.parse(msg.d);
                    alert(jsmsg);                     
                                       
                }

             });
        } else {
            confirm_value.value = "No";
            
        }
                        
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
    <style type="text/css">
        .style1
        {
            width: 64px;
        }
        .style2
        {
            width: 3px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Salary Generation</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" ><span class="headFont">Sal Month</span> </td>
                                        <td style="padding:5px;" class="style2" >:</td>
                                        <td style="padding:5px;" align="left" >
                                        <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>
                                        </td>
                                        
                                       <asp:HiddenField ID="hdnSalMonthID" runat="server"  />

                                    </tr>
                                </table>

                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Salary Generate"   
                                        Width="120px" CssClass="Btnclassname" OnClientClick="opentab(); return false;"/> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                         <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                        </div>
                                    </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                                     
                     
                  
                   
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

