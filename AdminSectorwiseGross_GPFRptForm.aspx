﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AdminSectorwiseGross_GPFRptForm.aspx.cs" Inherits="AdminSectorwiseGross_GPFRptForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        
        function opentab() {
            var SalMonth;
            var SalMonthID;
            var ReportType;
            var CPFORGPF;
            var PFCode;
            SalMonth = $('#txtPayMonth').val();
            SalMonthID = 0;
            ReportType = $("#ddlReportType").val();                    
            if (SalMonth == "") {
                alert("Salary Month Invalid");
                return false;
            }
         
            if (ReportType == "0") {
                alert("Please select Report Type");
                $('#ddlReportType').focus();
                return false;
            }

            if (ReportType == "1") {
                CPFORGPF = "GPF";
                PFCode = "99";
            }

            else  {
                CPFORGPF = "CPF";
                PFCode ="96";           
            }           
              
            var E = "{SalMonth:'" + SalMonth + "',SalMonthID:" + SalMonthID + ",CPFORGPF:'" + CPFORGPF + "',PFCode:" + PFCode + "}";
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",               
                url: "AdminSectorwiseGross_GPFRptForm.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    window.open("AdminSectorwiseGross_GPFReportView.aspx?ReportID=" + ReportType + "&SalMonth=" + SalMonth + "");
                    $(".loading-overlay").hide();
                }
            });                           
        }
                
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Admin Sector wise Gross , GPF and Without GPF</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" >
                                        <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>

                                        </td>
                                        <td style="padding:5px;" class="style3"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                        <asp:DropDownList ID="ddlReportType" Width="280px" Height="28px"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">Select Report Type</asp:ListItem>
                                            <asp:ListItem Value="1">Sector wise Gross,GPF and Without GPF</asp:ListItem>
                                            <asp:ListItem Value="2">Sector wise Gross,CPF and Without CPF</asp:ListItem>
                                        </asp:DropDownList>   
                                        </td>
 
                                       <%--<asp:HiddenField ID="hdnSalMonthID" runat="server"  />--%>
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;" /> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>

                                      
                                        
                                    </div>
                                        <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                        </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

