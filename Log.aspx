﻿<%@ Page Language="C#" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Log.aspx.cs" Inherits="Log" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style> 
inputs:-webkit-input-placeholder {
    color: #b5b5b5;
}

inputs-moz-placeholder {
    color: #b5b5b5;
}

.inputs {
    padding: 15px 25px;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-weight: 400;
    font-size: 14px;
    color: #9D9E9E;
    width:200px;
    text-shadow: 1px 1px 0 rgba(256, 256, 256, 1.0);
    background: #FFF;
    border: 1px solid #FFF;
    border-radius: 5px;
    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.50);
    -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.50);
    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.50);
}

.inputs:focus {
    background: #DFE9EC;
    color: #006E8B;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.25);
    -moz-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.25);
    -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.25);
    outline: 0;
}

.inputs:hover {
    background: #DFE9EC;
    color: #414848;
}
.main-div 
	{
	    height:200px;
	    width:auto;
	    /*border-style: solid;*/
	    border-color: #ffffff;
	    border-width: medium;
        /*margin:10px;*/
        padding-left:35px;
        text-align:center;
    }


.log-button {
	/*-moz-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	box-shadow:inset 0px 1px 0px 0px #bee2f9;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #63b8ee), color-stop(1, #468ccf) );
	background:-moz-linear-gradient( center top, #63b8ee 5%, #468ccf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#63b8ee', endColorstr='#468ccf');
	background-color:#63b8ee;
	-webkit-border-top-left-radius:9px;
	-moz-border-radius-topleft:9px;
	border-top-left-radius:9px;
	-webkit-border-top-right-radius:9px;
	-moz-border-radius-topright:9px;
	border-top-right-radius:9px;
	-webkit-border-bottom-right-radius:9px;
	-moz-border-radius-bottomright:9px;
	border-bottom-right-radius:9px;
	-webkit-border-bottom-left-radius:9px;
	-moz-border-radius-bottomleft:9px;
	border-bottom-left-radius:9px;
	text-indent:8px;
	border:1px solid #3866a3;
	display:inline-block;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:40px;
	line-height:40px;
	width:100px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 38px #7cacde;*/
    border:1px solid #15aeec; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 3px 3px 3px 3px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
 background-color: #49c0f0; background-image: -webkit-gradient(linear, left top, left bottom, from(#49c0f0), to(#2CAFE3));
 background-image: -webkit-linear-gradient(top, #49c0f0, #2CAFE3);
 background-image: -moz-linear-gradient(top, #49c0f0, #2CAFE3);
 background-image: -ms-linear-gradient(top, #49c0f0, #2CAFE3);
 background-image: -o-linear-gradient(top, #49c0f0, #2CAFE3);
 background-image: linear-gradient(to bottom, #49c0f0, #2CAFE3);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#49c0f0, endColorstr=#2CAFE3);
 width:100px;height:40px;
}
.log-button:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #468ccf), color-stop(1, #63b8ee) );
	background:-moz-linear-gradient( center top, #468ccf 5%, #63b8ee 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#468ccf', endColorstr='#63b8ee');
	background-color:#468ccf;
}.log-button:active {
	position:relative;
	top:1px;
}
</style>
    
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>

</head>
<body>
    <form id="form1" runat="server">
        <div id="header-wrapper">
			<div class="container">
				<div class="row">
					<div class="12u">
						
						<header id="header">
							<h1><img src="images/Kmda-logo.gif" alt="" width="100" height="50" />&nbsp;WEST BENGAL FINANCIAL CORPORATION</h1>
						</header>
					    
					</div>
				</div>
			</div>
		</div>
        <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						
						<%--<section>
							<h2>Login Page</h2>						
						</section>--%>

                        
                            <div>
                                <table style="text-align:left; width:100%; font-family:Verdana;">
                                    <tr>
                                        <td></td>
                                        <td style="font-weight:bold;  font-style: italic;">
                                            <section>
							                        <h2>Login Page</h2>						
						                    </section>
                                        </td>
                                    </tr>
                                    <tr>
                                      <td></td>
                                        <td style="width:60%;">
                                            <asp:TextBox autocomplete="off" ID="txtUserName" runat="server" class="inputs" placeholder="User Name"></asp:TextBox><br />
                                            <asp:TextBox autocomplete="off" ID="txtPass" TextMode="Password" runat="server" class="inputs" placeholder="Password"></asp:TextBox><br />
                                            <br />

                                            <asp:Button ID="btnLogin" runat="server" CssClass="log-button" Text="Login" OnClick="btnLogin_Click" />
                                        </td>
                                    </tr>
                                </table>
                              
                            </div>
                        </div>
				</div>
			</div>
		</div>
    </form>
</body>
</html>
