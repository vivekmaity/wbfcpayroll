﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ClassificationMaster.aspx.cs" Inherits="ClassificationMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            $("#txtcc").rules("add", { required: true, messages: { required: "Please enter name"} })
            $("#txtc").rules("add", { required: true, messages: { required: "Please enter name"} })
           
        }
        

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

//      function isNumberKey(evt)
//       {
//          var charCode = (evt.which) ? evt.which : evt.keyCode;
//          if (charCode != 46 && charCode > 31 
//            && (charCode < 48 || charCode > 57))
//             return false;

//          return true;
//       }
 
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Classification  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%"
                                                    AutoGenerateRows="False"
                                                    OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center"
                                                    GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 10px;"><span class="headFont">Classification Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtcc" ClientIDMode="Static" MaxLength="4" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                </td>
                                   
                                                                <td style="padding: 10px;"><span class="headFont">Classification&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtc" ClientIDMode="Static" MaxLength="100" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>



                                                        </table>

                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">


                                                            <tr>
                                                                <td style="padding: 10px;"><span class="headFont">Classification Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtcc" Text='<%# Eval("ClassificationCode") %>' ClientIDMode="Static" MaxLength="4" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                    <%--<asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ClassificationID") %>' />--%>
                                                                </td>
                                   
                                                                <td style="padding: 10px;"><span class="headFont">Classification &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtc" Text='<%# Eval("Classification") %>' ClientIDMode="Static" MaxLength="100" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 10px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                                <td style="padding: 10px;">&nbsp;</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"
                                                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();'
                                                                            OnClick="cmdSave_Click" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                                                            Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" PageSize="10"
                                                    AutoGenerateColumns="false" DataKeyNames="ClassificationID" OnRowCommand="tbl_RowCommand" AllowPaging="true"
                                                    CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                                     <AlternatingRowStyle BackColor="#FFFACD" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="Edit">
                                                            <HeaderStyle  />
                                                            <ItemTemplate>

                                                                <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                    runat="server" ID="btnEdit" CommandArgument='<%# Eval("ClassificationID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Delete">
                                                            <HeaderStyle />
                                                            <ItemTemplate>

                                                                <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                    OnClientClick='return Delete();'
                                                                    CommandArgument='<%# Eval("ClassificationID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="ClassificationID" Visible="false"  HeaderText="ClassificationID" />
                                                        <asp:BoundField DataField="ClassificationCode"  HeaderText="ClassificationCode" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="Classification"  HeaderText="Classification" />


                                                    </Columns>
                                                </asp:GridView>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

