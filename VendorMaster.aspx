﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" CodeFile="VendorMaster.aspx.cs" Inherits="VendorMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" runat="Server">

    <link href="css/Gridstyle.css" rel="stylesheet" />

       <script type="text/javascript"> 
           // company validation after 10th number digit enter..
           function validationPANonkeypress()
           {
               var txtPAN = $("#txtPanNo").val();
               if (txtPAN.length == 10) {
                   PANnumberfullVALIDATion();
               }
           }
           function PANnumberfullVALIDATion(){           
               var isValid = false;
               var regex = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/;
               isValid = regex.test($("[id*=txtPanNo]").val());
               if (isValid == false) {
                   var value = $('#txtPanNo').val();
                   if (value == "" || value == " ") {
                      // confirm("");
                       alert("Enter a PAN number.");
                     //  $('#txtPanNo').focus();
                       return false;
                   }
                   else if (value.length != 10) {
                       alert("You miss some digit/digits of PAN number");
                    //   $('#txtPanNo').focus();
                       return false;
                   }
                   else {
                       alert("You have entered a wrong PAN number");
                    //   $('#txtPanNo').focus();
                       return false;
                   }
               }
               else {
                   duplicate_check();
               }
           }
       // company validation on blur funtion....
           $(document).ready(function () {
               $("#txtPanNo").blur(function (evt) {                   
                   PANnumberfullVALIDATion();
               });
           });
           

           // Checking  pan no exit  or not in database..          
           function duplicate_check() {             
               var PANNO = $('#txtPanNo').val();              
               var W = "{panno:'" + PANNO + "'}";
               $.ajax({
                   type: "POST",
                   url: 'VendorMaster.aspx/DuplicatePannumber_Checkings',
                   data: W,
                   contentType: "application/json; charset=utf-8",
                   success: function (data) {
                       var item = jQuery.parseJSON(data.d); 
                       if (item == "Available") {
                           alert(PANNO + " this PAN number already in record.. try another pan number");
                         //  $('#txtPanNo').focus();
                           return false;
                       }
                       else {
                           $("#txtVendorName").prop('disabled', false);
                           $("#txtCompanyName").prop('disabled', false);
                           $("#txtVendorName").focus();
                           return true;
                       }
                   }
               });
           }
           $(document).ready(function(){
               $("#txtPanNo").keyup(function () {
                   var value = $("#txtPanNo").val();
                   if (value.length != 10)
                   {
                       $("#txtVendorName").prop('disabled', true);
                       $("#txtCompanyName").prop('disabled', true);
                   }

               });
           
           });


               //Copy from vendorname to company name...
               function CopyfromVendornametoCompanyname() {
                   var copy = $("#txtVendorName").val();
                   $("#txtCompanyName").val(copy);                 
               }

               $.validator.addMethod("ValidPhoneNO", function (value, element) {
                   return this.optional(element) || value.match(/^([1-9][0-9]*)|([0]+)$/);
               }, "Mobile Number Cannot Start With Zero");

               $.validator.addMethod("validPanno", function (value, element) {
                   //if (value != "") {
                   //var regex = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/;
                   return this.optional(element) || value.match(/^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/);
                   //}
               }, "Not a valid Pan No.Please enter a valid Pan no.");



               $(document).ready(function () {
                   $.validator.addMethod("validEmail", function (value, element) {
                       return this.optional(element) || value == value.match(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);//  /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
                       // --                                    or leave a space here ^^
                   }, "Not a Valid Email Address.Please Enter Valid Email Address. ");
                   $(".DefaultButton").click(function (event) {
                       event.preventDefault();
                   });

                   $(document).ready(function () {
                       $.validator.addMethod("noquotes", function (value, element) {
                           return this.optional(element) || value == value.match(/^[^'"]*$/);//value == value.match(/'"/);
                           // --                                    or leave a space here ^^
                       }, "No single or double quotes are allowed.");
                       $(".DefaultButton").click(function (event) {
                           event.preventDefault();
                       });
                   });

               });

               function ClearVendorMast() {
                   $("#txtVendorName").val("");
                   $("#txtAddress").val("");
                   $("#txtCompanyName").val("");
                   $("#txtPanNo").val("");
                   $("#txtEmail").val("");
                   $("#txtPhone").val("");
                   $("#txtCmpName").val("");
                   $("#ddlBank").val("");
                   $("#ddlBranch").val("");
                   $("#txtIFSC").val("");
                   $("#txtMICR").val("");
                   $("#txtBankCode").val("");
                   $("#ddlActiveFlag").val("");

               }

               //$(document).ready(function () {
               //    $("#txtPanNo").autocomplete({
               //        source: function (request, response) {
               //            var value = request.term; 
               //            if (value.length != 10) {
               //                alert("wrong pan no available");
               //                $("#txtPanNo").val("");
               //                $('#txtPanNo').focus();
               //                return;
               //            }
               //        },
               //        minLength: 10
               //    });
               //});
       
               $(document).ready(function () {
                   $("#txtVendorName").keydown(function (event) {
                       // Allow: backspace, delete, tab, escape, and enter
                       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 32 || (event.keyCode > 47 && event.keyCode < 58) ||
                           // Allow: Ctrl+A
                   (event.keyCode == 65 && event.ctrlKey === true) ||
                           // Allow: home, end, left, right
                   (event.keyCode >= 35 && event.keyCode <= 39)) {
                           // let it happen, don't do anything
                           return;
                       }
                       else {
                           // Ensure that it is a number and stop the keypress
                           if (event.shiftKey || (event.keyCode < 65 || event.keyCode > 90) && (event.keyCode < 96 || event.keyCode > 105)) {
                               event.preventDefault();
                               $("#errmsg").html("Digits Only").show().fadeOut(1500);
                               return false;

                           }
                       }
                   });
               });


               $(document).ready(function () {
                   $("#txtCompanyName").keydown(function (event) {
                       // Allow: backspace, delete, tab, escape, and enter
                       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||event.keyCode==32||event.keyCode == 190||
                           // Allow: Ctrl+A
                   (event.keyCode == 65 && event.ctrlKey === true) ||
                           // Allow: home, end, left, right
                   (event.keyCode >= 35 && event.keyCode <= 39)) {
                           // let it happen, don't do anything
                           return;
                       }
                       else {
                           // Ensure that it is a number and stop the keypress
                           if (event.shiftKey || (event.keyCode < 65 || event.keyCode > 90) && (event.keyCode < 96 || event.keyCode > 105)) {
                               event.preventDefault();
                               $("#errmsg").html("Digits Only").show().fadeOut(1500);
                               return false;

                           }
                       }
                   });
               });

           

               $(document).ready(function () {
                   $("#txtPhone").keydown(function (event) {
                       if (event.keyCode == 46 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 8 ||
                   (event.keyCode == 65 && event.ctrlKey === true) ||
                   (event.keyCode >= 35 && event.keyCode <= 39)) {
                           //alert(parseInt($('#txtPhone').val()).toString().length);
                           if ((event.keyCode != 8)) {
                               if (parseInt($('#txtPhone').val()).toString().length < 10) {
                                   alert("Please insert a valid mobile no.");
                                   $('#txtPhone').focus();
                                   return false;
                               }
                               return;
                           } else {
                               return true;
                           }
                       }
                       else {
                           if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                               event.preventDefault();
                               $("#Span3").html("Digits Only").show().fadeOut(1500);
                               return false;

                           }
                       }

                   });
               });


               $(document).ready(function () {
                   $("#txtBankCode").keydown(function (event) {
                       if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                   (event.keyCode == 65 && event.ctrlKey === true) ||
                   (event.keyCode >= 35 && event.keyCode <= 39)) {
                           //if ($('#txtPhone').length > 10 || $('#txtPhone').length < 10) {
                           //    alert("Please insert a valid mobile no.");
                           //}
                           return;
                       }
                       else {
                           if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                               event.preventDefault();
                               $("#Span3").html("Digits Only").show().fadeOut(1500);
                               return false;

                           }
                       }
                   });
               });


               //$("#txtPanNo").blur(function (event) {
               //    ValidatePAN();
               //});

               //function ValidatePAN() {
               //    var letters = /^[A-Za-z]/;
               //    var value = $('#txtPanNo').val();

               //    var self = this;

               //    // alert(secval);
               //    //alert(thirdval);
               //    if (value.length != 10) {
               //        alert("wrong pan no");
               //        //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
               //        //setTimeout(function () { self.focus(); }, 10);
               //    }
               //    else {
               //        var firstval = value.substring(0, 5);
               //        var secval = value.substring(5, 9);
               //        var thirdval = value.substring(9, 10);
               //    }
               //    for (var i = 0; i <= firstval.length - 1; i++) {

               //        var chkchar = firstval.substring(i, i + 1);
               //        if (chkchar.match(letters)) {
               //        }
               //        //else {
               //        //    //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
               //        //    alert("wrong pan no");
               //        //    //setTimeout(function () { self.focus(); }, 10);

               //        //}
               //    }

               //    for (var i = 0; i <= secval.length - 1; i++) {

               //        var chkchar1 = secval.substring(i, i + 1);
               //        if ($.isNumeric(chkchar1)) {
               //        }
               //        //else {
               //        //    //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
               //        //    alert("wrong pan no");
               //        //    //setTimeout(function () { self.focus(); }, 10);
               //        //}
               //    }
               //    if (thirdval.match(letters)) {

               //    }
               //    else {
               //        //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
               //        alert("wrong pan no");
               //        //setTimeout(function () { self.focus(); }, 10);
               //    }
               //}


        

               $(document).ready(function () {
                   $('#cmdCancel').click(function () {
                       //ClearVendorMast()
                       window.location.href = "VendorMaster.aspx";
                   });
               });

    </script>

    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate({ ignore: "" });
            $("#txtVendorName").rules("add", { required: true, messages: { required: "Please enter Vendor Name" } });
            $("#txtCompanyName").rules("add", { required: true, messages: { required: "Please enter Company Name" } });
            $("#txtPanNo").rules("add", { required: true, messages: { required: "Please enter Pan No" } });
            $("#ddlBank").rules("add", { required: true, messages: { required: "Please Select Bank Name" } });
            $("#ddlBranch").rules("add", { required: true, messages: { required: "Please Select Branch Name" } });
            $("#txtBankCode").rules("add", { required: true, messages: { required: "Please Enter Bank Account No" } });
            $("#txtPhone").rules("add", { required: true, messages: { required: "Please Enter Mobile Number" } });

            $("#txtPanNo").rules("add", { validPanno: true, messages: { required: "Please enter Pan No" } });

            $("#txtEmail").rules("add", { validEmail: true, messages: { required: "Please enter Valid EmailId" } });

            $("#txtAddress").rules("add", {
                noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },
            });

            $("#txtPhone").rules("add", { ValidPhoneNO: true, messages: { required: "Please enter Valid Phone Number" } });
            
          

            $("#txtPanNo").valid();
            $("#txtEmail").valid();
            $("#txtAddress").valid();
            $("#txtPhone").valid();
            if (!$("#form1").valid()) {
                return false;
            }
            
        }      

        
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        
       

    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Vendor  Master</h2>
                    </section>

                     <asp:HiddenField ID="hdnVendorID" runat="server" Value="" ClientIDMode="Static" />

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>                                                            

                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Pan &nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPanNo" autocomplete="off" ClientIDMode="Static" onkeyup="validationPANonkeypress()" runat="server" CssClass="textbox UpperCase"
                                                                        MaxLength="10"></asp:TextBox>
                                                                </td>

                                                            </tr>
                                                            <tr>

                                                                <td style="padding: 5px;"><span class="headFont">Vendor Name&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtVendorName" autocomplete="off" ClientIDMode="Static" Enabled="false" runat="server" CssClass="textbox UpperCase" OnTextChanged="txtVendorName_TextChanged" 
                                                                        MaxLength="400" onkeyup="CopyfromVendornametoCompanyname()"></asp:TextBox>
                                                                </td>

                                                                 <td style="padding: 5px;">
                                                                    <span class="headFont">Company Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtCompanyName" autocomplete="off" ClientIDMode="Static" Enabled="false" runat="server" CssClass="textbox UpperCase"
                                                                        MaxLength="250"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Address &nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left" colspan="4">
                                                                    <asp:TextBox ID="txtAddress" autocomplete="off" ClientIDMode="Static" runat="server"  height="30px" Width="830px" TextMode="MultiLine" CssClass="textbox UpperCase"></asp:TextBox>

                                                                </td>

                                                            </tr>

                                                            
                                                            <tr>

                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Email &nbsp;&nbsp;</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtEmail" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                        MaxLength="100"></asp:TextBox>
                                                                </td>


                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Mobile No &nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPhone" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                        MaxLength="10"></asp:TextBox>
                                                                </td>

                                                            </tr>


                                                            <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlBank" Width="220px" Height="23px"  runat="server" DataSource='<%#drpload("Bank") %>' 
                                                                        DataValueField="BankID" AutoPostBack="true" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static"
                                                                         OnSelectedIndexChanged="ddlBank_SelectedIndexChanged" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    
                                                                </td>

                                                                <td style="padding: 5px;"><span class="headFont">Branch Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server"  AppendDataBoundItems="true" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" autocomplete="off" >
                                                                                <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                            </asp:DropDownList> 
                                                                    <%--<asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server" DataValueField="BranchID" DataTextField="Branch" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                        <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtBranchID" ClientIDMode="Static" runat="server" Width="210px" Visible="false" CssClass="textbox"></asp:TextBox>--%>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">IFSC Code&nbsp;&nbsp;</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtIFSC" autocomplete="off" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">MICR No&nbsp;&nbsp;</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtMICR" autocomplete="off" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bank Account No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBankCode" autocomplete="off" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Active Flag&nbsp;&nbsp;</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlActiveFlag" autocomplete="off" Width="220px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                        <%--<asp:ListItem Text="(Select)" Selected="True" Value=""></asp:ListItem>--%>
                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>                                                            

                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Pan &nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPanNo" autocomplete="off" ClientIDMode="Static" onkeyup="validationPANonkeypress()" Text='<%# Eval("CompanyPAN") %>' runat="server" CssClass="textbox UpperCase"
                                                                        MaxLength="10"></asp:TextBox>
                                                                </td>

                                                            </tr>
                                                            <tr>

                                                                <td style="padding: 5px;"><span class="headFont">Vendor Name&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtVendorName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" OnTextChanged="txtVendorName_TextChanged" 
                                                                     onkeyup="CopyfromVendornametoCompanyname()"   MaxLength="400" Text='<%# Eval("VendorName") %>'></asp:TextBox>
                                                                </td>

                                                                 <td style="padding: 5px;">
                                                                    <span class="headFont">Company Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtCompanyName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Text='<%# Eval("CompanyName") %>'
                                                                        MaxLength="250"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Address &nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left" colspan="4">
                                                                    <asp:TextBox ID="txtAddress" autocomplete="off" ClientIDMode="Static" runat="server"  height="30px" Width="830px" TextMode="MultiLine" Text='<%# Eval("VendorAddress") %>' CssClass="textbox UpperCase"></asp:TextBox>

                                                                </td>

                                                            </tr>

                                                            
                                                            <tr>

                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Email &nbsp;&nbsp;</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtEmail" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyEmail") %>'
                                                                        MaxLength="100"></asp:TextBox>
                                                                </td>


                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Mobile No &nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPhone" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("CompanyMobile") %>' runat="server" CssClass="textbox"
                                                                        MaxLength="10"></asp:TextBox>
                                                                </td>

                                                            </tr>


                                                            <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                           <%-- <tr>

                                                                <td style="padding: 5px;"><span class="headFont">Vendor Name&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtVendorName" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                        MaxLength="400" Text='<%# Eval("VendorName") %>'></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Address &nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtAddress" ClientIDMode="Static" runat="server" Height="30px" TextMode="MultiLine" CssClass="textbox" Text='<%# Eval("VendorAddress") %>'></asp:TextBox>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td style="padding: 5px;"><span class="headFont">Company Name&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtCompanyName" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyName") %>'
                                                                        MaxLength="250"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Pan &nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPanNo" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyPAN") %>'
                                                                        MaxLength="10"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td style="padding: 5px;"><span class="headFont">Email &nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtEmail" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyEmail") %>'
                                                                        MaxLength="100"></asp:TextBox>
                                                                </td>

                                                                <td style="padding: 5px;"><span class="headFont">Mobile No &nbsp;&nbsp;</span></td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPhone" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CompanyMobile") %>'
                                                                        MaxLength="10"></asp:TextBox>
                                                                </td>

                                                            </tr>--%>


                                                            <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">: </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlBank" Width="220px" Height="23px" runat="server" DataSource='<%#drpload("Bank") %>' DataValueField="BankID" AutoPostBack="false" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>

                                                                <td style="padding: 5px;"><span class="headFont">Branch Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <%--<asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server" DataSource='<%#drpload("Branch") %>' DataValueField="BranchID" DataTextField="Branch" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                            </asp:DropDownList> --%>
                                                                    <asp:DropDownList ID="ddlBranch" Width="220px" Height="23px" runat="server" DataValueField="BranchID" DataTextField="Branch" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">IFSC Code&nbsp;&nbsp;</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtIFSC" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">MICR No&nbsp;&nbsp;</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtMICR" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Bank Account No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBankCode" autocomplete="off" ClientIDMode="Static" runat="server" Width="210px" Enabled="false" CssClass="textbox" Text='<%# Eval("AccountNo") %>'
                                                                        MaxLength="20"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Active Flag&nbsp;&nbsp;</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlActiveFlag" autocomplete="off" Width="220px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                        <asp:ListItem Text="(Select)" Selected="True" Value=""></asp:ListItem>
                                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </EditItemTemplate>

                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="require">*</span> indicates Mandatory Field
                                                                </td>
                                                                <td style="padding: 5px;">&nbsp;
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" Width="100px"
                                                                            CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' OnClick="cmdSave_Click" />

                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Refresh" Width="100px" CssClass="Btnclassname DefaultButton" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="overflow-y: scroll; height: auto; width: 100%">
                                                    <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                                                        AutoGenerateColumns="false" DataKeyNames="VendorID"
                                                        AllowPaging="False"  OnRowCommand="tbl_RowCommand"
                                                        CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
                                                        <AlternatingRowStyle BackColor="#FFFACD" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last"
                                                            Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <HeaderStyle />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                        runat="server" ID="btnEdit" CommandArgument='<%# Eval("VendorID") %>' />


                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderText="Delete">
                                                                <HeaderStyle  />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                        OnClientClick='return Delete();' CommandArgument='<%# Eval("VendorID") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>--%>
                                                            <asp:BoundField DataField="VendorID" Visible="false" ItemStyle-CssClass="labelCaption"
                                                                HeaderStyle-CssClass="TableHeader" HeaderText="VendorID" />

                                                            <asp:BoundField DataField="VendorName"
                                                                HeaderText="Vendor Name" />
                                                            <asp:BoundField DataField="CompanyName"
                                                                HeaderText="Company Name" />
                                                            <asp:BoundField DataField="CompanyPAN"
                                                                HeaderText="PAN" />
                                                            <asp:BoundField DataField="AccountNo"
                                                                HeaderText="ACNO" />
                                                            <asp:BoundField DataField="ActiveFlag"
                                                                HeaderText="Active" />
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>

