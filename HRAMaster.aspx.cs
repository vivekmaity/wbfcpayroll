﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Globalization;
public partial class HRAMaster : System.Web.UI.Page
{
    public static string hdnhraID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {

            DataTable dtstr = DBHandler.GetResult("Get_HRA");
            tbl.DataSource = dtstr;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_HRAByID", ID);
                dv.DataSource = dtResult;
                hdnhraID = dtResult.Rows[0]["HRAID"].ToString();
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlEmpType")).SelectedValue = dtResult.Rows[0]["EmpType"].ToString();
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_HRAByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                //TextBox txtHRACode = (TextBox)dv.FindControl("txtHRACode");
                DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
                TextBox txtHRA = (TextBox)dv.FindControl("txtHRA");
                TextBox txtMaxAmt = (TextBox)dv.FindControl("txtMaxAmt");
               // CheckBox chkActiveFlag = (CheckBox)dv.FindControl("chkActiveFlag");
                TextBox txtEffFrm = (TextBox)dv.FindControl("txtEffFrm");
                //TextBox txtEffTo = (TextBox)dv.FindControl("txtEffTo");
                //String Month = Convert.ToDateTime(txtEffFrm.Text).Month.ToString();
                //String Year = Convert.ToDateTime(txtEffFrm.Text).Year.ToString();
                string monthYear = Convert.ToDateTime(txtEffFrm.Text).ToString("MM/yyyy").Replace('-','/');

                DataTable dt = DBHandler.GetResult("Insert_HRA", DBNull.Value, ddlEmpType.SelectedValue, txtHRA.Text, Convert.ToDouble(txtMaxAmt.Text), Convert.ToDateTime(txtEffFrm.Text), DBNull.Value, Session[SiteConstants.SSN_INT_USER_ID], monthYear.ToString());
                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                if (dt.Rows.Count > 0)
                {
                    string Result = dt.Rows[0]["MSG"].ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('"+Result+"')</script>");
                }
                //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {
                //TextBox txtHRACode = (TextBox)dv.FindControl("txtHRACode");
                DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
                TextBox txtHRA = (TextBox)dv.FindControl("txtHRA");
                TextBox txtMaxAmount = (TextBox)dv.FindControl("txtMaxAmt");
               // CheckBox chkActiveFlag = (CheckBox)dv.FindControl("chkActiveFlag");
                TextBox txtEffectiveFrom = (TextBox)dv.FindControl("txtEffFrm");
                //TextBox txtEffectiveTo = (TextBox)dv.FindControl("txtEffTo");
                DateTime DtEffFrom = DateTime.ParseExact(txtEffectiveFrom.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //DateTime DtEffTo = DateTime.ParseExact(txtEffectiveTo.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                string ID = hdnhraID;

                DBHandler.Execute("Update_HRA", ID, DBNull.Value, ddlEmpType.SelectedValue, txtHRA.Text,
                    Convert.ToDouble(txtMaxAmount.Text), Convert.ToDateTime(txtEffectiveFrom.Text), DBNull.Value, Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tbl.PageIndex = e.NewPageIndex;
        PopulateGrid();
    }
}