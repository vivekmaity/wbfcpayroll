﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Management;
using System.Runtime.InteropServices;           

public partial class AdminCoOpPFLoanDeduReportView : System.Web.UI.Page
{
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
    static int ReportNameVal = 0;
    static int SalMonthId = 0;
    static string EmpType = "";
    static string EmpStatus = "";             
    static string SalaryFinYear = "";
    static int DeduEdid = 0;
    static int LoanEdid = 0;
    static int UserId = 0;
    static string ExportFileName = "";
    String ConnString = "";
    static string EmpStatus1 = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       GetPrinterList();
       SetPaperSize();              
       BindReport(crystalReport);
    }

    protected void ExportPDF(object sender, EventArgs e)
    {
        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);
      
        ExportFormatType formatType = ExportFormatType.NoFormat;
        switch (rbFormat.SelectedItem.Value)
        {
            case "Excel":
                formatType = ExportFormatType.Excel;
                break;
            case "PDF":
                formatType = ExportFormatType.PortableDocFormat;
                break;          
        }
        crystalReport.ExportToHttpResponse(formatType, Response, true, ExportFileName);
        Response.End();
    }

    private void BindReport(ReportDocument crystalReport)
    {          
        ConnString = DataAccess.DBHandler.GetConnectionString();
        SalaryFinYear = Convert.ToString(HttpContext.Current.Request.QueryString["SalaryFinYear"]);
        SalMonthId = Convert.ToInt32(HttpContext.Current.Request.QueryString["SalMonthId"]);        
        EmpType = Convert.ToString(HttpContext.Current.Request.QueryString["EmpType"]);
        EmpStatus = Convert.ToString(HttpContext.Current.Request.QueryString["EmpStatus"]);
        DeduEdid = Convert.ToInt32(HttpContext.Current.Request.QueryString["DeduEdid"]);
        LoanEdid = Convert.ToInt32(HttpContext.Current.Request.QueryString["LoanEdid"]);
        UserId = Convert.ToInt32(HttpContext.Current.Request.QueryString["UserId"]);
        ReportNameVal = Convert.ToInt32(HttpContext.Current.Request.QueryString["ReportID"]);
        
        if (EmpStatus.Length > 1)
        {
            EmpStatus1 = "[" + "'Y'" + "," + "'S'" + "]";
            EmpStatus = EmpStatus1;
        }
        else
        {
            EmpStatus = "[" + "'" + EmpStatus + "'" + "]";
        }
        
        if (ReportNameVal == 1)
        {
            crystalReport.Load(Server.MapPath("~/Reports/COOP_kmda_all.rpt"));
            crystalReport.RecordSelectionFormula = "{MST_Employee.Status} IN " + EmpStatus + "";
            //crystalReport.RecordSelectionFormula = "{MST_Sector.SectorID}=" + SectorID + " and {MST_SalaryMonth.SalMonthID}=" + SalMonthId + "  and {MST_Employee.Status}='Y'";
            ExportFileName = "WBFC COOP ALL";
        }
        if (ReportNameVal == 2 || ReportNameVal == 3)
        {
            crystalReport.Load(Server.MapPath("~/Reports/gpf_cpf_ALL.rpt"));
            crystalReport.RecordSelectionFormula = "{MST_Employee.Status} IN " + EmpStatus + "";
            //crystalReport.RecordSelectionFormula = "{MST_Sector.SectorID}=" + SectorID + " and {MST_SalaryMonth.SalMonthID}=" + SalMonthId + "  and {MST_Employee.Status}='Y'";
            ExportFileName = "WBFC GPF And CPF ALL";
        }
        crystalReport.Refresh();
        crystalReport.DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
        crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
        crystalReport.SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
        crystalReport.Refresh();
        crystalReport.SetParameterValue("@SalaryFinYear", SalaryFinYear);
        crystalReport.SetParameterValue("@SalMonth", SalMonthId);
        crystalReport.SetParameterValue("@EmpType", EmpType);
        crystalReport.SetParameterValue("@Status", EmpStatus);
        crystalReport.SetParameterValue("@Dedu_EDID", DeduEdid);
        crystalReport.SetParameterValue("@Loan_EDID", LoanEdid);
        crystalReport.SetParameterValue("@UserID", UserId); 
        CrystalReportViewer1.DisplayToolbar = true;
        CrystalReportViewer1.Zoom(150);  // Page Width
        CrystalReportViewer1.Visible = true;
        CrystalReportViewer1.ReportSource = crystalReport;
    }

    protected void SetPaperSize()
    {
        ddlPaperSize.Items.Clear();
        ddlPaperSize.Items.Add("Page - A4");
    }
                
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Func_Print()
    {
        var StrMsg = "Success";
        ReportDocument crystalReport = new ReportDocument();
        crystalReport.Refresh();
        crystalReport.PrintToPrinter(1, true, 1, crystalReport.FormatEngine.GetLastPageNumber(new CrystalDecisions.Shared.ReportPageRequestContext()));
        return StrMsg.ToJSON();
    }

    public void OnConfirmprint(object sender, EventArgs e)
    {
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {
            ReportDocument crystalReport = new ReportDocument();
            BindReport(crystalReport);

            ExportFormatType formatType = ExportFormatType.NoFormat;
            switch (rbFormat.SelectedItem.Value)
            {
                case "Excel":
                    formatType = ExportFormatType.Excel;
                    break;
                case "PDF":
                    formatType = ExportFormatType.PortableDocFormat;
                    break;
            }
            crystalReport.ExportToHttpResponse(formatType, Response, false, ExportFileName);
            Response.End();
        }
    }
    
    private bool GetPrinterList()          
    {
        bool retVal = false;
        PrintDocument printtdoc = new PrintDocument();
        string strDefaultPrinterName = printtdoc.PrinterSettings.PrinterName;
        foreach (String strPrinter in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
        {
            if (strPrinter.CompareTo(strDefaultPrinterName) == 0)
            {
                retVal = true;
            }
        }

        return retVal;
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        this.crystalReport.Close();
        this.crystalReport.Dispose();
    }

}