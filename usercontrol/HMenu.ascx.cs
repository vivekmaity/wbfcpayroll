﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;

public partial class usercontrol_HMenu : System.Web.UI.UserControl
{
    DataTable dtMenu;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
            if (Session[SiteConstants.SSN_INT_USER_ID] == null)
            {
                Response.Redirect("log.aspx", false);
                return;
            }
            if (!IsPostBack)
            {
                populateMenu();
            }
        }
        catch (Exception ex)
        {
            Page.RegisterStartupScript("Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void populateMenu()
    {
        try
        {
            //dtMenu = DBHandler.GetResult("Get_Menu", Session[SiteConstants.SSN_INT_USER_ID]);
            dtMenu = DBHandler.GetResult("Get_UserMenu", Session[SiteConstants.SSN_INT_USER_ID],1);
            DataTable objt = new DataTable();
            objt = GetData(0);
            string mainNode = "<ul>";

            foreach (DataRow dr in objt.Rows)
            {
                string node = "";

                node += "<li><a href='" + (dr["PageName"].ToString() == "" ? "#" : dr["PageName"].ToString()) + "'>" + dr["MenuName"].ToString() + "</a>";

                node += AddNodes(dr["MenuCode"].ToString());
                node += "</li>";
                mainNode += node;
            }
            mainNode += "</ul>";
            smoothmenu.InnerHtml = mainNode;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string AddNodes(string MenuID)
    {
        string otherNode = "";
        try
        {
            DataTable dt = new DataTable();
            dt = GetData(Convert.ToInt32(MenuID));

            if (dt.Rows.Count > 0)
            {
                string nNode = "<ul>";
                foreach (DataRow row in dt.Rows)
                {
                    nNode += "<li><a href='" + (row["PageName"].ToString() == "" ? "#" : row["PageName"].ToString()) + "'>" + row["MenuName"].ToString() + "</a>";
                    nNode += AddNodes(row["MenuCode"].ToString());
                    nNode += "</li>";
                }
                nNode += "</ul>";
                otherNode = nNode;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return otherNode;
    }

    protected DataTable GetData(int intPID)
    {
        DataTable objT = new DataTable();
        try
        {
            objT.Columns.Add("MenuCode");
            objT.Columns.Add("MenuName");
            objT.Columns.Add("ParentMenuCode");
            objT.Columns.Add("PageName");

            foreach (DataRow dr in dtMenu.Rows)
            {
                if (dr[2].ToString() != intPID.ToString())
                {
                    continue;
                }
                DataRow r = objT.NewRow();
                r[0] = dr[0];
                r[1] = dr[1];
                r[2] = dr[2];
                r[3] = dr[3];

                objT.Rows.Add(r);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return objT;
    }
}