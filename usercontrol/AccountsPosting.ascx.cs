﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class usercontrol_AccountsPosting : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dtToday = DateTime.Now.Date;
        txtPaymentOn.Text = dtToday.ToString("dd/MM/yyyy");

        PopulateInstrumentType();
    }

    protected void PopulateAccountDescription()
    {
        try
        {
            string SectorID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();

            DataTable dt = DBHandler.GetResult("Get_AccountDescription", SectorID);
            if (dt.Rows.Count > 0)
            {
                ddlAccountDescription.DataSource = dt;
                ddlAccountDescription.DataTextField = "AccountDescription";
                ddlAccountDescription.DataValueField = "Accountcode";
                ddlAccountDescription.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateInstrumentType()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Instrument");
            if (dt.Rows.Count > 0)
            {
                ddlModeofPayment.DataSource = dt;
                ddlModeofPayment.DataTextField = "Instrument";
                ddlModeofPayment.DataValueField = "InstrumentTypeID";
                ddlModeofPayment.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Bind_AccountDescription(string SectorID)
    {

        DataSet ds = DBHandler.GetResults("Get_AccountDescription", SectorID);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<AccountDescriptions> listAccountDescription = new List<AccountDescriptions>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AccountDescriptions objst = new AccountDescriptions();

                objst.Accountcode = dt.Rows[i]["Accountcode"].ToString();
                objst.AccountDescription = dt.Rows[i]["AccountDescription"].ToString();
                listAccountDescription.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listAccountDescription);
    }
    public class AccountDescriptions
    {
        public string Accountcode { get; set; }
        public string AccountDescription { get; set; }
    }
}