﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageDialog.ascx.cs" Inherits="usercontrol_MessageDialog" %>

<style type="text/css">
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width:100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: 0.15;
   filter: alpha(opacity=15);
   -moz-opacity: 0.15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: block;
   position: fixed;
   width: 150px;
   height: 50px;
   top: 50%;
   left: 35%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px #336699;
   background-color: #deedf7;
   padding: 4px;
   color: Black;
   font-weight:bold;
}
.web_dialog_title a
{
   color: Black;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
</style>

        <div id="overlay2" class="web_dialog_overlay"></div>
        <div id="dialog2" class="web_dialog">
            <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
                <tr>
                    <td colspan="2" class="web_dialog_title">
                        Information
                    </td>
                    <td class="web_dialog_title align_right">
                        <a href="#" id="btnmsgClose">Close</a>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; border: 0px; padding: 5px;" cellpadding="3" cellspacing="3">
            <tr>
               <td>Record has been Saved Successfully.</td>
            </tr>
               
            </table>
        </div>