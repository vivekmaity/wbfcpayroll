﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountsPosting.ascx.cs" Inherits="usercontrol_AccountsPosting" %>

<link href="css/pensionstyle.css" rel="stylesheet" />

<script>
    $(document).ready(function () {
        $(".DefaultButton").click(function (event) {
            event.preventDefault();
        });
    });
</script>

<table align="center" width="100%">
    <tr>
        <td>
            <%--<div class="content-overlay" id='overlay' style="z-index: 49;">--%>
                <div class="wrapper-outer">
                    <div class="wrapper-inner">
                        <div class="loadContent" style="background-color: lightyellow;">
                            <div id="Close" style="background-color: aqua; text-align: right;">
                                <a href="javascript:void(0);" id="A1" style="color: red;" >
                                    <img src="images/cross.png" alt="Close" title="Close" style="Height: 18px; Width: 20px;" /></a>
                            </div>
                           

                            <div id="Div1" runat="server">
                                <table width="100%" style="border: solid 2px lightblue; height: auto;">
                                    <tr>
                                        <td style="padding: 3px;">
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td>
                                                        <div id="tabs-1" style="float: left; border: solid 2px lightblue; width: 100%; height: auto;">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td colspan="2" style="padding: 3.5px; background: #deedf7;" class="style1">
                                                                        <span class="headFont" style="font-weight: bold;"><font size="3px">Payment Details:</font></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 2px; width: 330px;" align="right">
                                                                        <span class="headFont">Account Description :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                    </td>
                                                                    <td style="padding: 1px;" align="left">
                                                                        <asp:DropDownList ID="ddlAccountDescription" Width="201px" Height="22px" runat="server" DataValueField="InstrumentTypeID"
                                                                            DataTextField="Instrument" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                            <asp:ListItem Text="(Select Account)" Selected="True" Value=""></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 2px; width: 330px;" align="right">
                                                                        <span class="headFont">Mode of Payment :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                    </td>
                                                                    <td style="padding: 1px;" align="left">
                                                                        <asp:DropDownList ID="ddlModeofPayment" Width="201px" Height="22px" runat="server" DataValueField="InstrumentTypeID"
                                                                            DataTextField="Instrument" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                            <asp:ListItem Text="(Select Payment Mode)" Selected="True" Value=""></asp:ListItem>
<%--                                                                            <asp:ListItem Text="Bank Advice" Value="B"></asp:ListItem>
                                                                            <asp:ListItem Text="Cheque" Value="C"></asp:ListItem>--%>

                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 2px; width: 330px;" align="right">
                                                                        <span class="headFont">Bank Advice No. / Cheque No. :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                    </td>
                                                                    <td style="padding: 2px;" align="left">
                                                                        <asp:TextBox ID="txtBankorChequeNo" ClientIDMode="Static" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 2px; width: 330px;" align="right">
                                                                        <span class="headFont">Bank Advice Date/Cheque Date. :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                    </td>
                                                                    <td style="padding: 2px;" align="left">
                                                                        <asp:TextBox ID="txtBankorChequeDate" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                            Style="margin-left: 0px" Width="150px"></asp:TextBox>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 2px; width: 330px;" align="right">
                                                                        <span class="headFont">Payment Date :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                    </td>
                                                                    <td style="padding: 2px;" align="left">
                                                                        <asp:TextBox ID="txtPaymentDate" ClientIDMode="Static" runat="server" CssClass="textbox dpDate"
                                                                            Style="margin-left: 0px" Width="150px" Enabled="false"></asp:TextBox>

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding: 2px; width: 330px;" align="right">
                                                                        <span class="headFont">Payment On :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                    </td>
                                                                    <td style="padding: 2px;" align="left">
                                                                        <asp:TextBox ID="txtPaymentOn" ClientIDMode="Static" runat="server" CssClass="textbox dpDate"
                                                                            Style="margin-left: 0px" Width="150px" Enabled="false"></asp:TextBox>

                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td></td>
                                                                    <td style="padding: 2px;" align="left">
                                                                        <asp:Button ID="btnok" runat="server" Text="Save" CommandName="OK" ClientIDMode="Static"
                                                                            CssClass="Btnclassname DefaultButton" Width="50px" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>


                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
          <%--  </div>--%>
        </td>
    </tr>
</table>
