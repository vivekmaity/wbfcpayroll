﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmployeeDetails.ascx.cs" Inherits="usercontrol_EmployeeDetails" %>

<style type="text/css">
    .style1
    {
        width: 194px;
    }
</style>

<table align="center" width="100%">
    <tr>
        <td colspan="2" style="padding: 3.5px; background: #deedf7;" class="style1">
            <span class="headFont" style="font-weight: bold;"><font size="3px">Employee Details:</font></span>
        </td>
    </tr>
    <tr>
        <td style="padding: 2px;" align="left" class="style1"><span class="headFont">Employee Name :&nbsp;&nbsp;<%--</span><span class="require">*</span>--%></td>
        <td style="padding: 2px;" align="left">
            <asp:TextBox ID="txtEmpName" ClientIDMode="Static" runat="server" CssClass="textbox" Width="300px"
                MaxLength="6"></asp:TextBox>
        </td>
        </tr>
        <tr>
        <td style="padding: 2px;" class="style1"><span class="headFont">Designation :&nbsp;&nbsp;</span><span class="require">*</span></td>
        <td style="padding: 2px;" align="left">
            <asp:TextBox ID="txtDesignation" ClientIDMode="Static" runat="server" CssClass="textbox" 
                MaxLength="6"></asp:TextBox>
        </td>
    </tr>
        <tr>
        <td style="padding: 2px;" class="style1"><span class="headFont">Pay Scale :&nbsp;&nbsp;<%--</span><span class="require">*</span>--%></td>
        <td style="padding: 2px;" align="left">
            <asp:TextBox ID="txtPayScale" ClientIDMode="Static" runat="server" CssClass="textbox" 
                MaxLength="6" style="margin-left: 0px"></asp:TextBox>
        </td>
        </tr>
        <tr>
        <td style="padding: 2px;" class="style1"><span class="headFont">Net Pay as On :&nbsp;&nbsp;</span><span class="require">*</span>&nbsp;&nbsp;
        <span><asp:Label ID="lblNetPay" runat="server" Text=""></asp:Label></span></td>
        <td style="padding: 2px;" align="left"><asp:TextBox ID="txtNetPay" ClientIDMode="Static" runat="server" CssClass="textbox" 
                MaxLength="6" Width="80px"></asp:TextBox>
        </td>
    </tr>
        <tr>
        <td style="padding: 2px;" class="style1"><span class="headFont">Sector :&nbsp;&nbsp;<%--</span><span class="require">*</span>--%></td>
        <td style="padding: 2px;" align="left">
            <asp:TextBox ID="txtSector" ClientIDMode="Static" runat="server" CssClass="textbox" 
                MaxLength="6"></asp:TextBox>
        </td>
        </tr>
        <tr>
        <td style="padding: 2px;" class="style1"><span class="headFont">Location :&nbsp;&nbsp;</span><span class="require">*</span></td>
        <td  style="padding: 3px;" align="left">
            <asp:TextBox ID="txtLocation" ClientIDMode="Static" runat="server" CssClass="textbox" 
                MaxLength="6"></asp:TextBox>
        </td>
    </tr>
</table>
