﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;            
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;             
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;               

public partial class PaySlipReport : System.Web.UI.Page
{
    public string str1 = "";

    public static string hdnsalmo = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string GetSalMonth(int sectorid, string finYear)
    {
        //DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataSet ds = DBHandler.GetResults("Load_SalMonthForReport", finYear, sectorid);
        DataTable dt = ds.Tables[0];
        //DataTable dt1 = ds1.Tables[0];
        List<object> objdata = new List<object>();
        //List<CenterbySector> listCenter = new List<CenterbySector>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var obj = new
                {
                    Salmonthid = dt.Rows[i]["Salmonthid"],
                    SalaryMonth = dt.Rows[i]["SalaryMonth"]
                };
                objdata.Add(obj);
            }
        }
        
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(objdata);

    }
    public class CenterbySector
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
       public string PayMonths { get; set; }
       public int PayMonthsID { get; set; }
    }

    protected void PopulateCenter(int SecID, string SalFinYear)
    {
        try        
        {

            string SecId = "";
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
            DataTable dtSector = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
            SecId = dtSector.Rows[0]["SectorId"].ToString();
            if (dtSector.Rows.Count == 1)
            {
                DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth",Convert.ToInt32(SecId), SalFinYear);
                if (dtSalMonth.Rows.Count > 0)
                {
                    txtPayMonth.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
                    hdnsalmo = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString(); 
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static void Report_Paravalue(int SectorID, int SalMonth)
    {
        //DataTable dtcr = DBHandler.GetResult("SalaryProcess", SectorID, SalMonth);

    }

    [WebMethod]
    public static string FinYearForReport(int sectorid)
    {
        DataSet ds = DBHandler.GetResults("Load_FinYearForReport", sectorid);
        DataTable dt = ds.Tables[0];
        List<object> objdata = new List<object>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var obj = new
                {
                    SalaryFinYear = dt.Rows[i]["SalaryFinYear"],
                    NR = dt.Rows[i]["NR"]
                };
                objdata.Add(obj);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(objdata);

    }

    [WebMethod]
    public static string get_allrpt(int parentID, string ReportType)
    {
        DataSet ds = DBHandler.GetResults("Load_ChildPayrollReport", parentID, ReportType);
        DataTable dt = ds.Tables[0];
        List<object> objdata = new List<object>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var obj = new
                {
                    ReportID = dt.Rows[i]["ReportID"],
                    ReportName = dt.Rows[i]["ReportName"]
                };
                objdata.Add(obj);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(objdata);

    }

    [WebMethod]
    public static string get_allLocation(int SecID)
    {
        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataTable dt = ds.Tables[0];
        List<object> objdata = new List<object>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var obj = new
                {
                    CenterID = dt.Rows[i]["CenterID"],
                    CenterName = dt.Rows[i]["CenterName"]
                };
                objdata.Add(obj);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(objdata);

    }

    [WebMethod]
    public static string get_lodeDynamicRpt(int SalmonthID, string salmonth, string salaryFinYear, int ChildreportID, string reportHisCur, int sectorid, int CenterID)
    {
        DataSet ds = DBHandler.GetResults("Load_reportDynamic", SalmonthID, salmonth, salaryFinYear, sectorid, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]), ChildreportID, CenterID, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, reportHisCur);
        //DataTable dt = ds.Tables[0];
        List<object> objdata = new List<object>();
        //objdata.Add(ds);
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(objdata);

    }
}