﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ChangeSector.aspx.cs" Inherits="ChangeSector" %>
<%@ Register TagPrefix="art" TagName="EmployeeDetails" Src="usercontrol/EmployeeDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>

<script type="text/javascript">
    var ParameterTypes = "Sector";
   
    $(document).ready(function () {
        $('#tabs-4').hide();
        $("#rdChangeSector").prop("checked", true);
        /*=======================================================================================================================*/
        document.getElementById("txtEmpName").disabled = true;
        document.getElementById("txtDesignation").disabled = true;
        document.getElementById("txtPayScale").disabled = true;
        document.getElementById("txtNetPay").disabled = true;
        document.getElementById("txtSector").disabled = true;
        document.getElementById("txtLocation").disabled = true;
        /*=======================================================================================================================*/
        $('#txtTransDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow'
        });
        $("#btnTransDate").click(function () {
            $('#txtTransDate').datepicker("show");
            return false;
        });
        $('#txtReleDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow'
        });
        $("#btnReleDate").click(function () {
            $('#txtReleDate').datepicker("show");
            return false;
        });
        $('#txtMemoDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow'
        });
        $("#btnMemoDate").click(function () {
            $('#txtMemoDate').datepicker("show");
            return false;
        });


        $('#txtTranDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow'
        });
        $("#btnTranDate").click(function () {
            $('#txtTranDate').datepicker("show");
            return false;
        });

        $('#txtRelDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow'
        });
        $("#btnRelDate").click(function () {
            $('#txtRelDate').datepicker("show");
            return false;
        });

        $('#txtMemDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow'
        });
        $("#btnMemDate").click(function () {
            $('#txtMemDate').datepicker("show");
            return false;
        });

        $('#txtRentUptoDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow'
        });
        $("#btnRentUptoDate").click(function () {
            $('#txtRentUptoDate').datepicker("show");
            return false;
        });
        /*=======================================================================================================================*/
        $(function () {
            
            $("[id$=txtTransDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $(function () {
            $("[id$=txtReleDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $(function () {
            $("[id$=txtMemoDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });

        $(function () {
            $("[id$=txtTranDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $(function () {
            $("[id$=txtRelDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $(function () {
            $("[id$=txtMemDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        /*=======================================================================================================================*/
    });
$(document).ready(function () {
    $("#txtEmpNoSearch").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
    (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                event.preventDefault();
                return false;

            }
        }
    });
});
    //This is for 'Location' Binding on the Base of 'Sector'
    $(document).ready(function () {
        $("#ddlSec").change(function () {
            var s = $('#ddlSec').val();
            if (s != 0) {
                if ($("#ddlSec").val() != "Please select") {
                    var E = "{SecID: " + $('#ddlSec').val() + "}";
                    //alert(E);
                    var options = {};
                    options.url = "ChangeSector.aspx/GetCenter";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listCenter) {
                        var t = jQuery.parseJSON(listCenter.d);
                        var a = t.length;
                        $("#ddlCentr").empty();
                        if (a >= 0) {
                            $("#ddlCentr").append("<option value='0'>(Select Location)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlCentr").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        }

                        $("#ddlCentr").prop("disabled", false);
                    };
                    options.error = function () { alert("Error in retrieving Location!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlCentr").empty();
                    $("#ddlCentr").prop("disabled", true);
                }
            }
            else {
                $("#ddlCentr").empty();
                $("#ddlCentr").append("<option value=''>(Select Location)</option>")
            }
        });
    });
    $(document).ready(function () {
        $("#btnSave").click(function (event) {
            SaveChangeSector();
        });
        $("#btnSave1").click(function (event) {
            SaveChangeSector();
        });
    });

    function SaveChangeSector()
    {
        event.preventDefault();
        var E = "";
        //For Sector
        var s = $('#ddlSec').val();
        var c = $('#ddlCentr').val();
        var TransOrderNo = $('#txtTransOrderNo').val();
        var TransDate = $('#txtTransDate').val();
        var ReleOrderNo = $('#txtReleOrderNo').val();
        var ReleDate = $('#txtReleDate').val();
        var MemoNo = $('#txtMemoNo').val();
        var MemoDate = $('#txtMemoDate').val();
        var ProcHeadOffice = $('#txtProcHeadOffice').val();
        var Remarks = $('#txtRemarks').val();

        var EmpNo = $('#txtEmpNoSearch').val();

        //For Office
        var From = $('#txtFrom').val();
        var To = $('#txtTo').val();
        var TranOrderNo = $('#txtTranOrderNo').val();
        var TranDate = $('#txtTranDate').val();
        var RelOrderNo = $('#txtRelOrderNo').val();
        var RelDate = $('#txtRelDate').val();
        var MemNo = $('#txtMemNo').val();
        var MemDate = $('#txtMemDate').val();
        var ProHeadOffice = $('#txtProccHeadOffice').val();
        var Remark = $('#txtRemark').val();
        var ProceedingPost = $('#txtProceedingPost').val();
        var SancLeave = $('#txtSancLeave').val();
        var NoofDays = $('#txtNoofDays').val();
        //var HouseRent = $('#txtRentUpto').val();
        var HouseRentUpto = $('#txtRentUptoDate').val();

      
        if (ParameterTypes == "Sector") {
            if (s == "") {
                alert("Please Select Sector First !");
                $('#ddlSec').focus(); return false;
            }
            if (c == "") {
                alert("Please Select Center First !");
                $('#ddlCentr').focus(); return false;
            }
            if (TransOrderNo == "") {
                alert("Please Enter Transfer Order No. !");
                $('#txtTransOrderNo').focus(); return false;
            }
            if (TransDate == "") {
                alert("Please Enter Transfer Date !");
                $('#txtTransDate').focus(); return false;
            }
            if (ReleOrderNo == "") {
                alert("Please Enter Release Order No. !");
                $('#txtReleOrderNo').focus(); return false;
            }
            if (ReleDate == "") {
                alert("Please Enter Release Date !");
                $('#txtReleDate').focus(); return false;
            }
            if (MemoNo == "") {
                alert("Please Enter Memo No. !");
                $('#txtMemoNo').focus(); return false;
            }
            if (MemoDate == "") {
                alert("Please Enter Memo Date !");
                $('#txtMemoDate').focus(); return false;
            }
            if (ProcHeadOffice == "") {
                alert("Please Enter ProcHeadOffice !");
                $('#txtProcHeadOffice').focus(); return false;
            }
            if (Remarks == "") {
                alert("Please Enter Remarks !");
                $('#txtRemarks').focus(); return false;
            }

            var TransferType = 'S'; var ProceedingPost = ""; var SancLeave = ""; var NoofDays = ""; var HouseRentUpto = ""; //var HouseRent = "";
            var From = ""; var To = "";
            E = "{SecId: '" + s + "', CenId: '" + c + "', EmpNo: '" + EmpNo + "', TransOrderNo: '" + TransOrderNo + "', TransDate: '" + TransDate + "', ReleOrderNo: '" + ReleOrderNo + "', ReleDate: '" + ReleDate + "', MemoNo: '" + MemoNo + "', MemoDate: '" + MemoDate + "', ProcHeadOffice: '" + ProcHeadOffice + "', Remarks: '" + Remarks + "', From:'" + From + "', To:'" + To + "', ProceedingPost: '" + ProceedingPost + "', SancLeave: '" + SancLeave + "', NoofDays: '" + NoofDays + "',  HouseRentUpto: '" + HouseRentUpto + "', ParameterTypes: '" + TransferType + "'}";
         
        }
        else {
            if (From == "") {
                alert("Please Enter From Office !");
                $('#txtFrom').focus(); return false;
            }
            if (To == "") {
                alert("Please Enter To Office !");
                $('#txtTo').focus(); return false;
            }
            if (TranOrderNo == "") {
                alert("Please Enter Transfer Order No. !");
                $('#txtTranOrderNo').focus(); return false;
            }
            if (TranDate == "") {
                alert("Please Enter Transfer Date !");
                $('#txtTranDate').focus(); return false;
            }
            if (RelOrderNo == "") {
                alert("Please Enter Release Order No. !");
                $('#txtRelOrderNo').focus(); return false;
            }
            if (RelDate == "") {
                alert("Please Enter Release Date !");
                $('#txtRelDate').focus(); return false;
            }
            if (MemNo == "") {
                alert("Please Enter Memo No. !");
                $('#txtMemNo').focus(); return false;
            }
            if (MemDate == "") {
                alert("Please Enter Memo Date !");
                $('#txtMemDate').focus(); return false;
            }
            if (ProHeadOffice == "") {
                alert("Please Enter ProcHeadOffice !");
                $('#txtProccHeadOffice').focus(); return false;
            }
            if (Remark == "") {
                alert("Please Enter Remarks !");
                $('#txtRemark').focus(); return false;
            }
            if (ProceedingPost == "") {
                alert("Please Enter Proceeding Post !");
                $('#txtProceedingPost').focus(); return false;
            }
            if (SancLeave == "") {
                alert("Please Enter Sanction Leave !");
                $('#txtSancLeave').focus(); return false;
            }
            if (NoofDays == "") {
                alert("Please Enter No of Days !");
                $('#txtNoofDays').focus(); return false;
            }
            //if (HouseRent == "") {
            //    alert("Please Enter is House Rent ?");
            //    $('#txtRentUpto').focus(); return false;
            //}
            if (HouseRentUpto == "") {
                alert("Please Enter House Rent Upto Date ?");
                $('#txtRentUptoDate').focus(); return false;
            }
            var TransferType = 'O'; var SectorID = ""; var CenterID = "";
            E = "{SecId: '" + SectorID + "', CenId: '" + CenterID + "', EmpNo: '" + EmpNo + "', TransOrderNo: '" + TranOrderNo + "', TransDate: '" + TranDate + "', ReleOrderNo: '" + RelOrderNo + "', ReleDate: '" + RelDate + "', MemoNo: '" + MemNo + "', MemoDate: '" + MemDate + "', ProcHeadOffice: '" + ProHeadOffice + "', Remarks: '" + Remark + "', From:'" + From + "', To:'" + To + "', ProceedingPost: '" + ProceedingPost + "', SancLeave: '" + SancLeave + "', NoofDays: '" + NoofDays + "',  HouseRentUpto: '" + HouseRentUpto + "', ParameterTypes: '" + TransferType + "'}";
           
        }

        //alert(E);
        $.ajax({
            type: "POST",
            url: 'ChangeSector.aspx/SaveSector',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //var data = jQuery.parseJSON(D.d); alert(JSON.stringify(data));
                var data = JSON.parse(D.d); ///alert(data);

                if (data == "sucess") {
                    alert("Employee Sector is Changed Successfully."); $("#txtEmpNoSearch").focus();
                    window.location.href = "ChangeSector.aspx";
                    return;
                }
                else if (data == "Transfered") {
                    alert("This Employee is already Transfered."); $("#txtEmpNoSearch").focus();
                    //window.location.href = "ChangeSector.aspx";
                    return;
                }
                else if (data == "Generated") {
                    alert("Sorry ! You can not Transfer this Employee Because\nBank slip for both Sector has been already Generated.");
                    $("#txtEmpNoSearch").focus();
                    return;
                }
                else {
                    alert("Sorry ! You can not Transfer this Employee Because\nBank slip of   " + data + "  has been already Generated.");
                }
            },
            error: function (response) {
                // alert('h');
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }

    function beforeSave() {
            $("#form1").validate();
            $("#ddlSec").rules("add", { required: true, messages: { required: "Please enter" } });
            $("#ddlDept").rules("add", { required: true, messages: { required: "Please select"} });
            return $("#form1").validate();
        }
    function beforeSave_Search() {
            $("#form1").validate();
            $("#txtEmpNoSearch").rules("add", { required: true, messages: { required: "Please Enter Employee No." } });
            return $("#form1").validate();
    }

    //This is for RadioButton checked or unchecked
    $(document).ready(function () {
        $('#rdChangeSector').click(function () {
            if (document.getElementById('rdChangeSector').checked) {
                ParameterTypes = "Sector";
                $('#tabs-4').hide();
                $('#tabs-2').show();
            }
        });

        $('#rdChangeOffice').click(function () {
            if (document.getElementById('rdChangeOffice').checked) {
                ParameterTypes = "Office";
                $('#tabs-2').hide();
                $('#tabs-4').show();
            }
        });
    });

</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
<div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Sector_Change_LPC_Master</h2>
                    </section>

                    <table width="100%" style="margin-bottom: 10px;">

                        <tr >
                            <td style="padding: 5px;"><span class="headFont">Employee No.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                            <td style="padding: 5px;">:</td>
                            <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtEmpNoSearch" ClientIDMode="Static" runat="server" MaxLength="6"
                                        CssClass="textbox" Width="100px"></asp:TextBox>
                            </td>
                            <td style="padding: 5px;">
                                <div style="float: left; margin-left: 200px;">
                                    <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="Search" OnClick="cmdSearch_Click"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick="beforeSave_Search();" />
                                  

                                    <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" OnClick="cmdRefresh_Click"
                                        Width="100px" CssClass="Btnclassname"  OnClientClick='javascript: return unvalidate();' />
                                </div>
                            </td>
                             <td style="padding: 5px;">
                                <div style="float: left; margin-left: 200px;">
                                    <asp:RadioButton ID="rdChangeSector" Checked="true" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="Change Sector" />&nbsp;&nbsp;&nbsp;&nbsp;
                                     <asp:RadioButton ID="rdChangeOffice" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="Change Office" Checked="false" />
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <td align="center" colspan="6">
                                <div align="center">
                                </div>
                            </td>
                        </tr>

                    </table>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <div id="tabs-1" style="float: left; border: solid 2px lightblue; width: 50%; height:207px">
                                                    <art:EmployeeDetails ID="empDetails" runat="server" />
                                                </div>
                                                <div id="tabs-2" style="float: right; border: solid 2px lightblue; width: 49%;">
                                                    <table align="center" width="100%">
                                                        <tr>
                                                            <td colspan="4" style="padding: 3.5px; background: #deedf7;" class="style1">
                                                                <span class="headFont" style="font-weight: bold;"><font size="3px">Change Sector:</font></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;" align="left" class="style1">
                                                                <span class="headFont">Sector :&nbsp;&nbsp;<%--</span><span class="require">*</span>--%>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                           <asp:DropDownList ID="ddlSec" Width="200px" Height="22px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off"
                                                                           DataTextField="SectorName" DataValueField="SectorID">
                                                                                <asp:ListItem Text="(Select Sector)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                            </td>
                                                      
                                                            <td style="padding: 2px;" class="style1" align="left">
                                                                <span class="headFont">Location :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                           <asp:DropDownList ID="ddlCentr" Width="190px" Height="22px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Location)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                            </td>
                                                        </tr>

                                                         <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Trans Order No :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtTransOrderNo" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Trans Date :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtTransDate" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="155px" autocomplete="off"></asp:TextBox>
                                                                 <asp:ImageButton ID="btnTransDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                             </td>
                                                        </tr>

                                                        <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Reles Order No :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtReleOrderNo" ClientIDMode="Static" runat="server"
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Reles Date :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtReleDate" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="155px" autocomplete="off"></asp:TextBox>
                                                                 <asp:ImageButton ID="btnReleDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                             </td>
                                                        </tr>

                                                         <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Memo No :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtMemoNo" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Memo Date :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtMemoDate" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="155px" autocomplete="off"></asp:TextBox>
                                                                 <asp:ImageButton ID="btnMemoDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                             </td>
                                                        </tr>

                                                         <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Proceeding HeadOffice :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtProcHeadOffice" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Remarks :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtRemarks" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="183px" autocomplete="off"></asp:TextBox>
                                                             </td>
                                                        </tr>
                                                         <tr>
                                                            <td style="padding: 5px; float:right;">
                                                                    <asp:Button ID="btnSave" runat="server" Text="Generate LPC" CommandName="Save" Width="100px"
                                                                    CssClass="Btnclassname" ClientIDMode="Static"  />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="tabs-4" style="float: right; border: solid 2px lightblue; width: 49%;">
                                                    <table align="center" width="100%">
                                                        <tr>
                                                            <td colspan="4" style="padding: 3.5px; background: #deedf7;" class="style1">
                                                                <span class="headFont" style="font-weight: bold;"><font size="3px">Change Office:</font></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;" align="left" class="style1">
                                                                <span class="headFont">From Office :&nbsp;&nbsp;<%--</span><span class="require">*</span>--%>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                            <asp:TextBox ID="txtFrom" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                            </td>
                                                      
                                                            <td style="padding: 2px;" class="style1" align="left">
                                                                <span class="headFont">To Office &nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                         <asp:TextBox ID="txtTo" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                            </td>
                                                        </tr>

                                                         <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Trans Order No :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtTranOrderNo" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Trans Date :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtTranDate" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="155px" autocomplete="off"></asp:TextBox>
                                                                 <asp:ImageButton ID="btnTranDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                             </td>
                                                        </tr>

                                                        <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Reles Order No :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtRelOrderNo" ClientIDMode="Static" runat="server"
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Reles Date :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtRelDate" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="155px" autocomplete="off"></asp:TextBox>
                                                                 <asp:ImageButton ID="btnRelDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                             </td>
                                                        </tr>

                                                         <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Memo No :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtMemNo" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Memo Date :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtMemDate" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="155px" autocomplete="off"></asp:TextBox>
                                                                 <asp:ImageButton ID="btnMemDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                             </td>
                                                        </tr>

                                                         <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Proceeding HeadOffice :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtProccHeadOffice" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">Remarks :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtRemark" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="183px" autocomplete="off"></asp:TextBox>
                                                             </td>
                                                        </tr>
                                                        <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Proceeding Post :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td colspan="6" style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtProceedingPost" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="456px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                       </tr>
                                                        <tr>
                                                             <td style="padding: 2px;"><span class="headFont">Sanctioned Leave :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtSancLeave" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                             </td>

                                                             <td style="padding: 2px;"><span class="headFont">No. of Days :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                             <td style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtNoofDays" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="183px" autocomplete="off"></asp:TextBox>
                                                             </td>
                                                        </tr>
                                                       <%-- <tr>
                                                             <td style="padding: 2px;"><span class="headFont">House Rent :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td colspan="6" style="padding: 2px;" align="left">
                                                                 <asp:TextBox ID="txtRentUpto" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px"></asp:TextBox>
                                                             </td>

                                                       </tr>--%>
                                                        <tr>
                                                             <td style="padding: 2px;"><span class="headFont">House Rent Upto :&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                              
                                                             <td colspan="6" style="padding: 2px;" align="left">
                                                                <asp:TextBox ID="txtRentUptoDate" ClientIDMode="Static" runat="server" 
                                                                     CssClass="textbox" Width="193px" autocomplete="off"></asp:TextBox>
                                                                 <asp:ImageButton ID="btnRentUptoDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                             </td>

                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px; float:right;">
                                                                    <asp:Button ID="btnSave1" runat="server" Text="Generate LPC" CommandName="Save" Width="100px"
                                                                    CssClass="Btnclassname" ClientIDMode="Static"  />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>

