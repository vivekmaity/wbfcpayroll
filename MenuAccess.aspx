﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MenuAccess.aspx.cs" Inherits="MenuAccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            $("#ddluser").rules("add", { required: true, messages: { required: "Please select a User"} });
            
        }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						
						<section>
							<h2>User Menu Permission</h2>						
						</section>

                    <table width="98%" style="border:solid 1px lightblue; " >
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" 
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                    <InsertItemTemplate>
                        <table align="center" cellpadding="5" width="100%">
                            <tr>
                            <td style="padding:10px;" class="labelCaption">User &nbsp;&nbsp;<span class="require">*</span> </td>
                            <td style="padding:10px;" class="labelCaption">:</td>
                            <td style="padding:10px;" align="left" >
                                <asp:DropDownList ID="ddluser" DataSource='<%# drpload() %>' DataValueField="UserID" CssClass="textbox"
                                    DataTextField="UserName" Width="200px" runat="server" AppendDataBoundItems="true" autocomplete="off">
                                    <asp:ListItem Text="(Select A User)" Value=""  ></asp:ListItem> 
                                </asp:DropDownList>                            
                            </td>                            
                            </tr>
                            <tr class="headingCaption"><td style="padding:10px;" align="left">Menu Permissions</td></tr>
                            <tr>
                            <td style="padding:10px;">
                                <asp:TreeView ID="treeView_admin" runat="server" ShowCheckBoxes="All" ExpandDepth="1"
                                    ImageSet="Arrows" NodeIndent="10">
                                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                    <NodeStyle Font-Names="Arial" Font-Size="11px" ForeColor="Black"  
                                        HorizontalPadding="10px" NodeSpacing="2px" VerticalPadding="0px" />
                                    <ParentNodeStyle Font-Bold="False" />
                                    <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" 
                                        HorizontalPadding="0px" VerticalPadding="0px" />
                                </asp:TreeView>
                            </td>
                            </tr>                            
                        </table>
                    </InsertItemTemplate>

                    <EditItemTemplate>
                        <table align="center" cellpadding="5" width="100%">
                            <tr>
                            <td style="padding:10px;" class="labelCaption"><span class="headFont">User&nbsp;&nbsp;</span><span class="require">*</span> </td>
                            <td style="padding:10px;" class="labelCaption">:</td>
                            <td style="padding:10px;" align="left" >
                                <asp:DropDownList ID="ddluser" DataSource='<%# drpload() %>' DataValueField="UserID" CssClass="textbox"
                                    DataTextField="UserName" Width="200px" runat="server" AppendDataBoundItems="true" autocomplete="off"  >
                                    <asp:ListItem Text="(Select A User)" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>                            
                            </tr>
                            <tr class="headingCaption"><td align="left">Menu Permissions</td></tr>
                            <tr>
                            <td style="padding:10px;">
                                <asp:TreeView ID="treeView_admin" runat="server" ShowCheckBoxes="All" ExpandDepth="1"
                                    ImageSet="Arrows" NodeIndent="10">
                                    <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                    <NodeStyle Font-Names="Tahoma" Font-Size="13px" ForeColor="Black"  
                                        HorizontalPadding="10px" NodeSpacing="2px" VerticalPadding="0px" />
                                    <ParentNodeStyle Font-Bold="False" />
                                    <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" 
                                        HorizontalPadding="0px" VerticalPadding="0px" />
                                </asp:TreeView>
                            </td>
                            </tr>
                        </table>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <table align="center" cellpadding="5" width="100%">
                        <tr><td style="padding:10px;" colspan="4" class="labelCaption"><hr style="border:solid 1px lightblue" /></td></tr>
                        <tr>
                            <td style="padding:10px;" class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                            <td>&nbsp;</td>
                            <td style="padding:10px;"  align="left" >
                            <div style="float:left;margin-left:200px;">
                                <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave()' 
                                OnClick="cmdSave_Click" /> 
                                <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()'/>
                            </div>
                            </td>
                        </tr>
                        </table>
                    </FooterTemplate>
                    </asp:FormView>
                    
                </td>
            </tr>
            <tr><td style="padding:10px;" colspan="4" class="labelCaption"><hr style="border:solid 1px lightblue" /></td></tr>
            <tr>
                <td style="padding:10px;">
                    <div style="overflow-y:scroll;min-height:150px;width:100%">
                    <span style="font-weight:bold;color:Black;font-size:14px;">Select user to see menu permission : &nbsp;
                        <asp:DropDownList ID="ddluser2" DataSource='<%# drpload() %>' CssClass="textbox"
                            DataValueField="UserID" OnSelectedIndexChanged="ddluser2_SelectedIndexChanged"
                            DataTextField="UserName" Width="200px" runat="server" AutoPostBack="true" AppendDataBoundItems="true">
                            <asp:ListItem Text="(Select A User)" Value=""></asp:ListItem> 
                        </asp:DropDownList>
                    </span><br /><br />
                    <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"  OnRowDataBound="tbl_RowDataBound" EmptyDataText="No menu access is given to this user"
                    AutoGenerateColumns="false" DataKeyNames="UserID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false" >
                        <AlternatingRowStyle BackColor="Honeydew" />
                        <Columns>
                            <asp:TemplateField>
                            <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    
                                    <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                    runat="server" ID="btnEdit" CommandArgument='<%# Eval("UserID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>

                             <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                   
                                    <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='return Delete();' 
                                    CommandArgument='<%# Eval("UserID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                            <asp:BoundField DataField="UserID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="User Type ID" />
                            <asp:BoundField DataField="UserName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="User Name" />
                            <%--<asp:BoundField DataField="EmployeeID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Employee Code" />--%>
                            <asp:TemplateField HeaderText="Menu Access" HeaderStyle-CssClass="TableHeader">
                            <ItemTemplate>
                                <asp:GridView ID="grdMenu" Width="100%"  ShowHeader="false" runat="server" AutoGenerateColumns="False" >
                                <Columns>
                                    <asp:BoundField DataField="MenuName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" >
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                
                                </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        
                            </asp:TemplateField>                          
                        </Columns>
                    </asp:GridView> 
                    </div>
                </td>
            </tr>
        </table>		
					</div>
				</div>
			</div>
		</div>
</asp:Content>

