﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class TransferEmpDetail : System.Web.UI.UserControl
{
    //PopulateGrid
    public string SecID { get; set; }
    public string flag { get; set; }
    //***********************************************************************************************
    //PopulateTransferEmpDetails
    public string LPCBankListSecID { get; set; }
    //***********************************************************************************************
    //PopulateLocationNotFound
    public string LPCLocationnotAvailable { get; set; }
    //******************************************************************************************
    /*BindITaxArrearDetail*/
    public DataTable dtPFMoreorLess { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (SecID != null && SecID != "" && flag != null && flag!="")
            PopulateGrid();
        //******************************************************************************************
        if (LPCBankListSecID != null && LPCBankListSecID != "")
            PopulateTransferEmpDetails(LPCBankListSecID);
        //******************************************************************************************
        if (LPCLocationnotAvailable != null && LPCLocationnotAvailable != "")
            PopulateLocationNotFound(LPCLocationnotAvailable);
        //******************************************************************************************
        if (dtPFMoreorLess == null)
        { dtPFMoreorLess = new DataTable(); DataRow dr = dtPFMoreorLess.NewRow(); }
        if (dtPFMoreorLess.Rows.Count != 0)
            BindPFMoreorLess(dtPFMoreorLess);

    }

    protected void PopulateGrid()
    {
        try
        {
            DataTable dtsector = DBHandler.GetResult("Get_EmpdetailforacceptLPC", SecID);
            if (dtsector.Rows.Count > 0)
            {
                if (flag == "details")
                {
                    grdViewEmp.DataSource = dtsector;
                    grdViewEmp.DataBind();
                }
                else
                {
                    grdEmp.DataSource = dtsector;
                    grdEmp.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateTransferEmpDetails(string LPCBankListSecID)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Load_LPCCheckForSalbank_grid", LPCBankListSecID);
            if (dt.Rows.Count > 0)
            {
                gvTransferEmpDetails.DataSource = dt;
                gvTransferEmpDetails.DataBind();
            }
        }
        catch (Exception exx)
        {
            throw new Exception(exx.Message);
        }
    }

    protected void PopulateLocationNotFound(string LPCLocationnotAvailable)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("load_employeeSectorStatus_grid", LPCLocationnotAvailable);
            if (dt.Rows.Count > 0)
            {
                gvLocationNotAvailable.DataSource = dt;
                gvLocationNotAvailable.DataBind();
            }
        }
        catch (Exception exx)
        {
            throw new Exception(exx.Message);
        }
    }

    protected void BindPFMoreorLess(DataTable dtPFMoreorLess)
    {
        try
        {
            if (dtPFMoreorLess.Rows.Count > 0)
            {
                gvPFMoreorLess.DataSource = dtPFMoreorLess;
                gvPFMoreorLess.DataBind();
            }
        }
        catch (Exception exx)
        {
            throw new Exception(exx.Message);
        }
    }
}