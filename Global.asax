﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
         //DataAccessOracle.DBHandler.InitializeDB();
         //SiteConstants.ConnectingString = DataAccessOracle.DBHandler.ConnectionString;
        DataAccess.DBHandler.InitializeDB();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        //uservisitedcount();
        //System.Data.DataSet tmpDs = new System.Data.DataSet();
        try
        {
            
            Session.Timeout = 940;
            Session["init"] = 0;
            //System.Data.DataTable drParams = DataAccess.DBHandler.GetResult("Get_Parameters");
            //Session[SiteConstants.SSN_PARAMS] = drParams;

            //DataAccess.DBHandler.Execute("Update_HitCount");
            
            //tmpDs.ReadXml(Server.MapPath("~/App_Data/HitCounter.xml"));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        //Session["hits"] = tmpDs.Tables[0].Rows[0]["HitCount"].ToString();
        
    }

    //void uservisitedcount()
    //{
    //    try
    //    {
    //        System.Data.DataSet ds = new System.Data.DataSet();
    //        ds.ReadXml(Server.MapPath("~/App_Data/HitCounter.xml"));
    //        int hits = Convert.ToInt32(ds.Tables[0].Rows[0]["hits"].ToString());

    //        hits = hits + 1;

    //        ds.Tables[0].Rows[0]["hits"] = hits.ToString();
    //        ds.WriteXml(Server.MapPath("~/App_Data/HitCounter.xml"));
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        
    }
       
</script>
