﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Globalization;

public partial class MonthlySalaryDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dtstr = DBHandler.GetResult("Get_MonthlySalaryDetails");
            tbl.DataSource = dtstr;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_MonthlySalaryDetByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlEmployee")).SelectedValue = dtResult.Rows[0]["EmployeeID"].ToString();
                    ((DropDownList)dv.FindControl("ddlSalMonth")).SelectedValue = dtResult.Rows[0]["SalMonthID"].ToString();
                    ((DropDownList)dv.FindControl("ddlEDType")).SelectedValue = dtResult.Rows[0]["EDType"].ToString();
                    PopulateEDAbvv();
                    ((DropDownList)dv.FindControl("ddlEDAbbv")).SelectedValue = dtResult.Rows[0]["EDID"].ToString();
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_MonthlySalaryDetByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtEDAmt = (TextBox)dv.FindControl("txtEDAmt");
                DropDownList ddlEmployee = (DropDownList)dv.FindControl("ddlEmployee");
                DropDownList ddlSalMonth = (DropDownList)dv.FindControl("ddlSalMonth");
                DropDownList ddlEDType = (DropDownList)dv.FindControl("ddlEDType");
                DropDownList ddlEDAbbv = (DropDownList)dv.FindControl("ddlEDAbbv");

                DataTable dtCheck = DBHandler.GetResult("Check_MonthlySalaryDet", Convert.ToInt32(ddlEmployee.SelectedValue),
                    Convert.ToInt32(ddlSalMonth.SelectedValue));

                if (dtCheck.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record already exists for this employee for this salary month. You can update it.')</script>");
                }
                else
                {
                    DBHandler.Execute("Insert_MonthlySalaryDet", Convert.ToInt32(ddlEmployee.SelectedValue), Convert.ToInt32(ddlSalMonth.SelectedValue),
                        Convert.ToInt32(ddlEDAbbv.SelectedValue), Convert.ToDouble(txtEDAmt.Text), ddlEDType.SelectedValue,
                        ddlEDAbbv.SelectedItem.Text, Session[SiteConstants.SSN_INT_USER_ID]);
                    cmdSave.Text = "Add";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
                }
            }
            else if (cmdSave.CommandName == "Edit")
            {
                TextBox txtEDAmt = (TextBox)dv.FindControl("txtEDAmt");
                DropDownList ddlEmployee = (DropDownList)dv.FindControl("ddlEmployee");
                DropDownList ddlSalMonth = (DropDownList)dv.FindControl("ddlSalMonth");
                DropDownList ddlEDType = (DropDownList)dv.FindControl("ddlEDType");
                DropDownList ddlEDAbbv = (DropDownList)dv.FindControl("ddlEDAbbv");

                string ID = ((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("Update_MonthlySalaryDet", ID, Convert.ToInt32(ddlEmployee.SelectedValue), Convert.ToInt32(ddlSalMonth.SelectedValue),
                    Convert.ToInt32(ddlEDAbbv.SelectedValue), Convert.ToDouble(txtEDAmt.Text), ddlEDType.SelectedValue,
                    ddlEDAbbv.SelectedItem.Text, Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload(string name)
    {
        try
        {
            DataTable dt = new DataTable();
            if (name == "Employee")
            {
                dt = DBHandler.GetResult("Get_EmployeeDdl");
            }
            else if (name == "SalMonth")
            {
                dt = DBHandler.GetResult("Get_SalaryMonth1");
            }
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlEDTypeChanged(object sender, EventArgs e)
    {
        try
        {
            PopulateEDAbvv();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateEDAbvv()
    {
        try
        {
            DropDownList ddlEDType = (DropDownList)dv.FindControl("ddlEDType");
            DropDownList ddlEDAbbv = (DropDownList)dv.FindControl("ddlEDAbbv");
            DataTable dt = DBHandler.GetResult("Get_EDAbbvDdl", ddlEDType.SelectedValue);
            ddlEDAbbv.Items.Clear();
            if (dt.Rows.Count > 0)
            {
                ddlEDAbbv.Items.Insert(0, new ListItem("(Select ED Abbv)", "0"));
                ddlEDAbbv.DataSource = dt;
                ddlEDAbbv.DataTextField = "EDAbbv";
                ddlEDAbbv.DataValueField = "EDID";
                ddlEDAbbv.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}