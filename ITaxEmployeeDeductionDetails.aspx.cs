﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.Configuration;

public partial class ITaxEmployeeDeductionDetails : System.Web.UI.Page
{
    DataTable dtItaxDetail;
    DataTable dtItaxTypeDesc;
    public static int IntEmpItaxID;
    public static int Employee;
    public static int[] terms; //= new int[10];
    public static ArrayList propertyJSON = new ArrayList();

    public void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            PopulateAssessmentYear();
            BindGrid();
            if (Session["ItaxDetail"] == null)
            {
                dtItaxDetail = new DataTable();
                //dtItaxDetail = PensionerDataHandling.CreateITaxTypeTable(dtItaxDetail);
                HttpContext.Current.Session["ItaxDetail"] = dtItaxDetail;
            }
            if (Session["ItaxTypeDesc"] == null)
            {
                dtItaxTypeDesc = new DataTable();
                //dtItaxTypeDesc = PensionerDataHandling.CreateITaxTypeDescTable(dtItaxTypeDesc);
                HttpContext.Current.Session["ItaxTypeDesc"] = dtItaxTypeDesc;
            }
           
        }
        HttpContext.Current.Session["ItaxDetail"] = null;
        HttpContext.Current.Session["ItaxTypeDesc"] = null;

        HttpContext.Current.Session["Table1"] = null;
        HttpContext.Current.Session["Table2"] = null;
        HttpContext.Current.Session["Table3"] = null;

    }


    private void BindGrid()
    {
        DataTable IssueTable = new DataTable();
        IssueTable.Columns.Add("ChallanNo");
        IssueTable.Columns.Add("BSRNo");
        IssueTable.Columns.Add("ChallanDate");
        IssueTable.Columns.Add("TaxAmount");
        IssueTable.Columns.Add("Delete");
        IssueTable.Rows.Add();
        GridViewOther.DataSource = IssueTable;
        GridViewOther.DataBind();
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdShow = (Button)sender;
            if (cmdShow.CommandName == "Show")
            {

                TextBox txtEmpNo = (TextBox)dv.FindControl("txtEmpNo");
                string EmployeeNo = txtEmpNo.Text;
                TextBox SalaryFinYear = (TextBox)dv.FindControl("txtSalaryFinYear");
                string txtSalaryFinYear = SalaryFinYear.Text;
                TextBox AssessmentYear = (TextBox)dv.FindControl("txtAssessmentYear");
                string txtAssessmentYear = AssessmentYear.Text;
                if (txtSalaryFinYear != "")
                {
                    if (txtAssessmentYear != "")
                    {
                        if (EmployeeNo != "")
                        {
                            DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
                            int Secid = Convert.ToInt32(Sectors.SelectedValue.ToString());

                            DataSet ds = DBHandler.GetResults("Get_EmployeeDetails", EmployeeNo, Secid, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                            DataTable dtss = ds.Tables[0]; DataTable dts = ds.Tables[1]; DataTable dtresult = ds.Tables[2];

                            if (dtss.Rows.Count == 0)
                            {
                                if (dtresult.Rows[0][0].ToString() == "Y")
                                {
                                    string display = "Please Select the Appropriate Sector of this Employee.";
                                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                                    return;
                                }
                                else
                                {
                                    string display = "Sorry ! You are not Authorised.";
                                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                                    return;
                                }
                            }
                            else
                            {
                                hdnEmpGender.Value = dtss.Rows[0]["Gender"].ToString();
                                //DataTable dtItax = DBHandler.GetResult("LoadEmpDetailsForItax", HttpContext.Current.Session[SiteConstants.SSN_SALFIN], Secid, EmployeeNo, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                                //DBHandler.Execute("LoadEmpDetailsForItax", HttpContext.Current.Session[SiteConstants.SSN_SALFIN], Secid, EmployeeNo, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                                //DataTable dtItaxDetail = DBHandler.GetResult("Get_ITaxEmployeeDetailsNew", HttpContext.Current.Session[SiteConstants.SSN_SALFIN], Secid, EmployeeNo);
                                DBHandler.Execute("LoadEmpDetailsForItax", txtSalaryFinYear, Secid, EmployeeNo, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                                DataTable dtItaxDetail = DBHandler.GetResult("Get_ITaxEmployeeDetailsNew", txtSalaryFinYear, Secid, EmployeeNo);

                                if (dtItaxDetail.Rows.Count > 0)
                                {

                                    grvItax.DataSource = dtItaxDetail;
                                    grvItax.DataBind();

                                    int TotalGrossPay = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("GrossPay"));
                                    hdnTotalGrossPay.Value = TotalGrossPay.ToString();
                                    txtIncome.Text = TotalGrossPay.ToString();
                                    grvItax.FooterRow.Cells[0].Text = "Total";
                                    grvItax.FooterRow.Cells[0].Font.Bold = true;
                                    grvItax.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    grvItax.FooterRow.Cells[1].Text = TotalGrossPay.ToString();
                                    grvItax.FooterRow.Cells[1].Font.Bold = true;
                                    grvItax.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalPtax = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("Ptax"));
                                    hdnTotalPtax.Value = TotalPtax.ToString();
                                    txtTotalPTax.Text = TotalPtax.ToString();
                                    grvItax.FooterRow.Cells[2].Text = TotalPtax.ToString();
                                    grvItax.FooterRow.Cells[2].Font.Bold = true;
                                    grvItax.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalItax = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("Itax"));
                                    hdnTotalITax.Value = TotalItax.ToString();
                                    txtTDSAmt.Text = TotalItax.ToString();
                                    grvItax.FooterRow.Cells[3].Text = TotalItax.ToString();
                                    grvItax.FooterRow.Cells[3].Font.Bold = true;
                                    grvItax.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalGIS = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("GIS"));
                                    hdnTotalGIS.Value = TotalGIS.ToString();
                                    grvItax.FooterRow.Cells[4].Text = TotalGIS.ToString();
                                    grvItax.FooterRow.Cells[4].Font.Bold = true;
                                    grvItax.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalHRA = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("HRA"));
                                    hdnTotalHRA.Value = TotalHRA.ToString();
                                    txtTotHRA.Text = TotalHRA.ToString();
                                    grvItax.FooterRow.Cells[5].Text = TotalHRA.ToString();
                                    grvItax.FooterRow.Cells[5].Font.Bold = true;
                                    grvItax.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;


                                    int TotalGPF = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("GPF"));
                                    hdnTotalGPF.Value = TotalGPF.ToString();
                                    grvItax.FooterRow.Cells[6].Text = TotalGPF.ToString();
                                    grvItax.FooterRow.Cells[6].Font.Bold = true;
                                    grvItax.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalGPFRecv = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("GPFRecov"));
                                    hdnTotalGPFRec.Value = TotalGPFRecv.ToString();
                                    grvItax.FooterRow.Cells[7].Text = TotalGPFRecv.ToString();
                                    grvItax.FooterRow.Cells[7].Font.Bold = true;
                                    grvItax.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalHBL = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("HBL"));
                                    hdnTotalHBL.Value = TotalHBL.ToString();
                                    //txtTotIHBL.Text = TotalHBL.ToString();
                                    grvItax.FooterRow.Cells[8].Text = TotalHBL.ToString();
                                    grvItax.FooterRow.Cells[8].Font.Bold = true;
                                    grvItax.FooterRow.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalIHBL = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("IHBL"));
                                    hdnTotalIHBL.Value = TotalIHBL.ToString();
                                    txtTotIHBL.Text = TotalIHBL.ToString();
                                    grvItax.FooterRow.Cells[9].Text = TotalIHBL.ToString();
                                    grvItax.FooterRow.Cells[9].Font.Bold = true;
                                    grvItax.FooterRow.Cells[9].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalOther = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("OtherDedu"));
                                    hdnTotalOtherDe.Value = TotalOther.ToString();
                                    grvItax.FooterRow.Cells[10].Text = TotalOther.ToString();
                                    grvItax.FooterRow.Cells[10].Font.Bold = true;
                                    grvItax.FooterRow.Cells[10].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalNetPay = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("NetPay"));
                                    hdnTotalNetPay.Value = TotalNetPay.ToString();
                                    grvItax.FooterRow.Cells[11].Text = TotalNetPay.ToString();
                                    grvItax.FooterRow.Cells[11].Font.Bold = true;
                                    grvItax.FooterRow.Cells[11].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;

                                    int TotalArrerOT = dtItaxDetail.AsEnumerable().Sum(row => row.Field<Int32>("ArrearOT"));
                                    hdnTotalArrearOT.Value = TotalArrerOT.ToString();
                                    int val = Convert.ToInt32(TotalGrossPay.ToString()) + Convert.ToInt32(TotalArrerOT.ToString());
                                    txtIncome.Text = val.ToString();
                                    grvItax.FooterRow.Cells[12].Text = TotalArrerOT.ToString();
                                    grvItax.FooterRow.Cells[12].Font.Bold = true;
                                    grvItax.FooterRow.Cells[12].HorizontalAlign = HorizontalAlign.Right;
                                    grvItax.FooterRow.BackColor = System.Drawing.Color.Aqua;
                                }
                                txtEmpNo.Enabled = false;
                                DataTable gdt = DBHandler.GetResult("Get_EmpGender", EmployeeNo);
                                if (gdt.Rows.Count > 0)
                                {
                                    hdnEmpID.Value = gdt.Rows[0][1].ToString();
                                    Employee = Convert.ToInt32(hdnEmpID.Value);
                                    // PopulateOtherChallan(Convert.ToInt32(hdnEmpID.Value));
                                    TextBox txtGender = (TextBox)dv.FindControl("txtGender");
                                    if (gdt.Rows[0][0].ToString() == "M")
                                    {
                                        txtGender.Text = "Male";
                                    }
                                    else if (gdt.Rows[0][0].ToString() == "F")
                                    {
                                        txtGender.Text = "Female";
                                    }
                                }
                                HiddenCheckGrid.Value = "1";

                            }
                        }
                        else
                        {
                            txtEmpNo.Focus();
                        }
                    }
                    else
                    {
                        AssessmentYear.Focus();
                    }
                }
                else
                {
                    SalaryFinYear.Focus();
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected void PopulateAssessmentYear()
    {
        int minYear = 0; int maxYear = 0; int minAssmtYear = 0; int maxAssmtYear = 0;
        try
        {
            string FinYear = HttpContext.Current.Session[SiteConstants.SSN_SALACC].ToString();
            string[] lines = FinYear.Split('-');
            for (int i = 0; i <= lines.Length; i++)
            {
                minYear = Int32.Parse(lines[0].ToString());
                maxYear = Int32.Parse(lines[lines.Length - 1].ToString());
            }
            minAssmtYear = minYear + 1;
            maxAssmtYear = maxYear + 1;
            string AssessmentYear = minAssmtYear + "-" + maxAssmtYear;
            TextBox txtAssessmentYear = (TextBox)dv.FindControl("txtAssessmentYear");
            txtAssessmentYear.Text = AssessmentYear.ToString();

            TextBox txtSalaryFinYear = (TextBox)dv.FindControl("txtSalaryFinYear");
            txtSalaryFinYear.Text = FinYear;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetTaxBalAndChallan(string hdnEmpID, string SalFinYear, string AssesmentYear)
    {
        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        String xml = "";
        try
        {
            DataSet ds = DBHandler.GetResults("Load_AllChallanAndTaxAmt", hdnEmpID, UserID, SalFinYear, AssesmentYear);
            //DataSet ds = DBHandler.GetResults("Load_AllChallanAndTaxAmt", hdnEmpID, UserID, SalFinYear, AssesmentYear);
            xml = ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return xml;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AddNewSection(string Section, int MaxAmount, string FinYear, string Gender, string hdnEmpID, string Type, string MaxPercentageAmount)
    {
        String Content = "";
        int userID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        try
        {

            DataTable dt = DBHandler.GetResult("Insert_TaxType", Section, Convert.ToInt32(MaxAmount), Type, userID, FinYear, Gender, string.IsNullOrEmpty(MaxPercentageAmount) ? 0 : Convert.ToInt32(MaxPercentageAmount));
            if (dt.Rows.Count > 0)
            {
                DataSet dtAllTax = DBHandler.GetResults("Load_AllITaxTypeByID", dt.Rows[0][0].ToString());
                Content = dtAllTax.GetXml();
            }
        }
        catch (Exception ex)
        {
            Content = "Fail";
            throw new Exception(ex.Message);
        }
        return Content;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string TaxLiabilies(string TaxableIncome, string FinYear, string Gender)
    {
        String Content = "";
        object objdata = new object();
        try
        {
            DataSet ds = DBHandler.GetResults("Get_ITaxRateSlab", TaxableIncome, FinYear, Gender);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var obj = new {
                        ITaxLi = ds.Tables[0].Rows[0]["ITaxLi"],
                        rebate = ds.Tables[0].Rows[0]["rebate"],
                        EduCess = ds.Tables[0].Rows[0]["EduCess"],
                        SecCess = ds.Tables[0].Rows[0]["SecCess"]
                    };
                    objdata = obj;
                }
            }
            Content = ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return objdata.ToJSON();
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Grid2ndGrid(string hdnEmpID)
    {
        String Content = "";
        try
        {
            DataTable dtAllTax = DBHandler.GetResult("Load_AllITaxType", hdnEmpID);
            var page = new Page();
            string controlLocation = "~/ItaxGridView_Page.ascx";
            var userControl = (ItaxGridView_Page)page.LoadControl(controlLocation);
            userControl.EmpID = hdnEmpID;
            userControl.dtAllTax = dtAllTax;
            page.Controls.Add(userControl);
            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                Content = textWriter.ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Content;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveData(object TableChild, object TableParents, object TableChallan, string FinYear, string AssessmentYear, int EmpID, string hdnEmpGender, string hdnTotalGrossPay, string hdnTotalPtax,
        string hdnTotalITax, string hdnTotalGIS, string hdnTotalHRA, string hdnTotalGPF, string hdnTotalGPFRec, string hdnTotalIHBL, string hdnTotalHBL, string hdnTotalOtherDe, string hdnTotalNetPay,
        string hdnTotalArrearOT, string IncomeFromSalary, string InterestFromSaving, string InterestFromFD, string InterestFromOther, string TotDeduChapVIA, string TotTaxableIncome,
        string TaxOnNetIncome, string RabateSec87A, string Surcharge, string EducationCess, string HSEducationCess, string TaxLiabilities, string ReliefUS89,
        string TaxPayabl, string OtherITAX, string BalanceTax, object TableOtherSourceIncome)
    {

        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        string JSonVal = "";
        string ConString = "";
        int SrNo = 1;
        ConString = DataAccess.DBHandler.GetConnectionString();
        var db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
        var cmd = new SqlCommand();
        System.Data.SqlClient.SqlTransaction transaction;
        db.Open();
        transaction = db.BeginTransaction();
        try
        {
            try
            {

                cmd = new SqlCommand("Exec Save_DAT_EmpItax '" + FinYear + "','" + AssessmentYear + "'," + EmpID + ",'" + hdnTotalGrossPay + "','" + hdnTotalPtax +
                    "','" + hdnTotalITax + "','" + hdnTotalGIS + "','" + hdnTotalHRA + "','" + hdnTotalGPF + "','" + hdnTotalGPFRec +
                    "','" + hdnTotalHBL + "','" + hdnTotalIHBL + "','" + hdnTotalOtherDe + "','" + hdnTotalNetPay + "','" + hdnTotalArrearOT +
                    "','" + IncomeFromSalary + "','" + InterestFromSaving + "','" + InterestFromFD + "','" + InterestFromOther + "','" + TotDeduChapVIA + "','" + TotTaxableIncome + "','" + TaxOnNetIncome +
                    "','" + RabateSec87A + "','" + Surcharge + "','" + EducationCess + "','" + HSEducationCess + "','" + TaxLiabilities + "','" + ReliefUS89 + "','" + TaxPayabl +
                    "','" + OtherITAX + "','" + BalanceTax + "'," + UserID + "", db, transaction);

                SqlDataReader ReturnVal = cmd.ExecuteReader();
                if (ReturnVal.Read())
                {
                    IntEmpItaxID = (int)ReturnVal["strID"];
                }
                ReturnVal.Close();
                JSonVal = IntEmpItaxID.ToJSON();

                cmd = new SqlCommand("Exec Delete_TaxSavingType 'TaxType'," + IntEmpItaxID + ", " + UserID + "", db, transaction);
                cmd.ExecuteNonQuery();
                var SrParent = new JavaScriptSerializer();
                List<GetParents> Parents = SrParent.ConvertToType<List<GetParents>>(TableParents);
                for (int i = 0; i < Parents.Count; i++)
                {
                    cmd = new SqlCommand("Exec Save_ItaxSavingType " + IntEmpItaxID + "," + SrNo + "," + Parents[i].ParentID + ",'" + Parents[i].ParentlblMaxAmt + "','" + Parents[i].ParentAmt + "'," + UserID + "", db, transaction);
                    cmd.ExecuteNonQuery();
                    SrNo++;
                }
                Parents.Clear();
                SrNo = 1;
                var serializer1 = new JavaScriptSerializer();
                List<GetChild> Child = serializer1.ConvertToType<List<GetChild>>(TableChild);
                for (int i = 0; i < Child.Count; i++)
                {
                    cmd = new SqlCommand("Exec Save_ItaxSubSavingType " + IntEmpItaxID + "," + SrNo + "," + Child[i].ChildID + ",'" + Child[i].ChildlblMaxAmt + "','" + Child[i].ChildAmt + "'," + UserID + "", db, transaction);
                    cmd.ExecuteNonQuery();
                    SrNo++;
                }
                Child.Clear();
                cmd = new SqlCommand("Exec Delete_TaxSavingType 'Challan'," + IntEmpItaxID + ", " + UserID + "", db, transaction);
                cmd.ExecuteNonQuery();
                SrNo = 1;
                var Challan = new JavaScriptSerializer();
                List<GetChallan> TChallan = Challan.ConvertToType<List<GetChallan>>(TableChallan);
                for (int i = 0; i < TChallan.Count; i++)
                {
                    cmd = new SqlCommand("Exec Save_ItaxOtherChallan " + IntEmpItaxID + "," + SrNo + ",'" + TChallan[i].ChallanNo + "','" + TChallan[i].ChallanDate + "','" + TChallan[i].BSRNo + "','" + TChallan[i].Amount + "'," + UserID + "", db, transaction);
                    cmd.ExecuteNonQuery();
                    SrNo++;
                }
                TChallan.Clear();


                //****************Sup******************
                var OtherSourceIncome = new JavaScriptSerializer();
               // List<OtherSourceofIncome> OthSrcIncome = OtherSourceIncome.ConvertToType<List<OtherSourceofIncome>>(TableOtherSourceIncome);
                List<OtherSourceofIncome> OthSrcIncome = OtherSourceIncome.ConvertToType<List<OtherSourceofIncome>>(TableOtherSourceIncome);
                for (int i = 0; i < OthSrcIncome.Count; i++)
                {
                    cmd = new SqlCommand("Exec Save_DAT_EmpItaxDtls " + IntEmpItaxID + ",'" + FinYear.ToString() + "','" + AssessmentYear.ToString() + "'," + EmpID + ",'" + OthSrcIncome[i].OtherSourceId + "','" + OthSrcIncome[i].Amount + "'," + UserID + "", db, transaction);
                    cmd.ExecuteNonQuery();
                    SrNo++;
                }
                OthSrcIncome.Clear();

                //*************************************

                transaction.Commit();
                db.Close();
                JSonVal = "Record Save Successfully..!";
                return JSonVal;
            }
            catch (SqlException sqlError)
            {
                transaction.Rollback();
                JSonVal = "Record Not Save Successfully..!";
                db.Close();
                return JSonVal;

            }
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            db.Close();
            throw new Exception(ex.Message);
        }
    }


    class GetChild
    {
        public int ChildID { get; set; }
        public string ChildlblMaxAmt { get; set; }
        public string ChildAmt { get; set; }
    }
    class GetParents
    {
        public int ParentID { get; set; }
        public string ParentlblMaxAmt { get; set; }
        public string ParentAmt { get; set; }
    }
    class GetChallan
    {
        public string ChallanNo { get; set; }
        public string BSRNo { get; set; }
        public string ChallanDate { get; set; }
        public string Amount { get; set; }
    }
    class OtherSourceofIncome
    {
        public int OtherSourceId { get; set; }
        public string Amount { get; set; }
    }
    //Supriyo********************************************//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string PopulateTableValue(string SalaryFinYear, string AssessmentYear, int hdnEmpID)
    {
        List<object> result = new List<object>();
        object dataObj = new object();
        try
        {
            DataTable dt = DBHandler.GetResult("Load_OtherSourceIncome", SalaryFinYear, AssessmentYear, hdnEmpID);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dataObj = new
                    {
                        OtherSourceId = dr["OtherSourceId"],
                        OtherSourceName = dr["OtherSourceName"],
                        OtherIncome = dr["OtherIncome"]
                        
                    };
                    result.Add(dataObj);
                }
            }
            else
            {
                dataObj = null;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return result.ToJSON();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string PopulateNewAddedTableValue(string SalaryFinYear, string AssessmentYear, int hdnEmpID)
    {
        List<object> result = new List<object>();
        object dataObj = new object();
        try
        {
            DataTable dt = DBHandler.GetResult("Load_OtherSourceIncome", SalaryFinYear, AssessmentYear, hdnEmpID);
            if (dt.Rows.Count > 0)
            {

                dataObj = new
                {
                    OtherSourceId = dt.Rows[dt.Rows.Count - 1]["OtherSourceId"],
                    OtherSourceName = dt.Rows[dt.Rows.Count - 1]["OtherSourceName"],
                    OtherIncome = dt.Rows[dt.Rows.Count - 1]["OtherIncome"],
                    
                    Srlno = dt.Rows.Count
                };
                result.Add(dataObj);
            }
            else
            {
                dataObj = null;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return result.ToJSON();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveMediclaimDetails(string OtherSourceName)
    {
        object ObjData = new object();
        try
        {
            string ConString = "";
            ConString = DataAccess.DBHandler.GetConnectionString();
            SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            db.Open();
            transaction = db.BeginTransaction();
            SqlDataReader dr;
            try
            {
                try
                {
                    int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    int ID=0;
                    cmd = new SqlCommand("Exec Check_OtherSourceIncome '" + ID + "','" + OtherSourceName + "','" + UserId + "'", db, transaction);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        ObjData = new
                        {
                            Message = dr["Messege"].ToString(),
                            MessageCode = Convert.ToInt32(dr["ErrorCode"].ToString()),
                            Status = "Success"
                        };
                    }
                    dr.Close();
                    transaction.Commit();
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    ObjData = new
                    {
                        Message = sqlError.Message,
                        MessageCode = "",
                        Status = "Fail"
                    };
                }
                db.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return ObjData.ToJSON();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetACCyearBySalFinYear(string salaryFinYear)
    {
        List<object> result = new List<object>();
        object dataObj = new object();
        try
        {
            DataTable dt = DBHandler.GetResult("SalToAssessmentYear", salaryFinYear);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dataObj = new
                    {
                        ErrorCode = dr["ErrorCode"],
                        Messege = dr["Messege"],
                        Assessment = dr["Assessment"],

                    };
                    result.Add(dataObj);
                }
            }
            else
            {
                dataObj = null;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return result.ToJSON();
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveNewSavingType(string Description, int TaxTypeId, string Abbre, string MaxAmount, int EmpID, string type, string Percentage)
    {
        List<object> objdata = new List<object>();
        object ObjDataReturn = new object();

        try
        {
            try
            {
                try
                {
                    int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    int ID = 0;
                    DataTable dtinst = DBHandler.GetResult("InstUpdtItaxSavingType", ID, Abbre, Description, type, MaxAmount, string.IsNullOrEmpty(Percentage) ? 0 : Convert.ToDecimal(Percentage), TaxTypeId, 1, UserId);
                       if(dtinst.Rows.Count>0)
                       {
                           if (dtinst.Rows[0]["ErrorCode"].ToString() == "1")
                           {
                               DataTable dtSavingType = DBHandler.GetResult("Load_AllSavingType", EmpID, TaxTypeId);
                               if (dtSavingType.Rows.Count > 0)
                               {

                                   var Obja = new
                                   {
                                       ItaxSaveID = dtSavingType.Rows[dtSavingType.Rows.Count - 1]["ItaxSaveID"],
                                       SavingTypeDesc = dtSavingType.Rows[dtSavingType.Rows.Count - 1]["SavingTypeDesc"],
                                       Amount = dtSavingType.Rows[dtSavingType.Rows.Count - 1]["Amount"].ToString(),
                                   };
                                   objdata.Add(Obja);

                               }
                               ObjDataReturn = new
                               {
                                   data = objdata,
                                   MessageCode = dtinst.Rows[0]["ErrorCode"].ToString(),
                                   Status = "Success"
                               };
                           }
                           else
                           {
                               ObjDataReturn = new
                               {
                                   data = "",
                                   MessageCode = dtinst.Rows[0]["ErrorCode"].ToString(),
                                   Status = "Fail"
                               };
                           }
                        }
                   
                }
                catch (SqlException sqlError)
                {
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return ObjDataReturn.ToJSON();
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string CheckMaxAmount(string ItaxSavingsAmount, int TaxTypeId, string lblTotal)
    {
        object objdata = new object();
        DataTable dtSavingType = DBHandler.GetResult("check_ItaxMaxSave", TaxTypeId, ItaxSavingsAmount, lblTotal);
        if (dtSavingType.Rows.Count>0)
        {
            var obj = new
            {
                maxamt = dtSavingType.Rows[0]["maxamt"],
                totalamt = dtSavingType.Rows[0]["totalamt"]
            };
            objdata = obj;
        }
        return objdata.ToJSON();
    }

    //***************************************************//
}