﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class FinalPensionGeneration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulatePensionMonth();
        }
    }


    protected void PopulatePensionMonth()
    {
        try
        {
            var FinYear = HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString();
            DataTable dt = DBHandler.GetResult("Get_PensionMonth", FinYear);
            if (dt.Rows.Count > 0)
            {
                txtPayMonth.Text = dt.Rows[0]["PenMonth"].ToString();
                hdnPenMonthID.Value = dt.Rows[0]["PensionMonthID"].ToString();
                hdnPenMonth.Value = dt.Rows[0]["PenMonth"].ToString();
            }

        }
        catch (Exception exx)
        {
            throw new Exception(exx.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetEncodedValue(string penmonth)
    {
        string JSONVal = "";
        try
        {
            var FinYear = HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString();
            DataTable dtEncodeValue = DBHandler.GetResult("Get_PensionMonth", FinYear);

            if (dtEncodeValue.Rows.Count > 0)
            {
                string EncodedValue = dtEncodeValue.Rows[0]["EncodeValue"].ToString();
                if (EncodedValue != null || EncodedValue != "")
                {
                    JSONVal = EncodedValue.ToJSON();
                }
                else
                {
                    string encodevalue = "";
                    JSONVal = encodevalue.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string MessageBoxYesNo(string Message, string buttonText)
    {
        try
        {
            string controlLocation = "~/GridviewTemplete.ascx";
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.Message = Message;
            userControl.MessageButtonText = buttonText;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string MessageBox(string Message)
    {
        try
        {
            string controlLocation = "~/GridviewTemplete.ascx";
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.Message = Message;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GoForNextMonth(string penmonthid)
    {
        string JSONVal = ""; 
        try
        {
            DataSet ds1 = DBHandler.GetResults("Pen_Sal_Generation", penmonthid, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID], 'Y');
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }
}