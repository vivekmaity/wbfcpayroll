﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MonthlySalaryMaster.aspx.cs" Inherits="MonthlySalaryMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        function beforeSave() {
            $("#form1").validate();
            $("#ddlEmployeeName").rules("add", { required: true, messages: { required: "Please select Employee Name"} });
            $("#ddlEmployeeSalary").rules("add", { required: true, messages: { required: "Please select Employee Salary"} });
            $("#ddlEmployeeName").rules("add", { required: true, messages: { required: "Please select Employee Name"} });
            $("#txtGrossPay").rules("add", { required: true, messages: { required: "Please Enter Gross Pay"} });
            $("#txtTotalDeduction").rules("add", { required: true, messages: { required: "Please Enter Total Deduction"} });
            $("#txtNetPay").rules("add", { required: true, messages: { required: "Net Pay Can't Be Empty "} });
                           }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function SubNumbers() {
            var val1 = parseInt(document.getElementById("txtGrossPay").value);
            var val2 = parseInt(document.getElementById("txtTotalDeduction").value);
            if (val2 < val1) {
                var ansD = val1 - val2;
                document.getElementById('txtNetPay').value = ansD;
                //document.getElementById('txtNetPay').disabled = true;
            }
            else {
                alert('Deduction value can not be greater than gross value');
                //document.getElementById('txtNetPay').disabled = true;
            }
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Monthly&nbsp Salary&nbsp  Master</h2>						
						</section>
                    <table width="98%"   style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployeeName" Width="223px" Height="23px" runat="server" DataSource='<%#drpload1() %>'
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static"
                                                OnSelectedIndexChanged="ddlEmployeeNameSelectedIndexChanged" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee Name)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Employee Salary For Month&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployeeSalary" Width="223px" Height="23px" runat="server" DataSource='<%#drpload2() %>'
                                            DataValueField="SalMonthID" DataTextField="SalMonth" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="(Select Salary Month)" Value=""></asp:ListItem> 
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="padding:5px;" ><span class="headFont">Gross Pay &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtGrossPay" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Total Deduction &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtTotalDeduction" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" onkeypress="return isNumberKey(event)" onchange="SubNumbers()" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Net Pay &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtNetPay" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                         </td>
                                   </tr>
                                </table>
                            </InsertItemTemplate>

                            <EditItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployeeName" Width="223px" Height="23px" runat="server" DataSource='<%#drpload1() %>'
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static"
                                                OnSelectedIndexChanged="ddlEmployeeNameSelectedIndexChanged" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee Name)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("MonthlySalaryID") %>'/>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Employee Salary For Month &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployeeSalary" Width="223px" Height="23px" runat="server" DataSource='<%#drpload2() %>'
                                            DataValueField="SalMonthID" DataTextField="SalMonth" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="(Select Salary Month)" Value=""></asp:ListItem> 
                                            </asp:DropDownList>
                                            <%--<asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("MonthlySalaryID") %>'/>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Enter Gross Pay &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtGrossPay" ClientIDMode="Static" runat="server" CssClass="textbox"  MaxLength="10" Text='<%# Eval("GrossPay") %>' onkeypress= "return isNumberKey(event)" autocomplete="off" ></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Enter Total Deduction&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtTotalDeduction" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("TotalDeduction") %>' onkeypress= "return isNumberKey(event)" onchange="SubNumbers()" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Net Pay &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtNetPay" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("NetPay") %>' autocomplete="off"></asp:TextBox>
                                         </td>
                                   </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> Indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:auto;width:100%;height:300px">
                            <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="MonthlySalaryID" OnRowCommand="tbl_RowCommand" AllowPaging="false" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("MonthlySalaryID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                                OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("MonthlySalaryID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="MonthlySalaryID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="MonthlySalaryID" />
                                    <asp:BoundField DataField="EmployeeID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="EmployeeName" />
                                    <asp:BoundField DataField="SalMonthID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="LoanTypeID" />
                                    <asp:BoundField DataField="EmpName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Employee ID" />
                                    <asp:BoundField DataField="SalMonth" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Salary Month" />
                                    <asp:BoundField DataField="GrossPay" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Gross Pay" />
                                    <asp:BoundField DataField="TotalDeduction" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Total Deduction" />
                                    <asp:BoundField DataField="NetPay" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Net Pay" />
                                </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

