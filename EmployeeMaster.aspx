﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="EmployeeMaster.aspx.cs" Inherits="EmployeeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #AutoComplete-EmployeeNo {
            display: block;
            position: relative;
        }
         .ui-autocomplete {
            height: 200px;
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #cbc7c7;
            font-size: 13px;
            font-weight: normal;
            color: #242424;
            background: #fff;
            -moz-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            -webkit-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            box-shadow: inset 0.9px 1px 3px #e4e4e4;
            width: 350px;
            /*height:30px;*/
            padding: 5px;
            font-family: Segoe UI, Lucida Grande, Arial, Helvetica, sans-serif;
            margin: 3px 0;
        }
    </style>
    <script src="js/ajaxfileupload.js"></script>
    <script src="js/employeeMaster.js?v3.5"></script>
    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>

    <link href="css/style.css" rel="stylesheet" />
    <script src="js/ValidationTextBox.js"></script>

    <script type="text/javascript">
        function myAgeValidation(birthday) {

            var lre = /^\s*/;
            var datemsg = "";

            var inputDate = birthday.value;
            inputDate = inputDate.replace(lre, "");
            datemsg = isValidDate(inputDate);
            if (datemsg != "") {
                alert(datemsg);
                return;
            }
            else {
                //Now find the Age based on the Birth Date
                getAge((inputDate));
            }

        }

        function getAge(birth) {

            var today = new Date();
            var nowyear = today.getFullYear();
            var nowmonth = today.getMonth();
            var nowday = today.getDate();

            var calbirth = new Date(birth.substr(6, 10), birth.substr(3, 5) - 1, birth.substr(0, 2));

            var birthyear = calbirth.getFullYear();
            // alert(birthyear);
            var birthmonth = calbirth.getMonth();
            // alert(birthmonth);
            var birthday = calbirth.getDate();
            // alert(birthday);

            var age = nowyear - birthyear;
            var age_month = nowmonth - birthmonth;
            var age_day = nowday - birthday;

            if (age_month < 0 || (age_month == 0 && age_day < 0)) {
                age = parseInt(age) - 1;
            }
            $('#txtAge').val(age + ' years');
            //  alert(age);

        }

        function isValidDate(dateStr) {


            var msg = "";

            var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;

            var matchArray = dateStr.match(datePat); // is the format ok?
            if (matchArray == null) {
                msg = "Date is not in a valid format.";
                return msg;
            }

            month = matchArray[3]; // parse date into variables
            day = matchArray[1];
            year = matchArray[4];
            //      alert(month + " " + day + " " + year );

            if (month < 1 || month > 12) { // check month range
                msg = "Month must be between 1 and 12.";
                return msg;
            }

            if (day < 1 || day > 31) {
                msg = "Day must be between 1 and 31.";
                return msg;
            }

            if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
                msg = "Month " + month + " doesn't have 31 days!";
                return msg;
            }

            if (month == 2) { // check for february 29th
                var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                if (day > 29 || (day == 29 && !isleap)) {
                    msg = "February " + year + " doesn't have " + day + " days!";
                    return msg;
                }
            }

            if (day.charAt(0) == '0') day = day.charAt(1);
            //     alert("day " + day);
            //Incase you need the value in CCYYMMDD format in your server program
            //msg = (parseInt(year,10) * 10000) + (parseInt(month,10) * 100) + parseInt(day,10);

            return msg;  // date is valid
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            //document.getElementById("ddlIR").disabled = false; 
            //document.getElementById("chkIR").disabled = false;

            //document.getElementById("txtSpouseAmt").disabled = false;
            //document.getElementById("txtHealthScheme").disabled = false;
            //document.getElementById("txtEffFrm").disabled = false;
            //document.getElementById("txtEffTo").disabled = false;
            //document.getElementById("txtrentAmount").disabled = false;
        });

        $(document).ready(function () {
            //$("#txtAmount").keydown(function (event) {
            //    // Allow: backspace, delete, tab, escape, and enter
            //    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            //        // Allow: Ctrl+A
            //(event.keyCode == 65 && event.ctrlKey === true) ||
            //        // Allow: home, end, left, right
            //(event.keyCode >= 35 && event.keyCode <= 39)) {
            //        // let it happen, don't do anything
            //        return;
            //    }
            //    else {
            //        // Ensure that it is a number and stop the keypress
            //        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            //            event.preventDefault();
            //            $("#errmsg").html("Digits Only").show().fadeOut(1500);
            //            return false;

            //        }
            //    }
            //    });
            });


        $(document).ready(function () {
            $("#txtHeight").keydown(function (event) {

                // Allow: backspace, delete, tab, escape, and enter
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                    // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
                    // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#errmsg").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtPin").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span1").html("Digits Only").show().fadeOut(1500);
                        return false;
                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtPhone").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span2").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtMobile").keydown(function (event) {
                var reg = /^0+/gi;
                if (this.value.match(reg)) {
                    this.value = this.value.replace(reg, '');
                }

                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    if ($('#txtMobile').length > 10 || $('#txtMobile').length < 10) {
                        alert("Please insert a valid mobile no.");
                    }
                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span3").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtPin_P").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span4").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtPhone_P").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span5").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtExp").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span6").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtmarks").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span7").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtYear").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span8").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtBankCode").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span8").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtCoMembership").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span8").html("Digits Only").show().fadeOut(1500);
                        return false;

                    }
                }
            });
        });


    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#rdHouse, #rdBasic").change(function () {

                if ($("#rdHouse").is(":checked")) {
                    var house = $("#ddlHRACat").val();
                    if (house == "A" || house == "L") {
                       // $("#txtOldbasic").val('');
                        $("#txtPCTBasic").val('');
                        $("#txtrentAmount").val('');
                        var qrAlt = $("#ddlQuatrAlt").val();
                        if (qrAlt == "Y") {
                           // document.getElementById("txtOldbasic").disabled = true;
                            document.getElementById("txtPCTBasic").disabled = true;
                            document.getElementById("ddlHouse").disabled = false;
                        }
                    }
                }
                else if ($("#rdBasic").is(":checked")) {
                    $("#ddlHouse").val('');
                    $("#txtrentAmount").val('');
                    var qrAlt = $("#ddlQuatrAlt").val();
                    if (qrAlt == "Y") {
                        document.getElementById("ddlHouse").disabled = true;
                       // document.getElementById("txtOldbasic").disabled = false;
                        document.getElementById("txtPCTBasic").disabled = false;
                        $("#txtPCTBasic").focus();
                        var house = $("#ddlHRACat").val();
                        if (house == "A")
                            $("#txtPCTBasic").val($("#hdHra").val());
                        else
                            $("#txtPCTBasic").val('');
                    }
                }
                else
                    $('#tabHouse').show(); $('#tabBasic').show();
            });
        });

        $(document).ready(function () {
            $('#txtAppoint').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow'

            });

            $("#btnAppoint").click(function () {
                $('#txtAppoint').datepicker("show");
                return false;
            });

        });

        $(document).ready(function () {
            $('#txtJoining').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow'

            });

            $("#btnJoining").click(function () {
                $('#txtJoining').datepicker("show");
                return false;
            });

        });

        $(document).ready(function () {
            $('#txtRetirement').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow'

            });

            $("#btnRetirement").click(function () {
                $('#txtRetirement').datepicker("show");
                return false;
            });

        });
       
        $(document).ready(function () {
            var personal = document.getElementById("<%=tabPersonal.ClientID %>");
            var professional = document.getElementById("<%=tabProfessional.ClientID %>");

            if (document.getElementById('rdEmployee').checked) {
                //alert('ssssssssssssss');
                professional.style.display = 'none';
                personal.style.display = 'block';
            }
            else if (document.getElementById('rdEmployee1').checked) {
                personal.style.display = "none";
                professional.style.display = "block";
            }
            else {

                personal.style.display = "none";
                professional.style.display = "none";
            }
        });
        
    </script>
    
    <script type="text/javascript">

        function beforeSave() {
            $("#form1").validate();
            $("#EmpCode").rules("add", { required: true, messages: { required: "Please enter Employee Code" } });
            $("#ddlDept").rules("add", { required: true, messages: { required: "Please select Department" } });
            $("#txtEmpFName").rules("add", { required: true, messages: { required: "Please enter Employee First Name" } });

        }

        function beforeSave_Search() {
            $("#form1").validate();
            $("#txtEmpCodeSearch").rules("add", { required: true, messages: { required: "Please enter Employee No." } });
        }

        function beforeSave_EarnDeduction() {
            $("#form1").validate();
            $("#ddlempnameEdu").rules("add", { required: true, messages: { required: "Please select Employee Name" } });
            $("#ddlEarnDeduction").rules("add", { required: true, messages: { required: "Please select Earn/Deduction" } });
            $("#txtAmount").rules("add", { required: true, messages: { required: "Please enter Amount" } });
        }

        function beforeSave_JobPrevious() {
            $("#form1").validate();
            $("#ddlempname").rules("add", { required: true, messages: { required: "Please select Employee name" } })
            $("#txtCname").rules("add", { required: true, messages: { required: "Please enter Company name" } })
            //            $("#txtDOR").rules("add", { required: true, messages: { required: "Please enter Release date" } });

        }

        function beforeSave_Education() {
            $("#form1").validate();
            $("#ddlempnameEdu").rules("add", { required: true, messages: { required: "Please select Employee name" } })

        }


        function beforeSave_Family() {
            $("#form1").validate();
            $("#ddlempnameFamily").rules("add", { required: true, messages: { required: "Please select Employee name" } })
            $("#txtname").rules("add", { required: true, messages: { required: "Please enter  name" } })
            $("#txtrlsn").rules("add", { required: true, messages: { required: "Please enter relation" } })

        }


        function beforeSave_Ref() {
            $("#form1").validate();
            $("#ddlempnameRef").rules("add", { required: true, messages: { required: "Please select Employee name" } })
            $("#txtname").rules("add", { required: true, messages: { required: "Please enter  name" } })
            $("#txtref").rules("add", { required: true, messages: { required: "Please enter reference" } })
        }

        function beforeSave_Training() {
            $("#form1").validate();
            $("#ddlempnameTraining").rules("add", { required: true, messages: { required: "Please select Employee name" } })
            //            $("#txtname").rules("add", { required: true, messages: { required: "Please enter  name"} })
            //            $("#txtref").rules("add", { required: true, messages: { required: "Please enter reference"} })
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }


        $(function () {
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });

        });

        $(function () {
            $("#tabs1").tabs({
                collapsible: true,
                selected: 0
            });
       });

        var pageUrl = '<%=ResolveUrl("~/EmployeeMaster.aspx")%>'
       

    </script>

    <script type="text/javascript">


        function callfunction() {
            var personal = document.getElementById("<%=tabPersonal.ClientID %>");
            var professional = document.getElementById("<%=tabProfessional.ClientID %>");

            if (document.getElementById('rdEmployee').checked) {

                professional.style.display = 'none';
                personal.style.display = 'block';
            }
            else if (document.getElementById('rdEmployee1').checked) {
                personal.style.display = "none";
                professional.style.display = "block";
            }
            else {

                personal.style.display = "none";
                professional.style.display = "none";
            }
        }

        //function GetDropDownData() {
        //    var myDropDownList = $('#ddlState');

        //    $.ajax({
        //        type: "POST",
        //        url: "EmployeeMaster.aspx/PopulateDistrict",
        //        data: '{name: "abc" }',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (data) {
        //            $.each(jQuery.parseJSON(data.d), function () {
        //                myDropDownList.append($("<option></option>").val(this['FieldDescription']).html(this['FieldCode']));
        //            });
        //        },
        //        failure: function (response) {
        //            alert(response.d);
        //        }
        //    });
        //}
        //function OnSuccess(response) {
        //    console.log(response.d);
        //    alert(response.d);
        //}

        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "EmployeeMaster.aspx/BindState",
                data: {},
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var jsdata = JSON.parse(data.d);
                    $("#ddlState").append("<option value=''>(Select State)</option>")
                    $.each(jsdata, function (key, value) {
                        $("#ddlState").append($("<option></option>").val(value.StateId).html(value.State));
                    });
                },
                error: function (data) {
                    //alert("error found");
                }
            });
        });
        $(document).ready(function () {
                //$("#ddlDistrict").prop("disabled", true);
                $("#ddlState").change(function () {

                    if ($("#ddlState").val() != "Please select") {

                        var options = {};
                        options.url = "EmployeeMaster.aspx/BindDistricts";
                        options.type = "POST";
                        options.data = "{StateID: " + $('#ddlState').val() + "}";
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listdistrict) {
                            var t = jQuery.parseJSON(listdistrict.d);
                            var a = t.length;
                            $("#ddlDistrict").empty();
                            if (a > 0) {
                                $("#ddlDistrict").append("<option value=''>(Select District)</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlDistrict").append($("<option></option>").val(value.DistID).html(value.District));
                                });
                                }
                            $("#ddlDistrict").prop("disabled", false);
                        };
                            options.error = function () { alert("Error retrieving Districts!"); };
                            $.ajax(options);
                        }
                        else {
                            $("#ddlDistrict").empty();
                            $("#ddlDistrict").prop("disabled", true);
                        }
                });
            });
        $(document).ready(function () {
                    $.ajax({
                        type: "POST",
                        url: "EmployeeMaster.aspx/BindStateforPermanentAddress",
                        data: {},
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlState_P").append("<option value=''>(Select State)</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlState_P").append($("<option></option>").val(value.StateId).html(value.State));
                        });
                    },
                    error: function (data) {
                       // alert("error found");
                    }
                });
            });
        $(document).ready(function () {
                    //$("#ddlDistrict_P").prop("disabled", true);
                    $("#ddlState_P").change(function () {

                        if ($("#ddlState_P").val() != "Please select") {

                            var options = {};
                            options.url = "EmployeeMaster.aspx/BindDistrictsforPermanentAddress";
                            options.type = "POST";
                            options.data = "{StateID: " + $('#ddlState_P').val() + "}";
                            options.dataType = "json";
                            options.contentType = "application/json";
                            options.success = function (listdistrict) {
                                var t = jQuery.parseJSON(listdistrict.d);
                                var a = t.length;
                                $("#ddlDistrict_P").empty();
                                if (a > 0) {
                                    $("#ddlDistrict_P").append("<option value=''>(Select District)</option>")
                                    $.each(t, function (key, value) {
                                        $("#ddlDistrict_P").append($("<option></option>").val(value.DistID).html(value.District));
                                });
                                }
                            $("#ddlDistrict_P").prop("disabled", false);
                        };
                            options.error = function () { alert("Error retrieving Districts!"); };
                            $.ajax(options);
                        }
                        else {
                            $("#ddlDistrict_P").empty();
                            $("#ddlDistrict_P").prop("disabled", true);
                        }
                });
                });

        //This is for Bind DA on the Basis of EmployeeType
        $(document).ready(function () {
                    //$("#ddlDA").prop("disabled", true);
                    $("#ddlEmpType").change(function () {

                        if ($("#ddlEmpType").val() != "Please select") {
                            var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
                            //alert(E);
                            var options = {};
                            options.url = "EmployeeMaster.aspx/BindDA";
                            options.type = "POST";
                            options.data = E;
                            options.dataType = "json";
                            options.contentType = "application/json";
                            options.success = function (listdA) {
                                var t = jQuery.parseJSON(listdA.d);
                                var a = t.length;
                                $("#ddlDA").empty();
                                if (a >= 0) {
                                    $("#ddlDA").append("<option value=''>(Select DA)</option>")
                                    $.each(t, function (key, value) {
                                        $("#ddlDA").append($("<option selected='true'></option>").val(value.DAID).html(value.DARate));
                                       
                            });
                            }

                        $("#ddlDA").prop("disabled", false);
                    };
                        options.error = function () { alert("Error in retrieving DA!"); };
                        $.ajax(options);
                    }
                    else {
                        $("#ddlDA").empty();
                        $("#ddlDA").prop("disabled", true);
                    }
            });
        });
        //This is for Binding 'HRA' on the Base of Employee Type
        $(document).ready(function () {
            $("#ddlEmpType").change(function () {
                $("#ddlQuatrAlt").val('N');
                $("#ddlSpouseQtr").val('N');
                $("#ddlSpouseQtr").prop("disabled", false);
                if ($("#ddlEmpType").val() != "Please select") {
                    var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
                    //alert(E);
                    var options = {};
                    options.url = "EmployeeMaster.aspx/BindHRA";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listHRAS) {
                        var t = jQuery.parseJSON(listHRAS.d);
                        var a = t.length;
                        $("#ddlHRA").empty();
                        if (a >= 0) {
                            $("#ddlHRA").append("<option value='0'>(Select HRA)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlHRA").append($("<option selected='true'></option>").val(value.HRAID).html(value.HRA));
                            });
                        }

                        $("#ddlHRA").prop("disabled", false);

                        var hra = $('#ddlHRA').find('option:selected').text();
                        $('#hdHra').val(hra);
                    };
                    options.error = function () { alert("Error in retrieving HRA!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlHRA").empty();
                    $("#ddlHRA").prop("disabled", true);
                }
            });
        });

        //This is for Bind IR on the Basis of EmployeeType
        $(document).ready(function () {
            $("#ddlEmpType").change(function () {

                if ($("#ddlEmpType").val() != "Please select") {
                    var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
                    //alert(E);
                    var options = {};
                    options.url = "EmployeeMaster.aspx/BindIR";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listIR) {
                        var t = jQuery.parseJSON(listIR.d);
                        var a = t.length;
                        $("#ddlIR").empty();
                        if (a >= 0) {
                            $("#ddlIR").append("<option value=''>(Select IR)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlIR").append($("<option selected='true'></option>").val(value.IRID).html(value.IRRate));

                            });
                        }

                        //$("#ddlIR").prop("disabled", true);
                    };
                    options.error = function () { alert("Error in retrieving IR!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlIR").empty();
                    //$("#ddlIR").prop("disabled", true);
                }
            });
        });

        //This is for Binding 'Pay Scale Type' on the Base of Employee Type
        $(document).ready(function () {
                $("#ddlPayScaleType").prop("disabled", false);
                $("#ddlEmpType").change(function () {
                    if ($("#ddlEmpType").val() != "Please select") {
                        var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
                        //alert(E);
                        var options = {};
                        options.url = "EmployeeMaster.aspx/GetPayScalebyEMPType";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listpayScale) {
                            var t = jQuery.parseJSON(listpayScale.d);
                            var a = t.length;
                            $("#ddlPayScaleType").empty();
                            if (a >= 0) {
                                $("#ddlPayScaleType").append("<option value=''>(Select Pay Scale)</option>")
                                $.each(t, function (key, value) {
                                    //value.PayScaleID
                                    $("#ddlPayScaleType").append($("<option></option>").val(value.PayScaleID).html(value.PayScale));
                                });
                            }

                            $("#ddlPayScaleType").prop("disabled", false);
                        };
                        //options.error = function () { alert("Error in retrieving Pay Scale!"); };
                        $.ajax(options);
                    }
                    else {
                        $("#ddlPayScaleType").empty();
                        $("#ddlPayScaleType").prop("disabled", true);
                    }
                });
            });
        //This is for 'House' Binding on the Base of HRA Category
        $(document).ready(function () {
            //$("#ddlHouse").prop("disabled", true);
            $("#ddlHRACat").change(function () {
                if ($("#ddlHRACat").val() != "Please select") {
                    var E = "{HouseCategory: '" + $('#ddlHRACat').val() + "'}";
                    //alert(E);
                    var options = {};
                    options.url = "EmployeeMaster.aspx/BindHouse";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listHouse) {
                        var t = jQuery.parseJSON(listHouse.d);
                        var a = t.length;
                        $("#ddlHouse").empty();
                        if (a >= 0) {
                            $("#txtrentAmount").val('');
                            $("#ddlHouse").append("<option value=''>(Select House)</option>")  
                            $.each(t, function (key, value) {
                                $("#ddlHouse").append($("<option></option>").val(value.HouseID).html(value.HouseName));
                                });
                                }

                       // $("#ddlHouse").prop("disabled", false);
                        };
                            options.error = function () { alert("Error in retrieving House!"); };
                            $.ajax(options);
                        }
                        else {
                    $("#ddlHouse").empty();
                    $("#ddlHouse").prop("disabled", true);
                        }
                });
        });
        //This is for 'Pay Scale' Binding on the Base of Pay Scale Type
        $(document).ready(function () {
            //$("#ddlPayScale").prop("disabled", true);
            $("#ddlPayScaleType").change(function () {

                if ($("#ddlPayScaleType").val() != "Please select") {
                    var E = $("#ddlHRACat").val();
                    var F = $("#ddlPayScaleType").val();
                    var H = $("#ddlHouse").val();
                    if (F == 'O' && H != 0 && E == 'L') {
                        //document.getElementById("txtOldbasic").disabled = false;
                        document.getElementById("txtPCTBasic").disabled = false;
                        //document.getElementById("txtLic").disabled = false;
                    } else {
                        //document.getElementById("txtOldbasic").disabled = true;
                        document.getElementById("txtPCTBasic").disabled = true;
                        //document.getElementById("txtLic").disabled = true;
                    }

                    var options = {};
                    options.url = "EmployeeMaster.aspx/BindPayScale";
                    options.type = "POST";
                    options.data = "{EmpType: '" + $('#ddlPayScaleType').val() + "'}";
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listPayScale) {
                        var t = jQuery.parseJSON(listPayScale.d);
                        var a = t.length;
                        $("#ddlPayScale").empty();
                        if (a > 0) {
                            $("#ddlPayScale").append("<option value='0'>(Select Pay Scale)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlPayScale").append($("<option></option>").val(value.PayScaleID).html(value.PayScale));
                            });
                        } else {
                            $("#ddlPayScale").append("<option value='0'>(Select Pay Scale)</option>")
                        }
                        $("#ddlPayScale").prop("disabled", false);
                    };
                    options.error = function () { alert("Error retrieving Pay Scale!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlPayScale").empty();
                    $("#ddlPayScale").prop("disabled", true);
                }
            });
        });
        $(document).ready(function () {
            //$("#ddlHRA").prop("disabled", true);
            $("#ddlQuatrAlt").change(function () {

                if ($("#ddlEmpType").val() != "Please select") {
                    var B = $("#ddlQuatrAlt").val();
                    if (B == 'N' || B == '') {
                        $("select#ddlHRACat").val('');
                        $("select#ddlHouse").val('');
                        $("#ddlHRA").prop("disabled", false);
                        $("#ddlSpouseQtr").prop("disabled", false);
                        //$("#ddlHouse").prop("disabled", true);
                        $("#ddlHRACat").prop("disabled", true);
                        $("#chkHRA").attr("disabled", false);
                        //$("#txtOldbasic").val('');
                        $("#txtPCTBasic").val('');
                       // $("#txtLic").val('');
                        $("#txtrentAmount").val(''); 
                        //document.getElementById("txtOldbasic").disabled = true;
                        document.getElementById("txtPCTBasic").disabled = true;
                        //document.getElementById("txtLic").disabled = true;
                    }
                    else {
                        $("select#ddlSpouseQtr").val('0');
                        $("#txtSpouseAmt").val('');
                        $("#ddlHouse").prop("disabled", true);
                        //$("#txtOldbasic").prop("disabled", true);
                        $("#txtPCTBasic").prop("disabled", true);
                        $("#ddlHRACat").prop("disabled", false);
                        $("#ddlHRA").val('0');
                        $("#ddlHRA").prop("disabled", true);
                        $("#ddlSpouseQtr").prop("disabled", true);
                        $("#chkHRA").attr("disabled", true);
                        
                    }
                }
            });
        });
        $(document).ready(function () {
            document.getElementById("txtSpouseAmt").disabled = true;
            $("#ddlSpouseQtr").change(function () {

                if ($("#ddlEmpType").val() != "Please select") {
                    var B = $("#ddlSpouseQtr").val();
                    if (B == 'Y') {
                        document.getElementById("txtSpouseAmt").disabled = false;
                    }
                    else {
                        $("#txtSpouseAmt").val('');
                        document.getElementById("txtSpouseAmt").disabled = true;
                    }
                }
            });
        });
        $(document).ready(function () {
            $("#ddlHRACat").change(function () {
                if ($("#ddlHRACat").val() != "Please select") {
                    var E = $("#ddlHRACat").val();
                    var F = $("#ddlPayScaleType").val();
                    var H = $("#ddlHouse").val();

                    if (E == 'G' || E=='V') {
                        //$("#txtOldbasic").val('');
                        $("#txtPCTBasic").val('');
                        $("#txtrentAmount").val('');
                        $("#rdHouse").prop("checked", false);
                        $("#rdBasic").prop("checked", false);
                        document.getElementById("rdHouse").disabled = true;
                        document.getElementById("rdBasic").disabled = true;
                        document.getElementById("txtPCTBasic").disabled = true;
                        document.getElementById("ddlHouse").disabled = true;
                    }
                    else if (E == 'A' && H == 0) {
                        //$("#txtOldbasic").val('');
                        $("#txtPCTBasic").val('');
                        $("#txtrentAmount").val('');
                        $("#rdHouse").prop("checked", false);
                        $("#rdBasic").prop("checked", false);
                        document.getElementById("rdHouse").disabled = false;
                        document.getElementById("rdBasic").disabled = false;
                        document.getElementById("txtPCTBasic").disabled = true;
                        //document.getElementById("txtLic").disabled = true;
                       // alert("Please Select House.");
                       // $("#ddlHouse").focus();
                       // return true;
                    }
                    else if (E == 'L' && H == 0) {
                        //$("#txtOldbasic").val('');
                        $("#txtPCTBasic").val('');
                        $("#txtrentAmount").val('');
                        $("#rdHouse").prop("checked", false);
                        $("#rdBasic").prop("checked", false);
                        document.getElementById("rdHouse").disabled = false;
                        document.getElementById("rdBasic").disabled = false;
                        document.getElementById("txtPCTBasic").disabled = true;
                        //document.getElementById("txtLic").disabled = true;
                       // alert("Please Select House.");
                       //$("#ddlHouse").focus();
                       // return false;
                    }
                    else {
                        //document.getElementById("txtOldbasic").disabled = true;
                        document.getElementById("txtPCTBasic").disabled = true;
                        //document.getElementById("txtLic").disabled = true;
                        //$("#txtOldbasic").val('');
                        $("#txtPCTBasic").val('');
                        //$("#txtLic").val('');
                    }
                }
            });
        });
        $(document).ready(function () {
            $("#ddlHouse").change(function () {
                if ($("#ddlHRACat").val() != "Please select") {
                    var E = $("#ddlPayScaleType").val();
                    var H = $("#ddlHouse").val();
                    var F = $("#ddlHRACat").val();
                    if (F == 'L') {
                        if (H != 0 && E == 'O') {
                            //document.getElementById("txtOldbasic").disabled = false;
                            document.getElementById("txtPCTBasic").disabled = false;
                            //document.getElementById("txtLic").disabled = false;
                        } else {
//                            alert("Please Select Pay Scale Type --Old.");
//                            $("#ddlPayScaleType").focus();
//                            return false;
                            //document.getElementById("txtOldbasic").disabled = true;
                            //document.getElementById("txtPCTBasic").disabled = true;
                            //document.getElementById("txtLic").disabled = true;
                        }
                    }
                }
            });
        });
        //This is for 'Location' Binding on the Base of 'Sector'
        $(document).ready(function () {
            // $("#ddlPayScaleType").prop("disabled", false);
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + "}";
                        //alert(E);
                        var options = {};
                        options.url = "EmployeeMaster.aspx/GetCenter";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listCenter) {
                            var t = jQuery.parseJSON(listCenter.d);
                            var a = t.length;
                            $("#ddlCenter").empty();
                            if (a >= 0) {
                                $("#ddlCenter").append("<option value='0'>(Select Center)</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                                });
                            }

                            $("#ddlCenter").prop("disabled", false);
                        };
                        options.error = function () { alert("Error in retrieving Center!"); };
                        $.ajax(options);
                    }
                    else {
                        $("#ddlCenter").empty();
                        $("#ddlCenter").prop("disabled", true);
                    }
                }
                else {
                    $("#ddlCenter").empty();
                    $("#ddlCenter").append("<option value=''>(Select Center)</option>")
                }
            });

        });
        $(document).ready(function () {
            $("#ddlHealth").change(function () {
                if ($("#ddlHealth").val() != "Please select") {
                    var E = $("#ddlHealth").val();
                    if (E == 'Y') {
                        document.getElementById("txtHealthScheme").disabled = false;
                        document.getElementById("txtEffFrm").disabled = false;
                        document.getElementById("txtEffTo").disabled = false;
                        $("#btnEFDatePicker").prop("disabled", false);
                        $("#btnETDatePicker").prop("disabled", false);
                    }
                    else {
                        document.getElementById("txtHealthScheme").disabled = true;
                        document.getElementById("txtEffFrm").disabled = true;
                        document.getElementById("txtEffTo").disabled = true;
                        $("#btnEFDatePicker").prop("disabled", true);
                        $("#btnETDatePicker").prop("disabled", true);
                    }
                }
            });
        });
        $(document).ready(function () {
            $("#txtOldbasic").change("change", function () {
                var E = $("#txtOldbasic").val();
                if (E == "" || E == null) {
                    //$("#txtOldbasic").val(0);
                    $("#txtPCTBasic").val('');
                    $("#txtrentAmount").val('');
                    //calcAmount();
                }
                else {
                    var hdnHra = $("#ddlHRACat").val(); alert(hdnHra);
                    if (hdnHra == "A")
                        $("#txtPCTBasic").val();
                    else
                        $("#txtPCTBasic").val(0);
                    calcAmount();
                    //document.getElementById("txtLic").disabled = true;
                    //$("#txtLic").val('');
                }
            });
            $("#txtPCTBasic").change("change", function () {
                //var d = $("#txtPCTBasic").val();
                //if (d == "" || d == null) {
                //    $("#txtPCTBasic").val(0);
                //    calcAmount();
                //    //document.getElementById("txtLic").disabled = true;
                //}
                //else {
                //    calcAmount();
                //    //document.getElementById("txtLic").disabled = true;
                //}
            });
            function calcAmount() {
                var oldBasic = $("#txtOldbasic").val();
                var pct = $("#txtPCTBasic").val();
                var cal = (parseFloat(oldBasic) * parseFloat(pct) / 100);
                //var cal = ((parseFloat(oldBasic)) - (parseFloat(oldBasic) * parseFloat(pct) / 100));
                var rcal = Math.round(cal);
                $("#txtrentAmount").val(cal);
            }

        });


    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" runat="Server">
    <div id="main">
        <asp:ScriptManager ID="sm" runat="server" ScriptMode="Release" EnablePageMethods="true"></asp:ScriptManager>
        <table>
            <tr>
                <td>
                    <asp:Literal ID="lt1" runat="server"></asp:Literal>
                    <asp:Literal runat="server" ID="ltlj"></asp:Literal>
                    <asp:HiddenField ID="hdnEmpID" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hdnEDID" ClientIDMode="Static" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnDefaultMode" ClientIDMode="Static" runat="server" Value="Insert" />
                    <asp:HiddenField ID="hdnSearchEmpID" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hdnStatus" ClientIDMode="Static" runat="server" Value="" />
                    <asp:HiddenField ID="hdnPayScaleID" ClientIDMode="Static" runat="server" Value="" />
                    <asp:HiddenField ID="hdHra" ClientIDMode="Static" runat="server" Value="" />
                    <asp:HiddenField ID="hdnPayScaleAmount" ClientIDMode="Static" runat="server" Value="" />
                    <asp:HiddenField ID="hdnItax" ClientIDMode="Static" runat="server" Value="No" />
                    <asp:HiddenField ID="hdnCopSubs" ClientIDMode="Static" runat="server" Value="No" />
                    <asp:HiddenField ID="hdnIsRound" ClientIDMode="Static" runat="server" Value="No" />
                    
                </td>
            </tr>
        </table>
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Employee Master</h2>
                    </section>

                    <div id="dialog" style="display: none; width: 400px; height: 340px;">
                        <%--This is a simple popup--%>
                    </div>

                    <div class="loading-overlay">
                        <div class="loadwrapper">
                            <div class="ajax-loader-outer">Loading...</div>
                        </div>
                    </div>


                    <div class="content-overlay" id='overlay' style="z-index: 49;">
                        <div class="wrapper-outer">
                            <div class="wrapper-inner">
                                <div class="loadContent">
                                    <div id="Close" class="close-content"><a href="javascript:void(0);" id="A1" style="color: red;">Close</a></div>
                                    <div style="float: left;">
                                        <span style="font-size: 20px; font-weight: bold;" class="headFont">Order Details Entry</span>
                                    </div>
                                    <div id="tabEmpdetail" runat="server">
                                        <table width="100%" style="border: solid 2px lightblue; height: auto;">
                                            <tr>
                                                <td style="padding: 3px;">
                                                    <table align="center" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div id="tabs-1" style="float: left; border: solid 2px lightblue; width: 100%; height: auto;">
                                                                    <table align="center" width="100%">
                                                                        <tr>
                                                                            <td colspan="2" style="padding: 3.5px; background: #deedf7;" class="style1">
                                                                                <span class="headFont" style="font-weight: bold;"><font size="3px">Order Details:</font></span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding: 2px; width: 100px;" align="left" class="style1">
                                                                                <span class="headFont">Status :&nbsp;&nbsp;<%--</span><span class="require">*</span>--%>
                                                                            </td>
                                                                            <td style="padding: 1px;" align="left">
                                                                                <asp:TextBox ID="txtStatus" autocomplete="off" ClientIDMode="Static" Enabled="false" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding: 2px; width: 100px;" align="left" class="style1">
                                                                                <span class="headFont">Order No. :&nbsp;&nbsp;<%--</span><span class="require">*</span>--%>
                                                                            </td>
                                                                            <td style="padding: 1px;" align="left">
                                                                                <asp:TextBox ID="txtOrderNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding: 2px;" class="style1">
                                                                                <span class="headFont">Order Date :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                            </td>
                                                                            <td style="padding: 2px;" align="left">
                                                                                <asp:TextBox ID="txtOrderDate" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding: 2px;" class="style1">
                                                                                <span class="headFont">Remarks :&nbsp;&nbsp;</span><span class="require">*</span>
                                                                            </td>
                                                                            <td style="padding: 2px;" align="left">
                                                                                <asp:TextBox ID="txtRemarks" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Width="350px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" style="text-align:center;">
                                                                                <asp:Button ID="btnStatusSave" runat="server" Text="Save" CommandName="Add"
                                                                                    CssClass="Btnclassname DefaultButton" Width="50px" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <table width="100%" style="margin-bottom: 10px; ">

                        <tr >
                            <td style="padding: 5px;"><span class="headFont" style="font-size:16px;">Employee Name / Code.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                            <td style="padding: 5px;">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:TextBox ID="txtEmpCodeSearch" autocomplete="off" ClientIDMode="Static" oncopy="return true" onpaste="return true" oncut="return true"
                                runat="server" MaxLength="50" CssClass="textbox" Width="210px"></asp:TextBox>
                                <div id="AutoComplete-EmployeeNo"></div>
                            </td>

                            <td style="padding: 5px;">
                                <asp:RadioButton ID="rdEmployee" Checked="true" class="headFont" runat="server" onchange='callfunction();' Font-Size="Medium" GroupName="a" Text="Personal Detail" />
                                <asp:RadioButton ID="rdEmployee1" class="headFont" runat="server" onchange='callfunction();' Font-Size="Medium" GroupName="a" Text="Other Detail" />
                            </td>

                            <td style="padding: 5px;">
                                <div style="float: left; margin-left: 200px;">
                                    <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="Search" OnClick="cmdSearch_Click" 
                                        Width="100px" CssClass="Btnclassname" OnClientClick="beforeSave_Search();" />
                                  

                                    <asp:Button ID="cmdCancel" runat="server" Text="Refresh"
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                </div>
                            </td>


                        </tr>

                        <tr>
                            <td align="center" colspan="6">
                                <div align="center">
                                </div>
                            </td>
                        </tr>

                    </table>

                    <div id="tabPersonal" runat="server">
                        <%----------   Personal Tab-------------------------%>

                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <asp:FormView ID="dv" runat="server" Width="99%"
                                        AutoGenerateRows="False"
                                        DefaultMode="Insert" HorizontalAlign="Center"
                                        GridLines="None">
                                        <InsertItemTemplate>
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td colspan="5" style="padding: 10px;">
                                                        <div id="tabs" runat="server" clientidmode="Static">
                                                            <div class="prod-det-tab-out_main">
                                                                <%--class="prod-det-out" ul class="prod-det-tabs"--%>
                                                                <ul>

                                                                    <li>
                                                                        <a href="#tabs-1">Personal Information</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-2">Address Information</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-3">Official Details</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-4">Bank Details</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-5" style="display:none;">Upload Photo and Signature</a>
                                                                    </li>


                                                                </ul>
                                                            </div>

                                                            <div id="tabs-1">
                                                                <%--Personal Information--%>
                                                                <table align="center" width="100%">
<%--                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="EmpCode" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="5"></asp:TextBox>
                                                                    </tr>--%>
                                                                    <tr>

                                                                        <td style="padding: 5px;"><span class="headFont">Employee Code.&nbsp;&nbsp;</span><span class="require">*</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmpNo" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="textbox" MaxLength="5" Enabled="true"></asp:TextBox>
                                                                            <%--<asp:CheckBox ID="chkNewEmp" Text ="New" runat="server" Checked="true" ClientIDMode="Static" /> --%>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Branch &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCenter" autocomplete="off" Width="200px" Height="24px" runat="server" ClientIDMode="Static" AppendDataBoundItems="true">
                                                                                <asp:ListItem Text="(Select Branch)" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee Type &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlEmpType" autocomplete="off" Width="200px" Height="24px" runat="server" ClientIDMode="Static" AppendDataBoundItems="true">
                                                                                <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                                                <asp:ListItem Text="WBFC Employee" Value="K"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Classification&nbsp;&nbsp;<%--</span><span class="require">*</span>--%></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlClassifctn" autocomplete="off" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("Classification") %>'
                                                                                DataValueField="ClassificationID" DataTextField="Classification" AppendDataBoundItems="true" Enabled="false" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Classification)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmpName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" MaxLength="100" Width="350px"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Father's Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left" >
                                                                            <asp:TextBox ID="txtFatherName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" MaxLength="100" Width="350px"></asp:TextBox>
                                                                        </td>

                                                                    </tr>
                                                                   
                                                                    <%--<tr>
                                                            <td style="padding:5px;" ><span class="headFont">Center&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                            <td style="padding:5px;" >:</td>
                                                            <td style="padding:5px;" align="left" >
                                                                <asp:DropDownList ID="ddlCenter" Width="300px" runat="server" DataSource='<%# drpload("Center") %>'
                                                                    DataValueField="CenterID" DataTextField="CenterName" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                    <asp:ListItem Text="(Select Center)" Value=""></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>--%>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Religion&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlReligion" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Religion") %>'
                                                                                DataValueField="ReligionID" DataTextField="Religion" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Religion)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Qualification&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlQualftn" Width="200px" Height="24px" autocomplete="off" runat="server" DataSource='<%#drpload("Qualification") %>'
                                                                                DataValueField="QualificationID" DataTextField="Qualification" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Qualification)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Marital Status&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlMarital" Width="200px" autocomplete="off" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Marital Status)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Married" Value="M"></asp:ListItem>
                                                                                <asp:ListItem Text="Single" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="Widow" Value="W"></asp:ListItem>
                                                                                <asp:ListItem Text="Divorcee" Value="D"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Caste&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCaste" autocomplete="off" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Caste") %>'
                                                                                DataValueField="CasteID" DataTextField="Caste" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Caste)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Birth&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                    <asp:TextBox ID="txtDOB" ClientIDMode="Static" autocomplete="off"  runat="server" CssClass="dpDate textbox"  Width="190px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnDOBDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" /><br /><br />
                                                                                <asp:Label id="lblYear" runat="server" ClientIDMode="Static" Text="" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblYears" runat="server" ClientIDMode="Static" Text="-Years" style="font-weight:bold; "></asp:Label>
                                                                                <asp:Label id="lblMonth" runat="server" ClientIDMode="Static" Text="" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblMonths" runat="server" ClientIDMode="Static" Text="-Months" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblDay" runat="server" ClientIDMode="Static" Text="" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblDa" runat="server" ClientIDMode="Static" Text="-Days" style="font-weight:bold;"></asp:Label>
                                                                </nobr>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Sex&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlSex" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Sex)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                                                                <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                                                                <asp:ListItem Text="Unisex" Value="U"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Branch In Charge&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left" colspan="4">
                                                                            <asp:DropDownList ID="txtBranchInCharge" ClientIDMode="Static" runat="server"  AppendDataBoundItems="true" Width="200px" Height="24px" MaxLength="100" autocomplete="off">
                                                                               <%-- <asp:ListItem Text="(Select)" Value=""></asp:ListItem>--%>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <%--Personal Information--%>

                                                            <div id="tabs-2">
                                                                <%--Address Information--%>
                                                                <table align="center" width="100%">
                                                                    <tr>
                                                                        <td style="padding: 5px;background:#deedf7;"><span class="headFont" style="font-weight:bold;"><font size="3px">Present Contact Details:</font></span></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Address&nbsp;&nbsp;</span><span class="require">*</span></span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td colspan="4" style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtAddress" ClientIDMode="Static"  Height="15px" Width="780px" runat="server" CssClass="textbox UpperCase" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">State&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlState" DataSource='<%# drpload("State") %>'
                                                                                DataValueField="StateId" Height="24px"  Width="220px" ClientIDMode="Static"
                                                                                DataTextField="State" runat="server"
                                                                                AppendDataBoundItems="true" autocomplete="off">
                                                                          <%--<asp:ListItem Text="(Select State)" Value=""></asp:ListItem>--%>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">District&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDistrict" DataSource='<%# drpload("District") %>'
                                                                                DataValueField="DistID" Height="24px" Width="220px"
                                                                                DataTextField="District" runat="server"
                                                                                AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select District)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">City/Town&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtCity" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Pin Code&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPin" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span1"></span>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Phone Number&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPhone" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span2"></span>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Mobile Number&nbsp;&nbsp;</span></span><span class="require">*</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtMobile" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span3"></span>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">E-mail (if any)&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmail" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <%--Permanent Contact Details--%>
                                                                    <tr>
                                                                        <td colspan="2" style="padding: 5px; background: #deedf7;"><span class="headFont" style="font-weight: bold;"><font size="3px">Permanent Contact Details:</font></span></td>
                                                                   <td>
                                                                    <asp:CheckBox ID="ChkDo" Text ="Do" runat="server" ClientIDMode="Static" /> 
                                                                   </td>
                                                                         </tr>


                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Address&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td colspan="4" style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtAddress_P" ClientIDMode="Static"  Height="15px" Width="780px" runat="server" CssClass="textbox UpperCase" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">State&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlState_P" DataSource='<%# drpload("State") %>'
                                                                                DataValueField="StateId" Height="24px" Width="220px"
                                                                                DataTextField="State" runat="server"
                                                                                AppendDataBoundItems="true" autocomplete="off">
                                                                                <%--<asp:ListItem Text="(Select State)" Value=""></asp:ListItem>--%>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">District&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDistrict_P" DataSource='<%# drpload("District") %>'
                                                                                DataValueField="DISTID" Height="24px" Width="220px"
                                                                                DataTextField="DISTRICT" runat="server"
                                                                                AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Select District)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">City/Town&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtCity_P" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Pin Code&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPin_P" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span4"></span>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Phone Number&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPhone_P" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span5"></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Remarks&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td colspan="4" style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtAddressRemarks" Width="780px" TextMode="multiline" Columns="50" Rows="5" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span6"></span>

                                                                        </td>

                                                                    </tr>



                                                                </table>
                                                            </div>
                                                            <%--Address Information--%>

                                                            <div id="tabs-3">
                                                                <%--Employee Official Details--%>
                                                                <table align="center" width="100%">
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Joining (DOJ)&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                                <nobr>
                                                                                    <asp:TextBox ID="txtDOJ" Width="190px" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" autocomplete="off"></asp:TextBox>
                                                                                    <asp:ImageButton ID="btnDOJDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                                                </nobr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:Button ID="btnDo" runat="server" Text="Do" BackColor="#deedf7"  ClientIDMode="Static" CssClass="DefaultButton" />
                                                                        </td>
                                                                       
                                                                        <td style="padding: 5px;"><span class="headFont">DOJ Present Dept.&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                                <nobr>
                                                                                    <asp:TextBox ID="txtDOJDept" Width="190px" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" autocomplete="off"></asp:TextBox>
                                                                                    <asp:ImageButton ID="btnDOJPDDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                                                </nobr>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Release(DOR)&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                <asp:TextBox ID="txtDOR" ClientIDMode="Static" Width="190px" Enabled="false" runat="server" CssClass="dpDate textbox" autocomplete="off"></asp:TextBox>
                                                <%--<asp:ImageButton ID="btnDORDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />--%>
                                            </nobr>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Physically Challenged&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlPhysical" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Please Select)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Designation&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDesig" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Designation") %>'
                                                                                DataValueField="DesignationID" DataTextField="Designation" ClientIDMode="Static" AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Designation)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Class&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlGroup" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Group") %>'
                                                                                DataValueField="GroupID" DataTextField="GroupName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Class)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Cooperative&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCoop" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Cooperative") %>'
                                                                                DataValueField="CooperativeID" DataTextField="Cooperative" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Cooperative)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <%--<td style="padding: 5px;"><span class="headFont">Category&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCategory" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("Category") %>'
                                                                                DataValueField="CategoryID" DataTextField="Category" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Category)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>--%>
                                                                       <%-- //****************************Sup*****************************--%>
                                                                        <td style="padding: 5px;"><span class="headFont">Department&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDepartment" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("Department") %>'
                                                                                DataValueField="DepartmentID" DataTextField="Department" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Department)" Value="S"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <%--//************************************************************--%>
                                                                    </tr>
                                                                    <%--//******************Sup*************************************--%>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employer PF Rate&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmployerPfRate" ClientIDMode="Static" runat="server" CssClass="textbox" Enabled="true" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee PF Rate&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
<asp:DropDownList ID="txtEmployeePfRate" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("PFRate") %>' AppendDataBoundItems="true" ClientIDMode="Static"  DataValueField="PFRate" DataTextField="PFRate" autocomplete="off">
               <asp:ListItem Text="(Select PF Rate)" Value="S"></asp:ListItem>
      </asp:DropDownList>
<%--                                                                            <asp:TextBox ID="txtEmployeePfRate" ClientIDMode="Static" runat="server" CssClass="textbox" Enabled="true"></asp:TextBox>--%>
                                                                        </td>
                                                                    </tr>
                                                                    <%--//**********************************************************--%>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">DA&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDA" Width="90px" Height="24px" runat="server" DataSource='<%#drpload("DA") %>'
                                                                                DataValueField="DAID" DataTextField="DARate" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:CheckBox ID="chkDA" runat="server" Text="Not Applicable" CssClass="headFont" />                                                                     </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Status&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlStatus" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true"  ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Status)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Active" Selected="True" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="Transferred" Value="T"></asp:ListItem>
                                                                                <asp:ListItem Text="Retired" Value="R"></asp:ListItem>
                                                                                <asp:ListItem Text="Resign" Value="N"></asp:ListItem>
                                                                                <asp:ListItem Text="Suspended" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="Withheld" Value="W"></asp:ListItem>
                                                                                <asp:ListItem Text="Attachment" Value="A"></asp:ListItem>
                                                                                <asp:ListItem Text="Death" Value="D"></asp:ListItem>
                                                                                <asp:ListItem Text="Re-Employeed on Contract" Value="P"></asp:ListItem>
                                                                                <asp:ListItem Text="VRS" Value="V"></asp:ListItem>
                                                                                <asp:ListItem Text="Invalid" Value="I"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">PF Code&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPFCode" ClientIDMode="Static" Width="190px" runat="server" CssClass="textbox" MaxLength="100" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                         <td style="padding: 5px;"><span class="headFont">Pay Scale Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlPayScaleType" Width="200px" Height="24px" runat="server"  DataValueField="PayScaleID" DataTextField="PayScale"
                                                                                AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Pay Scale Type)" Value="0"></asp:ListItem>
<%--                                                                                <asp:ListItem Text="State" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="Central" Value="C"></asp:ListItem>
                                                                                <asp:ListItem Text="Old" Value="O"></asp:ListItem>--%>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Pay Scale&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlPayScale" Width="200px" Height="24px" runat="server" 
                                                                                DataValueField="PayScaleID" DataTextField="PayScale" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Pay Scale)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <%--td style="padding: 5px;"><span class="headFont">Grade Pay&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">--%>
                                                                            <%--<asp:TextBox ID="txtGradePay" Width="90px" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10"></asp:TextBox>
                                                                            <asp:CheckBox ID="chkGradePay" runat="server" Text="Not Applicable" CssClass="headFont" />
                                                                        </td>--%>
                                                                        <td style="padding: 5px;"><span class="headFont">Co-Op MemberShip&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtCoMembership" Width="90px" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Next Increment&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                                   <asp:TextBox ID="txtDNI" ClientIDMode="Static" Width="85px" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                         <%--<asp:ImageButton ID="btnDNIDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />--%>
                                                                            </nobr>
                                                                        </td>

                                                                        <td style="padding: 5px;"><span class="headFont">Pan No.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPAN" ClientIDMode="Static" runat="server" CssClass="textbox" Width="190px" autocomplete="off"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="regExptxtPanNo" runat="server" ControlToValidate="txtPan" ErrorMessage="Invalid PAN" ValidationExpression="[A-Za-z]{5}\d{4}[A-Za-z]{1}" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                     <tr style="display:none">
                                                                        <td style="padding: 5px;"><span class="headFont">Select PF&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" colspan="4">
                                                                             <asp:DropDownList ID="ddlPFRate" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("PFRate") %>'
                                                                                  AppendDataBoundItems="true" ClientIDMode="Static"  DataValueField="PFRate" DataTextField="PFRate" autocomplete="off">  
                                                                                <asp:ListItem Text="(Select PF Rate)" Value="S"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                
                                                                        <td>
                                                                         <asp:CheckBox ID="chkHRA" runat="server" Text="Fixed HRA" CssClass="headFont" />
                                                                        </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtmHRA" runat="server" Width="70px" autocomplete="off"  ></asp:TextBox>
                                                                            <asp:Label ID="lblHra" runat="server" CssClass="headFont"  Text="(Enter HRA)"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td style="padding: 5px;"><span class="headFont">Quarter Allot&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlQuatrAlt" Width="200px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Quarter)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">HRA&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHRA" Width="200px" runat="server" DataSource='<%#drpload("HRA") %>'
                                                                                DataValueField="HRAID" DataTextField="HRA" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select HRA)" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>

                                                                    </tr>
                                                                     <tr>
                                                                      <td style="padding: 5px;"><span class="headFont">Spouse HRA&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlSpouseQtr" Width="200px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Spouse Quarter)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                       <td style="padding: 5px;"><span class="headFont">Spouse HRA Amount&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtSpouseAmt" ClientIDMode="Static" runat="server" CssClass="textbox"  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                   </tr>
                                                                   <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;">
                                                                            <span class="headFont">HRA Category&nbsp;&nbsp;</span>
                                                                        </td>
                                                                        <td style="padding: 5px;">
                                                                            :
                                                                        </td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHRACat" Width="200px" runat="server" AppendDataBoundItems="true"
                                                                                ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select HRA Category)" Value=""></asp:ListItem>
                                                                                 <asp:ListItem Text="Govt Quarter" Value="G"></asp:ListItem>
                                                                                <asp:ListItem Text="KMDA Quarter" Value="G"></asp:ListItem>
                                                                                <asp:ListItem Text="Assessed Rent" Value="A"></asp:ListItem>
                                                                                <asp:ListItem Text="Licence Type" Value="L"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        
                                                                        <td style="padding: 5px;">
                                                                            <asp:RadioButton ID="rdHouse"  class="headFont" runat="server" onchange='callfunction();'
                                                                                Font-Size="Medium" GroupName="a" Text="House" />
                                                                            <asp:RadioButton ID="rdBasic" class="headFont" runat="server" onchange='callHouseBasic();'
                                                                                Font-Size="Medium" GroupName="a" Text="Basic" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <div id="tabHouse" runat="server" clientidmode="Static">
                                                                        <td style="padding: 5px;">
                                                                            <span class="headFont">House&nbsp;&nbsp;</span>
                                                                        </td>
                                                                        <td style="padding: 5px;">
                                                                            :
                                                                        </td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHouse" Width="200px" runat="server" DataSource='<%#drpload("House") %>'
                                                                                DataValueField="HouseID" DataTextField="HouseName" AppendDataBoundItems="true"
                                                                                ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select House)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            
                                                                        </td>
                                                                      </div>  
                                                                    </tr>
                                                                       
                                                                    <tr>
                                                                    <div id="tabBasic" runat="server" clientidmode="Static">
                                                                      <td style="padding: 5px;"><span class="headFont">PCT Basic&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPCTBasic" ClientIDMode="Static" Width="40px" runat="server"  CssClass="textbox"  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                            <span class="headFont">%</span>
                                                                        </td>
                                                                       </div>
                                                                       <td style="padding: 5px;"><span class="headFont">Rent Amount&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                        <asp:TextBox ID="txtrentAmount" runat="server" CssClass="textbox" Height="12px" Width="70px" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <%--<td style="padding: 5px;"><span class="headFont">Licence Fees&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtLic" ClientIDMode="Static" runat="server" CssClass="textbox" Height="12px" Width="70px" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                        </td>--%>
                                                                    </tr>
                                                                   
                                                                        

                                                                   
                                                                       <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Mediclaim&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHealth" Width="200px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Mediclaim)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Selected="True" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                             <asp:CheckBox ID="chkHealthScheme" runat="server" Text="Not Applicable" CssClass="headFont" /> 
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Mediclaim Details&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtHealthScheme" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Effective From&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                <asp:TextBox ID="txtEffFrm" ClientIDMode="Static" runat="server" CssClass="dpDate inputbox2" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnEFDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Effective To&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                <asp:TextBox ID="txtEffTo" ClientIDMode="Static" runat="server" CssClass="dpDate inputbox2" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnETDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                       <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                       <td style="padding: 5px;"><span class="headFont">LWP&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtLWP" ClientIDMode="Static" runat="server" Width="40px" CssClass="textbox" MaxLength="5" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                        <asp:Label ID="lblDays" runat="server" Text="Days" CssClass="headFont"></asp:Label>
                                                                        </td>

                                                                        <td style="padding: 5px;"><span class="headFont">IR&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlIR" Width="200px" runat="server" DataSource='<%#drpload("IR") %>'
                                                                                DataValueField="IRID" DataTextField="IRRate" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select IR)" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                             <asp:CheckBox ID="chkIR" runat="server" Text="Not Applicable" CssClass="headFont" /> 
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <%--Official Details--%>

                                                            <div id="tabs-4">
                                                                <%--Bank Details--%>
                                                                <table align="center" width="100%">
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlBank" Width="200px" Height="22px" runat="server" DataSource='<%#drpload("Bank") %>' DataValueField="BankID" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Branch Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlBranch" Width="200px" Height="22px" runat="server" DataSource='<%#drpload("Branch") %>' DataValueField="BankID" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off"> 
                                                                                <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                   <td style="padding: 5px;"><span class="headFont">IFSC Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtIFSC" ClientIDMode="Static" runat="server" Width="192px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">MICR No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtMICR" ClientIDMode="Static" runat="server" Width="192px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td style="padding: 5px;"><span class="headFont">Bank Account No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtBankCode" ClientIDMode="Static" runat="server" Width="192px" CssClass="textbox" MaxLength="20" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <%--Bank Details--%>

                                                            <div id="tabs-5">
                                                                <%--Upload Photo and Signature--%>
                                                                 <table align="center" id='table1' cellpadding="5" width="100%">
                                                            <tr>
                                                                <td class="headFont">Upload Photo -&nbsp;&nbsp;<span class="requirefield">*</span>
                                                                Size between 20KB to 50KB (Dimension 138 W X 177 H)  (4.5 cm Height x 3.5 cm Width)</td>
                                                                
                                                                <td class="labelCaptionbold">:</td>
                                                                <td align="left">
                                                                    <div style="float:left;width:540px;" >
                                                                        <div style="float:left;width:350px;">
                                                                            <input id="fileToUpload" type="file" runat="server"  name="fileToUpload" class="headFont" />
                                                                            <button id="buttonUpload" class="button01 DefaultButton">Upload</button>
                                                                            <img id="loading" src="images/ajax-loader_new.gif" style="display: none;">
                                                                            <span id="imgPhoto" style="width: 26px; height: 24px;"></span>
                                                                            <asp:TextBox ID="lblPhoto" runat="server" Style="cursor: not-allowed;" CssClass="text_field01" ReadOnly="true" Text="" autocomplete="off"></asp:TextBox>
                                                                        </div>
                                                                        <div style="float:left;">
                                                                            <div id="canPhoto" style="width:100px;height: 116px;border: solid 1px #6d760b;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class="headFont"> Upload Full Signature -&nbsp;&nbsp;<span class="requirefield">*</span>
                                                                    Size between 10 KB to 20KB (Dimension 250 W X 45 H) (1.1cm Height X 6.6 cm Width)
                                                                </td>
                                                                <td class="labelCaptionbold">:</td>
                                                                <td align="left">
                                                                    <div style="float:left;width:540px;" >
                                                                        <div style="float:left;width:350px;">
                                                                            <input id="fileToUploadSign" type="file"  name="fileToUploadSign" class="headFont" />
                                                                            <button id="buttonUploadSign" class="button01 DefaultButton">Upload</button>
                                                                            <img id="loadingSign" src="images/ajax-loader_new.gif" style="display: none;">
                                                                            <span id="imgSign" style="width: 26px; height: 24px;"></span>                                                                    
                                                                            <asp:TextBox ID="lblSign" runat="server" Style="cursor: not-allowed;" CssClass="text_field01" ReadOnly="true" Text="" autocomplete="off"></asp:TextBox>
                                                                        </div>
                                                                        <div style="float:left;">
                                                                            <div id="canSign" style="width:150px;height: 27px;border: solid 1px #6d760b;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                                </table>
                                                            </div>
                                                            <%--Upload Photo and Signature--%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </InsertItemTemplate>

                                        <EditItemTemplate>
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td colspan="5" style="padding: 10px;">
                                                        <div id="tabs" runat="server" clientidmode="Static">
                                                            <div class="prod-det-tab-out_main">
                                                                <%--class="prod-det-out" ul class="prod-det-tabs"--%>
                                                                <ul>
                                                                    <li>
                                                                        <a href="#tabs-1">Personal Information</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-2">Address Information</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-3">Official Details</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-4">Bank Details</a>
                                                                    </li>
                                                                    <li style="display:none;">
                                                                        <a href="#tabs-5">Upload Photo and Signature</a>
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                           <div id="tabs-1">
                                                                <%--Personal Information--%>
                                                                <table align="center" width="100%">
<%--                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="EmpCode" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="5"></asp:TextBox>
                                                                    </tr>--%>
                                                                    <tr>

                                                                        <td style="padding: 5px;"><span class="headFont">Employee Code.&nbsp;&nbsp;<%--</span><span class="require">*</span>--%></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmpNo" ClientIDMode="Static" Width="190px" runat="server" CssClass="textbox" MaxLength="6" Text='<%# Eval("EmpNo") %>' Enabled="false" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Branch &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCenter" Width="200px" Height="25px" runat="server" 
                                                                                DataTextField="CenterName" DataValueField="CenterID" ClientIDMode="Static" AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Branch)" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee Type &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlEmpType" Width="200px" Height="25px" runat="server" ClientIDMode="Static" AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                                                <asp:ListItem Text="WBFC Employee" Value="K"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Classification&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlClassifctn" Width="200px" Height="25px" runat="server" DataSource='<%# drpload("Classification") %>'
                                                                                DataValueField="ClassificationID" DataTextField="Classification" AppendDataBoundItems="true" Enabled="false" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Classification)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmpName" ClientIDMode="Static"  runat="server" CssClass="textbox UpperCase" MaxLength="100" Width="350px" Text='<%# Eval("EmpName") %>' autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Father's Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtFatherName" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" MaxLength="100" Width="350px" autocomplete="off"></asp:TextBox>
                                                                        </td> 
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Religion&nbsp;&nbsp; </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlReligion" Width="200px" Height="25px" runat="server" DataSource='<%#drpload("Religion") %>'
                                                                                DataValueField="ReligionID" DataTextField="Religion" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Religion)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Qualification&nbsp;&nbsp; </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlQualftn" Width="200px" Height="25px" runat="server" DataSource='<%#drpload("Qualification") %>'
                                                                                DataValueField="QualificationID" DataTextField="Qualification" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Qualification)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Marital Status&nbsp;&nbsp; </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlMarital" Width="200px" Height="25px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Marital Status)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Married" Value="M"></asp:ListItem>
                                                                                <asp:ListItem Text="Single" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="Window" Value="W"></asp:ListItem>
                                                                                <asp:ListItem Text="Divorcee" Value="D"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Caste&nbsp;&nbsp; </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCaste" Width="200px" Height="25px" runat="server" DataSource='<%#drpload("Caste") %>'
                                                                                DataValueField="CasteID" DataTextField="Caste" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Caste)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Birth&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                    <asp:TextBox ID="txtDOB" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox" Text='<%# Bind("DOB", "{0:dd/MM/yyyy}") %>' autocomplete="off"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnDOBDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" /><br /><br />
                                                                                 <asp:Label id="lblYear" runat="server" ClientIDMode="Static" Text="" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblYears" runat="server" ClientIDMode="Static" Text="-Years" style="font-weight:bold; "></asp:Label>
                                                                                <asp:Label id="lblMonth" runat="server" ClientIDMode="Static" Text="" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblMonths" runat="server" ClientIDMode="Static" Text="-Months" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblDay" runat="server" ClientIDMode="Static" Text="" style="font-weight:bold;"></asp:Label>
                                                                                <asp:Label id="lblDa" runat="server" ClientIDMode="Static" Text="-Days" style="font-weight:bold;"></asp:Label>
                                                                </nobr>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Sex&nbsp;&nbsp; </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlSex" Width="200px" Height="25px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Sex)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                                                                <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                                                                <asp:ListItem Text="Unisex" Value="U"></asp:ListItem>

                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Branch In Charge&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left" colspan="4">
                                                                            <asp:DropDownList ID="txtBranchInCharge" ClientIDMode="Static" runat="server"  AppendDataBoundItems="true" Width="200px" Height="24px" MaxLength="100" autocomplete="off">
                                                                                <%--<asp:ListItem Text="(Select)" Value="S"></asp:ListItem>--%>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                            </asp:DropDownList>                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            <div id="tabs-2">
                                                                <%--Address Information--%>
                                                                <table align="center" width="100%">
                                                                    <tr>
                                                                        <td style="padding:5px;background:#deedf7;"><span class="headFont" style="font-weight:bold;"><font size="3px">Present Contact Details:</font></span></td>
                                                                    </tr>
                                                                    <%--<tr>
                                                            <td style="padding:5px;" ><span class="headFont">Employee Address&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                            <td style="padding:5px;" >:</td>
                                                            <td style="padding:5px;" align="left" >
                                                                <asp:TextBox ID="txtEmpAdd" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>--%>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Address&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td colspan="4" style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtAddress" ClientIDMode="Static" TextMode="MultiLine" Height="15px" Width="780px" runat="server" CssClass="textbox UpperCase" Text='<%# Eval("EmpAddress") %>' autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">State&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlState"  
                                                                                DataValueField="StateId" Height="24px" Width="220px" ClientIDMode="Static"
                                                                                DataTextField="StateName" runat="server"
                                                                                AppendDataBoundItems="true" autocomplete="off">
                                                                          <asp:ListItem Text="(Select State)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">District&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDistrict" 
                                                                                DataValueField="DistID" Height="24px" Width="220px"
                                                                                DataTextField="District" runat="server"
                                                                                AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select District)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">City/Town&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtCity" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Text='<%# Eval("City") %>' autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Pin Code&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPin" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("Pin") %>' autocomplete="off"></asp:TextBox>
                                                                            <span id="Span1"></span>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Phone Number&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPhone" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("Phone") %>' autocomplete="off"></asp:TextBox>
                                                                            <span id="Span2"></span>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Mobile Number&nbsp;&nbsp;</span></span><span class="require">*</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtMobile" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("Mobile") %>' MaxLength="10" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span3"></span>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">E-mail (if any)&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmail" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("Email") %>' autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <%--Permanent Contact Details--%>

                                                                    <tr>
                                                                        <td colspan="2" colspan="5" style="padding:5px;background:#deedf7;"><span class="headFont" style="font-weight:bold;"><font size="3px">Permanent Contact Details:</font></span></td>
                                                                    <td>
                                                                    <asp:CheckBox ID="ChkDo" Text ="Do" runat="server" ClientIDMode="Static" /> 
                                                                   </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Address&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td colspan="4" style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtAddress_P" ClientIDMode="Static" TextMode="MultiLine" Height="15px" Width="780px" runat="server" CssClass="textbox UpperCase" Text='<%# Eval("PermAddress") %>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">State&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlState_P" 
                                                                                DataValueField="StateId" Height="24px"  Width="220px"
                                                                                DataTextField="StateName" runat="server"
                                                                                AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Select State)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">District&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDistrict_P" 
                                                                                DataValueField="DISTID" Height="24px"  Width="220px"
                                                                                DataTextField="DISTRICT" runat="server"
                                                                                AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Select District)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">City/Town&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtCity_P" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Text='<%# Eval("PermCity") %>' autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Pin Code&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPin_P" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("PermPin") %>' autocomplete="off"></asp:TextBox>
                                                                            <span id="Span4"></span>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Phone Number&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPhone_P" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("PermPhone") %>' autocomplete="off"></asp:TextBox>
                                                                            <span id="Span5"></span>
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Remarks&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td colspan="4" style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtAddressRemarks" Width="780px" TextMode="multiline" Columns="50" Rows="5" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                            <span id="Span6"></span>
                                                                        </td>

                                                                    </tr>



                                                                </table>
                                                            </div>

                                                            <div id="tabs-3">
                                                                <%--Employee Official Details--%>
                                                                <table align="center" width="100%">
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Joining (DOJ)&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                               <asp:TextBox ID="txtDOJ" Width="190px" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" Text='<%# Bind("DOJ", "{0:dd/MM/yyyy}") %>' autocomplete="off"></asp:TextBox>
                                                                               <asp:ImageButton ID="btnDOJDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                                AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:Button ID="btnDo" runat="server" Text="Do" BackColor="#deedf7"  ClientIDMode="Static" CssClass="DefaultButton" />
                                                                           </nobr>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">DOJ Present Dept.&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                               <asp:TextBox ID="txtDOJDept" Width="190px" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" Text='<%# Bind("DOJPresentDept", "{0:dd/MM/yyyy}") %>' autocomplete="off" ></asp:TextBox>
                                                                               <asp:ImageButton ID="btnDOJPDDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                                            </nobr>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Release(DOR)&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                                <asp:TextBox ID="txtDOR" Width="190px" ClientIDMode="Static" runat="server" Enabled="false" CssClass="dpDate textbox" Text='<%# Bind("DOR", "{0:dd/MM/yyyy}") %>' autocomplete="off"></asp:TextBox>
<%--                                                                                <asp:ImageButton ID="btnDORDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />--%>
                                                                            </nobr>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Physically Challenged&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlPhysical" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Please Select)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Designation&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDesig" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Designation") %>'
                                                                                DataValueField="DesignationID" DataTextField="Designation" ClientIDMode="Static" AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Designation)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Class&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlGroup" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Group") %>'
                                                                                DataValueField="GroupID" DataTextField="GroupName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Class)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Cooperative&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCoop" Width="200px" Height="24px" runat="server" DataSource='<%#drpload("Cooperative") %>'
                                                                                DataValueField="CooperativeID" DataTextField="Cooperative" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Cooperative)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <%--<td style="padding: 5px;"><span class="headFont">Category&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlCategory" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("Category") %>'
                                                                                DataValueField="CategoryID" DataTextField="Category" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Category)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>--%>
                                                                         <%-- //****************************Sup*****************************--%>
                                                                        <td style="padding: 5px;"><span class="headFont">Department&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDepartment" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("Department") %>'
                                                                                DataValueField="DepartmentID" DataTextField="Department" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Department)" Value="S"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <%--//************************************************************--%>
                                                                    </tr>
                                                                     <%--//******************Sup*************************************--%>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Employer PF Rate&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtEmployerPfRate" ClientIDMode="Static" runat="server" CssClass="textbox" Enabled="false" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Employee PF Rate&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="txtEmployeePfRate" Width="200px" Height="24px" runat="server" Enabled="true" DataSource='<%# drpload("PFRate") %>'
                                                                                  AppendDataBoundItems="true" ClientIDMode="Static"  DataValueField="PFRate" DataTextField="PFRate" autocomplete="off">
                                                                                <asp:ListItem Text="(Select PF Rate)" Value="S"></asp:ListItem>
                                                                            </asp:DropDownList>
<%--                                                                            <asp:TextBox ID="txtEmployeePfRate" ClientIDMode="Static" runat="server" CssClass="textbox" Enabled="false"></asp:TextBox>--%>
                                                                        </td>
                                                                    </tr>
                                                                    <%--//**********************************************************--%>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">DA&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlDA" Width="100px" Height="24px" runat="server" 
                                                                                DataValueField="DAID" DataTextField="DARate" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select DA)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:CheckBox ID="chkDA" runat="server" Text="Not Applicable" CssClass="headFont" />                                                                     </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Status&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlStatus" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Status)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Active" Selected="True" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="Transferred" Value="T"></asp:ListItem>
                                                                                <asp:ListItem Text="Retired" Value="R"></asp:ListItem>
                                                                                <asp:ListItem Text="Resign" Value="N"></asp:ListItem>
                                                                                <asp:ListItem Text="Suspended" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="Withheld" Value="W"></asp:ListItem>
                                                                                <asp:ListItem Text="Attachment" Value="A"></asp:ListItem>
                                                                                <asp:ListItem Text="Death" Value="D"></asp:ListItem>
                                                                                <asp:ListItem Text="Re-Employeed on Contract" Value="P"></asp:ListItem>
                                                                                <asp:ListItem Text="VRS" Value="V"></asp:ListItem>
                                                                                <asp:ListItem Text="Invalid" Value="I"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">PF Code&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPFCode" ClientIDMode="Static" Width="190px" textbox runat="server" CssClass="textbox"  Text='<%# Eval("PFCode") %>' autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                         <td style="padding: 5px;"><span class="headFont">Pay Scale Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlPayScaleType" Width="200px" runat="server"  Height="24px"
                                                                                AppendDataBoundItems="true" ClientIDMode="Static" DataValueField="PayScaleID" DataTextField="PayScale" autocomplete="off" >
                                                                                <asp:ListItem Text="(Select Pay Scale Type)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Pay Scale&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlPayScale" Width="200px" runat="server"  Height="24px"
                                                                                DataValueField="PayScaleID" DataTextField="PayScale" Enabled="false" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Pay Scale)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Co-Op MemberShip&nbsp;&nbsp;</span></td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtCoMembership" Width="90px" Text='<%# Eval("CoopMembership") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Date of Next Increment&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                                   <asp:TextBox ID="txtDNI" Width="90px" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" Text='<%# Bind("DNI", "{0:dd/MM/yyyy}") %>' autocomplete="off"></asp:TextBox>
                                                                                     <%--<asp:ImageButton ID="btnDNIDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />--%>
                                                                            </nobr>
                                                                        </td>

                                                                        <td style="padding: 5px;"><span class="headFont">Pan No.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPAN" ClientIDMode="Static" runat="server" CssClass="textbox" Width="190px" Text='<%# Eval("PAN") %>' autocomplete="off"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="regExptxtPanNo" runat="server" ControlToValidate="txtPan" ErrorMessage="Invalid PAN" ValidationExpression="[A-Za-z]{5}\d{4}[A-Za-z]{1}" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display:none">
                                                                        <td style="padding: 5px; "><span class="headFont">Select PF&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding:5px;" colspan="4">
                                                                             <asp:DropDownList ID="ddlPFRate" Width="200px" Height="24px" runat="server" DataSource='<%# drpload("PFRate") %>'
                                                                                  AppendDataBoundItems="true" ClientIDMode="Static"  DataValueField="PFRate" DataTextField="PFRate" autocomplete="off">
                                                                                <asp:ListItem Text="(Select PF Rate)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                   
                                                                    <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                
                                                                        <td>
                                                                         <asp:CheckBox ID="chkHRA" runat="server" Text="Fixed HRA" CssClass="headFont" />
                                                                        </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtmHRA" runat="server" CssClass="textbox" Width="80px"  autocomplete="off" ></asp:TextBox>
                                                                            <asp:Label ID="lblHra" runat="server" CssClass="headFont" Text="(Enter HRA)"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td style="padding: 5px;"><span class="headFont">Quarter Allot&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px; align="left">
                                                                            <asp:DropDownList ID="ddlQuatrAlt" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Quarter)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">HRA&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHRA" Width="200px" Height="24px" runat="server" 
                                                                                DataValueField="HRAID" DataTextField="HRA" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select HRA)" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>

                                                                    </tr>
                                                                     <tr>
                                                                      <td style="padding: 5px;"><span class="headFont">Spouse HRA&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlSpouseQtr" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Spouse Quarter)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                       <td style="padding: 5px;"><span class="headFont">Spouse HRA Amount&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtSpouseAmt" ClientIDMode="Static" runat="server" Text='<%# Eval("SpouseAmount") %>' CssClass="textbox" Width="190px" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                   </tr>
                                                                   <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px grey" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px grey" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px grey" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px grey" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px grey" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px grey" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">HRA Category&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHRACat" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select HRA Category)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Govt Quarter" Value="V"></asp:ListItem>
                                                                                <asp:ListItem Text="KMDA Quarter" Value="G"></asp:ListItem>
                                                                                <asp:ListItem Text="Assessed Rent" Value="A"></asp:ListItem>
                                                                                <asp:ListItem Text="Licence Type" Value="L"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;">
                                                                            <asp:RadioButton ID="rdHouse"  class="headFont" runat="server" onchange='callfunction();'
                                                                                Font-Size="Medium" GroupName="a" Text="House" />
                                                                            <asp:RadioButton ID="rdBasic" class="headFont" runat="server" onchange='callHouseBasic();'
                                                                                Font-Size="Medium" GroupName="a" Text="Basic" />
                                                                        </td>

                                                                        <%--<td style="padding: 5px;"><span class="headFont">House&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHouse" Width="200px" runat="server" 
                                                                                DataValueField="HouseID" DataTextField="HouseName" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select House)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:TextBox ID="txtrentAmount" runat="server" CssClass="textbox"  Height="12px" Width="50px"></asp:TextBox>
                                                                        </td>--%>
                                                                    </tr>

                                                                    <tr>
                                                                    <div id="tabHouse" runat="server" clientidmode="Static">
                                                                        <td style="padding: 5px;">
                                                                            <span class="headFont">House&nbsp;&nbsp;</span>
                                                                        </td>
                                                                        <td style="padding: 5px;">
                                                                            :
                                                                        </td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHouse" Width="200px" Height="24px" runat="server" 
                                                                                DataValueField="HouseID" DataTextField="HouseName" AppendDataBoundItems="true"
                                                                                ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select House)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            
                                                                        </td>
                                                                      </div>  
                                                                    </tr>
                                                                       
                                                                  <tr>
                                                                    <div id="tabBasic" runat="server" clientidmode="Static">
                                                                        <td style="padding: 5px;"><span class="headFont">PCT Basic&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtPCTBasic" ClientIDMode="Static" Text='<%# Eval("PCTBasic") %>' Width="40px" runat="server"  CssClass="textbox"  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                            <span class="headFont">%</span>
                                                                        </td>
                                                                       </div>
                                                                       <td style="padding: 5px;"><span class="headFont">Rent Amount&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                        <asp:TextBox ID="txtrentAmount" runat="server" CssClass="textbox" Height="12px" Width="70px" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <%--<td style="padding: 5px;"><span class="headFont">Licence Fees&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtLic" ClientIDMode="Static" runat="server" CssClass="textbox" Height="12px" Width="70px" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                        </td>--%>
                                                                    </tr>
                                                                       <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Mediclaim&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlHealth" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Mediclaim)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:CheckBox ID="chkHealthScheme" runat="server" Text="Not Applicable" CssClass="headFont" /> 
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Mediclaim Details&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtHealthScheme" ClientIDMode="Static" Width="190px" Text='<%# Eval("HealthSchemeDetail") %>' runat="server" CssClass="textbox" autocomplete="off" ></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Effective From&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                                <asp:TextBox ID="txtEffFrm" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"  Text='<%# Bind("EffectiveFrom", "{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                                                                <asp:ImageButton ID="btnEFDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                                            </nobr>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Effective To&nbsp;&nbsp;</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <nobr>
                                                                                <asp:TextBox ID="txtEffTo" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox" Text='<%# Bind("EffectiveTo", "{0:dd/MM/yyyy}") %>' autocomplete="off"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnETDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                                            </nobr>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                       <tr>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                        <td>
                                                                            <hr style="border: solid 1px lightblue" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                       <td style="padding: 5px;"><span class="headFont">LWP&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtLWP" ClientIDMode="Static" Text='<%# Eval("LWP") %>' runat="server" Width="40px" CssClass="textbox" MaxLength="5" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                        <asp:Label ID="lblDays" runat="server" Text="Days" CssClass="headFont"></asp:Label>
                                                                        </td>

                                                                        <td style="padding: 5px;"><span class="headFont">IR&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlIR" Width="200px" runat="server" 
                                                                                DataValueField="IRID" DataTextField="IRRate" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off" >
                                                                                <asp:ListItem Text="(Select IR)" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                             <asp:CheckBox ID="chkIR" runat="server" Text="Not Applicable" CssClass="headFont"  /> 
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            <div id="tabs-4">
                                                                <%--Bank Details--%>
                                                                <table align="center" width="100%">
                                                                    <tr>
                                                                        <td style="padding: 5px;"><span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlBank" Width="200px" Height="22px" runat="server"  DataValueField="BankID" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Branch Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:DropDownList ID="ddlBranch" Width="200px" Height="22px" runat="server"  DataValueField="BankID" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Branch)" Selected="True" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                   <td style="padding: 5px;"><span class="headFont">IFSC Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtIFSC" ClientIDMode="Static" runat="server" Width="192px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">MICR No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtMICR" ClientIDMode="Static" runat="server" Width="192px" Enabled="false" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td style="padding: 5px;"><span class="headFont">Bank Account No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                        <td style="padding: 5px;">:</td>
                                                                        <td style="padding: 5px;" align="left">
                                                                            <asp:TextBox ID="txtBankCode" ClientIDMode="Static" runat="server" Width="192px" CssClass="textbox"  MaxLength="20" autocomplete="off"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            <div id="tabs-5">
                                                                <%--Upload Photo and Signature--%>
                                                                 <table align="center" id='table1' cellpadding="5" width="100%">
                                                            <tr>
                                                                <td class="headFont">
                                                                <div style="float:left; margin-top:0px;">
                                                                Upload Photo -&nbsp;&nbsp;<span class="requirefield">*</span>
                                                                Size between 20KB to 50KB (Dimension 138 W X 177 H)  (4.5 cm Height x 3.5 cm Width)</td>
                                                                </div>
                                                                <td class="labelCaptionbold">:</td>
                                                                <td align="left">
                                                                    <div style="float:left;width:540px;" >
                                                                        <div style="float:left;width:350px;">
                                                                            <input id="fileToUpload" type="file" runat="server"  name="fileToUpload" class="headFont" Text='<%# Eval("Photo") %>' />
                                                                            <button id="buttonUpload" class="button01 DefaultButton">Upload</button>
                                                                            <img id="loading" src="images/ajax-loader_new.gif" style="display: none;">
                                                                            <span id="imgPhoto" style="width: 26px; height: 24px;"></span>
                                                                            <asp:TextBox ID="lblPhoto" runat="server" Style="cursor: not-allowed;" CssClass="text_field01" ReadOnly="true" Text=""></asp:TextBox>
                                                                        </div>
                                                                        <div style="float:left;">
                                                                            <div id="canPhoto" style="width:100px;height: 116px;border: solid 1px #6d760b;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class="headFont"> Upload Full Signature -&nbsp;&nbsp;<span class="requirefield">*</span>
                                                                    Size between 10 KB to 20KB (Dimension 250 W X 45 H) (1.1cm Height X 6.6 cm Width)
                                                                </td>
                                                                <td class="labelCaptionbold">:</td>
                                                                <td align="left">
                                                                    <div style="float:left;width:540px;" >
                                                                        <div style="float:left;width:350px;">
                                                                            <input id="fileToUploadSign" type="file"  name="fileToUploadSign" class="headFont" Text='<%# Eval("Signature") %>' />
                                                                            <button id="buttonUploadSign" class="button01 DefaultButton">Upload</button>
                                                                            <img id="loadingSign" src="images/ajax-loader_new.gif" style="display: none;">
                                                                            <span id="imgSign" style="width: 26px; height: 24px;"></span>                                                                    
                                                                            <asp:TextBox ID="lblSign" runat="server" Style="cursor: not-allowed;" CssClass="text_field01" ReadOnly="true" Text=""></asp:TextBox>
                                                                        </div>
                                                                        <div style="float:left;">
                                                                            <div id="canSign" style="width:150px;height: 27px;border: solid 1px #6d760b;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                                </table>
                                                            </div>


                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td colspan="4">
                                                        <hr style="border: solid 1px lightblue" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                    <td style="padding: 10px;">&nbsp;</td>
                                                    <td style="padding: 10px;" align="left">
                                                        <div style="float: left; margin-left: 200px;">
                                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"
                                                                Width="100px" CssClass="Btnclassname DefaultButton" OnClientClick="RunValidationsAndSetActiveTab()" />

                                                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update"
                                                                Width="100px" CssClass="Btnclassname DefaultButton" />

                                                               <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                                                Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                        </div>
                                                    </td>
                                                </tr>

                                            </table>
                                        </FooterTemplate>
                                    </asp:FormView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>


                        </table>


                    </div>
                    <%---------------- End of Personal Tab ----------------------------------------%>


                    <div id="tabProfessional" runat="server">
                        <%-- style="display: none">--%>  <%--------------------Professional Tab-------------------%>

                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <asp:FormView ID="dv1" runat="server" Width="99%"
                                        AutoGenerateRows="False"
                                        DefaultMode="Insert" HorizontalAlign="Center"
                                        GridLines="None" >
                                        <InsertItemTemplate>
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td colspan="5" style="padding: 10px;">
                                                        <div id="tabs1" runat="server" clientidmode="Static">
                                                            <div class="prod-det-tab-out_main">
                                                                <ul>
                                                                    <li>
                                                                        <a href="#tabs-1">Pay Details</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-2">Transfer Details</a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                            <div id="tabs-1">
                                                                <%--Employee Pay Details Information--%>

                                                                <table width="98%">
                                                                    <tr>
                                                                        <td style="padding: 3px;">


                                                                            <table align="center" width="100%">
                                                                                <tr>
                                                                                    <td style="padding: 5px;"><span class="headFont">Earn Deduction&nbsp;&nbsp;<span class="require">*</span> </td>
                                                                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                                                                    <td style="padding: 5px;" align="left">
                                                                                        <asp:DropDownList ID="ddlEarnDeduction"  DataSource='<%# drpload("EDName") %>'
                                                                                            DataValueField="EDID" Height="24px"
                                                                                            DataTextField="EarnDeduction" runat="server" CssClass="headFont"
                                                                                            AppendDataBoundItems="true" autocomplete="off">
                                                                                            <asp:ListItem Text="(Select Earn/Deduction)" Value=""></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>

                                                                                    <td style="padding: 5px;"><span class="headFont">Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                    <td style="padding: 5px;">:</td>
                                                                                    <td style="padding: 5px;" align="left">
                                                                                        <asp:TextBox ID="txtAmount" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div>
                                                                                            <asp:Button ID="cmdAdd" runat="server" Text="Add" ClientIDMode="Static"
                                                                                                Width="80px" CssClass="Btnclassname" OnClientClick="DefaultButton" />

                                                                                            <asp:Button ID="cmdUpdate" runat="server" Text="Update" ClientIDMode="Static"
                                                                                                Width="80px" CssClass="Btnclassname" OnClientClick="DefaultButton" />

                                                                                            <asp:Button ID="cmdCancels" runat="server" Text="Cancel" ClientIDMode="Static"
                                                                                                Width="80px" CssClass="Btnclassname" OnClientClick="DefaultButton" />
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                </tr>
                                                                            </table>
                                                                            <br />
                                                                            <div id="divTable" class="headFont"></div>
                                                                         <br />
                                                                            <div id="lbl" class="background">
                                                                              
                                                                                <div class="prod-det-tab-out_main" style="height:20px;">

                                                                                            <asp:Label ID="lblTotal" CssClass="showbalance" runat="server" Text="Total Earning(E) :"></asp:Label>&nbsp;
                                                                                            <asp:Label ID="lblTotalEarning" CssClass="showbalance" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                            <asp:Label ID="lblDeduction" runat="server" CssClass="showbalance" Text="Total Deduction(D) :"></asp:Label>&nbsp;
                                                                                            <asp:Label ID="lblTotalDeduction" CssClass="showbalance" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                            <asp:Label ID="lblNet" runat="server" CssClass="showbalance" Text="Net :"></asp:Label>&nbsp;
                                                                                            <asp:Label ID="lblTotalNet" CssClass="showbalance" runat="server"></asp:Label>

                                                                                </div>
                                                                              
                                                                            </div>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </InsertItemTemplate>

                                        <EditItemTemplate>
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td colspan="5" style="padding: 10px;">
                                                        <div id="tabs1" runat="server" clientidmode="Static">
                                                            <div class="prod-det-tab-out_main">
                                                                <ul>
                                                                    <li>
                                                                        <a href="#tabs-1">Pay Details</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#tabs-2">Others</a>
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                            <div id="tabs-1">
                                                                <%--Employee Pay Details Information--%>

                                                                <table width="98%">
                                                                    <tr>
                                                                        <td style="padding: 3px;">


                                                                            <table align="center" width="100%">
                                                                                <tr>
                                                                                    <td style="padding: 5px;"><span class="headFont">Earn Deduction&nbsp;&nbsp;<span class="require">*</span> </td>
                                                                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                                                                    <td style="padding: 5px;" align="left">
                                                                                        <asp:DropDownList ID="ddlEarnDeduction" autocomplete="off" DataSource='<%# drpload("EDName") %>'
                                                                                            DataValueField="EDID"
                                                                                            DataTextField="EarnDeduction" runat="server" Height="24px" CssClass="textbox"
                                                                                            AppendDataBoundItems="true" >
                                                                                            <asp:ListItem Text="(Select Earn/Deduction)" Value=""></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>

                                                                                    <td style="padding: 5px;"><span class="headFont">Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                                    <td style="padding: 5px;">:</td>
                                                                                    <td style="padding: 5px;" align="left">
                                                                                        <asp:TextBox ID="txtAmount" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div>
                                                                                            <asp:Button ID="cmdAdd" runat="server" Text="Add" ClientIDMode="Static"
                                                                                                Width="80px" OnClientClick="DefaultButton" />

                                                                                            <asp:Button ID="cmdUpdate" runat="server" Text="Update" ClientIDMode="Static"
                                                                                                Width="80px" OnClientClick="DefaultButton" />

                                                                                            <asp:Button ID="cmdCancels" runat="server" Text="Cancel" ClientIDMode="Static"
                                                                                                Width="80px" OnClientClick="DefaultButton" />
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                </tr>
                                                                            </table>
                                                                            <br />
                                                                            <div id="divTable"></div>
                                                                         <br />
                                                                            <div id="lbl" class="background">
                                                                              
                                                                                <div class="prod-det-tab-out_main" style="height:20px;">
                                                                               
                                                                                          
                                                                                            <asp:Label ID="lblTotal" CssClass="showbalance" runat="server" Text="Total Earning(E) :">
                                                                                        
                                                                                            </asp:Label><asp:Label ID="lblTotalEarning" CssClass="showbalance" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                          
                                                                                             <asp:Label ID="lblDeduction" runat="server" CssClass="showbalance" Text="Total Deduction(D) :"></asp:Label>
                                                                                            <asp:Label ID="lblTotalDeduction" CssClass="showbalance" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                            <asp:Label ID="lblNet" runat="server" CssClass="showbalance" Text="Net :"></asp:Label>
                                                                                            <asp:Label ID="lblTotalNet" CssClass="showbalance" runat="server"></asp:Label>

                                                                                </div>
                                                                              
                                                                            </div>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </EditItemTemplate>
                                            <FooterTemplate>
                                               <table align="center" width="100%">
                                                <tr>
                                                    <td colspan="4">
                                                        <hr style="border: solid 1px lightblue" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                    <td style="padding: 10px;">&nbsp;</td>
                                                    <td style="padding: 10px;" align="left">
                                                        <div style="float: left; margin-left: 200px;">
                                                            <asp:Button ID="cmdSave1" runat="server" Text="Create" CommandName="Add"
                                                                Width="100px" CssClass="Btnclassname DefaultButton" />

                                                             <asp:Button ID="btnUpdate1" runat="server" Text="Update" CommandName="Update"
                                                             Width="80px" CssClass="Btnclassname DefaultButton" />

                                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                                                Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />

                                                        </div>

                                                    </td></tr>


                                            </table>
                                                 
                                        </FooterTemplate>
                                    </asp:FormView>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                            </tr>

                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <%---------------------- End of Professional Tab -----------------------------------%>
                </div>
            </div>

        </div>
    </div>
</asp:Content>


