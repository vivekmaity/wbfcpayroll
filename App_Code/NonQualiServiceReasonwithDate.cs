﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NonQualiServiceReasonwithDate
/// </summary>
public class NonQualiServiceReasonwithDate
{

    public int NonQualiReasonID { get; set; }
    public string NonQualiReasonFrom { get; set; }
    public string NonQualiReasonTo { get; set; }
    public string NonQualiReason { get; set; }

	public NonQualiServiceReasonwithDate()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}