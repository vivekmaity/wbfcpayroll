﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;

using System.Net;
using System.IO;
using System.Data;
using DataAccess;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Class1
/// </summary>
public static class Utility
{

    
    public static void sendEmail(string toEmail, string cc, string bcc, bool bodyHTML, string subject, string body,string attachment)
    {
        try
        {
            
            MailMessage mail = new MailMessage();
            if (toEmail != null )
            {
                mail.To.Add(toEmail);
                mail.Subject = subject;
                if (cc != null )
                {
                    mail.CC.Add(cc);
                }
                if (bcc != null )
                {
                    mail.Bcc.Add(bcc);
                }
                mail.BodyEncoding = System.Text.Encoding.Default;
                mail.Priority=MailPriority.High;
                mail.IsBodyHtml = bodyHTML;
                if (attachment != null )
                {
                    mail.Attachments.Add(new Attachment(attachment));
                }
                mail.Body = body;
                sendSMTPMail(mail);
                mail.Dispose();
            }
        }
        catch(Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private static void sendSMTPMail(MailMessage mail)
    {
        try
        {
            DataRow drEmailSMSSetting = DBHandler.GetResult("Get_EmailSMSSettings").Rows[0];
            SmtpClient SmtpServer = new SmtpClient(drEmailSMSSetting["SMTP"].ToString());

            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = new System.Net.NetworkCredential
                        (drEmailSMSSetting["UserName"].ToString(), drEmailSMSSetting["Password"].ToString());
            SmtpServer.Port = Convert.ToInt32(drEmailSMSSetting["Port"].ToString());//587;

            SmtpServer.Host = drEmailSMSSetting["SMTP"].ToString();//"smtp.gmail.com"; //
            SmtpServer.EnableSsl = Convert.ToBoolean(drEmailSMSSetting["SSL"].ToString());
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;

            SmtpServer.Timeout = 1000000;
            mail.From = new MailAddress(drEmailSMSSetting["FromEmail"].ToString(), drEmailSMSSetting["DisplayName"].ToString());
            SmtpServer.Send(mail);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public static string SendSMS(string mobileNo,string message)
    {
        string retText = "";
        try
        {
            DataRow drEmailSMSSetting = DBHandler.GetResult("Get_EmailSMSSettings").Rows[0];
            string url = "";
            string username = drEmailSMSSetting["SMSUserName"].ToString();
            string password = drEmailSMSSetting["SMSPassword"].ToString();
            string masks = drEmailSMSSetting["SMSMasks"].ToString();
            string host = drEmailSMSSetting["HOST"].ToString();

            if (mobileNo.Contains(","))
            {
                url = host + "?UserName=" + HttpUtility.UrlEncode(username)
               + "&Password=" + HttpUtility.UrlEncode(password)
               + "&Type=Bulk"
               + "&To=" + HttpUtility.UrlEncode(mobileNo)
               + "&Mask=" + HttpUtility.UrlEncode(masks) + "&Message=" + HttpUtility.UrlEncode(message);
            }
            else
            {
                url = host + "?UserName=" + HttpUtility.UrlEncode(username)
              + "&Password=" + HttpUtility.UrlEncode(password)
              + "&Type=Individual"
              + "&To=" + HttpUtility.UrlEncode(mobileNo)
              + "&Mask=" + HttpUtility.UrlEncode(masks) + "&Message=" + HttpUtility.UrlEncode(message);
            }

            WebRequest wrURL;
            Stream objStream;
            wrURL = WebRequest.Create(url);
            objStream = wrURL.GetResponse().GetResponseStream();
            StreamReader objSReader = new StreamReader(objStream);

            string messagestat = objSReader.ReadToEnd().ToString().Trim();

            System.Threading.Thread.Sleep(1000);
            if (messagestat.ToUpper().Contains("SUCCESS"))
            {
                retText = "SUCCESS";
            }
            else
            {
                retText = "FAILURE";
            }
            wrURL = null;
            objStream = null;
            objSReader = null;
        }
        catch (Exception ex)
        {

        }
        return retText;
    }

    public static string ServerName(string ConnectionString)
    {
        string server = "";
        try
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = ConnectionString;
            server = builder.DataSource;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return server;
    }
    public static string DatabaseName(string ConnectionString)
    {
        string database = "";
        try
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = ConnectionString;
            database = builder.InitialCatalog;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return database;
    }
    public static string UserID(string ConnectionString)
    {
        string userid = "";
        try
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = ConnectionString;
            userid = builder.UserID;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return userid;
    }
    public static string Password(string ConnectionString)
    {
        string pwd = "";
        try
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = ConnectionString;
            pwd = builder.Password;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return pwd;
    }
}