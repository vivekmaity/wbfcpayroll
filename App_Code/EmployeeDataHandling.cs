﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Xml.Serialization;

/// <summary>
/// Summary description for EmployeeDataHandling
/// </summary>
public class EmployeeDataHandling
{
    public static DataTable CreateEmpMasterTable(DataTable dtempMaster)
    {
        try
        {
            //Personal Information
            dtempMaster.Columns.Add(new DataColumn("EmployeeID", typeof(System.Int32)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeCode", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeNo", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeType", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmpClassification", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeName", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeReligion", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeQualification", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeMaritalStatus", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeCaste", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeDOB", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeGender", typeof(System.String)));
            ////Address Information
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentAddress", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentState", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentDistrict", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentCity", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentPin", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentPhone", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentMobile", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePresentEmail", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePermanentAddress", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePermanentState", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePermanentDistrict", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePermanentCity", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePermanentPin", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePermanentPhone", typeof(System.String)));
            ////Official Details
            dtempMaster.Columns.Add(new DataColumn("EmployeeDOJ", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeDOR", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeDesignation", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeCooperative", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EmployeeDA", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("PFCode", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("DNI", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("HRA", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EffectiveFrom", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("QuarterAllot", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("OLDBasic", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("LicenceFees", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("SpouseAmount", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("HealthScheme", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("DOJPresentDept", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("PhysicallyChallenged", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("Group", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("Category", typeof(System.String)));
            //********************Sup******************************************************

            dtempMaster.Columns.Add(new DataColumn("DepartmentId", typeof(System.Int32)));
            dtempMaster.Columns.Add(new DataColumn("EmployerPfRate", typeof(System.Decimal)));
            dtempMaster.Columns.Add(new DataColumn("EmployeePfRate", typeof(System.Decimal)));

            dtempMaster.Columns.Add(new DataColumn("FathersName", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("AddressRemarks", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("BranchInCharge", typeof(System.String)));

            dtempMaster.Columns.Add(new DataColumn("PFRate", typeof(System.String)));

            //*****************************************************************************

            dtempMaster.Columns.Add(new DataColumn("Status", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("PayScaleType", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("PayScale", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("CoopMembership", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("HRACategory", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("HRAFlag", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("HRAFixedAmount", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("PANNO", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("EffectiveTo", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("House", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("PCTBasic", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("SpouseQuarter", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("LWP", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("IR", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("HealthSchemeDetails", typeof(System.String)));
            ////Bank Details
            dtempMaster.Columns.Add(new DataColumn("BranchID", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("BankAccountCode", typeof(System.String)));
            ////Phota and Signature
            dtempMaster.Columns.Add(new DataColumn("Photo", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("Signature", typeof(System.String)));
            dtempMaster.Columns.Add(new DataColumn("AdminID", typeof(System.Int32)));
            //Others
            dtempMaster.Columns.Add(new DataColumn("SectorID", typeof(System.Int32)));
            dtempMaster.Columns.Add(new DataColumn("CenterID", typeof(System.Int32)));
            dtempMaster.Columns.Add(new DataColumn("InsertedBy", typeof(System.Int32)));


        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtempMaster;
    }
    public static DataTable CreateEmpDetailTable(DataTable dtEmpDetail)
    {
        try
        {
            dtEmpDetail.Columns.Add(new DataColumn("BlockID", typeof(System.Int32)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockDate", typeof(System.String)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockedAdminID", typeof(System.Int32)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockedFor", typeof(System.String)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockedReason", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtEmpDetail;
    }
    public static DataTable CreatePayDetailsTable(DataTable dtPayDetail)
    {
        try
        {
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionID", typeof(System.Int32)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeduction", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("Amount", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionType", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtPayDetail;
    }
    public static DataTable CreatePayDetailsTableafterDelete(DataTable dtPayDetail)
    {
        try
        {
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionID", typeof(System.Int32)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeduction", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("Amount", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionType", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtPayDetail;
    }
    public static DataTable CreateAddPayDetailsTable(DataTable dtAddPayDetails)
    {
        try
        {
            dtAddPayDetails.Columns.Add(new DataColumn("EDID", typeof(System.Int32)));
            dtAddPayDetails.Columns.Add(new DataColumn("EarnDeduction", typeof(System.String)));
            dtAddPayDetails.Columns.Add(new DataColumn("Amount", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtAddPayDetails;
    }
    public static void UpdateMaster(DataTable dtMaster, EmployeeMasters master)
    {
        try
        {
            if (master != null && dtMaster != null)
            {
                DataRow[] dr1 = dtMaster.Select("EmployeeID=" + master.EmployeeID);
                if (dr1.Length > 0)
                {
                    foreach (DataRow dr in dr1)
                    {
                        
                        // dr["EmployeeCode"] =  master.EmployeeCode;//.Equals("") ? DBNull.Value : (object)master.EmployeeCode;
                        dr["EmployeeNo"] = master.EmployeeNo;//.Equals("") ? DBNull.Value : (object)master.EmployeeNo;
                        dr["EmployeeType"] = master.EmployeeType;//.Equals("") ? DBNull.Value : (object)master.EmployeeType;
                        dr["EmpClassification"] = master.EmpClassification;//.Equals("") ? DBNull.Value : (object)master.EmpClassification;
                        dr["EmployeeName"] = master.EmployeeName;//.Equals("") ? DBNull.Value : (object)master.EmployeeName;
                        dr["EmployeeReligion"] = master.EmployeeReligion;//.Equals("") ? DBNull.Value : (object)master.EmployeeReligion;
                        dr["EmployeeQualification"] = master.EmployeeQualification;//.Equals("") ? DBNull.Value : (object)master.EmployeeQualification;
                        dr["EmployeeMaritalStatus"] = master.EmployeeMaritalStatus;//.Equals("") ? DBNull.Value : (object)master.EmployeeMaritalStatus;
                        dr["EmployeeCaste"] = master.EmployeeCaste;//.Equals("") ? DBNull.Value : (object)master.EmployeeCaste;

                        string d = Convert.ToString(master.EmployeeDOB);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOB);
                        if (d != null && d != "")
                        {
                            DateTime dt = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EmployeeDOB"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EmployeeDOB"] = d; }

                        dr["EmployeeGender"] = master.EmployeeGender;//.Equals("") ? DBNull.Value : (object)master.EmployeeGender;
                        ////Address Information
                        dr["EmployeePresentAddress"] = master.EmployeePresentAddress;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentAddress;
                        dr["EmployeePresentState"] = master.EmployeePresentState;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentState;
                        dr["EmployeePresentDistrict"] = master.EmployeePresentDistrict;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentDistrict;
                        dr["EmployeePresentCity"] = master.EmployeePresentCity;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentCity;
                        dr["EmployeePresentPin"] = master.EmployeePresentPin;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentPin;
                        dr["EmployeePresentPhone"] = master.EmployeePresentPhone;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentPhone;
                        dr["EmployeePresentMobile"] = master.EmployeePresentMobile;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentMobile;
                        dr["EmployeePresentEmail"] = master.EmployeePresentEmail;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentEmail;
                        dr["EmployeePermanentAddress"] = master.EmployeePermanentAddress;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentAddress;
                        dr["EmployeePermanentState"] = master.EmployeePermanentState;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentState;
                        dr["EmployeePermanentDistrict"] = master.EmployeePermanentDistrict;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentDistrict;
                        dr["EmployeePermanentCity"] = master.EmployeePermanentCity;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentCity;
                        dr["EmployeePermanentPin"] = master.EmployeePermanentPin;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPin;
                        dr["EmployeePermanentPhone"] = master.EmployeePermanentPhone;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPhone;
                        //////Official Details
                        string doj = Convert.ToString(master.EmployeeDOJ);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOJ);
                        if (doj != null && doj != "")
                        {
                            DateTime dt = DateTime.ParseExact(doj, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EmployeeDOJ"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EmployeeDOJ"] = doj; }

                        string dor = Convert.ToString(master.EmployeeDOR);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOR);
                        if (dor != null && dor != "")
                        {
                            DateTime dt = DateTime.ParseExact(dor, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EmployeeDOR"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EmployeeDOR"] = dor; }

                        dr["EmployeeDesignation"] = master.EmployeeDesignation;//.Equals("") ? DBNull.Value : (object)master.EmployeeDesignation;
                        dr["EmployeeCooperative"] = master.EmployeeCooperative;//.Equals("") ? DBNull.Value : (object)master.EmployeeCooperative;
                        dr["EmployeeDA"] = master.EmployeeDA;//.Equals("") || master.EmployeeDA.Equals(null) ? DBNull.Value : (object)master.EmployeeDA;
                        dr["PFCode"] = master.PFCode;//.Equals("") ? DBNull.Value : (object)master.PFCode;

                        string dni = Convert.ToString(master.DNI);//.Equals("") ? DBNull.Value : (object)master.DNI);
                        if (dni != null && dni != "")
                        {
                            DateTime dt = DateTime.ParseExact(dni, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["DNI"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["DNI"] = dni; }

                        dr["HRAFlag"] = master.HRAFlag;
                        dr["HRAFixedAmount"] = master.HRAFixedAmount;
                        dr["HRA"] = master.HRA;//.Equals("") ? DBNull.Value : (object)master.HRA;

                        string efffrom = Convert.ToString(master.EffectiveFrom);//.Equals("") ? DBNull.Value : (object)master.EffectiveFrom);
                        if (efffrom != null && efffrom != "")
                        {
                            DateTime dt = DateTime.ParseExact(efffrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EffectiveFrom"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EffectiveFrom"] = efffrom; }

                        dr["QuarterAllot"] = master.QuarterAllot;//.Equals("") ? DBNull.Value : (object)master.QuarterAllot;

                        string OBasic = Convert.ToString(master.OLDBasic);//.Equals("") ? DBNull.Value : (object)master.OLDBasic);
                        if (OBasic != null && OBasic != "")
                        {
                            double oldBasic = Convert.ToDouble(OBasic.ToString()); dr["OLDBasic"] = oldBasic;
                        }
                        else { dr["OLDBasic"] = OBasic; }

                        string Lfees = Convert.ToString(master.LicenceFees);//.Equals("") ? DBNull.Value : (object)master.LicenceFees);
                        if (Lfees != null && Lfees != "")
                        {
                            double LicFees = Convert.ToDouble(Lfees.ToString()); dr["LicenceFees"] = LicFees;
                        }
                        else { dr["LicenceFees"] = Lfees; }

                        string Samount = Convert.ToString(master.SpouseAmount);//.Equals("") ? DBNull.Value : (object)master.SpouseAmount);
                        if (Samount != null && Samount != "")
                        {
                            double SPamount = Convert.ToDouble(Samount.ToString()); dr["SpouseAmount"] = SPamount;
                        }
                        else { dr["SpouseAmount"] = Samount; }

                        dr["HealthScheme"] = master.HealthScheme;//.Equals("") ? DBNull.Value : (object)master.HealthScheme;

                        string DojP = Convert.ToString(master.DOJPresentDept);//.Equals("") ? DBNull.Value : (object)master.DOJPresentDept);
                        if (DojP != null && DojP != "")
                        {
                            DateTime dt = DateTime.ParseExact(DojP, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["DOJPresentDept"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["DOJPresentDept"] = DojP; }

                        dr["PhysicallyChallenged"] = master.PhysicallyChallenged;//.Equals("") ? DBNull.Value : (object)master.PhysicallyChallenged;
                        dr["Group"] = master.Group;//.Equals("") ? DBNull.Value : (object)master.Group;
                        dr["Category"] = master.Category;//.Equals("") ? DBNull.Value : (object)master.Category;
                        //*****************Sup*****************************

                        dr["DepartmentId"] = master.DepartmentId;
                        dr["EmployerPfRate"] = master.EmployerPfRate;
                        dr["EmployeePfRate"] = master.EmployeePfRate;

                        dr["FathersName"] = master.FathersName;
                        dr["BranchInCharge"] = master.BranchInCharge;
                        dr["AddressRemarks"] = master.AddressRemarks;

                        dr["PFRate"] = master.PFRate;

                        //*************************************************
                        dr["Status"] = master.Status;//.Equals("") ? DBNull.Value : (object)master.Status;
                        dr["PayScaleType"] = master.PayScaleType;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                        dr["PayScale"] = master.PayScale;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                        dr["CoopMembership"] = master.CoopMembership;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                        dr["HRACategory"] = master.HRACategory;//.Equals("") ? DBNull.Value : (object)master.HRACategory;
                        dr["PANNO"] = master.PANNO;//.Equals("") ? DBNull.Value : (object)master.PANNO;

                        string EffTo = Convert.ToString(master.EffectiveTo);//.Equals("") ? DBNull.Value : (object)master.EffectiveTo);
                        if (EffTo != null && EffTo != "")
                        {
                            DateTime dt = DateTime.ParseExact(EffTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EffectiveTo"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EffectiveTo"] = EffTo; }

                        dr["House"] = master.House;//.Equals("") ? DBNull.Value : (object)master.House;

                        string PCTBascic = Convert.ToString(master.PCTBasic);//.Equals("") ? DBNull.Value : (object)master.PCTBasic);
                        if (PCTBascic != null && PCTBascic != "")
                        {
                            double PCTB = Convert.ToDouble(PCTBascic.ToString()); dr["PCTBasic"] = PCTB;
                        }
                        else { dr["PCTBasic"] = PCTBascic; }

                        dr["SpouseQuarter"] = master.SpouseQuarter;//.Equals("") ? DBNull.Value : (object)master.SpouseQuarter;

                        dr["LWP"] = master.LWP;//.Equals("") ? DBNull.Value : (object)master.LWP;
                        dr["IR"] = master.IR;//.Equals("") ? DBNull.Value : (object)master.LWP;
                        dr["HealthSchemeDetails"] = master.HealthSchemeDetails;//.Equals("") ? DBNull.Value : (object)master.HealthSchemeDetails;
                        ////Bank Details
                        dr["BranchID"] = master.BranchID;//.Equals("") ? DBNull.Value : (object)master.BankID;
                        dr["BankAccountCode"] = master.BankAccountCode;//.Equals("") ? DBNull.Value : (object)master.BankAccountCode;
                        ////Phota and Signature

                        dr["Photo"] = master.Photo;//.Equals("") ? DBNull.Value : (object)master.Photo;
                        dr["Signature"] = master.Signature;//.Equals("") ? DBNull.Value : (object)master.Signature;

                        int AdminID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                        dr["AdminID"] = AdminID;

                        //Others
                        dr["SectorID"] = master.SectorID;//.Equals("") ? DBNull.Value : (object)master.SectorID;
                        dr["CenterID"] = master.CenterID;//.Equals("") ? DBNull.Value : (object)master.CenterID;
                        dr["InsertedBy"] = AdminID;

                        dtMaster.Rows.Add(dr);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static void InsertMaster(DataTable dtMaster, EmployeeMasters master)
    {
        try
        {
            if (master != null && dtMaster != null)
            {
                DataRow dr = dtMaster.NewRow();
                object maxval = (dtMaster.Compute("max(EmployeeID)", String.Empty));
                int maxValue = 1;
                if (!(maxval is DBNull))
                {
                    maxValue = (Int32)maxval + 1;
                }
                //Personal Information
                dr["EmployeeID"] = maxValue;
               // dr["EmployeeCode"] =  master.EmployeeCode;//.Equals("") ? DBNull.Value : (object)master.EmployeeCode;
                dr["EmployeeNo"] = master.EmployeeNo;//.Equals("") ? DBNull.Value : (object)master.EmployeeNo;
                dr["EmployeeType"] = master.EmployeeType;//.Equals("") ? DBNull.Value : (object)master.EmployeeType;
                dr["EmpClassification"] = master.EmpClassification;//.Equals("") ? DBNull.Value : (object)master.EmpClassification;
                dr["EmployeeName"] = master.EmployeeName;//.Equals("") ? DBNull.Value : (object)master.EmployeeName;
                dr["EmployeeReligion"] = master.EmployeeReligion;//.Equals("") ? DBNull.Value : (object)master.EmployeeReligion;
                dr["EmployeeQualification"] = master.EmployeeQualification;//.Equals("") ? DBNull.Value : (object)master.EmployeeQualification;
                dr["EmployeeMaritalStatus"] = master.EmployeeMaritalStatus;//.Equals("") ? DBNull.Value : (object)master.EmployeeMaritalStatus;
                dr["EmployeeCaste"] = master.EmployeeCaste;//.Equals("") ? DBNull.Value : (object)master.EmployeeCaste;

                string d = Convert.ToString(master.EmployeeDOB);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOB);
                if (d != null && d != "")
                {
                    DateTime dt = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EmployeeDOB"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EmployeeDOB"] = d; }

                dr["EmployeeGender"] = master.EmployeeGender;//.Equals("") ? DBNull.Value : (object)master.EmployeeGender;
                ////Address Information
                dr["EmployeePresentAddress"] = master.EmployeePresentAddress;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentAddress;
                dr["EmployeePresentState"] = master.EmployeePresentState;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentState;
                dr["EmployeePresentDistrict"] = master.EmployeePresentDistrict;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentDistrict;
                dr["EmployeePresentCity"] = master.EmployeePresentCity;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentCity;
                dr["EmployeePresentPin"] = master.EmployeePresentPin;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentPin;
                dr["EmployeePresentPhone"] = master.EmployeePresentPhone;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentPhone;
                dr["EmployeePresentMobile"] = master.EmployeePresentMobile;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentMobile;
                dr["EmployeePresentEmail"] = master.EmployeePresentEmail;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentEmail;
                dr["EmployeePermanentAddress"] = master.EmployeePermanentAddress;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentAddress;
                dr["EmployeePermanentState"] = master.EmployeePermanentState;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentState;
                dr["EmployeePermanentDistrict"] = master.EmployeePermanentDistrict;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentDistrict;
                dr["EmployeePermanentCity"] = master.EmployeePermanentCity;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentCity;
                dr["EmployeePermanentPin"] = master.EmployeePermanentPin;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPin;
                dr["EmployeePermanentPhone"] = master.EmployeePermanentPhone;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPhone;
                //////Official Details
                string doj = Convert.ToString(master.EmployeeDOJ);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOJ);
                if (doj != null && doj != "")
                {
                    DateTime dt = DateTime.ParseExact(doj, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EmployeeDOJ"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EmployeeDOJ"] = doj; }

                string dor = Convert.ToString(master.EmployeeDOR);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOR);
                if (dor != null && dor != "")
                {
                    DateTime dt = DateTime.ParseExact(dor, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EmployeeDOR"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EmployeeDOR"] = dor; }

                dr["EmployeeDesignation"] = master.EmployeeDesignation;//.Equals("") ? DBNull.Value : (object)master.EmployeeDesignation;
                dr["EmployeeCooperative"] = master.EmployeeCooperative;//.Equals("") ? DBNull.Value : (object)master.EmployeeCooperative;
                dr["EmployeeDA"] = master.EmployeeDA;//.Equals("") || master.EmployeeDA.Equals(null) ? DBNull.Value : (object)master.EmployeeDA;
                dr["PFCode"] = master.PFCode;//.Equals("") ? DBNull.Value : (object)master.PFCode;

                string dni = Convert.ToString(master.DNI);//.Equals("") ? DBNull.Value : (object)master.DNI);
                if (dni != null && dni != "")
                {
                    DateTime dt = DateTime.ParseExact(dni, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DNI"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DNI"] = dni; }

                dr["HRAFlag"] = master.HRAFlag;
                dr["HRAFixedAmount"] = master.HRAFixedAmount;
                dr["HRA"] = master.HRA;//.Equals("") ? DBNull.Value : (object)master.HRA;

                string efffrom = Convert.ToString(master.EffectiveFrom);//.Equals("") ? DBNull.Value : (object)master.EffectiveFrom);
                if (efffrom != null && efffrom != "")
                {
                    DateTime dt = DateTime.ParseExact(efffrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EffectiveFrom"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EffectiveFrom"] = efffrom; }

                dr["QuarterAllot"] = master.QuarterAllot;//.Equals("") ? DBNull.Value : (object)master.QuarterAllot;

                string OBasic = Convert.ToString(master.OLDBasic);//.Equals("") ? DBNull.Value : (object)master.OLDBasic);
                if (OBasic != null && OBasic != "")
                {
                    double oldBasic = Convert.ToDouble(OBasic.ToString()); dr["OLDBasic"] = oldBasic;
                }
                else { dr["OLDBasic"] = OBasic; }

                string Lfees = Convert.ToString(master.LicenceFees);//.Equals("") ? DBNull.Value : (object)master.LicenceFees);
                if (Lfees != null && Lfees != "")
                {
                    double LicFees = Convert.ToDouble(Lfees.ToString()); dr["LicenceFees"] = LicFees;
                }
                else { dr["LicenceFees"] = Lfees; }

                string Samount = Convert.ToString(master.SpouseAmount);//.Equals("") ? DBNull.Value : (object)master.SpouseAmount);
                if (Samount != null && Samount != "")
                {
                    double SPamount = Convert.ToDouble(Samount.ToString()); dr["SpouseAmount"] = SPamount;
                }
                else { dr["SpouseAmount"] = Samount; }

                dr["HealthScheme"] = master.HealthScheme;//.Equals("") ? DBNull.Value : (object)master.HealthScheme;

                string DojP = Convert.ToString(master.DOJPresentDept);//.Equals("") ? DBNull.Value : (object)master.DOJPresentDept);
                if (DojP != null && DojP != "")
                {
                    DateTime dt = DateTime.ParseExact(DojP, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DOJPresentDept"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DOJPresentDept"] = DojP; }

                dr["PhysicallyChallenged"] = master.PhysicallyChallenged;//.Equals("") ? DBNull.Value : (object)master.PhysicallyChallenged;
                dr["Group"] = master.Group;//.Equals("") ? DBNull.Value : (object)master.Group;
                dr["Category"] = master.Category;//.Equals("") ? DBNull.Value : (object)master.Category;

                //********************Sup***************************************
                dr["DepartmentId"] = master.DepartmentId;
                dr["EmployerPfRate"] = master.EmployerPfRate;
                dr["EmployeePfRate"] = master.EmployeePfRate;

                dr["FathersName"] = master.FathersName;
                dr["BranchInCharge"] = master.BranchInCharge;
                dr["AddressRemarks"] = master.AddressRemarks;

                dr["PFRate"] = master.PFRate;


                //**************************************************************

                dr["Status"] = master.Status;//.Equals("") ? DBNull.Value : (object)master.Status;
                dr["PayScaleType"] = master.PayScaleType;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                dr["PayScale"] = master.PayScale;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                dr["CoopMembership"] = master.CoopMembership;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                dr["HRACategory"] = master.HRACategory;//.Equals("") ? DBNull.Value : (object)master.HRACategory;
                dr["PANNO"] = master.PANNO;//.Equals("") ? DBNull.Value : (object)master.PANNO;

                string EffTo = Convert.ToString(master.EffectiveTo);//.Equals("") ? DBNull.Value : (object)master.EffectiveTo);
                if (EffTo != null && EffTo != "")
                {
                    DateTime dt = DateTime.ParseExact(EffTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EffectiveTo"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EffectiveTo"] = EffTo; }

                dr["House"] = master.House;//.Equals("") ? DBNull.Value : (object)master.House;

                string PCTBascic = Convert.ToString(master.PCTBasic);//.Equals("") ? DBNull.Value : (object)master.PCTBasic);
                if (PCTBascic != null && PCTBascic != "")
                {
                    double PCTB = Convert.ToDouble(PCTBascic.ToString()); dr["PCTBasic"] = PCTB;
                }
                else { dr["PCTBasic"] = PCTBascic; }

                dr["SpouseQuarter"] = master.SpouseQuarter;//.Equals("") ? DBNull.Value : (object)master.SpouseQuarter;

                dr["LWP"] = master.LWP;//.Equals("") ? DBNull.Value : (object)master.LWP;
                dr["IR"] = master.IR;//.Equals("") ? DBNull.Value : (object)master.LWP;
                dr["HealthSchemeDetails"] = master.HealthSchemeDetails;//.Equals("") ? DBNull.Value : (object)master.HealthSchemeDetails;
                ////Bank Details
                dr["BranchID"] = master.BranchID;//.Equals("") ? DBNull.Value : (object)master.BankID;
                dr["BankAccountCode"] = master.BankAccountCode;//.Equals("") ? DBNull.Value : (object)master.BankAccountCode;
                ////Phota and Signature
               
                dr["Photo"] = master.Photo;//.Equals("") ? DBNull.Value : (object)master.Photo;
                dr["Signature"] = master.Signature;//.Equals("") ? DBNull.Value : (object)master.Signature;

                int AdminID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                dr["AdminID"] = AdminID;

                //Others
                dr["SectorID"] = master.SectorID;//.Equals("") ? DBNull.Value : (object)master.SectorID;
                dr["CenterID"] = master.CenterID;//.Equals("") ? DBNull.Value : (object)master.CenterID;
                dr["InsertedBy"] = AdminID;
                              
                master.EmployeeID = maxValue;
                dtMaster.Rows.Add(dr);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static void InsertMasterforUpdate(DataTable dtMaster, EmployeeMasters master)
    {
       // try
        //{
            if (master != null && dtMaster != null)
            {
                DataRow dr = dtMaster.NewRow();
                // dr["EmployeeCode"] =  master.EmployeeCode;//.Equals("") ? DBNull.Value : (object)master.EmployeeCode;
                dr["EmployeeNo"] = master.EmployeeNo;//.Equals("") ? DBNull.Value : (object)master.EmployeeNo;
                dr["EmployeeType"] = master.EmployeeType;//.Equals("") ? DBNull.Value : (object)master.EmployeeType;
                dr["EmpClassification"] = master.EmpClassification;//.Equals("") ? DBNull.Value : (object)master.EmpClassification;
                dr["EmployeeName"] = master.EmployeeName;//.Equals("") ? DBNull.Value : (object)master.EmployeeName;
                dr["EmployeeReligion"] = master.EmployeeReligion;//.Equals("") ? DBNull.Value : (object)master.EmployeeReligion;
                dr["EmployeeQualification"] = master.EmployeeQualification;//.Equals("") ? DBNull.Value : (object)master.EmployeeQualification;
                dr["EmployeeMaritalStatus"] = master.EmployeeMaritalStatus;//.Equals("") ? DBNull.Value : (object)master.EmployeeMaritalStatus;
                dr["EmployeeCaste"] = master.EmployeeCaste;//.Equals("") ? DBNull.Value : (object)master.EmployeeCaste;

                string d = Convert.ToString(master.EmployeeDOB);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOB);
                if (d != null && d != "")
                {
                    DateTime dt = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EmployeeDOB"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EmployeeDOB"] = d; }

                dr["EmployeeGender"] = master.EmployeeGender;//.Equals("") ? DBNull.Value : (object)master.EmployeeGender;
                ////Address Information
                dr["EmployeePresentAddress"] = master.EmployeePresentAddress;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentAddress;
                dr["EmployeePresentState"] = master.EmployeePresentState;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentState;
                dr["EmployeePresentDistrict"] = master.EmployeePresentDistrict;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentDistrict;
                dr["EmployeePresentCity"] = master.EmployeePresentCity;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentCity;
                dr["EmployeePresentPin"] = master.EmployeePresentPin;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentPin;
                dr["EmployeePresentPhone"] = master.EmployeePresentPhone;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentPhone;
                dr["EmployeePresentMobile"] = master.EmployeePresentMobile;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentMobile;
                dr["EmployeePresentEmail"] = master.EmployeePresentEmail;//.Equals("") ? DBNull.Value : (object)master.EmployeePresentEmail;
                dr["EmployeePermanentAddress"] = master.EmployeePermanentAddress;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentAddress;
                dr["EmployeePermanentState"] = master.EmployeePermanentState;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentState;
                dr["EmployeePermanentDistrict"] = master.EmployeePermanentDistrict;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentDistrict;
                dr["EmployeePermanentCity"] = master.EmployeePermanentCity;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentCity;
                dr["EmployeePermanentPin"] = master.EmployeePermanentPin;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPin;
                dr["EmployeePermanentPhone"] = master.EmployeePermanentPhone;//.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPhone;
                //////Official Details
                string doj = Convert.ToString(master.EmployeeDOJ);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOJ);
                if (doj != null && doj != "")
                {
                    DateTime dt = DateTime.ParseExact(doj, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EmployeeDOJ"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EmployeeDOJ"] = doj; }

                string dor = Convert.ToString(master.EmployeeDOR);//.Equals("") ? DBNull.Value : (object)master.EmployeeDOR);
                if (dor != null && dor != "")
                {
                    DateTime dt = DateTime.ParseExact(dor, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EmployeeDOR"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EmployeeDOR"] = dor; }

                dr["EmployeeDesignation"] = master.EmployeeDesignation;//.Equals("") ? DBNull.Value : (object)master.EmployeeDesignation;
                dr["EmployeeCooperative"] = master.EmployeeCooperative;//.Equals("") ? DBNull.Value : (object)master.EmployeeCooperative;
                dr["EmployeeDA"] = master.EmployeeDA;//.Equals("") || master.EmployeeDA.Equals(null) ? DBNull.Value : (object)master.EmployeeDA;
                dr["PFCode"] = master.PFCode;//.Equals("") ? DBNull.Value : (object)master.PFCode;

                string dni = Convert.ToString(master.DNI);//.Equals("") ? DBNull.Value : (object)master.DNI);
                if (dni != null && dni != "")
                {
                    DateTime dt = DateTime.ParseExact(dni, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DNI"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DNI"] = dni; }

                dr["HRA"] = master.HRA;//.Equals("") ? DBNull.Value : (object)master.HRA;

                string efffrom = Convert.ToString(master.EffectiveFrom);//.Equals("") ? DBNull.Value : (object)master.EffectiveFrom);
                if (efffrom != null && efffrom != "")
                {
                    DateTime dt = DateTime.ParseExact(efffrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EffectiveFrom"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EffectiveFrom"] = efffrom; }

                dr["QuarterAllot"] = master.QuarterAllot;//.Equals("") ? DBNull.Value : (object)master.QuarterAllot;

                string OBasic = Convert.ToString(master.OLDBasic);//.Equals("") ? DBNull.Value : (object)master.OLDBasic);
                if (OBasic != null && OBasic != "")
                {
                    double oldBasic = Convert.ToDouble(OBasic.ToString()); dr["OLDBasic"] = oldBasic;
                }
                else
                {
                        dr["OLDBasic"] = OBasic;
                }

                string Lfees = Convert.ToString(master.LicenceFees);//.Equals("") ? DBNull.Value : (object)master.LicenceFees);
                if (Lfees != null && Lfees != "")
                {
                    double LicFees = Convert.ToDouble(Lfees.ToString()); dr["LicenceFees"] = LicFees;
                }
                else { dr["LicenceFees"] = Lfees; }

                string Samount = Convert.ToString(master.SpouseAmount);//.Equals("") ? DBNull.Value : (object)master.SpouseAmount);
                if (Samount != null && Samount != "")
                {
                    double SPamount = Convert.ToDouble(Samount.ToString()); dr["SpouseAmount"] = SPamount;
                }
                else { dr["SpouseAmount"] = Samount; }

                dr["HealthScheme"] = master.HealthScheme;//.Equals("") ? DBNull.Value : (object)master.HealthScheme;

                string DojP = Convert.ToString(master.DOJPresentDept);//.Equals("") ? DBNull.Value : (object)master.DOJPresentDept);
                if (DojP != null && DojP != "")
                {
                    DateTime dt = DateTime.ParseExact(DojP, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DOJPresentDept"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DOJPresentDept"] = DojP; }

                dr["PhysicallyChallenged"] = master.PhysicallyChallenged;//.Equals("") ? DBNull.Value : (object)master.PhysicallyChallenged;
                dr["Group"] = master.Group;//.Equals("") ? DBNull.Value : (object)master.Group;
                dr["Category"] = master.Category;//.Equals("") ? DBNull.Value : (object)master.Category;

                //********************Sup***************************************
                dr["DepartmentId"] = master.DepartmentId;
                dr["EmployerPfRate"] = master.EmployerPfRate;
                dr["EmployeePfRate"] = master.EmployeePfRate;


                dr["FathersName"] = master.FathersName;
                dr["BranchInCharge"] = master.BranchInCharge;
                dr["AddressRemarks"] = master.AddressRemarks;

                dr["PFRate"] = master.PFRate;


                //**************************************************************

                dr["Status"] = master.Status;//.Equals("") ? DBNull.Value : (object)master.Status;
                dr["PayScaleType"] = master.PayScaleType;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                dr["PayScale"] = master.PayScale;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                dr["CoopMembership"] = master.CoopMembership;//.Equals("") ? DBNull.Value : (object)master.PayScale;
                dr["HRACategory"] = master.HRACategory;//.Equals("") ? DBNull.Value : (object)master.HRACategory;
                dr["PANNO"] = master.PANNO;//.Equals("") ? DBNull.Value : (object)master.PANNO;

                string EffTo = Convert.ToString(master.EffectiveTo);//.Equals("") ? DBNull.Value : (object)master.EffectiveTo);
                if (EffTo != null && EffTo != "")
                {
                    DateTime dt = DateTime.ParseExact(EffTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["EffectiveTo"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["EffectiveTo"] = EffTo;  }

                dr["House"] = master.House;//.Equals("") ? DBNull.Value : (object)master.House;

                string PCTBascic = Convert.ToString(master.PCTBasic);//.Equals("") ? DBNull.Value : (object)master.PCTBasic);
                if (PCTBascic != null && PCTBascic != "")
                {
                    double PCTB = Convert.ToDouble(PCTBascic.ToString()); dr["PCTBasic"] = PCTB;
                }
                else { dr["PCTBasic"] = PCTBascic; }

                dr["SpouseQuarter"] = master.SpouseQuarter;//.Equals("") ? DBNull.Value : (object)master.SpouseQuarter;

                dr["LWP"] = master.LWP;//.Equals("") ? DBNull.Value : (object)master.LWP;
                dr["IR"] = master.IR;//.Equals("") ? DBNull.Value : (object)master.LWP;
                dr["HealthSchemeDetails"] = master.HealthSchemeDetails;//.Equals("") ? DBNull.Value : (object)master.HealthSchemeDetails;
                ////Bank Details
                dr["BranchID"] = master.BranchID;//.Equals("") ? DBNull.Value : (object)master.BankID;
                dr["BankAccountCode"] = master.BankAccountCode;//.Equals("") ? DBNull.Value : (object)master.BankAccountCode;
                ////Phota and Signature

                dr["Photo"] = master.Photo;//.Equals("") ? DBNull.Value : (object)master.Photo;
                dr["Signature"] = master.Signature;//.Equals("") ? DBNull.Value : (object)master.Signature;

                int AdminID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                dr["AdminID"] = AdminID;

                //Others
                dr["SectorID"] = master.SectorID;//.Equals("") ? DBNull.Value : (object)master.SectorID;
                dr["CenterID"] = master.CenterID;//.Equals("") ? DBNull.Value : (object)master.CenterID;
                dr["InsertedBy"] = AdminID;

                dtMaster.Rows.Add(dr);
            }
        //}
        //catch (Exception ex)
        //{
            //throw new Exception(ex.Message);

        //}
    }
    public static void UpdateMasterforUpdate(DataTable dtMaster, EmployeeMasters master)
    {
        try
        {
            if (master != null && dtMaster != null)
            {
                DataRow[] dr1 = dtMaster.Select("EmployeeID=" + master.EmployeeID);
                if (dr1.Length > 0)
                {
                    foreach (DataRow dr in dr1)
                    {
                        //DataRow dr = dtMaster.NewRow();
                        dr["EmployeeCode"] = master.EmployeeCode.Equals("") ? DBNull.Value : (object)master.EmployeeCode;
                        dr["EmployeeNo"] = master.EmployeeNo.Equals("") ? DBNull.Value : (object)master.EmployeeNo;
                        dr["EmployeeType"] = master.EmployeeType.Equals("") ? DBNull.Value : (object)master.EmployeeType;
                        dr["EmpClassification"] = master.EmpClassification.Equals("") ? DBNull.Value : (object)master.EmpClassification;
                        dr["EmployeeName"] = master.EmployeeName.Equals("") ? DBNull.Value : (object)master.EmployeeName;
                        dr["EmployeeReligion"] = master.EmployeeReligion.Equals("") ? DBNull.Value : (object)master.EmployeeReligion;
                        dr["EmployeeQualification"] = master.EmployeeQualification.Equals("") ? DBNull.Value : (object)master.EmployeeQualification;
                        dr["EmployeeMaritalStatus"] = master.EmployeeMaritalStatus.Equals("") ? DBNull.Value : (object)master.EmployeeMaritalStatus;
                        dr["EmployeeCaste"] = master.EmployeeCaste.Equals("") ? DBNull.Value : (object)master.EmployeeCaste;

                        string d = Convert.ToString(master.EmployeeDOB.Equals("") ? DBNull.Value : (object)master.EmployeeDOB);
                        if (d != null && d != "")
                        {
                            DateTime dt = DateTime.ParseExact(d, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EmployeeDOB"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EmployeeDOB"] = d; }

                        dr["EmployeeGender"] = master.EmployeeGender.Equals("") ? DBNull.Value : (object)master.EmployeeGender;
                        ////Address Information
                        dr["EmployeePresentAddress"] = master.EmployeePresentAddress.Equals("") ? DBNull.Value : (object)master.EmployeePresentAddress;
                        dr["EmployeePresentState"] = master.EmployeePresentState.Equals("") ? DBNull.Value : (object)master.EmployeePresentState;
                        dr["EmployeePresentDistrict"] = master.EmployeePresentDistrict.Equals("") ? DBNull.Value : (object)master.EmployeePresentDistrict;
                        dr["EmployeePresentCity"] = master.EmployeePresentCity.Equals("") ? DBNull.Value : (object)master.EmployeePresentCity;
                        dr["EmployeePresentPin"] = master.EmployeePresentPin.Equals("") ? DBNull.Value : (object)master.EmployeePresentPin;
                        dr["EmployeePresentPhone"] = master.EmployeePresentPhone.Equals("") ? DBNull.Value : (object)master.EmployeePresentPhone;
                        dr["EmployeePresentMobile"] = master.EmployeePresentMobile.Equals("") ? DBNull.Value : (object)master.EmployeePresentMobile;
                        dr["EmployeePresentEmail"] = master.EmployeePresentEmail.Equals("") ? DBNull.Value : (object)master.EmployeePresentEmail;
                        dr["EmployeePermanentAddress"] = master.EmployeePermanentAddress.Equals("") ? DBNull.Value : (object)master.EmployeePermanentAddress;
                        dr["EmployeePermanentState"] = master.EmployeePermanentState.Equals("") ? DBNull.Value : (object)master.EmployeePermanentState;
                        dr["EmployeePermanentDistrict"] = master.EmployeePermanentDistrict.Equals("") ? DBNull.Value : (object)master.EmployeePermanentDistrict;
                        dr["EmployeePermanentCity"] = master.EmployeePermanentCity.Equals("") ? DBNull.Value : (object)master.EmployeePermanentCity;
                        dr["EmployeePermanentPin"] = master.EmployeePermanentPin.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPin;
                        dr["EmployeePermanentPhone"] = master.EmployeePermanentPhone.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPhone;
                        //////Official Details
                        string doj = Convert.ToString(master.EmployeeDOJ.Equals("") ? DBNull.Value : (object)master.EmployeeDOJ);
                        if (doj != null && doj != "")
                        {
                            DateTime dt = DateTime.ParseExact(doj, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EmployeeDOJ"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EmployeeDOJ"] = doj; }

                        string dor = Convert.ToString(master.EmployeeDOR.Equals("") ? DBNull.Value : (object)master.EmployeeDOR);
                        if (dor != null && dor != "")
                        {
                            DateTime dt = DateTime.ParseExact(dor, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EmployeeDOR"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EmployeeDOR"] = dor; }

                        dr["EmployeeDesignation"] = master.EmployeeDesignation.Equals("") ? DBNull.Value : (object)master.EmployeeDesignation;
                        dr["EmployeeCooperative"] = master.EmployeeCooperative.Equals("") ? DBNull.Value : (object)master.EmployeeCooperative;
                        dr["EmployeeDA"] = master.EmployeeDA.Equals("") || master.EmployeeDA.Equals(null) ? DBNull.Value : (object)master.EmployeeDA;
                        dr["PFCode"] = master.PFCode.Equals("") ? DBNull.Value : (object)master.PFCode;

                        string dni = Convert.ToString(master.DNI.Equals("") ? DBNull.Value : (object)master.DNI);
                        if (dni != null && dni != "")
                        {
                            DateTime dt = DateTime.ParseExact(dni, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["DNI"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["DNI"] = dni; }

                        dr["HRA"] = master.HRA.Equals("") ? DBNull.Value : (object)master.HRA;

                        string efffrom = Convert.ToString(master.EffectiveFrom.Equals("") ? DBNull.Value : (object)master.EffectiveFrom);
                        if (efffrom != null && efffrom != "")
                        {
                            DateTime dt = DateTime.ParseExact(efffrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EffectiveFrom"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EffectiveFrom"] = efffrom; }

                        dr["QuarterAllot"] = master.QuarterAllot.Equals("") ? DBNull.Value : (object)master.QuarterAllot;

                        string OBasic = Convert.ToString(master.OLDBasic.Equals("") ? DBNull.Value : (object)master.OLDBasic);
                        if (OBasic != null && OBasic != "")
                        {
                            double oldBasic = Convert.ToDouble(OBasic.ToString()); dr["OLDBasic"] = oldBasic;
                        }
                        else { dr["OLDBasic"] = OBasic; }

                        string Lfees = Convert.ToString(master.LicenceFees.Equals("") ? DBNull.Value : (object)master.LicenceFees);
                        if (Lfees != null && Lfees != "")
                        {
                            double LicFees = Convert.ToDouble(Lfees.ToString()); dr["LicenceFees"] = LicFees;
                        }
                        else { dr["LicenceFees"] = Lfees; }

                        string Samount = Convert.ToString(master.SpouseAmount.Equals("") ? DBNull.Value : (object)master.SpouseAmount);
                        if (Samount != null && Samount != "")
                        {
                            double SPamount = Convert.ToDouble(Samount.ToString()); dr["SpouseAmount"] = SPamount;
                        }
                        else { dr["SpouseAmount"] = Samount; }

                        dr["HealthScheme"] = master.HealthScheme.Equals("") ? DBNull.Value : (object)master.HealthScheme;

                        string DojP = Convert.ToString(master.DOJPresentDept.Equals("") ? DBNull.Value : (object)master.DOJPresentDept);
                        if (DojP != null && DojP != "")
                        {
                            DateTime dt = DateTime.ParseExact(DojP, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["DOJPresentDept"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["DOJPresentDept"] = DojP; }

                        dr["PhysicallyChallenged"] = master.PhysicallyChallenged.Equals("") ? DBNull.Value : (object)master.PhysicallyChallenged;
                        dr["Group"] = master.Group.Equals("") ? DBNull.Value : (object)master.Group;
                        dr["Category"] = master.Category.Equals("") ? DBNull.Value : (object)master.Category;

                        //********************Sup***************************************
                        dr["DepartmentId"] = master.DepartmentId.Equals("") ? 0 : (object)master.DepartmentId;
                        dr["EmployerPfRate"] = master.EmployerPfRate.Equals("") ? DBNull.Value : (object)master.EmployerPfRate;
                        dr["EmployeePfRate"] = master.EmployeePfRate.Equals("") ? DBNull.Value : (object)master.EmployeePfRate;


                        dr["FathersName"] = master.FathersName.Equals("") ? 0 : (object)master.FathersName;
                        dr["BranchInCharge"] = master.BranchInCharge.Equals("") ? 0 : (object)master.BranchInCharge;
                        dr["AddressRemarks"] = master.AddressRemarks.Equals("") ? 0 : (object)master.AddressRemarks;

                        dr["PFRate"] = master.PFRate.Equals("") ? 0 : (object)master.PFRate;

                        //**************************************************************

                        dr["Status"] = master.Status.Equals("") ? DBNull.Value : (object)master.Status;
                        dr["PayScaleType"] = master.PayScaleType.Equals("") ? DBNull.Value : (object)master.PayScaleType;
                        dr["PayScale"] = master.PayScale.Equals("") ? DBNull.Value : (object)master.PayScale;
                        dr["CoopMembership"] = master.CoopMembership.Equals("") ? DBNull.Value : (object)master.CoopMembership;
                        dr["HRACategory"] = master.HRACategory.Equals("") ? DBNull.Value : (object)master.HRACategory;
                        dr["PANNO"] = master.PANNO.Equals("") ? DBNull.Value : (object)master.PANNO;

                        string EffTo = Convert.ToString(master.EffectiveTo.Equals("") ? DBNull.Value : (object)master.EffectiveTo);
                        if (EffTo != null && EffTo != "")
                        {
                            DateTime dt = DateTime.ParseExact(EffTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dr["EffectiveTo"] = dt.ToString("MM/dd/yyyy");
                        }
                        else { dr["EffectiveTo"] = EffTo; }

                        dr["House"] = master.House.Equals("") ? DBNull.Value : (object)master.House;

                        string PCTBascic = Convert.ToString(master.PCTBasic.Equals("") ? DBNull.Value : (object)master.PCTBasic);
                        if (PCTBascic != null && PCTBascic != "")
                        {
                            double PCTB = Convert.ToDouble(PCTBascic.ToString()); dr["PCTBasic"] = PCTB;
                        }
                        else { dr["PCTBasic"] = PCTBascic; }

                        dr["SpouseQuarter"] = master.SpouseQuarter.Equals("") ? DBNull.Value : (object)master.SpouseQuarter;

                        dr["LWP"] = master.LWP.Equals("") ? DBNull.Value : (object)master.LWP;
                        dr["HealthSchemeDetails"] = master.HealthSchemeDetails.Equals("") ? DBNull.Value : (object)master.HealthSchemeDetails;
                        ////Bank Details
                        dr["BranchID"] = master.BranchID.Equals("") ? DBNull.Value : (object)master.BranchID;
                        dr["BankAccountCode"] = master.BankAccountCode.Equals("") ? DBNull.Value : (object)master.BankAccountCode;
                        ////Phota and Signature
                        dr["Photo"] = master.Photo.Equals("") ? DBNull.Value : (object)master.Photo;
                        dr["Signature"] = master.Signature.Equals("") ? DBNull.Value : (object)master.Signature;

                        int AdminID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                        dr["AdminID"] = AdminID;

                        //Others
                        dr["SectorID"] = master.SectorID.Equals("") ? DBNull.Value : (object)master.SectorID;

                        dtMaster.Rows.Add(dr);
                    }
                }
            } 
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static void UpdatePayDetails(DataTable dtPayDetails, EmployeePayDetails payDetails)
    {
        try
        {
            if (payDetails != null && dtPayDetails != null)
            {
                DataRow[] dr1 = dtPayDetails.Select("EmployeePayDetailID=" + payDetails.EmployeePayDetailID);
                if (dr1.Length > 0)
                {
                    foreach (DataRow dr in dr1)
                    {
                        dr["EmployeeName"] = payDetails.EmployeeID;
                        dr["EarnDeduction"] = payDetails.EarnDeductionID;
                        dr["Amount"] = payDetails.Amount;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static void InsertPayDetail(DataTable dtPayDetails)
    {
        try
        {
            if (dtPayDetails.Rows.Count > 0)
            {
                    for (int i = 0; i < dtPayDetails.Rows.Count; i++)
                    {
                        DataRow dr = dtPayDetails.NewRow();
                        object maxval = (dtPayDetails.Compute("max(EmployeePayDetailID)", String.Empty));
                        int maxValue = 1;
                        if (!(maxval is DBNull))
                        {
                            maxValue = (Int32)maxval + 1;
                        }

                        //dr["EmployeePayDetailID"] = dtPayDetails.Rows[i]["EmployeePayDetailID"] = maxValue;
                        //dr["EmployeeID"] = aep.EmployeeID;//.Equals("") ? DBNull.Value : (object)payDetails.EmployeeID;
                        //dr["EarnDeductionID"] = aep.EarnDeductionID;
                        //dr["Amount"] = aep.Amount;

                        //aep.EmployeePayDetailID = maxValue;
                        //dtPayDetails.Rows.Add(dr);
                        dtPayDetails.Rows[i]["EmployeePayDetailID"] = maxValue;
                    }
                }
        }

        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static int SaveMaster(DataTable dtMaster, EmployeeMasters master, int AdminID)
    {
        int EmployeeID = 0;
        try
        {
            if (dtMaster != null && master != null)
            {
                DataTable dt = DataAccess.DBHandler.GetResult("Insert_EmpMaster",
                    //Personal Information 
                   DBNull.Value,
                   master.EmployeeNo.Equals("") ? DBNull.Value : (object)master.EmployeeNo,
                   master.EmployeeType.Equals("") ? DBNull.Value : (object)master.EmployeeType,
                   master.EmpClassification.Equals("") ? 0 : (object)master.EmpClassification,
                   master.EmployeeName.Equals("") ? DBNull.Value : (object)master.EmployeeName,
                   master.EmployeeReligion.Equals("") ? DBNull.Value : (object)master.EmployeeReligion,
                   master.EmployeeQualification.Equals("") ? DBNull.Value : (object)master.EmployeeQualification,
                   master.EmployeeMaritalStatus.Equals("") ? DBNull.Value : (object)master.EmployeeMaritalStatus,
                   master.EmployeeCaste.Equals("") ? DBNull.Value : (object)master.EmployeeCaste,
                   master.EmployeeDOB.Equals("") ? DBNull.Value : (object)master.EmployeeDOB,
                   master.EmployeeGender.Equals("") ? DBNull.Value : (object)master.EmployeeGender,
                    //Address Information 
                   master.EmployeePresentAddress.Equals("") ? DBNull.Value : (object)master.EmployeePresentAddress,
                   master.EmployeePresentState.Equals("") ? DBNull.Value : (object)master.EmployeePresentState,
                   master.EmployeePresentDistrict.Equals("") ? DBNull.Value : (object)master.EmployeePresentDistrict,
                   master.EmployeePresentCity.Equals("") ? DBNull.Value : (object)master.EmployeePresentCity,
                   master.EmployeePresentPin.Equals("") ? DBNull.Value : (object)master.EmployeePresentPin,
                   master.EmployeePresentPhone.Equals("") ? DBNull.Value : (object)master.EmployeePresentPhone,
                   master.EmployeePresentMobile.Equals("") ? DBNull.Value : (object)master.EmployeePresentMobile,
                   master.EmployeePresentEmail.Equals("") ? DBNull.Value : (object)master.EmployeePresentEmail,
                   master.EmployeePermanentAddress.Equals("") ? DBNull.Value : (object)master.EmployeePermanentAddress,
                   master.EmployeePermanentState.Equals("") ? DBNull.Value : (object)master.EmployeePermanentState,
                   master.EmployeePermanentDistrict.Equals("") ? DBNull.Value : (object)master.EmployeePermanentDistrict,
                   master.EmployeePermanentCity.Equals("") ? DBNull.Value : (object)master.EmployeePermanentCity,
                   master.EmployeePermanentPin.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPin,
                   master.EmployeePermanentPhone.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPhone,
                    //Official Details   
                   master.EmployeeDOJ.Equals("") ? DBNull.Value : (object)master.EmployeeDOJ,
                   master.DOJPresentDept.Equals("") ? DBNull.Value : (object)master.DOJPresentDept,
                   master.EmployeeDOR.Equals("") ? DBNull.Value : (object)master.EmployeeDOR,
                   master.PhysicallyChallenged.Equals("") ? DBNull.Value : (object)master.PhysicallyChallenged,
                   master.EmployeeDesignation.Equals("") ? DBNull.Value : (object)master.EmployeeDesignation,
                   master.Group.Equals("") ? DBNull.Value : (object)master.Group,
                   master.EmployeeCooperative.Equals("") ? DBNull.Value : (object)master.EmployeeCooperative,
                   master.Category.Equals("") ? DBNull.Value : (object)master.Category,

                   //******************Sup************************************

                   master.DepartmentId.Equals("") ? 0 : (object)master.DepartmentId,
                   master.EmployeePfRate.Equals("") ? DBNull.Value:(object)master.EmployeePfRate,
                   master.EmployerPfRate.Equals("") ? DBNull.Value:(object)master.EmployerPfRate,

                   master.FathersName.Equals("") ? DBNull.Value : (object)master.FathersName,
                   master.AddressRemarks.Equals("") ? DBNull.Value : (object)master.AddressRemarks,
                   master.BranchInCharge.Equals("") ? DBNull.Value : (object)master.BranchInCharge,

                   master.PFRate.Equals("") ? DBNull.Value : (object)master.PFRate,


                   //*********************************************************
                   master.EmployeeDA.Equals("") ? DBNull.Value : (object)master.EmployeeDA,
                   master.Status.Equals("") ? DBNull.Value : (object)master.Status,
                   master.PFCode.Equals("") ? DBNull.Value : (object)master.PFCode,
                   master.PayScaleType.Equals("") ? DBNull.Value : (object)master.PayScaleType,
                   master.PayScale.Equals("") ? DBNull.Value : (object)master.PayScale,
                   master.CoopMembership.Equals("") ? DBNull.Value : (object)master.CoopMembership,
                   master.DNI.Equals("") ? DBNull.Value : (object)master.DNI,
                   master.HRACategory.Equals("") ? DBNull.Value : (object)master.HRACategory,
                   master.HRAFlag.Equals("") ? DBNull.Value : (object)master.HRAFlag,
                   master.HRAFixedAmount.Equals("") ? DBNull.Value : (object)master.HRAFixedAmount,
                   master.HRA.Equals("") ? DBNull.Value : (object)master.HRA,
                   master.PANNO.Equals("") ? DBNull.Value : (object)master.PANNO,
                   master.EffectiveFrom.Equals("") ? DBNull.Value : (object)master.EffectiveFrom,
                   master.EffectiveTo.Equals("") ? DBNull.Value : (object)master.EffectiveTo,
                   master.QuarterAllot.Equals("") ? DBNull.Value : (object)master.QuarterAllot,
                   master.House.Equals("") ? DBNull.Value : (object)master.House,
                   master.OLDBasic.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.OLDBasic),
                   master.PCTBasic.Equals("") ? DBNull.Value : (object)master.PCTBasic,
                   master.LicenceFees.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.LicenceFees),
                   master.SpouseQuarter.Equals("") ? DBNull.Value : (object)master.SpouseQuarter,
                   master.SpouseAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.SpouseAmount),
                   master.LWP.Equals("") ? DBNull.Value : (object)master.LWP,
                   master.IR.Equals("") ? DBNull.Value : (object)master.IR,
                   master.HealthScheme.Equals("") ? DBNull.Value : (object)master.HealthScheme,
                   master.HealthSchemeDetails.Equals("") ? DBNull.Value : (object)master.HealthSchemeDetails,
                    //Bank Details 
                   master.BranchID.Equals("") ? DBNull.Value : (object)master.BranchID,
                   master.BankAccountCode.Equals("") ? DBNull.Value : (object)master.BankAccountCode,
                    //Phota and Signature  
                   master.Photo.Equals("") ? DBNull.Value : (object)master.Photo,
                   master.Signature.Equals("") ? DBNull.Value : (object)master.Signature,
                   AdminID,
                    //Others
                   master.SectorID.Equals("") ? DBNull.Value : (object)master.SectorID,
                   master.CenterID.Equals("") ? DBNull.Value : (object)master.CenterID,
                   AdminID);
                                    

                EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());
                int OldEmployeeID = master.EmployeeID;
                master.EmployeeID = EmployeeID;
                DataRow[] dr1 = dtMaster.Select("EmployeeID=" + OldEmployeeID);
                if (dr1.Length > 0)
                {
                    foreach (DataRow dr in dr1)
                    {
                        dr["EmployeeID"] = EmployeeID;
                    }
                }
            }
        }
          catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return EmployeeID;
    }
    public static int SaveMasterforUpdate(DataTable dtMaster, EmployeeMasters master, int AdminID, string Remarks)
    {
        int EmployeeID = 0;
        //try
        //{

            if (dtMaster != null && master != null)
            {

                DataTable dt = DataAccess.DBHandler.GetResult("Update_EmpMaster",
                    //Personal Information 
                   master.EmployeeID.Equals("") ? DBNull.Value : (object)master.EmployeeID,
                   DBNull.Value,
                   master.EmployeeNo.Equals("") ? DBNull.Value : (object)master.EmployeeNo,
                   master.EmployeeType.Equals("") ? DBNull.Value : (object)master.EmployeeType,
                   master.EmpClassification.Equals("") ? DBNull.Value : (object)master.EmpClassification,
                   master.EmployeeName.Equals("") ? DBNull.Value : (object)master.EmployeeName,
                   master.EmployeeReligion.Equals("") ? DBNull.Value : (object)master.EmployeeReligion,
                   master.EmployeeQualification.Equals("") ? DBNull.Value : (object)master.EmployeeQualification,
                   master.EmployeeMaritalStatus.Equals("") ? DBNull.Value : (object)master.EmployeeMaritalStatus,
                   master.EmployeeCaste.Equals("") ? DBNull.Value : (object)master.EmployeeCaste,
                   master.EmployeeDOB.Equals("") ? DBNull.Value : (object)toSqlDate("dd/MM/yyyy",master.EmployeeDOB),//date
                   master.EmployeeGender.Equals("") ? DBNull.Value : (object)master.EmployeeGender,
                    //Address Information 
                   master.EmployeePresentAddress.Equals("") ? DBNull.Value : (object)master.EmployeePresentAddress,
                   master.EmployeePresentState.Equals("") ? DBNull.Value : (object)master.EmployeePresentState,
                   master.EmployeePresentDistrict.Equals("") ? DBNull.Value : (object)master.EmployeePresentDistrict,
                   master.EmployeePresentCity.Equals("") ? DBNull.Value : (object)master.EmployeePresentCity,
                   master.EmployeePresentPin.Equals("") ? DBNull.Value : (object)master.EmployeePresentPin,
                   master.EmployeePresentPhone.Equals("") ? DBNull.Value : (object)master.EmployeePresentPhone,
                   master.EmployeePresentMobile.Equals("") ? DBNull.Value : (object)master.EmployeePresentMobile,
                   master.EmployeePresentEmail.Equals("") ? DBNull.Value : (object)master.EmployeePresentEmail,
                   master.EmployeePermanentAddress.Equals("") ? DBNull.Value : (object)master.EmployeePermanentAddress,
                   master.EmployeePermanentState.Equals("") ? DBNull.Value : (object)master.EmployeePermanentState,
                   master.EmployeePermanentDistrict.Equals("") ? DBNull.Value : (object)master.EmployeePermanentDistrict,
                   master.EmployeePermanentCity.Equals("") ? DBNull.Value : (object)master.EmployeePermanentCity,
                   master.EmployeePermanentPin.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPin,
                   master.EmployeePermanentPhone.Equals("") ? DBNull.Value : (object)master.EmployeePermanentPhone,
                    //Official Details   
                   master.EmployeeDOJ.Equals("") ? DBNull.Value : (object)toSqlDate("dd/MM/yyyy",master.EmployeeDOJ),//date
                   master.DOJPresentDept.Equals("") ? DBNull.Value : (object)toSqlDate("dd/MM/yyyy",master.DOJPresentDept),//date
                   master.EmployeeDOR.Equals("") ? DBNull.Value : (object)toSqlDate("dd/MM/yyyy",master.EmployeeDOR),//date
                   master.PhysicallyChallenged.Equals("") ? DBNull.Value : (object)master.PhysicallyChallenged,
                   master.EmployeeDesignation.Equals("") ? DBNull.Value : (object)master.EmployeeDesignation,
                   master.Group.Equals("") ? DBNull.Value : (object)master.Group,
                   master.EmployeeCooperative.Equals("") ? DBNull.Value : (object)master.EmployeeCooperative,
                   //master.Category.Equals("") ? DBNull.Value : (object)master.Category,

                   //******************Sup************************************

                   master.DepartmentId.Equals("") ? 0 : (object)master.DepartmentId,
                   master.EmployeePfRate.Equals("") ? DBNull.Value : (object)master.EmployeePfRate,
                   master.EmployerPfRate.Equals("") ? DBNull.Value : (object)master.EmployerPfRate,

                   master.FathersName.Equals("") ? DBNull.Value : (object)master.FathersName,
                   master.BranchInCharge.Equals("") ? DBNull.Value : (object)master.BranchInCharge,
                   master.AddressRemarks.Equals("") ? DBNull.Value : (object)master.AddressRemarks,

                   master.PFRate.Equals("") ? DBNull.Value : (object)master.PFRate,

                   //*********************************************************

                   master.EmployeeDA.Equals("") ? DBNull.Value : (object)master.EmployeeDA,
                   master.Status.Equals("") ? DBNull.Value : (object)master.Status,
                   master.PFCode.Equals("") ? DBNull.Value : (object)master.PFCode,
                   master.PayScaleType.Equals("") ? DBNull.Value : (object)master.PayScaleType,
                   master.PayScale.Equals("") ? DBNull.Value : (object)master.PayScale,
                   master.CoopMembership.Equals("") ? DBNull.Value : (object)master.CoopMembership,
                   master.DNI.Equals("") ? DBNull.Value : (object)master.DNI,
                   master.HRACategory.Equals("") ? DBNull.Value : (object)master.HRACategory,
                   master.HRAFlag.Equals("") ? DBNull.Value : (object)master.HRAFlag,
                   master.HRAFixedAmount.Equals("") ? 0 : (object)master.HRAFixedAmount,
                   master.HRA.Equals("") ? DBNull.Value : (object)master.HRA,
                   master.PANNO.Equals("") ? DBNull.Value : (object)master.PANNO,


                //-------------------------
                   master.EffectiveFrom.Equals("") ? DBNull.Value : (object)toSqlDate("dd/MM/yyyy",master.EffectiveFrom),//date
                   master.EffectiveTo.Equals("") ? DBNull.Value : (object)toSqlDate("dd/MM/yyyy",master.EffectiveTo),//date
                   master.QuarterAllot.Equals("") ? DBNull.Value : (object)master.QuarterAllot,
                   master.House.Equals("") ? DBNull.Value : (object)master.House,
                   master.OLDBasic.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.OLDBasic),
                   master.PCTBasic.Equals("") ? DBNull.Value : (object)master.PCTBasic,
                   master.LicenceFees.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.LicenceFees),
                   master.SpouseQuarter.Equals("") ? DBNull.Value : (object)master.SpouseQuarter,
                   master.SpouseAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.SpouseAmount),
                   master.LWP.Equals("") ? DBNull.Value : (object)master.LWP,
                   master.IR.Equals("") ? DBNull.Value : (object)master.IR,
                   master.HealthScheme.Equals("") ? DBNull.Value : (object)master.HealthScheme,
                   master.HealthSchemeDetails.Equals("") ? DBNull.Value : (object)master.HealthSchemeDetails,
                   // //Bank Details 
                   master.BranchID.Equals("") ? DBNull.Value : (object)master.BranchID,
                   master.BankAccountCode.Equals("") ? DBNull.Value : (object)master.BankAccountCode,
                    //Phota and Signature  
                   master.Photo.Equals("") ? DBNull.Value : (object)master.Photo,
                   master.Signature.Equals("") ? DBNull.Value : (object)master.Signature,
                   AdminID,
                    //Others
                   master.SectorID.Equals("") ? DBNull.Value : (object)master.SectorID,
                   master.CenterID.Equals("") ? DBNull.Value : (object)master.CenterID,
                   AdminID,
                   Remarks.Equals("") ? DBNull.Value : (object)Remarks);

                EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());
            }
        //}
        //catch (Exception ex)
        //{
        //    throw new Exception(ex.Message);
        //}
        return EmployeeID;
    }
    static string toSqlDate(string curDdateFormat,string DateValue)
    {
        if (string.IsNullOrEmpty(DateValue))
        {
            return "";
        }
        else
        {
            return DateTime.ParseExact(DateValue, curDdateFormat, System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd");
        }
    }
    public static int SavePayDetails(DataTable dtPayDetails, int EmoloyeeID, int AdminID)
    {
        int EmployeePayDetailID = 0;
        try
        {
            if (dtPayDetails.Rows.Count > 0 || dtPayDetails.Rows.Count == 0)
            {
                DataTable dtresult = DataAccess.DBHandler.GetResult("Delete_PayDetails", EmoloyeeID);
                for (int i = 0; i < dtPayDetails.Rows.Count; i++)
                {
                    int EarnDeductionID = Convert.ToInt32(dtPayDetails.Rows[i]["EarnDeductionID"].Equals("") ? DBNull.Value : (object)dtPayDetails.Rows[i]["EarnDeductionID"].ToString());
                    DataTable dt = DataAccess.DBHandler.GetResult("Insert_PayDetails",
                    EmoloyeeID,
                    EarnDeductionID,
                    dtPayDetails.Rows[i]["Amount"].Equals("") ? DBNull.Value : (object)dtPayDetails.Rows[i]["Amount"],
                    AdminID);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return EmployeePayDetailID;
    }
    public static void DeletePayDetail(DataTable dtPayDetails, int EarnDeductionID, int noofrows)
    {
        try
        {
                if (EarnDeductionID == 2)
                {
                    //if (dtPayDetails.Rows.Count !=5 || dtPayDetails.Rows.Count != 6)
                    //{
                    //for (int i = 0; i < dtPayDetails.Rows.Count; i++)
                    //{
                    //    int edid = Convert.ToInt32(dtPayDetails.Rows[i]["EarnDeductionID"]);
                    //    if (edid == 2 || edid == 9 || edid == 3 || edid == 4 || edid == 5 || edid == 18)
                    //    {
                            DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                            foreach (DataRow row in dr1)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr2 = dtPayDetails.Select("EarnDeductionID=" + 9);
                            foreach (DataRow row in dr2)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr3 = dtPayDetails.Select("EarnDeductionID=" + 3);
                            foreach (DataRow row in dr3)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr4 = dtPayDetails.Select("EarnDeductionID=" + 4);
                            foreach (DataRow row in dr4)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr5 = dtPayDetails.Select("EarnDeductionID=" + 5);
                            foreach (DataRow row in dr5)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr6 = dtPayDetails.Select("EarnDeductionID=" + 18);
                            foreach (DataRow row in dr6)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr7 = dtPayDetails.Select("EarnDeductionID=" + 7);
                            foreach (DataRow row in dr7)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            dtPayDetails.AcceptChanges();
                        //}
                

                }
                else
                {
                    DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                    foreach (DataRow row in dr1)
                    {
                        if (row.RowState != DataRowState.Deleted)
                        {
                                row.Delete();
                                dtPayDetails.AcceptChanges();
                        }
                    }
                }
             }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
    
    }
    public static void DeletePayDetailbySuspended(DataTable dtPayDetails, int EarnDeductionID)
    {
        try
        {

                DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                foreach (DataRow row in dr1)
                {
                    if (row.RowState != DataRowState.Deleted)
                    {
                        row.Delete();
                        dtPayDetails.AcceptChanges();
                    }
                }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }

    }
    public static void UpdatePayDetail(DataTable dtPayDetails, int EDID, string EDName, string amount)
    {
        try
        {

            try
            {

                DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EDID);
                foreach (DataRow row in dr1)
                {
                    row.Delete();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        //return TotBooking;
    }
    public static string UpdateCoOperativeDetails(string id, int EDID, string Amount)
    {
        string Result = ""; DataTable dtres = new DataTable();
        try
        {
            dtres = DBHandler.GetResult("Update_CoOperativeCal", id, EDID, Amount);
            if (dtres.Rows.Count > 0)
            {
                Result = "success";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Result;
    }

    public static DataTable CreatePopUpStatusTable(DataTable dtPopUpStatus)
    {
        try
        {
            dtPopUpStatus.Columns.Add(new DataColumn("Status", typeof(System.String)));
            dtPopUpStatus.Columns.Add(new DataColumn("OrderNo", typeof(System.String)));
            dtPopUpStatus.Columns.Add(new DataColumn("OrderDate", typeof(System.String)));
            dtPopUpStatus.Columns.Add(new DataColumn("Remarks", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtPopUpStatus;
    }

	public EmployeeDataHandling()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}