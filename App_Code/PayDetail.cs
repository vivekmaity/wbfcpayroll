﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PayDetail
/// </summary>
public class PayDetails
{
    public int PayDetailID { get; set; }
    public int EmployeeID { get; set; }
    public int EDID { get; set; }
    public double Amount { get; set; }
    public EmployeeDetails[] empDetail;

	public PayDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}