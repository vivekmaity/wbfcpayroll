﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NomineeDetails
/// </summary>
public class Nominee
{
    public int EmployeeID { get; set; }
    public int NomineeID { get; set; }
    public string NomineeTypeID { get; set; }
    public string NomineeType { get; set; }
    public string NomineeName { get; set; }
    public string NomineeRelationID { get; set; }
    public string NomineeRelation { get; set; }
    public string NomineeDOB { get; set; }
    public string NomineeAddress { get; set; }
    public string NomineeState { get; set; }
    public string NomineeDistrict { get; set; }
    public string NomineeCity { get; set; }
    public string NomineePin { get; set; }
    public string NomineePhone { get; set; }
    public string NomineeIDMark { get; set; }
    public string FamilyPension { get; set; }
    public string GratuityPerct { get; set; }

	public Nominee()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}