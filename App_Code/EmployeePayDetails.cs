﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmployeePayDetails
/// </summary>
public class EmployeePayDetails
{
    public int EmployeePayDetailID { get; set; }
    public int EmployeeID { get; set; }
    public int EarnDeductionID { get; set; }
    public string Amount { get; set; }
	public EmployeePayDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}