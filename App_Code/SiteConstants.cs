﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SiteConstants
/// </summary>

public class SiteConstants
{
    private static string _ConnectingString;
    public const string SSN_DR_USER_DETAILS = "UserDetails";
    public const string SSN_INT_USER_ID = "UserID";
    public const string SSN_PARAMS = "Params";
    public const string SSN_SALFIN = "SalFinYear";
    public const string SSN_SALACC = "SalAccYear";
    public const string SSN_SECTORID = "SectorID";
    public const string SSN_PAYMONTHID = "PayMonthID";
    public const string SSN_USERTYPECODE = "User_TypeCode";

    public const string StrFormName = "FormName";
    public const string StrReportName = "ReportName";
    public const string StrReportType = "ReportType";

    public static string ConnectingString
    {
        get { return SiteConstants._ConnectingString; }
        set { SiteConstants._ConnectingString = value; }
    }
    public SiteConstants()
    {

    }
}

