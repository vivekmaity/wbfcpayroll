﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AddEmployeePayDetails
/// </summary>
public class AddEmployeePayDetails
{
    public int EmployeePayDetailID { get; set; }
    public int EmployeeID { get; set; }
    public int EarnDeductionID { get; set; }
    public string EarnDeduction { get; set; }
    public string Amount { get; set; }
    public string EarnDeductionType { get; set; }

	public AddEmployeePayDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}