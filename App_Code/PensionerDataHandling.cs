﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

/// <summary>
/// Summary description for EmployeeDataHandling
/// </summary>
public class PensionerDataHandling
{
    public static DataTable CreatePensionMasterTable(DataTable dtPenMaster)
    {
        try
        {
            //File Information
            dtPenMaster.Columns.Add(new DataColumn("PensionerID", typeof(System.Int32)));
            dtPenMaster.Columns.Add(new DataColumn("FileNo", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("FileDate", typeof(System.String)));
            //Bank Details
            dtPenMaster.Columns.Add(new DataColumn("BankID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("BranchID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("BankAccount", typeof(System.String)));
            //Personal Details
            dtPenMaster.Columns.Add(new DataColumn("EmpNo", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("SectorID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("LocationID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PensionerName", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("ReligionID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("QualificationID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("MaritalStatusID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("CastID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("DOB", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("Gender", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("RetirementTypeID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("GroupID", typeof(System.String)));
            //Address Information
            dtPenMaster.Columns.Add(new DataColumn("PresentAddress", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PresentState", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PresentDistrict", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PresentCity", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PresentPin", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PresentPhone", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PresentMobile", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PresentEmail", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PermanentAddress", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PermanentState", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PermanentDistrict", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PermanentCity", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PermanentPin", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PermanentPhone", typeof(System.String)));
            //Official Information
            dtPenMaster.Columns.Add(new DataColumn("DOJ", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("DOR", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("DesignationID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PhysicalHandicappedID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("CategoryID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("IsAliveID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("ExpiryDate", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("RetirementAge", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalServiceYear", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalServiceMonth", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalServiceDay", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalNonQualiServiceID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalNonQualiServiceYear", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalNonQualiServiceMonth", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalNonQualiServiceDay", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalQualiServiceYear", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalQualiServiceMonth", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("TotalQualiServiceDay", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("IsPensionerinGovtService", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PFCode", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PayScaleType", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PayScaleID", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PAN", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("Mediclaim", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("MediEffFrom", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("MediEffTo", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("RopaID", typeof(System.String)));
            //Sanction Details
            dtPenMaster.Columns.Add(new DataColumn("PenSanOrderNo", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PenSanDate", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PenSanBy", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PaymentDate", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PensionerType", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("PPONo", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("Accepted", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("NormalAdvance", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("Remarks", typeof(System.String)));
            //Phota and Signature
            dtPenMaster.Columns.Add(new DataColumn("Photo", typeof(System.String)));
            dtPenMaster.Columns.Add(new DataColumn("Signature", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtPenMaster;
    }
    public static DataTable CreateEmpDetailTable(DataTable dtEmpDetail)
    {
        try
        {
            dtEmpDetail.Columns.Add(new DataColumn("BlockID", typeof(System.Int32)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockDate", typeof(System.String)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockedAdminID", typeof(System.Int32)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockedFor", typeof(System.String)));
            dtEmpDetail.Columns.Add(new DataColumn("BlockedReason", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtEmpDetail;
    }
    public static DataTable CreatePayDetailsTable(DataTable dtPayDetail)
    {
        try
        {
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionID", typeof(System.Int32)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeduction", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("Amount", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionType", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtPayDetail;
    }
    public static DataTable CreatePayDetailsTableafterDelete(DataTable dtPayDetail)
    {
        try
        {
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionID", typeof(System.Int32)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeduction", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("Amount", typeof(System.String)));
            dtPayDetail.Columns.Add(new DataColumn("EarnDeductionType", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtPayDetail;
    }
    public static DataTable CreateAddPayDetailsTable(DataTable dtAddPayDetails)
    {
        try
        {
            dtAddPayDetails.Columns.Add(new DataColumn("EDID", typeof(System.Int32)));
            dtAddPayDetails.Columns.Add(new DataColumn("EarnDeduction", typeof(System.String)));
            dtAddPayDetails.Columns.Add(new DataColumn("Amount", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtAddPayDetails;
    }
    public static DataTable CreateNomineeMasterTable(DataTable dtNominee)
    {
        try
        {
            dtNominee.Columns.Add(new DataColumn("EmployeeID", typeof(System.Int32)));
            dtNominee.Columns.Add(new DataColumn("NomineeID", typeof(System.Int32)));
            dtNominee.Columns.Add(new DataColumn("NomineeTypeID", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeType", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeName", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeRelationID", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeRelation", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeDOB", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeAddress", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeState", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeDistrict", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeCity", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineePin", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineePhone", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeIDMark", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPension", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("GratuityPerct", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtNominee;
    }

    public static DataTable CreatePensionNomineeMasterTable(DataTable dtNominee)
    {
        try
        {
            dtNominee.Columns.Add(new DataColumn("EmployeeID", typeof(System.Int32)));
            dtNominee.Columns.Add(new DataColumn("NomineeID", typeof(System.Int32)));
            dtNominee.Columns.Add(new DataColumn("NomineeTypeID", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeType", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeName", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeRelationID", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeRelation", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeDOB", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeAddress", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeState", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeDistrict", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeCity", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineePin", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineePhone", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeIDMark", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPension", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("GratuityPerct", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("isGovtServ", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("OccupyQuart", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPensionRank", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPensionEffecFrom", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPensionEffecTo", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("GratuityRank", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtNominee;
    }

    public static DataTable CreateDeathPensionNomineeMasterTable(DataTable dtNominee)
    {
        try
        {
            dtNominee.Columns.Add(new DataColumn("EmployeeID", typeof(System.Int32)));
            dtNominee.Columns.Add(new DataColumn("NomineeID", typeof(System.Int32)));
            dtNominee.Columns.Add(new DataColumn("NomineeTypeID", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeType", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeName", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeRelationID", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeRelation", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeDOB", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeAddress", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeState", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeDistrict", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeCity", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineePin", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineePhone", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("NomineeIDMark", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPension", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("isGovtServ", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("OccupyQuart", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPensionRank", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPensionEffecFrom", typeof(System.String)));
            dtNominee.Columns.Add(new DataColumn("FamilyPensionEffecTo", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtNominee;
    }

    public static void UpdateMaster(DataTable dtMaster, PensionerMaster master)
    {
        try
        {
            if (master != null && dtMaster != null)
            {
                DataRow dr = dtMaster.NewRow();
                //File Information
                dr["FileNo"] = master.FileNo;
                dr["FileDate"] = master.FileDate;
                //Bank Details
                dr["BankID"] = master.BankID;
                dr["BranchID"] = master.BranchID;
                dr["BankAccount"] = master.BankAccount;
                //Personal Details
                dr["EmpNo"] = master.EmpNo;
                dr["SectorID"] = master.SectorID;
                dr["LocationID"] = master.LocationID;
                dr["PensionerName"] = master.PensionerName;
                dr["ReligionID"] = master.ReligionID;
                dr["QualificationID"] = master.QualificationID;
                dr["MaritalStatusID"] = master.MaritalStatusID;
                dr["CastID"] = master.CastID;
                dr["DOB"] = master.DOB;
                dr["Gender"] = master.Gender;
                dr["RetirementTypeID"] = master.RetirementTypeID;
                dr["GroupID"] = master.GroupID;
                //Address Information
                dr["PresentAddress"] = master.PresentAddress;
                dr["PresentState"] = master.PresentState;
                dr["PresentDistrict"] = master.PresentDistrict;
                dr["PresentCity"] = master.PresentCity;
                dr["PresentPin"] = master.PresentPin;
                dr["PresentPhone"] = master.PresentPhone;
                dr["PresentMobile"] = master.PresentMobile;
                dr["PresentEmail"] = master.PresentEmail;
                dr["PermanentAddress"] = master.PermanentAddress;
                dr["PermanentState"] = master.PermanentState;
                dr["PermanentDistrict"] = master.PermanentDistrict;
                dr["PermanentCity"] = master.PermanentCity;
                dr["PermanentPin"] = master.PermanentPin;
                dr["PermanentPhone"] = master.PermanentPhone;
                //Official Details
                string doj = Convert.ToString(master.DOJ);
                if (doj != null && doj != "")
                {
                    DateTime dt = DateTime.ParseExact(doj, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DOJ"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DOJ"] = doj; }

                string dor = Convert.ToString(master.DOR);
                if (dor != null && dor != "")
                {
                    DateTime dt = DateTime.ParseExact(dor, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DOR"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DOR"] = dor; }

                dr["DesignationID"] = master.DesignationID;
                dr["PhysicalHandicappedID"] = master.PhysicalHandicappedID;
                dr["CategoryID"] = master.CategoryID;
                dr["IsAliveID"] = master.IsAliveID;

                string exp = Convert.ToString(master.ExpiryDate);//.Equals("") ? DBNull.Value : (object)master.DNI);
                if (exp != null && exp != "")
                {
                    DateTime dt = DateTime.ParseExact(exp, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["ExpiryDate"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["ExpiryDate"] = exp; }

                dr["RetirementAge"] = master.RetirementAge;
                dr["TotalServiceYear"] = master.TotalServiceYear;
                dr["TotalServiceMonth"] = master.TotalServiceMonth;
                dr["TotalServiceDay"] = master.TotalServiceDay;
                dr["TotalNonQualiServiceID"] = master.TotalNonQualiServiceID;
                dr["TotalNonQualiServiceYear"] = master.TotalNonQualiServiceYear;
                dr["TotalNonQualiServiceMonth"] = master.TotalNonQualiServiceMonth;
                dr["TotalNonQualiServiceDay"] = master.TotalNonQualiServiceDay;

                dr["TotalQualiServiceYear"] = master.TotalQualiServiceYear;
                dr["TotalQualiServiceMonth"] = master.TotalQualiServiceMonth;
                dr["TotalQualiServiceDay"] = master.TotalQualiServiceDay;


                dr["IsPensionerinGovtService"] = master.IsPensionerinGovtService;
                dr["PFCode"] = master.PFCode;
                dr["PayScaleType"] = master.PayScaleType;
                dr["PayScaleID"] = master.PayScaleID;
                dr["PAN"] = master.PAN;
                dr["Mediclaim"] = master.Mediclaim;
                string EffFrom = Convert.ToString(master.MediEffFrom);
                if (EffFrom != null && EffFrom != "")
                {
                    DateTime dt = DateTime.ParseExact(EffFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["MediEffFrom"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["MediEffFrom"] = EffFrom; }

                string EffTo = Convert.ToString(master.MediEffTo);
                if (EffTo != null && EffTo != "")
                {
                    DateTime dt = DateTime.ParseExact(EffTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["MediEffTo"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["MediEffTo"] = EffTo; }

                dr["RopaID"] = master.RopaID;
                //Sanction Details
                dr["PenSanOrderNo"] = master.PenSanOrderNo;

                string SanDate = Convert.ToString(master.PenSanDate);
                if (SanDate != null && SanDate != "")
                {
                    DateTime dt = DateTime.ParseExact(SanDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["MediEffTo"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["MediEffTo"] = SanDate; }
                dr["PenSanBy"] = master.PenSanBy;

                string payDate = Convert.ToString(master.PaymentDate);
                if (payDate != null && payDate != "")
                {
                    DateTime dt = DateTime.ParseExact(payDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["PaymentDate"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["PaymentDate"] = payDate; }
                dr["PensionerType"] = master.PensionerType;
                dr["PPONo"] = master.PPONo;
                dr["Accepted"] = master.Accepted;
                dr["NormalAdvance"] = master.NormalAdvance;
                dr["Remarks"] = master.Remarks;
               //Phota and Signature
                dr["Photo"] = master.Photo;
                dr["Signature"] = master.Signature;

                //master.EmployeeID = maxValue;
                dtMaster.Rows.Add(dr);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static void UpdatePayDetails(DataTable dtPayDetails, EmployeePayDetails payDetails)
    {
        try
        {
            if (payDetails != null && dtPayDetails != null)
            {
                DataRow[] dr1 = dtPayDetails.Select("EmployeePayDetailID=" + payDetails.EmployeePayDetailID);
                if (dr1.Length > 0)
                {
                    foreach (DataRow dr in dr1)
                    {
                        dr["EmployeeName"] = payDetails.EmployeeID;
                        dr["EarnDeduction"] = payDetails.EarnDeductionID;
                        dr["Amount"] = payDetails.Amount;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static void InsertMaster(DataTable dtMaster, PensionerMaster master)
    {
        try
        {
            if (master != null && dtMaster != null)
            {
                DataRow dr = dtMaster.NewRow();
                object maxval = (dtMaster.Compute("max(PensionerID)", String.Empty));
                int maxValue = 1;
                if (!(maxval is DBNull))
                {
                    maxValue = (Int32)maxval + 1;
                }
                //File Information
                dr["PensionerID"] = maxValue;
                dr["FileNo"] = master.FileNo;
                dr["FileDate"] = master.FileDate;
                //Bank Details
                dr["BankID"] = master.BankID;
                dr["BranchID"] = master.BranchID;
                dr["BankAccount"] = master.BankAccount;
                //Personal Details
                dr["EmpNo"] = master.EmpNo;
                dr["SectorID"] = master.SectorID;
                dr["LocationID"] = master.LocationID;
                dr["PensionerName"] = master.PensionerName;
                dr["ReligionID"] = master.ReligionID;
                dr["QualificationID"] = master.QualificationID;
                dr["MaritalStatusID"] = master.MaritalStatusID;
                dr["CastID"] = master.CastID;
                dr["DOB"] = master.DOB;
                dr["Gender"] = master.Gender;
                dr["RetirementTypeID"] = master.RetirementTypeID;
                dr["GroupID"] = master.GroupID;
                //Address Information
                dr["PresentAddress"] = master.PresentAddress;
                dr["PresentState"] = master.PresentState;
                dr["PresentDistrict"] = master.PresentDistrict;
                dr["PresentCity"] = master.PresentCity;
                dr["PresentPin"] = master.PresentPin;
                dr["PresentPhone"] = master.PresentPhone;
                dr["PresentMobile"] = master.PresentMobile;
                dr["PresentEmail"] = master.PresentEmail;
                dr["PermanentAddress"] = master.PermanentAddress;
                dr["PermanentState"] = master.PermanentState;
                dr["PermanentDistrict"] = master.PermanentDistrict;
                dr["PermanentCity"] = master.PermanentCity;
                dr["PermanentPin"] = master.PermanentPin;
                dr["PermanentPhone"] = master.PermanentPhone;
                //Official Details
                string doj = Convert.ToString(master.DOJ);
                if (doj != null && doj != "")
                {
                    DateTime dt = DateTime.ParseExact(doj, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DOJ"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DOJ"] = doj; }

                string dor = Convert.ToString(master.DOR);
                if (dor != null && dor != "")
                {
                    DateTime dt = DateTime.ParseExact(dor, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["DOR"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["DOR"] = dor; }

                dr["DesignationID"] = master.DesignationID;
                dr["PhysicalHandicappedID"] = master.PhysicalHandicappedID;
                dr["CategoryID"] = master.CategoryID;
                dr["IsAliveID"] = master.IsAliveID;

                string exp = Convert.ToString(master.ExpiryDate);//.Equals("") ? DBNull.Value : (object)master.DNI);
                if (exp != null && exp != "")
                {
                    DateTime dt = DateTime.ParseExact(exp, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["ExpiryDate"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["ExpiryDate"] = exp; }

                dr["RetirementAge"] = master.RetirementAge;
                dr["TotalServiceYear"] = master.TotalServiceYear;
                dr["TotalServiceMonth"] = master.TotalServiceMonth;
                dr["TotalServiceDay"] = master.TotalServiceDay;
                dr["TotalNonQualiServiceID"] = master.TotalNonQualiServiceID;
                dr["TotalNonQualiServiceYear"] = master.TotalNonQualiServiceYear;
                dr["TotalNonQualiServiceMonth"] = master.TotalNonQualiServiceMonth;
                dr["TotalNonQualiServiceDay"] = master.TotalNonQualiServiceDay;

                dr["TotalQualiServiceYear"] = master.TotalQualiServiceYear;
                dr["TotalQualiServiceMonth"] = master.TotalQualiServiceMonth;
                dr["TotalQualiServiceDay"] = master.TotalQualiServiceDay;


                dr["IsPensionerinGovtService"] = master.IsPensionerinGovtService;
                dr["PFCode"] = master.PFCode;
                dr["PayScaleType"] = master.PayScaleType;
                dr["PayScaleID"] = master.PayScaleID;
                dr["PAN"] = master.PAN;
                dr["Mediclaim"] = master.Mediclaim;
                string EffFrom = Convert.ToString(master.MediEffFrom);
                if (EffFrom != null && EffFrom != "")
                {
                    DateTime dt = DateTime.ParseExact(EffFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["MediEffFrom"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["MediEffFrom"] = EffFrom; }

                string EffTo = Convert.ToString(master.MediEffTo);
                if (EffTo != null && EffTo != "")
                {
                    DateTime dt = DateTime.ParseExact(EffTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["MediEffTo"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["MediEffTo"] = EffTo; }

                dr["RopaID"] = master.RopaID;
                //Sanction Details
                dr["PenSanOrderNo"] = master.PenSanOrderNo;

                string SanDate = Convert.ToString(master.PenSanDate);
                if (SanDate != null && SanDate != "")
                {
                    DateTime dt = DateTime.ParseExact(SanDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["MediEffTo"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["MediEffTo"] = SanDate; }
                dr["PenSanBy"] = master.PenSanBy;

                string payDate = Convert.ToString(master.PaymentDate);
                if (payDate != null && payDate != "")
                {
                    DateTime dt = DateTime.ParseExact(payDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dr["PaymentDate"] = dt.ToString("MM/dd/yyyy");
                }
                else { dr["PaymentDate"] = payDate; }
                dr["PensionerType"] = master.PensionerType;
                dr["PPONo"] = master.PPONo;
                dr["Accepted"] = master.Accepted;
                dr["NormalAdvance"] = master.NormalAdvance;
                dr["Remarks"] = master.Remarks;
               //Phota and Signature
                dr["Photo"] = master.Photo;
                dr["Signature"] = master.Signature;

                //master.EmployeeID = maxValue;
                dtMaster.Rows.Add(dr);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    public static void InsertPayDetail(DataTable dtPayDetails)
    {
        try
        {
            if (dtPayDetails.Rows.Count > 0)
            {
                    for (int i = 0; i < dtPayDetails.Rows.Count; i++)
                    {
                        DataRow dr = dtPayDetails.NewRow();
                        object maxval = (dtPayDetails.Compute("max(EmployeePayDetailID)", String.Empty));
                        int maxValue = 1;
                        if (!(maxval is DBNull))
                        {
                            maxValue = (Int32)maxval + 1;
                        }

                        //dr["EmployeePayDetailID"] = dtPayDetails.Rows[i]["EmployeePayDetailID"] = maxValue;
                        //dr["EmployeeID"] = aep.EmployeeID;//.Equals("") ? DBNull.Value : (object)payDetails.EmployeeID;
                        //dr["EarnDeductionID"] = aep.EarnDeductionID;
                        //dr["Amount"] = aep.Amount;

                        //aep.EmployeePayDetailID = maxValue;
                        //dtPayDetails.Rows.Add(dr);
                        dtPayDetails.Rows[i]["EmployeePayDetailID"] = maxValue;
                    }
                }
        }

        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }

    public static int SaveMaster(DataTable dtMaster, PensionerMaster master, int NonQualiServiceID, int AdminID)
    {
        int PensionerID = 0;
        try
        {
            if (dtMaster != null && master != null)
            {
                DataTable dt = DataAccess.DBHandler.GetResult("Insert_PenPensionerMaster",
                    //File Information 
                   master.EmpNo.Equals("") ? DBNull.Value : (object)master.EmpNo,
                   master.SectorID.Equals("") ? DBNull.Value : (object)master.SectorID,
                   master.LocationID.Equals("") ? DBNull.Value : (object)master.LocationID,
                   master.FileNo.Equals("") ? DBNull.Value : (object)master.FileNo,
                   master.FileDate.Equals("") ? DBNull.Value : (object)master.FileDate,
                    //Personal Information 
                   master.PensionerName.Equals("") ? DBNull.Value : (object)master.PensionerName,
                   master.RetirementTypeID.Equals("") ? DBNull.Value : (object)master.RetirementTypeID,
                   master.GroupID.Equals("") ? DBNull.Value : (object)master.GroupID,
                   master.ReligionID.Equals("") ? DBNull.Value : (object)master.ReligionID,
                   master.QualificationID.Equals("") ? DBNull.Value : (object)master.QualificationID,
                   master.MaritalStatusID.Equals("") ? DBNull.Value : (object)master.MaritalStatusID,
                   master.CastID.Equals("") ? DBNull.Value : (object)master.CastID,
                   master.DOB.Equals("") ? DBNull.Value : (object)master.DOB,
                   master.Gender.Equals("") ? DBNull.Value : (object)master.Gender,
                    //Address Information 
                   master.PresentAddress.Equals("") ? DBNull.Value : (object)master.PresentAddress,
                   master.PresentState.Equals("") ? DBNull.Value : (object)master.PresentState,
                   master.PresentDistrict.Equals("") ? DBNull.Value : (object)master.PresentDistrict,
                   master.PresentCity.Equals("") ? DBNull.Value : (object)master.PresentCity,
                   master.PresentPin.Equals("") ? DBNull.Value : (object)master.PresentPin,
                   master.PresentPhone.Equals("") ? DBNull.Value : (object)master.PresentPhone,
                   master.PresentMobile.Equals("") ? DBNull.Value : (object)master.PresentMobile,
                   master.PresentEmail.Equals("") ? DBNull.Value : (object)master.PresentEmail,
                   master.PermanentAddress.Equals("") ? DBNull.Value : (object)master.PermanentAddress,
                   master.PermanentState.Equals("") ? DBNull.Value : (object)master.PermanentState,
                   master.PermanentDistrict.Equals("") ? DBNull.Value : (object)master.PermanentDistrict,
                   master.PermanentCity.Equals("") ? DBNull.Value : (object)master.PermanentCity,
                   master.PermanentPin.Equals("") ? DBNull.Value : (object)master.PermanentPin,
                   master.PermanentPhone.Equals("") ? DBNull.Value : (object)master.PermanentPhone,
                    //Official Details   
                   master.DOJ.Equals("") ? DBNull.Value : (object)master.DOJ,
                   master.DOR.Equals("") ? DBNull.Value : (object)master.DOR,
                   master.PhysicalHandicappedID.Equals("") ? DBNull.Value : (object)master.PhysicalHandicappedID,
                   master.DesignationID.Equals("") ? DBNull.Value : (object)master.DesignationID,
                   master.CategoryID.Equals("") ? DBNull.Value : (object)master.CategoryID,
                   master.IsAliveID.Equals("") ? DBNull.Value : (object)master.IsAliveID,
                   master.ExpiryDate.Equals("") ? DBNull.Value : (object)master.ExpiryDate,
                   master.RetirementAge.Equals("") ? DBNull.Value : (object)master.RetirementAge,
                   master.TotalServiceYear.Equals("") ? DBNull.Value : (object)master.TotalServiceYear,
                   master.TotalServiceMonth.Equals("") ? DBNull.Value : (object)master.TotalServiceMonth,
                   master.TotalServiceDay.Equals("") ? DBNull.Value : (object)master.TotalServiceDay,
                   NonQualiServiceID.Equals("") ? DBNull.Value : (object)NonQualiServiceID,
                   master.TotalNonQualiServiceYear.Equals("") ? DBNull.Value : (object)master.TotalNonQualiServiceYear,
                   master.TotalNonQualiServiceMonth.Equals("") ? DBNull.Value : (object)master.TotalNonQualiServiceMonth,
                   master.TotalNonQualiServiceDay.Equals("") ? DBNull.Value : (object)master.TotalNonQualiServiceDay,
                   master.TotalQualiServiceYear.Equals("") ? DBNull.Value : (object)master.TotalQualiServiceYear,
                   master.TotalQualiServiceMonth.Equals("") ? DBNull.Value : (object)master.TotalQualiServiceMonth,
                   master.TotalQualiServiceDay.Equals("") ? DBNull.Value : (object)master.TotalQualiServiceDay,
                   master.IsPensionerinGovtService.Equals("") ? DBNull.Value : (object)master.IsPensionerinGovtService,
                   master.PFCode.Equals("") ? DBNull.Value : (object)master.PFCode,
                   master.PayScaleID.Equals("") ? DBNull.Value : (object)master.PayScaleID,
                   master.PayScaleType.Equals("") ? DBNull.Value : (object)master.PayScaleType,
                   master.PAN.Equals("") ? DBNull.Value : (object)master.PAN,
                   master.Mediclaim.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.Mediclaim),
                   master.MediEffFrom.Equals("") ? DBNull.Value : (object)master.MediEffFrom,
                   master.MediEffTo.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.MediEffTo),
                   master.RopaID.Equals("") ? DBNull.Value : (object)master.RopaID,
                   //Sanction Details
                   master.PenSanOrderNo.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.PenSanOrderNo),
                   master.PenSanDate.Equals("") ? DBNull.Value : (object)master.PenSanDate,
                   master.PenSanBy.Equals("") ? DBNull.Value : (object)master.PenSanBy,
                   master.PaymentDate.Equals("") ? DBNull.Value : (object)master.PaymentDate,
                   master.PensionerType.Equals("") ? DBNull.Value : (object)master.PensionerType,
                   master.PPONo.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.PPONo),
                   master.Accepted.Equals("") ? DBNull.Value : (object)master.Accepted,
                   master.NormalAdvance.Equals("") ? DBNull.Value : (object)master.NormalAdvance,
                   master.Remarks.Equals("") ? DBNull.Value : (object)master.Remarks,
                    //Bank Details 
                   master.BranchID.Equals("") ? DBNull.Value : (object)master.BranchID,
                   master.BankAccount.Equals("") ? DBNull.Value : (object)master.BankAccount,
                    //Phota and Signature  
                   master.Photo.Equals("") ? DBNull.Value : (object)master.Photo,
                   master.Signature.Equals("") ? DBNull.Value : (object)master.Signature,

                   //Others
                   AdminID,AdminID
                   );


                PensionerID = Convert.ToInt32(dt.Rows[0]["EmployeeID"].ToString());
                int OldEmployeeID = master.PensionerID;
                master.PensionerID = PensionerID;
                DataRow[] dr1 = dtMaster.Select("PensionerID=" + OldEmployeeID);
                if (dr1.Length > 0)
                {
                    foreach (DataRow dr in dr1)
                    {
                        dr["PensionerID"] = PensionerID;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return PensionerID;
    }
    public static int UpdatePensionerMaster(DataTable dtMaster, PensionerMaster master, int NonQualiServiceID, int AdminID)
    {
        int PensionerID = 0;
        try
        {
            if (dtMaster != null && master != null)
            {
                DataTable dt = DataAccess.DBHandler.GetResult("Update_PenPensionerMaster",
                    //File Information 
                   master.PensionerID.Equals("") ? DBNull.Value : (object)master.PensionerID,
                   master.EmpNo.Equals("") ? DBNull.Value : (object)master.EmpNo,
                   master.SectorID.Equals("") ? DBNull.Value : (object)master.SectorID,
                   master.LocationID.Equals("") ? DBNull.Value : (object)master.LocationID,
                   master.FileNo.Equals("") ? DBNull.Value : (object)master.FileNo,
                   master.FileDate.Equals("") ? DBNull.Value : (object)master.FileDate,
                    //Personal Information 
                   master.PensionerName.Equals("") ? DBNull.Value : (object)master.PensionerName,
                   master.RetirementTypeID.Equals("") ? DBNull.Value : (object)master.RetirementTypeID,
                   master.GroupID.Equals("") ? DBNull.Value : (object)master.GroupID,
                   master.ReligionID.Equals("") ? DBNull.Value : (object)master.ReligionID,
                   master.QualificationID.Equals("") ? DBNull.Value : (object)master.QualificationID,
                   master.MaritalStatusID.Equals("") ? DBNull.Value : (object)master.MaritalStatusID,
                   master.CastID.Equals("") ? DBNull.Value : (object)master.CastID,
                   master.DOB.Equals("") ? DBNull.Value : (object)master.DOB,
                   master.Gender.Equals("") ? DBNull.Value : (object)master.Gender,
                   // //Address Information 
                   master.PresentAddress.Equals("") ? DBNull.Value : (object)master.PresentAddress,
                   master.PresentState.Equals("") ? DBNull.Value : (object)master.PresentState,
                   master.PresentDistrict.Equals("") ? DBNull.Value : (object)master.PresentDistrict,
                   master.PresentCity.Equals("") ? DBNull.Value : (object)master.PresentCity,
                   master.PresentPin.Equals("") ? DBNull.Value : (object)master.PresentPin,
                   master.PresentPhone.Equals("") ? DBNull.Value : (object)master.PresentPhone,
                   master.PresentMobile.Equals("") ? DBNull.Value : (object)master.PresentMobile,
                   master.PresentEmail.Equals("") ? DBNull.Value : (object)master.PresentEmail,
                   master.PermanentAddress.Equals("") ? DBNull.Value : (object)master.PermanentAddress,
                   master.PermanentState.Equals("") ? DBNull.Value : (object)master.PermanentState,
                   master.PermanentDistrict.Equals("") ? DBNull.Value : (object)master.PermanentDistrict,
                   master.PermanentCity.Equals("") ? DBNull.Value : (object)master.PermanentCity,
                   master.PermanentPin.Equals("") ? DBNull.Value : (object)master.PermanentPin,
                   master.PermanentPhone.Equals("") ? DBNull.Value : (object)master.PermanentPhone,
                   // //Official Details   
                   master.DOJ.Equals("") ? DBNull.Value : (object)master.DOJ,
                   master.DOR.Equals("") ? DBNull.Value : (object)master.DOR,
                   master.PhysicalHandicappedID.Equals("") ? DBNull.Value : (object)master.PhysicalHandicappedID,
                   master.DesignationID.Equals("") ? DBNull.Value : (object)master.DesignationID,
                   master.CategoryID.Equals("") ? DBNull.Value : (object)master.CategoryID,
                   master.IsAliveID.Equals("") ? DBNull.Value : (object)master.IsAliveID,
                   master.ExpiryDate.Equals("") ? DBNull.Value : (object)master.ExpiryDate,
                   master.RetirementAge.Equals("") ? DBNull.Value : (object)master.RetirementAge,
                   master.TotalServiceYear.Equals("") ? DBNull.Value : (object)master.TotalServiceYear,
                   master.TotalServiceMonth.Equals("") ? DBNull.Value : (object)master.TotalServiceMonth,
                   master.TotalServiceDay.Equals("") ? DBNull.Value : (object)master.TotalServiceDay,
                   NonQualiServiceID.Equals("") ? DBNull.Value : (object)NonQualiServiceID,
                   master.TotalNonQualiServiceYear.Equals("") ? DBNull.Value : (object)master.TotalNonQualiServiceYear,
                   master.TotalNonQualiServiceMonth.Equals("") ? DBNull.Value : (object)master.TotalNonQualiServiceMonth,
                   master.TotalNonQualiServiceDay.Equals("") ? DBNull.Value : (object)master.TotalNonQualiServiceDay,
                   master.TotalQualiServiceYear.Equals("") ? DBNull.Value : (object)master.TotalQualiServiceYear,
                   master.TotalQualiServiceMonth.Equals("") ? DBNull.Value : (object)master.TotalQualiServiceMonth,
                   master.TotalQualiServiceDay.Equals("") ? DBNull.Value : (object)master.TotalQualiServiceDay,
                   master.IsPensionerinGovtService.Equals("") ? DBNull.Value : (object)master.IsPensionerinGovtService,
                   master.PFCode.Equals("") ? DBNull.Value : (object)master.PFCode,
                   master.PayScaleID.Equals("") ? DBNull.Value : (object)master.PayScaleID,
                   master.PayScaleType.Equals("") ? DBNull.Value : (object)master.PayScaleType,
                   master.PAN.Equals("") ? DBNull.Value : (object)master.PAN,
                   master.Mediclaim.Equals("") ? DBNull.Value : (object)master.Mediclaim,
                   master.MediEffFrom.Equals("") ? DBNull.Value : (object)master.MediEffFrom,
                   master.MediEffTo.Equals("") ? DBNull.Value : (object)master.MediEffTo,
                   master.RopaID.Equals("") ? DBNull.Value : (object)master.RopaID,
                   // //Sanction Details
                   master.PenSanOrderNo.Equals("") ? DBNull.Value : (object)master.PenSanOrderNo,
                   master.PenSanDate.Equals("") ? DBNull.Value : (object)master.PenSanDate,
                   master.PenSanBy.Equals("") ? DBNull.Value : (object)master.PenSanBy,
                   master.PaymentDate.Equals("") ? DBNull.Value : (object)master.PaymentDate,
                   master.PensionerType.Equals("") ? DBNull.Value : (object)master.PensionerType,
                   master.PPONo.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(master.PPONo),
                   master.Accepted.Equals("") ? DBNull.Value : (object)master.Accepted,
                   master.NormalAdvance.Equals("") ? DBNull.Value : (object)master.NormalAdvance,
                   master.Remarks.Equals("") ? DBNull.Value : (object)master.Remarks,
                    //Bank Details 
                   master.BranchID.Equals("") ? DBNull.Value : (object)master.BranchID,
                   master.BankAccount.Equals("") ? DBNull.Value : (object)master.BankAccount,
                    //Phota and Signature  
                   master.Photo.Equals("") ? DBNull.Value : (object)master.Photo,
                   master.Signature.Equals("") ? DBNull.Value : (object)master.Signature,

                   //Others
                   AdminID, AdminID
                   );


                PensionerID = Convert.ToInt32(dt.Rows[0]["PensionerID"].ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return PensionerID;
    }
    public static int SavePayDetails(DataTable dtPayDetails, int EmoloyeeID, int AdminID)
    {
        int EmployeePayDetailID = 0;
        try
        {
            if (dtPayDetails.Rows.Count > 0 || dtPayDetails.Rows.Count == 0)
            {
                DataTable dtresult = DataAccess.DBHandler.GetResult("Delete_PenPayDetails", EmoloyeeID);
                for (int i = 0; i < dtPayDetails.Rows.Count; i++)
                {
                    int EarnDeductionID = Convert.ToInt32(dtPayDetails.Rows[i]["EarnDeductionID"].Equals("") ? DBNull.Value : (object)dtPayDetails.Rows[i]["EarnDeductionID"].ToString());
                    DataTable dt = DataAccess.DBHandler.GetResult("Insert_PenPayDetails",
                    EmoloyeeID,
                    EarnDeductionID,
                    dtPayDetails.Rows[i]["Amount"].Equals("") ? DBNull.Value : (object)dtPayDetails.Rows[i]["Amount"],
                    AdminID);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return EmployeePayDetailID;
    }
    public static int SaveNonQualiServReasonwithDate(DataTable dtNonQuReason, string TotNonQualiservID, int AdminID)
    {
        int NonQualiServiceID = 0;
        try
        {
            if (dtNonQuReason.Rows.Count > 0)
            {
                dtNonQuReason.Columns.Add("NonQualiDetailID");
                for (int i = 0; i < dtNonQuReason.Rows.Count; i++)
                {
                    dtNonQuReason.Rows[i]["NonQualiDetailID"] = i + 1;
                }

                DataTable dtresult = DataAccess.DBHandler.GetResult("Delete_PenNonQualiService", TotNonQualiservID);
                for (int i = 0; i < dtNonQuReason.Rows.Count; i++)
                {
                    string FromDate=dtNonQuReason.Rows[i]["FromDate"].ToString();
                    string ToDate=dtNonQuReason.Rows[i]["ToDate"].ToString();

                    DateTime dtFrom = Convert.ToDateTime(FromDate);
                    DateTime dtTo = Convert.ToDateTime(ToDate);

                    DataTable dt = DataAccess.DBHandler.GetResult("Insert_PenNonQualiService",
                    TotNonQualiservID.Equals("") ? 0 : (object)TotNonQualiservID,
                    dtNonQuReason.Rows[i]["NonQualiDetailID"].Equals("") ? DBNull.Value : (object)dtNonQuReason.Rows[i]["NonQualiDetailID"],
                    dtFrom.ToString("dd/MM/yyyy").Equals("") ? DBNull.Value : (object)dtFrom.ToString("dd/MM/yyyy"),
                    dtTo.ToString("dd/MM/yyyy").Equals("") ? DBNull.Value : (object)dtTo.ToString("dd/MM/yyyy"),
                    dtNonQuReason.Rows[i]["ReasonID"].Equals("") ? DBNull.Value : (object)dtNonQuReason.Rows[i]["ReasonID"],
                    AdminID);

                    NonQualiServiceID = Convert.ToInt32(dt.Rows[0]["NonQualiServiceID"].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return NonQualiServiceID;
    }
    public static int SaveNomineeDetails(DataTable dtNomineeDetails, int PensionerID, int AdminID)
    {
        int EmployeePayDetailID = 0;
        try
        {
            if (dtNomineeDetails.Rows.Count > 0 || dtNomineeDetails.Rows.Count == 0)
            {
                DataTable dtresult = DataAccess.DBHandler.GetResult("Delete_PenNomineeDetails", PensionerID);
                for (int i = 0; i < dtNomineeDetails.Rows.Count; i++)
                {
                    DataTable dt = DataAccess.DBHandler.GetResult("Insert_PenNomineeDetails",
                    PensionerID,
                    dtNomineeDetails.Rows[i]["NomineeTypeID"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeTypeID"],
                    dtNomineeDetails.Rows[i]["NomineeName"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeName"],
                    dtNomineeDetails.Rows[i]["NomineeRelationID"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeRelationID"],
                    dtNomineeDetails.Rows[i]["NomineeAddress"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeAddress"],
                    dtNomineeDetails.Rows[i]["NomineeState"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeState"],
                    dtNomineeDetails.Rows[i]["NomineeDistrict"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeDistrict"],
                    dtNomineeDetails.Rows[i]["NomineeCity"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeCity"],
                    dtNomineeDetails.Rows[i]["NomineePin"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineePin"],
                    dtNomineeDetails.Rows[i]["NomineePhone"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineePhone"],
                    dtNomineeDetails.Rows[i]["NomineeDOB"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeDOB"],
                    dtNomineeDetails.Rows[i]["NomineeIDMark"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["NomineeIDMark"],
                    dtNomineeDetails.Rows[i]["FamilyPension"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["FamilyPension"],
                    dtNomineeDetails.Rows[i]["GratuityPerct"].Equals("") ? DBNull.Value : (object)dtNomineeDetails.Rows[i]["GratuityPerct"]
                   );
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return EmployeePayDetailID;
    }

    public static void DeletePayDetail(DataTable dtPayDetails, int EarnDeductionID, int noofrows)
    {
        try
        {
                if (EarnDeductionID == 2)
                {
                    //if (dtPayDetails.Rows.Count !=5 || dtPayDetails.Rows.Count != 6)
                    //{
                    //for (int i = 0; i < dtPayDetails.Rows.Count; i++)
                    //{
                    //    int edid = Convert.ToInt32(dtPayDetails.Rows[i]["EarnDeductionID"]);
                    //    if (edid == 2 || edid == 9 || edid == 3 || edid == 4 || edid == 5 || edid == 18)
                    //    {
                            DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                            foreach (DataRow row in dr1)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr2 = dtPayDetails.Select("EarnDeductionID=" + 9);
                            foreach (DataRow row in dr2)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr3 = dtPayDetails.Select("EarnDeductionID=" + 3);
                            foreach (DataRow row in dr3)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr4 = dtPayDetails.Select("EarnDeductionID=" + 4);
                            foreach (DataRow row in dr4)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr5 = dtPayDetails.Select("EarnDeductionID=" + 5);
                            foreach (DataRow row in dr5)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr6 = dtPayDetails.Select("EarnDeductionID=" + 18);
                            foreach (DataRow row in dr6)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            DataRow[] dr7 = dtPayDetails.Select("EarnDeductionID=" + 7);
                            foreach (DataRow row in dr7)
                            {
                                if (row.RowState != DataRowState.Deleted)
                                {
                                    row.Delete();
                                }
                            }
                            dtPayDetails.AcceptChanges();
                        //}
                

                }
                else
                {
                    DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                    foreach (DataRow row in dr1)
                    {
                        if (row.RowState != DataRowState.Deleted)
                        {
                                row.Delete();
                                dtPayDetails.AcceptChanges();
                        }
                    }
                }
             }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
    
    }
    public static void DeletePayDetailbySuspended(DataTable dtPayDetails, int EarnDeductionID)
    {
        try
        {

                DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                foreach (DataRow row in dr1)
                {
                    if (row.RowState != DataRowState.Deleted)
                    {
                        row.Delete();
                        dtPayDetails.AcceptChanges();
                    }
                }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }

    }
    public static void UpdatePayDetail(DataTable dtPayDetails, int EDID, string EDName, string amount)
    {
        try
        {

            try
            {

                DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + EDID);
                foreach (DataRow row in dr1)
                {
                    row.Delete();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        //return TotBooking;
    }
    public static string UpdateCoOperativeDetails(string id, int EDID, string Amount)
    {
        string Result = ""; DataTable dtres = new DataTable();
        try
        {
            dtres = DBHandler.GetResult("Update_CoOperativeCal", id, EDID, Amount);
            if (dtres.Rows.Count > 0)
            {
                Result = "success";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Result;
    }

    public static DataTable CreateNonQualiReason(DataTable dtNonQualiReason)
    {
        try
        {
            dtNonQualiReason.Columns.Add(new DataColumn("NonQualiReasonID", typeof(System.String)));
            dtNonQualiReason.Columns.Add(new DataColumn("NonQualiReason", typeof(System.String)));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return dtNonQualiReason;
    }
    public static DataTable DeleteNonQualiReason(DataTable dtNonQualiReason, int DetTermID)
    {
        try
        {
            DataRow[] dr1 = dtNonQualiReason.Select("NonQualiReasonID=" + DetTermID);
            foreach (DataRow row in dr1)
            {
                if (row.RowState != DataRowState.Deleted)
                {
                    row.Delete();
                    dtNonQualiReason.AcceptChanges();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
        return dtNonQualiReason;
    }
    public PensionerDataHandling()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}