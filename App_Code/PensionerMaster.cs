﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmployeeMaster
/// </summary>
public class PensionerMaster
{
    //File Information
    public int PensionerID { get; set; }
    public string FileNo { get; set; }
    public string FileDate { get; set; }
    //Bank Details
    public string BankID { get; set; }
    public string BranchID { get; set; }
    public string BankAccount { get; set; }
    //Personal Details
    public string EmpNo { get; set; }
    public string SectorID { get; set; }
    public string LocationID { get; set; }
    public string PensionerName { get; set; }
    public string ReligionID { get; set; }
    public string QualificationID { get; set; }
    public string MaritalStatusID { get; set; }
    public string CastID { get; set; }
    public string DOB { get; set; }
    public string Gender { get; set; }
    public string RetirementTypeID { get; set; }
    public string GroupID { get; set; }
    //Address Information
    public string PresentAddress { get; set; }
    public string PresentState { get; set; }
    public string PresentDistrict { get; set; }
    public string PresentCity { get; set; }
    public string PresentPin { get; set; }
    public string PresentPhone { get; set; }
    public string PresentMobile { get; set; }
    public string PresentEmail { get; set; }
    public string PermanentAddress { get; set; }
    public string PermanentState { get; set; }
    public string PermanentDistrict { get; set; }
    public string PermanentCity { get; set; }
    public string PermanentPin { get; set; }
    public string PermanentPhone { get; set; }
    //Official Information
    public string DOJ { get; set; }
    public string DOR { get; set; }
    public string DesignationID { get; set; }
    public string PhysicalHandicappedID { get; set; }
    public string CategoryID { get; set; }
    public string IsAliveID { get; set; }
    public string ExpiryDate { get; set; }
    public string RetirementAge { get; set; }
    public string TotalServiceYear { get; set; }
    public string TotalServiceMonth { get; set; }
    public string TotalServiceDay { get; set; }
    public string TotalNonQualiServiceID { get; set; }
    public string TotalNonQualiServiceYear { get; set; }
    public string TotalNonQualiServiceMonth { get; set; }
    public string TotalNonQualiServiceDay { get; set; }
    public string TotalQualiServiceYear { get; set; }
    public string TotalQualiServiceMonth { get; set; }
    public string TotalQualiServiceDay { get; set; }
    public string IsPensionerinGovtService { get; set; }
    public string PFCode { get; set; }
    public string PayScaleType { get; set; }
    public string PayScaleID { get; set; }
    public string PAN { get; set; }
    public string Mediclaim { get; set; }
    public string MediEffFrom { get; set; }
    public string MediEffTo { get; set; }
    public string RopaID { get; set; }
    //Sanction Details
    public string PenSanOrderNo { get; set; }
    public string PenSanDate { get; set; }
    public string PenSanBy { get; set; }
    public string PaymentDate { get; set; }
    public string PensionerType { get; set; }
    public string PPONo { get; set; }
    public string Accepted { get; set; }
    public string NormalAdvance { get; set; }
    public string Remarks { get; set; }
    //Phota and Signature
    public string Photo { get; set; }
    public string Signature { get; set; }

    public PensionerMaster()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}