﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class LoanMaster : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
           // SetPageNoCache();
            
            if (!IsPostBack)
            {

                try
                {
                    int SecID = Convert.ToInt32(((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue);
                    string SessionSecID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
                    PopulateLoanDesc();
                    if (SecID != 0 || SessionSecID != "0")
                        PopulateEmployee(SecID == 0 ? Convert.ToInt32(SessionSecID) : SecID);
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
                } 
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                //PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string EmpNo = txtEmpNo.Text.Trim();
            string msg = "";
            DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
            int Secid = Convert.ToInt32(Sectors.SelectedValue.ToString());
            if (Secid != 0)
            {
               msg= PopulateGrid(EmpNo);
               PopulateEmployee(Secid);
            }
            else
            {
                string display = "Please select Sector.";
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                Sectors.Focus();
            }
            if (msg != "" && msg != null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + msg + "')</script>");
                txtEmpNo.Focus();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private string PopulateGrid(string EmpNo)
    {
        
        try
        {
            string display = "";
           // DataTable dtstr = DBHandler.GetResult("Get_Loan");  //This is for loading overall data on page load of employee.....
            //DataTable dtstr = DBHandler.GetResult("Get_Loan", EmpNo);
            DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
            int Secid = Convert.ToInt32(Sectors.SelectedValue.ToString());
            DataSet ds = DBHandler.GetResults("Get_Loan", EmpNo, Secid, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable  dtss = ds.Tables[0];
            DataTable dtresult = ds.Tables[1];
            
            if (dtresult.Rows[0][0].ToString() == "Y")
            {
                display = "Please select the appropriate Sector of this Employee.";
                //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                //Sectors.Focus();
            }
            else if (dtresult.Rows[0][0].ToString() == "N")
            {
                display = "Sorry ! You are not authorised.";
                //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
            }
            else
            {
                if (dtss.Rows.Count == 0)
                {
                    tbl.DataSource = dtss;
                    tbl.DataBind();
                    display = "Sorry ! No Loan Found.";
                    //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                    //Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
                }
                else
                {
                    tbl.DataSource = dtss;
                    tbl.DataBind();
                    PopulateEmployee(Secid);
                    PopulateLoanDesc();
                }
            }


            return display;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public void SetPageNoCache()
    {
        try
        {
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            //Response.Cache.SetAllowResponseInBrowserHistory(false);
            //Response.Cache.SetNoStore();
            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Response.AppendHeader("Expires", "0");

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddSeconds(-1));
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            Response.Cache.SetNoStore();
            HttpResponse.RemoveOutputCacheItem("/LoanMaster.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                int SecID = Convert.ToInt32(((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue);
                DataTable dtResult = DBHandler.GetResult("Get_LoanByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    string LOANtYPE = dtResult.Rows[0]["LoanType"].ToString();
                    ((DropDownList)dv.FindControl("ddlLoanType")).SelectedValue = LOANtYPE;
                    PopulateLoanDesc();
                    ((DropDownList)dv.FindControl("ddlLoanDesc")).SelectedValue = dtResult.Rows[0]["LoanTypeID"].ToString();
                    PopulateEmployee(SecID);
                    ((DropDownList)dv.FindControl("ddlEmployee")).SelectedValue = dtResult.Rows[0]["EmployeeID"].ToString();

                    CheckBox chkCloseInd = (CheckBox)dv.FindControl("chkCloseInd");
                    chkCloseInd.Enabled = true;
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_LoanByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                string EmpNo = txtEmpNo.Text.Trim();
                PopulateGrid(EmpNo);
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                string SecID = ((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue;
                DataTable dtEncodeValue = DBHandler.GetResult("Get_SalPayMonth", SecID, HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);
                if (dtEncodeValue.Rows.Count > 0)
                {
                    string EncValue = dtEncodeValue.Rows[0]["EncodeValue"].ToString();
                    if (EncValue == "" || EncValue == null)
                    {

                        string empid = hdnEmployeeID.Value;
                        TextBox txtLoanAmt = (TextBox)dv.FindControl("txtLoanAmt");
                        TextBox txtInstallAmt = (TextBox)dv.FindControl("txtInstallAmt");
                        TextBox txtLoanDeducStrt = (TextBox)dv.FindControl("txtLoanDeducStrt");
                        TextBox txtTotLoanInstl = (TextBox)dv.FindControl("txtTotLoanInstl");
                        TextBox txtCurLoanInst = (TextBox)dv.FindControl("txtCurLoanInst");
                        TextBox txtLoanAmtPaid = (TextBox)dv.FindControl("txtLoanAmtPaid");
                        TextBox txtAdjDate = (TextBox)dv.FindControl("txtAdjDate");
                        TextBox txtAdjAmt = (TextBox)dv.FindControl("txtAdjAmt");
                        TextBox txtLoanSancDate = (TextBox)dv.FindControl("txtLoanSancDate");
                        TextBox txtRefNo = (TextBox)dv.FindControl("txtRefNo");
                        TextBox txtRefDate = (TextBox)dv.FindControl("txtRefDate");

                        TextBox txtDisbursementNo = (TextBox)dv.FindControl("txtDisbursementNo");
                        TextBox txtDisbursementDate = (TextBox)dv.FindControl("txtDisbursementDate");

                        DropDownList ddlLoanType = (DropDownList)dv.FindControl("ddlLoanType");
                        DropDownList ddlLoanDesc = (DropDownList)dv.FindControl("ddlLoanDesc");
                        DropDownList ddlEmployee = (DropDownList)dv.FindControl("ddlEmployee");
                        CheckBox chkCloseInd = (CheckBox)dv.FindControl("chkCloseInd");

                        DataTable dtCount = DBHandler.GetResult("Check_isLoanAvailable", empid, ddlLoanDesc.SelectedValue);
                        if(dtCount.Rows.Count>0)
                        {
                            int count = Convert.ToInt32(dtCount.Rows[0]["Count"]);
                            if (count > 0)
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Sorry ! this Loan is already Available for this Employee.')</script>");
                            }
                            else
                            {
                                //DateTime LoanDeducStart = DateTime.ParseExact(txtLoanDeducStrt.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //DateTime AdjDate = DateTime.ParseExact(txtAdjDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //DateTime LoanSancDate = DateTime.ParseExact(txtLoanSancDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //DateTime RefDate = DateTime.ParseExact(txtRefDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                string LoanDStart = txtLoanDeducStrt.Text; string LoanDedStart = "";
                                if (LoanDStart != null && LoanDStart != "")
                                {
                                    DateTime dt = DateTime.ParseExact(LoanDStart, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    LoanDedStart = dt.ToString("dd/MM/yyyy");
                                }
                                else { LoanDedStart = LoanDStart; }

                                string Adate = txtAdjDate.Text; string AdjDate = "";
                                if (Adate != null && Adate != "")
                                {
                                    DateTime dt = DateTime.ParseExact(Adate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    AdjDate = dt.ToString("dd/MM/yyyy");
                                }
                                else { AdjDate = Adate; }

                                string LoanSdate = txtLoanSancDate.Text; string LoanSancDate = "";
                                if (LoanSdate != null && LoanSdate != "")
                                {
                                    DateTime dt = DateTime.ParseExact(LoanSdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    LoanSancDate = dt.ToString("dd/MM/yyyy");
                                }
                                else { LoanSancDate = LoanSdate; }

                                string Rdate = txtRefDate.Text; string RefDate = "";
                                if (Rdate != null && Rdate != "")
                                {
                                    DateTime dt = DateTime.ParseExact(Rdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    RefDate = dt.ToString("dd/MM/yyyy");
                                }
                                else { RefDate = Rdate; }

                                string DisbursementDate = txtDisbursementDate.Text; string DisDate = "";
                                if (DisbursementDate != null && DisbursementDate != "")
                                {
                                    DateTime dt = DateTime.ParseExact(DisbursementDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    DisDate = dt.ToString("dd/MM/yyyy");
                                }
                                else { DisDate = DisbursementDate; }

                                TextBox txtInstNo1 = (TextBox)dv.FindControl("txtInstNo1");
                                TextBox txtInstAmt1 = (TextBox)dv.FindControl("txtInstAmt1");

                                TextBox txtInstNo2 = (TextBox)dv.FindControl("txtInstNo2");
                                TextBox txtInstAmt2 = (TextBox)dv.FindControl("txtInstAmt2");

                                TextBox txtInstNo3 = (TextBox)dv.FindControl("txtInstNo3");
                                TextBox txtInstAmt3 = (TextBox)dv.FindControl("txtInstAmt3");

                                int FisrstInstall = txtInstNo1.Text == "" ? 0 : Convert.ToInt32(txtInstNo1.Text); double FirstAmount = txtInstAmt1.Text == "" ? 0 : Convert.ToInt32(txtInstAmt1.Text);
                                int SecondInstall = txtInstNo2.Text == "" ? 0 : Convert.ToInt32(txtInstNo2.Text); double SecondAmount = txtInstAmt2.Text == "" ? 0 : Convert.ToInt32(txtInstAmt2.Text);
                                int ThirdInstall = txtInstNo3.Text == "" ? 0 : Convert.ToInt32(txtInstNo3.Text); double ThirdAmount = txtInstAmt3.Text == "" ? 0 : Convert.ToInt32(txtInstAmt3.Text);

                                int TotalInstall = (FisrstInstall + SecondInstall + ThirdInstall);
                                int EnterTotalInstall = txtTotLoanInstl.Text == "" ? 0 : Convert.ToInt32(txtTotLoanInstl.Text);
                                if (TotalInstall != EnterTotalInstall)
                                {
                                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Total Entered Installment No. is not correct.')</script>");
                                    txtTotLoanInstl.Focus();
                                    return;
                                }

                                double TotalLoanAmt = ((FisrstInstall * FirstAmount) + (SecondInstall * SecondAmount) + (ThirdInstall * ThirdAmount));
                                double EnterTotalLoanAmt = txtLoanAmt.Text == "" ? 0 : Convert.ToDouble(txtLoanAmt.Text);
                                double LoanAmount = Convert.ToDouble(EnterTotalLoanAmt);

                                if (TotalLoanAmt != LoanAmount)
                                {
                                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Total Entered Loan Amount does not match.')</script>");
                                    txtLoanAmt.Focus();
                                    return;
                                }

                                DBHandler.Execute("Insert_Loan",
                                    (ddlLoanDesc.SelectedValue).Equals("") ? DBNull.Value : (object)Convert.ToInt32(ddlLoanDesc.SelectedValue),
                                    empid,
                                    //(ddlEmployee.SelectedValue).Equals("") ? DBNull.Value : (object)Convert.ToInt32(ddlEmployee.SelectedValue),
                                    (txtLoanAmt.Text).Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtLoanAmt.Text),
                                    0,
                                    LoanDedStart,
                                    (txtTotLoanInstl.Text).Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtTotLoanInstl.Text),
                                    (txtCurLoanInst.Text).Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtCurLoanInst.Text),
                                    (txtLoanAmtPaid.Text).Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtLoanAmtPaid.Text),
                                    (chkCloseInd.Checked == true ? "Y" : "N"),
                                    (txtAdjAmt.Text).Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtAdjAmt.Text),
                                    AdjDate.Equals("") ? DBNull.Value : (object)AdjDate,
                                    LoanSancDate.Equals("") ? DBNull.Value : (object)LoanSancDate,
                                    txtRefNo.Text.Equals("") ? DBNull.Value : (object)txtRefNo.Text,
                                    RefDate.Equals("") ? DBNull.Value : (object)RefDate,
                                    txtDisbursementNo.Text.Equals("")? DBNull.Value :(object)txtDisbursementNo.Text,
                                    DisDate.Equals("")? DBNull.Value:(object)DisDate,
                                    FisrstInstall, FirstAmount,
                                    SecondInstall, SecondAmount,
                                    ThirdInstall, ThirdAmount,
                                    Session[SiteConstants.SSN_INT_USER_ID]);

                                cmdSave.Text = "Add";
                                dv.ChangeMode(FormViewMode.Insert);
                                //PopulateEmployee(SecID);
                                PopulateLoanDesc();
                                dv.DataBind();
                                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
                            }
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Sorry ! Your salary has been already Processed. You can not Save.')</script>");
                    }
                }

            }
            else if (cmdSave.CommandName == "Edit")
            {

                string SecID = ((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue;
                DataTable dtEncodeValue = DBHandler.GetResult("Get_SalPayMonth", SecID, HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);
                if (dtEncodeValue.Rows.Count > 0)
                {
                    string EncValue = dtEncodeValue.Rows[0]["EncodeValue"].ToString();
                    if (EncValue == "" || EncValue == null)
                    {
                        TextBox txtLoanAmt = (TextBox)dv.FindControl("txtLoanAmt");
                        TextBox txtInstallAmt = (TextBox)dv.FindControl("txtInstallAmt");
                        TextBox txtLoanDeducStrt = (TextBox)dv.FindControl("txtLoanDeducStrt");
                        TextBox txtTotLoanInstl = (TextBox)dv.FindControl("txtTotLoanInstl");
                        TextBox txtCurLoanInst = (TextBox)dv.FindControl("txtCurLoanInst");
                        TextBox txtLoanAmtPaid = (TextBox)dv.FindControl("txtLoanAmtPaid");
                        TextBox txtAdjDate = (TextBox)dv.FindControl("txtAdjDate");
                        TextBox txtAdjAmt = (TextBox)dv.FindControl("txtAdjAmt");
                        TextBox txtLoanSancDate = (TextBox)dv.FindControl("txtLoanSancDate");
                        TextBox txtRefNo = (TextBox)dv.FindControl("txtRefNo");
                        TextBox txtRefDate = (TextBox)dv.FindControl("txtRefDate");

                        TextBox txtDisbursementNo = (TextBox)dv.FindControl("txtDisbursementNo");
                        TextBox txtDisbursementDate = (TextBox)dv.FindControl("txtDisbursementDate");

                        DropDownList ddlLoanType = (DropDownList)dv.FindControl("ddlLoanType");
                        DropDownList ddlLoanDesc = (DropDownList)dv.FindControl("ddlLoanDesc");
                        DropDownList ddlEmployee = (DropDownList)dv.FindControl("ddlEmployee");
                        CheckBox chkCloseInd = (CheckBox)dv.FindControl("chkCloseInd");

                       string chkFlag= chkCloseInd.Checked == true ? "Y" : "N";
                       string Reason = hdnValidReason.Value;
                       if (chkFlag == "Y" && Reason == "")
                       {
                           ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>var tt=prompt('Please Enter a valid Reason to close the Loan.');$('#hdnValidReason').val(tt);</script>");
                           return;
                       }

                        string LoanDStart = txtLoanDeducStrt.Text; string LoanDedStart = "";
                        if (LoanDStart != null && LoanDStart != "")
                        {
                            DateTime dt = DateTime.ParseExact(LoanDStart, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            LoanDedStart = dt.ToString("dd/MM/yyyy");
                        }
                        else { LoanDedStart = LoanDStart; }

                        string Adate = txtAdjDate.Text; string AdjDate = "";
                        if (Adate != null && Adate != "")
                        {
                            DateTime dt = DateTime.ParseExact(Adate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            AdjDate = dt.ToString("dd/MM/yyyy");
                        }
                        else { AdjDate = Adate; }

                        string LoanSdate = txtLoanSancDate.Text; string LoanSancDate = "";
                        if (LoanSdate != null && LoanSdate != "")
                        {
                            DateTime dt = DateTime.ParseExact(LoanSdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            LoanSancDate = dt.ToString("dd/MM/yyyy");
                        }
                        else { LoanSancDate = LoanSdate; }

                        string Rdate = txtRefDate.Text; string RefDate = "";
                        if (Rdate != null && Rdate != "")
                        {
                            DateTime dt = DateTime.ParseExact(Rdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            RefDate = dt.ToString("dd/MM/yyyy");
                        }
                        else { RefDate = Rdate; }

                        string DisbursementDate = txtDisbursementDate.Text; string DisDate = "";
                        if (DisbursementDate != null && DisbursementDate != "")
                        {
                            DateTime dt = DateTime.ParseExact(DisbursementDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            DisDate = dt.ToString("dd/MM/yyyy");
                        }
                        else { DisDate = DisbursementDate; }

                        TextBox txtInstNo1 = (TextBox)dv.FindControl("txtInstNo1");
                        TextBox txtInstAmt1 = (TextBox)dv.FindControl("txtInstAmt1");

                        TextBox txtInstNo2 = (TextBox)dv.FindControl("txtInstNo2");
                        TextBox txtInstAmt2 = (TextBox)dv.FindControl("txtInstAmt2");

                        TextBox txtInstNo3 = (TextBox)dv.FindControl("txtInstNo3");
                        TextBox txtInstAmt3 = (TextBox)dv.FindControl("txtInstAmt3");

                        int FisrstInstall = txtInstNo1.Text == "" ? 0 : Convert.ToInt32(txtInstNo1.Text); double FirstAmount = txtInstAmt1.Text == "" ? 0 : Convert.ToInt32(txtInstAmt1.Text);
                        int SecondInstall = txtInstNo2.Text == "" ? 0 : Convert.ToInt32(txtInstNo2.Text); double SecondAmount = txtInstAmt2.Text == "" ? 0 : Convert.ToInt32(txtInstAmt2.Text);
                        int ThirdInstall = txtInstNo3.Text == "" ? 0 : Convert.ToInt32(txtInstNo3.Text); double ThirdAmount = txtInstAmt3.Text == "" ? 0 : Convert.ToInt32(txtInstAmt3.Text);

                        int TotalInstall = (FisrstInstall + SecondInstall + ThirdInstall);
                        int EnterTotalInstall = txtTotLoanInstl.Text == "" ? 0 : Convert.ToInt32(txtTotLoanInstl.Text);
                        if (TotalInstall != EnterTotalInstall)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Total Entered Installment No. is not correct.')</script>");
                            txtTotLoanInstl.Focus();
                            return;
                        }

                        double TotalLoanAmt = ((FisrstInstall * FirstAmount) + (SecondInstall * SecondAmount) + (ThirdInstall * ThirdAmount));
                        double EnterTotalLoanAmt = txtLoanAmt.Text == "" ? 0 : Convert.ToDouble(txtLoanAmt.Text);
                        double LoanAmount = Convert.ToDouble(EnterTotalLoanAmt);

                        if (TotalLoanAmt != LoanAmount)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Total Entered Loan Amount does not match.')</script>");
                            txtLoanAmt.Focus();
                            return;
                        }
                        int LoanID = Convert.ToInt32(((HiddenField)dv.FindControl("hdnID")).Value);

                        DBHandler.Execute("Update_Loan", 
                            LoanID,//01
                            (ddlLoanDesc.SelectedValue).Equals("") ? DBNull.Value : (object)Convert.ToInt32(ddlLoanDesc.SelectedValue),//02
                            (ddlEmployee.SelectedValue).Equals("") ? DBNull.Value : (object)Convert.ToInt32(ddlEmployee.SelectedValue),//03
                            (txtLoanAmt.Text).Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtLoanAmt.Text),//04
                            0,//05
                            LoanDedStart,//06
                            (txtTotLoanInstl.Text).Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtTotLoanInstl.Text),//07
                            (txtCurLoanInst.Text).Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtCurLoanInst.Text),//08
                            (txtLoanAmtPaid.Text).Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtLoanAmtPaid.Text),//09
                            (chkCloseInd.Checked == true ? "Y" : "N"),//10
                            (txtAdjAmt.Text).Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtAdjAmt.Text),//11
                            AdjDate.Equals("") ? DBNull.Value : (object)AdjDate,//12
                            LoanSancDate.Equals("") ? DBNull.Value : (object)LoanSancDate,//13
                            txtRefNo.Text.Equals("") ? DBNull.Value : (object)txtRefNo.Text,//14
                            RefDate.Equals("") ? DBNull.Value : (object)RefDate,//15
                            txtDisbursementNo.Text.Equals("") ? DBNull.Value : (object)txtDisbursementNo.Text,//16
                            DisDate.Equals("") ? DBNull.Value : (object)DisDate,//17
                            Reason.Equals("") ? DBNull.Value:(object)Reason,//18
                            FisrstInstall,//19 
                            FirstAmount,//20
                            SecondInstall, //21
                            SecondAmount,//22
                            ThirdInstall,//23
                            ThirdAmount,//24
                            Session[SiteConstants.SSN_INT_USER_ID]//25
                            );

                        cmdSave.Text = "Create";
                        dv.ChangeMode(FormViewMode.Insert);
                         //ddlLoanDesc = (DropDownList)dv.FindControl("ddlLoanDesc");
                         //OnChangeModeToInsert();
                        dv.DataBind();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                        string EmpNo = txtEmpNo.Text.Trim();
                        PopulateGrid(EmpNo);
                        return;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Sorry ! Your salary has been already Processed. You can not Save.')</script>");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

   

    protected void ddlLoanType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int SecID = Convert.ToInt32(((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue);
            string SessionSecID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
            PopulateLoanDesc();
            if (SecID != 0 || SessionSecID != "0")
                PopulateEmployee(SecID == 0 ? Convert.ToInt32(SessionSecID) : SecID);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void PopulateLoanDesc()
    {
        try
        {
            var x = dv.CurrentMode;
            DropDownList ddlLoanType = (DropDownList)dv.FindControl("ddlLoanType");
            DropDownList ddlLoanDesc = (DropDownList)dv.FindControl("ddlLoanDesc");
            DataTable dt = DBHandler.GetResult("Get_LoanDesc", ddlLoanType.SelectedValue);
            ddlLoanDesc.Items.Clear();
            if (dt.Rows.Count > 0)
            {
                ddlLoanDesc.Items.Insert(0, new ListItem("(Select Loan Description)", "0"));
                ddlLoanDesc.DataSource = dt;
                ddlLoanDesc.DataTextField = "LoanDesc";
                ddlLoanDesc.DataValueField = "LoanTypeID";
                ddlLoanDesc.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void PopulateEmployee(int SecID)
    {
        try
        {
            string SessionSecID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
            DataSet ds = DBHandler.GetResults("Get_EmployeebySector", SecID == 0 ? Convert.ToInt32(SessionSecID) : SecID);
            DataTable dtEmp = ds.Tables[0];
            DropDownList ddlEmployee = (DropDownList)dv.FindControl("ddlEmployee");
            ddlEmployee.Items.Clear();
            if (dtEmp.Rows.Count > 0)
            {
                ddlEmployee.Items.Insert(0, new ListItem("(Select Employee)", "0"));
                ddlEmployee.DataSource = dtEmp;
                ddlEmployee.DataTextField = "EmpName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string CheckforAvailableLoan(string EmployeeID, string LoanType, string LoanDesc)
    {
        string JSonVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Get_CheckforAvailableLoan", EmployeeID, LoanDesc);
            if (dt.Rows.Count > 0)
            {
                string value="";
                string Result = dt.Rows[0]["Count"].ToString();
                if(Result == "1")
                    value = "Available";
                else
                    value = "NotAvailable";
                JSonVal = value.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSonVal;
    }

    [WebMethod]
    public static string GetEmployee(int SecID)
    {
        string SessionSecID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
        DataSet ds = DBHandler.GetResults("Get_EmployeebySector", SecID == 0 ? Convert.ToInt32(SessionSecID) : SecID);
        DataTable dtEmp = ds.Tables[0];

        List<EmployeebySector> listEmployee = new List<EmployeebySector>();

        if (dtEmp.Rows.Count > 0)
        {
            for (int i = 0; i < dtEmp.Rows.Count; i++)
            {
                EmployeebySector objst = new EmployeebySector();

                objst.EmployeeID = Convert.ToInt32(dtEmp.Rows[i]["EmployeeID"]);
                objst.EmpName = Convert.ToString(dtEmp.Rows[i]["EmpName"]);

                listEmployee.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listEmployee);
    }
    public class EmployeebySector
    {
        public int EmployeeID { get; set; }
        public string EmpName { get; set; }
    }

    protected void dv_ModeChanged(object sender, EventArgs e)
    {
       
    }
    private void OnChangeModeToInsert()
    {
        FormView f = dv;
        if (f.CurrentMode == FormViewMode.Insert)
        {
            PopulateLoanDesc();
            int SecID = Convert.ToInt32(((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue);
            PopulateEmployee(SecID);
        }
    }
    protected void Unnamed_Click(object sender, EventArgs e)
    {
        if (dv.CurrentMode == FormViewMode.Insert)
        {
            dv.ChangeMode(FormViewMode.Edit);
        }
        else
        {
            dv.ChangeMode(FormViewMode.Insert);
        }
        dv.DataBind();
        PopulateLoanDesc();
        int SecID = Convert.ToInt32(((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue);
        PopulateEmployee(SecID);
    }
}