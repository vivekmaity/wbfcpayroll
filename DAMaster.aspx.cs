﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Globalization;

public partial class DAMaster : System.Web.UI.Page
{
    public static string hdnDaID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //PopulateGrid();
                //string ef = DateTime.Now.ToShortDateString();
                //TextBox txtEffFrm = (TextBox)dv.FindControl("txtEffFrm");
                //TextBox txtEffTo = (TextBox)dv.FindControl("txtEffTo");
                //txtEffFrm.Text = ef;
                //txtEffTo.Text = "31/12/2070";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                //PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString(),"");

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                GridViewRow grdrow = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
                int rowIndex = grdrow.RowIndex;
                Label val = (Label)(tbl.Rows[rowIndex].Cells[0].FindControl("lblEmpType"));
                string EmpType = val.Text;
                string empType = "";
                if (EmpType !="")
                {
                    if (EmpType == "KMDA Employee")
                        empType = "K";
                    else if (EmpType == "Central Employee")
                        empType = "C";
                    else
                        empType = "S";

                    UpdateDeleteRecord(2, e.CommandArgument.ToString(), empType);
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid(string EmpType)
    {
        try
        {
            //DataTable dtstr = DBHandler.GetResult("Get_DA");
            DataTable dtstr = DBHandler.GetResult("Get_DAbyEmpType", EmpType);
            if (dtstr.Rows.Count > 0)
            {
                for (int j = 0; j < dtstr.Rows.Count; j++)
                {
                    string EffTo = dtstr.Rows[j]["EffectiveTo"].ToString();
                    if (EffTo == "")
                        dtstr.Rows[j]["EffectiveTo"] = "ACTIVE";
                }
                tbl.DataSource = dtstr;
                tbl.DataBind();
            }
            else
            {
                tbl.DataSource = dtstr;
                tbl.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateDeleteRecord(int flag, string ID, string EmpType)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_DAByID", ID);
                dv.DataSource = dtResult;
                hdnDaID = dtResult.Rows[0]["DAID"].ToString();
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlEmpType")).SelectedValue = dtResult.Rows[0]["EmpType"].ToString();
                }
            }
            else if (flag == 2)
            {
                DataTable dt = DBHandler.GetResult("Delete_DAByID", ID, Session[SiteConstants.SSN_INT_USER_ID]);
                if (dt.Rows.Count > 0)
                {
                    string status = dt.Rows[0]["status"].ToString();
                    if (status == "Y")
                    {
                        dv.ChangeMode(FormViewMode.Insert);
                        PopulateGrid(EmpType);
                        dv.DataBind();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Sorry ! You can not Delete this DA.\nBecause this DA is Already used by other Employee.')</script>");
                    }
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
                TextBox txtOrderNo = (TextBox)dv.FindControl("txtOrderNo");
                TextBox txtOrderDate = (TextBox)dv.FindControl("txtOrderDate");
                TextBox txtDARate = (TextBox)dv.FindControl("txtDARate");
                TextBox txtMaxAmt = (TextBox)dv.FindControl("txtMaxAmt");
                TextBox txtEffFrm = (TextBox)dv.FindControl("txtEffFrm");

                string OrderDate = txtOrderDate.Text;
                string EffFrm = txtEffFrm.Text;
                string monthYear = Convert.ToDateTime(txtEffFrm.Text).ToString("MM/yyyy").Replace('-', '/');
                DataTable dt = DBHandler.GetResult("Insert_DA", 
                    ddlEmpType.SelectedValue, 
                    txtOrderNo.Text, 
                    DateTime.ParseExact(OrderDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"),
                    Convert.ToDouble(txtDARate.Text),
                    Convert.ToDouble(txtMaxAmt.Text),
                    DateTime.ParseExact(EffFrm.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"),
                    Session[SiteConstants.SSN_INT_USER_ID],
                    monthYear.ToString());

                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid(ddlEmpType.SelectedValue);
                dv.DataBind();
                if (dt.Rows.Count > 0)
                {
                    string Result = dt.Rows[0]["MSG"].ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + Result + "')</script>");
                }
            }
            else if (cmdSave.CommandName == "Edit")
            {
                DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
                TextBox txtOrderNo = (TextBox)dv.FindControl("txtOrderNo");
                TextBox txtOrderDate = (TextBox)dv.FindControl("txtOrderDate");
                TextBox txtDARate = (TextBox)dv.FindControl("txtDARate");
                TextBox txtMaxAmt = (TextBox)dv.FindControl("txtMaxAmt");
                TextBox txtEffFrm = (TextBox)dv.FindControl("txtEffFrm");
                TextBox txtEffTo = (TextBox)dv.FindControl("txtEffTo");

                string OrderDate = txtOrderDate.Text;
                string EffFrm = txtEffFrm.Text;
                //string EffTo =txtEffTo.Text;

                string ID = hdnDaID;
                    //((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("Update_DA", 
                    ID, 
                    ddlEmpType.SelectedValue, 
                    txtOrderNo.Text,
                    DateTime.ParseExact(OrderDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"), 
                    Convert.ToDouble(txtDARate.Text),
                    Convert.ToDouble(txtMaxAmt.Text),
                    DateTime.ParseExact(EffFrm.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"),
                    //DateTime.ParseExact(EffTo.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd"),
                    Session[SiteConstants.SSN_INT_USER_ID]);

                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid(ddlEmpType.SelectedValue);
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void ddlEmpType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
            string EmpType = ddlEmpType.SelectedValue;

            DataTable dtstr = DBHandler.GetResult("Get_DAbyEmpType", EmpType);

            if (dtstr.Rows.Count > 0)
            {
                for (int j = 0; j < dtstr.Rows.Count; j++)
                {
                    string EffTo = dtstr.Rows[j]["EffectiveTo"].ToString();
                    if (EffTo == "")
                        dtstr.Rows[j]["EffectiveTo"] = "ACTIVE";
                }
                tbl.DataSource = dtstr;
                tbl.DataBind();
            }

       }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}