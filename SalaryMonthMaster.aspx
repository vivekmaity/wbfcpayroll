﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="SalaryMonthMaster.aspx.cs" Inherits="SalaryMonthMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        function beforeSave() {
            $("#form1").validate();
            $("#txtSalMon").rules("add", { required: true, messages: { required: "Please enter Sector Code"} });
            $("#txtSectorName").rules("add", { required: true, messages: { required: "Please enter Sector Name"} });
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Salary Month Master</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Salary Month&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtSalMon"  autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="7"></asp:TextBox>                            
                                        </td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>

                            <EditItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                        <td style="padding:10px;"> <span class="headFont">Sector Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtSalMon" autocomplete="off" Text='<%# Eval("SalMonth") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="7"></asp:TextBox>                            
                                            <%--<asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("SalMonthID") %>'/>--%>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:auto;width:100%">
                            <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="SalMonthID" OnRowCommand="tbl_RowCommand" AllowPaging="false" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("SalMonthID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                                OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("SalMonthID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SalMonthID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SalMonthID" />
                                    <asp:BoundField DataField="SalMonth" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Salary Month" />
                                </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

