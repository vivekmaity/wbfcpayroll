﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class UpdateDaHraMa : System.Web.UI.Page
{
    public static string SectorID = "";
    public static string EarningTypes = "";
    public static string EmpNos = "";
    public static string EmpTypes = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AjaxGetGridCtrl(string EarningType, string DaHraMaValue, string EmpType, string EmpNo)
    {
        EarningTypes = EarningType;
        EmpNos = EmpNo;
        EmpTypes = EmpType;
        string SalMonthID = HttpContext.Current.Session[SiteConstants.SSN_PAYMONTHID].ToString();
        string SecID = SectorID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
        string AdminID  = HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID].ToString();
        try
        {
            return GetGridControlContent("~/GridviewTemplete.ascx", EarningType, DaHraMaValue, EmpType, SecID, SalMonthID, AdminID, EmpNo);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private static String GetGridControlContent(String controlLocation, string EarningType, string DaHraMaValue, string EmpType, string SecID, string SalMonthID, string AdminID, string EmpNo)
    {
        try
        {
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.DaHraMaEmpNo = EmpNo;
            userControl.DaHraMaEarningType = EarningType;
            userControl.DaHraMaValue = DaHraMaValue;
            userControl.DaHraMaEmpType = EmpType;
            userControl.DaHraMaSecID = SecID;
            userControl.DaHraMaPayMonthID = SalMonthID;
            userControl.DaHraMaAdminID = AdminID;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string CountTotalEmployee()
    {
        string JsonVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Get_ForEmpDaHraMaDetail", SectorID, EarningTypes, EmpNos, EmpTypes);
            if (dt.Rows.Count > 0)
            {
                string TotalEmp=dt.Rows.Count.ToString();
                JsonVal=TotalEmp.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JsonVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateEmpDaHraMa(string EmpNos, string decline, string remarks)
    {
        string JSonVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Update_EmpDaHraMa", EmpNos, decline, remarks, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (dt.Rows.Count > 0)
            {
                string Result = dt.Rows[0]["Result"].ToString();
                if (Result == "Updated")
                {
                    JSonVal = Result.ToJSON();
                }
                else
                {
                    string Rest = "NoUpdated";
                    JSonVal = Rest.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSonVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateFinalEmpDaHraMa(string Daid)
    {
        string JSonVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Update_FinalEmpDaHraMa", SectorID, Daid, EarningTypes, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (dt.Rows.Count > 0)
            {
                string Result = dt.Rows[0]["Result"].ToString();
                if (Result == "Updated")
                {
                    JSonVal = Result.ToJSON();
                }
                else
                {
                    string Rest = "NoUpdated";
                    JSonVal = Rest.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSonVal;
    }

    

    //This is for Binding 'DA' on the Base of Employee Type
    [WebMethod]
    public static string BindDA(string EmpType)
    {

        DataSet ds = DBHandler.GetResults("Get_DAbyEmpType", EmpType);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<DA> listdA = new List<DA>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DA objst = new DA();

                objst.DAID = Convert.ToInt32(dt.Rows[i]["DAID"]);
                objst.DARate = Convert.ToString(dt.Rows[i]["DARate"]);
                listdA.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listdA);
    }
    public class DA
    {
        public int DAID { get; set; }
        public string DARate { get; set; }
    }

    //This is for Binding 'HRA' on the Base of Employee Type
    [WebMethod]
    public static string BindHRA(string EmpType)
    {

        DataSet ds = DBHandler.GetResults("c", EmpType);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<HRAS> listHRAS = new List<HRAS>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HRAS objst = new HRAS();

                objst.HRAID = Convert.ToInt32(dt.Rows[i]["HRAID"]);
                objst.HRA = Convert.ToString(dt.Rows[i]["HRA"]);
                listHRAS.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listHRAS);
    }
    public class HRAS
    {
        public int HRAID { get; set; }
        public string HRA { get; set; }
    }

    //This is for Binding 'MA' on the Base of Employee Type
    [WebMethod]
    public static string BindMA(string EmpType)
    {

        DataSet ds = DBHandler.GetResults("Load_MA");
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<MA> listMA = new List<MA>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MA objst = new MA();

                objst.MAID = Convert.ToInt32(dt.Rows[i]["EDID"]);
                objst.PayVal = Convert.ToString(dt.Rows[i]["PayVal"]);
                listMA.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listMA);
    }
    public class MA
    {
        public int MAID { get; set; }
        public string PayVal { get; set; }
    }
}