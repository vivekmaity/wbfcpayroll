﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LPCreport.aspx.cs" Inherits="LPCreport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        var ParameterTypes = "S";
        $(document).ready(function () {
            $('#rdSec').click(function () {
                if (document.getElementById('rdSec').checked) {
                    ParameterTypes = "S";
                }
            });

            $('#rdOffice').click(function () {
                if (document.getElementById('rdOffice').checked) {
                    ParameterTypes = "O";
                }
            });
        });

        $(document).ready(function () {
            
            $("#txtEmpCodeSearch").autocomplete({
                source: function (request, response) {
                    var E = "";
                    var text = request.term; 
                    if (text != "") {
                        E = "{prefix:'" + request.term + "',ParameterTypes:'" + ParameterTypes + "'}";
                        //E = request.term;
                        //alert(E);
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/AjaxGetGridCtrl',
                            data: E,
                           
                            contentType: "application/json; charset=utf-8",
                            success: function (listTranferOrder) {
                                var t = jQuery.parseJSON(listTranferOrder.d); //alert(JSON.stringify(item));
                                //$("#ddTransOrder").val(item);
                                var a = t.length;
                                $("#ddTransOrder").empty();
                                if (a >= 0) {
                                    $("#ddTransOrder").append("<option value='0'>(Select Tranfer Order)</option>")
                                    $.each(t, function (key, value) {
                                        $("#ddTransOrder").append($("<option></option>").val(value.TransferOrderNo).html(value.TransferOrderNo));
                                    });
                                }
                            }
                        });
                    }
                    else {
                        var PagingCondition = "PageLoad";
                        var E = "{pgIndex:" + 0 + ", pgSize:" + 10 + ", Condition:'" + PagingCondition + "'}";
                        //alert(E);
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/AjaxPaging',
                            data: E,
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                var item = data.d; //alert(item);
                                $("#gvtbl").html(item);
                            },
                            error: function (response) {
                                alert('Some Error Occur');
                            },
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }
                },
                minLength: 0
            });
        });
       

       
        
       
       

        function beforeSave() {
            $("#form1").validate();

        }

        

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }


        function PrintOpentab() {
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            var StrEmpNo = '';
            var TransOrder = '';
            
            FormName = "LPCreport.aspx";
            ReportName = "Kmda_LPC";
            ReportType = "LPCreport";
           
            if ($('#txtEmpCodeSearch').val() == '') {
                alert("Employee No. can not be null");
                $('#txtEmpCodeSearch').focus();
                return false;
            }

            

            StrEmpNo = $("#txtEmpCodeSearch").val();
            TransOrder = $("#ddTransOrder").val();
            
           
             $(".loading-overlay").show();
            var E = '';
            E = "{StrEmpNo:'" + StrEmpNo + "',TransOrder:'" + TransOrder + "', FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                        
            $.ajax({

                type: "POST",
                url: "LPCreport.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    $(".loading-overlay").hide();
                    window.open("ReportView1.aspx?");
                }
            });
        }


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style type="text/css">
        .style7
        {
            height: 31px;
        }
        .style16
        {
            height: 31px;
            width: 129px;
        }
        .style17
        {
            width: 129px;
        }
        .style18
        {
            width: 97px;
        }
        .style19
        {
            width: 341px;
        }
        .style20
        {
            height: 31px;
            width: 341px;
        }
        .style21
        {
            height: 16px;
            width: 129px;
        }
        .style22
        {
            height: 16px;
        }
        .style23
        {
            height: 16px;
            width: 341px;
        }
        .style24
        {
            width: 97px;
            height: 16px;
        }
        .auto-style1 {
            width: 219px;
        }
        .auto-style2 {
            width: 219px;
            height: 16px;
        }
    </style>
</asp:Content>
          
<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>LPC Report</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding: 5px; vertical-align: top;">
                                            <asp:RadioButton ID="rdSec" Checked="true" class="headFont" runat="server"  GroupName="a" Text="Sector" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdOffice" class="headFont" runat="server"  GroupName="a" Text="Office" Checked="false" />
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont"  >Employee No. &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left"  >
                                        <asp:TextBox ID="txtEmpCodeSearch" ClientIDMode="Static" oncopy="return true" onpaste="return true" oncut="return true"
                                         runat="server" MaxLength="6" CssClass="textbox" Width="200px"></asp:TextBox>
                                         </td>   

                                        <td style="padding:5px;" class="style1" bgcolor="White" ><span class="headFont"> Transfer Order &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" bgcolor="White" >
                                       <asp:DropDownList ID="ddTransOrder" runat="server" class="headFont" Width="230px" Height="28px" CssClass="textbox" DataValueField="TransferOrderNo" DataTextField="TransferOrderNo"
                                        AppendDataBoundItems="true" Enabled="true" ClientIDMode="Static">
                                        <asp:ListItem Text="Select Transfer Order" Selected="True" Value="0"></asp:ListItem>
                                        </asp:DropDownList> 

                                        </td>
                                        
                                        </tr>
                                </table> 
                           
                                <table align="center"  width="100%">
                                <tr><td  ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    
                                    <%--<td style="padding:5px;" class="style1"><span class="headFont">* indicates Mandatory Field &nbsp;&nbsp;</span> </td>
                                    <td style="padding:5px;">&nbsp;</td>--%>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:450px;">
                                        <asp:Button ID="cmdShow" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="PrintOpentab(); return false;" 
                                            BorderColor="#669999"/> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Reset"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdReset_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                   <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                        </div>    
                                   
                                    </td>
                                   
                                </tr>
                                
                                </table>
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

