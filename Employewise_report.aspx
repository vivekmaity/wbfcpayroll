﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" CodeFile="Employewise_report.aspx.cs" Inherits="Employewise_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
    <script>
        $(document).ready(function () {
            fin_year();
            $("#txtEmpName").autocomplete({
                source: function (request, response) {
                    //alert(pageUrl);
                    $.ajax({
                        type: "POST",
                        url: 'Employewise_report.aspx/GetEmpCodeWithName',
                        data: "{prefix:'" + request.term + "',Sectorid:'" + $("#ddlSector").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {

                            alert('Some Error Occur');
                            // $(".loading-overlay").hide();
                        },
                        failure: function (response) {
                            alert(response.d);
                            //$(".loading-overlay").hide();
                        }
                    });
                },
                appendTo: "#AutoComplete-EmployeeNo",
                select: function (e, i) {
                    $("#txtEmpName").val(i.item.label);
                    $("#hdntxtEmpID").val(i.item.val);
                    return false;
                    //alert($("#hdnSearchEmpID").val(i.item.val));

                },
                minLength: 1
            })
            .click(function () {
                $("#txtEmpName").val(''); $("#hdntxtEmpID").val('');
                $(this).autocomplete('search', ($(this).val()));
            });
            $("#cmdCancel").click(function () {
                $("#hdntxtEmpID").val('');
                Location.reload();
            });
           

        })

        function PrintOpentab() {

            //$(".loading-overlay").show();
            var SalaryFinYear;
            var SalMonth;
            var SectorID;
            var CenterID;
            var EmpType;
            var Status;
            var SchType;
            var SectorGroup;
            var CenterGroup;
            var EmpTypeGroup;
            var StatusGroup;
            var EDID;
            var UserID;

            SalaryFinYear = $('#txtSalFinalYear').val();
            SalMonth = $('#txtPayMonths').val();
            var salmonthID = 0;
            var E = "{SalmonthID: " + salmonthID + ",SalaryFinYear:'" + SalaryFinYear + "',SalMonth:'" + SalMonth + "',SectorID: " + $('#ddlSector').val() + ",CenterID: 0,EmpType:'K',Status:'K',EDID:" + $('#hdntxtEmpID').val() + ",ChildreportID:40,reportHisCur:'',isPDFExcel:1}";
            //console.log(E); return false;
            $(".loading-overlay").show();
            $.ajax({

                type: "POST",
                url: "Employewise_report.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    window.open("ReportView_b.aspx?ReportID=40&SalMonth=" + SalMonth + "&ReportTypeID=4");
                    $(".loading-overlay").hide();
                }
            });


        }
        function fin_year() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var E = "{sectorid: " + $('#ddlSector').val() + "}";
            var options = {};
            options.url = "PaySlipReport.aspx/FinYearForReport";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (FinYear) {
                var t = jQuery.parseJSON(FinYear.d);
                var a = t.length;
                if (a >= 0) {
                    //$("#ddlfinyear").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#txtSalFinalYear").append($("<option></option>").val(value.SalaryFinYear).html(value.SalaryFinYear));
                    });
                    var myVal = $('#txtSalFinalYear option:last').val();
                    $('#txtSalFinalYear').val(myVal);
                }
            };
            options.error = function () { alert("Error in retrieving Ina year!"); };
            $.ajax(options);
        }
    </script>
    <style type="text/css">
        #AutoComplete-EmployeeNo {
            display: block;
            position: relative;
        }
         .ui-autocomplete {
            height: 200px;
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #cbc7c7;
            font-size: 13px;
            font-weight: normal;
            color: #242424;
            background: #fff;
            -moz-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            -webkit-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            box-shadow: inset 0.9px 1px 3px #e4e4e4;
            width: 350px;
            /*height:30px;*/
            padding: 5px;
            font-family: Segoe UI, Lucida Grande, Arial, Helvetica, sans-serif;
            margin: 3px 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Employeewise Report</h2>						
						</section>
                        <table style="border:solid 2px lightblue;width:98%;"  >
                            <tr>
                                <td style="padding:15px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td style="padding:5px;"></td>
                                            <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding:5px;" >:</td>
                                            <td style="padding:5px;text-align:left;" >
                                                <asp:TextBox ID="txtEmpName" autocomplete="off" placeholder="--AutoComplete--" runat="server" CssClass="textbox" Style="width: 90%;" ></asp:TextBox>
                                                <asp:HiddenField ID="hdntxtEmpID" runat="server" Value="" ClientIDMode="Static" />
                                                <div id="AutoComplete-EmployeeNo"></div>
                                            </td>
                                            <td style="padding:5px;" ><span class="headFont">Financial Year &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding:5px;" >:</td>
                                            <td style="padding:5px;" align="left" >
                                                <asp:DropDownList ID="txtSalFinalYear" autocomplete="off" Width="200px" Height="25px" CssClass="textbox" Enabled="true" runat="server"></asp:DropDownList>
                                                <asp:HiddenField ID="hdnuserID" ClientIDMode="Static" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" width="100%" style="margin-top: 40px;">
                                        <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                        <tr>
                                            <td style="padding:5px;" ><span class="require">*</span> Indicates Mandatory Field</td>
                                            <td style="padding:5px;">&nbsp;</td>
                                            <td style="padding:5px;" align="left" >
                                                <div style="float:left;margin-left:200px;">
                                                    <asp:Button ID="cmdSave" runat="server" Text="View" Width="100px" CssClass="Btnclassname" OnClientClick="PrintOpentab(); return false;" /> 
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                                    Width="100px" CssClass="Btnclassname"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

