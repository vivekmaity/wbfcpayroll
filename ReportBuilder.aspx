﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ReportBuilder.aspx.cs" Inherits="ReportBuilder" %>

<%@ Register TagPrefix="art" TagName="EmpdetailforDesig" Src="GridviewTemplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">

    <link href="css/style.css" rel="stylesheet" />
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

    $(document).ready(function () {
        $("#cmdShow").hide();

        $(".DefaultButton").click(function (event) {
            event.preventDefault();
        });

        $("#Close").click(function (e) {
            $("#overlay").hide();
            e.preventDefault();
        });

        $("#cmdShow").click(function (e) {

            var ReportID = $("#ddlReportName").val();
            var ReportName = $('#ddlReportName').find('option:selected').text();
            var ParmFlag = hdnparamflag;// $("#hdnParamflag").val();
            var C = "{ReportID: " + ReportID + ", ParmFlag: '" + ParmFlag + "'}";
            //alert(C);

            $.ajax({
                type: "POST",
                url: pageUrl + '/AjaxGetGridCtrl',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    //alert(JSON.stringify(D));

                    $('#Header').text(ReportName);
                    $('#mainDiv').html(D.d);
                    $("#overlay").show();
                },
                error: function (response) {
                    alert('');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        });
    });
    var hdnparamflag = "";
$(document).ready(function () {
    $("#ddlReportName").change(function () {
        if ($("#ddlReportName").val() != "Please select") {

            var ReportID = $("#ddlReportName").val();
            var C = "{ReportID: " + ReportID + "}";
            //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/ShowButtonForReport',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    //alert(JSON.stringify(D));
                    var da = JSON.parse(D.d);
                    //alert(da);
                    if (da == 'N') {
                       // $("#hdnParamflag").val(da);
                        hdnparamflag = da;
                   $("#cmdShow").show();
                       
                    }
                    else
                        $("#cmdShow").hide();
                },
                error: function (response) {
                    alert('');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    });
});
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
<div id="main">
<asp:ScriptManager ID="sm" runat="server" ScriptMode="Release" EnablePageMethods="true"></asp:ScriptManager>
<%--<asp:HiddenField ID="hdnParamflag" runat="server" Value="" />--%>
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Report Builder Master</h2>
                    </section>

                    <div class="content-overlay" id='overlay'>
                        <div class="wrapper-outer">
                            <div class="wrapper-inner">
                                <div class="loadContent">
                                    <div id="Close" class="close-content" ><a href="javascript:void(0);" id="prev" style="color:red;">Close</a></div>
                                    <div style="float: left;">
                                        <h1 style="font-family:Verdana;  font-size:18px; font-weight:bold;">
                                        <span><asp:Label ID="Header" ClientIDMode="Static" runat="server" Text=""></asp:Label>
                                        </span>
                                        </h1>
                                    </div>
                                    <table cellpadding="5" style="border: solid 1px lightblue; padding-bottom: 10px; width: 650px; overflow: scroll;" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table align="center" cellpadding="5">
                                                    <tr>
                                                        <td>
                                                            <div class='divCaption' id='divCaption'></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="mainDiv" runat="server" clientidmode="Static" style=" width:650px; height:440px; overflow:scroll;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="float: right;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                           </div>
                        </div>
                    </div>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td style="padding: 2px; width: 100px;">
                                                <span class="headFont">Report Name : &nbsp;&nbsp;</span><span class="require">*</span>
                                            </td>
                                            <td style="padding: 2px;" align="left">
                                                <asp:DropDownList ID="ddlReportName" Width="218px" Height="22px" ClientIDMode="Static"
                                                    runat="server" AppendDataBoundItems="true"
                                                    DataTextField="ReportHeader" DataValueField="ReportID" autocomplete="off">
                                                    <asp:ListItem Text="(Select Report Name)" Selected="True" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="cmdShow" runat="server" Text="Show Report" CommandName="Show" CausesValidation="false"
                                                    Width="100px" ClientIDMode="Static" CssClass="Btnclassname DefaultButton" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
</div>  
</asp:Content>

