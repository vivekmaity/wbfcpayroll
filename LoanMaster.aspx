﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LoanMaster.aspx.cs" Inherits="LoanMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>

    <script type="text/javascript" >
        $(document).ready(function () {
            $('#txtLoanDeducStrt').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow',
                onSelect: function (dates) {
                    var aa = $('#txtLoanDeducStrt').val();
                    var a = aa.split('/');
                    for (var i = 0; i < a.length; i++) {
                        date = a[0]; month = a[1]; year = a[a.length - 1];
                    }
                    $('#txtLoanDeducStrt').val("01"+"/"+month+"/"+ year);

                }
            });

            $("#btnLDSDatePicker").click(function () {
                $('#txtLoanDeducStrt').datepicker("show");
                return false;
            });

            $('#txtAdjDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow'
            });

            $("#btnAdjDatePicker").click(function () {
                $('#txtAdjDate').datepicker("show");
                return false;
            });

            $('#txtLoanSancDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow'
            });

            $("#btnLSancDatePicker").click(function () {
                $('#txtLoanSancDate').datepicker("show");
                return false;
            });

            $('#txtRefDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow'
            });

            $("#btnRefDatePicker").click(function () {
                $('#txtRefDate').datepicker("show");
                return false;
            });


            $('#txtDisbursementDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow'
            });

            $("#btnDisbursementDate").click(function () {
                $('#txtDisbursementDate').datepicker("show");
                return false;
            });

            $(".dpDate").datepicker("option", "dateFormat", "dd/mm/yy");
            $(".calendar").click(function () {
                $(".dpdate").datepicker("show");
                return false;
            });
        });
        /*=======================================================================================================================*/
        $(function () {
            $("[id$=txtLoanDeducStrt]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $(function () {
            $("[id$=txtAdjDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $(function () {
            $("[id$=txtLoanSancDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $(function () {
            $("[id$=txtRefDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        /*=======================================================================================================================*/
        function beforeSave() {
            $("#form1").validate();
            $("#ddlLoanType").rules("add", { required: true, messages: { required: "Please select Loan Type"} });
            $("#ddlLoanDesc").rules("add", { required: true, messages: { required: "Please select Loan Description"} });
            $("#ddlEmployee").rules("add", { required: true, messages: { required: "Please select Employee"} });
            $("#txtLoanAmt").rules("add", { required: true, messages: { required: "Please enter Loan Amount"} });
            $("#txtLoanDeducStrt").rules("add", { required: true, messages: { required: "Please select Loan Deduction Start Date"} });
            $("#txtTotLoanInstl").rules("add", { required: true, messages: { required: "Please enter total Loan Installment"} });
            $("#txtCurLoanInst").rules("add", { required: true, messages: { required: "Please enter current Loan Installment" } });
            $("#txtLoanAmtPaid").rules("add", { required: true, messages: { required: "Please enter Loan Amount Paid" } });
            $("#txtLoanSancDate").rules("add", { required: true, messages: { required: "Please enter Loan Sanction Date" } });
            $("#txtRefNo").rules("add", { required: true, messages: { required: "Please enter reference number"} });
            $("#txtRefDate").rules("add", { required: true, messages: { required: "Please select reference date"} });

           return $("#form1").validate();
        }
        function beforeSaveforEmpNo() {
            $("#form1").validate();
            $("#txtEmpNo").rules("add", { required: true, messages: { required: "Please enter Employee No." } });
        }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        //This is for 'Employee' Binding on the Base of 'Sector'
        $(document).ready(function () {
            // $("#ddlPayScaleType").prop("disabled", false);
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();
                $('#hdnSecID').val(s);
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + "}";
                        //alert(E);
                        var options = {};
                        options.url = "LoanMaster.aspx/GetEmployee";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listEmployee) {
                            var t = jQuery.parseJSON(listEmployee.d);
                            var a = t.length;
                            $("#ddlEmployee").empty();

                            if (a >= 0) {
                                $("#ddlEmployee").append("<option selected='true' value=''>(Select Employee)</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlEmployee").append($("<option></option>").val(value.EmployeeID).html(value.EmpName));
                                });
                            }

                            $("#ddlEmployee").prop("disabled", false);
                        };
                        options.error = function () { alert("Error in retrieving Employee!"); };
                        $.ajax(options);
                    }
                    else {
                        $("#ddlEmployee").empty();
                        $("#ddlEmployee").prop("disabled", true);
                    }
                }
                else {
                    $("#ddlEmployee").empty();
                    $("#ddlEmployee").append("<option value=''>(Select Employee)</option>")
                }
            });
            $(document).ready(function () {
                $("#ddlEmployee").change(function () {
                    var e = $('#ddlEmployee').val();
                    $('#hdnEmployeeID').val(e);
                    var LoanType = $('#ddlLoanType').val();
                    var LoanDesc = $('#ddlLoanDesc').val();
                    // alert(e); alert(LoanType); alert(LoanDesc);
                    var E = "{EmployeeID: '" + e + "', LoanType: '" + LoanType + "', LoanDesc: '" + LoanDesc + "'}";
                    //alert(E);
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/CheckforAvailableLoan',
                        data: E,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            //alert(JSON.stringify(D));
                            var data = jQuery.parseJSON(D.d);
                            //alert(data);
                            if (data == "Available") {
                                alert("This LoanType is already Available for this Employee.");
                                //$("#cmdSave").prop("disabled", true);
                            }
                            else {
                                //$("#cmdSave").prop("disabled", false);
                            }
                        },
                        error: function (response) {
                            alert('');
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                });
            });
        });

        //**************************************************************************************************************************
        $(document).ready(function () {
            $("#txtTotLoanInstl").blur("change", function () {
                var TotalInstal = $("#txtTotLoanInstl").val() == "" ? 0 : parseFloat($("#txtTotLoanInstl").val());
                var loanAmount = $("#txtLoanAmt").val() == "" ? 0 : parseFloat($("#txtLoanAmt").val());

                if (TotalInstal != "" || TotalInstal != null || TotalInstal != "undefined") {
                    CalculateLoanInstSlab();
                    calInst1st();
                    calInst2nd();
                }
            });
        });
        //**************************************************************************************************************************
        function CalculateLoanInstSlab() {
            var cal = ""; 
            var TotalInstal = $("#txtTotLoanInstl").val() == "" ? 0 : parseFloat($("#txtTotLoanInstl").val()); //alert(TotalInstal);

            $("#txtCurLoanInst").val(0);
            $("#txtLoanAmtPaid").val(0);

            if (TotalInstal == ""  ) {
                $("#txtInstNo1").val('');
                $("#txtInstAmt1").val('');
            }
        }
        function calInst1st() {
            var cal = "";
            var LoanAmt = $("#txtLoanAmt").val() == "" ? 0 : parseFloat($("#txtLoanAmt").val());
            var TotInstNo = $("#txtTotLoanInstl").val() == "" ? 0 : parseFloat($("#txtTotLoanInstl").val());
            if (TotInstNo == "") {
                $("#txt1stInst").val(cal);
            }
            else {
                cal = parseFloat(LoanAmt) / parseFloat(TotInstNo); //alert(cal);
                rem = parseFloat(LoanAmt) % parseFloat(TotInstNo); //alert(rem);
                if(rem !=0)
                    $("#txtInstNo1").val(TotInstNo - 1);
                else
                    $("#txtInstNo1").val(TotInstNo);

                   var rcal = Math.round(cal);
                   $("#txtInstAmt1").val(rcal);
            }
        }
        function calInst2nd() {
            var cal = ""; 
            var LoanAmt = $("#txtLoanAmt").val() == "" ? 0 : parseFloat($("#txtLoanAmt").val()); //alert(LoanAmt);
            var TotInstNo = $("#txtTotLoanInstl").val() == "" ? 0 : parseFloat($("#txtTotLoanInstl").val()); //alert(TotInstNo);
            var Inst1stAmt = $("#txtInstAmt1").val(); //alert(Inst1st);

            if (TotInstNo == "") {
                $("#txtInstNo2").val(cal);
            }
            else
            {
                cal = parseFloat(LoanAmt) - ((parseFloat(TotInstNo) - 1) * parseFloat(Inst1stAmt)); //alert(cal);
                rem = parseFloat(LoanAmt) % parseFloat(TotInstNo); //alert(rem);
                if (rem != 0) {
                    var rcal = Math.round(cal);
                    $("#txtInstAmt2").val(rcal);
                    $("#txtInstNo2").val(1);
                }
                else {
                    $("#txtInstAmt2").val('');
                    $("#txtInstNo2").val('');
                }

            }
        }
        //**************************************************************************************************************************
        $(document).ready(function () {
            $('#chkCloseInd').click(function () {
                if ($("#chkCloseInd").is(":checked")) {
                    var my_text = prompt('Please Enter a valid Reason to close the Loan.');
                    if (my_text)
                    {
                        $("#hdnValidReason").val(my_text);
                    } 
                }
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <asp:HiddenField ID="hdnEmployeeID" ClientIDMode="Static" runat="server" Value="" />
<asp:HiddenField ID="hdnSecID" ClientIDMode="Static" runat="server" Value="" />
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Loan Master</h2>						
						</section>
                        <asp:HiddenField ID="hdnValidReason" runat="server" Value="" ClientIDMode="Static" />


                        <tr>
                                        <td>
                                            <table align="center" width="100%">
                                                <tr>
                                                    <br />
                                                    <span style="font-size: 22px; font-weight: bold;">Employee No : &nbsp;&nbsp;</span>
                                                    <asp:TextBox ID="txtEmpNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="6" ></asp:TextBox>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="Search" CausesValidation="false"
                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSaveforEmpNo();' OnClick="cmdSearch_Click" />

                                                </tr>
                                                <tr>
                                                    <td>

                                                        <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                                            DataKeyNames="LoanID" OnRowCommand="tbl_RowCommand" EmptyDataText="No Data Found." AllowPaging="true" PageSize="10"
                                                            CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">

                                                            <AlternatingRowStyle BackColor="#FFFACD" />
                                                            <PagerSettings FirstPageText="First" LastPageText="Last"
                                                                Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Edit">
                                                                    <HeaderStyle />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("LoanID") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                               </asp:TemplateField>
                                                                <%--                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                                OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("LoanID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>--%>
                                                                <asp:BoundField DataField="LoanID" Visible="false" HeaderText="LoanID" />
                                                                <asp:BoundField DataField="LoanTypeID" Visible="false" HeaderText="LoanTypeID" />
                                                                <asp:BoundField DataField="LoanDesc" HeaderText="Loan Description" />
                                                                <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                                                <asp:BoundField DataField="LoanAmount" HeaderText="Loan Amount" ItemStyle-HorizontalAlign="Center" />
                                                                <%--<asp:BoundField DataField="InstallAmount" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Install Amount" />--%>
                                                                <asp:BoundField DataField="LoanDeducStartDate" HeaderText="Loan Deduction Start Date" ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="TotLoanInstall" HeaderText="Total Loan Install" ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="CurLoanInstall" HeaderText="Current Loan Install" ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="LoanAmountPaid" HeaderText="Loan Amount Paid" ItemStyle-HorizontalAlign="Center" />
                                                                <%--<asp:BoundField DataField="AdjustAmount"  HeaderText="Adjust Amount" ItemStyle-HorizontalAlign="Center" />--%>
                                                                <%--<asp:BoundField DataField="AdjustDate"  HeaderText="Adjust Date" ItemStyle-HorizontalAlign="Center" />--%>
                                                                <%--<asp:BoundField DataField="LoanSancDate"  HeaderText="Loan Sanction Date" ItemStyle-HorizontalAlign="Center" />--%>

                                                                <asp:BoundField DataField="inst_amt1" HeaderText="Installment" ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="inst_amt2" HeaderText="Last Installment" ItemStyle-HorizontalAlign="Center" />
                                                                <%--                                    <asp:BoundField DataField="ReferenceNo" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Reference No" />
                                    <asp:BoundField DataField="ReferenceDate" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Reference Date" />--%>

                                                                <%-- <asp:BoundField DataField="inst_no1" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="First installment no" />
                                    <asp:BoundField DataField="inst_amt1" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="First installment Amount" />
                                    <asp:BoundField DataField="inst_no2" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Second installment no" />
                                    <asp:BoundField DataField="inst_amt2" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Second installment Amount" />
                                    <asp:BoundField DataField="inst_no3" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Third installment no" />
                                    <asp:BoundField DataField="inst_amt3" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Third installment Amount" />--%>
                                                            </Columns>
                                                        </asp:GridView>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                        <br />
                        <table width="98%"   style="border:solid 2px lightblue;">
                    <tr>
                        <%--OnModeChanged="dv_ModeChanged"--%>
                        <%--OnClick="Unnamed_Click"--%>
                        <td style="padding:15px;">
                            <%--<asp:Button ID="Button1" Text="change mode"  runat="server" />--%>
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnClick="Unnamed_Click" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" OnModeChanged="dv_ModeChanged" >
                               
                            <InsertItemTemplate>
                                <%--<label>Insert Mode</label>--%>
                                <table align="center" width="100%">
                                   
                                    <tr>
                                        <td colspan="6" style="padding: 5px; background: #7DCEA0  ; width:100%;"><span style="font-weight: bold; font-family:Verdana; font-style:italic;"><font size="3px">Loan Description</font></span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Type &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlLoanType" Width="218px" Height="22px" runat="server" AppendDataBoundItems="true"
                                                
                                                AutoPostBack="false"  Enabled="false" autocomplete="off">
                                                <asp:ListItem Text="Loan" Value="N"></asp:ListItem>
                                                <%--<asp:ListItem Text="Earning Recovery" Value="B"></asp:ListItem>
                                                <asp:ListItem Text="Out of Account Loan" Value="O"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Loan Description&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <%-- DataValueField="LoanTypeID" DataTextField="LoanDesc"--%>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlLoanDesc" Width="218px" Height="22px" runat="server"
                                                AppendDataBoundItems="true" ClientIDMode="Static">
                                                <asp:ListItem Text="(Select Loan Description)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployee" Width="218px" runat="server" Height="22px" 
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static">
                                                <asp:ListItem Text="(Select Employee)" Value="" ></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont" style="background-color:red;color:white; font-size:18px;font-weight:bold;">Close Indicator&nbsp;&nbsp;<%--</span><span class="require">*</span>--%></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:CheckBox ID="chkCloseInd" runat="server" Enabled="false"  ClientIDMode="Static" Text="(Check   if   Yes)" BackColor="Yellow" Font-Size="20px" Font-Bold="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Amount &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanAmt" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18" onkeypress="return isNumberKey(event)" ></asp:TextBox>
                                        </td>
<%--                                        <td style="padding:5px;" ><span class="headFont">Install Amount &nbsp;&nbsp;</span><span class="require" >*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtInstallAmt" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Deduction Start &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtLoanDeducStrt" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                                <asp:ImageButton ID="btnLDSDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Total Loan Install&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtTotLoanInstl" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Current Loan Install&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtCurLoanInst" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Loan Amount Paid&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanAmtPaid" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Adjustment Date&nbsp;&nbsp;</span><%--<span class="require">*</span> --%></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtAdjDate" autocomplete="off"  ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                                <asp:ImageButton ID="btnAdjDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Adjustment Amount&nbsp;&nbsp;</span><%--<span class="require">*</span>--%></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtAdjAmt" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Sanction Date&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtLoanSancDate" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                                <asp:ImageButton ID="btnLSancDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Reference No.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtRefNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Reference Date&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtRefDate" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                                <asp:ImageButton ID="btnRefDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                    </tr>


                                  <%-----------------------------------------------Supriyo 28-08-17-----------------------------------------------------------%>

                                     <tr>
                                        
                                        <td style="padding:5px;" ><span class="headFont"> Loan Disbursement Sanction No&nbsp;&nbsp;</span><%--<span class="require">*</span>--%></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtDisbursementNo" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="18"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>

                                         <td style="padding:5px;" ><span class="headFont"> Disbursement Date.&nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr> 
                                                <asp:TextBox ID="txtDisbursementDate" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                                <asp:ImageButton ID="btnDisbursementDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                    </tr>
                                    
                                     
                                    <%--------------------------------------------------------------------------------- %>
                                    <%--------------------------Added on 11042015--------------------------------------%>
                                     <tr><td colspan="6" ><hr style="border:solid 1px lightblue" /></td></tr>
                                    <tr>
                                        <td colspan="6" style="padding: 5px; background: #7DCEA0; width:100%;"><span style="font-weight: bold; font-family:Verdana; font-style:italic;"><font size="3px">First Installment Slab</font></span></td>
                                    </tr>
                                    <tr>
                                    <td style="padding:5px;" ><span class="headFont">Installment No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstNo1" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </td>

                                    <td style="padding:5px;" ><span class="headFont">Installment Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstAmt1" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </td>
                                    </tr>
                                     <tr><td colspan="6" ><hr style="border:solid 1px lightblue" /></td></tr>
                                    <tr>
                                        <td colspan="6" style="padding: 5px; background: #7DCEA0;"><span  style="font-weight: bold;font-family:Verdana; font-style:italic;"><font size="3px">Second Installment Slab</font></span></td>
                                    </tr>
                                    
                                    <tr>
                                    <td style="padding:5px;" ><span class="headFont">Installment No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstNo2" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </td>

                                    <td style="padding:5px;" ><span class="headFont">Installment Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstAmt2" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </td>
                                    </tr>
                                    <tr><td colspan="6" ><hr style="border:solid 1px lightblue" /></td></tr>
                                    <tr>
                                        <td colspan="6" style="padding: 5px; background: #7DCEA0;"><span  style="font-weight: bold;font-family:Verdana; font-style:italic;"><font size="3px">Third Installment Slab</font></span></td>
                                    </tr>
                                    
                                    <tr>
                                    <td style="padding:5px;" ><span class="headFont">Installment No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstNo3" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </td>

                                    <td style="padding:5px;" ><span class="headFont">Installment Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstAmt3" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10"  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </td>
                                    </tr>

                                    <%---------------------------------------------------------------------------------%>
                                </table>
                            </InsertItemTemplate>
                            <EditItemTemplate>
                                <label>update mode</label>
                                <table align="center" width="100%">
                                    <tr>
                                        <td colspan="6" style="padding: 5px; background: #7DCEA0; width:100%;"><span style="font-weight: bold; font-family:Verdana; font-style:italic;"><font size="3px">Loan Description</font></span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Type &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlLoanType" Width="218px" Height="22px" runat="server" AppendDataBoundItems="true"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlLoanType_SelectedIndexChanged" Enabled="false" autocomplete="off">
                                                <asp:ListItem Text="(Select Loan Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Government Loan" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="Earning Recovery" Value="B"></asp:ListItem>
                                                <asp:ListItem Text="Out of Account Loan" Value="O"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("LoanID") %>'/>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Loan Description&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <%--DataValueField="LoanTypeID" DataTextField="LoanDesc"--%>
                                            <asp:DropDownList ID="ddlLoanDesc" Width="218px" Height="22px" runat="server" 
                                                AppendDataBoundItems="true" ClientIDMode="Static" Enabled="false">
                                                <asp:ListItem Text="(Select Loan Description)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployee" Width="218px" runat="server" Enabled="false" Height="22px"
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee)" Value="" ></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont" style="background-color:red;color:white; font-size:18px;font-weight:bold;">Close Indicator&nbsp;&nbsp;</span><%--<span class="require">*</span>--%></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:CheckBox ID="chkCloseInd" runat="server" ClientIDMode="Static" Text="(Check if Yes)" BackColor="Yellow" Font-Size="20px" Font-Bold="true" Checked='<%# Eval("CloseInd").ToString().Equals("1")  %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Amount &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanAmt" ClientIDMode="Static" runat="server" Enabled="false" autocomplete="off"
                                            CssClass="textbox" MaxLength="18" Text='<%# Eval("LoanAmount") %>'  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>
<%--                                        <td style="padding:5px;" ><span class="headFont">Install Amount &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtInstallAmt" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18" Text='<%# Eval("InstallAmount") %>'  onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Deduction Start &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtLoanDeducStrt" ClientIDMode="Static" runat="server" Enabled="false" width="190px"
                                                CssClass="dpDate textbox" Text='<%# Eval("LoanDeducStartDate") %>' autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnLDSDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" Enabled="false"/>
                                            </nobr>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Total Loan Install&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtTotLoanInstl" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("TotLoanInstall") %>'  onkeypress="return isNumberKey(event)" Enabled="false" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Current Loan Install&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtCurLoanInst" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("CurLoanInstall") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Loan Amount Paid&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanAmtPaid" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18" Text='<%# Eval("LoanAmountPaid") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Adjustment Date&nbsp;&nbsp;</span><%--<span class="require">*</span>--%></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtAdjDate" ClientIDMode="Static" width="190px" runat="server" CssClass="dpDate textbox" Text='<%# Eval("AdjustDate") %>' Enabled="false" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnAdjDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" Enabled="false" />
                                            </nobr>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Adjustment Amount&nbsp;&nbsp;</span><%--<span class="require">*</span>--%></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtAdjAmt" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18" Text='<%# Eval("AdjustAmount") %>' Enabled="false"  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Sanction Date&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtLoanSancDate" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" width="190px" Text='<%# Eval("LoanSancDate") %>' Enabled="false" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnLSancDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" Enabled="false" />
                                            </nobr>
                                        </td>
                                        <td style="padding:5px;" ><span class="headFont">Reference No.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtRefNo" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" Enabled="false" Text='<%# Eval("ReferenceNo") %>' autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Reference Date&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtRefDate" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" width="190px" Enabled="false" Text='<%# Eval("ReferenceDate") %>' autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnRefDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" Enabled="false" />
                                            </nobr>
                                        </td>
                                    </tr>
                                    <%-----------------------------------------------Supriyo 28-08-17-----------------------------------------------------------%>

                                     <tr>
                                        <%--Text='<% Eval("") %>'--%>
                                        <td style="padding:5px;" ><span class="headFont"> Loan Disbursement Sanction No&nbsp;&nbsp;</span><%--<span class="require">*</span>--%></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtDisbursementNo" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18" Enabled="false" Text='<%# Eval("DisbursementNo") %>' autocomplete="off"></asp:TextBox>
                                        </td>

                                         <td style="padding:5px;" ><span class="headFont"> Disbursement Date.&nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtDisbursementDate" ClientIDMode="Static" Width="190px"  runat="server" CssClass="dpDate textbox" Enabled="false" Text='<%# Eval("DisbursementDate") %>' autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnDisbursementDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                    </tr>
                                    
                                     
                                    <%--------------------------------------------------------------------------------- %>
                                            <%--------------------------Added on 11042015--------------------------------------%>
                                   <tr><td colspan="6" ><hr style="border:solid 1px lightblue" /></td></tr>
                                     <tr>
                                    <td colspan="6" style="padding: 5px; background: #7DCEA0; width:100%;"><span style="font-weight: bold; font-family:Verdana; font-style:italic;"><font size="3px">First Installment Slab</font></span></td>
                                    </tr>
                                    <tr>
                                    <td style="padding:5px;" ><span class="headFont">Installment No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstNo1" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10"  Text='<%# Eval("inst_no1") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                    </td>

                                    <td style="padding:5px;" ><span class="headFont">Installment Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstAmt1" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("inst_amt1") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                    </td>
                                    </tr>


                                      <tr><td colspan="6" ><hr style="border:solid 1px lightblue" /></td></tr>
                                     <tr>
                                    <td colspan="6" style="padding: 5px; background: #7DCEA0; width:100%;"><span style="font-weight: bold; font-family:Verdana; font-style:italic;"><font size="3px">Second Installment Slab</font></span></td>
                                    </tr>
                                    <tr>
                                    <td style="padding:5px;" ><span class="headFont">Installment No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstNo2" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("inst_no2") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                    </td>

                                    <td style="padding:5px;" ><span class="headFont">Installment Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstAmt2" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("inst_amt2") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                    </td>
                                    </tr>

                                    <tr><td colspan="6" ><hr style="border:solid 1px lightblue" /></td></tr>
                                     <tr>
                                    <td colspan="6" style="padding: 5px; background: #7DCEA0; width:100%;"><span style="font-weight: bold; font-family:Verdana; font-style:italic;"><font size="3px">Third Installment Slab</font></span></td>
                                    </tr>
                                    <tr>
                                    <td style="padding:5px;" ><span class="headFont">Installment No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstNo3" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("inst_no3") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                    </td>

                                    <td style="padding:5px;" ><span class="headFont">Installment Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                         <asp:TextBox ID="txtInstAmt3" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("inst_amt3") %>'  onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                    </td>
                                    </tr>
                                    
                                    <%---------------------------------------------------------------------------------%>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                        
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            
                            </asp:FormView>
                    
                         </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

