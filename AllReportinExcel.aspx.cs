﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using NPOI.HSSF.UserModel;


public partial class AllReportinExcel : System.Web.UI.Page
{
    public static string ReportName = "";
    public static string ReportID = "";
    public static string ParameterFlag = "";
    public static string PaymentDate = "";
    public static string SalMonthID = "";
    public static string Schedule = "";
    public static string EmployeeList = "";

    public static string LifeCertificate = "";
    public static string licEmployeeID = "";
    public static string licEmployeeNO = "";
    public static string licPPONO = "";
    public static string licBranchID = "";
    public static string licCertificateYear = "";

    StringBuilder htmlTable = new StringBuilder(); 

    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Request.QueryString["ReportID"]!=null)
        ReportID = HttpContext.Current.Request.QueryString["ReportID"].ToString();

        if (HttpContext.Current.Request.QueryString["ParameterFlag"] != null)
        ParameterFlag = HttpContext.Current.Request.QueryString["ParameterFlag"].ToString();

        if (HttpContext.Current.Request.QueryString["PaymentDate"] != null)
            PaymentDate = HttpContext.Current.Request.QueryString["PaymentDate"].ToString();

        if (HttpContext.Current.Request.QueryString["StrSalMonthID"] != null)
            SalMonthID = HttpContext.Current.Request.QueryString["StrSalMonthID"].ToString();
        
        if (HttpContext.Current.Request.QueryString["StrSchedule"] != null)
            Schedule = HttpContext.Current.Request.QueryString["StrSchedule"].ToString();

        if (HttpContext.Current.Request.QueryString["EmployeeList"] != null)
            EmployeeList = HttpContext.Current.Request.QueryString["EmployeeList"].ToString();
        

        if (HttpContext.Current.Request.QueryString["ReportName"] != null)
        {
            ReportName = HttpContext.Current.Request.QueryString["ReportName"].ToString();
            ExportinExcel(ReportName);
        }
    }

    protected void ExportinExcel(string ReportName)
    {
        if (ReportName == "AdminBankSlipCumList")
            OnAdminBankSlip();
            //OnAdminBankSlipCumList();
        if (ReportName == "DynamicReportExcels")
            OnDynamicReports();
        if (ReportName == "Arrear_bank_slip")
            OnArrearBankSlipReports();
        if (ReportName == "PFPaymentDetailReport")
            OnPFPaymentReports();

        //Done by Siddartha
        if (ReportName == "PaySummarySecWiseHistory")
            OnPaySumamarySecWiseHistoryReports();
        if (ReportName == "History_PaySlip_new_LocationWISE_A5")
            OnPaySlipHistoryReports();
        if (ReportName == "Pay_Schedule_ITAX_History")
            OnPayScheduleHistoryReports();
        if (ReportName == "Pay_Schedule_PTAX_History")
            OnPaySchedulePtaxHistoryReports();
        if (ReportName == "Pay_Schedule_Mice_History")
            OnPayScheduleAllHistoryReports();
        //End Done by Siddartha

        if (ReportName == "LeaveProportionExcel")
            OnLeaveProportionExcel();

        if (ReportName == "BeneficiaryPensionerDetails")
            OnBeneficiaryPensionerReports();

        if (ReportName == "Pension_Bank_Slip")
            OnPensionBankSliprReports();

        //Done by Siddartha
        if (ReportName == "Pay_Schedule_ITAX_Admin")
            OnAdminScheduleItaxReports();
        if (ReportName == "Pay_Schedule_PTAX_Admin")
            OnAdminSchedulePtaxReports();
        if (ReportName == "Pay_Schedule_MICE_Admin")
            OnAdminScheduleReports();
        if (ReportName == "Pay_Schedule_LOAN_Admin")
            OnAdminScheduleLoanReports();
        //End by Siddartha

        //Done by Deepak
        if (ReportName == "LifeCertificateRecord")
            OnSearchPensionerLifeCertificate();
        //End by Deepak

        //Done by Siddartha On Admin Sechudle Report
        if (ReportName == "AdminAllsector_COOP")
            AdminAllsectorCOOPReports();
        if (ReportName == "AdminAllsector_CPF_all_PF_state")
            AdminAllsectorCPFPFeports();
        //End by Siddartha

        //Done by Siddartha On Admin Sechudle Hist Report
        if (ReportName == "AdminAllsector_COOP_hist")
            AdminAllsectorCOOPHistReports();
        if (ReportName == "AdminAllsector_CPF_all_PF_state_hist")
            AdminAllsectorCPFPFHistReports();
        //End by Siddartha On Admin Sechudle Hist Report

        //Done by Siddartha On Pension BankSlip new format Report
        if (ReportName == "AdminBankSlipCumListNewFormat")
            AdminBankSlipCumListNewFormatReports();

        if (ReportName == "Pension_Bank_Slip_new")
            PensionBankSlipNewFormatReports();
        // End Done by Siddartha On Pension BankSlip new format Report
    }

    public void OnAdminBankSlipCumList()
    {
        DataTable dt = DBHandler.GetResult("Get_BankSlipinExcel");
        if (dt.Rows.Count > 0)
        {
            DataColumnCollection dcCollection = dt.Columns;
            // Export Data into EXCEL Sheet
            Microsoft.Office.Interop.Excel._Application ExcelApp = new Excel.Application();
            ExcelApp.Application.Workbooks.Add(Type.Missing);

            Excel.Range range = ExcelApp.Cells[1, 1] as Excel.Range;
            range.EntireRow.Font.Name = "Calibri";
            range.EntireRow.Font.Bold = true;
            range.EntireRow.Font.Size = 12;

            ExcelApp.Cells.Range["A:A"].NumberFormat = "@";
            ExcelApp.Cells.Range["B:B"].NumberFormat = "@";
            ExcelApp.Cells.Range["F:F"].NumberFormat = "@";
            ExcelApp.Cells.Range["G:G"].NumberFormat = "@";
            ExcelApp.Cells.Range["O:O"].NumberFormat = "@";

            // ExcelApp.Cells.CopyFromRecordset(objRS);

            for (int i = 1; i < dt.Rows.Count + 2; i++)
            {
                for (int j = 1; j < dt.Columns.Count + 1; j++)
                {
                    if (i == 1)
                    {
                        ExcelApp.Cells[i, j] = dcCollection[j - 1].ToString();
                    }
                    else
                    {
                        ExcelApp.Cells[i, j] = dt.Rows[i - 2][j - 1].ToString();
                    }
                }
            }

            //string foldername = DateTime.Now.ToString();
            //if (!Directory.Exists(@"E:/Generated_Excel/"))
            //{
            //    Directory.CreateDirectory(@"E:/Generated_Excel/");
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}
            //else
            //{
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}

            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Employee_Detail.xls"));
            //Response.WriteFile("E:\\Generated_Excel\\Employee_Detail.xlsx"); //put filepath here
            
            Response.Flush();
            Response.End();
        }
    }

    public void OnDynamicReports()
    {
        string RepParam = "";
        if (ParameterFlag == "N")
            RepParam = "";
        else
            RepParam = "";

        DataTable dt = DBHandler.GetResult("returnReportQuery", ReportID, ParameterFlag, RepParam, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (dt.Rows.Count > 0)
        {
            DataColumnCollection dcCollection = dt.Columns;
            // Export Data into EXCEL Sheet
            Microsoft.Office.Interop.Excel._Application ExcelApp = new Excel.Application();
            ExcelApp.Application.Workbooks.Add(Type.Missing);

            Excel.Range range = ExcelApp.Cells[1, 1] as Excel.Range;
            range.EntireRow.Font.Name = "Calibri";
            range.EntireRow.Font.Bold = true;
            range.EntireRow.Font.Size = 12;


            //ExcelApp.Cells.Range["B:B"].NumberFormat = "@";
            //ExcelApp.Cells.Range["G:G"].NumberFormat = "@";
            //ExcelApp.Cells.Range["O:O"].NumberFormat = "@";

            // ExcelApp.Cells.CopyFromRecordset(objRS);

            for (int i = 1; i < dt.Rows.Count + 2; i++)
            {
                for (int j = 1; j < dt.Columns.Count + 1; j++)
                {
                    if (i == 1)
                    {
                        ExcelApp.Cells[i, j] = dcCollection[j - 1].ToString();
                    }
                    else
                    {
                        ExcelApp.Cells[i, j] = dt.Rows[i - 2][j - 1].ToString();
                    }
                }
            }
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "DynamicRports.xls"));
            //Response.WriteFile("E:\\Generated_Excel\\Employee_Detail.xlsx"); //put filepath here

            Response.Flush();
            Response.End();
        }
    }



    public void OnPFPaymentReports()
    {
        DataTable dt = DBHandler.GetResult("Get_BankSlipinExcel");
        if (dt.Rows.Count > 0)
        {
            DataColumnCollection dcCollection = dt.Columns;
            // Export Data into EXCEL Sheet
            Microsoft.Office.Interop.Excel._Application ExcelApp = new Excel.Application();
            ExcelApp.Application.Workbooks.Add(Type.Missing);

            Excel.Range range = ExcelApp.Cells[1, 1] as Excel.Range;
            range.EntireRow.Font.Name = "Calibri";
            range.EntireRow.Font.Bold = true;
            range.EntireRow.Font.Size = 12;

            ExcelApp.Cells.Range["A:A"].NumberFormat = "@";
            ExcelApp.Cells.Range["B:B"].NumberFormat = "@";
            ExcelApp.Cells.Range["F:F"].NumberFormat = "@";
            ExcelApp.Cells.Range["G:G"].NumberFormat = "@";
            ExcelApp.Cells.Range["O:O"].NumberFormat = "@";

            // ExcelApp.Cells.CopyFromRecordset(objRS);

            for (int i = 1; i < dt.Rows.Count + 2; i++)
            {
                for (int j = 1; j < dt.Columns.Count + 1; j++)
                {
                    if (i == 1)
                    {
                        ExcelApp.Cells[i, j] = dcCollection[j - 1].ToString();
                    }
                    else
                    {
                        ExcelApp.Cells[i, j] = dt.Rows[i - 2][j - 1].ToString();
                    }
                }
            }

            //string foldername = DateTime.Now.ToString();
            //if (!Directory.Exists(@"E:/Generated_Excel/"))
            //{
            //    Directory.CreateDirectory(@"E:/Generated_Excel/");
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}
            //else
            //{
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}

            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Employee_Detail.xls"));
            //Response.WriteFile("E:\\Generated_Excel\\Employee_Detail.xlsx"); //put filepath here

            Response.Flush();
            Response.End();
        }
    }

    public void OnPaySumamarySecWiseHistoryReports()
    {
        DataTable dt = DBHandler.GetResult("Get_BankSlipinExcel");
        if (dt.Rows.Count > 0)
        {
            DataColumnCollection dcCollection = dt.Columns;
            // Export Data into EXCEL Sheet
            Microsoft.Office.Interop.Excel._Application ExcelApp = new Excel.Application();
            ExcelApp.Application.Workbooks.Add(Type.Missing);

            Excel.Range range = ExcelApp.Cells[1, 1] as Excel.Range;
            range.EntireRow.Font.Name = "Calibri";
            range.EntireRow.Font.Bold = true;
            range.EntireRow.Font.Size = 12;


            ExcelApp.Cells.Range["B:B"].NumberFormat = "@";
            ExcelApp.Cells.Range["G:G"].NumberFormat = "@";
            ExcelApp.Cells.Range["O:O"].NumberFormat = "@";

            // ExcelApp.Cells.CopyFromRecordset(objRS);

            for (int i = 1; i < dt.Rows.Count + 2; i++)
            {
                for (int j = 1; j < dt.Columns.Count + 1; j++)
                {
                    if (i == 1)
                    {
                        ExcelApp.Cells[i, j] = dcCollection[j - 1].ToString();
                    }
                    else
                    {
                        ExcelApp.Cells[i, j] = dt.Rows[i - 2][j - 1].ToString();
                    }
                }
            }

            //string foldername = DateTime.Now.ToString();
            //if (!Directory.Exists(@"E:/Generated_Excel/"))
            //{
            //    Directory.CreateDirectory(@"E:/Generated_Excel/");
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}
            //else
            //{
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}

            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Employee_Detail.xls"));
            //Response.WriteFile("E:\\Generated_Excel\\Employee_Detail.xlsx"); //put filepath here

            Response.Flush();
            Response.End();
        }
    }
    public void OnPaySlipHistoryReports()
    {
        DataTable dt = DBHandler.GetResult("Get_BankSlipinExcel");
        if (dt.Rows.Count > 0)
        {
            DataColumnCollection dcCollection = dt.Columns;
            // Export Data into EXCEL Sheet
            Microsoft.Office.Interop.Excel._Application ExcelApp = new Excel.Application();
            ExcelApp.Application.Workbooks.Add(Type.Missing);

            Excel.Range range = ExcelApp.Cells[1, 1] as Excel.Range;
            range.EntireRow.Font.Name = "Calibri";
            range.EntireRow.Font.Bold = true;
            range.EntireRow.Font.Size = 12;


            ExcelApp.Cells.Range["B:B"].NumberFormat = "@";
            ExcelApp.Cells.Range["G:G"].NumberFormat = "@";
            ExcelApp.Cells.Range["O:O"].NumberFormat = "@";

            // ExcelApp.Cells.CopyFromRecordset(objRS);

            for (int i = 1; i < dt.Rows.Count + 2; i++)
            {
                for (int j = 1; j < dt.Columns.Count + 1; j++)
                {
                    if (i == 1)
                    {
                        ExcelApp.Cells[i, j] = dcCollection[j - 1].ToString();
                    }
                    else
                    {
                        ExcelApp.Cells[i, j] = dt.Rows[i - 2][j - 1].ToString();
                    }
                }
            }

            //string foldername = DateTime.Now.ToString();
            //if (!Directory.Exists(@"E:/Generated_Excel/"))
            //{
            //    Directory.CreateDirectory(@"E:/Generated_Excel/");
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}
            //else
            //{
            //    ExcelApp.ActiveWorkbook.SaveCopyAs("E:\\Generated_Excel\\Employee_Detail.xlsx");
            //    ExcelApp.ActiveWorkbook.Saved = true;
            //    ExcelApp.Quit();
            //}

            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Employee_Detail.xls"));
            //Response.WriteFile("E:\\Generated_Excel\\Employee_Detail.xlsx"); //put filepath here

            Response.Flush();
            Response.End();
        }
    }

    public void OnArrearBankSlipReports()
    {
        DataTable dt = DBHandler.GetResult("Load_ArrearbankSlipExcel", PaymentDate);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "PaymentAmount", "ArrearBankSlip");
        }
    }

    public void OnAdminBankSlip()
    {
        DataTable dt = DBHandler.GetResult("Get_BankSlipinExcel");
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt,"NetPay","");

            //decimal Total = 0;
            //var workbook = new HSSFWorkbook();

            //var sheet = workbook.CreateSheet();

            ////sheet.SetColumnWidth(0, 20 * 256);
            ////sheet.SetColumnWidth(1, 20 * 256);
            ////sheet.SetColumnWidth(2, 20 * 256);
            ////sheet.SetColumnWidth(3, 20 * 256);
            ////sheet.SetColumnWidth(4, 20 * 256);

            ////Create a header row
            //var headerRow = sheet.CreateRow(0);
            //headerRow.CreateCell(0).SetCellValue("EmpNo");
            //headerRow.CreateCell(1).SetCellValue("EmpName");
            //headerRow.CreateCell(2).SetCellValue("Branch");
            //headerRow.CreateCell(3).SetCellValue("MICR");
            //headerRow.CreateCell(4).SetCellValue("IFSC");
            //headerRow.CreateCell(5).SetCellValue("BankAccCode");
            //headerRow.CreateCell(6).SetCellValue("NetPay");
            //headerRow.CreateCell(7).SetCellValue("BankName");

            //////(Optional) freeze the header row so it is not scrolled
            ////sheet.CreateFreezePane(0, 1, 0, 1);

            //int rowNumber = 1;
            //var row = sheet.CreateRow(rowNumber);
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    //Create a new Row
            //    row = sheet.CreateRow(rowNumber++);

            //    //Set the Values for Cells
            //    row.CreateCell(0).SetCellValue(dt.Rows[i]["EmpNo"].ToString());
            //    row.CreateCell(1).SetCellValue(dt.Rows[i]["EmpName"].ToString());
            //    row.CreateCell(2).SetCellValue(dt.Rows[i]["Branch"].ToString());
            //    row.CreateCell(3).SetCellValue(dt.Rows[i]["MICR"].ToString());
            //    row.CreateCell(4).SetCellValue(dt.Rows[i]["IFSC"].ToString());
            //    row.CreateCell(5).SetCellValue(dt.Rows[i]["BankAccCode"].ToString());
            //    row.CreateCell(6).SetCellValue(dt.Rows[i]["NetPay"].ToString());
            //    row.CreateCell(7).SetCellValue(dt.Rows[i]["BankName"].ToString());

            //    string value = dt.Rows[i]["NetPay"].ToString();
            //    Total = Total + Convert.ToDecimal(value);
            //}


            //// Create the style object
            ////var detailSubtotalCellStyle = workbook.CreateCellStyle();
            ////// Define a thin border for the top and bottom of the cell
            ////detailSubtotalCellStyle.BorderTop = CellBorderType.THIN;
            ////detailSubtotalCellStyle.BorderBottom = CellBorderType.THIN;
            ////// Create a font object and make it bold
            ////var detailSubtotalFont = workbook.CreateFont();
            ////detailSubtotalFont.Boldweight = (short)FontBoldWeight.BOLD;
            ////detailSubtotalCellStyle.SetFont(detailSubtotalFont);

            //// Add a row for the detail row
            //row = sheet.CreateRow(dt.Rows.Count);
            //// Create the first cell – "Total" – and apply the style
            //var cell = row.CreateCell(5);
            //cell.SetCellValue("Total:");

            //string FinalAmt = Total.ToString();
            //row.CreateCell(6).SetCellValue(FinalAmt);

            ////cell.CellStyle = detailSubtotalCellStyle;
            //// Create the second cell for the Order column, which is empty in this row
            ////var cellTotal = row.CreateCell(6);
            ////cellTotal.SetCellValue(Total);
            ////cell.CellStyle = detailSubtotalCellStyle;

            //using (var exportData = new MemoryStream())
            //{
            //    workbook.Write(exportData);
            //    string saveAsFileName = string.Format("EmployeeDetails-{0:d}.xls", DateTime.Now).Replace("/", "-");
            //    Response.ContentType = "application/vnd.ms-excel";
            //    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
            //    Response.Clear();
            //    Response.BinaryWrite(exportData.GetBuffer());
            //    Response.End();
            //}
        }
    }

    public void OnLeaveProportionExcel()
    {
        DataTable dt = DBHandler.GetResult("Load_LeEncashApprovalList", EmployeeList);
        if (dt.Rows.Count > 0)
        {
            string Columns = "AMOUNT";
            GenerateExcels(dt, Columns, "LeEncashApprovalList");
        }
    }

    public void OnBeneficiaryPensionerReports()
    {


        DataTable dt = DBHandler.GetResult("Load_BeneficiaryPensionerExcel");
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "", "BeneficiaryPensioner");
        }
    }

    public void OnPensionBankSliprReports()
    {
        DataTable dt = DBHandler.GetResult("Load_PenBankSlipExcel");
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "NetPay", "Pension_Bank_Slip");
        }
    }

    public void OnAdminScheduleItaxReports()
    {
        int StrSalMonthID = Int32.Parse(HttpContext.Current.Request.QueryString["SalMonthID"].ToString());
        //int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_AdminScheduleItaxExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "PayScheduleITAXAdmin");
        }
    }
    public void OnAdminSchedulePtaxReports()
    {
        int StrSalMonthID = Int32.Parse(HttpContext.Current.Request.QueryString["SalMonthID"].ToString());
        //int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_AdminSchedulePtaxExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "PaySchedulePTAXAdmin");
        }
    }
    public void OnAdminScheduleReports()
    {
        int StrSalMonthID = Int32.Parse(HttpContext.Current.Request.QueryString["SalMonthID"].ToString());
        //int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_AdminSchedulereportExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "PayScheduleAdmin");
        }
    }
    public void OnAdminScheduleLoanReports()
    {
        int StrSalMonthID = Int32.Parse(HttpContext.Current.Request.QueryString["SalMonthID"].ToString());
        //int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_AdminScheduleLoanExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "PayScheduleLoanAdmin");
        }
    }


    //History Excell  Generation
    public void OnPayScheduleHistoryReports()
    {

        int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_ItaxScheduleHistExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "ItaxScheduleHistExcel");
        }
    }
    public void OnPaySchedulePtaxHistoryReports()
    {

        int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_PtaxScheduleHistExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "PtaxScheduleHistExcel");
        }
    }
    public void OnPayScheduleAllHistoryReports()
    {

        int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_AllScheduleHistExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "AllScheduleHistExcel");
        }
    }
    public void OnPayScheduleLoanHistoryReports()
    {

        int StrSalMonthID = Int32.Parse(SalMonthID);
        int StrSchedule = Int32.Parse(Schedule);
        DataTable dt = DBHandler.GetResult("Load_AllScheduleLoadHistExcel", StrSalMonthID, StrSchedule);
        if (dt.Rows.Count > 0)
        {
            GenerateExcel(dt, "EDAmount", "AllScheduleLoanHistExcel");
        }
    }

    public void OnSearchPensionerLifeCertificate()
    {
        licEmployeeID = HttpContext.Current.Request.QueryString["EmployID"].ToString();
        licBranchID = HttpContext.Current.Request.QueryString["BankBranchID"].ToString();
        licPPONO = HttpContext.Current.Request.QueryString["PPOno"].ToString();
        licEmployeeNO = HttpContext.Current.Request.QueryString["EmployNO"].ToString();
        licCertificateYear = HttpContext.Current.Request.QueryString["LifeYear"].ToString();
        string licInsertBY = Convert.ToString(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);

        DataTable dt = DBHandler.GetResult("GET_PensionerLifeCertificate_New",
                                              licEmployeeID.Equals("") ? DBNull.Value : (object)licEmployeeID,
                                              licEmployeeNO.Equals("") ? DBNull.Value : (object)licEmployeeNO,
                                              licPPONO.Equals("") ? DBNull.Value : (object)licPPONO,
                                             licBranchID.Equals("") ? DBNull.Value : (object)licBranchID,
                                             Convert.ToInt32(licInsertBY),
                                             Convert.ToInt32(licCertificateYear));


        if (dt.Rows.Count > 0)
        {
            dt.Columns.RemoveAt(0);
            dt.Columns.Add("SlNo", typeof(System.Int32));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["SlNo"] = i + 1;
            }
            dt.Columns["SlNo"].SetOrdinal(0);

            string Columns = "";
            GenerateExcels(dt, Columns, "PensionerLifeCertificate");
        }
    }

    public void AdminAllsectorCOOPReports()
    {
        int SalMonthID = Int32.Parse(HttpContext.Current.Request.QueryString["SalMonthID"].ToString());
        string  StrEmpType = HttpContext.Current.Request.QueryString["StrEmpType"].ToString();
        string  SalFinYear = HttpContext.Current.Request.QueryString["SalFinYear"].ToString();


        int Dedu_EDID = 22;
        int Loan_EDID = 315;

        DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF", SalFinYear, SalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCOOP", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (dt.Rows.Count > 0)
        {
            string Columns = "DedAmt,LoanAmt, Total_Realisation";
            GenerateExcels(dt, Columns, "AdminAllsectorCOOP");
        }
    }
    public void AdminAllsectorCPFPFeports()
    {
        int SalMonthID = Int32.Parse(HttpContext.Current.Request.QueryString["SalMonthID"].ToString());
        string SalFinYear = HttpContext.Current.Request.QueryString["SalFinYear"].ToString();
        string StrEmpType = HttpContext.Current.Request.QueryString["StrEmpType"].ToString();

        int StrSchedule = Int32.Parse(Schedule);
        int Dedu_EDID = 14;
        int Loan_EDID = 311;


        if (StrSchedule == 2)
        {
            Dedu_EDID = 33;
            Loan_EDID = 311;
        }

        DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF", SalFinYear, SalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCPFPF", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (dt.Rows.Count > 0)
        {
            string Columns = "DedAmt,LoanAmt, Total_Realisation";
            GenerateExcels(dt, Columns, "AdminAllsectorCPFPF");
        }
    }

    public void AdminAllsectorCOOPHistReports()
    {
        string StrEmpType = HttpContext.Current.Request.QueryString["StrEmpType"].ToString();
        string SalFinYear = HttpContext.Current.Request.QueryString["SalFinYear"].ToString();

        int Dedu_EDID = 22;
        int Loan_EDID = 315;

        DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF_hist", SalFinYear, SalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCOOP_Hist", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (dt.Rows.Count > 0)
        {
            string Columns = "DedAmt,LoanAmt, Total_Realisation";
            GenerateExcels(dt, Columns, "AdminAllsector_COOP_hist");
        }
    }
    public void AdminAllsectorCPFPFHistReports()
    {
        string StrEmpType = HttpContext.Current.Request.QueryString["StrEmpType"].ToString();
        string SalFinYear = HttpContext.Current.Request.QueryString["SalFinYear"].ToString();

        int StrSchedule = Int32.Parse(Schedule);
        int Dedu_EDID = 14;
        int Loan_EDID = 311;

        //if (StrSchedule == 1)
        //{
        //    Dedu_EDID = 14;
        //    Loan_EDID = 311;
        //}
        if (StrSchedule == 2)
        {
            Dedu_EDID = 33;
            Loan_EDID = 311;
        }

        DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF_hist", SalFinYear, SalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCPFPF_Hist", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (dt.Rows.Count > 0)
        {
            string Columns = "DedAmt,LoanAmt, Total_Realisation";
            GenerateExcels(dt, Columns, "AdminAllsector_CPF_PF_hist");
        }
    }

    public void AdminBankSlipCumListNewFormatReports()
    {
        DataTable dt = DBHandler.GetResult("Get_BankSlipinExcel_NewFormat", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (dt.Rows.Count > 0)
        {
            string Columns = "AMOUNT";
            GenerateExcels(dt, Columns, "BankSlipinExcel");
        }
    }
    public void PensionBankSlipNewFormatReports()
    {
        //if (HttpContext.Current.Request.QueryString["PenMonth"] != null)
         string  PenMonth = HttpContext.Current.Request.QueryString["PenMonth"].ToString();

        DataTable dt = DBHandler.GetResult("Load_PenBankSlipExcelNwFormat", PenMonth);
        if (dt.Rows.Count > 0)
        {
            string Columns = "AMOUNT";
            GenerateExcels(dt, Columns, "BankSlipinExcelNew");
        }
    }

    public void GenerateExcel(DataTable dt, string SumFieldName, string ReportName)
    {

        decimal Total = 0; string Position = "";
        if (dt.Rows.Count > 0)
        {
            var workbook = new HSSFWorkbook();
            var sheet = workbook.CreateSheet();

            //Create a header row
            var headerRow = sheet.CreateRow(0);

            string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
            for (int j = 0; j < columnNames.Length; j++)
            {
                headerRow.CreateCell(j).SetCellValue(columnNames[j]);
            }
            int rowNumber = 1;
            var row = sheet.CreateRow(rowNumber);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //Create a new Row
                row = sheet.CreateRow(rowNumber++);

                string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                for (int j = 0; j < columnNamess.Length; j++)
                {
                    row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                }

                if (SumFieldName != "")
                {
                    DataColumnCollection columns = dt.Columns;
                    if (columns.Contains(SumFieldName))
                    {
                        string value = dt.Rows[i][SumFieldName].ToString();
                        Total = Total + Convert.ToDecimal(value);

                        Position = dt.Columns.IndexOf(SumFieldName).ToString();
                    }
                    else
                    {
                        Total = Total + 0;
                    }
                }
            }

            if (SumFieldName != "")
            {
                // Add a row for the detail row
                row = sheet.CreateRow(dt.Rows.Count+1);
                var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                cell.SetCellValue("Total:");

                string FinalAmt = Total.ToString();
                row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
            }
            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                string saveAsFileName = string.Format(ReportName + "-{0:d}.xls", DateTime.Now).Replace("/", "-");
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                Response.Clear();
                Response.BinaryWrite(exportData.GetBuffer());
                Response.End();
            }
        }
    }

    public void GenerateExcels(DataTable dt, string SumFieldName, string ReportName)
    {
        DataTable dtTotalPosition = new DataTable();

        decimal Total = 0; string Position = "";
        if (dt.Rows.Count > 0)
        {
            var workbook = new HSSFWorkbook();
            var sheet = workbook.CreateSheet();

            //Create a header row
            var headerRow = sheet.CreateRow(0);

            string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
            for (int j = 0; j < columnNames.Length; j++)
            {
                headerRow.CreateCell(j).SetCellValue(columnNames[j]);
            }
            int rowNumber = 1;
            var row = sheet.CreateRow(rowNumber);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //Create a new Row
                row = sheet.CreateRow(rowNumber++);

                string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                for (int j = 0; j < columnNamess.Length; j++)
                {
                    row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                }
            }

            if (SumFieldName != "")
            {
                string col = "";
                string[] lines = SumFieldName.Split(',');
                for (int i = 0; i < lines.Length; i++)
                {
                    col = lines[i].Trim().ToString();
                    DataColumnCollection columns = dt.Columns;
                    if (columns.Contains(col))
                    {
                        Total = 0;
                        for (int k = 0; k < dt.Rows.Count; k++)
                        {
                            string value = dt.Rows[k][col].ToString();
                            Total = Total + Convert.ToDecimal(value);
                            Position = dt.Columns.IndexOf(col).ToString();
                        }
                        //Add Total amount and Column Position in a DataTable
                        if (dtTotalPosition != null)
                        {
                            if (dtTotalPosition.Rows.Count > 0)
                            {
                                DataRow dtNewRow = dtTotalPosition.NewRow();
                                dtNewRow["Position"] = Position;
                                dtNewRow["Total"] = Total;
                                dtTotalPosition.Rows.Add(dtNewRow);
                            }
                            else
                            {
                                dtTotalPosition.Columns.Add("Position");
                                dtTotalPosition.Columns.Add("Total");

                                DataRow dtNewRow = dtTotalPosition.NewRow();
                                dtNewRow["Position"] = Position;
                                dtNewRow["Total"] = Total;
                                dtTotalPosition.Rows.Add(dtNewRow);
                            }
                        }
                    }
                    else
                    {
                        if (dtTotalPosition != null)
                        {
                            if (dtTotalPosition.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                dtTotalPosition.Columns.Add("Position");
                                dtTotalPosition.Columns.Add("Total");
                            }
                        }
                    }
                }
            }

            if (SumFieldName != "")
            {
                // Add a row for the detail row
                row = sheet.CreateRow(dt.Rows.Count + 1);
                //var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                //cell.SetCellValue("Total:");

                if (dtTotalPosition.Rows.Count > 0)
                {
                    for (int j = 0; j < dtTotalPosition.Rows.Count; j++)
                    {
                        string FinalPosition = dtTotalPosition.Rows[j]["Position"].ToString();
                        string FinalAmt = dtTotalPosition.Rows[j]["Total"].ToString();

                        row.CreateCell(Convert.ToInt32(FinalPosition)).SetCellValue(FinalAmt);
                    }
                }
                else
                {

                }
            }


            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                string saveAsFileName = string.Format(ReportName + "-{0:d}.xls", DateTime.Now).Replace("/", "-");
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                Response.Clear();
                Response.BinaryWrite(exportData.GetBuffer());
                Response.End();
            }
        }
    }
    
}