﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MediClaim.aspx.cs" Inherits="MediClaim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">
    <script src="js/MediClaim.js?v1"></script>
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" runat="Server">
    <style type="text/css">
        h1, h2, h3, h4, h5, h6 {
            text-transform: none;
        }

        #AutoComplete-EmployeeName,#AutoComplete-EmployeeName_Search {
            display: block;
            position: relative;
        }

        .ui-autocomplete {
            height: 200px;
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #cbc7c7;
            font-size: 13px;
            font-weight: normal;
            color: #242424;
            background: #fff;
            -moz-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            -webkit-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            box-shadow: inset 0.9px 1px 3px #e4e4e4;
            width: 350px;
            /*height:30px;*/
            padding: 5px;
            font-family: Segoe UI, Lucida Grande, Arial, Helvetica, sans-serif;
            margin: 3px 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" runat="Server">
    <div class="main">
        <div class="container" style="padding-top: 30px; padding-bottom: 140px">
            <div class="row main-row">
                <div class="12u" style="padding: 30px">
                    <div style="padding-bottom:10px">
                        <asp:Panel ID="Panel1" runat="server" Width="100%" BorderStyle="Solid" style="background-color:#cfe4e4">
                            <table >
                                <tr>
                                    <td style="padding: 5px; width: 200px"><span class="headFont">Employee Name&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding: 5px;">:</td>
                                    <td style="padding: 5px;width:300px" align="left">
                                        <asp:TextBox ID="txtEmployeeNameSearch" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" Width="290px"></asp:TextBox>
                                        <input type="hidden" id="hdnIEmployeeId_Search" />
                                        <input type="hidden" id="hdnLoanId" />
                                        <div id="AutoComplete-EmployeeName_Search"></div>
                                    </td>
                                
                                <td style="text-align: left">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" Width="120px" Height="30px" CssClass="Btnclassname" />
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" Width="120px" Height="30px" CssClass="Btnclassname" />
                                </td>
                            </tr>
                                <tr>
                                    <td colspan="4">
                                        <table id="tblDetailsMediclaim" border="1px" class="Grid" style="width: 100%; border-collapse: collapse;" align="center">
                                            <thead>
                                                <tr>
                                                    
                                                    <th style="padding:7px 9px;background: #efeff7">MediclaimNo</th>
                                                    <th style="padding:7px 9px;background: #efeff7"">Employee Name</th>
                                                    <th style="padding:7px 9px;background: #efeff7"">LoanAmount</th>
                                                    <th style="padding:7px 9px;background: #efeff7"">LoanDeducStartDate</th>
                                                    <th style="padding:7px 9px;background: #efeff7"">TotLoanInstall</th>
                                                    <th style="padding:7px 9px;background: #efeff7"">CurLoanInstall</th>
                                                    <th style="padding:7px 9px;background: #efeff7"">LoanAmountPaid</th>
                                                    <th  style="padding:7px 9px;background: #efeff7">FinYear</th>
                                                    <th  style="padding:7px 9px;background: #efeff7"">1st Installment No</th>
                                                    <th style="padding:7px 9px;background: #efeff7"">1st Installment Amount</th>
                                                    <th  style="padding:7px 9px;background: #efeff7"">2nd Installment No</th>
                                                    <th  style="padding:7px 9px;background: #efeff7"">2nd Installment Amount</th>
                                                    <th  style="padding:7px 9px;background: #efeff7"">Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </asp:Panel>
                    </div>
                    <table style="border: solid 2px lightblue; width: 100%">
                        <thead>
                            <tr>
                                <th colspan="6" style="background-color: #3e5a82; text-align: left">
                                    <h2 style="color: #edf702; font: larger; padding-left: 20px; padding-top: 10px">Mediclaim</h2>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Employee Name&nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left" colspan="4">
                                    <asp:TextBox ID="txtEmployeeName" ClientIDMode="Static" runat="server" CssClass="textbox" Width="290px"></asp:TextBox>
                                    <input type="hidden" id="hdnIEmployeeId" />
                                    <div id="AutoComplete-EmployeeName"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Mediclaim No &nbsp;</span></td>
                                <td style="padding: 5px; width: 20px">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtMediClaimNo" ClientIDMode="Static" runat="server" CssClass="textbox" Width="190px"></asp:TextBox>
                                </td>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Mediclaim Start Date&nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <nobr>
                                    <asp:TextBox ID="txtMediClaimStartDate" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                    <asp:ImageButton ID="btnLDSDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                </nobr>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Mediclaim Amount &nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px; width: 20px">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtMediClaimAmount" ClientIDMode="Static" runat="server" placeholder="0" CssClass="textbox" Width="190px" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Total Installment No &nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px; width: 20px">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtInstallMentNO" ClientIDMode="Static" runat="server" CssClass="textbox" placeholder="0" Width="190px" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Current Mediclaim Installment No&nbsp;</span></td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtCurrentMediclaimInstallmentNo" ClientIDMode="Static" runat="server" CssClass="textbox" placeholder="0" Width="290px" disabled></asp:TextBox>
                                </td>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Mediclaim Amount Paid&nbsp;</span></td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtMediclaimAmountPaid" ClientIDMode="Static" runat="server" CssClass="textbox" placeholder="0" Width="290px" disabled></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <h2 style="background-color: #a1acbb; color: #1e1365; padding: 5px">1st Installment Slab</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Installment No&nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtSlabInsatallNoIst" ClientIDMode="Static" runat="server" CssClass="textbox" placeholder="0" Width="290px" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Installment Amount&nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtSlabInsatallAmountIst" ClientIDMode="Static" runat="server" CssClass="textbox" placeholder="0" Width="290px" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <h2 style="background-color: #a1acbb; color: #1e1365; padding: 5px">2nd Installment Slab</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Installment No&nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtSlabInsatallNoScnd" ClientIDMode="Static" runat="server" CssClass="textbox" placeholder="0" Width="290px" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 5px; width: 200px"><span class="headFont">Installment Amount&nbsp;</span><span class="require">*</span> </td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtSlabInsatallAmountScnd" ClientIDMode="Static" runat="server" CssClass="textbox" placeholder="0" Width="290px" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                             
                            <tr>
                                <td colspan="6">
                                    <hr style="border: solid 1px lightblue" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" style="text-align: center">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" Width="120px" Height="30px" CssClass="Btnclassname" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="120px" Height="30px" CssClass="Btnclassname" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

