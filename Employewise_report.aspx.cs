﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class Employewise_report : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GetEmpCodeWithName(string prefix, string Sectorid)
    {
        List<string> CodewithName = new List<string>();
        //customers.Add(string.Format("{0}-{1}", sdr["ContactName"], sdr["CustomerId"]));

        try
        {
            DataTable dtCodeName = DBHandler.GetResult("Get_SearchCodewithNameID", prefix, Sectorid == "" ? 0 : Convert.ToInt32(Sectorid));
            if (dtCodeName.Rows.Count > 0)
            {
                foreach (DataRow row in dtCodeName.Rows)
                {
                    CodewithName.Add(string.Format("{0}|{1}", row["EmpName"], row["EmpNo"]));
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return CodewithName.ToArray();
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(int SalmonthID, string SalaryFinYear, string SalMonth, int SectorID, int CenterID, string EmpType, string Status, int EDID, int ChildreportID, string reportHisCur, int isPDFExcel)
    {
        String Msg = "";
        int UserID;
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        // DataSet dtcr = DBHandler.GetResults("Get_PaySchedule", SalaryFinYear, SalMonth, SectorID, CenterID, EmpType, Status, EDID, SectorGroup, CenterGroup, EmpTypeGroup, StatusGroup, SchType, UserID);
        DataSet dtcr = DBHandler.GetResults("Load_reportDynamic", SalmonthID, SalMonth, SalaryFinYear, SectorID, UserID, ChildreportID, CenterID, EmpType, Status, EDID, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, isPDFExcel, reportHisCur);
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);

    }
}