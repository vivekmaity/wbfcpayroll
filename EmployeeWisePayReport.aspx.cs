﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;            
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;             
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Drawing;


public partial class EmployeeWisePayReport : System.Web.UI.Page
{
    public string str1 = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
               // PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    //protected void cmdCancel_Click(object sender, EventArgs e)
    //{
    //    Response.ContentType = "application/pdf";
    //    Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf");
    //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    StringWriter sw = new StringWriter();
    //    HtmlTextWriter hw = new HtmlTextWriter(sw);
    //    this.Page.RenderControl(hw);
    //    StringReader sr = new StringReader(sw.ToString());
    //    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
    //    XMLWorkerHelper xmlparser=new XMLWorkerHelper(pdfDoc)
    //    //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
    //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
    //    pdfDoc.Open();
    //    htmlparser.Parse(sr);
    //    pdfDoc.Close();
    //    Response.Write(pdfDoc);
    //    Response.End();
    //}


    protected void cmdSearch_Click(object sender, EventArgs e) {
    }
    
    [WebMethod]
    public static string EmployeeDetails(int SecID, string EmpNo)
    {
         string msg = "";
        //string EmployeeNo = txtEmpCodeSearch.;
        DataSet ds1 = DBHandler.GetResults("Get_EmpDetails", SecID, EmpNo);
        DataTable dt = ds1.Tables[0];
        DataTable dtEmpName = ds1.Tables[0];
        //DataTable dtSalMonthID = ds1.Tables[0];

        List<EmpDetails> Empdetails = new List<EmpDetails>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                EmpDetails objst = new EmpDetails();

                objst.EmpName = Convert.ToString(dtEmpName.Rows[i]["EmpName"]);
                objst.DOB = Convert.ToString(dtEmpName.Rows[i]["DOB"]);
                objst.DOR = Convert.ToString(dtEmpName.Rows[i]["DOR"]);
                objst.SectorName = Convert.ToString(dtEmpName.Rows[i]["SectorName"]);
                objst.Designation = Convert.ToString(dtEmpName.Rows[i]["Designation"]);
                objst.PFCode = Convert.ToString(dtEmpName.Rows[i]["PFCode"]);
                objst.CenterName = Convert.ToString(dtEmpName.Rows[i]["CenterName"]);
                objst.PayScale = Convert.ToString(dtEmpName.Rows[i]["PayScale"]);
                objst.printdate = Convert.ToString(dtEmpName.Rows[i]["printdate"]);
                objst.PAN = Convert.ToString(dtEmpName.Rows[i]["PAN"]);
                Empdetails.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Empdetails);

    }
    public class EmpDetails
    {
        public string EmpName { get; set; }
        public string DOB { get; set; }
        public string DOR { get; set; }
        public string SectorName { get; set; }
        public string Designation { get; set; }
        public string PFCode { get; set; }
        public string CenterName { get; set; }
        public string PayScale { get; set; }
        public string printdate { get; set; }
        public string PAN { get; set; }
        
        //public int PayMonthsID { get; set; }
    }
    [WebMethod]
    public static string PopulateEmpGrid(int SecID, string EmpNo)
    {
        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        string fYear = HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString();
        
       DataTable DTCheckReportVal1 = DBHandler.GetResult("Rpt_EmpWiseSalProjection", fYear, SecID, EmpNo, UserID);
       DataTable dtEmpGrid = DBHandler.GetResult("Get_ITaxEmployeeDetailsNew", fYear, SecID, EmpNo);
        
        //DataTable dtSalMonthID = ds1.Tables[0];

        List<GridDetail> EmpGrddetails = new List<GridDetail>();
        if (dtEmpGrid.Rows.Count > 0)
        {
            for (int i = 0; i < dtEmpGrid.Rows.Count; i++)
            {
                GridDetail objst = new GridDetail();
                objst.EmployeeID = Convert.ToString(dtEmpGrid.Rows[i]["EmployeeID"]);
                objst.SalMonth = Convert.ToString(dtEmpGrid.Rows[i]["SalMonth"]);
                objst.GrossPay = Convert.ToString(dtEmpGrid.Rows[i]["GrossPay"]);
                objst.Ptax = Convert.ToString(dtEmpGrid.Rows[i]["Ptax"]);
                objst.Itax = Convert.ToString(dtEmpGrid.Rows[i]["Itax"]);
                objst.GPF = Convert.ToString(dtEmpGrid.Rows[i]["GPF"]);
                objst.GPFRecov = Convert.ToString(dtEmpGrid.Rows[i]["GPFRecov"]);
                objst.NetPay = Convert.ToString(dtEmpGrid.Rows[i]["NetPay"]);
                objst.IHBL = Convert.ToString(dtEmpGrid.Rows[i]["IHBL"]);
                objst.HBL = Convert.ToString(dtEmpGrid.Rows[i]["HBL"]);
                objst.Status = Convert.ToString(dtEmpGrid.Rows[i]["sStatus"]);
                objst.ArrearOT = Convert.ToString(dtEmpGrid.Rows[i]["ArrearOT"]);
                objst.HRA = Convert.ToString(dtEmpGrid.Rows[i]["HRA"]);
                objst.OtherDedu = Convert.ToString(dtEmpGrid.Rows[i]["OtherDedu"]);
                objst.GIS = Convert.ToString(dtEmpGrid.Rows[i]["GIS"]);
                EmpGrddetails.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(EmpGrddetails);
    }

    [WebMethod]
    public static string PopulateTotalGrid(int SecID, string EmpNo)
    {
        string fYear = HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString();
        DataTable dtTotalGrid = DBHandler.GetResult("Get_ITaxEmployeeTotal", fYear, SecID, EmpNo);
        //DataTable dtSalMonthID = ds1.Tables[0];

        List<GridTotal> EmpGrdTotal = new List<GridTotal>();
        if (dtTotalGrid.Rows.Count > 0)
        {
            for (int i = 0; i < dtTotalGrid.Rows.Count; i++)
            {
                GridTotal objst = new GridTotal();
                objst.TotalGross = Convert.ToString(dtTotalGrid.Rows[i]["TotalGross"]);
                objst.TotalPtax = Convert.ToString(dtTotalGrid.Rows[i]["TotalPtax"]);
                objst.TotalItax = Convert.ToString(dtTotalGrid.Rows[i]["TotalItax"]);
                objst.TotalGPF = Convert.ToString(dtTotalGrid.Rows[i]["TotalGPF"]);
                objst.TotalNetPay = Convert.ToString(dtTotalGrid.Rows[i]["TotalNetPay"]);
                objst.TotalIHBL = Convert.ToString(dtTotalGrid.Rows[i]["TotalIHBL"]);
                objst.TotalHBL = Convert.ToString(dtTotalGrid.Rows[i]["TotalHBL"]);
                objst.TotalGPFRecover = Convert.ToString(dtTotalGrid.Rows[i]["TotalGPFRecover"]);
                objst.TotalArrearOT = Convert.ToString(dtTotalGrid.Rows[i]["TotalArrearOT"]);
                
               
                EmpGrdTotal.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(EmpGrdTotal);
    }

    public class GridDetail
    {
        public string EmployeeID { get; set; }
        public string SalMonth { get; set; }
        public string GrossPay { get; set; }
        public string ArrearOT { get; set; }
        public string Ptax { get; set; }
        public string Itax { get; set; }
        public string GPF { get; set; }
        public string GPFRecov { get; set; }
        public string NetPay { get; set; }
        public string IHBL { get; set; }
        public string HBL { get; set; }
        public string Status { get; set; }
        public string HRA { get; set; }
        public string OtherDedu { get; set; }
        public string GIS { get; set; }

        //public int PayMonthsID { get; set; }
    }

    public class GridTotal
    {
        public string TotalGross { get; set; }
        public string TotalPtax { get; set; }
        public string TotalItax { get; set; }
        public string TotalGPF { get; set; }
        public string TotalNetPay { get; set; }
        public string TotalIHBL { get; set; }
        public string TotalHBL { get; set; }
        public string TotalGPFRecover { get; set; }
        public string TotalArrearOT { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(string EmpNo, int SecID,int StrReportTyp,string FormName, string ReportName, string ReportType)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        String Msg = "";
        string StrFormula = "";
        int UserID;
        //int SecID;
        string StrPaperSize = "";
        string Finyr = Convert.ToString(HttpContext.Current.Session[SiteConstants.SSN_SALACC]);
        StrPaperSize = "Page - A4";
       UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        //SecID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]);
        //DataTable DTCheckReportVal1 = DBHandler.GetResult("Rpt_EmpWiseSalProjection", Finyr, SecID, EmpNo, UserID);
        //StrFormula = "{tmp_Pensionslip_monthly.inserted_By}=" + UserID + "";
        if (StrReportTyp == 1)
        {
            StrFormula = "{MST_Employee.EmpNo}='" + EmpNo + "' and {MST_Sector.SectorID}=" + SecID + " and {temp_itax_emp_wise.FinYear}='" + Finyr + "' and {temp_itax_emp_wise.InsertedBy}=" + UserID + "";
        }
        
        if (StrReportTyp == 2)
        {
            DataTable DTCheckReportVal1 = DBHandler.GetResult("Rpt_EmpWiseSalOrginal", Finyr, SecID, EmpNo, UserID);
            StrFormula = "{MST_Employee.EmpNo}='" + EmpNo + "' and {MST_Sector.SectorID}=" + SecID + " and {temp_itax_emp_wise.FinYear}='" + Finyr + "' and {temp_itax_emp_wise.Status} in['C','H','B'] and {temp_itax_emp_wise.InsertedBy}=" + UserID + "";

        }
        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

        //DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Update_RptParameterValue", UserID, FormName, ReportName, ReportType, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9, Parameter10);

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue_Payslip(string EmployeeID, string SalMonth, string Status,string SecID, string FormName, string ReportName, string ReportType)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        String Msg = "";
        string StrFormula = "";
        int UserID;
        int IntSecID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        IntSecID = Convert.ToInt32(SecID);
        int EmpID = Convert.ToInt32(EmployeeID);

        DataTable DTCheckReportVal2 = DBHandler.GetResult("SalaryProcessPresentHistoryAutomatic_PaySlip", Status, SalMonth, EmployeeID,SecID,UserID);
        if (ReportName == "History_PaySlip_new_LocationWISE_A5")
        {
            StrFormula = "{tmp_mst_employee.EmployeeID}=" + EmployeeID + " and {MST_SalaryMonth.SalMonth}='" + SalMonth + "' and {tmp_payslipHistory_monthly.inserted_By}=" + UserID + " and {tmp_mst_employee.tabInsertedBy}=" + UserID + " ";

        }

        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

        //DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Update_RptParameterValue", UserID, FormName, ReportName, ReportType, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9, Parameter10);

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Count_Excel(int SecID, string EmpNo,string FinYR, string ReportName)
    {

        int UserID;
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        string jsval = ""; string rowscount = "";
        if (ReportName == "Salary_Projection_Report")
        {
            DataTable dt = DBHandler.GetResult("Load_EmpWisePayReportExcel", EmpNo, FinYR, SecID, UserID);
            rowscount = dt.Rows.Count.ToJSON();
        }

        if (ReportName == "EMP_Orginal_Sal_Statement")
        {
            DataTable dt = DBHandler.GetResult("Load_EmpWiseOrginalPayReportExcel", EmpNo, FinYR, SecID, UserID);
            rowscount = dt.Rows.Count.ToJSON();
        }
        //if (ReportName == "Pay_Schedule_PTAX_Admin")
        //{
        //    DataTable dt = DBHandler.GetResult("Load_AdminSchedulePtaxExcel", SalMonthID, StrSchedule);
        //    rowscount = dt.Rows.Count.ToJSON();
        //}
        //if (ReportName == "Pay_Schedule_MICE_Admin")
        //{
        //    DataTable dt = DBHandler.GetResult("Load_AdminSchedulereportExcel", SalMonthID, StrSchedule);
        //    rowscount = dt.Rows.Count.ToJSON();
        //}
        //if (ReportName == "Pay_Schedule_LOAN_Admin")
        //{
        //    DataTable dt = DBHandler.GetResult("Load_AdminScheduleLoanExcel", SalMonthID, StrSchedule);
        //    rowscount = dt.Rows.Count.ToJSON();
        //}

        jsval = rowscount.ToJSON();
        return jsval;
    }
}