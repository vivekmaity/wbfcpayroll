﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <style>
        .lblhideshow
        {
            display:none;
        }
    </style>
<script type="text/javascript">
    $(document).ready(function () {
        $("#imgRight").hide();
        $("#imgWrong").hide();
        $("#txtnew").attr("disabled", true);
        $("#txtconfirm").attr("disabled", true);
        $("#Add").attr("disabled", true);

        $("#lblright").hide();
        $("#lblwrong").hide();
    });

    $(document).ready(function () {
    $("#txtold").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: pageUrl + '/CheckUserPassword',
                data: "{prefix:'" + request.term + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var item = data.d; //alert(item);
                    var oldpwd = $("#txtold").val(); //alert(oldpwd);
                    if (item == oldpwd)
                    {
                        $("#imgRight").show();
                        $("#imgWrong").hide();
                        $("#txtnew").attr("disabled", false);
                        $("#txtconfirm").attr("disabled", false);
                        $("#txtnew").focus();
                    }
                    else
                    {
                        $("#imgRight").hide();
                        $("#imgWrong").show();
                        $("#txtnew").val('');
                        $("#txtconfirm").val('');
                        $("#txtnew").attr("disabled", true);
                        $("#txtconfirm").attr("disabled", true);
                    }
                },
                error: function (response) {
                    alert('Some Error Occur');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        },
        //select: function (e, i) {
        //    $("#hdnSearchEmpID").val(i.item.val);
        //    //alert($("#hdnSearchEmpID").val(i.item.val));
        //},
        minLength: 1
    });
    });

    $(document).ready(function () {
        $("#txtconfirm").autocomplete({
            source: function (request, response) {
                var newpwd = $("#txtnew").val();
                var confpwd = $("#txtconfirm").val();
                if (newpwd == confpwd) {
                    $("#lblright").show();
                    $("#lblwrong").hide();
                    $("#Add").attr("disabled", false);
                    $("#Add").focus();
                }
                else {
                    $("#lblright").hide();
                    $("#lblwrong").show();
                    $("#Add").attr("disabled", true);
                }
            },
            minLength: 1
        });
    });
    function beforeSave() {
        $("#form1").validate();
        $("#txtold").rules("add", { required: true, messages: { required: "Please Enter Your Old Password"} });
        $("#txtnew").rules("add", { required: true, messages: { required: "Please Enter Your New Password"} });
        $("#txtconfirm").rules("add", { required: true, messages: { required: "Please Re-Enter Your New Password"} });
    }
    
</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
<div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Change Password</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:DetailsView ID="dv" runat="server" Width="70%" CellPadding="5" CellSpacing="5"
                                                    AutoGenerateRows="False" Style="padding: 10px;" DefaultMode="Insert" OnModeChanging="dv_ModeChanging" 
                                                    GridLines="None" BackColor="AliceBlue" BorderColor="#0099FF" BorderWidth="0"
                                                    BorderStyle="Ridge">
                                                    <FooterStyle HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="headerStyle" HorizontalAlign="Center" />
                                                    <Fields>
                                                        <asp:TemplateField HeaderText="Old Password :" HeaderStyle-CssClass="headFont" >
                                                            <InsertItemTemplate>
                                                                <asp:TextBox ID="txtold" TextMode="Password" Width="200px" runat="server" CssClass="textbox"></asp:TextBox>
                                                                <img id="imgRight" src="img/icons/success.gif" alt="Change Password" style="Height:20px; Width:22px; display:none;" />
                                                                <img id="imgWrong" src="img/icons/fail.gif" alt="Change Password" style="Height:20px; Width:22px; display:none;" />
                                                            </InsertItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField >
                                                            <InsertItemTemplate>
                                                            </InsertItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="New Password :" HeaderStyle-CssClass="headFont" >
                                                            <InsertItemTemplate>
                                                                <asp:TextBox ID="txtnew" TextMode="Password" Width="200px" CssClass="textbox" runat="server"></asp:TextBox>
                                                            </InsertItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField >
                                                            <InsertItemTemplate>
                                                            </InsertItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Confirm Password :" HeaderStyle-CssClass="headFont">
                                                            <InsertItemTemplate>
                                                                <asp:TextBox ID="txtconfirm" TextMode="Password" Width="200px" CssClass="textbox"
                                                                    runat="server"></asp:TextBox>
                                                                <asp:Label ID="lblright" runat="server" Text="Right" ClientIDMode="Static" ForeColor="Green" CssClass="lblhideshow"></asp:Label>
                                                                <asp:Label ID="lblwrong" runat="server" Text="Wrong" ClientIDMode="Static" ForeColor="Red" CssClass="lblhideshow"></asp:Label>
                                                                <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="txtnew"
                                                                    ControlToValidate="txtconfirm" Display="Dynamic" ErrorMessage="" />
                                                            </InsertItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                    </Fields>
                                                   
                                           
                                               
                                        
                                                    <FooterTemplate>
                                                        <div align="right">
                                                            <asp:Button ID="Add" CommandName="Add" CssClass="Btnclassname" ToolTip="Submit" runat="server"
                                                                Text="Submit" OnClientClick="beforeSave()" CausesValidation="false" OnClick="Submit_Click"  />

                                                                <asp:Button ID="btnreset" CommandName="reset" CssClass="Btnclassname" ToolTip="Reset" runat="server"
                                                                Text="Reset" CausesValidation="false" OnClick="btnreset_Click"  />
                                                           <%-- <input type="reset" id="btnreset" value="Reset" class="Btnclassname" />--%>
                                                        </div>
                                                    </FooterTemplate>
                                                </asp:DetailsView>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

