﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Xml.Serialization;

public partial class LoanMsterRevised : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetEmployeeDetails(string EmployeeNo)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("get_empDetails_for_loan", EmployeeNo);  
            List<object> data = new List<object>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var dtDetails = new
                    {
                        EmployeeId = dr["EmployeeID"].ToString(),
                        EmployeeName = dr["EmpName"].ToString(),
                        Location = dr["CenterName"].ToString(),
                        DOR = dr["DOR"].ToString(),
                        NoOfLeftMonths=dr["CountMonths"].ToString()

                    };
                    data.Add(dtDetails);
                }
            }
            return data.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    [WebMethod]
    public static string GET_LoanDescription()
    {
        DataSet ds = DBHandler.GetResults("Get_LoanDescription");
        DataTable dt = ds.Tables[0];
        List<LoanDescription> listLoan = new List<LoanDescription>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                LoanDescription objst = new LoanDescription();

                objst.LoanTypeID = Convert.ToInt32(dt.Rows[i]["LoanTypeID"]);
                objst.LoanDesc = Convert.ToString(dt.Rows[i]["LoanDesc"]);

                listLoan.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listLoan);
    }
    class LoanDescription
    {
        public string LoanDesc { get; set; }
        public int LoanTypeID { get; set; }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_LoanDetails(object param)
    {
        try
        {
            string ConString = ""; string JSonVal = "";
            JavaScriptSerializer JSData = new JavaScriptSerializer();
            List<LoanMaster> SubChild = JSData.ConvertToType<List<LoanMaster>>(param);
            ConString = DataAccess.DBHandler.GetConnectionString();
            SqlConnection con = new SqlConnection(ConString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction Transaction;
            con.Open();
            Transaction = con.BeginTransaction();
            SqlDataReader dr;
            try
            {
                int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                int a = SubChild[0].FrstInstallmentNo.Equals("") ? 0 : SubChild[0].FrstInstallmentNo;
                cmd = new SqlCommand("Exec Insert_Loan_Revised '" + SubChild[0].EmployeeId + "' ,'" + 
                SubChild[0].LoanDescriptionId + "','" + 
                SubChild[0].SanctionNo + "','" +
                DateTime.ParseExact(SubChild[0].SanctionDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                SubChild[0].SanctionAmount + "','" +
                SubChild[0].DisbursementOrderNo + "','" +
                DateTime.ParseExact( SubChild[0].DisbursementOrderDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd")+ "','" +
                SubChild[0].DisbursementAmount + "','" +
                DateTime.ParseExact(SubChild[0].DeductionStartDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                SubChild[0].InterestOnDisbursementAmount + "','" +
                SubChild[0].TotalLoanInstall+"','"+
                SubChild[0].CurrentLoanInstall + "','" +
                SubChild[0].LoanPrincipalAmount + "','" +
                SubChild[0].LoanInterestAmount + "','" +
                //1st Slab
                (SubChild[0].FrstInstallmentNo.Equals("") ? 0 : SubChild[0].FrstInstallmentNo) + "','" +
                (SubChild[0].FrstInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].FrstInstallmentPrincipleAmount) + "','" +
                (SubChild[0].FrstInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].FrstInstallmentInterestAmount) + "','" + 
                //2nd Slab
                (SubChild[0].secndInstallmentNo.Equals("") ? 0 : SubChild[0].secndInstallmentNo) + "','" +
                (SubChild[0].secndInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].secndInstallmentPrincipleAmount) + "','" +
                (SubChild[0].secndInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].secndInstallmentInterestAmount) + "','" +
                //3rd Slab
                (SubChild[0].thirdInstallmentNo.Equals("") ? 0 : SubChild[0].thirdInstallmentNo) + "','" +
                (SubChild[0].thirdInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].thirdInstallmentPrincipleAmount) + "','" +
                (SubChild[0].thirdInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].thirdInstallmentInterestAmount) + "','" +
                //4th Slab
                (SubChild[0].FourthInstallmentNo.Equals("") ? 0 : SubChild[0].FourthInstallmentNo) + "','" +
                (SubChild[0].FourthInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].FourthInstallmentPrincipleAmount) + "','" +
                (SubChild[0].FourthInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].FourthInstallmentInterestAmount) + "','" +
                //5th Slab
                (SubChild[0].FifthInstallmentNo.Equals("") ? 0 : SubChild[0].FifthInstallmentNo) + "','" +
                (SubChild[0].FifthInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].FifthInstallmentPrincipleAmount) + "','" +
                (SubChild[0].FifthInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].FifthInstallmentInterestAmount) + "','" +
                //Retirement Adjustment Slab
                (SubChild[0].RetirementPrincipleAmount.Equals("") ? 0 : SubChild[0].RetirementPrincipleAmount) + "','" +
                (SubChild[0].RetirementPrincipleAdjustmentAmount.Equals("") ? 0 : SubChild[0].RetirementPrincipleAdjustmentAmount) + "','" +
                DateTime.ParseExact(SubChild[0].RetirementAdjustmentDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                UserId + "'", con, Transaction);
                cmd.ExecuteNonQuery();
                Transaction.Commit();
                string Val = "Success";
                JSonVal = Val.ToJSON();
            }
            catch(SqlException sqlError)
            {
                Transaction.Rollback();
                string Val = "Fail";
                JSonVal = Val.ToJSON();
            }
            finally {
                con.Close();
            }
            return JSonVal;
        }
        catch(Exception ex) {
            throw new Exception(ex.Message);
        }
        return "";
    }
    class LoanMaster
    {
        public int LoanId { get; set; }
        public int EmployeeId { get; set; }
        public int LoanDescriptionId { get; set; }

        public string SanctionNo { get; set; }
        public string SanctionDate { get; set; }
        public double SanctionAmount { get; set; }

        public string DisbursementOrderNo { get; set; }
        public string DisbursementOrderDate { get; set; }
        public double DisbursementAmount { get; set; }

        public string DeductionStartDate { get; set; }
        public double InterestOnDisbursementAmount { get; set; }
        

        public int TotalLoanInstall { get; set; }
        public int CurrentLoanInstall { get; set; }
        public double LoanPrincipalAmount { get; set; }
        public double LoanInterestAmount { get; set; }
        //1St Slab
        public int FrstInstallmentNo { get; set; }
        public double FrstInstallmentPrincipleAmount { get; set; }
        public double FrstInstallmentInterestAmount { get; set; }
        //2nd Slab
        public int secndInstallmentNo { get; set; }
        public double secndInstallmentPrincipleAmount { get; set; }
        public double secndInstallmentInterestAmount { get; set; }
        //3rd Slab
        public int thirdInstallmentNo { get; set; }
        public double thirdInstallmentPrincipleAmount { get; set; }
        public double thirdInstallmentInterestAmount { get; set; }
        //4th Slab
        public int FourthInstallmentNo { get; set; }
        public double FourthInstallmentPrincipleAmount { get; set; }
        public double FourthInstallmentInterestAmount { get; set; }
        //5ht Slab
        public int FifthInstallmentNo { get; set; }
        public double FifthInstallmentPrincipleAmount { get; set; }
        public double FifthInstallmentInterestAmount { get; set; }
        //Retirement Adjustment
        public double RetirementPrincipleAmount { get; set; }
        public double RetirementPrincipleAdjustmentAmount { get; set; }
        public string RetirementAdjustmentDate { get; set; }

    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] Get_SanctionNoForAutoComplete(string SanctionNo, string EmployeeId, string LoanDescriptionId)
    {
        List<string> result = new List<string>();
        try
        {
            
            DataTable dtLoaneeData = DBHandler.GetResult("Get_SanctionNoAutocomplete", SanctionNo, EmployeeId, LoanDescriptionId);  //HttpContext.Current.Session["MenuID"]
            foreach (DataRow dtRow in dtLoaneeData.Rows)
            {
                result.Add(string.Format("{0}|{1}", dtRow["SanctionNoDate"].ToString().Trim(), dtRow["LoanID"].ToString().Trim()));
            }
            
        }
        catch { }
        return result.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] Get_SanctionNoForAutoComplete_Disbursement(string SanctionNo, string EmployeeId, string LoanDescriptionId)
    {
        List<string> result = new List<string>();
        try
        {

            DataTable dtLoaneeData = DBHandler.GetResult("Get_SanctionNoAutocomplete_SanctionAmountGreaterThanDisbursementAmount", SanctionNo, EmployeeId, LoanDescriptionId);  //HttpContext.Current.Session["MenuID"]
            foreach (DataRow dtRow in dtLoaneeData.Rows)
            {
                result.Add(string.Format("{0}|{1}", dtRow["SanctionNoDate"].ToString().Trim(), dtRow["LoanID"].ToString().Trim()));
            }
            
        }
        catch { }
        return result.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetDetails_AgainstSanctionNo(string LoanId)
    {
        String JSonVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Details_AgainstSanctionNo",Convert.ToInt32(LoanId));
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow r in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }
                JSonVal = rows.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSonVal;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Check_DuplicateSanctionNo(string SanctionNo)
    {
        string JSValue = "";
        DataTable dt = DBHandler.GetResult("Check_DuplicateSanctionNo", SanctionNo);
        if (dt.Rows.Count > 0)
        {
            JSValue = dt.Rows[0]["DuplicateStatus"].ToString();
        }
        return JSValue.ToJSON();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetDetails_AgainstSanctionNoExisting(string LoanId)
    {

        DataTable dt = DBHandler.GetResult("Get_Details_AgainstSanctionNoExisting", Convert.ToInt32(LoanId));
            List<object> data = new List<object>();
            if (dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    var dataDetails = new {
                        DisbursementOrderNo = dr["DisbursementOrderNo"].ToString(),
                        LoanID=dr["LoanID"].ToString(),
                        DisburseAmount = dr["DisburseAmount"].ToString(),
                        DisbursementOrderDate = dr["DisbursementOrderDate"],
                        SanctionNo = dr["SanctionNo"],
                        SanctionAmount = dr["SanctionAmount"]
                    };
                    data.Add(dataDetails);
                }
            }
       
        return data.ToJSON();
    }


    ////////
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_LoanDetailsAgainstAlreadySanctionNo(object param)
    {
        try
        {
            string ConString = ""; string JSonVal = "";
            JavaScriptSerializer JSData = new JavaScriptSerializer();
            List<LoanMaster> SubChild = JSData.ConvertToType<List<LoanMaster>>(param);
            ConString = DataAccess.DBHandler.GetConnectionString();
            SqlConnection con = new SqlConnection(ConString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction Transaction;
            con.Open();
            Transaction = con.BeginTransaction();
            SqlDataReader dr;
            try
            {
                int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                int a = SubChild[0].FrstInstallmentNo.Equals("") ? 0 : SubChild[0].FrstInstallmentNo;
                cmd = new SqlCommand("Exec Insert_Loan_Revised_Against_AlreadySanctionNo '" + SubChild[0].LoanId + "','" + SubChild[0].EmployeeId + "' ,'" +
                SubChild[0].LoanDescriptionId + "','" +
                SubChild[0].SanctionNo + "','" +
                DateTime.ParseExact(SubChild[0].SanctionDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                SubChild[0].SanctionAmount + "','" +
                SubChild[0].DisbursementOrderNo + "','" +
                DateTime.ParseExact(SubChild[0].DisbursementOrderDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                SubChild[0].DisbursementAmount + "','" +
                DateTime.ParseExact(SubChild[0].DeductionStartDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                SubChild[0].InterestOnDisbursementAmount + "','" +
                SubChild[0].TotalLoanInstall + "','" +
                SubChild[0].LoanPrincipalAmount + "','" +
                SubChild[0].LoanInterestAmount + "','" +
                //1st Slab
                (SubChild[0].FrstInstallmentNo.Equals("") ? 0 : SubChild[0].FrstInstallmentNo) + "','" +
                (SubChild[0].FrstInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].FrstInstallmentPrincipleAmount) + "','" +
                (SubChild[0].FrstInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].FrstInstallmentInterestAmount) + "','" +
                    //2nd Slab
                (SubChild[0].secndInstallmentNo.Equals("") ? 0 : SubChild[0].secndInstallmentNo) + "','" +
                (SubChild[0].secndInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].secndInstallmentPrincipleAmount) + "','" +
                (SubChild[0].secndInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].secndInstallmentInterestAmount) + "','" +
                    //3rd Slab
                (SubChild[0].thirdInstallmentNo.Equals("") ? 0 : SubChild[0].thirdInstallmentNo) + "','" +
                (SubChild[0].thirdInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].thirdInstallmentPrincipleAmount) + "','" +
                (SubChild[0].thirdInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].thirdInstallmentInterestAmount) + "','" +
                    //4th Slab
                (SubChild[0].FourthInstallmentNo.Equals("") ? 0 : SubChild[0].FourthInstallmentNo) + "','" +
                (SubChild[0].FourthInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].FourthInstallmentPrincipleAmount) + "','" +
                (SubChild[0].FourthInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].FourthInstallmentInterestAmount) + "','" +
                    //5th Slab
                (SubChild[0].FifthInstallmentNo.Equals("") ? 0 : SubChild[0].FifthInstallmentNo) + "','" +
                (SubChild[0].FifthInstallmentPrincipleAmount.Equals("") ? 0 : SubChild[0].FifthInstallmentPrincipleAmount) + "','" +
                (SubChild[0].FifthInstallmentInterestAmount.Equals("") ? 0 : SubChild[0].FifthInstallmentInterestAmount) + "','" +
                    //Retirement Adjustment Slab
                (SubChild[0].RetirementPrincipleAmount.Equals("") ? 0 : SubChild[0].RetirementPrincipleAmount) + "','" +
                (SubChild[0].RetirementPrincipleAdjustmentAmount.Equals("") ? 0 : SubChild[0].RetirementPrincipleAdjustmentAmount) + "','" +
                DateTime.ParseExact(SubChild[0].RetirementAdjustmentDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                UserId + "'", con, Transaction);
                cmd.ExecuteNonQuery();
                Transaction.Commit();
                string Val = "Success";
                JSonVal = Val.ToJSON();
            }
            catch (SqlException sqlError)
            {
                Transaction.Rollback();
                string Val = "Fail";
                JSonVal = Val.ToJSON();
            }
            finally
            {
                con.Close();
            }
            return JSonVal;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return "";
    }
    ////////


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Get_SanctionNoToGenerateNewDisbursementNo(string SanctionNo)
    {
        string JSValue = "";
        DataTable dt = DBHandler.GetResult("Get_SanctionNoToGenerateNewDisbursementNo", SanctionNo);
        if (dt.Rows.Count > 0)
        {
            JSValue = dt.Rows[0]["DuplicateStatus"].ToString();
        }
        return JSValue.ToJSON();
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_PrepaymentAmount(object param)
    {
        try
        {
         string ConString = ""; string JSonVal = "";
            JavaScriptSerializer JSData = new JavaScriptSerializer();
            List<Prepayment> SubChild = JSData.ConvertToType<List<Prepayment>>(param);
            ConString = DataAccess.DBHandler.GetConnectionString();
            SqlConnection con = new SqlConnection(ConString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction Transaction;
            con.Open();
            Transaction = con.BeginTransaction();
            SqlDataReader dr;
            try
            {
                int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                cmd = new SqlCommand("Exec Save_PrepaymentDetails '" +
                SubChild[0].LoanId + "','" +
                DateTime.ParseExact(SubChild[0].PrepaymentDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                SubChild[0].PrepaymentPrincipleAmount + "','" +
                SubChild[0].PrepaymentInterestAmount + "','" +
                SubChild[0].PrepaymentInterestAdjusmentAmount + "','" +
                UserId + "'", con, Transaction);
                cmd.ExecuteNonQuery();
                Transaction.Commit();
                string Val = "Success";
                JSonVal = Val.ToJSON();
            }
            catch (SqlException sqlError)
            {
                Transaction.Rollback();
                string Val = "Fail";
                JSonVal = Val.ToJSON();
            }
            finally
            {
                con.Close();
            }
            return JSonVal;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return "";
    }
    class Prepayment
    {
        public string PrepaymentDate { get; set; }
        public int PrepaymentPrincipleAmount { get; set; }
        public int PrepaymentInterestAmount { get; set; }
        public int PrepaymentInterestAdjusmentAmount { get; set; }
        public int LoanId { get; set; }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_RetirementpaymentAmount(object param)
    {
        try
        {
            string ConString = ""; string JSonVal = "";
            JavaScriptSerializer JSData = new JavaScriptSerializer();
            List<RetirementAmount> SubChild = JSData.ConvertToType<List<RetirementAmount>>(param);
            ConString = DataAccess.DBHandler.GetConnectionString();
            SqlConnection con = new SqlConnection(ConString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction Transaction;
            con.Open();
            Transaction = con.BeginTransaction();
            SqlDataReader dr;
            try
            {
                int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                cmd = new SqlCommand("Exec Save_RetirementpaymentDetails '" +
                SubChild[0].LoanId + "','" +
                DateTime.ParseExact(SubChild[0].RetAdjDate.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy'-'MM'-'dd") + "','" +
                SubChild[0].RetAdjPrinAmt + "','" +
                SubChild[0].RetAdjIntAmt + "','" +
                UserId + "'", con, Transaction);
                cmd.ExecuteNonQuery();
                Transaction.Commit();
                string Val = "Success";
                JSonVal = Val.ToJSON();
            }
            catch (SqlException sqlError)
            {
                Transaction.Rollback();
                string Val = "Fail";
                JSonVal = Val.ToJSON();
            }
            finally
            {
                con.Close();
            }
            return JSonVal;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return "";
    }
    class RetirementAmount
    {
        public string RetAdjDate { get; set; }
        public int RetAdjPrinAmt { get; set; }
        public int RetAdjIntAmt { get; set; }
        public int LoanId { get; set; }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Validate_SanctionDate(string DisbursementOrderDate, string LoanSanctionDate)
    {
        string JSON = "";
        DataTable dt = DBHandler.GetResult("Validate_SanctionDate", DisbursementOrderDate, LoanSanctionDate);
        string Status = dt.Rows[0]["Result"].ToString();
        JSON = Status.ToJSON();
        return JSON;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Validate_DisbursementOrderDate(string DisbursementOrderDate, string LoanSanctionDate)
    {
        string JSON = "";
        DataTable dt = DBHandler.GetResult("Validate_DisbursementDate", DisbursementOrderDate, LoanSanctionDate);
        string Status = dt.Rows[0]["Result"].ToString();
        JSON = Status.ToJSON();
        return JSON;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Get_EncodedValuebySectorID(string SectorId)
    {
        string JSON = "";
        DataTable dt = DBHandler.GetResult("Get_EncodedValuebySectorID", SectorId.Equals("") ? 0 : Convert.ToInt32(SectorId));
        string EncodedValue = dt.Rows[0]["EncodedValue"].ToString();
        JSON = EncodedValue.ToJSON();
        return JSON;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Validate_DeductionStartDate(string DisbursementOrderDate, string DeductionStartDate)
    {
        string JSON = "";
        DataTable dt = DBHandler.GetResult("Validate_DeductionStartDate", DisbursementOrderDate, DeductionStartDate);
        string Status = dt.Rows[0]["Result"].ToString();
        JSON = Status.ToJSON();
        return JSON;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Get_RetirementBenifitByLoanId(string LoanId)
    {
        string JSON = "";
        DataTable dtstr = DBHandler.GetResult("Get_RetirementBenifitByLoanId", Convert.ToInt32(LoanId));
        List<object> data = new List<object>();
        if (dtstr.Rows.Count > 0)
        {
            foreach (DataRow dr in dtstr.Rows)
            {
                var dtDetails = new
                {
                    RetAdjPrincleAmt = dr["RetAdjPrincleAmt"],
                    RetAdjAmountDate = dr["RetAdjAmountDate"],
                    RetAdjInterestAmt = dr["RetAdjInterestAmt"]
                };
                data.Add(dtDetails);
            }
        }
        return data.ToJSON();
    }
    [WebMethod]
    public static string[] AutoComplete_SearchEmployee(string txtEmpNoSearch)
  {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_Employee", txtEmpNoSearch, 1);
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["EmployeeID"].ToString().Trim(), dtRow["EmpNo"].ToString().Trim()));
        }
        return result.ToArray();
    }
}