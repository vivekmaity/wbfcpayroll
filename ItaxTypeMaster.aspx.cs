﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using DataAccess;

public partial class ItaxTypeMaster : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }   
    
   [WebMethod]    
    public static string Get_Data(string FinYear)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("LoadItaxType",FinYear);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string Save(string ItaxTypeID,string TypeDesc, string MaxType, string MaxAmount, string FinYear, string Sex)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("InstUpdtItaxType", ItaxTypeID.Equals("")?0:(object)ItaxTypeID, TypeDesc, MaxType, MaxAmount, FinYear, Sex, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string Delete(string ID)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("Delete_ItaxType",ID);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}