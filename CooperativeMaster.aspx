﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="CooperativeMaster.aspx.cs" Inherits="CooperativeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            //$("#txtCooperativeCode").rules("add", { required: true, messages: { required: "Please enter Cooperative Code"} });
            $("#txtCooperativeName").rules("add", { required: true, messages: { required: "Please enter Cooperative Name"} });
        }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Cooperative Master</h2>						
						</section>
                    <table width="98%"   style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <%--<td style="padding:5px;" ><span class="headFont">Cooperative Code &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtCooperativeCode" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3"></asp:TextBox>                            
                                        </td>--%>
                                        <td style="padding:5px;"><span class="headFont">Cooperative Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                       <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtCooperativeName" autocomplete="off"  ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>                            
                                        </td>
                                    </tr>
                                   
                                </table>
                            </InsertItemTemplate>

                            <EditItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                        <%--<td style="padding:10px;"> <span class="headFont">Cooperative Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtCooperativeCode" Text='<%# Eval("CooperativeCode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" Enabled="false" autocomplete="off"></asp:TextBox>                            
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("CooperativeID") %>'/>
                                        </td>--%>
                                        <td style="padding:5px;"><span class="headFont">Cooperative Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtCooperativeName" Text='<%# Eval("Cooperative") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="20" autocomplete="off"></asp:TextBox>                            
                                           
                                        </td>
                                        
                                    </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:auto;width:100%">
                            <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="CooperativeCode" OnRowCommand="tbl_RowCommand" AllowPaging="false" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("CooperativeID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                                OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("CooperativeID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CooperativeID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Cooperative ID" />
                                    <%--<asp:BoundField DataField="CooperativeCode" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Cooperative Code" />--%>
                                    <asp:BoundField DataField="Cooperative" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Cooperative Name" />
                                    </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

