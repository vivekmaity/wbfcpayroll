﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;


public partial class Field_Permission : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try 
        {
            if (!IsPostBack)
            {
                PopulateUser();
                PopulateTable();
              //LockField.InnerHtml=  GetGridControlContent("~/GridviewTemplete.ascx"); //mainDiv.InnerHtml = GetGridControlContent("~/TransferEmpDetail.ascx", SecID, flag);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected  void PopulateUser()
    {
        try
        {
            DataTable dtUser = DBHandler.GetResult("Get_UserName");
            if (dtUser.Rows.Count > 0)
            {
                ddlUser.DataSource = dtUser;
                ddlUser.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void PopulateTable()
    {
        try
        {
            DataTable dtTable = DBHandler.GetResult("Get_TableName");
            if (dtTable.Rows.Count > 0)
            {
                ddlTableName.DataSource = dtTable;
                ddlTableName.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AjaxGetGridCtrl()
    {
        try
        {
            return GetGridControlContent("~/GridviewTemplete.ascx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private static String GetGridControlContent(String controlLocation)
    {
        try
        {
            string FieldPermission = "Lock";
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.FieldPermission = FieldPermission;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string PopulateGridUserWise(string UserID)
    {
        try
        {
            return GetGridUserWise("~/GridviewTemplete.ascx", UserID);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private static String GetGridUserWise(String controlLocation, string UserID)
    {
        try
        {
            string FieldPermission = "UserWise";
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.UserWise = FieldPermission;
            userControl.UserID = UserID;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetEmployee(string EmpNo)
    {
        string JSONVal = ""; string EmpName = "";
        try
        {
            DataSet dt = DBHandler.GetResults("get_empDetails_for_FieldPermission", EmpNo);
            DataTable dtEmpdetails = dt.Tables[0];
            if (dtEmpdetails.Rows.Count > 0)
            {
                EmpName = dtEmpdetails.Rows[0]["EmpName"].ToString();
            }
            JSONVal = EmpName.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateLockFlag(string EmpNo, string Lockflag)
    {
        string JSONVal = ""; string Value="";
        try
        {
            DataTable dt = DBHandler.GetResult("Update_LockFlag", EmpNo, Lockflag);
            if (dt.Rows.Count > 0)
            {
                string result = dt.Rows[0]["Result"].ToString();
                if (result == "Updated")
                    Value = "Success";
                else
                    Value = "Fail";
            }
            JSONVal = Value.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteLockPermission(string FieldID)
    {
        string JSONVal = ""; string Value = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Delete_LockFieldPermission", FieldID);
            Value = "Deleted";
            JSONVal = Value.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string LockEmployeePermissions(string UserID, string Tablename, string Fieldname, string Fieldtype)
    {
        string JSONVal = ""; int FieldID = 0; string result="";
        try
        {
            DataSet ds = DBHandler.GetResults("Insert_FieldPermission", UserID, Tablename, Fieldname, Fieldtype);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                FieldID = Convert.ToInt32(dt.Rows[0]["FieldID"]);
                if (FieldID > 0)
                    result = "Success";
                else
                    result = "Fail";
            }
            JSONVal = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }


    [WebMethod]
    public static string BindFieldName(string TableName)
    {
        DataSet ds = DBHandler.GetResults("Get_FieldName", TableName);
        DataTable dt = ds.Tables[0];

        List<Fields> listFiled = new List<Fields>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Fields objst = new Fields();

                objst.COLUMN_NAMEid = dt.Rows[i]["COLUMN_NAME"].ToString();
                objst.COLUMN_NAME = dt.Rows[i]["COLUMN_NAME"].ToString();

                listFiled.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listFiled);

    }
    public class Fields
    {
        public string COLUMN_NAMEid { get; set; }
        public string COLUMN_NAME { get; set; }
    }
}