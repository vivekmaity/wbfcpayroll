﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BankMaster.aspx.cs" Inherits="BankMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function beforeSave() {
            $("#form1").validate();
            $("#txtBankCode").rules("add", { required: true, messages: { required: "Please enter Bank Code"} });
            $("#txtBankName").rules("add", { required: true, messages: { required: "Please enter Bank Name"} });
            $("#txtBranch").rules("add", { required: true, messages: { required: "Please enter Bank branch"} });
            $("#txtIFSC").rules("add", { required: true, messages: { required: "Please enter Bank IFSC"} });
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <asp:HiddenField ID="hdnBankID" runat="server" Value="" ClientIDMode="Static" />
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Bank  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                             
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    :
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                      <asp:TextBox ID="txtBankName" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox"
                                                                        MaxLength="200"></asp:TextBox>
                                                                </td>
                                                            </tr>
                              
                                                        </table>
                                                    </InsertItemTemplate>
                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                               
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    :
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBankName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                        MaxLength="200" Text='<%# Eval("BankName") %>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="require">*</span> indicates Mandatory Field
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" Width="100px"
                                                                            CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' OnClick="cmdSave_Click" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Refresh" Width="100px" CssClass="Btnclassname"
                                                                            OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="overflow-y: scroll; height: auto; width: 100%">
                                                    <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                                                        AutoGenerateColumns="false" DataKeyNames="BankID" OnRowCommand="tbl_RowCommand"
                                                        AllowPaging="true" PageSize="10"
                                                         CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                                        <AlternatingRowStyle BackColor="#FFFACD" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <HeaderStyle  />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                        runat="server" ID="btnEdit" CommandArgument='<%# Eval("BankID") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <HeaderStyle  />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                        OnClientClick='return Delete();' CommandArgument='<%# Eval("BankID") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="BankID" Visible="false" ItemStyle-CssClass="labelCaption"
                                                                HeaderStyle-CssClass="TableHeader" HeaderText="BankID" />
                                                           
                                                            <asp:BoundField DataField="BankName" 
                                                                HeaderText="Bank Name" />
                                                            
                                                            
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

