﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;            
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;             
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class PayScheduleHistory : System.Web.UI.Page
{
    public string str1 = "";
    static string StrFormula = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                //PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    
    public class SectorbyMonth
    {
        //public int CenterID { get; set; }
        //public string CenterName { get; set; }
        public string SalMonth { get; set; }
        public int SalMonthID { get; set; }
    }
    [WebMethod]
     public static string GetLocation(int SecID, string SalFinYear)
    {
        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        //DataSet ds1 = DBHandler.GetResults("Get_SalPayMonthHist", SecID);
        //DataTable dt = ds.Tables[0];
        DataTable dtLocation = ds.Tables[0];
        //DataTable dtSalMonthID = ds1.Tables[0];

        List<CenterbyReportType> listLocation = new List<CenterbyReportType>();

        if (dtLocation.Rows.Count > 0)
        {
            for (int i = 0; i < dtLocation.Rows.Count; i++)
            {
                CenterbyReportType objst = new CenterbyReportType();

                //objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                //objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                objst.CenterName = Convert.ToString(dtLocation.Rows[i]["CenterName"]);
                objst.CenterID = Convert.ToInt32(dtLocation.Rows[i]["CenterID"]);
                listLocation.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listLocation);

    }
     public class CenterbyReportType
     {
         //public int CenterID { get; set; }
         //public string CenterName { get; set; }
         public string CenterName { get; set; }
         public int CenterID { get; set; }
     }


     [WebMethod]
     public static string GetDeduction()
     {
         String SchType;
         SchType = "Deduction";
         DataSet ds = DBHandler.GetResults("Get_ScheduleType", SchType);
         DataTable dt = ds.Tables[0];

         List<ScheduleDeduction> listDeduction = new List<ScheduleDeduction>();

         if (dt.Rows.Count > 0)
         {
             for (int i = 0; i < dt.Rows.Count; i++)
             {
                 ScheduleDeduction objst = new ScheduleDeduction();

                 objst.EDID = Convert.ToInt32(dt.Rows[i]["EDID"]);
                 objst.EDName = Convert.ToString(dt.Rows[i]["EDName"]);

                 listDeduction.Insert(i, objst);
             }
         }

         JavaScriptSerializer jscript = new JavaScriptSerializer();
         return jscript.Serialize(listDeduction);

     }
     [WebMethod]
     public static string GetSalMonth(string SalFinYear)
     {
         string AccFinYr = HttpContext.Current.Session[SiteConstants.SSN_SALACC].ToString();
         //DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
         DataSet ds1 = DBHandler.GetResults("Get_SalPayMonthHist", AccFinYr);
         //DataTable dt = ds.Tables[0];
         DataTable dtSalMonth = ds1.Tables[0];
         //DataTable dtSalMonthID = ds1.Tables[0];

         List<SectorbyMonth> listSalMonth = new List<SectorbyMonth>();

         if (dtSalMonth.Rows.Count > 0)
         {
             for (int i = 0; i < dtSalMonth.Rows.Count; i++)
             {
                 SectorbyMonth objst = new SectorbyMonth();

                 //objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                 //objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                 objst.SalMonth = Convert.ToString(dtSalMonth.Rows[i]["SalMonth"]);
                 objst.SalMonthID = Convert.ToInt32(dtSalMonth.Rows[i]["SalMonthID"]);
                 listSalMonth.Insert(i, objst);
             }
         }

         JavaScriptSerializer jscript = new JavaScriptSerializer();
         return jscript.Serialize(listSalMonth);

     }
     public class ScheduleDeduction
     {
         public int EDID { get; set; }
         public string EDName { get; set; }
     }
     [WebMethod]
     public static string GetLoan()
     {

         String SchType;
         SchType = "Loan";
         DataSet ds = DBHandler.GetResults("Get_ScheduleType", SchType);
         DataTable dt = ds.Tables[0];

         List<ScheduleLoan> listLoan = new List<ScheduleLoan>();

         if (dt.Rows.Count > 0)
         {
             for (int i = 0; i < dt.Rows.Count; i++)
             {
                 ScheduleLoan objst = new ScheduleLoan();

                 objst.EDID = Convert.ToInt32(dt.Rows[i]["EDID"]);
                 objst.EDName = Convert.ToString(dt.Rows[i]["EDName"]);

                 listLoan.Insert(i, objst);
             }
         }

         JavaScriptSerializer jscript = new JavaScriptSerializer();
         return jscript.Serialize(listLoan);

     }
     public class ScheduleLoan
     {
         public int EDID { get; set; }
         public string EDName { get; set; }
     }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
     public static string Report_Paravalue(int StrSalMonthID, int StrSchedule, string StrEmpType, string SalFinYear,string FormName, string ReportName, string ReportType)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        String Msg = "";
        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        //if (StrSchedule == 1)
        //{
        //    ReportName = "Pay_Schedule_ITAX_History";
        //}
        //if (StrSchedule == 5)
        //{
        //    ReportName = "Pay_Schedule_PTAX_History";
        //}
        //if (ReportName == "PaySummarySecWiseHistory")
        //{
        //    StrFormula = "{tmp_Summary_PayDetails_sectorWise_History.Secid}=" + StrSecID + " and {tmp_Summary_PayDetails_sectorWise_History.MonthID}=" + StrSalMonthID + " and {tmp_Summary_PayDetails_sectorWise_History.insertedBy}=" +UserID+ "";

        //}
        //if (ReportName == "History_PaySlip_new_LocationWISE_A5")
        //{
        //    StrFormula = "{tmp_payslipHistory_monthly.SecID}=" + StrSecID + " and {MST_SalaryMonth.SalMonthID}=" + StrSalMonthID + " and {tmp_payslipHistory_monthly.inserted_By}=" + UserID + " and {tmp_mst_employee.tabInsertedBy}=" + UserID + " and {tmp_mst_employee.Status} in ['Y','S']";

        //}
        //if (ReportName == "PaySummarySecWiseLocationWise_History")
        //{
        //    if (StrCenID == 0)
        //    {
        //        StrFormula = "{tmp_Summary_PayDetails_sectorWise_centerWise_History.Secid}=" + StrSecID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.MonthID}=" + StrSalMonthID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.insertedBy}=" + UserID + "";
        //    }
        //    else
        //    {
        //        StrFormula = "{tmp_Summary_PayDetails_sectorWise_centerWise_History.Secid}=" + StrSecID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.MonthID}=" + StrSalMonthID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.Centerid}=" + StrCenID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.insertedBy}=" + UserID + "";
        //    }

        //}
        StrFormula = "{MST_SalaryMonth.SalMonthID}=" + StrSalMonthID + " and {DAT_MonthlySalary_History.EDID}=" + StrSchedule + "";
        if (ReportName == "AdminAllsector_COOP_hist")
        {
            int Dedu_EDID = 22;
            int Loan_EDID = 315;

            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF_hist", SalFinYear, StrSalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, UserID);
            StrFormula = "{RPT_AdminAllsector_COOP_PF_CPF_Hist.UserID}=" + UserID;
        }
        if (ReportName == "AdminAllsector_CPF_all_PF_state_hist")
        {
            int Dedu_EDID = 14;
            int Loan_EDID = 311;

            //if (StrSchedule == 1)
            //{
            //    Dedu_EDID = 14;
            //    Loan_EDID = 311;
            //}
            if (StrSchedule == 2)
            {
                Dedu_EDID = 33;
                Loan_EDID = 311;
            }

            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF_hist", SalFinYear, StrSalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            StrFormula = "{RPT_AdminAllsector_COOP_PF_CPF_Hist.UserID}=" + UserID;
        }
        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

        //DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Update_RptParameterValue", UserID, FormName, ReportName, ReportType, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9, Parameter10);

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Count_Excel(int StrSalMonthID, int StrSchedule,string SalFinYear,string StrEmpType, string ReportName)
    {
        string jsval = ""; string rowscount = "";
        if (ReportName == "Pay_Schedule_ITAX_History")
        {
           DataTable dt = DBHandler.GetResult("Load_ItaxScheduleHistExcel", StrSalMonthID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }

        if (ReportName == "Pay_Schedule_PTAX_History")
        {
            DataTable dt = DBHandler.GetResult("Load_PtaxScheduleHistExcel", StrSalMonthID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }

        if (ReportName == "Pay_Schedule_Mice_History")
        {
            DataTable dt = DBHandler.GetResult("Load_AllScheduleHistExcel", StrSalMonthID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }

        if (ReportName == "Pay_Schedule_Loan_History")
        {
            DataTable dt = DBHandler.GetResult("Load_AllScheduleLoadHistExcel", StrSalMonthID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }
        if (ReportName == "AdminAllsector_COOP_hist")
        {
            int Dedu_EDID = 22;
            int Loan_EDID = 315;

            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF_hist", SalFinYear, StrSalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCOOP_Hist", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            rowscount = dt.Rows.Count.ToJSON();
        }
        if (ReportName == "AdminAllsector_CPF_all_PF_state_hist")
        {
            int Dedu_EDID = 14;
            int Loan_EDID = 311;

            //if (StrSchedule == 1)
            //{
            //    Dedu_EDID = 14;
            //    Loan_EDID = 311;
            //}
            if (StrSchedule == 2)
            {
                Dedu_EDID = 33;
                Loan_EDID = 311;
            }

            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF_hist", SalFinYear, StrSalMonthID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCPFPF_Hist", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            rowscount = dt.Rows.Count.ToJSON();
        }
        jsval = rowscount.ToJSON();
        return jsval;
    }

}