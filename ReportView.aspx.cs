﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;                 
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;                  
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Management;   
   
public partial class ReportView : System.Web.UI.Page        
{
    static string SalaryFinYear="";
    static string SalMonth="";
    static int SectorID = 0;
    static int CenterID=0;
    static string EmpType="";
    static string Status="";
    static int EDID=0;
    static string SectorGroup="";
    static string CenterGroup="";
    static string EmpTypeGroup="";
    static string StatusGroup="";
    static string SchType="";
    static int UserID = 0;
    static string RptName = "";
    static string RptSectorName = "";
    static string RptSalMonth = "";
    static string PDFRptName = "";       
    static string RptType = "";
    static string RptEDAbbv = "";
    String ConnString = "";      
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
    protected void Page_Load(object sender, EventArgs e)            
    {
            SetPaperSize();
            BindReport(crystalReport);
    }

    protected void ExportPDF(object sender, EventArgs e)
    {
        
            ReportDocument crystalReport = new ReportDocument();
            bool ReportStatus=BindReport(crystalReport);
            if (ReportStatus == true)
            {

                ExportFormatType formatType = ExportFormatType.NoFormat;
                switch (rbFormat.SelectedItem.Value)
                {
                    case "Excel":
                        formatType = ExportFormatType.Excel;
                        break;
                    case "PDF":
                        formatType = ExportFormatType.PortableDocFormat;
                        break;
                }
                crystalReport.ExportToHttpResponse(formatType, Response, true, PDFRptName);
                Response.End();
            }
            else {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Record Found!!.')</script>");

            }
      
        
    }

    private bool BindReport(ReportDocument crystalReport)
    {
        bool ReportStatus = true;
        ConnString = DataAccess.DBHandler.GetConnectionString();
        SqlConnection con = new SqlConnection(ConnString);
        if (!IsPostBack)
        {
            Exec_Proc();
        }
        //crystalReport.Refresh();
        DataTable dt = new DataTable();
        //SqlDataAdapter(Query,Connection)
        SqlDataAdapter adp = new SqlDataAdapter("Select * from MST_PaySched_Print where USERID=" + UserID + "", con);
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["EDAbbv1"].ToString() != "")
            {
                RptName = dt.Rows[0]["EDAbbv1"].ToString();
                RptType = dt.Rows[0]["SchedType"].ToString();
                RptSectorName = dt.Rows[0]["SectorName"].ToString();
                RptSalMonth = dt.Rows[0]["SalMonth"].ToString();
                RptEDAbbv = dt.Rows[0]["EDAbbv"].ToString();
                PDFRptName = RptSectorName + "_" + RptEDAbbv + "(" + RptSalMonth + ")";
            }




            if (RptType == "DEDUCTION" && RptName != "")
            {

                if (RptName == "ITAX")
                {
                    crystalReport.Load(Server.MapPath("~/Reports/Pay_Schedule_ITAX.rpt"));
                    crystalReport.RecordSelectionFormula = "{Rpt_Print_PaySched.USERID}=" + UserID + "";
                }
                else if (RptName == "PTAX")
                {
                    crystalReport.Load(Server.MapPath("~/Reports/Pay_Schedule_PTAX.rpt"));
                    crystalReport.RecordSelectionFormula = "{Rpt_Print_PaySched.USERID}=" + UserID + "";
                }

                else if (RptName == "MISC-D" || RptName == "HRA DEDU" || RptName == "ELEC" || RptName == "SSS" || RptName == "LIC")
                {
                    crystalReport.Load(Server.MapPath("~/Reports/Pay_Schedule_Misc_LIC_Ded.rpt"));
                    crystalReport.RecordSelectionFormula = "{Rpt_Print_PaySched.USERID}=" + UserID + "";
                }
                else
                {
                    // Other Report Calling
                    crystalReport.Load(Server.MapPath("~/Reports/Pay_Schedule_Misc_LIC_Ded.rpt"));
                    crystalReport.RecordSelectionFormula = "{Rpt_Print_PaySched.USERID}=" + UserID + "";
                }
            }
            else if (RptType == "LOAN" && RptName != "")
            {
                // Loan Report Calling
                crystalReport.Load(Server.MapPath("~/Reports/Pay_Schedule_Loan.rpt"));
                crystalReport.RecordSelectionFormula = "{Rpt_Print_PaySched.USERID}=" + UserID + "";

            }

            if (RptType != "" && RptName != "")
            {
                crystalReport.DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
                crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                crystalReport.SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
                crystalReport.VerifyDatabase();
                crystalReport.Refresh();
                CrystalReportViewer1.DataBind();
                CrystalReportViewer1.DisplayToolbar = true;
                CrystalReportViewer1.Zoom(150);  // Page Width
                CrystalReportViewer1.Visible = true;
                CrystalReportViewer1.ReportSource = crystalReport;
                crystalReport.Refresh();
            }
        }
        else
        {
            ReportStatus = false;
        }
        return ReportStatus;
    }

    public static void Exec_Proc()
    {
        SalaryFinYear = Convert.ToString(HttpContext.Current.Request.QueryString["SalaryFinYear"]);
        SalMonth = Convert.ToString(HttpContext.Current.Request.QueryString["SalMonth"]);
        SectorID = Convert.ToInt32(HttpContext.Current.Request.QueryString["SectorID"]);
        CenterID = Convert.ToInt32(HttpContext.Current.Request.QueryString["CenterID"]);
        EmpType = Convert.ToString(HttpContext.Current.Request.QueryString["EmpType"]);
        Status = Convert.ToString(HttpContext.Current.Request.QueryString["Status"]);
        EDID = Convert.ToInt32(HttpContext.Current.Request.QueryString["EDID"]);
        SectorGroup = Convert.ToString(HttpContext.Current.Request.QueryString["SectorGroup"]);
        CenterGroup = Convert.ToString(HttpContext.Current.Request.QueryString["CenterGroup"]);
        EmpTypeGroup = Convert.ToString(HttpContext.Current.Request.QueryString["EmpTypeGroup"]);
        StatusGroup = Convert.ToString(HttpContext.Current.Request.QueryString["StatusGroup"]);
        SchType = Convert.ToString(HttpContext.Current.Request.QueryString["SchType"]);
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);

        DataTable TableSector = DBHandler.GetResult("Get_Sector", UserID);
        DataSet dtcr = new DataSet();
         dtcr = DBHandler.GetResults("Get_PaySchedule", SalaryFinYear, SalMonth,SectorID, CenterID, EmpType, Status, EDID, SectorGroup, CenterGroup, EmpTypeGroup, StatusGroup, SchType, UserID);
        
    }

    [WebMethod(EnableSession=true)]
    [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
    public static string Func_Print()
    {
        var StrMsg="Success";
        
        
        //var strconfirm = "<script>if(!window.confirm('Are you sure?')){window.location.href='Default.aspx'}</script>";
        ReportDocument crystalReport = new ReportDocument();
        crystalReport.Refresh();
        //crystalReport.PrintToPrinter(1, true, 1, 1);
        //crystalReport.PrintOptions.PrinterName = ddlPrinterName.SelectedItem.Text;
        //crystalReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
        crystalReport.PrintToPrinter(1, true, 1, crystalReport.FormatEngine.GetLastPageNumber(new CrystalDecisions.Shared.ReportPageRequestContext()));
        return StrMsg.ToJSON();

    }

    protected void SetPaperSize()
    {
        ddlPaperSize.Items.Clear();
        ddlPaperSize.Items.Add("Page - A4");
        
    }

    //private bool GetPrinterList()
    //{
    //    ddlPrinterName.Items.Clear();
    //    bool retVal = false;
    //    PrintDocument printtdoc = new PrintDocument();
    //    //prt.PrinterSettings.PrinterName returns the name of the Default Printer
    //    string strDefaultPrinterName = printtdoc.PrinterSettings.PrinterName;
    //    //this will loop through all the Installed printers and add the Printer Names to a ComboBox.
    //    foreach (String strPrinter in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
    //    {
    //        ddlPrinterName.Items.Add(strPrinter);

    //        //This will set the ComboBox Index where the Default Printer Name matches with the current Printer Name returned by for loop
    //        if (strPrinter.CompareTo(strDefaultPrinterName) == 0)
    //        {
    //            ddlPrinterName.Text = strDefaultPrinterName;

    //            retVal = true;
    //        }
    //    }

    //    return retVal;
    //}

    protected void Page_Unload(object sender, EventArgs e)
    {
        //if (this.crystalReport != null)
        //{
        this.crystalReport.Close();
        this.crystalReport.Dispose();
        //}
    }

    public void OnConfirmprint(object sender, EventArgs e)
    {
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {

            ReportDocument crystalReport = new ReportDocument();
            bool ReportStatus = BindReport(crystalReport);
            if (ReportStatus == true)
            {

                ExportFormatType formatType = ExportFormatType.NoFormat;
                switch (rbFormat.SelectedItem.Value)
                {
                    case "Excel":
                        formatType = ExportFormatType.Excel;
                        break;
                    case "PDF":
                        formatType = ExportFormatType.PortableDocFormat;
                        break;
                }

                crystalReport.ExportToHttpResponse(formatType, Response, false, PDFRptName);

                Response.End();

            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Record Found!!.')</script>");

            }
        }
        

    }

    //public void OnConfirm(object sender, EventArgs e)
    //{
    //    string confirmValue = Request.Form["confirm_value"];
    //    if (confirmValue == "Yes")
    //    {
    //        crystalReport.Refresh();
    //        crystalReport.PrintOptions.PrinterName = ddlPrinterName.SelectedItem.Text;


    //        if (ddlPaperSize.SelectedItem.Text == "Page - A4")
    //        {
    //            crystalReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
    //            crystalReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
    //            crystalReport.PrintToPrinter(1, true, 1, crystalReport.FormatEngine.GetLastPageNumber(new CrystalDecisions.Shared.ReportPageRequestContext()));
    //        }
    //        crystalReport.Refresh();
    //    }
        
    //}

    [WebMethod]

    public static string Report_Print1(String confirmvalue, String StrPaperSize, String StrPaperName)
    {
        if (confirmvalue == "Yes")
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
            crystalReport.Refresh();
            crystalReport.PrintOptions.PrinterName = StrPaperName;


            if (StrPaperSize == "Page - A4")
            {
                crystalReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                crystalReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
                crystalReport.PrintToPrinter(1, true, 1, crystalReport.FormatEngine.GetLastPageNumber(new CrystalDecisions.Shared.ReportPageRequestContext()));
            }
            crystalReport.Refresh();
        }
        String Msg = "Salary Generation Has Been Completed Successfully";
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);

    }

}