﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="SalaryGenerationInfo.aspx.cs" Inherits="SalaryGenerationInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Salary Generation Info</h2>						
						</section>
                        <table width="98%"   style="border:solid 2px lightblue;  "  >
                            <tr>
                                <td style="padding: 5px; width:450px;" align="right"><span class="headFont">Pay Month &nbsp;&nbsp;</span> </td>
                                <td style="padding: 5px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Enabled="false" ></asp:TextBox>
                                </td>

                            </tr>
                    <tr>
                        <td style="padding:15px;" colspan="3">
                            <asp:GridView ID="grvSalInfo" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="SectorCode" EmptyDataText="No Data Found." AllowPaging="false" PageSize="10"
                                CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">

                                <AlternatingRowStyle BackColor="#FFFACD" />
                                <PagerSettings FirstPageText="First" LastPageText="Last"
                                    Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                <Columns>
                                    <asp:BoundField DataField="SectorCode" HeaderText="Sector Code" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="SectorName" HeaderText="Sector Name" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

