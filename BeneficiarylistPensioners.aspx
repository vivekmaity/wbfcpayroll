﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BeneficiarylistPensioners.aspx.cs" Inherits="BeneficiarylistPensioners" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
        <script type="text/javascript">
            var ParameterTypes = "P";
            $(document).ready(function () {
                $('#rdPDF').click(function () {
                    if (document.getElementById('rdPDF').checked) {
                        ParameterTypes = "P";
                    }
                });

                $('#rdExcel').click(function () {
                    if (document.getElementById('rdExcel').checked) {
                        ParameterTypes = "E";
                    }
                });
            });

            //This is for 'Location' Binding on the Base of 'Sector'

            $(document).ready(function () {

            });


            function opentab() {
                var PenMonth;
                var CurrMonth;
                var Reporttype;
                var ReportName = '';
                var ReportType = '';
                var StrReportTyp = '';
                FormName = "BeneficiarylistPensioners.aspx";
                ReportName = 'BeneficiaryPensionerDetails';
                ReportType = "Beneficiary Pensioner Details";
                PenMonthid =0;
                CurrMonth = $('#txtPayMonth').val();
                Reporttype = $('#ddlReportType').val();
                

                if (ParameterTypes == "P") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";
                if (confirm("Do you want to view the report ?")) {
                    confirm_value.value = "Yes";
                    $(".loading-overlay").show();
                    var E = "{PenMonthid:'" + PenMonthid + "',FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                    $.ajax({
                        type: "POST",
                        url: "BeneficiarylistPensioners.aspx/Report_Paravalue",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            jsmsg = JSON.parse(msg.d);
                            $(".loading-overlay").hide();
                            window.open("ReportView1.aspx?");
                        }

                    });
                } else {
                    confirm_value.value = "No";

                }
                }
                else {
                    //var ReportName = "Arrear_bank_slip";

                    //var PaymentDate = $('#ddlArrearPayDate').find('option:selected').text();
                    window.open("AllReportinExcel.aspx?ReportName=" + ReportName);

                }

            }

            function unvalidate() {
                $("#form1").validate().currentForm = '';
                return true;
            }

            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
</script>
    <style type="text/css">
        .style1
        {
            width: 100px;
        }
        .style2
        {
            width: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Beneficiary list Pensioners</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" ><span class="headFont">Pension Month</span> </td>
                                        <td style="padding:5px;" class="style2" >:</td>
                                        <td style="padding:5px;" align="left" >
                                        <asp:TextBox ID="txtPayMonth" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false" autocomplete="off"></asp:TextBox>
                                        </td>
                                        
                                       <%--<asp:HiddenField ID="hdnPenMonthID" runat="server"  />--%>

                                        <td style="padding:5px;" class="style3" bgcolor="White"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;" bgcolor="White">:</td>
                                        <td style="padding:5px;" align="left" bgcolor="White" >
                                        <asp:DropDownList ID="ddlReportType" Width="200px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">(Select Report Type)</asp:ListItem>
                                            <asp:ListItem Value="1"> Beneficiary list Pensioners</asp:ListItem>
                                            <%--<asp:ListItem Value="2">Pay Summary</asp:ListItem>
                                            <asp:ListItem Value="3">Pay Summary Location Wise</asp:ListItem>
                                             <asp:ListItem Value="4">Pay Register</asp:ListItem>--%>
                                        </asp:DropDownList>   
                                        </td>
                                        <td style="padding: 5px; vertical-align: top;">
                                            <asp:RadioButton ID="rdPDF" Checked="true" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="PDF" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdExcel" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="Excel" Checked="false" />
                                        </td>

                                    </tr>
                                </table>

                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;"/> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                         <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Pension Generate...</div>
                                        </div>
                                        </div>
                                    </div>       
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                                     
                     
                  
                   
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

