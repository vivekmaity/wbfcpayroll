﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class KMDAMaster : System.Web.UI.MasterPage
{
    DataTable dtMenu;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_INT_USER_ID] == null)
            {
                Response.Redirect("log.aspx", false);
                return;
            }
            string Res = CheckValidPage();
            if (Res == "Yes")
            {

            }

            else
            {
                Response.Redirect("log.aspx", false);
                //return;
            }
            if (!IsPostBack)
            {

            }
        }
        catch (Exception ex)
        {
            Page.RegisterStartupScript("Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
        
        if (!IsPostBack)
        {
            if (Session[SiteConstants.SSN_INT_USER_ID] != null)
            {
                DataTable dt = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
                if (dt.Rows.Count > 0)
                {
                    ddlSector.DataSource = dt;
                    ddlSector.DataBind();
                    if(dt.Rows.Count ==1)
                        ddlSector.SelectedValue = dt.Rows[0]["SectorID"].ToString();
                    Session[SiteConstants.SSN_SECTORID] = dt.Rows[0]["SectorID"].ToString();
                }
            }
        }
        string AccFinYear = ""; string SalFinYear = "";
        string Today = DateTime.Now.ToString("dd/MM/yyyy");
        int year = Convert.ToInt32(Today.Substring(6, 4));
        int month = Convert.ToInt32(Today.Substring(3,2));

        //This is for Account Financial Year
        if (month > 3)
        {
            AccFinYear = Convert.ToString(year + "-" + (year + 1));
        }
        else
        {
            AccFinYear = Convert.ToString((year - 1) + "-" + year);
        }
        txtAccFinYear.Text = AccFinYear;

        //This is for Salary Financial Year
        if (month > 2)
        {
            SalFinYear = Convert.ToString(year + "-" + (year + 1));
        }
        else
        {
            SalFinYear = Convert.ToString((year - 1) + "-" + year);
        }
        txtAccFinYear.Text = AccFinYear;
        txtSalFinYear.Text = SalFinYear;
        Session[SiteConstants.SSN_SALFIN] = txtSalFinYear.Text;
        Session[SiteConstants.SSN_SALACC] = txtAccFinYear.Text;

        Get_SalMonth();
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            //Response.Redirect("Log.aspx");
            //Response.Redirect("Log.aspx", false);
            if (HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID] == null)
            {
                //Response.Redirect("Log.aspx", false);
                Response.Redirect("http://localhost:28883/Administration/AdminHome/SelectModule");
                return;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string CheckValidPage()
    {
        string Url = ""; string PageNameWithFolderName = ""; string PageNames = ""; string Result = "";
        try
        {
            Url = HttpContext.Current.Request.Url.AbsoluteUri;
            PageNameWithFolderName = HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oFileInfo = new System.IO.FileInfo(PageNameWithFolderName);
            PageNames = oFileInfo.Name;
            dtMenu = DBHandler.GetResult("Get_Menu", Session[SiteConstants.SSN_INT_USER_ID]);
            for (int i = 0; i < dtMenu.Rows.Count; i++)
            {
                if (PageNames == dtMenu.Rows[i]["PageName"].ToString())
                    Result = "Yes";
                else if(PageNames == "CoOperativeCalculation.aspx")
                    Result = "Yes";
                else if (PageNames == "ChangePassword.aspx")
                    Result = "Yes";
                else if (PageNames == "Pensioner.aspx")
                    Result = "Yes";
                else if (PageNames == "PensionerCalculation.aspx")
                    Result = "Yes";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Result;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateUserLockFlag()
    {
        string JSonVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Update_UserLoginLockFlag", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            //if (dt.Rows.Count > 0)
            //{
            //    string Result = dt.Rows[0]["Result"].ToString();
            //    if (Result == "Updated")
            //    {
            //        JSonVal = Result.ToJSON();
            //    }
            //    else
            //    {
            //        string Rest = "NoUpdated";
            //        JSonVal = Rest.ToJSON();
            //    }
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSonVal;
    }

    protected void Get_SalMonth()
    {
        try
        {
            
            DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", ddlSector.SelectedValue, txtSalFinYear.Text);
            DataTable dtSalMonth = ds1.Tables[0];
            DataTable dtSalMonthID = ds1.Tables[0];
            if (dtSalMonth.Rows.Count > 0)
            {
                txtPayMonths.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
                Session[SiteConstants.SSN_PAYMONTHID] = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();
            }
        }
        catch (Exception exx)
        {
            throw new Exception(exx.Message);
        }
    }

}
