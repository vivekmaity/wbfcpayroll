﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" ViewStateEncryptionMode="Always" AutoEventWireup="true"
    CodeFile="MenuMaster.aspx.cs" Inherits="MenuMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        
        $(document).ready(function () {
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
        });

        function Blank_Menu(){
            $('#txtMenuName').val(''); $('#txtParentMenu').val(''); $('#hdnParentMenuID').val(''); $('#txtPageName').val('');
            $('#txtRank').val(''); $('#txtModuleCode').val(''); $('#txtMenuType').val('');
        }
        function Blank_User() {
            $('#txtMenuCode').val(''); $('#hdnMenuCodeID').val(''); $('#txtUserID').val(''); $('#hdnUserID').val('');
        }

        $(document).ready(function () {
            $("#txtParentMenu").autocomplete({
                source: function (request, response) {
                    var E = "{txtParentMenu:'" + $("#txtParentMenu").val().trim() +"'}";
                    $.ajax({
                        type: "POST",
                        url: "MenuMaster.aspx/AutoComplete_ParentMenu",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        }
                    });
                },
                appendTo: "#AutoComplete-ParentMenu",
                select: function (e, i) {
                    $("#hdnParentMenuID").val(i.item.val);
                },
                minLength: 0
            }).click(function () {
                $(this).autocomplete('search', ($(this).val()));
            });
        });

        $(document).ready(function () {
            $("#txtMenuCode").autocomplete({
                source: function (request, response) {
                    var E = "{txtMenuCode:'" + $("#txtMenuCode").val().trim() + "'}";
                    $.ajax({
                        type: "POST",
                        url: "MenuMaster.aspx/AutoComplete_MenuCode",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        }
                    });
                },
                appendTo: "#AutoComplete-MenuCode",
                select: function (e, i) {
                    $("#hdnMenuCodeID").val(i.item.val);
                },
                minLength: 0
            }).click(function () {
                $(this).autocomplete('search', ($(this).val()));
            });
        });

        $(document).ready(function () {
            $("#txtUserID").autocomplete({
                source: function (request, response) {
                    var E = "{txtUserID:'" + $("#txtUserID").val().trim() + "'}";
                    $.ajax({
                        type: "POST",
                        url: "MenuMaster.aspx/AutoComplete_UserID",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        }
                    });
                },
                appendTo: "#AutoComplete-User",
                select: function (e, i) {
                    $("#hdnUserID").val(i.item.val);
                },
                minLength: 0
            }).click(function () {
                $(this).autocomplete('search', ($(this).val()));
            });
        });

        $(document).ready(function () {
            $("#txtSearchMenu").autocomplete({
                source: function (request, response) {
                    var E = "{txtMenuCode:'" + $("#txtSearchMenu").val().trim() + "'}";
                    $.ajax({
                        type: "POST",
                        url: "MenuMaster.aspx/AutoComplete_MenuCode",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        }
                    });
                },
                appendTo: "#AutoComplete-SearchMenu",
                select: function (e, i) {
                    $("#hdnSearchMenuID").val(i.item.val);
                    SearchSelectMenu();
                    $("#cmdSaveMenu").attr('value', 'Update Menu');
                },
                minLength: 0
            }).click(function () {
                $(this).autocomplete('search', ($(this).val()));
            });
        });

        function SearchSelectMenu() {
            var E = "{hdnSearchMenuID:'" + $('#hdnSearchMenuID').val().trim() + "'}";
            $.ajax({
                type: "POST",
                url: 'MenuMaster.aspx/Search_SelectMenu',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var xmlDoc = $.parseXML(D.d);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    $('#txtMenuName').val($(Table).find("MenuName").text());
                    $('#txtParentMenu').val($(Table).find("ParentMenuName").text());
                    $('#hdnParentMenuID').val($(Table).find("ParentMenuCode").text());
                    $('#txtPageName').val($(Table).find("PageName").text());
                    $('#txtRank').val($(Table).find("Rank").text());
                    $('#txtModuleCode').val($(Table).find("ModuleCode").text());
                    $('#txtMenuType').val($(Table).find("MenuType").text());

                    $("#GridUser tbody tr").empty();
                    $(xml).find("Table1").each(function () {
                        $("#GridUser tbody").append("<tr><td style='display:none'> " +
                            $(this).find("UserID").text() + "</td><td>" +
                            $(this).find("UserName").text() + "</td><td>" +
                            "<div align='center'><img src='images/Edit.png' onclick=Edit(this) />" + "</td><td>" +
                            "<div align='center'><img src='images/delete.png' onclick=Delete(this) />" + "</td></tr>");
                    });
                },
                error: function (response) {
                    alert("Error Occured"); return false;
                }
            });
        }

        function Edit(t) {

        }

        function Delete(t) {
            var conf = confirm("Are you sure want to DELETE this Record ?");
            if (conf == true) {
                var userid = $(t).closest('tr').find('td:eq(0)').text(); //alert(b);
                var E = "{userid:'" + userid.trim() + "', hdnSearchMenuID:'" + $('#hdnSearchMenuID').val().trim() + "'}"; //alert(E);
                $.ajax({
                    type: "POST",
                    url: 'MenuMaster.aspx/DeleteUser',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var strmsg = data.d;
                        alert(strmsg);
                        SearchSelectMenu();
                        return false;
                    },
                    error: function (response) {
                        alert("Error Occured"); return false;
                    }
                });
            }
        }

        $(document).on('click', '#cmdRefreshMenu', function () {
            Blank_Menu(); return false;
        });
        $(document).on('click', '#cmdRefreshUser', function () {
            Blank_User(); return false;
        });

        $(document).on('click', '#cmdSaveMenu', function () {
            var E = ''; var URL = "";
            if ($('#txtMenuName').val().trim() == '') { alert("Please Enter Menu Name !!"); return false; $('#txtMenuName').focus(); }
            //if ($('#txtParentMenu').val().trim == '' || $('#hdnParentMenuID').val().trim == '') { alert("Please select Parent Menu Name !!"); return false; $('#txtParentMenu').focus(); }
            if ($('#txtPageName').val().trim() == '') { alert("Please Enter Page Name !!"); return false; $('#txtPageName').focus(); }

            if ($("#cmdSaveMenu").attr('value') == 'Save Menu') {
                E = "{txtMenuName:'" + $('#txtMenuName').val().trim() + "', hdnParentMenuID:'" + $('#hdnParentMenuID').val().trim() + "', txtPageName:'" + $('#txtPageName').val().trim()
                    + "', txtRank:'" + $('#txtRank').val().trim() + "', txtModuleCode:'" + $('#txtModuleCode').val().trim() + "', txtMenuType:'" + $('#txtMenuType').val().trim() + "'}";

                URL = 'MenuMaster.aspx/SaveMenu';
            }
            else {
                E = "{txtMenuName:'" + $('#txtMenuName').val().trim() + "', hdnParentMenuID:'" + $('#hdnParentMenuID').val().trim() + "', txtPageName:'" + $('#txtPageName').val().trim()
                    + "', txtRank:'" + $('#txtRank').val().trim() + "', txtModuleCode:'" + $('#txtModuleCode').val().trim() + "', txtMenuType:'" + $('#txtMenuType').val().trim() + "', hdnSearchMenuID:'" + $('#hdnSearchMenuID').val().trim() + "'}";

                URL = 'MenuMaster.aspx/UpdateMenu';
            }
                $.ajax({
                    type: "POST",
                    url: URL,
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == "1") {
                            alert("Record Save Successfully !");
                            Blank_Menu();
                            $("#GridUser tbody").empty();
                            $("#cmdSaveMenu").attr('value', 'Save Menu');
                            $("#txtSearchMenu").val(''); $("#hdnSearchMenuID").val('');
                            return false;
                        }
                        if (data.d == "0") {
                            alert("Error Occured"); return false;
                        }
                    },
                    error: function (response) {
                        alert("Error Occured"); return false;
                    }
                });
            
        });
        $(document).on('click', '#cmdSaveUser', function () {
            if ($('#txtMenuCode').val().trim() == '' || $('#hdnMenuCodeID').val().trim() == '') { alert("Please choose Menu Code !!"); return false; $('#txtMenuCode').focus(); }
            if ($('#txtUserID').val().trim() == '' || $('#hdnUserID').val().trim() == '') { alert("Please choose UserID !!"); return false; $('#txtUserID').focus(); }

            var E = "{hdnMenuCodeID:'" + $('#hdnMenuCodeID').val().trim() + "', hdnUserID:'" + $('#hdnUserID').val().trim() + "'}";
            $.ajax({
                type: "POST",
                url: 'MenuMaster.aspx/SaveUser',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "1") {
                        alert("Record Save Successfully !");
                        Blank_User();
                        return false;
                    }
                    if (data.d == "0") {
                        alert("Error Occured"); return false;
                    }
                },
                error: function (response) {
                    alert("Error Occured"); return false;
                }
            });
        });

        $(document).on('click', '#btnParentSearch', function () {
            //if ($('#txtParentMenu').val().trim == '' || $('#hdnParentMenuID').val().trim == '') { alert("Please select Parent Menu for Search !!"); return false; $('#txtParentMenu').focus(); }

            var E = "{hdnParentMenuID:'" + $('#hdnParentMenuID').val().trim() + "'}";
            $.ajax({
                type: "POST",
                url: 'MenuMaster.aspx/parent_Details',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $("#divCh").show();
                    var xmlDoc = $.parseXML(data.d);
                    var xml = $(xmlDoc);
                    //var Table = xml.find("Table");

                    $("#GridView1").empty();
                    $("#GridView1").append("<tr><th>Menu Name</th><th>Rank</th><th>PageName</th><th>Menu Type</th></tr>");//
                    $(xml).find("Table").each(function () {
                        
                        $("#GridView1").append("<tr class='trclass'><td align='center'>" +
                            $(this).find("MenuName").text() + "</td> <td>" +
                            $(this).find("Rank").text() + "</td> <td>" +
                            $(this).find("PageName").text() + "</td> <td>" +
                            $(this).find("MenuType").text() + "</td></tr>");
                    });
                },
                error: function (response) {
                    alert("Error Occured"); return false;
                }
            });
        });

        $(document).on('click', '#closeid', function () {
            $("#divCh").hide();
            $('#txtParentMenu').val(''); $('#hdnParentMenuID').val('');
        })
        $(document).on('click', '#btnRefreshSearch', function () {
            window.location.href = "MenuMaster.aspx";
        })

    </script>
    <style type="text/css">
        .style7
        {
            height: 31px;
        }
        .style16
        {
            height: 31px;
            width: 129px;
        }
        .style17
        {
            width: 129px;
        }
        .style18
        {
            width: 97px;
        }
        .style19
        {
            width: 341px;
        }
        .style20
        {
            height: 31px;
            width: 341px;
        }
        .style21
        {
            height: 16px;
            width: 129px;
        }
        .style22
        {
            height: 16px;
        }
        .style23
        {
            height: 16px;
            width: 341px;
        }
        .style24
        {
            width: 97px;
            height: 16px;
        }
        .auto-style1 {
            width: 219px;
        }
        .auto-style2 {
            width: 219px;
            height: 16px;
        }
        #AutoComplete-ParentMenu, #AutoComplete-MenuCode, #AutoComplete-User, #AutoComplete-SearchMenu {
            display: block;
            position: relative;
        }

        .ui-autocomplete {
            height: 200px;
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #cbc7c7;
            font-size: 13px;
            font-weight: normal;
            color: #242424;
            background: #fff;
            -moz-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            -webkit-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            box-shadow: inset 0.9px 1px 3px #e4e4e4;
            width: 350px;
            /*height:30px;*/
            padding: 5px;
            font-family: Segoe UI, Lucida Grande, Arial, Helvetica, sans-serif;
            margin: 3px 0;
        }
    </style>
</asp:Content>
          
<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>menu master</h2>						
						</section>
                        <table width="98%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 15px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td colspan="9" style="padding: 5px; width:100%; background: #deedf7;"><span class="headFont" style="font-weight: bold;"><font size="3px">Search Menu :</font></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;" class="style1" bgcolor="White"><span class="headFont">Menu Name&nbsp;&nbsp;</span> </td>
                                            <td style="padding: 5px;" bgcolor="White">:</td>
                                            <td style="padding: 5px;" align="left" class="style2" bgcolor="White">
                                                <asp:TextBox ID="txtSearchMenu" autocomplete="off" autofocus="true" placeholder="--AutoComplete--" runat="server" CssClass="textbox"></asp:TextBox>
                                                <asp:Button id="btnRefreshSearch" Text="Refresh" Width="57px" runat="server" ClientIDMode="Static" CssClass="Btnclassname DefaultButton"/>
                                                <asp:HiddenField ID="hdnSearchMenuID" runat="server" Value="" ClientIDMode="Static" />
                                                <div id="AutoComplete-SearchMenu"></div>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                    </table>

                                    <table align="center" width="100%">
                                        <tr>
                                            <td colspan="9" style="padding: 5px; background: #deedf7;"><span class="headFont" style="font-weight: bold;"><font size="3px">Menu Entry :</font></span>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="padding: 5px;" class="style1" bgcolor="White"><span class="headFont">Menu Name  &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 5px;" bgcolor="White">:</td>
                                            <td style="padding: 5px;" align="left" class="style2" bgcolor="White">
                                                <asp:TextBox ID="txtMenuName" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                                            </td>

                                            <td style="padding: 5px;" class="style1" bgcolor="White"><span class="headFont">Parent Menu&nbsp;&nbsp;</span></td>
                                            <td style="padding: 5px;" bgcolor="White">:</td>
                                            <td style="padding: 5px;" align="left" class="style2" bgcolor="White">
                                                <asp:TextBox ID="txtParentMenu" autocomplete="off" placeholder="--AutoComplete--" runat="server" CssClass="textbox"></asp:TextBox>
                                                <asp:Button id="btnParentSearch" Text="Search" Width="50px" runat="server" ClientIDMode="Static" CssClass="Btnclassname DefaultButton"/>
                                                <asp:HiddenField ID="hdnParentMenuID" runat="server" Value="" ClientIDMode="Static" />
                                                <div id="AutoComplete-ParentMenu"></div>
                                            </td>

                                            <td style="padding: 5px;" width="170px">
                                                <span class="headFont">Page Name&nbsp;&nbsp;</span><span class="require">*</span>
                                            </td>
                                            <td style="padding: 5px;">:</td>
                                            <td style="padding: 5px;" align="left">
                                                <asp:TextBox ID="txtPageName" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="padding: 5px;" class="style1" bgcolor="White"><span class="headFont">Rank&nbsp;&nbsp;</span> </td>
                                            <td style="padding: 5px;" bgcolor="White">:</td>
                                            <td style="padding: 5px;" align="left" class="style2" bgcolor="White">
                                                <asp:TextBox ID="txtRank" runat="server" autocomplete="off" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                                            </td>

                                            <td style="padding: 5px;" class="style1" bgcolor="White"><span class="headFont">Module Code&nbsp;&nbsp;</span> </td>
                                            <td style="padding: 5px;" bgcolor="White">:</td>
                                            <td style="padding: 5px;" align="left" class="style2" bgcolor="White">
                                                <asp:TextBox ID="txtModuleCode" runat="server" autocomplete="off" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                                            </td>

                                            <td style="padding: 5px;" width="170px">
                                                <span class="headFont">Menu Type&nbsp;&nbsp;</span><span class="require">*</span>
                                            </td>
                                            <td style="padding: 5px;">:</td>
                                            <td style="padding: 5px;" align="left">
                                                <asp:TextBox ID="txtMenuType" runat="server" autocomplete="off" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="9" style="padding: 5px;" align="left">
                                                <div id="Div1" style="display: normal; float: left; margin-left: 450px;">
                                                    <asp:Button ID="cmdSaveMenu" runat="server" Text="Save Menu"
                                                        Width="100px" CssClass="Btnclassname DefaultButton" />
                                                    <asp:Button ID="cmdRefreshMenu" runat="server" Text="Refresh"
                                                        Width="100px" CssClass="Btnclassname DefaultButton" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table align="center" width="100%">
                                        <tr>
                                            <td colspan="9">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9" style="padding: 5px; background: #deedf7;"><span class="headFont" style="font-weight: bold;"><font size="3px">User Permission :</font></span>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td style="padding: 5px;" width="170px">
                                                <span class="headFont">Menu Name&nbsp;&nbsp;</span><span class="require">*</span>
                                            </td>
                                            <td style="padding: 5px;">:
                                            </td>
                                            <td style="padding: 5px;" align="left">
                                                <asp:TextBox ID="txtMenuCode" autocomplete="off" placeholder="--AutoComplete--" runat="server" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                                                <asp:HiddenField ID="hdnMenuCodeID" runat="server" Value="" ClientIDMode="Static" />
                                                <div id="AutoComplete-MenuCode"></div>
                                            </td>
                                            <td style="padding: 5px;" width="170px">
                                                <span class="headFont">UserID&nbsp;&nbsp;</span><span class="require">*</span>
                                            </td>
                                            <td style="padding: 5px;">:</td>
                                            <td style="padding: 5px;" align="left">
                                                <asp:TextBox ID="txtUserID" autocomplete="off" placeholder="--AutoComplete--" runat="server" ClientIDMode="Static" CssClass="textbox"></asp:TextBox>
                                                <asp:HiddenField ID="hdnUserID" runat="server" Value="" ClientIDMode="Static" />
                                                <div id="AutoComplete-User"></div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="9" style="padding: 5px;" align="left">
                                                <div id="Div2" style="display: normal; float: left; margin-left: 450px;">
                                                    <asp:Button ID="cmdSaveUser" runat="server" Text="Save Menu Permission"
                                                        Width="150px" CssClass="Btnclassname DefaultButton" />
                                                    <asp:Button ID="cmdRefreshUser" runat="server" Text="Refresh"
                                                        Width="100px" CssClass="Btnclassname DefaultButton" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                                <div style="overflow-y: scroll; height: 270px;" id="divGrid">
                                                    <table id="GridUser" class="Grid" cellspacing="0" rules="all" border="1" style="width: 100%; border-collapse: collapse; text-align: center;">
                                                        <thead>
                                                            <tr class="trclass">
                                                                <%--<th>UserID</th>--%>
                                                                <th>UserName</th>
                                                                <th>Edit(Not Working Now)</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;" align="left">

                                                <%--<div id="sh" style="float: left; margin-left: 450px;">
                                                    <asp:Button ID="cmdSave" runat="server" Text="Save"
                                                        Width="100px" CssClass="Btnclassname" OnClientClick="Save(); return false;"
                                                        BorderColor="#669999" />
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Reset"
                                                        Width="100px" CssClass="Btnclassname" OnClick="cmdReset_Click" OnClientClick='javascript: return unvalidate();' />
                                                </div>--%>
                                                <div class="loading-overlay">
                                                    <div class="loadwrapper">
                                                        <div class="ajax-loader-outer">Loading...</div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>

    <div class="content-overlay" id='divCh'>
    <div class="wrapper-outer">
        <div class="wrapper-inner">
            <div class="loadContent">
                <div id="closeid" class="close-content"><a href="javascript:void(0);" id="a1" style="color: red;">Close</a></div>
                <%--<table cellpadding="5" style="border: solid 1px lightblue; padding-bottom: 10px; width: 100%; overflow: scroll;" cellspacing="0">--%>
                <table cellpadding="5" style="border: solid 1px lightblue; padding-bottom: 10px; width: 100%; overflow: scroll;" cellspacing="0">
                    <tr>
                        <td>
                    <div style="overflow: scroll; height:300px">
                            <asp:GridView ID="GridView1" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" >
                                <AlternatingRowStyle BackColor="#FFFACD" />
                                <Columns>
                                    <asp:BoundField HeaderText="Menu Name" />
                                    <asp:BoundField HeaderText="Rank" />
                                    <asp:BoundField HeaderText="PageName" />
                                    <asp:BoundField HeaderText="Menu Type" />
                                </Columns>
                            </asp:GridView>
                        </div>
                            </td>
                        </tr>
                    </table>
                
                <%--<table style="width:100%">
                    <tr>
                        <td style="padding: 5px;" align="left">
                            <div style="float: left; margin-left: 450px;">
                                <asp:Button ID="btnok" runat="server" Text="OK" Width="70px" CssClass="Btnclassname DefaultButton"/>
                            </div>                   
                        </td>
                    </tr>
                </table>--%>
                <%--</table>--%>
            </div>
        </div>
    </div>
</div>

</asp:Content>

