﻿$(document).ready(function () {
    $("#txtEmployeeName").autocomplete({
        source: function (request, response) {
            var E = "{EmpName: '" + $("#txtEmployeeName").val() + "'}";
            $.ajax({
                type: "POST",
                url: "MediClaim.aspx/GetEmployeeNameAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                },
              

            });
        },
        appendTo: "#AutoComplete-EmployeeName",
        select: function (e, i) {
            $("#hdnIEmployeeId").val(i.item.val);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    //***************************Search Component*********************************
    $("#txtEmployeeNameSearch").autocomplete({
        source: function (request, response) {
            var E = "{EmpName: '" + $("#txtEmployeeNameSearch").val() + "'}";
            $.ajax({
                type: "POST",
                url: "MediClaim.aspx/GetEmployeeNameAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                },


            });
        },
        appendTo: "#AutoComplete-EmployeeName_Search",
        select: function (e, i) {
            $("#hdnIEmployeeId_Search").val(i.item.val);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    //****************************************************************************
    $(function () {
        $("[id$=txtMediClaimStartDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });

    $('#txtMediClaimStartDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });

    $("#btnLDSDatePicker").click(function () {
        $('#txtMediClaimStartDate').datepicker("show");
        return false;
    });
    $("#txtMediClaimAmount").keyup(function () {
        if ($("#txtInstallMentNO").val() != "" || $("#txtInstallMentNO").val() != 0) {
            CalculateInstallmentDetailsAuto();
        }
    });
    $("#txtInstallMentNO").keyup(function () {
        if ($("#txtMediClaimAmount").val() > 0)
        {
            CalculateInstallmentDetailsAuto();
        }
    });
    $("#btnSave").click(function (e) {
        e.preventDefault();
        var Status1 = validateBeforeSave();
        if (Status1 != true) {
            return false;
        }
        var Status2 = ValidateAmountBeforeSave();
        if (Status2 != true) {
            return false;
        }
        var Status3 = ValidateNegativeAmount();
        if (Status3 != true) {
            return false;
        }
        if (confirm("Do you want to save !")) {
            SaveMediClaim();
        }
    });
    $("#btnSearch").click(function (e) {
        e.preventDefault();
        if($("#txtEmployeeNameSearch").val()=="" || $("#txtEmployeeNameSearch").val()==null)
        {
            alert("Please Select Employee first!");
            $("#txtEmployeeNameSearch").focus();
            return false;
        }
        GetDetailsForEdit();
    });
    $(document).on('click', ".btnEdit", function () {
        var currentrow = $(this).parent().parent();
        var EmpName = currentrow.find(".EmpName").text();
        var MediclaimNo = currentrow.find(".MediclaimNo").text();
        var LoanAmount = currentrow.find(".LoanAmount").text();
        var LoanDeducStartDate = currentrow.find(".LoanDeducStartDate").text();
        var TotLoanInstall = currentrow.find(".TotLoanInstall").text();
        var CurLoanInstall = currentrow.find(".CurLoanInstall").text();
        var LoanAmountPaid = currentrow.find(".LoanAmountPaid").text();
        var inst_no1 = currentrow.find(".inst_no1").text();
        var inst_amt1 = currentrow.find(".inst_amt1").text();
        var inst_no2 = currentrow.find(".inst_no2").text();
        var inst_amt2 = currentrow.find(".inst_amt2").text();
        if (parseInt(CurLoanInstall) > 0 || parseFloat(LoanAmountPaid) > 0) {
            alert("Mediclaim Deduction Started. So you can not edit this!");
            return false;
        }
        else {

            $("#txtEmployeeName").val(EmpName);
            $("#txtMediClaimNo").val(MediclaimNo);
            $("#txtMediClaimStartDate").val(LoanDeducStartDate);
            $("#txtMediClaimAmount").val(LoanAmount);
            $("#txtInstallMentNO").val(TotLoanInstall);
            $("#txtCurrentMediclaimInstallmentNo").val(CurLoanInstall);
            $("#txtMediclaimAmountPaid").val(LoanAmountPaid);
            $("#txtSlabInsatallNoIst").val(inst_no1);
            $("#txtSlabInsatallAmountIst").val(inst_amt1);
            $("#txtSlabInsatallNoScnd").val(inst_no2);
            $("#txtSlabInsatallAmountScnd").val(inst_amt2);
            $("#btnSave").attr('value', 'Update');
            $("#txtEmployeeName").attr("disabled", "disabled");
        }
    });
    $("#btnCancel,#btnReset").click(function (e) {
        e.preventDefault();
        window.location.href="MediClaim.aspx";
    });

});
    function isNumber(evt) {
        if (event.which != 46 && (event.which <= 47 || event.which >= 59)) {
            event.preventDefault();
            return false;
            if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
                event.preventDefault();
                return false;
            }
        }
        return true;
    }
    function CalculateInstallmentDetailsAuto() {

        var calculationAmount1stSlab = 0;
        var RemainderAmount = 0;
        var TotalInstallMentNo = ($("#txtInstallMentNO").val() == "" ? 0 : parseFloat($("#txtInstallMentNO").val()));
        var TotalAmount = ($("#txtMediClaimAmount").val() == "" ? 0 : parseFloat($("#txtMediClaimAmount").val()));

        calculationAmount1stSlab = parseFloat(TotalAmount) / parseFloat(TotalInstallMentNo);
        RemainderAmount = parseFloat(TotalAmount) % parseFloat(TotalInstallMentNo);
        if (RemainderAmount != 0) {
            //**********************1St Slab***********************
            $("#txtSlabInsatallNoIst").val(parseFloat(TotalInstallMentNo) - 1);
            $("#txtSlabInsatallAmountIst").val(Math.round(calculationAmount1stSlab));
            //**********************2nd Slab***********************
            $("#txtSlabInsatallNoScnd").val(parseFloat(1));
            var restInstallNo = parseFloat(TotalInstallMentNo) - 1;
            var s = (Math.round(parseFloat(calculationAmount1stSlab)) * parseFloat(restInstallNo));
            var ScndPAmount = Math.round(parseFloat(TotalAmount) - parseFloat(s));
            $("#txtSlabInsatallAmountScnd").val(ScndPAmount);
        }
    
        else {
            //*************************1St Slab********************
            $("#txtSlabInsatallNoIst").val(parseFloat(TotalInstallMentNo));
            $("#txtSlabInsatallAmountIst").val(Math.round(calculationAmount1stSlab));

            //**********************2nd Slab***********************
            $("#txtSlabInsatallNoScnd").val(0);
            $("#txtSlabInsatallAmountScnd").val(0);
        }

    }
    function validateBeforeSave() {
        var Status = true;
        if ($("#hdnLoanId").val() == "" || $("#hdnLoanId").val() == null) {
            if ($("#hdnIEmployeeId").val() == "" || $("#hdnIEmployeeId").val() == null) {
                alert("Please select beneficiary !");
                $("#txtEmployeeName").focus();
                Status = false;
            }
        }
        else if ($("#txtMediClaimAmount").val() == 0 || $("#txtMediClaimAmount").val() == "") {
            alert("Please enter MediClaim Amount !");
            $("#txtMediClaimAmount").focus();
            Status = false;
        }
        else if ($("#txtMediClaimStartDate").val() == 0 || $("#txtMediClaimStartDate").val() == "") {
            alert("Please select MediClaim Start Date !");
            $("#txtMediClaimStartDate").focus();
            Status = false;
        }
        else if ($("#txtInstallMentNO").val() == 0 || $("#txtInstallMentNO").val() == "") {
            alert("Please enter Installment No !");
            $("#txtInstallMentNO").focus();
            Status = false;
        }
        else if ($("#txtAccFinYear").val() == "" || $("#txtAccFinYear").val() == null) {
            alert("Please select Financial Year !");
            Status = false;
        }
        return Status;
    }
    function ValidateAmountBeforeSave() {
        var Status = true;
        var NetDisburseMediclaimAmount = $("#txtMediClaimAmount").val();
        var IStSlabInstallNo = $("#txtSlabInsatallNoIst").val();
        var IStSlabIstallAmout = $("#txtSlabInsatallAmountIst").val();
        var ScndSlabInstallNo = $("#txtSlabInsatallNoScnd").val();
        var ScndSlabIstallAmout = $("#txtSlabInsatallAmountScnd").val();

        var TotalAmount = (parseFloat(IStSlabIstallAmout) * parseFloat(IStSlabInstallNo)) + (parseFloat(ScndSlabIstallAmout) * parseFloat(ScndSlabInstallNo));
        if (NetDisburseMediclaimAmount != TotalAmount)
        {
            alert("Disburse mediclaim amount should be same with total slab amount");
            Status = false;
        }
        return Status;
    }
    function ValidateNegativeAmount() {
        var Status = true;
        var IStSlabIstallAmout =parseFloat($("#txtSlabInsatallAmountIst").val());
        var ScndSlabIstallAmout = parseFloat($("#txtSlabInsatallAmountScnd").val());
        if (IStSlabIstallAmout<0)
        {
            alert("Slab Amount should not be negative !");
            Status = false;
        }
        if (ScndSlabIstallAmout < 0) {
            alert("Slab Amount should not be negative !");
            Status = false;
        }
        return Status;
    };
    function BindFormData() {
        var objData = {};
        if ($("#hdnLoanId").val() == "" || $("#hdnLoanId").val() == null) {
            objData.LoanId = null;
            objData.EmployeeId = $("#hdnIEmployeeId").val();
        }
        else {
            objData.LoanId = $("#hdnLoanId").val();
            objData.EmployeeId = $("#hdnIEmployeeId_Search").val();
        }
        objData.MediclaimNo = $("#txtMediClaimNo").val();
        objData.MediClaimStartDate = $("#txtMediClaimStartDate").val();
        objData.MediClaimAmount = $("#txtMediClaimAmount").val() == "" ? 0 : $("#txtMediClaimAmount").val();
        objData.InstallMentNO = $("#txtInstallMentNO").val() == "" ? 0 : $("#txtInstallMentNO").val();
        objData.SlabInsatallNoIst = $("#txtSlabInsatallNoIst").val() == "" ? 0 : $("#txtSlabInsatallNoIst").val();
        objData.SlabInsatallAmountIst = $("#txtSlabInsatallAmountIst").val() == "" ? 0 : $("#txtSlabInsatallAmountIst").val();
        objData.SlabInsatallNoScnd = $("#txtSlabInsatallNoScnd").val() == "" ? 0 : $("#txtSlabInsatallNoScnd").val();
        objData.SlabInsatallAmountScnd = $("#txtSlabInsatallAmountScnd").val() == "" ? 0 : $("#txtSlabInsatallAmountScnd").val();
        objData.AccFinYear = $("#txtAccFinYear").val();

        return objData;
    };
    function SaveMediClaim() {
    
        var frmData = BindFormData();
        $.ajax({
            type: "POST",
            url: 'MediClaim.aspx/SaveMediclaimDetails',
            data: JSON.stringify({ param: frmData }),
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d);
                if (data.MessageCode == 1) {
                    alert(data.Message);
                    $("#btnSave").attr('value', 'Save');
                    window.location.href = "MediClaim.aspx";
                }
                if (data.MessageCode == 0) {
                    alert(data.Message);
                }
            }
        });
    };
    function GetDetailsForEdit() {
        $("#tblDetailsMediclaim tbody").empty();
        var data = "{ EmployeeId: '" + $("#hdnIEmployeeId_Search").val() + "',FinYear:'" + $("#txtAccFinYear").val() + "' }";
        $.ajax({
            type: "POST",
            url: 'MediClaim.aspx/GetdataForUpdate',
            data: data,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //alert(D.d.length);
                if (D.d.length > 0) {
                    var data = JSON.parse(D.d);
                    $("#hdnLoanId").val(data.LoanID);
                    $("#hdnIEmployeeId_Search").val(data.EmployeeID);
                    $("#tblDetailsMediclaim tbody").append("<tr>" +
                         "<td align='center' class='MediclaimNo'>" + data.MediclaimNo + "</td>" +
                         "<td align='center' class='EmpName'>" + data.EmpName + "</td>" +
                         "<td align='center' class='LoanAmount' >" + data.LoanAmount + "</td>" +
                         "<td align='center' class='LoanDeducStartDate'>" + data.LoanDeducStartDate + "</td>" +
                         "<td align='center' class='TotLoanInstall'>" + data.TotLoanInstall + "</td>" +
                         "<td align='center' class='CurLoanInstall'>" + data.CurLoanInstall + "</td>" +
                         "<td align='center' class='LoanAmountPaid'>" + data.LoanAmountPaid + "</td>" +
                         "<td align='center' class='FinYear'>" + data.FinYear + "</td>" +
                         "<td align='center' class='inst_no1'>" + data.inst_no1 + "</td>" +
                         "<td align='center' class='inst_amt1'>" + data.inst_amt1 + "</td>" +
                         "<td align='center' class='inst_no2'>" + data.inst_no2 + "</td>" +
                         "<td align='center' class='inst_amt2'>" + data.inst_amt2 + "</td>" +
                         "<td class='btnc' align='center' ><img src='img/Edit.jpg'  class='btnEdit' value='Edit'/></td></tr>");

                }
            }
        });
    };