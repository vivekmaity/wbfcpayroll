﻿
$(document).ready(function () {
    $("#txtLifeCertificateYear").blur(function () {
        //if the letter is not digit then display error and don't type anything
        var txtyr = $("#txtLifeCertificateYear").val();
        if (txtyr == "" || txtyr == " " || txtyr == null) {
            alert("life certificate year should not be blank");
             $("#txtLifeCertificateYear").val((new Date()).getFullYear());
            return false;
        }
    });
});


$(document).ready(function () {
    $('#txtEmployeename').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
         //   alert('Please Enter Alphabate');
            return false;
        }
    });
});

$(document).ready(function () {
    $('#txtBranchSearch').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (!regex.test(str)) {
            e.preventDefault();
            return false;
        }       
    });
});

//Life certificate year validation onli number can enter..
$(document).ready(function () {
 
    $("#txtLifeCertificateYear").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {               
            return false;
        }
    });
    $("#txtEmployeeNumber").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $("#txtPPOno").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });  


    $("#txtLifeCertificateYear").keyup(function () {
       // alert("inside keyup");
        var yr=$("#txtLifeCertificateYear").val();
        if (yr.length == 4)
        {
           // alert("kk");
            var licyr = $("#txtLifeCertificateYear").val();
            var currentDate= new Date();
            var yearDivition = licyr - currentDate.getFullYear();
            if (yearDivition > 5 || yearDivition < -5)
            {                
                alert("Enter Year  maximum or minimum 5 years from Current Year");
              //  alert(currentDate.getFullYear());
                $("#txtLifeCertificateYear").val(currentDate.getFullYear());
             //   $("#txtLifeCertificateYear").Text(currentDate.getFullYear());
              //  return false;
            }
        }
        
    });
});

$(document).ready(function () {
    // $("#lblCountSelecedCheckbox").val("Total record selected  = ");    
    $('#cmdCancel').click(function () {
        window.location.href = "UpdateLifeCertificate.aspx";
    });
    $('#btnExcelReport').click(function () {
        // if (GridFlag == "SearchDATA") {
        var empid = $("#txthdnEmpID").val();
        //  alert(empid);
        var branchid = $("#txthdnBranchID").val();
        var ppo = $("#txtPPOno").val();
        var empnumber = $("#txtEmployeeNumber").val();
        var licYEAR = $("#txtLifeCertificateYear").val();
        var ReportName = "LifeCertificateRecord";
        //  alert(ReportName);
        window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "&EmployID=" + empid + "&BankBranchID=" + branchid + "&PPOno=" + ppo + "&EmployNO=" + empnumber + "&LifeYear=" + licYEAR);
    });
});
function beforeSave_Search() { 
        if ($("#txtLifeCertificateYear").val() == "") {
            alert("Mention Life certificate Year");
         //   GridFlag = "";
            return false;
        }
        else if ($("#txthdnEmpID").val() == "" && $("#txthdnBranchID").val() == "" && $("#txtPPOno").val() == "" && $("#txtEmployeeNumber").val() == "") {
            alert("Enter a value of above field for quick serch");
           // GridFlag = "";
            return false;
        }
        else {
          //  GridFlag = "SearchDATA";
            return true;
        }
}

//Select Bank Name in Dropdownlist...
/* $(document).ready(function () {
    var bnkID=$("#txthdnBankID").val();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "UpdateLifeCertificate.aspx/GET_MST_BankName",
        data:{},
        dataType: "json",
        success: function (data) {                    
            var jsdata = JSON.parse(data.d);
          //  $("#ddlBankSearch").empty();
              $("#ddlBankSearch").append("<option value=''>(Select Bank)</option>")           
            $.each(jsdata, function (key, value) {
                $("#ddlBankSearch").append($("<option></option>").val(value.BankId).html(value.BankName));
            
            });
        }
    });
    // $("#ddlChannel option[value="bnkID"]").attr("selected", "selected");
  //  $("#ddlBankSearch").select.val(bnkID);
  //  alert(bnkID);
});*/

$(document).ready(function () {
    $("#ddlBankSearch").change(function () {//put BankID into hide textbox..
        $('#txthdnBankID').val($("#ddlBankSearch").val());
    });
    AutoComplete_PensionerName();
    AutoComplete_BranchName();
});

//Autocomplete Pensioner name...
function AutoComplete_PensionerName() {
    $("#txtEmployeename").autocomplete({
        source: function (request, response) {
            var empName = $("#txtEmployeename").val().trim();
            var W = "{'EmpName':'" + empName + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "UpdateLifeCertificate.aspx/GET_AutoComplete_PensionerName",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[0],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        appendTo: "#autocompletePensionerName",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            $("#txthdnEmpID").val(arr[1]);
            //return false;
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
}

////autocomplete Branch Name.. GET_MST_BranchName
function  AutoComplete_BranchName() {
    $("#txtBranchSearch").autocomplete({
        source: function (request, response) {
            if ($("#txthdnBankID").val() == "") {
                alert("Please Enter Bank Name To Select Branch.");
                return false;
            }
            var lifeBranch = $("#txtBranchSearch").val().trim();
            var lifeBank = $("#txthdnBankID").val();
            var W = "{'LifeBranchName':'" + lifeBranch + "','LifeBankID':'" + lifeBank + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "UpdateLifeCertificate.aspx/GET_MST_BranchName",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[0],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        appendTo: "#autocompleteBranchName",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            $("#txthdnBranchID").val(arr[1]);
            //return false;
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
}














