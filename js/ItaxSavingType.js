﻿$(function () {
    var Selector = {
        txtAccFinYear:$('#txtAccFinYear'),
        ddlItaxType: $("#ddlItaxType"),
        txtAbbraviation: $("#txtAbbraviation"),
        hdnItaxSavingTypeID: $("#hdnItaxSavingTypeID"),
        txtDescription: $("#txtDescription"),
        txtMaxAmount: $("#txtMaxAmount"),
        chckIsActive: $("#chckIsActive"),       
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        tbl: $("#tbl")
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    var xml = $.parseXML(responce.d);
                    callback(xml);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    var xml = $.parseXML(responce.d);
                    callback(xml);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    }
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeydown();
            objClientSide.Event.onBlur();
        },
        Event: {
            onLoad: function () {               
                objClientSide.Method.GetItaxSavingType();
                objClientSide.Method.GetItaxType();
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    var ID = $(this).closest("tr").find(".clsID").text();
                    var ItaxTypeID = $(this).closest("tr").find(".clsTypeID").text();
                    var Abbraviation = $(this).closest("tr").find(".clsAbbraviation").text();
                    var Description = $(this).closest("tr").find(".clsDescription").text();
                    var Amount = $(this).closest("tr").find(".clsMaxAmount").text();
                    var IsAmount = $(this).closest("tr").find(".clsIsActiveFlag").text();
                    Selector.hdnItaxSavingTypeID.val(ID);
                    Selector.ddlItaxType.val(ItaxTypeID);
                    Selector.txtAbbraviation.val(Abbraviation);
                    Selector.txtDescription.val(Description);
                    Selector.txtMaxAmount.val(Amount);
                    console.log(IsAmount);
                    var tf = IsAmount == 0 ? false : true;
                    $("input[name=IsActive]").prop('checked', tf);
                  
                    Selector.btnSave.val('Update');
                });
                $(document).on("click", ".clsDelete", function () {
                    var ID = $(this).closest("tr").find(".clsID").text();
                    if (Selector.hdnItaxSavingTypeID.val() == ID) {
                        alert("this Record is in Edit  Mode. You can not Delete this record now..");
                        return false;
                    }

                    if (confirm("Are you sure want to Delete this Record ??")) {

                        objClientSide.Method.Delete(ID);
                    }
                });
            },
            onKeydown: function () {
                Selector.txtMaxAmount.keypress(function (e) {
                    if (!(e.keyCode > 45 && e.keyCode < 58) || e.keyCode == 47 || (e.keyCode == 46 && $(this).val().indexOf('.') != -1)) {
                        return false;
                    }
                });
                Selector.txtDescription.keypress(function () {
                    objClientSide.Method.Search();
                });
            },
            onBlur: function () {               
                Selector.txtDescription.blur(function () {
                    objClientSide.Method.Search();
                });
            }
        },
        Method: {          
            GetItaxType: function () {              
                var Data = { FinYear: Selector.txtAccFinYear.val().trim() };
                var url = "ItaxTypeMaster.aspx/Get_Data";
                objServerSide.PostMehtod(url,Data, function (xml) {
                    var xmlTable = $(xml).find('Table');
                    Selector.ddlItaxType.empty();
                    Selector.ddlItaxType.append("<option value=''>Select Itax Type</option>")
                    $.each(xmlTable, function (index, value) {
                        var typedesc = $(value).find('TypeDesc').text() + ' ( ' + $(value).find('FullSex').text() + ' - ' + $(value).find('MaxAmount').text() + ' ' + $(value).find('FullMaxType').text()+' )';
                        Selector.ddlItaxType.append("<option value='" + $(value).find('ItaxTypeID').text() + "'>" + typedesc + "</option>");
                    });
                });
            },
            GetItaxSavingType: function () {               
                var url = "ItaxSavingType.aspx/Get_Data";
                //  console.log(Data);
                objServerSide.GetMethod(url, function (xml) {
                    var xmlTable = $(xml).find('Table');
                 //   console.log(jsdata);
                    objClientSide.Method.AppendTable(xmlTable);
                });
            },
            AppendTable: function (xmlTable) {
              //  console.log(jsdata);
                Selector.tbl.find('tbody').empty();
                $.each(xmlTable, function (index, value) {
                    index = index + 1;
                    //   A.ItaxSaveID,A.SavingTypeAbbr,A.SavingTypeDesc,A.MaxAmount,A.ItaxTypeID,B.TypeDesc+' -'+ B.MaxType TypeDesc, A.IsActive,FullIsActive                  
                    Selector.tbl.find('tbody').append("<tr>"
                        + "<td class='clsID hide' style='display:none;'>" + $(value).find('ItaxSaveID').text() + "</td>"
                        + "<td class=''>" + index + "</td>"
                        + "<td class='clsTypeID' style='display:none;'>" + $(value).find('ItaxTypeID').text() + "</td>"
                        + "<td class='clsType'>" +$(value).find('TypeDesc').text()  + "</td>"
                        + "<td class=' clsAbbraviation'>" +$(value).find('SavingTypeAbbr').text()  + "</td>"
                        + "<td class=' clsDescription'>" +$(value).find('SavingTypeDesc').text()  + "</td>"
                        + "<td class='clsMaxAmount'>" +$(value).find('MaxAmount').text()  + "</td>"
                        + "<td class=' clsIsActiveFlag' style='display:none;'>" + $(value).find('IsActive').text() + "</td>"
                        + "<td class=' clsIsActive'>" +$(value).find('FullIsActive').text()  + "</td>"
                        + "<td class=' clsUpdate text-center text-info' style='cursor:pointer;color:#007697;' > Edit</td>"
                        + "<td class=' clsDelete text-center text-default' style='cursor:pointer;color:#007697;'> Delete</td>"
                        + "</tr>");
                });
            },
            Save: function () {
                var rt = objClientSide.Method.Validation();
                if (rt == false) {
                    return false;
                }
           //     ItaxSaveID, SavingTypeAbbr, SavingTypeDesc, MaxAmount, ItaxTypeID, IsActive
                var Data = {
                    ItaxSaveID: Selector.hdnItaxSavingTypeID.val(), SavingTypeAbbr: Selector.txtAbbraviation.val().trim(),
                    SavingTypeDesc: Selector.txtDescription.val(), MaxAmount:Selector.txtMaxAmount.val().trim(),
                    ItaxTypeID: Selector.ddlItaxType.val(), IsActive: Selector.chckIsActive.is(':checked')==true?1:0
                };
                console.log(Data);
                var URL = "ItaxSavingType.aspx/Save";
                objServerSide.PostMehtod(URL, Data, function (xml) {                   
                    if (xml == 'error') {
                        alert('Error In Server side.. Operation Fail');
                        return false;
                    }
                    var xmlTable = $(xml).find('Table');
                    var Massage = '';
                    var returnCode = '';
                    $.each(xmlTable, function (index, value) {
                        Massage = $(value).find("Messege").text();
                        returnCode = $(value).find("ErrorCode").text();
                        alert(Massage);
                        if (returnCode == 1) {
                            window.location.reload();
                        }
                    });
                });
            },
            Delete: function (ID) {
                var Data = {ID: ID };
                var URL = "ItaxSavingType.aspx/Delete";
                objServerSide.PostMehtod(URL, Data, function (xml) {
                    if (xml == 'error') {
                        alert('Error In Server side.. Operation Fail');
                        return false;
                    }
                    var xmlTable = $(xml).find('Table');
                    var Massage = '';
                    var returnCode = '';
                    $.each(xmlTable, function (index, value) {
                        Massage = $(value).find("Massage").text();
                        returnCode = $(value).find("returnCode").text();
                        alert(Massage);
                        if (returnCode == 0) {
                            window.location.reload();
                        }
                    });
                });
            },
            Refresh: function () {
                Selector.txtMaxAmount.val("");
                Selector.hdnItaxSavingTypeID.val("");
                Selector.txtDescription.val("");
                Selector.txtAbbraviation.val("");                
                Selector.btnSave.val('Save');
                objClientSide.Method.GetItaxType();
                objClientSide.Method.GetItaxSavingType();
            },
            Validation: function () {
                if (Selector.ddlItaxType.val() == '') {
                    alert('Select Itax Type');
                    Selector.ddlItaxType.focus();
                    return false;
                }
                if (Selector.txtAbbraviation.val() == '') {
                    alert('write a Abbraviation');
                    Selector.txtAbbraviation.focus();
                    return false;
                }
                if (Selector.txtDescription.val().trim() == '') {
                    alert('Write Type Description');
                    Selector.txtDescription.focus();
                    return false;
                }
                if (Selector.txtMaxAmount.val().trim() == '') {
                    alert('Enter Value');
                    Selector.txtMaxAmount.focus();
                    return false;
                }
                return true;
            },
            Search: function () {
                var input = Selector.txtDescription.val().trim().toUpperCase();
                var tr = Selector.tbl.find('tbody>tr');
                $.each(tr, function (index, value) {
                    var description = $(this).find('.clsDescription').text();
                    if (description.toUpperCase().indexOf(input) > -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        }
    };

    objClientSide.Initialization();
});