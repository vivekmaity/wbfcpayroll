﻿//********************************Page Load Scope*******************************
var EmployeeId = 0;
var LoanId = 0;
$(function () {
    $('#txtEmpNoSearch').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });
   
    $('#txtDisbursementOrderDateExisting,#txtRepaymentDate,#txtRetAdjDate,#txtRetirementAdjustmentDate,#txtAsdujtmentDateAgainstAlreadyDisburse').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    $('#txtLoanSanctionDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {

            var DisbursementOrderDate = ""; 
            if ($("#txtDisbursementOrderDate").val() != "") {
                var iDate = ($("#txtDisbursementOrderDate").val()).split('/');
                DisbursementOrderDate = iDate[2] + "-" + iDate[1] + "-" + iDate[0];
            }
            else
                DisbursementOrderDate = 1500 + "-" + 01 + "-" + 01;

            var E = "{DisbursementOrderDate:'" + DisbursementOrderDate + "',LoanSanctionDate:'" + dates + "'}";
            $.ajax({
                type: "POST",
                url: 'LoanMsterRevised.aspx/Validate_SanctionDate',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var t = JSON.parse(data.d);
                    if (t.length>0 && t !="OK") {
                        alert(t);
                        $('#txtLoanSanctionDate').val('');
                        return false;
                    }

                }
            });
        }
    });

    $('#txtDisbursementOrderDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            var DisbursementOrderDate = Date.parse(dates);

            var LoanSanctionDate = "";
            if ($("#txtLoanSanctionDate").val() != "") {
                var iDate = ($("#txtLoanSanctionDate").val()).split('/');
                LoanSanctionDate = iDate[2] + "-" + iDate[1] + "-" + iDate[0];
            }
            else
                LoanSanctionDate = 1500 + "-" + 01 + "-" + 01;

            var E = "{DisbursementOrderDate:'" + dates + "',LoanSanctionDate:'" + LoanSanctionDate + "'}";
            $.ajax({
                type: "POST",
                url: 'LoanMsterRevised.aspx/Validate_DisbursementOrderDate',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var t = JSON.parse(data.d);
                    if (t.length > 0 && t != "OK") {
                        alert(t);
                        $('#txtDisbursementOrderDate').val('');
                        return false;
                    }
                }
            });
        }
    });

    $('#DisbursementDateAgainstAlreadtSanctionNo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        //onSelect: function (dates) {
        //    var DisbursementOrderDate = Date.parse(dates);

        //    var LoanSanctionDate = "";
        //    if ($("#txtAlreadySanctionReferenceDate").val() != "") {
        //        var iDate = ($("#txtAlreadySanctionReferenceDate").val()).split('/');
        //        LoanSanctionDate = iDate[2] + "-" + iDate[1] + "-" + iDate[0];
        //    }
        //    else
        //        LoanSanctionDate = 1500 + "-" + 01 + "-" + 01;

        //    var E = "{DisbursementOrderDate:'" + dates + "',LoanSanctionDate:'" + LoanSanctionDate + "'}";
        //    $.ajax({
        //        type: "POST",
        //        url: 'LoanMsterRevised.aspx/Validate_DisbursementOrderDate',
        //        data: E,
        //        contentType: "application/json; charset=utf-8",
        //        success: function (data) {
        //            var t = JSON.parse(data.d);
        //            if (t.length > 0 && t != "OK") {
        //                alert(t);
        //                $('#DisbursementDateAgainstAlreadtSanctionNo').val('');
        //                return false;
        //            }
        //        }
        //    });
        //}
    });

    $('#txtDeductionStartDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            var DeductionStartDate = Date.parse(dates);

            var DisbursementOrderDate = "";
            if ($("#txtDisbursementOrderDate").val() != "") {
                var iDate = ($("#txtDisbursementOrderDate").val()).split('/');
                DisbursementOrderDate = iDate[2] + "-" + iDate[1] + "-" + iDate[0];
            }
            else
                DisbursementOrderDate = 1500 + "-" + 01 + "-" + 01;

            var E = "{DisbursementOrderDate:'" + DisbursementOrderDate + "',DeductionStartDate:'" + dates + "'}";
            $.ajax({
                type: "POST",
                url: 'LoanMsterRevised.aspx/Validate_DeductionStartDate',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var t = JSON.parse(data.d);
                    if (t.length > 0 && t != "OK") {
                        alert(t);
                        $('#txtDeductionStartDate').val('');
                        return false;
                    }
                }
            });
        }
    });
    $('#DisbursementDeductionStartDtAgainstAlreadtSanctionNo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            var DeductionStartDate = Date.parse(dates);

            var DisbursementOrderDate = "";
            if ($("#DisbursementDateAgainstAlreadtSanctionNo").val() != "") {
                var iDate = ($("#DisbursementDateAgainstAlreadtSanctionNo").val()).split('/');
                DisbursementOrderDate = iDate[2] + "-" + iDate[1] + "-" + iDate[0];
            }
            else
                DisbursementOrderDate = 1500 + "-" + 01 + "-" + 01;

            var E = "{DisbursementOrderDate:'" + DisbursementOrderDate + "',DeductionStartDate:'" + dates + "'}";
            $.ajax({
                type: "POST",
                url: 'LoanMsterRevised.aspx/Validate_DeductionStartDate',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var t = JSON.parse(data.d);
                    if (t.length > 0 && t != "OK") {
                        alert(t);
                        $('#DisbursementDeductionStartDtAgainstAlreadtSanctionNo').val('');
                        return false;
                    }
                }
            });
        }
    });
    /*==================================================== Date Masking ===================================================================*/

    $(function () {
        $("[id$=txtDeductionStartDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });

    $(function () {
        $("[id$=DisbursementDateAgainstAlreadtSanctionNo]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtDisbursementOrderDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtLoanSanctionDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtDisbursementOrderDateExisting]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });



    $(function () {
        $("[id$=DisbursementDeductionStartDtAgainstAlreadtSanctionNo]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtRepaymentDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtRetAdjDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtRetirementAdjustmentDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtAsdujtmentDateAgainstAlreadyDisburse]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    /*=======================================================================================================================*/
});
//*****************Button Press Scope*********************************//
$(document).ready(function () {
    $('#ImageButton1').click(function (e) {
        e.preventDefault();
        $('#txtLoanSanctionDate').datepicker("show");
    });
    $('#ImageButton2').click(function (e) {
        e.preventDefault();
        $('#txtDeductionStartDate').datepicker("show");
    });
    $('#ImageButton3').click(function (e) {
        e.preventDefault();
        $('#txtDisbursementOrderDate').datepicker("show");
    });
    $("#btnAddNewLoan").click(function (e) {
        e.preventDefault();
        $(this).css('color', 'yelow');
        $("#btnDisAdinExtLoan").css('color', 'white');
        $("#btnExistingLoanDetails").css('color', 'white');
        $(".buttonbig").css('background-color', '#0094ff');
        $(".buttonbig1").css('background-color', 'green');
        $(".buttonbig2").css('background-color', '#0094ff');


        $("#DisbursementAddNewLoan").hide();
        $("#DescriptionBodyDisbursementAgainstExistingLoan").hide();
        $("#DisbursementLoanEdit").hide();
        $("#DescriptionBodyExistingLoanDetails").hide();
        if (!$("#DescriptionBodyAddNewLoan").is(':visible')) {
            $("#DescriptionBodyAddNewLoan").show();
        }
        else {
            $("#DescriptionBodyAddNewLoan").hide();
        }
        
    });
    $("#btnDisAdinExtLoan").click(function (e) {
        e.preventDefault();
        $(this).css('color', 'yelow');
        $("#btnAddNewLoan").css('color', 'white');
        $("#btnExistingLoanDetails").css('color', 'white');

        $(".buttonbig").css('background-color', '#0094ff');
        $(".buttonbig1").css('background-color', '#0094ff');
        $(".buttonbig2").css('background-color', 'green');

        $("#DisbursementAddNewLoan").hide();
        $("#DescriptionBodyAddNewLoan").hide();
        $("#DisbursementLoanEdit").hide();
        $("#DescriptionBodyExistingLoanDetails").hide();
        if (!$("#DescriptionBodyDisbursementAgainstExistingLoan").is(':visible')) {
            $("#DescriptionBodyDisbursementAgainstExistingLoan").show();
        }
        else {
            $("#DescriptionBodyDisbursementAgainstExistingLoan").hide();
        }

    });
    $("#btnExistingLoanDetails").click(function (e) {
        e.preventDefault();
        $(this).css('color', 'yelow');
        $("#btnAddNewLoan").css('color', 'white');
        $("#btnDisAdinExtLoan").css('color', 'white');

        $(".buttonbig").css('background-color', 'green');
        $(".buttonbig1").css('background-color', '#0094ff');
        $(".buttonbig2").css('background-color', '#0094ff');

        $("#DisbursementAddNewLoan").hide();
        $("#DescriptionBodyDisbursementAgainstExistingLoan").hide();
        $("#DescriptionBodyAddNewLoan").hide();
        $("#DisbursementLoanEdit").hide();
        if (!$("#DescriptionBodyExistingLoanDetails").is(':visible')) {
            $("#DescriptionBodyExistingLoanDetails").show();
        }
        else {
            $("#DescriptionBodyExistingLoanDetails").hide();
        }

    });
    $("#ListofAlreadyDisbursementLoans").click(function (e) {
        e.preventDefault();
        $("#DisbursementAddNewLoan").hide();
        $("#DescriptionBodyAddNewLoan").hide();
        if (!$("#DisbursementLoanEdit").is(':visible')) {
            $("#DisbursementLoanEdit").show();
        }
        else {
            $("#DisbursementLoanEdit").hide();
        }
    });
    $("#AddNewLoan").click(function (e) {
        e.preventDefault();
        $("#DescriptionBodyAddNewLoan").hide();
        $("#DescriptionBodyExistingLoanDetails").hide();
        $("#DisbursementLoanEdit").hide();
        if (!$("#DisbursementAddNewLoan").is(':visible')) {
            $("#DisbursementAddNewLoan").show();
        }
        else {
            $("#DisbursementAddNewLoan").hide();
        }
        GenerateAutoDisbursementOrderNo();
    });
    $("#btnSearchEployee").click(function (e) {
        Get_EmployeeDetails();
    });
    
    $("#cmdSave").click(function (e) {
        e.preventDefault();
        var Status = ValidateFormControlVaue();
        if (Status != true) {
            return false;
        }
        var Status1 = CheckTotalInstallmentNo_With_SlabInstallNO();
        if (Status1 != true) {
            return false;
        }
        var Status2 = CheckTotalDisbursementAmount_with_TotalPrincipleSlabAmount();
        if (Status2 != true) {
            return false;
        }
        var Status3 = CheckTotalDisbursementInterestAmount_with_TotalInterestSlabAmount();
        if (Status3 != true) {
            return false;
        }
        var Status4 = CheckSanctionAmountWithDisburseAmount();
        if (Status4 != true) {
            return false;
        }
        var Status5 = CheckDuplicateSanctionNo();
        if (Status5 != true) {
            return false;
        }
        var Status6 = ValidateTotalNoOfInstallmentNoWithDOR();
        if (Status6!=true)
        {
            return false;
        }
        var Status7 = Get_EncodedValue_BySector()
        if (Status7 != true) {
            return false;
        }
        if (confirm("Do you want to save this Loan!!")) {
            Save_Loan_Details();
        }
    });
    $("#cmdCancel").click(function () {
        window.location.href = "LoanMsterRevised.aspx";
    });
    $("#txtLoanDisbursementAmount,#txtLoanSanctionAmount").keyup(function () {
        CheckSanctionAmountWithDisburseAmount();
        if ($("#txtTotalLoanInstall").val() != "" && $("#txtTotalLoanInstall").val() != 0) {
            //CalculateInstallmentDetailsAuto();temp
            CalculateAutoField();
        }
        else {
            RefreshAllInstallTAB();
        }
    });

    $("#txtSanctionNoAutoComplete").keydown(function (event) {
        if ($("#ddlLoanDescription").val == "0") {
            alert("Select Loan Description !!");
            $("#ddlLoanDescription").focus();
            return false;
        }
        $("#hdnSanctionNo").val('');
    });
    $("#txtSanctionNoAutoCompleteExisting").keydown(function (event) {
        if ($("#ddlLoanDescriptionExisting").val == "0") {
            alert("Select Loan Description !!");
            $("#ddlLoanDescriptionExisting").focus();
            return false;
        }
        $("#hdnAutocompleteExistingSanctionNo").val('');
    });
    $("#txtSanctionNoAutoComplete").autocomplete({
        source: function (request, response) {
            var E = "{SanctionNo: '" + $("#txtSanctionNoAutoComplete").val() + "',EmployeeId:'"+EmployeeId+"',LoanDescriptionId:'"+$("#ddlLoanDescription").val()+"'}";
            $.ajax({
                type: "POST",
                url: "LoanMsterRevised.aspx/Get_SanctionNoForAutoComplete_Disbursement",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                }
            });
        },
        appendTo: "#AutoComplete-SanctionNo",
        select: function (e, i) {
            LoanId=i.item.val;
            E = "{LoanId:'" + i.item.val + "'}";
            $.ajax({
                type: "POST",
                url: 'LoanMsterRevised.aspx/GetDetails_AgainstSanctionNo',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    if (D.d.length > 0) {
                        var t = JSON.parse(D.d);
                        $('#txtLoanSanctionAmountDisExistingLoan').val(t[0].SanctionAmount);
                        $('#txtAlreadySanctionReferenceDate').val(t[0].SanctionDate);
                        $('#totalDisbursementAmount').text(t[0].SumDisbursedAmount);
                        $('#LeftDisbursementAmount').text(t[0].leftDisbursementAmount.toFixed(2));
                    }
                    else {
                        $('#txtLoanSanctionAmountDisExistingLoan').val("");
                        $('#txtAlreadySanctionReferenceDate').val("");
                        $('#totalDisbursementAmount').text("XXXXXXXX");
                        $('#LeftDisbursementAmount').text("XXXXXXXX");
                    }
                }
            });
            PopulateGridAgainstAlreadySanctionNoDisbursementPage();

        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $("#txtSanctionNoAutoCompleteExisting").autocomplete({
        source: function (request, response) {
            var E = "{SanctionNo: '" + $("#txtSanctionNoAutoCompleteExisting").val() + "',EmployeeId:'" + EmployeeId + "',LoanDescriptionId:'" + $("#ddlLoanDescriptionExisting").val() + "'}";
            $.ajax({
                type: "POST",
                url: "LoanMsterRevised.aspx/Get_SanctionNoForAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                }
            });
        },
        appendTo: "#Autocomplete-SanctionNoExisting",
        select: function (e, i) {
            LoanId = i.item.val;
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));

    });
    $("#txtNewSanctionNo").keyup(function () {
        var Status = CheckDuplicateSanctionNo();
        if (Status != true) {
            alert("This Sanction No already exists!!");
            return false;
        }
        else {
            GenerateAutoDisbursementOrderNo1stTime();
        }
        
    });
    $("#btnSearchByExistingSanctionNo").click(function (e) {
        e.preventDefault();
        PopulateGridOfExistingLoanSanctionNoWise();
    });
    $(document).on('click', ".btnEdit", function () {
        var currentrow = $(this).parent().parent();
        var DisbursementOrderNo = currentrow.find(".DisbursementOrderNo").text();
        var DisbursementOrderDate = currentrow.find(".DisbursementOrderDate").text();
        var DisburseAmount = currentrow.find(".DisburseAmount").text();
        var SanctionNo = currentrow.find(".SanctionNo").text();

        $("#txtDisbursementAmountExisting").val(DisburseAmount);
        $("#txtDisburseOrderNoExisting").val(DisbursementOrderNo);
        $("#txtDisbursementOrderDateExisting").val(DisbursementOrderDate);
        $("#DisbursementAddNewLoan").show();
        //if (!$("#DisbursementAddNewLoan").is(':visible')) {
        //    $("#DisbursementAddNewLoan").show();
        //}
        //else {
        //    $("#DisbursementAddNewLoan").hide();
        //}
        //GenerateAutoDisbursementOrderNo();
    });
    $(document).on('change', '#ddlLoanDescriptionExisting', function () {
        $("#hdnAutocompleteExistingSanctionNo").val('');
        $("#txtSanctionNoAutoCompleteExisting").val('');
    })
    $(document).on('change', '#ddlLoanDescription', function () {
        $("#hdnSanctionNo").val('');
        $("#txtSanctionNoAutoComplete").val('');
    })
    $("#btnSaveAgainstAlreadySanctionNo").click(function (e) {
        e.preventDefault();
        var Status = ValidateBeforSave_AgainstAlredySanctionNo();
        if (Status != true) {
            return false;
        }
        var Status1 = CheckTotalInstallmentNoAgainstAlreadySanctionNo_With_SlabInstallNO();
        if (Status1 != true) {
            return false;
        }
        var Status2 = CheckTotalDisbursementAmountAgainstAlreadySanctionNo_with_TotalPrincipleSlabAmount();
        if (Status2 != true) {
            return false;
        }
        var Status3 = CheckTotalDisbursementInterestAmountAgainstAlreadySanctionNo_with_TotalInterestSlabAmount();
        if (Status3 != true) {
            return false;
        }
        var Status4 = CheckDisbursementAmountAgainstAlreadySanctionNo();
        if (Status4 != true) {
            return false;
        }
        var Status5 = ValidateTotalNoOfInstallmentNoWithDORAgainstAlreadySancton();
        if (Status5!=true)
        {
            return false;
        }
        var Status6 = Get_EncodedValue_BySector()
        if (Status6 != true) {
            return false;
        }
        if (confirm("Do you want to save this Loan!!")) {
            SaveLoanAgainstAlreadySanctionNo();
        }
    });
    $("#DisbursementAmountAgainstAlreadtSanctionNo").keyup(function () {
        CheckDisbursementAmountAgainstAlreadySanctionNo();
        if ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() != "" && $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() != 0) {
            //CalculateInstallmentDetailsAutoAgainstalreadySanctionNo();tmp
            CalculateAutoFieldGainstAlreadySanction();
        }
        else {
            RefreshAllInstallTABAgainstDisbursement();
        }
    });
    $(document).on('click', '.btnPrePaiment', function () {
        var ctrl = $(this);
        var currentrow = ctrl.parent().parent();
        var LoanId = currentrow.find(".LoanID").text();
        $(".btnPrePaiment").attr('LoanID', LoanId);
        $("#overlay").show();
    });
    $(document).on('click', '.btnRetirementAdjustment', function () {
        var ctrl = $(this);
        var currentrow = ctrl.parent().parent();
        var LoanId = currentrow.find(".LoanID").text();
        $(".btnRetirementAdjustment").attr('LoanID', LoanId);
        $("#overlay1").show();
        Get_RetirementBenifitByLoanId(LoanId);
    });
    $(document).on('click', '#prev', function () {
        $("#overlay").hide();
    });
    $(document).on('click', '#prev1', function () {
        $("#overlay1").hide();
    });
    $("#btnSaveRepaymentDetails").click(function (e) {
        e.preventDefault();
        var Status = validateBeforePrepaymentSave()
        if (Status != true) {
            return false;
        }
        if (confirm("Do you want to save this !!")) {
            Save_PrepaymentAmount();
        }
    });
    $("#btnSaveRetirementAdjustment").click(function (e) {
        e.preventDefault();
        var Status = validateBeforeRetirementpaymentSave()
        if (Status != true) {
            return false;
        }
        if (confirm("Do you want to save this !!")) {
            Save_RetirementAdjustment();
        }
    });
    $("#txtTotalLoanInstall").keyup(function () {
        var Status6 = ValidateTotalNoOfInstallmentNoWithDOR();
        if (Status6 != true) {
            return false;
        }
        if ($("#txtTotalLoanInstall").val() != "" && $("#txtTotalLoanInstall").val() != 0 && $("#txtInterestOnDisbursementAmount") != "" && $("#txtLoanDisbursementAmount").val()) {
            //CalculateInstallmentDetailsAuto();tmp
            //CalculateInterestInstallmentDetailsAuto();tmp
            CalculateAutoField();
        }
        else {
            RefreshAllInstallTAB();
        }
    });
    $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").keyup(function () {
        var Status6 = ValidateTotalNoOfInstallmentNoWithDORAgainstAlreadySancton();
        if (Status6 != true) {
            return false;
        }
        if ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() != "" &&  $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() != 0 && $("#DisbursementAmountAgainstAlreadtSanctionNo").val() != "") {
            //CalculateInstallmentDetailsAutoAgainstalreadySanctionNo();//tmp
            //CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo();//tmp
            CalculateAutoFieldGainstAlreadySanction();
        }
        else {
            RefreshAllInstallTABAgainstDisbursement();
        }
    });
    $("#txtInterestOnDisbursementAmount").keyup(function () {
        if ($("#txtTotalLoanInstall").val() != "" && $("#txtTotalLoanInstall").val() != 0) {
            //CalculateInterestInstallmentDetailsAuto();tmp
            CalculateAutoField();
        }
        //else {
        //    RefreshAllInstallTAB();
        //}
    });
    $("#DisbursementIneterestAgainstAlreadtSanctionNo").keyup(function () {
        if ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() != "" && $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() != 0) {
            //CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo();
            CalculateAutoFieldGainstAlreadySanction();
        }
        //else {
        //    RefreshAllInstallTAB();
        //}
    });
    //$("#txtLoanSanctionAmount,#txtLoanDisbursementAmount,#txtInterestOnDisbursementAmount,#FrstInstallmentPrincipleAmount,#FrstInstallmentInterestAmount,#secndInstallmentPrincipleAmount,#secndInstallmentInterestAmount,#thirdInstallmentPrincipleAmount,#thirdInstallmentInterestAmount,#FourthInstallmentPrincipleAmount,#FourthInstallmentInterestAmount,#FifthInstallmentPrincipleAmount,#FifthInstallmentInterestAmount,#txtRetPrincipleAmount,#txtRetirementAdjustmentAmount").keypress(function () {
    //        if (event.which != 46 && (event.which <= 47 || event.which >= 59)) {
    //            event.preventDefault();
    //            if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
    //                event.preventDefault();
    //            }
    //        }
    //});
});
//************************************************************************
//************Function Scope*****************************************************
function jqAlert(message, title) {
    if (!title)
        title = 'Alert';

    if (!message)
        message = 'No Message to Display.';

    $('<div></div>').html(message).dialog({
        title: title,
        width:500,
        resizable: false,
        //modal: false,
        position: ['middle',40],
        
        buttons: {
            'Ok': function () {
                $(this).dialog('close');
            }
        }
    });
}
function isNumber(evt) {
    //evt = (evt) ? evt : window.event;
    //var charCode = (evt.which) ? evt.which : evt.keyCode;
    //if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    //    return false;
    //}
    if (event.which != 46 && (event.which == 58 || (event.which <= 47 || event.which >= 59))) {
        event.preventDefault();
        return false;
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
            return false;
        }
    }
    return true;
}


     //***************Save New Loan*******************************************************************//
function ValidateFormControlVaue() {
    var Status = true;
    if ($("#ddlLoanDesc").val() == "0") {
        //alert($("#ddlLoanDesc").val());
        alert("Select Loan Description!");
        $("#ddlLoanDesc").focus();
        Status= false;
    }

    else if ($("#txtNewSanctionNo").val() == "") {
        alert("Please enter Sanction No!");
        $("#txtNewSanctionNo").focus();
        Status= false;
    }
    else if ($("#txtLoanSanctionDate").val() == "") {
        alert("Please select Sanction Date!");
        $("#txtLoanSanctionDate").focus();
        Status= false;
    }
    else if ($("#txtLoanSanctionAmount").val() == "") {
        alert("Please enter Sanction Amount!");
        $("#txtLoanSanctionAmount").focus();
        Status= false;
    }
    
    else if ($("#txtDisbursementOrderNo").val() == "") {
        alert("Please enter Disbursement No!");
        $("#txtDisbursementOrderNo").focus();
        Status= false;
    }
    else if ($("#txtDisbursementOrderDate").val() == "") {
        alert("Please select Disbursement Date!");
        $("#txtDisbursementOrderDate").focus();
        Status= false;
    }
    else if ($("#txtLoanDisbursementAmount").val() == "") {
        alert("Please enter Disbursement Amount!");
        $("#txtLoanDisbursementAmount").focus();
        Status= false;
    }

    else if ($("#txtDeductionStartDate").val() == "") {
        alert("Please select Deduction start Date!");
        $("#txtDeductionStartDate").focus();
        Status= false;
    }
    else if ($("#txtInterestOnDisbursementAmount").val() == "") {
        alert("Please enter Interest on Disbursement Amount!");
        $("#txtInterestOnDisbursementAmount").focus();
        Status= false;
    }
    else if ($("#txtTotalLoanInstall").val() == "") {
        alert("Please enter Total Installment No!");
        $("#txtTotalLoanInstall").focus();
        Status= false;
    }
    return Status;
}
function Save_Loan_Details() {

    var RetirementAdjustmentDate = "";
    if ($("#txtRetirementAdjustmentDate").val() != "") {
        var iDate = ($("#txtRetirementAdjustmentDate").val()).split('/');
        RetirementAdjustmentDate = iDate[0] + "/" + iDate[1] + "/" + iDate[2];

    }
    else
        RetirementAdjustmentDate = "01" + "/" + "01" + "/" + "1500";


    var ArrayObjLoanMaster = [];
    var objLoanMaster = new Object();
    objLoanMaster.LoanId = 0;
    objLoanMaster.EmployeeId = EmployeeId;
    objLoanMaster.LoanDescriptionId = $("#ddlLoanDesc").val();
    objLoanMaster.SanctionNo = $("#txtNewSanctionNo").val();
    objLoanMaster.SanctionDate = $("#txtLoanSanctionDate").val();
    objLoanMaster.SanctionAmount = $("#txtLoanSanctionAmount").val();
    objLoanMaster.DisbursementOrderNo = $("#txtDisbursementOrderNo").val();
    objLoanMaster.DisbursementOrderDate = $("#txtDisbursementOrderDate").val();
    objLoanMaster.DisbursementAmount = $("#txtLoanDisbursementAmount").val();
    objLoanMaster.DeductionStartDate = $("#txtDeductionStartDate").val();
    objLoanMaster.InterestOnDisbursementAmount = $("#txtInterestOnDisbursementAmount").val();
    //
    objLoanMaster.TotalLoanInstall = $("#txtTotalLoanInstall").val();
    objLoanMaster.CurrentLoanInstall = ($("#txtCurrentLoanInstall").val() == "" ? 0 : $("#txtCurrentLoanInstall").val());
    objLoanMaster.LoanPrincipalAmount = ($("#txtLoanAmountPaid").val() == "" ? 0 : $("#txtLoanAmountPaid").val());
    objLoanMaster.LoanInterestAmount = ($("#txtLoanIntAmountPaid").val() == "" ? 0 : $("#txtLoanIntAmountPaid").val());
    //1st Slab
    objLoanMaster.FrstInstallmentNo = ($("#FrstInstallmentNo").val() == "" ? 0 : $("#FrstInstallmentNo").val());
    objLoanMaster.FrstInstallmentPrincipleAmount = ($("#FrstInstallmentPrincipleAmount").val() == "" ? 0 : $("#FrstInstallmentPrincipleAmount").val());
    objLoanMaster.FrstInstallmentInterestAmount = ($("#FrstInstallmentInterestAmount").val() == "" ? 0 : $("#FrstInstallmentInterestAmount").val());
    //2nd Slab
    objLoanMaster.secndInstallmentNo = ($("#secndInstallmentNo").val() == "" ? 0 : $("#secndInstallmentNo").val());
    objLoanMaster.secndInstallmentPrincipleAmount = ($("#secndInstallmentPrincipleAmount").val() == "" ? 0 : $("#secndInstallmentPrincipleAmount").val());
    objLoanMaster.secndInstallmentInterestAmount = ($("#secndInstallmentInterestAmount").val() == "" ? 0 : $("#secndInstallmentInterestAmount").val());
    //3rd Slab
    objLoanMaster.thirdInstallmentNo = ($("#thirdInstallmentNo").val() == "" ? 0 : $("#thirdInstallmentNo").val());
    objLoanMaster.thirdInstallmentPrincipleAmount = ($("#thirdInstallmentPrincipleAmount").val() == "" ? 0 : $("#thirdInstallmentPrincipleAmount").val());
    objLoanMaster.thirdInstallmentInterestAmount = ($("#thirdInstallmentInterestAmount").val() == "" ? 0 : $("#thirdInstallmentInterestAmount").val());
    //4th Slab
    objLoanMaster.FourthInstallmentNo = ($("#FourthInstallmentNo").val() == "" ? 0 : $("#FourthInstallmentNo").val());;
    objLoanMaster.FourthInstallmentPrincipleAmount = ($("#FourthInstallmentPrincipleAmount").val() == "" ? 0 : $("#FourthInstallmentPrincipleAmount").val());
    objLoanMaster.FourthInstallmentInterestAmount = ($("#FourthInstallmentInterestAmount").val() == "" ? 0 : $("#FourthInstallmentInterestAmount").val());
    //5ht Slab
    objLoanMaster.FifthInstallmentNo = ($("#FifthInstallmentNo").val() == "" ? 0 : $("#FifthInstallmentNo").val());;
    objLoanMaster.FifthInstallmentPrincipleAmount = ($("#FifthInstallmentPrincipleAmount").val() == "" ? 0 : $("#FifthInstallmentPrincipleAmount").val());
    objLoanMaster.FifthInstallmentInterestAmount = ($("#FifthInstallmentInterestAmount").val() == "" ? 0 : $("#FifthInstallmentInterestAmount").val());
    //Retirement Adjustment Slab
    objLoanMaster.RetirementPrincipleAmount = ($("#txtRetPrincipleAmount").val() == "" ? 0 : $("#txtRetPrincipleAmount").val());
    objLoanMaster.RetirementPrincipleAdjustmentAmount = ($("#txtRetirementAdjustmentAmount").val() == "" ? 0 : $("#txtRetirementAdjustmentAmount").val());
    objLoanMaster.RetirementAdjustmentDate = (RetirementAdjustmentDate);
   


    ArrayObjLoanMaster[0] = objLoanMaster;

    var E = "{param:" + JSON.stringify(ArrayObjLoanMaster) + "}";

    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Save_LoanDetails',
        data: E,
        cache: false,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d.length > 0) {
                var Result = JSON.parse(data.d);
                if (Result == "Success") {
                    alert("Loan Details Saved sucessfully");
                    window.location.href = "LoanMsterRevised.aspx";
                }
                else {
                    alert("Loan Details not Saved");
                }
            }
        }
    });

}
function CheckTotalInstallmentNo_With_SlabInstallNO() {
    var Status1 = true;
    var TotalLoanInstall = $("#txtTotalLoanInstall").val();
    var TotalSlabInstallmentNo = 0;
    TotalSlabInstallmentNo = (parseInt($("#FrstInstallmentNo").val() == "" ? 0 : $("#FrstInstallmentNo").val()) + parseInt($("#secndInstallmentNo").val() == "" ? 0 : $("#secndInstallmentNo").val()) + parseInt($("#thirdInstallmentNo").val() == "" ? 0 : $("#thirdInstallmentNo").val()) + parseInt($("#FourthInstallmentNo").val() == "" ? 0 : $("#FourthInstallmentNo").val()) + parseInt($("#FifthInstallmentNo").val() == "" ? 0 : $("#FifthInstallmentNo").val()));
    if (TotalLoanInstall != TotalSlabInstallmentNo) {
        alert("Total Installment No should be same with sum of slab Installment No!!");
        Status1= true;
    }
    return Status1;
}
function CheckTotalDisbursementAmount_with_TotalPrincipleSlabAmount() {
    var Status2 = true;
    var TotalDisbursementAmount = $("#txtLoanDisbursementAmount").val();
    var TotalSlabPrincipleAmount = 0;


    var IstPrincipleAmount = parseFloat($("#FrstInstallmentPrincipleAmount").val() == "" ? 0 : $("#FrstInstallmentPrincipleAmount").val());
    var ScndPrincipleAmount = parseFloat($("#secndInstallmentPrincipleAmount").val() == "" ? 0 : $("#secndInstallmentPrincipleAmount").val());
    var ThirdPrincipleAmount = parseFloat($("#thirdInstallmentPrincipleAmount").val() == "" ? 0 : $("#thirdInstallmentPrincipleAmount").val());
    var FourthPrincipleAmount = parseFloat($("#FourthInstallmentPrincipleAmount").val() == "" ? 0 : $("#FourthInstallmentPrincipleAmount").val());
    var FifthPrincipleAmount = parseFloat($("#FifthInstallmentPrincipleAmount").val() == "" ? 0 : $("#FifthInstallmentPrincipleAmount").val());

    var IstInstallNo = parseInt($("#FrstInstallmentNo").val() == "" ? 0 : $("#FrstInstallmentNo").val());
    var SecondInstallNo = parseInt($("#secndInstallmentNo").val() == "" ? 0 : $("#secndInstallmentNo").val());
    var ThirdInstallNo = parseInt($("#thirdInstallmentNo").val() == "" ? 0 : $("#thirdInstallmentNo").val());
    var FourthInstallNo = parseInt($("#FourthInstallmentNo").val() == "" ? 0 : $("#FourthInstallmentNo").val());
    var FifthInstallNo = parseInt($("#FifthInstallmentNo").val() == "" ? 0 : $("#FifthInstallmentNo").val());

    var RetAdjPrincipleAmount = parseFloat($("#txtRetPrincipleAmount").val() == "" ? 0 : $("#txtRetPrincipleAmount").val());

    if (IstPrincipleAmount < 0 || ScndPrincipleAmount < 0 || ThirdPrincipleAmount < 0 || FourthPrincipleAmount < 0 || FifthPrincipleAmount < 0) {
        alert("Installment Slab Amount can't be negative!!");
        Status2 = false;
    }
    else {
        TotalSlabPrincipleAmount = (IstPrincipleAmount * IstInstallNo) + (ScndPrincipleAmount * SecondInstallNo) + (ThirdPrincipleAmount * ThirdInstallNo) + (FourthPrincipleAmount * FourthInstallNo) + (FifthPrincipleAmount * FifthInstallNo) + (RetAdjPrincipleAmount);
        if (TotalSlabPrincipleAmount != TotalDisbursementAmount) {
            alert("Disbursement Amount should be same with sum of slab Installment Principle Amount!!");
            Status2 = false;
        }
    }
    return Status2;
}
function CheckTotalDisbursementInterestAmount_with_TotalInterestSlabAmount() {
    var Status3 = true;
    var TotalDisbursementInterestAmount = $("#txtInterestOnDisbursementAmount").val();
    var TotalSlabInterestAmount = 0;

    var IstInterestAmount = parseFloat($("#FrstInstallmentInterestAmount").val() == "" ? 0 : $("#FrstInstallmentInterestAmount").val());
    var ScndInterestAmount = parseFloat($("#secndInstallmentInterestAmount").val() == "" ? 0 : $("#secndInstallmentInterestAmount").val());
    var ThirdInterestAmount = parseFloat($("#thirdInstallmentInterestAmount").val() == "" ? 0 : $("#thirdInstallmentInterestAmount").val());
    var FourthInterestAmount = parseFloat($("#FourthInstallmentInterestAmount").val() == "" ? 0 : $("#FourthInstallmentInterestAmount").val());
    var FifthInterestAmount = parseFloat($("#FifthInstallmentInterestAmount").val() == "" ? 0 : $("#FifthInstallmentInterestAmount").val());

    var IstInstallNo = parseInt($("#FrstInstallmentNo").val() == "" ? 0 : $("#FrstInstallmentNo").val());
    var SecondInstallNo = parseInt($("#secndInstallmentNo").val() == "" ? 0 : $("#secndInstallmentNo").val());
    var ThirdInstallNo = parseInt($("#thirdInstallmentNo").val() == "" ? 0 : $("#thirdInstallmentNo").val());
    var FourthInstallNo = parseInt($("#FourthInstallmentNo").val() == "" ? 0 : $("#FourthInstallmentNo").val());
    var FifthInstallNo = parseInt($("#FifthInstallmentNo").val() == "" ? 0 : $("#FifthInstallmentNo").val());

    var RetAdjInterestAmount = parseFloat($("#txtRetirementAdjustmentAmount").val() == "" ? 0 : $("#txtRetirementAdjustmentAmount").val());
    if (IstInterestAmount < 0 || ScndInterestAmount < 0 || ThirdInterestAmount < 0 || FourthInterestAmount < 0 || FifthInterestAmount < 0) {
        alert("Installment Interest Slab  Amount can't be negative!!");
        Status3 = false;
    }
    else {

        TotalSlabInterestAmount = (IstInterestAmount * IstInstallNo) + (ScndInterestAmount * SecondInstallNo) + (ThirdInterestAmount * ThirdInstallNo) + (FourthInterestAmount * FourthInstallNo) + (FifthInterestAmount * FifthInstallNo) + (RetAdjInterestAmount);
        if (TotalSlabInterestAmount != TotalDisbursementInterestAmount) {
            alert("Interest on Disbursement Amount should be same with sum of slab Installment Interest Amount!!");
            Status3 = false;
        }
    }
    return Status3;
}
function CheckSanctionAmountWithDisburseAmount() {
    var status = true;
    var SanctionAmount = $("#txtLoanSanctionAmount").val();
    var DisbursementAmount = $("#txtLoanDisbursementAmount").val();
    if (parseFloat(DisbursementAmount == "" ? 0 : DisbursementAmount) > parseFloat(SanctionAmount == "" ? 0 : SanctionAmount)) {
        status = false;
        alert("Disbursement Amount alaways be less or equal to Sanction Amount!");
        $("#txtLoanDisbursementAmount").val('');
    }
    return status;
}
function CheckDuplicateSanctionNo() {
    var Status = true;
    var SanctionNo = $("#txtNewSanctionNo").val();
    var E = "{ SanctionNo: '" + SanctionNo + "' }";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Check_DuplicateSanctionNo',
        data: E,
        contentType: "application/json; charset=utf-8",
        async:false,
        success: function (data) {
            var t = JSON.parse(data.d);
            if (t == "YES") {
                Status = false;
            }

        }
    });
    return Status;
}
function ValidateTotalNoOfInstallmentNoWithDOR() {
    var Status = true;
    var NoOfMonthsFromDor = ($("#NoOfMonths").text() == "" ? 0 : $("#NoOfMonths").text());
    var NoOfInstallmentNo = ($("#txtTotalLoanInstall").val() == "" ? 0 : $("#txtTotalLoanInstall").val());
    if (parseFloat(NoOfMonthsFromDor) < parseFloat(NoOfInstallmentNo)) {
        alert("No of Installment No should be equal or less then No of months left from Retirement!");
        Status = true;
    }
    return Status;
}
    //*********************End****************************************************************************//
function PopulateGridOfExistingLoanSanctionNoWise() {
    if (LoanId == "") {
        alert("Select Sanction No!!");
        $("#txtSanctionNoAutoCompleteExisting").focus();
        return false;
    }
    E = "{LoanId:'" + LoanId + "'}";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/GetDetails_AgainstSanctionNoExisting',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $("#tblExistingLoanDetailsbySanctionNo tbody").empty();
            if (D.d.length > 0) {
                var DtDetails = JSON.parse(D.d)
                var SlNo = 1;
                $("#SpanLoanSanctionAmount").text(DtDetails[0]["SanctionAmount"]);
                $.each(DtDetails, function (index, value) { 
                    $("#tblExistingLoanDetailsbySanctionNo tbody").append("<tr " + (index % 2 == 0 ? "" : "style='background-color:LemonChiffon;'") + ">" +
                        "<td align='center' class=''>" + SlNo + "</td>" +
                         "<td align='center' class='DisbursementOrderNo'>" + value.DisbursementOrderNo + "</td>" +
                         "<td align='center' class='DisbursementOrderDate' >" + value.DisbursementOrderDate + "</td>" +
                         "<td align='center' class='SanctionNo' style='display:none'>" + value.SanctionNo + "</td>" +
                         "<td align='center' class='LoanID' style='display:none'>" + value.LoanID + "</td>" +
                        "<td align='center' class='DisburseAmount'>" + value.DisburseAmount + "</td>" +
                        "<td class='btnc' align='center' ><input type='button' class='btnPrePaiment' value='Pre-Payment'/></td>" +
                        "<td class='btnc' align='center' ><input type='button' class='btnRetirementAdjustment' value='Retirement Adjustment'/></td>" +
                        "<td class='btnc' align='center' ><img src='img/Edit.jpg'  class='btnEdit' value='Edit'/></td></tr>"+
                        "</tr>");
                    SlNo = SlNo + 1;

                });
            }
        }
    });
}
    //****************************Against Already Sanction No****************************************//
function SaveLoanAgainstAlreadySanctionNo() {

    var RetirementAdjustmentDate = "";
    if ($("#txtAsdujtmentDateAgainstAlreadyDisburse").val() != "") {
        var iDate = ($("#txtAsdujtmentDateAgainstAlreadyDisburse").val()).split('/');
        RetirementAdjustmentDate = iDate[0] + "/" + iDate[1] + "/" + iDate[2];
    }
    else
        RetirementAdjustmentDate = "01" + "/" + "01" + "/" + "1500" ;


    var ArrayObjLoanMasterAgainstAlreadySanctionNo = [];
    var objLoanMasterAgainstAlreadySanctionNo = new Object();

    objLoanMasterAgainstAlreadySanctionNo.LoanId = LoanId;
    objLoanMasterAgainstAlreadySanctionNo.EmployeeId = EmployeeId;
    objLoanMasterAgainstAlreadySanctionNo.LoanDescriptionId = $("#ddlLoanDescription").val();
    objLoanMasterAgainstAlreadySanctionNo.SanctionNo = $("#txtSanctionNoAutoComplete").val();
    objLoanMasterAgainstAlreadySanctionNo.SanctionDate = $("#txtAlreadySanctionReferenceDate").val();
    objLoanMasterAgainstAlreadySanctionNo.SanctionAmount = $("#txtLoanSanctionAmountDisExistingLoan").val();



    objLoanMasterAgainstAlreadySanctionNo.DisbursementOrderNo = $("#DisbursementNoAgainstAlreadtSanctionNo").val();
    objLoanMasterAgainstAlreadySanctionNo.DisbursementOrderDate = $("#DisbursementDateAgainstAlreadtSanctionNo").val();

    objLoanMasterAgainstAlreadySanctionNo.DisbursementAmount = $("#DisbursementAmountAgainstAlreadtSanctionNo").val();

    objLoanMasterAgainstAlreadySanctionNo.DeductionStartDate = $("#DisbursementDeductionStartDtAgainstAlreadtSanctionNo").val();
    objLoanMasterAgainstAlreadySanctionNo.InterestOnDisbursementAmount = $("#DisbursementIneterestAgainstAlreadtSanctionNo").val();



    //
    objLoanMasterAgainstAlreadySanctionNo.TotalLoanInstall = $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val();
    objLoanMasterAgainstAlreadySanctionNo.LoanPrincipalAmount = $('#DisbursementPrincipalAmountPaidAgainstAlreadtSanctionNo').val();
    objLoanMasterAgainstAlreadySanctionNo.LoanInterestAmount = $('#DisbursementInterestAmountPaidAgainstAlreadtSanctionNo').val();
    //1st Slab
    objLoanMasterAgainstAlreadySanctionNo.FrstInstallmentNo = ($("#FirstInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FirstInstallmentNoAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.FrstInstallmentPrincipleAmount = ($("#FirstPrincipleAgainstDisbursement").val() == "" ? 0 : $("#FirstPrincipleAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.FrstInstallmentInterestAmount = ($("#FirstInterestAgainstDisbursement").val() == "" ? 0 : $("#FirstInterestAgainstDisbursement").val());
    //2nd Slab
    objLoanMasterAgainstAlreadySanctionNo.secndInstallmentNo = ($("#SecondInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#SecondInstallmentNoAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.secndInstallmentPrincipleAmount = ($("#SecondPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#SecondPrincipleAmountAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.secndInstallmentInterestAmount = ($("#SecondInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#SecondInterestAmountAgainstDisbursement").val());
    //3rd Slab
    objLoanMasterAgainstAlreadySanctionNo.thirdInstallmentNo = ($("#ThirdInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#ThirdInstallmentNoAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.thirdInstallmentPrincipleAmount = ($("#ThirdPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#ThirdPrincipleAmountAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.thirdInstallmentInterestAmount = ($("#ThirdInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#ThirdInterestAmountAgainstDisbursement").val());
    //4th Slab
    objLoanMasterAgainstAlreadySanctionNo.FourthInstallmentNo = ($("#FourthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FourthInstallmentNoAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.FourthInstallmentPrincipleAmount = ($("#FourthPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#FourthPrincipleAmountAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.FourthInstallmentInterestAmount = ($("#FourthInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#FourthInterestAmountAgainstDisbursement").val());
    //5ht Slab
    objLoanMasterAgainstAlreadySanctionNo.FifthInstallmentNo = ($("#FifthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FifthInstallmentNoAgainstDisbursement").val());;
    objLoanMasterAgainstAlreadySanctionNo.FifthInstallmentPrincipleAmount = ($("#FifthPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#FifthPrincipleAmountAgainstDisbursement").val());
    objLoanMasterAgainstAlreadySanctionNo.FifthInstallmentInterestAmount = ($("#FifthInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#FifthInterestAmountAgainstDisbursement").val());
    //
    ArrayObjLoanMasterAgainstAlreadySanctionNo[0] = objLoanMasterAgainstAlreadySanctionNo;
    //Retirement Adjustment Slab
    objLoanMasterAgainstAlreadySanctionNo.RetirementPrincipleAmount = ($("#txtRetPrincipleAgainstAlreadyDisburse").val() == "" ? 0 : $("#txtRetPrincipleAgainstAlreadyDisburse").val());
    objLoanMasterAgainstAlreadySanctionNo.RetirementPrincipleAdjustmentAmount = ($("#txtRetAdjIntrstAmountAgainstAlreadyDisburse").val() == "" ? 0 : $("#txtRetAdjIntrstAmountAgainstAlreadyDisburse").val());
    objLoanMasterAgainstAlreadySanctionNo.RetirementAdjustmentDate = (RetirementAdjustmentDate);
    var E = "{param:" + JSON.stringify(ArrayObjLoanMasterAgainstAlreadySanctionNo) + "}";

    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Save_LoanDetailsAgainstAlreadySanctionNo',
        data: E,
        cache: false,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.d.length > 0) {
                var Result = JSON.parse(data.d);
                if (Result == "Success") {
                    alert("Loan Details Saved sucessfully");
                    window.location.href = "LoanMsterRevised.aspx";
                }
                else {
                    alert("Loan Details not Saved");
                }
            }
        }
    });

}  
function ValidateBeforSave_AgainstAlredySanctionNo() {
    var Status = true;
    if ($("#DisbursementNoAgainstAlreadtSanctionNo").val() == "") {
        alert("Please Enter Disbursement Order No!");
        $("#DisbursementNoAgainstAlreadtSanctionNo").focus();
        Status = false;
    }

    else if ($("#DisbursementDateAgainstAlreadtSanctionNo").val() == "") {
        alert("Please select Disbursement Date!");
        $("#DisbursementDateAgainstAlreadtSanctionNo").focus();
        Status = false;
    }
    else if ($("#DisbursementAmountAgainstAlreadtSanctionNo").val() == "") {
        alert("Please enter Disbursement Amount!");
        $("#DisbursementAmountAgainstAlreadtSanctionNo").focus();
        Status = false;
    }
    else if ($("#DisbursementIneterestAgainstAlreadtSanctionNo").val() == "") {
        alert("Please enter Disbursement Ineterest Amount!");
        $("#DisbursementIneterestAgainstAlreadtSanctionNo").focus();
        Status = false;
    }
    else if ($("#DisbursementDeductionStartDtAgainstAlreadtSanctionNo").val() == "") {
        alert("Please select  Deduction Start Date!");
        $("#DisbursementDeductionStartDtAgainstAlreadtSanctionNo").focus();
        Status = false;
    }
    else if ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() == "") {
        alert("Please enter Total Loan Install No!");
        $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").focus();
        Status = false;
    }

    return Status;
}
function CheckTotalInstallmentNoAgainstAlreadySanctionNo_With_SlabInstallNO() {
    var Status1 = true;
    var TotalLoanInstall = $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val();
    var TotalSlabInstallmentNo = 0;
    TotalSlabInstallmentNo = (parseInt($("#FirstInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FirstInstallmentNoAgainstDisbursement").val()) + parseInt($("#SecondInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#SecondInstallmentNoAgainstDisbursement").val()) + parseInt($("#ThirdInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#ThirdInstallmentNoAgainstDisbursement").val()) + parseInt($("#FourthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FourthInstallmentNoAgainstDisbursement").val()) + parseInt($("#FifthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FifthInstallmentNoAgainstDisbursement").val()));
    if (TotalLoanInstall != TotalSlabInstallmentNo) {
        alert("Total Installment No should be same with sum of slab Installment No!!");
        Status1 = false;
    }
    return Status1;
}
function CheckTotalDisbursementAmountAgainstAlreadySanctionNo_with_TotalPrincipleSlabAmount() {
    var Status2 = true;
    var TotalDisbursementAmount = $("#DisbursementAmountAgainstAlreadtSanctionNo").val();
    var TotalSlabPrincipleAmount = 0;

    var IstPrincipleAmount=parseFloat($("#FirstPrincipleAgainstDisbursement").val() == "" ? 0 : $("#FirstPrincipleAgainstDisbursement").val());
    var ScndPrincipleAmount = parseFloat($("#SecondPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#SecondPrincipleAmountAgainstDisbursement").val());
    var ThirdPrincipleAmount = parseFloat($("#ThirdPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#ThirdPrincipleAmountAgainstDisbursement").val());
    var FourthPrincipleAmount = parseFloat($("#FourthPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#FourthPrincipleAmountAgainstDisbursement").val());
    var FifthPrincipleAmount = parseFloat($("#FifthPrincipleAmountAgainstDisbursement").val() == "" ? 0 : $("#FifthPrincipleAmountAgainstDisbursement").val());

    

    var IstInstallNo = parseInt($("#FirstInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FirstInstallmentNoAgainstDisbursement").val());
    var SecondInstallNo = parseInt($("#SecondInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#SecondInstallmentNoAgainstDisbursement").val());
    var ThirdInstallNo = parseInt($("#ThirdInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#ThirdInstallmentNoAgainstDisbursement").val());
    var FourthInstallNo = parseInt($("#FourthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FourthInstallmentNoAgainstDisbursement").val());
    var FifthInstallNo = parseInt($("#FifthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FifthInstallmentNoAgainstDisbursement").val());

    var RetAdjPrincipleAmount = parseFloat($("#txtRetPrincipleAgainstAlreadyDisburse").val() == "" ? 0 : $("#txtRetPrincipleAgainstAlreadyDisburse").val());
    if (IstPrincipleAmount < 0 || ScndPrincipleAmount < 0 || ThirdPrincipleAmount < 0 || FourthPrincipleAmount < 0 || FifthPrincipleAmount < 0) {
        alert("Installment Slab Amount can't be negative!!");
        Status2 = false;
    }
    else {
        TotalSlabPrincipleAmount = (IstPrincipleAmount * IstInstallNo) + (ScndPrincipleAmount * SecondInstallNo) + (ThirdPrincipleAmount * ThirdInstallNo) + (FourthPrincipleAmount * FourthInstallNo) + (FifthPrincipleAmount * FifthInstallNo) + (RetAdjPrincipleAmount);
        if (TotalSlabPrincipleAmount != TotalDisbursementAmount) {
            alert("Disbursement Amount should be same with sum of slab Installment Principle Amount!!");
            Status2 = false;
        }
    }
    return Status2;
}
function CheckTotalDisbursementInterestAmountAgainstAlreadySanctionNo_with_TotalInterestSlabAmount() {
    var Status3 = true;
    var TotalDisbursementInterestAmount = $("#DisbursementIneterestAgainstAlreadtSanctionNo").val();
    var TotalSlabInterestAmount = 0;

    var IstInterestAmount = parseFloat($("#FirstInterestAgainstDisbursement").val() == "" ? 0 : $("#FirstInterestAgainstDisbursement").val());
    var ScndInterestAmount = parseFloat($("#SecondInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#SecondInterestAmountAgainstDisbursement").val());
    var ThirdInterestAmount = parseFloat($("#ThirdInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#ThirdInterestAmountAgainstDisbursement").val());
    var FourthInterestAmount = parseFloat($("#FourthInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#FourthInterestAmountAgainstDisbursement").val());
    var FifthInterestAmount = parseFloat($("#FifthInterestAmountAgainstDisbursement").val() == "" ? 0 : $("#FifthInterestAmountAgainstDisbursement").val());

    var IstInstallNo = parseInt($("#FirstInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FirstInstallmentNoAgainstDisbursement").val());
    var SecondInstallNo = parseInt($("#SecondInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#SecondInstallmentNoAgainstDisbursement").val());
    var ThirdInstallNo = parseInt($("#ThirdInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#ThirdInstallmentNoAgainstDisbursement").val());
    var FourthInstallNo = parseInt($("#FourthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FourthInstallmentNoAgainstDisbursement").val());
    var FifthInstallNo = parseInt($("#FifthInstallmentNoAgainstDisbursement").val() == "" ? 0 : $("#FifthInstallmentNoAgainstDisbursement").val());

    var RetAdjInterestAmount = parseFloat($("#txtRetAdjIntrstAmountAgainstAlreadyDisburse").val() == "" ? 0 : $("#txtRetAdjIntrstAmountAgainstAlreadyDisburse").val());
    if (IstInterestAmount < 0 || ScndInterestAmount < 0 || ThirdInterestAmount < 0 || FourthInterestAmount < 0 || FifthInterestAmount < 0) {
        alert("Installment Slab Interest Amount can't be negative!!");
        Status3 = false;
    }
    else {

        TotalSlabInterestAmount = (IstInterestAmount * IstInstallNo) + (ScndInterestAmount * SecondInstallNo) + (ThirdInterestAmount * ThirdInstallNo) + (FourthInterestAmount * FourthInstallNo) + (FifthInterestAmount * FifthInstallNo) + (RetAdjInterestAmount);
        if (TotalSlabInterestAmount != TotalDisbursementInterestAmount) {
            alert("Interest on Disbursement Amount should be same with sum of slab Installment Interest Amount!!");
            Status3 = false;
        }
    }
    return Status3;
}
function CheckDisbursementAmountAgainstAlreadySanctionNo() {
    var Status = true;
    var LaftDisbursementAmount = $("#LeftDisbursementAmount").text();
    var DisbursementAmountAgainstAlreadtSanctionNo=$("#DisbursementAmountAgainstAlreadtSanctionNo").val();
    if (parseFloat(DisbursementAmountAgainstAlreadtSanctionNo == "" ? 0 : DisbursementAmountAgainstAlreadtSanctionNo) > parseFloat(LaftDisbursementAmount == "XXXXXXXX" ? 0 : LaftDisbursementAmount)) {
        Status = false;
        alert("Disbursement Amount alaways be less or equal to Left Disbursement Amount!");
    }
    return Status;
}
function CheckDisbursementOrderDateAgainstAlreadySanctionNo() {
    //to be conitinue.......
}
function ValidateTotalNoOfInstallmentNoWithDORAgainstAlreadySancton() {
    var Status = true;
    var NoOfMonthsFromDor = ($("#NoOfMonths").text() == "" ? 0 : $("#NoOfMonths").text());
    var NoOfInstallmentNo = ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() == "" ? 0 : $("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val());
    if (parseFloat(NoOfMonthsFromDor) < parseFloat(NoOfInstallmentNo)) {
        alert("No of Installment No should be equal or less then No of months left from Retirement!");
        Status = true;
    }
    return Status;
}
   //*****************************End******************************************************************//
function PopulateGridAgainstAlreadySanctionNoDisbursementPage() {
    E = "{LoanId:'" + LoanId + "'}";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/GetDetails_AgainstSanctionNoExisting',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $("#tblDetails tbody").empty();
            if (D.d.length > 0) {
                var DtDetails = JSON.parse(D.d)
                var SlNo = 1;
                $.each(DtDetails, function (index, value) {
                    $("#tblDetails tbody").append("<tr " + (index % 2 == 0 ? "" : "style='background-color:LemonChiffon;'") + ">" +
                        "<td align='center' class=''>" + SlNo + "</td>" +
                         "<td align='center' class='DisbursementOrderNo'>" + value.DisbursementOrderNo + "</td>" +
                         "<td align='center' class='DisbursementOrderDate' >" + value.DisbursementOrderDate + "</td>" +
                         "<td align='center' class='SanctionNo' style='display:none'>" + value.SanctionNo + "</td>" +
                        "<td align='center' class='DisburseAmount'>" + value.DisburseAmount + "</td></tr>");
                    SlNo = SlNo + 1;

                });
            }
        }
    });
}
function GenerateAutoDisbursementOrderNo() {
    var EmployeeNo = $("#txtEmpNoSearch").val();
    var SanctionNo = $("#txtSanctionNoAutoComplete").val();
    var NewSanctionNo = SanctionNo.substring(0, (SanctionNo.length - 13));
    var DIsbursementOrderNo = "";
    var E = "{ SanctionNo: '" + NewSanctionNo + "' }";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Get_SanctionNoToGenerateNewDisbursementNo',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var t = JSON.parse(data.d);
            if (t == "YES") {
                DIsbursementOrderNo = EmployeeNo + '-' + NewSanctionNo + '-' + '1'
            }
            else {
                var s = t.length;
                var b = t.substring(2, 3);
                var LastDigits = t.substring((t.length - 1), t.length);
                LastDigits = parseInt(LastDigits) + 1;
                DIsbursementOrderNo = EmployeeNo + '-' + NewSanctionNo + '-' + LastDigits
                $("#DisbursementNoAgainstAlreadtSanctionNo").val(DIsbursementOrderNo);

            }

        }
    });
}
function GenerateAutoDisbursementOrderNo1stTime() {
    var SanctionNo = $("#txtNewSanctionNo").val();
    if (SanctionNo != "") {
        var EmployeeNo = $("#txtEmpNoSearch").val().split('--')[0];
        var NewSanctionNo = SanctionNo.substring(0, (SanctionNo.length - 13));
        var DIsbursementOrderNo = "";
        DIsbursementOrderNo = EmployeeNo + '-' + SanctionNo + '-' + '1'
        $("#txtDisbursementOrderNo").val(DIsbursementOrderNo);
    }
}
function Save_PrepaymentAmount() {
    var RepaymentDate = $("#txtRepaymentDate").val();
    var RepaymentPrincipleAmount = $("#txtRepaymentPrincipleAmount").val();
    var RepaymentInterestAmount = $("#txtRepaymentPrincipleAmount").val();
    var RepaymentInterestAdjusmentAmount = $("#txtRepaymentPrincipleAmount").val();
    var LoanId = $(".btnPrePaiment").attr("LoanID");
    var ArrayRepayment = [];
    var objRepayment = new Object();
    objRepayment.PrepaymentDate = RepaymentDate;
    objRepayment.PrepaymentPrincipleAmount = RepaymentPrincipleAmount;
    objRepayment.PrepaymentInterestAmount = RepaymentInterestAmount;
    objRepayment.PrepaymentInterestAdjusmentAmount = RepaymentInterestAdjusmentAmount;
    objRepayment.LoanId = LoanId;

    ArrayRepayment[0] = objRepayment;
    var E = "{param:" + JSON.stringify(ArrayRepayment) + "}";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Save_PrepaymentAmount',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var t = JSON.parse(data.d);
            if (t == "Success") {
                alert("Pre-Payment Details Saved Sucessfully!!");
                RemovePopUpdateAfterPrepaymentSave();
            }

        }
    });
}
function validateBeforePrepaymentSave() {
    var Status = true;
    if ($("#txtRepaymentDate").val() == "") {
        alert("Please select PrePayment Date!");
        $("#txtNewSanctionNo").focus();
        Status = false;
    }
    else if ($("#txtRepaymentPrincipleAmount").val() == "") {
        alert("Please Enter PrePayment Amount!");
        $("#txtLoanSanctionDate").focus();
        Status = false;
    }
    return Status;
}
function RemovePopUpdateAfterPrepaymentSave() {
    $("#txtRepaymentDate").val('');
    $("#txtRepaymentPrincipleAmount").val('');
    $("#txtRepaymentPrincipleAmount").val('');
    $("#txtRepaymentPrincipleAmount").val('');
    $(".btnPrePaiment").attr("LoanID", '');
};
function Save_RetirementAdjustment() {
    var RetAdjDate = $("#txtRetAdjDate").val();
    var RetAdjPrinAmt = $("#txtRetAdjPrinAmt").val();
    var RetAdjIntAmt = $("#txtRetAdjIntAmt").val();
    var LoanId = $(".btnRetirementAdjustment").attr("LoanID");
    var ArrayRetirementpayment = [];
    var objRetAdjpayment = new Object();
    objRetAdjpayment.RetAdjDate = RetAdjDate;
    objRetAdjpayment.RetAdjPrinAmt = RetAdjPrinAmt;
    objRetAdjpayment.RetAdjIntAmt = RetAdjIntAmt;
    objRetAdjpayment.LoanId = LoanId;

    ArrayRetirementpayment[0] = objRetAdjpayment;
    var E = "{param:" + JSON.stringify(ArrayRetirementpayment) + "}";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Save_RetirementpaymentAmount',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var t = JSON.parse(data.d);
            if (t == "Success") {
                alert("Retirement Adjustment Details Saved Sucessfully!!");
                RemovePopUpdateAfterPrepaymentSave();
            }

        }
    });
}
function validateBeforeRetirementpaymentSave() {
    var Status = true;
    if ($("#txtRetAdjDate").val() == "") {
        alert("Please select  Date!");
        $("#txtNewSanctionNo").focus();
        Status = false;
    }
    else if ($("#txtRetAdjPrinAmt").val() == "") {
        alert("Please Enter  Amount!");
        $("#txtLoanSanctionDate").focus();
        Status = false;
    }
    return Status;
}
function Get_EncodedValue_BySector() {
    var Satus = true;
    var E = "{SectorId:'" + $("#ddlSector").val() + "'}";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Get_EncodedValuebySectorID',
        data: E,
        async:false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var t = JSON.parse(data.d);
            if (t.length>0) {
                alert("Salary has already been  processed for this month.You can not give input SORRY!");
                Satus = false;
            }

        }
    });
    return Satus;
}

function Get_RetirementBenifitByLoanId(LoanId) {
    var E = "{LoanId:'" + LoanId + "'}";
    $.ajax({
        type: "POST",
        url: 'LoanMsterRevised.aspx/Get_RetirementBenifitByLoanId',
        data: E,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var t = JSON.parse(data.d);
            if (t.length > 0) {
                var RetAdjAmountDate = t[0]["RetAdjAmountDate"];
                var RetAdjPrincleAmt = t[0]["RetAdjPrincleAmt"];
                var RetAdjInterestAmt = t[0]["RetAdjInterestAmt"];
                $("#txtRetAdjDate").val(RetAdjAmountDate);
                $("#txtRetAdjPrinAmt").val(RetAdjPrincleAmt);
                $("#txtRetAdjIntAmt").val(RetAdjInterestAmt);
            }
        }
    });
}
//*************************************************************************

function CalculateInstallmentDetailsAuto() {
    var calculationAmount1stSlab = 0;
    var RemainderAmount = 0;
    var TotalInstallMentNo = ($("#txtTotalLoanInstall").val() == "" ? 0 : parseFloat($("#txtTotalLoanInstall").val()));
    var TotalDisBursementAmount = ($("#txtLoanDisbursementAmount").val() == "" ? 0 : parseFloat($("#txtLoanDisbursementAmount").val()));
    var TotalInterestAmount = ($("#txtLoanDisbursementAmount").val() == "" ? 0 : parseFloat($("#txtLoanDisbursementAmount").val()));

    var TotalDisBursementAmountInt = Math.floor(TotalDisBursementAmount);
    var TotalDisBursementAmountDecimalPart = (parseFloat(TotalDisBursementAmount) % 1).toFixed(2);

    calculationAmount1stSlab = parseFloat(TotalDisBursementAmountInt) / parseFloat(TotalInstallMentNo);
    RemainderAmount = parseFloat(TotalDisBursementAmountInt) % parseFloat(TotalInstallMentNo);
    if ((RemainderAmount != 0 || TotalDisBursementAmountDecimalPart != 0 ) && TotalInstallMentNo != 1) {
        //**********************1St Slab***********************
        $("#FrstInstallmentNo").val(parseFloat(TotalInstallMentNo) - 1);
        $("#FrstInstallmentPrincipleAmount").val(Math.round(calculationAmount1stSlab));
        //**********************2nd Slab***********************
        $("#secndInstallmentNo").val(parseFloat(1));
        var restInstallNo = parseFloat(TotalInstallMentNo) - 1;
        var s = (Math.round(parseFloat(calculationAmount1stSlab)) * parseFloat(restInstallNo));
        var ScndPAmount = Math.round(parseFloat(TotalDisBursementAmountInt) - parseFloat(s));
        $("#secndInstallmentPrincipleAmount").val(ScndPAmount + parseFloat(TotalDisBursementAmountDecimalPart));
    }
    else if (TotalInstallMentNo == 1) {
        //*************************1St Slab********************
        $("#FrstInstallmentNo").val(parseFloat(TotalInstallMentNo));
        $("#FrstInstallmentPrincipleAmount").val(TotalDisBursementAmount);

        //**********************2nd Slab***********************
        $("#secndInstallmentNo").val(0);
        $("#secndInstallmentPrincipleAmount").val(0);
    }
    else {
        //*************************1St Slab********************
        $("#FrstInstallmentNo").val(parseFloat(TotalInstallMentNo));
        $("#FrstInstallmentPrincipleAmount").val(Math.round(calculationAmount1stSlab));

        //**********************2nd Slab***********************
        $("#secndInstallmentNo").val(0);
        $("#secndInstallmentPrincipleAmount").val(0);
    }

}
function CalculateInterestInstallmentDetailsAuto() {

    var calculationAmount1stSlab = 0;
    var RemainderAmount = 0;
    var TotalInstallMentNo = ($("#txtTotalLoanInstall").val() == "" ? 0 : parseFloat($("#txtTotalLoanInstall").val()));
    var TotalInterestAmount = ($("#txtInterestOnDisbursementAmount").val() == "" ? 0 : parseFloat($("#txtInterestOnDisbursementAmount").val()));



    var TotalInterestAmountInt = Math.floor(TotalInterestAmount);
    var TotalInterestAmountDecimalPart = (parseFloat(TotalInterestAmount) % 1).toFixed(2);

    calculationAmount1stSlab = parseFloat(TotalInterestAmountInt) / parseFloat(TotalInstallMentNo);
    RemainderAmount = parseFloat(TotalInterestAmountInt) % parseFloat(TotalInstallMentNo);
    if ((RemainderAmount != 0 || TotalInterestAmountDecimalPart !=0 ) &&  TotalInstallMentNo != 1) {
        //**********************1St Slab***********************
        $("#FrstInstallmentNo").val(parseFloat(TotalInstallMentNo) - 1);
        $("#FrstInstallmentInterestAmount").val(Math.round(calculationAmount1stSlab));
        //**********************2nd Slab***********************
        $("#secndInstallmentNo").val(parseFloat(1));
        var restInstallNo = parseFloat(TotalInstallMentNo) - 1;
        var s = (Math.round(parseFloat(calculationAmount1stSlab)) * parseFloat(restInstallNo));
        var ScndPAmount = Math.round(parseFloat(TotalInterestAmountInt) - parseFloat(s));
        $("#secndInstallmentInterestAmount").val(ScndPAmount + parseFloat(TotalInterestAmountDecimalPart));
    }
    else if (TotalInstallMentNo == 1) {
        //*************************1St Slab********************

        $("#FrstInstallmentNo").val(parseFloat(TotalInstallMentNo));
        $("#FrstInstallmentInterestAmount").val(TotalInterestAmount);

        //**********************2nd Slab***********************
        $("#secndInstallmentNo").val(0);
        $("#secndInstallmentInterestAmount").val(0);
    }
    else {
        //*************************1St Slab********************
        $("#FrstInstallmentNo").val(parseFloat(TotalInstallMentNo));
        $("#FrstInstallmentInterestAmount").val(Math.round(calculationAmount1stSlab));

        //**********************2nd Slab***********************
        $("#secndInstallmentNo").val(0);
        $("#secndInstallmentInterestAmount").val(0);
    }
}
function RefreshAllInstallTAB() {
    //********************InstallmentNo******************************
    $("#FrstInstallmentNo").val(0);
    $("#secndInstallmentNo").val(0);
    $("#thirdInstallmentNo").val(0);
    $("#FourthInstallmentNo").val(0);
    $("#FifthInstallmentNo").val(0);

    //********************Installment Principle Amount******************************

    $("#FrstInstallmentPrincipleAmount").val(0);
    $("#secndInstallmentPrincipleAmount").val(0);
    $("#thirdInstallmentPrincipleAmount").val(0);
    $("#FourthInstallmentPrincipleAmount").val(0);
    $("#FifthInstallmentPrincipleAmount").val(0);

    //********************Installment InterastAmount******************************
    $("#FrstInstallmentInterestAmount").val(0);
    $("#secndInstallmentInterestAmount").val(0);
    $("#thirdInstallmentInterestAmount").val(0);
    $("#FourthInstallmentInterestAmount").val(0);
    $("#FifthInstallmentInterestAmount").val(0);

}

//*************************Auto calculate installment tab against already sanction no********************************

function CalculateInstallmentDetailsAutoAgainstalreadySanctionNo() {

    var calculationAmount1stSlab = 0;
    var RemainderAmount = 0;
    var TotalInstallMentNo = ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() == "" ? 0 : parseFloat($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val()));
    var TotalDisBursementAmount = ($("#DisbursementAmountAgainstAlreadtSanctionNo").val() == "" ? 0 : parseFloat($("#DisbursementAmountAgainstAlreadtSanctionNo").val()));

    var TotalDisBursementAmountInt = Math.floor(TotalDisBursementAmount);
    var TotalDisBursementAmountDecimalPart = (parseFloat(TotalDisBursementAmount) % 1).toFixed(2);

    calculationAmount1stSlab = parseFloat(TotalDisBursementAmountInt) / parseFloat(TotalInstallMentNo);
    RemainderAmount = parseFloat(TotalDisBursementAmountInt) % parseFloat(TotalInstallMentNo);
    if ((RemainderAmount != 0 || TotalDisBursementAmountDecimalPart != 0) && TotalInstallMentNo != 1) {
        //**********************1St Slab***********************
        $("#FirstInstallmentNoAgainstDisbursement").val(parseFloat(TotalInstallMentNo) - 1);
        $("#FirstPrincipleAgainstDisbursement").val(Math.round(calculationAmount1stSlab));
        //**********************2nd Slab***********************
        $("#SecondInstallmentNoAgainstDisbursement").val(parseFloat(1));
        var restInstallNo = parseFloat(TotalInstallMentNo) - 1;
        var s = (Math.round(parseFloat(calculationAmount1stSlab)) * parseFloat(restInstallNo));
        var ScndPAmount = Math.round(parseFloat(TotalDisBursementAmountInt) - parseFloat(s));
        $("#SecondPrincipleAmountAgainstDisbursement").val(ScndPAmount + parseFloat(TotalDisBursementAmountDecimalPart));
    }
    else if (TotalInstallMentNo==1) {
        //*************************1St Slab********************
        $("#FirstInstallmentNoAgainstDisbursement").val(parseFloat(TotalInstallMentNo));
        $("#FirstPrincipleAgainstDisbursement").val(TotalDisBursementAmount);

        //**********************2nd Slab***********************
        $("#SecondInstallmentNoAgainstDisbursement").val(0);
        $("#SecondPrincipleAmountAgainstDisbursement").val(0);
       }
    else {
        //*************************1St Slab********************
        $("#FirstInstallmentNoAgainstDisbursement").val(parseFloat(TotalInstallMentNo));
        $("#FirstPrincipleAgainstDisbursement").val(Math.round(calculationAmount1stSlab));

        //**********************2nd Slab***********************
        $("#SecondInstallmentNoAgainstDisbursement").val(0);
        $("#SecondPrincipleAmountAgainstDisbursement").val(0);
       }

  }
function CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo() {

    var calculationAmount1stSlab = 0;
    var RemainderAmount = 0;
    var TotalInstallMentNo = ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() == "" ? 0 : parseFloat($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val()));
    var TotalInterestAmount = ($("#DisbursementIneterestAgainstAlreadtSanctionNo").val() == "" ? 0 : parseFloat($("#DisbursementIneterestAgainstAlreadtSanctionNo").val()));

    var TotalInterestAmountInt = Math.floor(TotalInterestAmount);
    var TotalInterestAmountDecimalPart = (parseFloat(TotalInterestAmount) % 1).toFixed(2);

    calculationAmount1stSlab = parseFloat(TotalInterestAmountInt) / parseFloat(TotalInstallMentNo);
    RemainderAmount = parseFloat(TotalInterestAmountInt) % parseFloat(TotalInstallMentNo);
    if ((RemainderAmount != 0 || TotalInterestAmountDecimalPart != 0 ) && TotalInstallMentNo != 1) {
        //**********************1St Slab***********************
        $("#FirstInstallmentNoAgainstDisbursement").val(parseFloat(TotalInstallMentNo) - 1);
        $("#FirstInterestAgainstDisbursement").val(Math.round(calculationAmount1stSlab));
        //**********************2nd Slab***********************
        $("#SecondInstallmentNoAgainstDisbursement").val(parseFloat(1));
        var restInstallNo = parseFloat(TotalInstallMentNo) - 1;
        var s = (Math.round(parseFloat(calculationAmount1stSlab)) * parseFloat(restInstallNo));
        var ScndPAmount = Math.round(parseFloat(TotalInterestAmountInt) - parseFloat(s));
        $("#SecondInterestAmountAgainstDisbursement").val(ScndPAmount + parseFloat(TotalInterestAmountDecimalPart));
    }
    else if (TotalInstallMentNo == 1) {
        //*************************1St Slab********************
        $("#FirstInstallmentNoAgainstDisbursement").val(parseFloat(TotalInstallMentNo));
        $("#FirstInterestAgainstDisbursement").val(TotalInterestAmount);

        //**********************2nd Slab***********************
        $("#SecondInstallmentNoAgainstDisbursement").val(0);
        $("#SecondInterestAmountAgainstDisbursement").val(0);
    }
    else {
        //*************************1St Slab********************
        $("#FirstInstallmentNoAgainstDisbursement").val(parseFloat(TotalInstallMentNo));
        $("#FirstInterestAgainstDisbursement").val(Math.round(calculationAmount1stSlab));

        //**********************2nd Slab***********************
        $("#SecondInstallmentNoAgainstDisbursement").val(0);
        $("#SecondInterestAmountAgainstDisbursement").val(0);
    }

}
function RefreshAllInstallTABAgainstDisbursement() {
    //********************InstallmentNo******************************
    $("#FirstInstallmentNoAgainstDisbursement").val(0);
    $("#SecondInstallmentNoAgainstDisbursement").val(0);
    $("#ThirdInstallmentNoAgainstDisbursement").val(0);
    $("#FourthInstallmentNoAgainstDisbursement").val(0);
    $("#FifthInstallmentNoAgainstDisbursement").val(0);

    //********************Installment Principle Amount******************************

    $("#FirstPrincipleAgainstDisbursement").val(0);
    $("#SecondPrincipleAmountAgainstDisbursement").val(0);
    $("#ThirdPrincipleAmountAgainstDisbursement").val(0);
    $("#FourthPrincipleAmountAgainstDisbursement").val(0);
    $("#FifthPrincipleAmountAgainstDisbursement").val(0);

    //********************Installment InterastAmount******************************
    $("#FirstInterestAgainstDisbursement").val(0);
    $("#SecondInterestAmountAgainstDisbursement").val(0);
    $("#ThirdInterestAmountAgainstDisbursement").val(0);
    $("#FourthInterestAmountAgainstDisbursement").val(0);
    $("#FifthInterestAmountAgainstDisbursement").val(0);

}

//*********************************************************
function CalculateAutoField() {
    var RemainderAmountPrinc = 0;
    var RemainderAmountInst = 0;
    var calculationAmount1stSlab = 0;

    var TotalInstallMentNo = ($("#txtTotalLoanInstall").val() == "" ? 0 : parseFloat($("#txtTotalLoanInstall").val()));
    var TotalDisBursementAmount = ($("#txtLoanDisbursementAmount").val() == "" ? 0 : parseFloat($("#txtLoanDisbursementAmount").val()));
    var TotalInterestAmount = ($("#txtInterestOnDisbursementAmount").val() == "" ? 0 : parseFloat($("#txtInterestOnDisbursementAmount").val()));


    var TotalDisBursementAmountDecimalPart = (parseFloat(TotalDisBursementAmount) % 1).toFixed(2);
    var TotalInterestAmountDecimalPart = (parseFloat(TotalInterestAmount) % 1).toFixed(2);

    RemainderAmountPrinc = parseFloat(Math.floor(TotalDisBursementAmount)) % parseFloat(TotalInstallMentNo);
    RemainderAmountInst = parseFloat(Math.floor(TotalInterestAmount)) % parseFloat(TotalInstallMentNo);
    if (TotalDisBursementAmountDecimalPart == 0 && TotalInterestAmountDecimalPart == 0 && RemainderAmountPrinc == 0 && RemainderAmountInst==0)
    {
        CalculateInstallmentDetailsAuto();
        CalculateInterestInstallmentDetailsAuto();
    }
    else {  
    
    if (RemainderAmountPrinc != 0 || TotalDisBursementAmountDecimalPart !=0) {
        if (RemainderAmountInst == 0 && TotalInterestAmountDecimalPart == 0) {
            CalculateInstallmentDetailsAuto();
            calculationAmount1stSlab = parseFloat(TotalInterestAmount) / parseFloat(TotalInstallMentNo);
            $("#FrstInstallmentInterestAmount").val(Math.round(calculationAmount1stSlab));
            $("#secndInstallmentInterestAmount").val(Math.round(calculationAmount1stSlab));
        }
        else {
            CalculateInstallmentDetailsAuto();
            CalculateInterestInstallmentDetailsAuto();
        }
    }
    else if (RemainderAmountPrinc == 0 || TotalDisBursementAmountDecimalPart == 0) {
        if (RemainderAmountInst != 0 || TotalInterestAmountDecimalPart != 0) {
            CalculateInterestInstallmentDetailsAuto();
            calculationAmount1stSlab = parseFloat(TotalDisBursementAmount) / parseFloat(TotalInstallMentNo);
            $("#FrstInstallmentPrincipleAmount").val(Math.round(calculationAmount1stSlab));
            $("#secndInstallmentPrincipleAmount").val(Math.round(calculationAmount1stSlab));
        }
        else {
            CalculateInstallmentDetailsAuto();
            CalculateInterestInstallmentDetailsAuto();
        }
    }
    else {
        CalculateInstallmentDetailsAuto();
        CalculateInterestInstallmentDetailsAuto();
    }
    }
}
function CalculateAutoFieldGainstAlreadySanction() {
    var RemainderAmountPrinc = 0;
    var RemainderAmountInst = 0;
    var calculationAmount1stSlab = 0;

    var TotalInstallMentNo = ($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val() == "" ? 0 : parseFloat($("#DisbursementTotalLoanInstallAgainstAlreadtSanctionNo").val()));
    var TotalDisBursementAmount = ($("#DisbursementAmountAgainstAlreadtSanctionNo").val() == "" ? 0 : parseFloat($("#DisbursementAmountAgainstAlreadtSanctionNo").val()));
    var TotalInterestAmount = ($("#DisbursementIneterestAgainstAlreadtSanctionNo").val() == "" ? 0 : parseFloat($("#DisbursementIneterestAgainstAlreadtSanctionNo").val()));


    var TotalDisBursementAmountDecimalPart = (parseFloat(TotalDisBursementAmount) % 1).toFixed(2);
    var TotalInterestAmountDecimalPart = (parseFloat(TotalInterestAmount) % 1).toFixed(2);

    RemainderAmountPrinc = parseFloat(Math.floor(TotalDisBursementAmount)) % parseFloat(TotalInstallMentNo);
    RemainderAmountInst = parseFloat(Math.floor(TotalInterestAmount)) % parseFloat(TotalInstallMentNo);
    if (TotalDisBursementAmountDecimalPart == 0 && TotalInterestAmountDecimalPart == 0 && RemainderAmountPrinc == 0 && RemainderAmountInst == 0) {
        CalculateInstallmentDetailsAutoAgainstalreadySanctionNo();
        CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo();
    }
    else {

        if (RemainderAmountPrinc != 0 || TotalDisBursementAmountDecimalPart != 0) {
            if (RemainderAmountInst == 0 && TotalInterestAmountDecimalPart == 0) {
                CalculateInstallmentDetailsAutoAgainstalreadySanctionNo();
                calculationAmount1stSlab = parseFloat(TotalInterestAmount) / parseFloat(TotalInstallMentNo);
                $("#FirstInterestAgainstDisbursement").val(Math.round(calculationAmount1stSlab));
                $("#SecondInterestAmountAgainstDisbursement").val(Math.round(calculationAmount1stSlab));
            }
            else {
                CalculateInstallmentDetailsAutoAgainstalreadySanctionNo();
                CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo();
            }
        }
        else if (RemainderAmountPrinc == 0 || TotalDisBursementAmountDecimalPart == 0) {
            if (RemainderAmountInst != 0 || TotalInterestAmountDecimalPart != 0) {
                CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo();
                calculationAmount1stSlab = parseFloat(TotalDisBursementAmount) / parseFloat(TotalInstallMentNo);
                $("#FirstPrincipleAgainstDisbursement").val(Math.round(calculationAmount1stSlab));
                $("#SecondPrincipleAmountAgainstDisbursement").val(Math.round(calculationAmount1stSlab));
            }
            else {
                CalculateInstallmentDetailsAutoAgainstalreadySanctionNo();
                CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo();
            }
        }
        else {
            CalculateInstallmentDetailsAutoAgainstalreadySanctionNo();
            CalculateInterestInstallmentDetailsAutoAgainstalreadySanctionNo();
        }
    }

    
}

$(document).ready(function () {
    $("#txtEmpNoSearch").autocomplete({
        source: function (request, response) {
            var E = "{txtEmpNoSearch:'" + $("#txtEmpNoSearch").val().trim() + "'}";
            $.ajax({
                type: "POST",
                url: "LoanMsterRevised.aspx/AutoComplete_SearchEmployee",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[1],
                            val: item.split('|')[0]
                        }
                    }))
                }
            });
        },
        //appendTo: "#AutoComplete-ParentMenu",
        select: function (e, i) {
            EmployeeId = i.item.val;
            Get_EmployeeDetails(i.item.label);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    function Get_EmployeeDetails(label) {
        //e.preventDefault();
        PopulateLoanDescription();
        var EmployeeNo = label.split('--')[0];
        if (EmployeeNo == "") {
            //jqAlert('Please Enter Employee No!!');
            //$('#showMessage').html('Please Enter Employee No!!');
            $("#txtEmpNoSearch").focus();
            return false;
        }
        else {
            //EmployeeNo = EmployeeNo;
            var E = "{EmployeeNo: '" + EmployeeNo + "'}";
            $.ajax({
                type: "POST",
                url: "LoanMsterRevised.aspx/GetEmployeeDetails",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var dataSource = JSON.parse(data.d);
                    EmployeeId = dataSource[0]["EmployeeId"];
                    var EmployeeName = dataSource[0]["EmployeeName"];
                    var Location = dataSource[0]["Location"];
                    var DOR = dataSource[0]["DOR"];
                    var NoOfLeftMonths = dataSource[0]["NoOfLeftMonths"];
                    //$("#hdnEmployeeId").val(EmployeeId);
                    $("#txtEmpName").val(EmployeeName);
                    $("#txtLocation").val(Location);
                    $("#txtDateofRetirement").val(DOR);
                    $("#NoOfMonths").text(NoOfLeftMonths);
                }
            });
        }
    }
    function PopulateLoanDescription() {
        $("#ddlLoanDesc,#ddlLoanDescription,#ddlLoanDescriptionExisting").empty();
        $("#ddlLoanDesc,#ddlLoanDescription,#ddlLoanDescriptionExisting").append("<option value='0'>(Select Loan Description)</option>");
        var E = "{}";
        $.ajax({
            type: "POST",
            url: 'LoanMsterRevised.aspx/GET_LoanDescription',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var t = JSON.parse(data.d);
                if (t.length > 0) {
                    $("#ddlLoanDesc,#ddlLoanDescription,#ddlLoanDescriptionExisting").empty();
                    $("#ddlLoanDesc,#ddlLoanDescription,#ddlLoanDescriptionExisting").append("<option value='0'>(Select Loan Description)</option>");
                    $.each(t, function (key, value) {
                        $("#ddlLoanDesc,#ddlLoanDescription,#ddlLoanDescriptionExisting").append($("<option></option>").val(value.LoanTypeID).html(value.LoanDesc));
                    });
                }

            }
        });
    }

});

