﻿
$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    $('#txtFromdate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 13 || iKeyCode != 46) {
            alert("Manual Entry Not Allowed");
        }
    });

    $('#txtFromdate').keydown(function (evt) {
        $('#txtFromdate').val('');
        return false;
    });

    $('#txtTodate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 13 || iKeyCode != 46) {
            alert("Manual Entry Not Allowed");
        }
    });

    $('#txtTodate').keydown(function (evt) {
        $('#txtTodate').val('');
        return false;
    });

    $('#txtEmpNo').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            alert("Only Numbers Allowed!");
            return false;
        }
    });

    $("#txtAmount").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            alert("Please Enter Numeric Value");
            return false;
        }
    });
});



$(document).ready(function () {
    $('#txtFromdate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy',
        onSelect: function (selected) {
            $("#txtTodate").datepicker("option", "minDate", selected)
        }
    });
});

$(document).ready(function () {
    $('#txtTodate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy',
        onSelect: function (selected) {
            $("#txtFromdate").datepicker("option", "maxDate", selected)

        }
    });
});

function unvalidate() {
    $("#form1").validate().currentForm = '';
    return true;
}
function cmdCancel_Click() {
    $('#txtEmpNo').val('');
    $('#txtEmpName').val('');
    $('#txtEmpDsg').val('');
    $('#txtEmpNo').focus();

    $('#txtFromdate').val('');
    $('#txtTodate').val('');
    $('#ddlEarnDedu').val('');
    $('#txtAmount').val('');
    $('#txtTotalErn').val('');
    $('#txtTotalDeduc').val('');
    $('#txtNet').val('');
    $('#txtRemarks').val('');
    $('#hdnEDType').val('');
    $('#EmpID').val('');

    var E = {};
    $.ajax({
        type: "POST",
        url: 'ArrearSalary.aspx/RefreshButton',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#ITaxArrearDetail').html(data.d);
            return false;
        },
        error: function (response) {
            alert('Some Error Occured!');
        }
    });
}

//This is for Add Arrear Details
$(document).ready(function () {
    $("#cmdAdd").live('click', function () {
        if ($("#txtEmpName").val() == "" || $("#txtEmpDsg").val() == "") {
            alert("Please Select Valid Employee Number To Add Details!");
            $("#txtEmpNo").focus();
            return false;
        }

        if ($("#txtFromdate").val() == "") {
            alert("Please Select From Date Field!");
            $("#txtFromdate").focus();
            return false;
        }

        if ($("#txtTodate").val() == "") {
            alert("Please Select To Date Field!");
            $("#txtTodate").focus();
            return false;
        }

        var EmpId = $("#EmpID").val();
        var EarnDeduID = $("#ddlEarnDedu").val();
        var EarnDeduText = $('#ddlEarnDedu').find('option:selected').text();
        var EarnDeduAmount = $("#txtAmount").val();
        if ($("#ddlEarnDedu").val() != "") {
            if ($("#txtAmount").val() != "") {
                E = "{EarnDeduID:'" + EarnDeduID + "', EarnDeduText:'" + EarnDeduText + "', EarnDeduAmount:'" + EarnDeduAmount + "',EmpId:'" + EmpId + "'}";
                $.ajax({
                    type: "POST",
                    url: "ArrearSalary.aspx/Add_EarnDeductionArrearDetail",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        if (msg.d == "Duplicate") {
                            alert("This Earning/Deduction Type Already Added!");
                            return false;
                        }
                        else if (msg.d == "Pan") {
                            alert("Please Provide PAN Details At Employee Details Before Adding iTax Deduction!");
                            return false;
                        }
                        else {
                            var total = 0;
                            if ($("#hdnEDType").val() == "E") {

                                if ($("#txtTotalErn").val() != "") {
                                    total = parseInt($("#txtTotalErn").val());
                                }
                                total = total + parseInt($("#txtAmount").val());
                                $("#txtTotalErn").val(total);
                            }
                            else if ($("#hdnEDType").val() == "D") {
                                if ($("#txtTotalDeduc").val() != "") {
                                    total = parseInt($("#txtTotalDeduc").val());
                                }
                                total = total + parseInt($("#txtAmount").val());
                                $("#txtTotalDeduc").val(total);
                            }
                            TotalCalculation();
                            $("#ITaxArrearDetail").empty().html(msg.d);
                            $("#ddlEarnDedu").val('');
                            $("#txtAmount").val('');
                        }
                    }
                });
            }
            else {
                $("#txtAmount").focus(); return true;
            }
        }
        else {
            $("#ddlEarnDedu").focus(); return true;
        }

    });
});

function TotalCalculation() {
    var TotEarn = 0;
    if ($("#txtTotalErn").val() != "") {
        TotEarn = parseInt($("#txtTotalErn").val());
    }

    var TotDedu = 0;
    if ($("#txtTotalDeduc").val() != "") {
        TotDedu = parseInt($("#txtTotalDeduc").val());
    }
    $("#txtNet").val(TotEarn - TotDedu);
}

//This is for Delete Arrear Details
$('#Itaxtype a').live('click', function () {

    var t = confirm("Are You sure you want to Delete ?");
    if (t == true) {
        flag = $(this).attr('id'); //alert(flag);

        var EarnDeductionID = $(this).attr('itemid'); //alert(SlNo);

        var E = "{EarnDeductionID: '" + EarnDeductionID + "' }";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: 'ArrearSalary.aspx/Delete_ITaxArrearDetail',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#ITaxArrearDetail').html(D.d);
                CountChange();
            }
        });
    }
});

function CountChange() {
    $.ajax({
        type: "POST",
        url: 'ArrearSalary.aspx/CountCheck',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (Data) {
            var amount = Data.d;
            var amountarr = amount.split(",");
            $("#txtTotalErn").val(amountarr[0]);
            $("#txtTotalDeduc").val(amountarr[1]);
            $("#txtNet").val(parseInt(amountarr[0]) - parseInt(amountarr[1]));
        }
    });
}


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(document).ready(function () {
    $("#txtEmpNo").keypress(function () {
        $("#txtEmpNo").autocomplete({
            source: function (request, response) {
                var SectorID = $('#ddlSector').val();
                if (SectorID == "0") {
                    alert("Please Select Sector To Select Employee Number!");
                    $("#txtEmpNo").val('');
                    return false;
                }
                var E = "";
                var text = request.term;
                if (text != "") {
                    E = "{prefix:'" + request.term + "'}";
                    $.ajax({
                        type: "POST",
                        url: 'ArrearSalary.aspx/AjaxGetGridCtrl',
                        data: E,
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var item = jQuery.parseJSON(data.d);
                            if (item.length > 0) {
                                var empName = item[0].EmployeeName;
                                var Designation = item[0].Designation;
                                var EmpId = item[0].EmployeeID;
                                var EmpSector = item[0].SectorID;

                                if (EmpSector == SectorID)
                                {
                                    $("#txtEmpName").val(empName);
                                    $("#txtEmpDsg").val(Designation);
                                    $("#EmpID").val(EmpId);

                                    SearchArrearSalary(EmpId);
                                }
                                else
                                {
                                    alert("Employee Number Is Not Valid For Your Sector!");
                                    $("#txtEmpNo").val('');
                                    return false;
                                }
                            }
                        }
                    });
                }
            },
            minLength: 6
        });
    });

    $("#txtEmpNo").keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            $("#txtEmpName").val('');
            $("#txtEmpDsg").val('');

            $("#hdnSaveUpdate").val("Save");
            SearchArrearSalary(0);
        }
    });
});

function SearchArrearSalary(EmpId)
{
    E = "{EmpId:'" + EmpId + "'}"; 
    $.ajax({
        type: "POST",
        url: 'ArrearSalary.aspx/GET_ArrearSalaryForUpdate',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var item = jQuery.parseJSON(data.d); 
            if (item != "NotFound")   //item.length > 0 || 
            {
                var day = ""; var month = ""; var year = "";
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                var FromDate = item[0].ArrearFrom;

                var dateString = FromDate.substr(6);
                var currentTime = new Date(parseInt(dateString));

                day = currentTime.getDate() < 10 ? '0' + currentTime.getDate() : currentTime.getDate();
                if (currentTime.getMonth() == 9)
                    month = parseInt(currentTime.getMonth()) + 1;
                else
                    month = currentTime.getMonth() < 10 ? '0' + (parseInt(currentTime.getMonth()) + 1) : (parseInt(currentTime.getMonth()) + 1);

                year = currentTime.getFullYear();

                var Fdate = day + "/" + month + "/" + year;
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                var Tday = ""; var Tmonth = ""; var Tyear = "";
                var ToDate = item[0].ArrearTo;

                var dateString = ToDate.substr(6);
                var currentTime = new Date(parseInt(dateString));

                Tday = currentTime.getDate() < 10 ? '0' + currentTime.getDate() : currentTime.getDate();
                if (currentTime.getMonth() == 9)
                    Tmonth = parseInt(currentTime.getMonth()) + 1;
                else
                    Tmonth = currentTime.getMonth() < 10 ? '0' + (parseInt(currentTime.getMonth()) + 1) : (parseInt(currentTime.getMonth()) + 1);

                Tyear = currentTime.getFullYear();

                var Tdate = Tday + "/" + Tmonth + "/" + Tyear;
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                var TotalEarning = item[0].TotalEarning;
                var TotalDeduction = item[0].TotalDeduction;
                var NetArrear = item[0].NetArrear;
                var Remarks = item[0].Remarks;
                var ArrearID = item[0].ArrearID;

                $("#txtFromdate").val(Fdate);
                $("#txtTodate").val(Tdate);
                $("#txtTotalErn").val(TotalEarning);
                $("#txtTotalDeduc").val(TotalDeduction);
                $("#txtNet").val(NetArrear);
                $("#txtRemarks").val(Remarks);

                $("#hdnSaveUpdate").val("Update");
                SearchArrearSalaryDetail(ArrearID);
            }
            else
            {
                $("#txtFromdate").val('');
                $("#txtTodate").val('');
                $("#txtTotalErn").val('');
                $("#txtTotalDeduc").val('');
                $("#txtNet").val('');
                $("#txtRemarks").val('');

                SearchArrearSalaryDetail(0);
            }
        }
    });
}

function SearchArrearSalaryDetail(ArrearID)
{
    E = "{ArrearID:'" + ArrearID + "'}";
    $.ajax({
        type: "POST",
        url: 'ArrearSalary.aspx/GET_ArrearSalaryDetailForUpdate',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //var item = jQuery.parseJSON(data.d);
            $("#ITaxArrearDetail").empty().html(data.d);
        }
    });
}

$(document).ready(function () {
    $('#pgingFooter a').live('click', function () {
        flag = $(this).attr('id'); //alert(flag);
        FillGrid(flag);
    });
});

$(document).ready(function () {
    $("#ddlEarnDedu").change(function () {
        if ($("#ddlEarnDedu").val() != "") {
            E = "{EDID:'" + $("#ddlEarnDedu").val() + "'}";

            $.ajax({
                type: "POST",
                url: 'ArrearSalary.aspx/GET_EarnOrDeducType',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var item = jQuery.parseJSON(data.d);
                    $("#hdnEDType").val(item);
                    return false;
                },
                error: function (response) {
                    alert("Connection Error!");
                    return false;
                },
            });
        }
    });
});

$(document).ready(function () {
    $("#cmdSave").click(function (event) {
        var EmpId = $("#EmpID").val();
        var FromDate = $("#txtFromdate").val();
        var ToDate = $("#txtTodate").val();
        var TotalEarn = $("#txtTotalErn").val();
        var TotalDeduc = $("#txtTotalDeduc").val();
        var NetArrear = $("#txtNet").val();
        var Remarks = $("#txtRemarks").val();
        var SaveUpdate= $("#hdnSaveUpdate").val();

        if (EmpId == "") {
            alert("Please Select Correct User Details!");
            $("#txtEmpNo").focus();
            return false;
        }

        if (FromDate == "") {
            alert("Please Select From Date Details!");
            $("#txtFromdate").focus();
            return false;
        }

        if (ToDate == "") {
            alert("Please Select To Date Details!");
            $("#txtTodate").focus();
            return false;
        }

        if (TotalEarn == "" || TotalEarn == "0") {
            alert("Please Select Earning Details!");
            $("#ddlEarnDedu").focus();
            return false;
        }

        if (TotalDeduc == "") {
            TotalDeduc = 0;
            //alert("Please Select Deduction Details!");
            //$("#ddlEarnDedu").focus();
            //return false;
        }

        if (parseInt(NetArrear) < 0) {
            alert("Net Arrear Can Not Be negative Amount!");
            $("#ddlEarnDedu").focus();
            return false;
        }

        if (Remarks == "") {
            alert("Please Select Remarks!");
            $("#txtRemarks").focus();
            return false;
        }

        var FDate = FromDate.split("/");
        var FromDate = FDate[2] + "-" + FDate[1] + "-" + FDate[0];

        var TDate = ToDate.split("/");
        var ToDate = TDate[2] + "-" + TDate[1] + "-" + TDate[0];

        E = "{EmpId:'" + EmpId + "', FromDate:'" + FromDate + "', ToDate:'" + ToDate + "', TotalEarn:'" + TotalEarn + "', TotalDeduc:'" + TotalDeduc + "', NetArrear:'" + NetArrear + "', Remarks:'" + Remarks + "'}";

        var URL = "";
        if (SaveUpdate == "Save")
            URL = 'ArrearSalary.aspx/Save_ArrearSalMasterAndDetail';
        else
            URL = 'ArrearSalary.aspx/Update_ArrearSalMasterAndDetail';
        $.ajax({
            type: "POST",
            url: URL,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                if (data.d != 'Duplicate') {
                    alert(data.d);
                    cmdCancel_Click();
                }
                else {
                    alert("The Record With Same From And To Date Already EXISTS! Insertion Failed!");
                    cmdCancel_Click();
                }
            },
            error: function (response) {
                alert('Some Error Occured!');
                cmdCancel_Click();
            },
            failure: function (response) {
                alert(response.d);
                cmdCancel_Click();
            }
        });

    });
});