﻿/// <reference path="jquery-1.9.0.js" />
/// <reference path="utility.js" />
/// <reference path="json2.js" />
/// <reference path="ajaxfileupload.js" />
/// <reference path="jquery.iframe-transport.js" />
/// <reference path="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"/>
/// <reference path="jquery.qtip.js" />
/// <reference path="jquery.browser.js" />
/// <reference path="jquery.maskedinput.js" />

var yearmsg;
var curdt = $("#hdnCurdate").val();
function sleepFor(sleepDuration) {
    var now = new Date().getTime();
    while (new Date().getTime() < now + sleepDuration) { /* do nothing */ }
}
function getAge(dobstring,dateInput) {
    var now = new Date();
    //var today = new Date(now.getYear(), now.getMonth(), now.getDate());
    var today = new Date(dateInput.substring(6, 10),
                       dateInput.substring(3, 5) - 1,
                       dateInput.substring(0, 2)
                       );
    var yearNow = today.getYear();
    var monthNow = today.getMonth();
    var dateNow = today.getDate();

    var dob = new Date(dobstring.substring(6, 10),                      
                       dobstring.substring(3, 5)-1,
                       dobstring.substring(0, 2) 
                       );

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;    
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    ////////////if (age.years > 1) yearString = " years";
    ////////////else yearString = " year";
    ////////////if (age.months > 1) monthString = " months";
    ////////////else monthString = " month";
    ////////////if (age.days > 1) dayString = " days";
    ////////////else dayString = " day";


    ////////////if ((age.years > 0) && (age.months > 0) && (age.days > 0))
    ////////////    ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
    ////////////else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
    ////////////    ageString = "Only " + age.days + dayString + " old!";
    ////////////else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
    ////////////    ageString = age.years + yearString + " old. Happy Birthday!!";
    ////////////else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
    ////////////    ageString = age.years + yearString + " and " + age.months + monthString + " old.";
    ////////////else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
    ////////////    ageString = age.months + monthString + " and " + age.days + dayString + " old.";
    ////////////else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
    ////////////    ageString = age.years + yearString + " and " + age.days + dayString + " old.";
    ////////////else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
    ////////////    ageString = age.months + monthString + " old.";
    ////////////else ageString = "Oops! Could not calculate age!";

    return age;
}

function getAgeYear(dobstring, dateBefore) {
    
    var dldt = new Date(dateBefore.substring(6, 10),
                  dateBefore.substring(3, 5) - 1,
                  dateBefore.substring(0, 2)
                  );

    var dlyr = dldt.getFullYear(); //01/05/1994


    var dobdate = new Date(dobstring.substring(6, 10),
                     dobstring.substring(3, 5) - 1,
                     dobstring.substring(0, 2)
                     );
    var dobYear = dobdate.getFullYear();

    var yrs = (parseInt(dlyr) - parseInt(dobYear));

    //if (dldt.getMonth() < dobdate.getMonth() ||
    //    dldt.getMonth() == dobdate.getMonth() && dldt.getDate() < dobdate.getDate()) {
    //    yrs--;
    //}
    return yrs;
}
function getAgeYearMin(dobstring, dateInput) {
    var now = new Date();
    //var today = new Date(now.getYear(), now.getMonth(), now.getDate());
    var today = new Date(dateInput.substring(6, 10),
                       dateInput.substring(3, 5) - 1,
                       dateInput.substring(0, 2)
                       );
    var yearNow = today.getYear();
    var monthNow = today.getMonth();
    var dateNow = today.getDate();

    var dob = new Date(dobstring.substring(6, 10),
                       dobstring.substring(3, 5) - 1,
                       dobstring.substring(0, 2)
                       );

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    //var dldt = new Date(dateBefore.substring(6, 10),
    //              dateBefore.substring(3, 5) - 1,
    //              dateBefore.substring(0, 2)
    //              );

    //var dlyr = dldt.getFullYear(); //01/05/1994


    //var dobdate = new Date(dobstring.substring(6, 10),
    //                 dobstring.substring(3, 5) - 1,
    //                 dobstring.substring(0, 2)
    //                 );
    //var dobYear = dobdate.getFullYear();

    //var yrs = (parseInt(dlyr) - parseInt(dobYear));

    //if (dldt.getMonth() < dobdate.getMonth() ||
    //    dldt.getMonth() == dobdate.getMonth() && dldt.getDate() <= dobdate.getDate()) {
    //    yrs--;
    //}
    //return yrs;
    return age.years;
}
function checkBrowser() {
    if ($.browser.msie && $.browser.versionNumber <= 7) {
        alert("Your browser do not support this website page please upgarde your browser");
        window.location.href = "default.aspx";
        return false;
    } else {
        return true;
    }
}
$(document).ready(function () {
    if (!checkBrowser()) {
        return;
    }
    $(document).bind('contextmenu', function (e) {
        e.preventDefault();
        //alert('Right Click is not allowed');
    });
    $(".loading-overlay").show();
    $(".loading-overlay").hide();
    $(".confirm-overlay").show();
    $(".confirm-overlay").hide();
    
    $('#txtDOB').mask("99/99/9999", { placeholder: "_" });
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    //var mindt = new Date(1965, 1-1);
    //alert(mindt);
    $('#txtDOB').datepicker({
        yearRange: '1964:1998',
        showOtherMonths: true,
        selectOtherMonths: true,
        changeYear: true,
        changeMonth: true,
        showOn: 'both',
        buttonImage: 'images/calendar_icon.png',
        buttonImageOnly: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        minDate: new Date(1964, 1 - 1, 1),
        maxDate: new Date(1998, 12-1, 31),        
        duration: 'slow'
        //defaultDate:'1965-01-01',
        //maxDate: 0
        //,onClose: function () {
        //    /* Validate a specific element: */
        //    $("qualification-form").validate().element("#txtDOB");
        //}
        
    });

    $('#txtFromDate').datepicker({
        yearRange: '1980:2015',
        showOtherMonths: true,
        selectOtherMonths: true,
        changeYear: true,
        changeMonth: true,        
        showOn: 'both',
        buttonImage: 'images/calendar_icon.png',
        buttonImageOnly: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        maxDate: 0
    });

    $('#txtToDate').datepicker({
        yearRange: '1980:2015',
        showOtherMonths: true,
        selectOtherMonths: true,
        changeYear: true,
        changeMonth: true,
        showOn: 'both',
        buttonImage: 'images/calendar_icon.png',
        buttonImageOnly: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        maxDate: 0
    });

    $('#txtDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        showOn: 'both',
        buttonImage: 'images/calendar_icon.png',
        buttonImageOnly: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow'
    });
    window.applicationPage = new Employ.ApplicationPage();
    window.applicationPage.init();
    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });


    $('#button-personal-details').qtip({
        content: 'Enter your personal details and click here to move to the next tab payment details.',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#button-personal-details') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });

    $('#button-qualification-details').qtip({
        content: 'Enter your qualification and other details and click here to move to the next upload and submit tab.',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#button-qualification-details') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });
    $('#button-upload-details').qtip({
        content: 'Upload your photo, signature and fill your place and accept the declaration and click here to sumbit your application ',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#button-upload-details') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });

    $('#button_payment_details').qtip({
        content: 'Choose your payment mode and click here to go to the respective page.',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#button_payment_details') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });

    $('#step1').qtip({
        content: 'Personal Details',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#step1') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });
    $('#step2').qtip({
        content: 'Payment Details',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#step2') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });

    $('#step3').qtip({
        content: 'Communication and Qualification Details',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#step3') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });
    $('#step4').qtip({
        content: 'Upload your Photograph, Signature and Submit your application',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#step4') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });

    $('#ddlQualification').qtip({
        content: $('#ddlQualification option:selected').text(),
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#ddlQualification') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });


    $('#txtQualificationName2').parent().qtip({
        content: $('#txtQualificationName2').val(),
        
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#txtQualificationName2') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });

    $('#txtQualificationName3').parent().qtip({
        content: $('#txtQualificationName3').val(),
        
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#txtQualificationName3') // my target
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });


    $('#txtToYear1').qtip({
        content: 'Give Current Year if still working there.',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#txtToYear1') // my target
        },show: {
            event: 'focus'
        },
        hide: {
            event: 'blur'
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });
    $('#txtToYear2').qtip({
        content: 'Give Current Year if still working there.',
        position: {
            my: 'bottom center',  // Position my top left...
            at: 'top center', // at the bottom right of...
            target: $('#txtToYear2') // my target
        },show: {
            event: 'focus'
        },
        hide: {
            event: 'blur'
        },
        style: {
            classes: 'qtip-green qtip-rounded'
        }
    });
});

function Trim(input) {
    var lre = /^\s*/;
    var rre = /\s*$/;
    input = input.replace(lre, "");
    input = input.replace(rre, "");
    return input;
}
$.validator.addMethod("hscheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-hscheck").on("blur.validate-hscheck", function () {
            $(element).valid();
        });
    }
    //alert(parseInt($min.val()) + 2);
    if (value!=""){
        return parseInt(value) >= parseInt($min.val()) + 2;
    }
    return true;
}, "HS must be greater than or equal to minimum two years");

$.validator.addMethod("ugcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-ugcheck").on("blur.validate-ugcheck", function () {
            $(element).valid();
        });
    }
    //alert(parseInt($min.val()) + 2);
    if ($min.val() != "" && value != "") {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt(value) >= parseInt(dobYear) + (15 + 2 + 2));
        return parseInt(value) >= parseInt(dobYear) + (15+2+2);
    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "UG must be greater than or equal to minimum 19 years from Date of Birth");
$.validator.addMethod("yearcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-yearcheck").on("blur.validate-yearcheck", function () {
            $(element).valid();
        });
    }
    var validator = this;
    //alert(parseInt($min.val()) + 2);
    if ($min.val() != "" && value != "") {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();

        var curdate = new Date($("#hdnCurdate").val());
        var newDate = new Date();
        newDate.setTime(curdate.getTime());
        var curyear = newDate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt(value) >= parseInt(dobYear) + (15 + 2 + 2));
        var B = window.applicationPage.applicationForm.applicationMaster;
        if (B.MaxQualification == "M") {           
            //alert((parseInt(value) < parseInt(dobYear) + (14)));
            //alert((parseInt(value) >= curyear));
            if ((parseInt(value) < parseInt(dobYear) + (14)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 15 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15)));
                return false;
            }
        } else if (B.MaxQualification == "E") {
            if ((parseInt(value) < parseInt(dobYear) + (14 + 2 + 4)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        } else if (B.MaxQualification == "D") {
            
            if ((parseInt(value) < parseInt(dobYear) + (14 + 3)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 18 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 3)));
                return false;
            }
        }
        else if (B.MaxQualification == "P") {
            if ((parseInt(value) < parseInt(dobYear) + (14 + 2 + 3))  || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        }
        
    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "Check your Qualification Years");
$.validator.addMethod("yearqualcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-yearcheck").on("blur.validate-yearcheck", function () {
            $(element).valid();
        });
    }
    var validator = this;
    //alert(parseInt($min.val()) + 2);
    var B = window.applicationPage.applicationForm.applicationMaster;
    if ($min.val() != "" && value != "" && B.PostID==3) {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();


        var curdate = new Date($("#hdnCurdate").val());
        var newDate = new Date();
        newDate.setTime(curdate.getTime());
        var curyear = newDate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt(value) >= parseInt(dobYear) + (15 + 2 + 2));
        var B = window.applicationPage.applicationForm.applicationMaster;
        if (B.MaxQualification == "M") {

            if ((parseInt(value) < parseInt(dobYear) + (14)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 15 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15)));
                return false;
            }
        } else if (B.MaxQualification == "E") {
            if ((parseInt(value) < parseInt(dobYear) + (14 + 2 + 4)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        } else if (B.MaxQualification == "D") {

            if ((parseInt(value) < parseInt(dobYear) + (14 + 3)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 18 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 3)));
                return false;
            }
        }
        //else if (B.MaxQualification == "G") {
        //    if (parseInt(value) < parseInt(dobYear) + (14 + 2 + 2)) {
        //        //var errors = {};
        //        //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
        //        //validator.showErrors(errors);
        //        //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
        //        return false;
        //    }
        //}

    }else if ($min.val() != "" && value != "" && B.PostID==4) {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();


        var curdate = new Date($("#hdnCurdate").val());
        var newDate = new Date();
        newDate.setTime(curdate.getTime());
        var curyear = newDate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt(value) >= parseInt(dobYear) + (15 + 2 + 2));
        var B = window.applicationPage.applicationForm.applicationMaster;
        if (B.MaxQualification == "M") {

            if ((parseInt(value) < parseInt(dobYear) + (14)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 15 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15)));
                return false;
            }
        } else if (B.MaxQualification == "E") {
            if ((parseInt(value) < parseInt(dobYear) + (14 + 2 + 4)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        } else if (B.MaxQualification == "D") {

            if ((parseInt(value) < parseInt(dobYear) + (14 + 3)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 18 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 3)));
                return false;
            }
        }
        else if (B.MaxQualification == "P") {
            if (parseInt(value) < parseInt(dobYear) + (14)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        }

    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "Check your Qualification Years");
$.validator.addMethod("yearqualHScheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-yearcheck").on("blur.validate-yearcheck", function () {
            $(element).valid();
        });
    }
    var validator = this;
    //alert(parseInt($min.val()) + 2);
    var B = window.applicationPage.applicationForm.applicationMaster;
    if ($min.val() != "" && value != "" && B.PostID==4) {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();


        var curdate = new Date($("#hdnCurdate").val());
        var newDate = new Date();
        newDate.setTime(curdate.getTime());
        var curyear = newDate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt(value) >= parseInt(dobYear) + (15 + 2 + 2));
        var B = window.applicationPage.applicationForm.applicationMaster;
        if (B.MaxQualification == "M") {

            if ((parseInt(value) < parseInt(dobYear) + (14)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 15 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15)));
                return false;
            }
        } else if (B.MaxQualification == "E") {
            if ((parseInt(value) < parseInt(dobYear) + (14 + 2 + 4)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        } else if (B.MaxQualification == "D") {

            if ((parseInt(value) < parseInt(dobYear) + (14 + 3)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 18 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 3)));
                return false;
            }
        }
        else if (B.MaxQualification == "P") {
            if (parseInt(value) < parseInt(dobYear) + (14+2)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        }

    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "Check your Qualification Years");
$.validator.addMethod("yearqualBedcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-yearcheck").on("blur.validate-yearcheck", function () {
            $(element).valid();
        });
    }
    var validator = this;
    //alert(parseInt($min.val()) + 2);
    var B = window.applicationPage.applicationForm.applicationMaster;
    if ($min.val() != "" && value != "" && B.PostID == 4) {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();


        var curdate = new Date($("#hdnCurdate").val());
        var newDate = new Date();
        newDate.setTime(curdate.getTime());
        var curyear = newDate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt(value) >= parseInt(dobYear) + (15 + 2 + 2));
        var B = window.applicationPage.applicationForm.applicationMaster;
        if (B.MaxQualification == "M") {

            if ((parseInt(value) < parseInt(dobYear) + (14)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 15 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15)));
                return false;
            }
        } else if (B.MaxQualification == "E") {
            if ((parseInt(value) < parseInt(dobYear) + (14 + 2 + 4)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        } else if (B.MaxQualification == "D") {

            if ((parseInt(value) < parseInt(dobYear) + (14 + 3)) || (parseInt(value) >= curyear)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 18 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 3)));
                return false;
            }
        }
        else if (B.MaxQualification == "P") {
            if (parseInt(value) < parseInt(dobYear) + (14 + 2+3)) {
                //var errors = {};
                //errors[element.name] = "Year must be greater than or equal to minimum 19 years from Date of Birth";
                //validator.showErrors(errors);
                //alert(parseInt(value)  + "  " + (parseInt(dobYear)) + "  " + (parseInt(dobYear) + (15 + 2 + 2)));
                return false;
            }
        }

    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "Check your Qualification Years"); 
$.validator.addMethod("llbcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-llbcheck").on("blur.validate-llbcheck", function () {
            $(element).valid();
        });
    }
    //alert(parseInt($min.val()) + 2);
    if ($min.val() != "" && value != "") {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt($min.val()) >= parseInt(dobYear) + 16);
        return parseInt(value) >= parseInt(dobYear) + (15 + 2 + 5);
    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "LLB must be greater than or equal to minimum 21 years from Date of Birth");
$.validator.addMethod("btechcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-btechcheck").on("blur.validate-btechcheck", function () {
            $(element).valid();
        });
    }
    //alert(parseInt($min.val()) + 2);
    if ($min.val() != "" && value!="") {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt($min.val()) >= parseInt(dobYear) + 16);
        return parseInt(value) >= parseInt(dobYear) + (15 + 2 + 4);
    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "LLB must be greater than or equal to minimum 22 years from Date of Birth");
$.validator.addMethod("pgcheck", function (value, element, param) {
    var $min = $(param);
    
        if (this.settings.onfocusout) {
            $min.off(".validate-pgcheck").on("blur.validate-pgcheck", function () {
                $(element).valid();
            });
        }
        //alert(value);
        if (value != "") {
            //alert(parseInt($min.val()) + 2);
            return parseInt(value) >= parseInt($min.val()) + 2;
        }
        return true;
}, "PG must be greater than or equal to minimum two years");
$.validator.addMethod("empfromyearcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-ugcheck").on("blur.validate-ugcheck", function () {
            $(element).valid();
        });
    }
    //alert(parseInt($min.val()) + 2);
    if ($min.val() != "" && value != "") {
        //alert(parseInt($min.val()) + 2);
        var vl = $min.val();
        var dobdate = new Date(vl.substring(6, 10),
                       vl.substring(3, 5) - 1,
                       vl.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();

        var curdate = new Date($("#hdnCurdate").val());
        var newDate=new Date();
        newDate.setTime(curdate.getTime());
        var curyear = newDate.getFullYear();
        //alert(newDate);
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt(value) >= parseInt(dobYear) + (15 + 2 + 2));
        //alert((parseInt(value) >= parseInt(dobYear) + (16)));
        //alert(parseInt(value) <= curyear);
        return ((parseInt(value) >= parseInt(dobYear) + (16)) && (parseInt(value)<=curyear));
    }
    //if (value != "") {
    //    return parseInt(value) >= parseInt($min.val()) + 2;
    //}
    return true;
}, "From year must be greater than or equal to minimum 16 years from Date of Birth and less than or equal to current year");
$.validator.addMethod("emptoyearcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-pgcheck").on("blur.validate-pgcheck", function () {
            $(element).valid();
        });
    }
    //alert(value);
    if (value != "") {
        //alert(parseInt($min.val()) + 2);
        var curdate = new Date($("#hdnCurdate").val());
        var newDate = new Date();
        newDate.setTime(curdate.getTime());
        var curyear = newDate.getFullYear();
        return ((parseInt(value) >= parseInt($min.val())) && (parseInt(value) <= parseInt(curyear)));
    }
    return true;
}, "To year sholud be greater than or equal to from year and less than or equal to current year.");
$.validator.addMethod("percentcheck", function (value, element, param) {
    //var $min = $(param);

    //if (this.settings.onfocusout) {
    //    $min.off(".validate-percentcheck").on("blur.validate-percentcheck", function () {
    //        $(element).valid();
    //    });
    //}
    //alert(value);
    var B = window.applicationPage.applicationForm.applicationMaster;
    if (value != "") {
        //alert(parseFloat(param));
        //alert(parseFloat(value));
        //alert(parseFloat(value) <= parseFloat(param));
        if (B.PostID == 4) {
           // alert($(element).attr("id"));
            if ($(element).attr("id") == "txtPercent2") {
                return (parseFloat(value) >= 55 && parseFloat(value) <= parseFloat(param));
            } else if ($(element).attr("id") == "txtPercent3") {
                return (parseFloat(value) >= 50 && parseFloat(value) <= parseFloat(param));
            } else {
                return (parseFloat(value) > 0 && parseFloat(value) <= parseFloat(param));
            }
            
        } else {
            return (parseFloat(value) > 0 && parseFloat(value) <= parseFloat(param));
        }
        
    }
    return true;
}, "Percentage must be greater than 0 and less than and equal to 100");
$.validator.addMethod("dobcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-dobcheck").on("blur.validate-dobcheck", function () {
            $(element).valid();
        });
    }
    //alert(value);
    if (value != "") {
        //alert(parseInt($min.val()) + 2);
        var dobdate = new Date(value.substring(6, 10),
                       value.substring(3, 5) - 1,
                       value.substring(0, 2)
                       );
        var dobYear = dobdate.getFullYear();
        //alert(dobYear);
        //alert(parseInt(dobYear) + 16);
        //alert(parseInt($min.val()) >= parseInt(dobYear) + 16);
        return parseInt($min.val())>=parseInt(dobYear)+15  ;
    }
    
}, "DOB year should be minimum 16 years less than Madhyamik year");
$.validator.addMethod("alpha", function (value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
}, "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic only , i.e within A to Z");
$.validator.addMethod("validEmail", function (value, element) {
    return this.optional(element) || value == value.match(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);//  /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
    // --                                    or leave a space here ^^
}, "Not a Valid Email Address");
$.validator.addMethod("alphaNumeric", function (value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z0-9\s]+$/);
    // --                                    or leave a space here ^^
}, "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9");
$.validator.addMethod("noquotes", function (value, element) {
    return this.optional(element) || value == value.match(/^[^'"]*$/);//value == value.match(/'"/);
    // --                                    or leave a space here ^^
}, "No single or double quotes are allowed.");
$.validator.addMethod("checkUBAB", function (value, element) {
    //return this.optional(element) || value == (value!="" && value=="UB");
    if (value != "") {
        if (value == "UB" && !$("#chkUB").is(":checked")) {
            return false;
        } else if (value == "AB" && !$("#chkAB").is(":checked")) {
            return false;
        }
    }
    return true;
    // --                                    or leave a space here ^^
}, "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic only , i.e within A to Z");
$.validator.addMethod("dojcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-dojcheck").on("blur.validate-dojcheck", function () {
            $(element).valid();
        });
    }
    //alert(value);
    if (value != "") {
        //alert(parseInt($min.val()) + 2);
        var dobvalue = $min.val();
        var dojdate = new Date(value.substring(6, 10),
                       value.substring(3, 5) - 1,
                       value.substring(0, 2)
                       );
        var dojYear = dojdate.getFullYear();

        var dobdate = new Date(dobvalue.substring(6, 10),
                      dobvalue.substring(3, 5) - 1,
                      dobvalue.substring(0, 2)
                      );
        var dobYear = dobdate.getFullYear();
        //alert(dobYear);

        

        //alert(parseInt(dlyr) - parseInt(dobYear));
       // alert(parseInt(dojYear) - parseInt(dobYear));
        return (parseInt(dojYear)- parseInt(dobYear)>=  15);
    }

}, "Date Of Joining year should be minimum 15 years greater than Date of Joining");
$.validator.addMethod("trainyearcheck", function (value, element, param) {
    var $min = $(param);

    if (this.settings.onfocusout) {
        $min.off(".validate-trainyearcheck").on("blur.validate-trainyearcheck", function () {
            $(element).valid();
        });
    }
    //alert(value);
    if (value != "") {
        //alert(parseInt($min.val()) + 2);
       
        var frmdate = new Date(value.substring(6, 10),
                       value.substring(3, 5) - 1,
                       value.substring(0, 2)
                       );
        var frmYear = frmdate.getFullYear();

        var dobvalue = $min.val();
        var dobdate = new Date(dobvalue.substring(6, 10),
                      dobvalue.substring(3, 5) - 1,
                      dobvalue.substring(0, 2)
                      );
        var dobYear = dobdate.getFullYear();
        //alert(dobYear);



        //alert(parseInt(dlyr) - parseInt(dobYear));
        // alert(parseInt(dojYear) - parseInt(dobYear));
        return (parseInt(frmYear) - parseInt(dobYear) >= 16);
    }
    return true;
}, "Year should be minimum 16 years greater than date of birth ");
//$.validator.addMethod("checksexhusband", function (value, element) {
//    //var $min = $(param);

//    //if (this.settings.onfocusout) {
//    //    $min.off(".validate-yearcheck").on("blur.validate-yearcheck", function () {
//    //        $(element).valid();
//    //    });
//    //}

//    if (value != "") {

//        if ($("#ddlSex").val() == 'M' && $('#rdH').is(":checked")) {

//            return false;

//        }
//        //}


//    }
//    //if (value != "") {
//    //    return parseInt(value) >= parseInt($min.val()) + 2;
//    //}
//    return true;
//}, "When husband selected gender cannot be Male");
$.validator.addMethod("trainingcheck", function (value, element) {
    //var $min = $(param);

    //if (this.settings.onfocusout) {
    //    $min.off(".validate-dojcheck").on("blur.validate-dojcheck", function () {
    //        $(element).valid();
    //    });
    //}
    //alert(value);
    var B = window.applicationPage.applicationForm.applicationMaster;
    if ($("#txtFromDate").val() != ""  && $("#txtToDate").val() != ""  && B.PostID == 2) {
        //alert(parseInt($min.val()) + 2);id()
        var dtfrom = $("#txtFromDate").val();
        var dtfromdate = new Date(dtfrom.substring(6, 10),
                      dtfrom.substring(3, 5) - 1,
                      dtfrom.substring(0, 2)
                      );
        var dtfromYear = dtfromdate.getFullYear();
        var dtto = $("#txtToDate").val();
        var dttodate = new Date(dtto.substring(6, 10),
                       dtto.substring(3, 5) - 1,
                       dtto.substring(0, 2)
                       );
        var dttoYear = dttodate.getFullYear();
        
        //alert(dobYear);



        //alert(parseInt(dlyr) - parseInt(dobYear));
        // alert(parseInt(dojYear) - parseInt(dobYear));
        //alert(parseInt(dttoYear) - parseInt(dtfromYear));
        //alert(Math.floor((dttodate.getTime() - dtfromdate.getTime()) / (1000 * 60 * 60 * 24)))
        var diff = (Math.floor((dttodate.getTime() - dtfromdate.getTime()) / (1000 * 60 * 60 * 24)))
        //return (parseInt(dttoYear) - parseInt(dtfromYear) >= 1);
        return ((diff) >= 364);
    }
    return true;

}, "Training must be of minimum one year");
function getAlert() {
    var message = $('<p />', { text: 'Please enter the required fields marked as red(*) before going to the next tab.' }),
       ok = $('<button />', { text: 'Ok', 'class': 'full' });

    dialogue(message.add(ok), 'Alert!');
  
}

function dialogue(content, title) {
    $('<div />').qtip({
        content: {
            text: content,
            title: title
        },
        position: {
            my: 'center', at: 'center',
            target: $(window)
        },
        show: {
            ready: true,
            modal: {
                on: true,
                blur: false
            }
        },
        hide: false,
        style: {
            classes: 'qtip-green qtip-rounded'
        },
        events: {
            render: function (event, api) {
                $('button', api.elements.content).click(function (e) {
                    api.hide(e);
                });
            },
            hide: function (event, api) { api.destroy(); }
        }
    });
}

function RedirectAfterSessionExpired(RedirectPage) {
    alert("Your Session Expired Please Login Again.");
    window.location.href = RedirectPage;
}
// filter the files before Uploading for text file only  
function CheckForImageFile(fupload) {
    //alert(fupload);
    var file = $(fupload);
    //alert(file.attr("id"));
    var fileName = file.val();
    //alert(fileName);
    //Checking for file browsed or not 
    if ($.trim(fileName) == '') {
        alert("Please select a file to upload!!!");
        file.focus();
        return false;
    }

    //Setting the extension array for diff. type of text files 
    //var extArray = new Array(".jpeg", ".jpg", ".png", ".bmp", ".tiff",".gif");
    var extArray = new Array(".jpg", ".png");

    //getting the file name
    while (fileName.indexOf("\\") != -1)
        fileName = fileName.slice(fileName.indexOf("\\") + 1);

    //Getting the file extension                     
    var ext = fileName.slice(fileName.indexOf(".")).toLowerCase();

    //matching extension with our given extensions.
    for (var i = 0; i < extArray.length; i++) {
        if (extArray[i] == ext) {
            return true;
        }
    }
    alert("Please only upload files that end in types:  "
      + (extArray.join("  ")) + "\nPlease select a new "
      + "file to upload again.");
    file.focus();
    return false;
}
function GetFileSize(fileid, type) {
    try {
        var fileSize = 0;
        //for IE
        if ($.browser.msie) {
            //before making an object of ActiveXObject, 
            //please make sure ActiveX is enabled in your IE browser
            var objFSO = new ActiveXObject("Scripting.FileSystemObject");
            var filePath = $("#" + fileid)[0].value;
            var objFile = objFSO.getFile(filePath);
            fileSize = objFile.size; //size in kb
            //fileSize = fileSize / 1048576; //size in mb 
        }
            //for FF, Safari, Opeara and Others
        else {
            fileSize = $("#" + fileid)[0].files[0].size //size in kb
            //fileSize = fileSize / 1048576; //size in mb 
        }
        //alert("Uploaded File Size is" + fileSize + " kb");
        if (type == 'P' && parseInt(fileSize) > 50000) {
            alert('File Size is not within the specified range.');
            return false;
        } else if (type == 'S' && parseInt(fileSize) > 20000) {
            alert('File Size is not within the specified range.');
            return false;
        }
        //alert('Size OK');
        return true;
    }
    catch (e) {
        alert("Error is :" + e);
    }
}
function CheckForDocFile(fupload) {
    //alert(fupload);
    var file = $(fupload);
    //alert(file.attr("id"));
    var fileName = file.val();
    //alert(fileName);
    //Checking for file browsed or not 
    if ($.trim(fileName) == '') {
        alert("Please select a file to upload!!!");
        file.focus();
        return false;
    }

    //Setting the extension array for diff. type of text files 
    var extArray = new Array(".pdf", ".doc", ".docx");

    //getting the file name
    while (fileName.indexOf("\\") != -1)
        fileName = fileName.slice(fileName.indexOf("\\") + 1);

    //Getting the file extension                     
    var ext = fileName.slice(fileName.indexOf(".")).toLowerCase();

    //matching extension with our given extensions.
    for (var i = 0; i < extArray.length; i++) {
        if (extArray[i] == ext) {
            return true;
        }
    }
    alert("Please only upload files that end in types:  "
      + (extArray.join("  ")) + "\nPlease select a new "
      + "file to upload and submit again.");
    file.focus();
    return false;
}
function checkDate(ch) {
    if (ch.id == "txtFromDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtToDate").datepicker("option", "minDate", '-100Y');
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtToDate").datepicker("option", "minDate", testm);
        }
    } else if (ch.id == "txtToDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtFromDate").datepicker("option", "maxDate", 0);
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtFromDate").datepicker("option", "maxDate", 0);
        }
    }
};


Employ.PersonalInfo = function () {
    var A = this;
    this.validator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#personal-info").show();
        $("#button-personal-details").click(A.clickPersonalDetails);
        //$("#ddlPermDistrict").change(window.applicationPage.PopulatePermPS);
        //$("#ddlMailDistrict").change(window.applicationPage.PopulateMailPS);
        $("#ddlPost").change(A.PostChanged);
        $("#ddlPWD").change(A.CasteChanged);
      //  $("#ddlKMC").change(A.CasteChanged);
        $("#ddlCaste").change(A.CasteChanged);
        $("#ddlStreamSpecialisation").change(A.CasteChanged);
        $("#ddlMinority").change(A.casteChanged);
        
        $("#txtDOB").change(A.DOBChanged);
        $("#rdH").click(A.rdHClicked);
        $("#rdF").click(A.rdHClicked);
        $("#chkCopy").click(A.CheckCopyClicked);
        
    };
    this.rdHClicked = function () {
        $("#ddlSex").valid();
    };

    //******************************************************//
    this.CheckCopyClicked = function () {
        var $this = $(this);
        if ($this.is(":checked")) {
            $("#txtPermAddress").val($("#txtMailAddress").val());
            $("#txtPermPO").val($("#txtMailPO").val());
            $("#ddlPermState").val($("#ddlMailState").val());
            //window.applicationPage.PopulateMailPS();
            //var PermPS = $("#ddlPermPS").val();
            //alert(PermPS);
            //sleepFor(100);
            $("#txtPermPS").val($("#txtMailPS").val());
            $("#txtPermPincode").val($("#txtMailPincode").val());
        } else {
            $("#txtPermAddress").val("");
            $("#txtPermPO").val("");
            $("#ddlPermState").val("");
            //window.applicationPage.PopulateMailPS();
            $("#txtPermPS").val("");
            $("#txtPermPincode").val("");
        }
    };
    //*****************************************************//
    this.getValidator = function () {
        return $("#personal-form").validate({
            rules: {
                //ddlHomeDistrict: { required: true },
                ddlPost: { required: true },
                txtAdvNo: { required: true, maxlength: 10 },
                txtName: { required: true, alpha: true,maxlength:45 },
                txtFHName: { required: true, alpha: true, maxlength: 45 },
                txtFHMobileNo: { required: true,number: true, minlength:10, maxlength: 10 },
                chkIndian:{required:true},
                ddlSex: { required: true},
                ddlCaste: { required: true },
                ddlPWD: { required: true },
                ddlStreamSpecialisation: { required: true },
                ddlMinority: { required: true },
                //ddlKMC: {
                //    required: function (element) {
                //        var B = window.applicationPage.applicationForm.applicationMaster;
                //        if (B.PostID == 1) {
                //            return true;
                //        } else {
                //            return false;
                //        }
                //    }
                //},

                txtMailAddress: { required: true, maxlength: 90, noquotes: true },
                txtMailPO: { required: true, maxlength: 50, noquotes: true },
                ddlMailState: { required: true },
                txtMailPS: { required: true, maxlength: 20, noquotes: true },
                txtMailPincode: { required: true, number: true, minlength: 6, maxlength: 6 },
                txtPermAddress: { required: true, maxlength: 90, noquotes: true },
                txtPermPO: { required: true, maxlength: 50, noquotes: true },
                ddlPermState: { required: true },
                txtPermPS: { required: true, maxlength: 20, noquotes: true },
                txtPermPincode: { required: true, number: true, minlength: 6, maxlength: 6 },


                txtDOB: { required: true, dpDate: true },//, dobcheck: '#txtSecYear'                
                txtYY: { required: true },
                txtMM: { required: true },
                txtDD: { required: true },                
                txtCandidateEmail: { email: true,maxlength:35,validEmail:true },
                txtCandidateMobile: { required: true, number: true, minlength: 10, maxlength: 10 }
                
                //,
                //chkIndian:{required:true}
            },
            messages: {
                //ddlHomeDistrict: "Please select home district",
                ddlPost: "Please select post applied for.",
                txtAdvNo: { required: "Please enter your Advertisement No.", maxlength: "Maximum length 35" },
                txtName: { required: "Please enter your name", alpha: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic only , i.e within A to Z" },
                txtFHName: { required: "Please enter your father's/Guardian's name", alpha: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic only , i.e within A to Z" },
                txtFHMobileNo: {
                    required: "Please enter mobile number",
                    number: "Please enter valid number",
                    minlength: "Minimum 10 number",
                    maxlength: "Maximum 10 number"
                },
                ddlSex: { required: "Please select your Gender" },
                ddlCaste: "Please select caste",
                ddlPWD: { required: "Please Select PWD Yes/No" },
                ddlStreamSpecialisation: "Please Select Department",
                ddlMinority: { required: "please select Minority Yes/No" },
                //*******************************************************************//
                txtMailAddress: { required: "Please enter your communication address", maxlength: "Maximum 90 character allowed", noquotes: "No single or double quotes are allowed." },
                txtMailPO: { required: "Please enter communication P.O.", maxlength: "Maximum 50 character allowed", noquotes: "No single or double quotes are allowed." },
                ddlMailState: "Please select communication state",
                txtMailPS: { required: "Please select communication P.S.", maxlength: "Maximum 20 character allowed", noquotes: "No single or double quotes are allowed." },
                txtMailPincode: {
                    required: "Please select communication pincode",
                    number: "Please enter valid number",
                    minlength: "Minimum 6 number",
                    maxlength: "Maximum 6 number"
                     },
                txtPermAddress: { required: "Please enter your permanent address", maxlength: "Maximum 90 character allowed", noquotes: "No single or double quotes are allowed." },
                txtPermPO: { required: "Please enter permanent P.O.", maxlength: "Maximum 50 character allowed", noquotes: "No single or double quotes are allowed." },
                ddlPermState: "Please select permanent state",
                txtPermPS: { required: "Please enter permanent P.S.", maxlength: "Maximum 20 character allowed", noquotes: "No single or double quotes are allowed." },
                txtPermPincode: {
                    required: "Please select permanent pincode",
                    number: "Please enter valid number",
                    minlength: "Minimum 6 number",
                    maxlength: "Maximum 6 number"
                },
                //******************************************************************//
               // ddlKMC: { required: "Please Select KMC Employee Yes/No" },
                txtDOB: { required: "Please enter Date of Birth " },
                txtYY: "Please enter Age Year",
                txtMM: "Please enter Age Month",
                txtDD: "Please enter Age Day",

                txtCandidateEmail: {
                    email: "Please enter valid email address",
                    maxlength: "Maximum Length 35",
                    validEmail:"Not a Valid Email Address"
                },
                txtCandidateMobile: {
                    required: "Please enter mobile number",
                    number: "Please enter valid number",
                    minlength: "Minimum 10 number",
                    maxlength: "Maximum 10 number"
                },
                chkIndian: "Only citizen of India is allowed to apply for this post"
            }
        })

    };
    this.PostChanged = function () {
        if ($("#ddlPost").val() != "") {
            A.GetPostChanged();
            window.applicationPage.PopulateQualification();
            //window.applicationPage.PopulateDesiredQualification();
            //var B = window.applicationPage.applicationForm;
            //if (B.applicationMaster.PostID == 1) {
            //    window.applicationPage.PopulateDesiredQualification2();
            //}
            window.applicationPage.ShowHideQualTrainDesired();
        }
        
    };
    this.GetPostChanged = function () {
        $(".loading-overlay").show();

        var B = window.applicationPage.applicationForm;
        B.applicationMaster.PostID = parseInt($("#ddlPost").val());
        var E = "{\'ApplicationForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/GetPostValues',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.applicationPage.applicationForm = JSON.parse(response.d);
                //alert(JSON.stringify(window.applicationPage.applicationForm));
                var B = window.applicationPage.applicationForm;

                //if (B.applicationMaster.PostAbbv == 'S') {
                //    $('#ddlSex').val("M");
                //} else if (B.applicationMaster.PostAbbv == 'L') {
                //    $('#ddlSex').val("F");
                //} else {
                //    $('#ddlSex').val("");
                //}
                $(".loading-overlay").hide();
            },
            error: function (response) {
                alert('Some Error Occur');

                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    }
    
    this.CasteChanged = function () {
        if ($("#txtDOB").val() != "" && $('#txtDOB').valid()) {
            //alert('in caste change');
            //A.CheckDOB(getAgeYear($("#txtDOB").val(), $("#hdnInputDate").val()));
            $("#divDobValid").html('');
            //A.CheckDOB(getAgeYear($("#txtDOB").val(), $("#hdnInputDate").val()), getAgeYearMin($("#txtDOB").val(), $("#hdnInputDate").val()));
            A.CheckDOB();
        }
        if ($("#ddlCaste").val() != "") {
            
            window.applicationPage.SetApplicationAmount();
            
        }
    };
    this.DOBChanged = function () {
        if ($("#txtDOB").val() != "" && $('#txtDOB').valid()) {
            //var age = getAge($("#txtDOB").val(), $("#hdnInputDate").val());
            //alert(age);
            //$("#txtYY").val(age.years);
            //$("#txtMM").val(age.months);
            //$("#txtDD").val(age.days);
            $("#divDobValid").html('');
            //A.CheckDOB(getAgeYear($("#txtDOB").val(), $("#hdnInputDate").val()));
            //A.CheckDOB(getAgeYear($("#txtDOB").val(), $("#hdnInputDate").val()), getAgeYearMin($("#txtDOB").val(), $("#hdnInputDate").val()));
            A.CheckDOB();

        } else {
            $("#divDobValid").html('');
            $("#txtYY").val("");
            $("#txtMM").val("");
            $("#txtDD").val("");
        }

    };
    this.CheckDOB = function () {
        
        //alert(years + '   min year  '+minYears);
        $("#divDobValid").html('');
        $("#txtYY").val('');
        $("#txtMM").val('');
        $("#txtDD").val('');
        if ($("#ddlCaste").val() != "" && $('#txtDOB').valid()) {
            A.CheckDOBValid();
        }
    };
    //this.CheckDOB = function (years, minYears) {

    //    //alert(years + '   min year  '+minYears);
    //    $("#divDobValid").html('');
    //    $("#txtYY").val('');
    //    $("#txtMM").val('');
    //    $("#txtDD").val('');
    //    if ($("#ddlCaste").val() != "" && $('#txtDOB').valid()) {
    //        A.CheckDOBValid();
    //    }
    //};
    this.CheckDOBValid = function () {
        $(".loading-overlay").show();
        var B = window.applicationPage.applicationForm;
        //var KMC = ($("#ddlKMC").val() == "" ? "N" : $("#ddlKMC").val());
        var PWD = ($("#ddlPWD").val() == "" ? "N" : $("#ddlPWD").val());
        var CalcDate = $("#hdnInputDate").val();
        var E = "{DOB:'" + $.trim($("#txtDOB").val()) + "',ExamID:" + B.applicationMaster.ExamID + ",CasteID:'" + ($("#ddlCaste").val()) +
             "',PWD:'"+PWD+"',CalcDate:'" + CalcDate + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckDOBValid',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ret = JSON.parse(response.d);
                //alert(JSON.stringify(ret));

                $("#divDobValid").html('');
                if (parseInt(ret.Status) == 1) {
                    //alert(B.applicationMaster.ApplicationID);
                    $("#divDobValid").html(ret.ReturnValue);
                } else if (parseInt(ret.Status) == 0) {
                    $("#divDobValid").html('');
                }
                $("#txtYY").val(ret.YY);
                $("#txtMM").val(ret.MM);
                $("#txtDD").val(ret.DD);
                $("#txtYY").valid();
                $("#txtMM").valid();
                $("#txtDD").valid();
                $(".loading-overlay").hide();
                //return false;
            },
            error: function (response) {
                alert('Some Error Occur');

                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    };
    
    this.clickPersonalDetails = function () {
        $("#button-personal-details").attr("disabled", true);
        if ($.trim($("#divDobValid").html()) == ""  && A.validator.form()) {
            var B = window.applicationPage.applicationForm;
            if (B.applicationMaster.Step1 != 'Y' && B.applicationMaster.ApplicationID == 0) {
                var msg = "Information once entered in this personal details page  cannot be changed after saved. Do you want to continue?";
                if (confirm(msg)) {
                    A.setFormValues();
                } else {
                    $("#button-personal-details").attr("disabled", false);
                }
                
            } else {
                $("#button-personal-details").attr("disabled", false);
                A.Navigate();
                
            }
        } else {
            $("#button-personal-details").attr("disabled", false);
            getAlert();
            A.validator.focusInvalid();
        }
    };


    this.setFormValues = function () {

                // Permanent Address Copy.
        
        var B = window.applicationPage.applicationForm;
        
        B.applicationMaster.PostID = parseInt($("#ddlPost").val());
        B.applicationMaster.AdvertisementNo = $.trim($("#txtAdvNo").val()).toString().toUpperCase();        
        B.applicationMaster.Name = $.trim($("#txtName").val()).toString().toUpperCase();
        if ($("#rdF").is(":checked")) {
            B.applicationMaster.FatherName = $.trim($("#txtFHName").val()).toString().toUpperCase();
        //}  else if ($("#rdM").is(":checked")) {
        //    B.applicationMaster.MotherName = $("#txtFHName").val().toString().toUpperCase();
        } else if ($("#rdH").is(":checked")) {
            B.applicationMaster.GuardianName = $.trim($("#txtFHName").val()).toString();
        }
        B.applicationMaster.FHMobile = $.trim($("#txtFHMobileNo").val()).toString();
        B.applicationMaster.Nationality = ($("#chkIndian").is(":checked") ? "Y" : "N");
        B.applicationMaster.Sex = ($("#ddlSex").val());
        B.applicationMaster.CasteID = $("#ddlCaste").val();
        B.applicationMaster.PWD = $("#ddlPWD").val();
        B.applicationMaster.DepartmentName = $("#ddlStreamSpecialisation").val();
        B.applicationMaster.Minority = $("#ddlMinority").val();
       // B.applicationMaster.KMC = $("#ddlKMC").val();
        B.applicationMaster.DOB = $.trim($("#txtDOB").val());
        B.applicationMaster.AgeAsOn = ($.trim($("#txtYY").val()) + "-" + $.trim($("#txtMM").val()) + "-" + $.trim($("#txtDD").val()));
        B.applicationMaster.Email = $.trim($("#txtCandidateEmail").val()).toString();
        B.applicationMaster.Mobile = $.trim($("#txtCandidateMobile").val()).toString();

        //***************************************************//
        B.applicationMaster.MailAddress = $.trim($("#txtMailAddress").val()).toString().replace(/\r?\n|\r/g, " ").toUpperCase();
        B.applicationMaster.MailPO = $.trim($("#txtMailPO").val()).toString().toUpperCase();
        B.applicationMaster.MailStateID = parseInt($("#ddlMailState").val());
        B.applicationMaster.MailPS = ($.trim($("#txtMailPS").val()).toString().toUpperCase());
        B.applicationMaster.MailPinCode = parseInt($("#txtMailPincode").val());

        B.applicationMaster.PermAddress = $.trim($("#txtPermAddress").val()).toString().replace(/\r?\n|\r/g, " ").toUpperCase();
        B.applicationMaster.PermPO = $.trim($("#txtPermPO").val()).toString().toUpperCase();
        B.applicationMaster.PermStateID = parseInt($("#ddlPermState").val());
        B.applicationMaster.PermPS = ($.trim($("#txtPermPS").val()).toString().toUpperCase());
        B.applicationMaster.PermPinCode = parseInt($("#txtPermPincode").val());
        //***************************************************//



        

        
        
        
        //alert(JSON.stringify(B));
        A.SavePersonalDetails();
        
    };/********* need to edit***********/


    this.SavePersonalDetails = function () {
        
        //$(".loading-overlay").show();
        var B = window.applicationPage.applicationForm;
        var E = "{\'ApplicationForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SavePersonalInfo',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $(".loading-overlay").show();
            },
            success: function (response) {
                window.applicationPage.applicationForm = JSON.parse(response.d);
                //alert(JSON.stringify(window.applicationPage.applicationForm));
                $(".loading-overlay").hide();
                B = window.applicationPage.applicationForm;
                //alert(JSON.stringify(B));
                if (B.SessionExpired == "N") {
                    if (B.AppExpired == "N") {
                        if (B.SubmitFlag == "N") {
                            if (B.PreviousSaved == "N") {
                                if (B.applicationMaster.ApplicationID > 0 && B.applicationMaster.Step1 == "Y") {
                                    $("#button-personal-details").attr("disabled", false);
                                    window.applicationPage.SetStep1Completed();
                                    A.Navigate();
                                }
                            } else {
                                $("#button-personal-details").attr("disabled", false);
                                alert("Your personal information already saved for this post.");
                                //window.location.href = B.DefaultPage;
                                location.reload();
                            }

                        } else {
                            $("#button-personal-details").attr("disabled", false);
                            alert("You have already applied for this post. To check your application status go to MyAccount.");
                            window.location.href = B.DefaultPage;
                        }
                    } else {
                        $("#button-personal-details").attr("disabled", false);
                        alert("New Registration/Submission for Payment for this Post has already expired.");
                        window.location.href = 'RegistrationExpired.aspx?T=I&E=' + B.applicationMaster.ExamID;
                    }
                } else {
                    $("#button-personal-details").attr("disabled", false);
                    RedirectAfterSessionExpired("login.aspx");
                }
                
                
            },
            error: function (response) {
                alert('Some Error Occur');
                $("#button-personal-details").attr("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $("#button-personal-details").attr("disabled", false);
                $(".loading-overlay").hide();
            }
        });
    };
    this.Navigate = function () {
        
        $("#personal-info").hide();
        $(".personal-info-tab").removeClass("tab-active");
        $(".personal-info-tab").addClass("tab-visited");
        $(".personal-info-tab").click(A.tabClick);
        window.applicationPage.paymentinfo.transition();
    };
    this.transition = function () {
        $("#personal-info").show();
        $(".personal-info-tab").addClass("tab-active").removeClass("tab-disabled");

    };
    this.tabClick = function () {
        window.applicationPage.setTabActive(".personal-info-tab");
        $(".tabcontent").hide();
        $("#personal-info").show();

    };

};
Employ.PaymentInfo = function () {
    var A = this;
    this.validator;

    this.init = function () {
        this.validator = A.getValidator();
        $("#button_payment_details").click(A.clickPaymentDetails);
        $("#button-pay-next-details").click(A.clickPayNextDetails);
        //$("#btnAdd").click(A.AddClicked);
        $("#ddlPayMode").change(A.PaymentChange);
        //$("#btn").click(A.btnClicked);
    };
    this.PaymentChange = function () {
        //if ($("#ddlPayMode").val() != "") {
            window.applicationPage.SetApplicationAmount();
        //}
    };
    this.getValidator = function () {
        return $("#payment-form").validate({
            rules: {
                ddlPayMode: { required: true },
                txtApplAmt: {
                    required: function (element) {
                        return ($("#ddlCaste").val() != "C" || $("#ddlCaste").val() != "T");
                    }
                },
                
                txtProcessingCharge: { required: true },
                txtBankCharge: {
                    required: function (element) {
                        return ($("#ddlPayMode").val() == "O");
                    }
                },
                chkPayAccept:{required:true}


            },
            messages: {
                ddlPayMode:"Select Mode of Payment",
                txtApplAmt: { required: "Please enter application amount"},
                txtProcessingCharge: "Please enter Processing Charges",
                txtBankCharge: "Please enter Bank Charge",
                chkPayAccept:{required:"Please accept the terms and condition before you can proceed further"}
            }
        })
    };
    this.getHighestVal = function (data, index) {
        var max = 0;
        $.each(data, function (i, v) {
            thisVal = v[index];
            max = (max < thisVal) ? thisVal : max;
        });
        alert(max);
        return max;
    }
    //this.btnClicked = function () {
        
    //}
    this.clearTextBox = function () {
        $("#txtIPODDNo").val('');
        $("#txtIPOAmt").val('');
        //$("#txtIPOBB").val('');
        //$("#txtBankCode").val('');
        var B = window.applicationPage.applicationForm;
        var RemainAmount = B.RequiredTotalIPO - B.TotalIPO;
        $("#txtIPOAmt").val(RemainAmount);
    };
    this.clickPayNextDetails = function () {
        var B = window.applicationPage.applicationForm;
        if (B.applicationMaster.Step2 == "Y" && B.applicationMaster.NextStepAllowed == "Y" && B.applicationMaster.SubmitForPay == "Y" && B.applicationMaster.PaymentSuccessful == "Y") {
            A.Navigate();
        } else {
            alert("Payment has not yet been realised for your application. You are not allowed to move forward.");
        }
    };
    this.clickPaymentDetails = function () {
        $("#button_payment_details").attr("disabled", true);//A.check() &&
        
        if (A.validator.form()) {
            var md=$('#ddlPayMode').val();
            var ms = (md == "O" ? "You have selected " + $('#ddlPayMode option:selected').text() + " mode. You will  be redirected to the payment gateway page for online payment."
                +" You will again be redirected to this page once your payment is successful for filling your address and other details."
                + " Are you sure you want to proceed?" :
                "You have selected " + $('#ddlPayMode option:selected').text() + " mode. You will  be redirected to the Challan page. "
                + "Please take a printout of the challan and deposit the total amount to UNITED BANK OF INDIA after two(2) working days from the date of challan generation."
                + " You have to come again to this page once your payment is been successfully reconciled with bank after one(1) working days from the date of payment,"
                + " for filling your qualifcation, address and other details."
                + " Are you sure you want to proceed?");
            if (confirm(ms)) {
                A.setFormValues();
            } else {
                $("#button_payment_details").attr("disabled", false);
            }
        } else {
            $("#button_payment_details").attr("disabled", false);
            getAlert();
            A.validator.focusInvalid();
        }
        //else {
        //    $("#button-payment-details").attr("disabled", false);
        //}
    };
    this.check = function () {
        var B = window.applicationPage.applicationForm;
        var C = Employ.Utils.fixArray(B.applicationPayment);
        //alert(B.applicationMaster.CasteID);
        if (B.applicationMaster.CasteID != "C" && B.applicationMaster.CasteID != "T") {
            if (C == "") {
                alert('Enter Payment Details ');
                return false;
            } else if (B.TotalIPO < B.RequiredTotalIPO) {
                alert('IPO/DD Amount cannot be less than the required IPO/DD Amount=Rs. ' + B.RequiredTotalIPO);
                return false;
            }
        }
        return true;
    };
    this.setFormValues = function () {
        var B = window.applicationPage.applicationForm;
        B.applicationMaster.PayMode = ($("#ddlPayMode").val().toString().toUpperCase());
        B.applicationMaster.ApplicationAmount = ($.trim($("#txtApplAmt").val()));
        B.applicationMaster.ProcessingCharge = ($.trim($("#txtProcessingCharge").val()));
        B.applicationMaster.BankCharge = ($.trim($("#txtBankCharge").val()));        
        B.applicationMaster.TotalAmount = ($.trim($("#txtTotal").val()));

        //B.applicationMaster.PreviousPostID = ($("#ddlPreviousPost").val());

        
        //alert(JSON.stringify(B));
        A.SavePaymentInfo();

        //var B = window.applicationPage.applicationForm;
        //if (B.applicationMaster.Step3 == "Y") {
        //    A.Navigate();
        //}
        //window.applicationPage.SubmitForm();
    };
    this.SavePaymentInfo = function () {
        $(".loading-overlay").show();
        var B = window.applicationPage.applicationForm;
        //alert(B);
        var E = "{\'ApplicationForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SavePaymentInfo',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                //alert(JSON.stringify(window.applicationPage.applicationForm));
                //window.applicationPage.ShowPaymentDetails(window.applicationPage.applicationForm.applicationPayment);

                window.applicationPage.applicationForm = JSON.parse(response.d);
                
                var B = window.applicationPage.applicationForm;
                var SubmitForPay = B.applicationMaster.SubmitForPay;
                //alert(JSON.stringify(B));
                if (B.SessionExpired == "N") {
                    if (B.AppExpired == "N") {
                        if (B.SubmitFlag == "N") {
                            if (SubmitForPay != null && SubmitForPay == "Y" && B.applicationMaster.Step1 == 'Y' && B.applicationMaster.ApplicationID != null && B.applicationMaster.ApplicationID != 0) {
                                //alert(SubmitForPay);
                                if (B.applicationMaster.PaymentSuccessful == "N" && B.applicationMaster.NextStepAllowed == "N" && B.applicationMaster.Step2 == "N") {
                                    $("#button_payment_details").attr("disabled", false);
                                    //return true;
                                    //$("#btn").click();
                                    $('#btn').trigger('click');
                                } else {
                                    $("#button_payment_details").attr("disabled", false);
                                    alert("You have already paid for this post.");
                                    window.location.reload();
                                }
                                
                            }

                            //A.btnClicked();
                        } else {
                            $("#button_payment_details").attr("disabled", false);
                            alert("You have already applied for this post. To check your application status go to MyAccount.");
                            window.location.href = B.DefaultPage;
                        }
                    } else {
                        $("#button_payment_details").attr("disabled", false);
                        alert("New Registration/Submission for Payment for this Post has already expired.");
                        window.location.href = 'RegistrationExpired.aspx?T=I&E=' + B.applicationMaster.ExamID;
                    }
                } else {
                    RedirectAfterSessionExpired("login.aspx");
                }
                return false;
                //var redirect = B.Redirect;
                //if (redirect != null && redirect == "Y" && B.applicationMaster.ApplicationID != null && B.applicationMaster.ApplicationID != 0) {
                //    //alert(B.applicationMaster.ApplicationID);
                //    alert('Your Application Submitted Successfully.');
                //    $(".loading-overlay").hide();
                //    $("#button-upload-details").attr("disabled", false);
                //    //return true;
                //    window.location.href = "ApplicationCompete.aspx?ApplicationID=" + B.applicationMaster.ApplicationID + "&ExamID=" + B.applicationMaster.ExamID;
                //}
                //return false;

                $(".loading-overlay").hide();
            },
            error: function (response) {
                alert('Some Error Occur');
                $("#button_payment_details").attr("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $("#button_payment_details").attr("disabled", false);
                $(".loading-overlay").hide();
            }
        });
    };
    this.Navigate = function () {
        $("#payment-info").hide();
        $(".payment-info-tab").removeClass("tab-active");
        $(".payment-info-tab").addClass("tab-visited");
        $(".payment-info-tab").click(A.tabClick);
        window.applicationPage.qualificationinfo.transition();
        //window.applicationPage.uploadinfo.transition();
    };
    this.transition = function () {

        $("#payment-info").show();
        $(".payment-info-tab").addClass("tab-active").removeClass("tab-disabled");

    };
    this.tabClick = function () {
        window.applicationPage.setTabActive(".payment-info-tab");
        $(".tabcontent").hide();
        $("#payment-info").show();

    };
};
Employ.QualificationInfo = function () {
    var A = this;
    this.validator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#button-qualification-details").click(A.clickQualificationDetails);
       
        $("#txtFromDate").change(A.FromDateChanged);
        $("#txtToDate").change(A.ToDateChanged);
    };

    this.FromDateChanged=function(){
        checkDate(this);
    };
    this.ToDateChanged=function(){
        checkDate(this);
    };
    //this.CheckCopyClicked = function () {
    //    var $this = $(this);
    //    if ($this.is(":checked")) {
    //        $("#txtPermAddress").val($("#txtMailAddress").val());
    //        $("#txtPermPO").val($("#txtMailPO").val());
    //        $("#ddlPermState").val($("#ddlMailState").val());
    //        //window.applicationPage.PopulateMailPS();
    //        //var PermPS = $("#ddlPermPS").val();
    //        //alert(PermPS);
    //        //sleepFor(100);
    //        $("#txtPermPS").val($("#txtMailPS").val());
    //        $("#txtPermPincode").val($("#txtMailPincode").val());
    //    } else {
    //        $("#txtPermAddress").val("");
    //        $("#txtPermPO").val("");
    //        $("#ddlPermState").val("");
    //        //window.applicationPage.PopulateMailPS();
    //        $("#txtPermPS").val("");
    //        $("#txtPermPincode").val("");
    //    }
    //};  
    
    this.getValidator = function () {
        return $("#qualification-form").validate({
            rules: {
                         
                               
                ddlQualification:{required:true},
                txtYear: {
                    required: true, number: true, minlength: 4, maxlength: 4, yearcheck: '#txtDOB'
                },
                txtPercent: {
                    number: true, percentcheck: 100
                },
                
                txtBoard: {
                    noquotes: true, maxlength: 50
                },
                txtRollNo: {
                    alphaNumeric: true, maxlength: 20
                },
                
                txtQualificationName2: {
                    alphaNumeric: true, maxlength: 110,
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 3 || B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    
                },
                txtYear2: {
                    number: true, minlength: 4, maxlength: 4,
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 3 || B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    yearqualcheck: '#txtDOB'
                },
                txtBoard2: {
                    noquotes: true, maxlength: 50
                },
                txtRollNo2: {
                    alphaNumeric: true, maxlength: 20
                },
                txtPercent2: {
                    number: true, percentcheck: 100, required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtQualificationName3: {
                    alphaNumeric: true, maxlength: 110, required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if ( B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtYear3: {
                    number: true, minlength: 4, maxlength: 4,
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }, yearqualHScheck: '#txtDOB'
                },
                txtBoard3: {
                    noquotes: true, maxlength: 50
                },
                txtRollNo3: {
                    alphaNumeric: true, maxlength: 20
                },
                txtPercent3: {
                    number: true, percentcheck: 100, required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtQualificationName4: {
                    alphaNumeric: true, maxlength: 110, required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtYear4: {
                    number: true, minlength: 4, maxlength: 4,
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }, yearqualBedcheck: '#txtDOB'
                },
                txtBoard4: {
                    noquotes: true, maxlength: 50
                },
                txtRollNo4: {
                    alphaNumeric: true, maxlength: 20
                },
                txtPercent4: {
                    number: true, percentcheck: 100
                },
                chkSpokenBengali:{
                    required:function(element){
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                chkExtensive: {
                    required:function(element){
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtFromDate: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 2) {
                            return true;
                        } else if (B.PostID == 4) {
                            return (($("#txtToDate").val().length > 0) || ($.trim($("#txtInstitueName").val()).length > 0))
                        } else {
                            return false;
                        }
                    }, dpDate: true, trainingcheck: true, trainyearcheck: '#txtDOB'
                },
                txtToDate: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 2) {
                            return true;
                        } else if (B.PostID == 4) {
                            return (($("#txtFromDate").val().length > 0) || ($.trim($("#txtInstitueName").val()).length>0))
                        } else {
                            return false;
                        }
                    }, dpDate: true, trainingcheck: true
                },
                txtInstitueName: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 2) {
                            return true;
                        } else if (B.PostID == 4) {
                            return (($("#txtToDate").val().length > 0) || (($("#txtFromDate").val()).length > 0))
                        } else {
                            return false;
                        }
                    },
                    alphaNumeric: true
                },
                ddlDesiredQualification: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 1 || B.PostID == 2 || B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                ddlDesiredFlag: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 1 || B.PostID == 2 || B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                ddlDesiredQualification2: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 1 || B.PostID==4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                ddlDesiredFlag2: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 1 || B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                ddlDesiredQualification3: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                ddlDesiredFlag3: {
                    required: function (element) {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtOrg1: {
                    noquotes: true
                },
                txtDesg1: {
                    //alphaNumeric: true
                    noquotes: true
                },
                txtNature1: {
                    //alphaNumeric: true
                    noquotes: true
                },
                txtFromYear1:{
                    number:true,minlength:4,maxlength:4,empfromyearcheck:"#txtDOB"
                },
                txtToYear1: {
                    required: function (element) {
                        return $.trim($("#txtFromYear1").val()).length > 0;
                    }, number: true, minlength: 4, maxlength: 4, emptoyearcheck: "#txtFromYear1"
                },
                txtOrg2: {
                    noquotes: true
                },
                txtDesg2: {
                    //alphaNumeric: true
                    noquotes: true
                },
                txtNature2: {
                    //alphaNumeric: true
                    noquotes: true
                },
                txtFromYear2: {
                    number: true, minlength: 4,maxlength:4,empfromyearcheck:"#txtDOB"
                },
                txtToYear2: {
                    required: function (element) {
                        return $("#txtFromYear2").val().length > 0;
                    }, number: true, minlength: 4, maxlength: 4, emptoyearcheck: "#txtFromYear2"
                }
            },
            messages: {

               
                
                ddlQualification:{required:"Please select educational qualification"},
                txtYear: {
                    required: "Please enter year of passing",
                    number: "Please enter valid number",
                    minlength: "minimum length 4",
                    maxlength: "maximum length 4",
                    yearcheck: function () {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.MaxQualification == "M") {
                            return "Year must be greater than or equal to minimum 14 years from Date of Birth and less than current year";                            
                        } else if (B.MaxQualification == "E") {                           
                            return "Year must be greater than or equal to minimum 20 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "D") {                            
                            return "Year must be greater than or equal to minimum 17 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "P") {
                            return "Year must be greater than or equal to minimum 19 years from Date of Birth and less than current year";
                        }
                    }
                },
                
                txtPercent: {                   
                    number: "Please enter valid number", percentcheck: "Percentage must be greater than 0 and less than and equal to 100"
                },
                txtBoard: {
                    noquotes: "No single or double quotes are allowed."
                },
                txtRollNo: {
                    alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                },

                txtQualificationName2: {
                    alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                },
                txtYear2: {
                    number: "Number Required", minlength: "Minimum length 4", maxlength: "maximum 4 length", required: "Please enter year of passing",
                    yearqualcheck: function () {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.MaxQualification == "M") {
                            return "Year must be greater than or equal to minimum 14 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "E") {
                            return "Year must be greater than or equal to minimum 20 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "D") {
                            return "Year must be greater than or equal to minimum 17 years from Date of Birth and less than current year";
                        }else if (B.MaxQualification == "P") {
                            return "Year must be greater than or equal to minimum 14 years from Date of Birth and less than current year";
                        }
                    }
                },
                txtBoard2: {
                    noquotes: "No single or double quotes are allowed."
                },
                txtRollNo2: {
                    alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                },
                txtPercent2: {
                    number: "Number required", percentcheck: function () {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return "Percentage must be greater than equal to 55 and less than and equal to 100";
                        } else {
                            return "Percentage must be greater than 0 and less than and equal to 100";
                        }
                        
                    }, required: "Please enter percentage of marks"
                },
                txtQualificationName3: {
                    alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                },
                txtYear3: {
                    number: "Number required", minlength: "Minimum length 4", maxlength: "Maximum length 4", yearqualHScheck: function () {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.MaxQualification == "M") {
                            return "Year must be greater than or equal to minimum 14 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "E") {
                            return "Year must be greater than or equal to minimum 20 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "D") {
                            return "Year must be greater than or equal to minimum 17 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "P") {
                            return "Year must be greater than or equal to minimum 16 years from Date of Birth and less than current year";
                        }
                    }, required: "Please enter year of passing"
                },
                txtBoard3: {
                    noquotes: "No single or double quotes are allowed."
                },
                txtRollNo3: {
                    alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                },
                txtPercent3: {
                    number: "Number required",  percentcheck: function () {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 4) {
                            return "Percentage must be greater than equal to 50 and less than and equal to 100";
                        } else {
                            return "Percentage must be greater than 0 and less than and equal to 100";
                        }
                        
                    }, required: "Please enter percentage of marks"
                },
                txtQualificationName4: {
                    alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                },
                txtYear4: {
                    number: "Number required", minlength: "Minimum length 4", maxlength: "Maximum length 4", yearqualBedcheck: function () {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.MaxQualification == "M") {
                            return "Year must be greater than or equal to minimum 14 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "E") {
                            return "Year must be greater than or equal to minimum 20 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "D") {
                            return "Year must be greater than or equal to minimum 17 years from Date of Birth and less than current year";
                        } else if (B.MaxQualification == "P") {
                            return "Year must be greater than or equal to minimum 19 years from Date of Birth and less than current year";
                        }
                    }, required: "Please enter year of passing"
                },
                txtBoard4: {
                    noquotes: "No single or double quotes are allowed."
                },
                txtRollNo4: {
                    alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                },
                txtPercent4: {
                    number: "Number required", percentcheck: "Percentage must be greater than 0 and less than and equal to 100"
                },
                chkSpokenBengali: {
                    required: "Selection required for this post "
                },
                chkExtensive: {
                    required: "Selection required for this post "
                },
                txtFromDate: {
                    required: "Please enter from date"
                },
                txtToDate: {
                    required: "Please enter to date"
                    
                },
                txtInstitueName: {
                    required: function () {
                        var B = window.applicationPage.applicationForm.applicationMaster;
                        if (B.PostID == 2) {
                            return "Please enter training institute/organization/municipality name";
                        } else if (B.PostID == 4) {
                            return "Please enter school name";
                        }
                        
                    },
                    alphaNumeric: true
                },

                ddlDesiredQualification: { required: "Please select desired qualification" },
                ddlDesiredFlag: { required: "Please select yes/no" },
                ddlDesiredQualification2: { required: "Please select desired qualification" },
                ddlDesiredFlag2: { required: "Please select yes/no" },
                ddlDesiredQualification3: { required: "Please select desired qualification" },
                ddlDesiredFlag3: { required: "Please select yes/no" },
                txtOrg1: {
                    noquotes: "No single or double quotes are allowed."
                },
                txtDesg1: {
                    //alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                    noquotes: "No single or double quotes are allowed."
                },
                txtNature1: {
                    //alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                    noquotes: "No single or double quotes are allowed."
                },
                txtFromYear1: {
                    number: "Number required", minlength: "Minimum length 4", maxlength: "Maximum length 4", empfromyearcheck: "From year must be greater than or equal to minimum 16 years from Date of Birth and less than or equal to current year"
                },
                txtToYear1: { required: "Please enter to year", number: "Number required", minlength: "Minimum length 4", maxlength: "Maximum length 4", emptoyearcheck: "To year sholud be greater than or equal to from year and less than or equal to current year." },
                txtOrg2: {
                    noquotes: "No single or double quotes are allowed."
                },
                txtDesg2: {
                    //alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                    noquotes: "No single or double quotes are allowed."
                },
                txtNature2: {
                    //alphaNumeric: "No wild card characters like - ,*,?,! is allowed. Entry should be alphabetic and numeric only , i.e within A to Z and 0 to 9"
                    noquotes: "No single or double quotes are allowed."
                },
                txtFromYear2: {
                    number: "Number required", minlength: "Minimum length 4", maxlength: "Maximum length 4", empfromyearcheck: "From year must be greater than or equal to minimum 16 years from Date of Birth and less than or equal to current year"
                },
                txtToYear2: { required: "Please enter to year", number: "Number required", minlength: "Minimum length 4", maxlength: "Maximum length 4", emptoyearcheck: "To year sholud be greater than or equal to from year and less than or equal to current year." }
                
                
            }
        })
    };
    this.clickQualificationDetails = function () {
        if ( A.validator.form()) {
            A.setFormValues();
        } else {
            getAlert();
            A.validator.focusInvalid();
        }
    };
    this.setFormValues = function () {
        var B = window.applicationPage.applicationForm;
        

       
        
        
        B.applicationMaster.QualificationID = $("#ddlQualification").val();
        B.applicationMaster.Year = $.trim($("#txtYear").val());
        B.applicationMaster.DivisionGrade = $.trim($("#txtDivision").val()).toString().toUpperCase();
        B.applicationMaster.Board = $.trim($("#txtBoard").val()).toString().toUpperCase();
        B.applicationMaster.RollNo = $.trim($("#txtRollNo").val()).toString().toUpperCase();
        B.applicationMaster.Percentage = ($.trim($("#txtPercent").val()) == "" ? 0.00 : $("#txtPercent").val());


        B.applicationMaster.QualificationName2 = $.trim($("#txtQualificationName2").val()).toString().replace(/\r?\n|\r/g, " ").toUpperCase();
        B.applicationMaster.Year2 = $.trim($("#txtYear2").val());
        B.applicationMaster.DivisionGrade2 = $.trim($("#txtDivision2").val()).toString().toUpperCase();
        B.applicationMaster.Board2 = $.trim($("#txtBoard2").val()).toString().toUpperCase();
        B.applicationMaster.RollNo2 = $.trim($("#txtRollNo2").val()).toString().toUpperCase();
        B.applicationMaster.Percentage2 = ($.trim($("#txtPercent2").val()) == "" ? 0.00 : $("#txtPercent2").val());


        B.applicationMaster.QualificationName3 = $.trim($("#txtQualificationName3").val()).toString().replace(/\r?\n|\r/g, " ").toUpperCase();
        B.applicationMaster.Year3 = $.trim($("#txtYear3").val());
        B.applicationMaster.DivisionGrade3 = $.trim($("#txtDivision3").val()).toString().toUpperCase();
        B.applicationMaster.Board3 = $.trim($("#txtBoard3").val()).toString().toUpperCase();
        B.applicationMaster.RollNo3 = $.trim($("#txtRollNo3").val()).toString().toUpperCase();
        B.applicationMaster.Percentage3 = ($.trim($("#txtPercent3").val()) == "" ? 0.00 : $("#txtPercent3").val());

        B.applicationMaster.QualificationName4 = $.trim($("#txtQualificationName4").val()).toString().replace(/\r?\n|\r/g, " ").toUpperCase();
        B.applicationMaster.Year4 = $.trim($("#txtYear4").val());
        B.applicationMaster.DivisionGrade4 = $.trim($("#txtDivision4").val()).toString().toUpperCase();
        B.applicationMaster.Board4 = $.trim($("#txtBoard4").val()).toString().toUpperCase();
        B.applicationMaster.RollNo4 = $.trim($("#txtRollNo4").val()).toString().toUpperCase();
        B.applicationMaster.Percentage4 = ($.trim($("#txtPercent4").val()) == "" ? 0.00 : $("#txtPercent4").val());


        B.applicationMaster.BengaliSpoken = ($("#chkSpokenBengali").is(":checked") ? "Y" : "N");
        B.applicationMaster.ExtensiveOfficial = ($("#chkExtensive").is(":checked") ? "Y" : "N");

        B.applicationMaster.TrainingName = (B.applicationMaster.PostID == 2 || B.applicationMaster.PostID == 4 ? $.trim($("#spTrainingName").html()).toString().toUpperCase() : "");
        B.applicationMaster.FromDate = (B.applicationMaster.PostID == 2 || B.applicationMaster.PostID==4 ? $("#txtFromDate").val() : "");
        B.applicationMaster.ToDate = (B.applicationMaster.PostID == 2 || B.applicationMaster.PostID==4 ? $("#txtToDate").val() : "");
        B.applicationMaster.OrgInstMuniciName = (B.applicationMaster.PostID == 2 || B.applicationMaster.PostID==4 ? $.trim($("#txtInstitueName").val()).toString().toUpperCase() : "");

        B.applicationMaster.DesiredQualificationID = (B.applicationMaster.PostID == 1 || B.applicationMaster.PostID == 2 || B.applicationMaster.PostID == 4 ? $("#ddlDesiredQualification").val() : 0);
        B.applicationMaster.DesiredQualificationFlag = (B.applicationMaster.PostID == 1 || B.applicationMaster.PostID == 2 || B.applicationMaster.PostID == 4 ? $("#ddlDesiredFlag").val() : "");

        B.applicationMaster.DesiredQualificationID2 = (B.applicationMaster.PostID == 1 || B.applicationMaster.PostID == 4 ? $("#ddlDesiredQualification2").val() : 0);
        B.applicationMaster.DesiredQualificationFlag2 = (B.applicationMaster.PostID == 1 || B.applicationMaster.PostID == 4 ? $("#ddlDesiredFlag2").val() : "");

        B.applicationMaster.DesiredQualificationID3 = (B.applicationMaster.PostID == 4 ? $("#ddlDesiredQualification3").val() : 0);
        B.applicationMaster.DesiredQualificationFlag3 = (B.applicationMaster.PostID == 4 ? $("#ddlDesiredFlag3").val() : "");

        B.applicationMaster.OrganizationName1 = $.trim($("#txtOrg1").val()).toString().toUpperCase();
        B.applicationMaster.Designation1 = $.trim($("#txtDesg1").val()).toString().toUpperCase();
        B.applicationMaster.NatureDuty1 = $.trim($("#txtNature1").val()).toString().toUpperCase();
        B.applicationMaster.FromYear1 = $.trim($("#txtFromYear1").val());
        B.applicationMaster.ToYear1 = $.trim($("#txtToYear1").val());

        B.applicationMaster.OrganizationName2 = $.trim($("#txtOrg2").val()).toString().toUpperCase();
        B.applicationMaster.Designation2 = $.trim($("#txtDesg2").val()).toString().toUpperCase();
        B.applicationMaster.NatureDuty2 = $.trim($("#txtNature2").val()).toString().toUpperCase();
        B.applicationMaster.FromYear2 = $.trim($("#txtFromYear2").val());
        B.applicationMaster.ToYear2 = $.trim($("#txtToYear2").val());

        
        //alert(JSON.stringify(B));
        A.SaveQualificationInfo();
        
    };
    this.SaveQualificationInfo = function () {

        $(".loading-overlay").show();
        var B = window.applicationPage.applicationForm;
        var E = "{\'ApplicationForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveQualificationInfo',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.applicationPage.applicationForm = JSON.parse(response.d);
                //alert(JSON.stringify(window.applicationPage.applicationForm));
                $(".loading-overlay").hide();
                var B = window.applicationPage.applicationForm;
                if (B.SessionExpired == "N") {                   
                    if (B.SubmitFlag == "N") {
                        if (B.applicationMaster.Step3 == "Y") {
                            A.Navigate();
                        }
                    } else {
                        alert("You have already applied for this post. To check your application status go to MyAccount.");
                        window.location.href = B.DefaultPage;
                    }
                } else {
                    RedirectAfterSessionExpired("login.aspx");
                }
            },
            error: function (response) {
                alert('Some Error Occur');

                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    };
    this.Navigate = function () {
        
        $("#qualification-info").hide();
        $(".qualification-info-tab").removeClass("tab-active");
        $(".qualification-info-tab").addClass("tab-visited");
        $(".qualification-info-tab").click(A.tabClick);
        
        //window.applicationPage.paymentinfo.transition();
        window.applicationPage.uploadinfo.transition();
    };
    this.transition = function () {
        $("#qualification-info").show();
        $(".qualification-info-tab").addClass("tab-active").removeClass("tab-disabled");

    };
    this.tabClick = function () {
        window.applicationPage.setTabActive(".qualification-info-tab");
        $(".tabcontent").hide();
        $("#qualification-info").show();

    };
};
Employ.UploadInfo = function () {
    var A = this;
    this.validator;
    this.init = function () {
        this.validator = A.getValidator();
        //$('#upload-info').show();
        $('#buttonUpload').click(A.UploadFilePhoto);
        //$('#buttonUploadNOC').click(A.UploadFileNOC);
        //$('#buttonUploadEPay').click(A.UploadFileEPay);
        $('#buttonUploadSign').click(A.UploadFileSign);
        //$('#buttonUploadQual').click(A.UploadFileQual);
        //$('#buttonUploadDOB').click(A.UploadFileDOB);
        //$('#buttonUploadCaste').click(A.UploadFileCaste);
        
        $("#button-upload-details").click(A.clickUploadDetails);
    };
    this.getValidator = function () {
        return $("#upload-form").validate({
            rules: {
                lblPhoto: { required: true },
                //lblNOC: { required: true },
                //lblEPay: { required: true },
                lblSign: { required: true },
                txtPlace: { required: true, noquotes: true },
                //txtDate: { required: true },
                chkaccept: { required: true }
                //lblQual: { required: true },
                //lblDOB: { required: true },
                //lblCaste:{required:true}
                //ddlHomeDistrict: { required: true }

            },
            messages: {
                lblPhoto: "Please upload your Photo",
                //lblNOC: "Please upload your NOC",
                //lblEPay: "Please upload your E-Payment Receipt",
                lblSign: "Please upload your Signature",
                txtPlace: { required: "Please enter place" },
                //txtDate: "Please enter date",
                chkaccept: "Accept the declaration before submitting."
                //lblQual: "Please upload your education qualification certificate",
                //lblDOB: "Please upload your date of birth certificate",
                //lblCaste: "Please upload your caste certificate"
                //ddlHomeDistrict: "Please select home district"
            }
        })
    };
    this.UploadFilePhoto = function () {
        //$("#loading").ajaxStart(function () {
        //    $(this).show();
        //}).ajaxComplete(function () {
        //    $(this).hide();
        //});
        var B = window.applicationPage.applicationForm;
        $("#lblPhoto").val('');
        $("#imgPhoto").empty().html("");
        $("#canPhoto").empty().html('');
        if ($("#fileToUpload").val() == "") {
            alert('Select file before uploading');
            return false;
        }
        if (!CheckForImageFile("#fileToUpload")) {
            //alert('select');
            return false;
        }
        if (!GetFileSize("fileToUpload", 'P')) {
            //alert('select');
            return false;
        }
        
        $("#loading").show();
        //$('#buttonUpload').attr("disabled", true);
        $('#buttonUpload').hide();
        $.ajaxFileUpload(
            {
                url: 'service/AjaxFileUpload.ashx?picsFolder=UploadPhoto&ExamID=' + B.applicationMaster.ExamID + "&AppUserID=" + B.applicationMaster.ApplicationUserID+"&AID="+B.applicationMaster.ApplicationID,
                async: true,
                secureuri: false,
                fileElementId: 'fileToUpload',
                dataType: 'json',
                data: { name: 'logan', id: 'id' },
                success: function (data, status) {
                    $("#loading").hide();
                    //$('#buttonUpload').attr("disabled", false);
                    $('#buttonUpload').show();
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            var html = "<img src='images/cross.png' alt='cross' />";
                            $("#imgPhoto").empty().html(html);
                            $("#lblPhoto").val('');
                            $("#canPhoto").empty().html('');
                            alert(data.error);
                        } else {
                            var B = window.applicationPage.applicationForm;
                            $("#lblPhoto").val(data.msg);
                            var html = "<img src='images/check.png' alt='tick' />";
                            $("#imgPhoto").empty().html(html);
                            var cnhtml = "<img src='" + data.msg + "' width='100px' height='116px' alt='canimg' />"
                            $("#canPhoto").empty().html(cnhtml);

                            //B.applicationMaster.CandidatePhoto = data.msg;
                            //alert(data.msg);
                        }
                    } else {
                        var html = "<img src='images/cross.png' alt='cross' />";
                        $("#imgPhoto").empty().html(html);
                        $("#lblPhoto").val('');
                        $("#canPhoto").empty().html('');
                    }
                },
                error: function (data, status, e) {
                    alert(e);
                    //$('#buttonUpload').attr("disabled", false);
                    $('#buttonUpload').show();
                    var html = "<img src='images/cross.png' alt='cross' />";
                    $("#imgPhoto").empty().html(html);
                    $("#lblPhoto").val('');
                    $("#canPhoto").empty().html('');
                    $("#loading").hide();
                }
            }
           )
        //$('#buttonUpload').show();
        return false;
    };
    this.UploadFileSign = function () {
        //$("#loading").ajaxStart(function () {
        //    $(this).show();
        //}).ajaxComplete(function () {
        //    $(this).hide();
        //});
        var B = window.applicationPage.applicationForm;
        $("#lblSign").val('');
        $("#imgSign").empty().html("");
        $("#canSign").empty().html('');
        if ($("#fileToUploadSign").val() == "") {
            alert('Select file before uploading');
            return false;
        }
        if (!CheckForImageFile("#fileToUploadSign")) {
            //alert('select');
            return false;
        }
        if (!GetFileSize("fileToUploadSign", 'S')) {
            //alert('select');
            return false;
        }
        //$("#lblSign").val('');
        $("#loadingSign").show();
        //$('#buttonUploadSign').attr("disabled", true);
        $('#buttonUploadSign').hide();
        $.ajaxFileUpload
        (
            {
                url: 'service/AjaxFileUpload.ashx?picsFolder=UploadSign&ExamID=' + B.applicationMaster.ExamID + "&AppUserID=" + B.applicationMaster.ApplicationUserID + "&AID=" + B.applicationMaster.ApplicationID,
                async: true,
                secureuri: false,
                fileElementId: 'fileToUploadSign',
                dataType: 'json',
                data: { name: 'logan', id: 'id' },
                success: function (data, status) {
                    $("#loadingSign").hide();
                    //$('#buttonUploadSign').attr("disabled", false);
                    $('#buttonUploadSign').show();
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            var html = "<img src='images/cross.png' alt='cross' />";
                            $("#imgSign").empty().html(html);
                            $("#lblSign").val('');
                            $("#canSign").empty().html('');
                            alert(data.error);
                        } else {
                            var B = window.applicationPage.applicationForm;
                            $("#lblSign").val(data.msg);
                            var html = "<img src='images/check.png' alt='tick' />";
                            $("#imgSign").empty().html(html);
                            var cnhtml = "<img src='" + data.msg + "' width='150px' height='27px' alt='canimg' />"
                            $("#canSign").empty().html(cnhtml);
                            //B.applicationMaster.CandidateSign = data.msg;
                            //B.applicationMaster.CandidateSignBack = data.msg;
                            //alert(data.msg);
                        }
                    } else {
                        var html = "<img src='images/cross.png' alt='cross' />";
                        $("#imgSign").empty().html(html);
                        $("#lblSign").val('');
                        $("#canSign").empty().html('');
                    }
                },
                error: function (data, status, e) {
                    alert(e);
                    $('#buttonUploadSign').show();
                    //$('#buttonUploadSign').attr("disabled", false);
                    var html = "<img src='images/cross.png' alt='cross' />";
                    $("#imgSign").empty().html(html);
                    $("#lblSign").val('');
                    $("#canSign").empty().html('');
                    $("#loadingSign").hide();
                }
            }
           )
        return false;
    };
    
    this.clickUploadDetails = function () {
        //$("#invalidCaptcha").empty();
        $("#button-upload-details").attr("disabled", true);
        if (A.validator.form()) {
            var msg = "Your application will get submitted. Application once submitted will not be available for any modification. Do you want to continue? ";
            if (confirm(msg)) {
                A.setFormValues();
            } else {
                $("#button-upload-details").attr("disabled", false);
            }
        } else {
            $("#button-upload-details").attr("disabled", false);
            getAlert();
            A.validator.focusInvalid();
        }//else {
        //    
        //}

    };
    this.setFormValues = function () {
        var B = window.applicationPage.applicationForm;
        B.applicationMaster.CandidatePhoto = $.trim($("#lblPhoto").val());
        B.applicationMaster.CandidateSign = $.trim($("#lblSign").val());
        B.applicationMaster.Place = $.trim($("#txtPlace").val()).toString().toUpperCase();
        B.applicationMaster.DateOfSubmission = ($.trim($("#txtDate").val()));
        B.applicationMaster.Declaration = ($("#chkaccept").is(":checked") ? "Y" : "N");
        
        // captcha is removed as per sanjoy da on 21/10/2014 instead called direct save method
        //A.ValidateCaptcha();
        A.SaveUploadInfo();

        //window.applicationPage.SubmitForm();
        //alert(JSON.stringify(B));
        //A.Navigate();
        

    };
    this.ValidateCaptcha = function () {
        $(".loading-overlay").show();
        var captchaInfo ={
                            challengeValue: Recaptcha.get_challenge(),
                            responseValue: Recaptcha.get_response()
        }
        $.ajax({
            type: "POST",
            url: pageUrl + '/ValidateCaptcha',
            data: JSON.stringify(captchaInfo),  // requires ref to JSON (http://www.JSON.org/json2.js)
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            //async: false,
            success: function (msg) {
                //alert(msg.d);
                if (msg.d) { // Either true or false, true indicates CAPTCHA is validated successfully.
                    // this could hide your captcha widget
                    //$("#recaptchaDiv").html(" ");
                    Recaptcha.destroy();
                    // execute some JS function upon successful captcha validation
                    //window.applicationPage.SubmitForm();
                    A.SaveUploadInfo();
                } else {
                    // execute some JS function upon failed captcha validation (like throwing up a modal indicating failed attempt)
                    $(".loading-overlay").hide();
                    //$("#button-upload-details").attr("disabled", false);
                    $("#invalidCaptcha").empty().html("The Captcha text you entered did not verify, please try again. ")
                    // don't forget to reload/reset the captcha to try again
                    Recaptcha.reload();
                }
                return false;
            },
            error: function (response) {
                alert('Some Error Occur');
                //$("#button-upload-details").attr("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                //$("#button-upload-details").attr("disabled", false);
                $(".loading-overlay").hide();
            }
        });
    };
    this.SaveUploadInfo = function () {
        $(".loading-overlay").show();
        var B = window.applicationPage.applicationForm;
        var E = "{\'ApplicationForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveUploadInfo',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.applicationPage.applicationForm = JSON.parse(response.d);
                //alert(JSON.stringify(window.applicationPage.applicationForm));
                $(".loading-overlay").hide();
                var B = window.applicationPage.applicationForm;
                //alert(JSON.stringify(B));
                //alert(B.applicationMaster.Step3);
                //var redirect = B.Redirect;
                //if (redirect != null && redirect == "Y" && B.applicationMaster.ApplicationID != null && B.applicationMaster.ApplicationID != 0) {
                //    //alert(B.applicationMaster.ApplicationID);
                //    alert('Your Application Submitted Successfully.');
                //    $(".loading-overlay").hide();
                //    $("#button-upload-details").attr("disabled", false);
                //    //return true;
                //    window.location.href = "ApplicationCompete.aspx?ApplicationID=" + B.applicationMaster.ApplicationID + "&ExamID=" + B.applicationMaster.ExamID;
                //}
                if (B.SessionExpired == "N") {
                    if (B.SubmitFlag == "N") {
                        if (B.applicationMaster.ApplicationID > 0 && B.applicationMaster.Step4 == "Y" && B.applicationMaster.Step5 == "Y" && B.Redirect == "Y") {
                            alert('Your Application Submitted Successfully.');
                            //$(".loading-overlay").hide();
                            $("#button-upload-details").attr("disabled", false);
                            //return true;
                            //window.location.href = "ApplicationCompete.aspx?ApplicationID=" + B.applicationMaster.ApplicationID + "&ExamID=" + B.applicationMaster.ExamID;
                            window.location.href = B.CompletionPage;
                        }
                    } else {
                        alert("You have already applied for this post. To check your application status go to MyAccount.");
                        window.location.href = B.DefaultPage;
                    }
                } else {
                    RedirectAfterSessionExpired("login.aspx");
                }
            },
            error: function (response) {
                $("#button-upload-details").attr("disabled", false);
                alert('Some Error Occur');

                //Recaptcha.reload();
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                $("#button-upload-details").attr("disabled", false);
                alert(response.d);
                //Recaptcha.reload();
                $(".loading-overlay").hide();
            }
        });
    };
    this.Navigate = function () {
        //$("#upload-info").hide();
        //$(".upload-info-tab").removeClass("tab-active");
        //$(".upload-info-tab").addClass("tab-visited");
        //$(".upload-info-tab").click(A.tabClick);
        ////window.applicationPage.personalinfo.transition();
        //window.applicationPage.paymentinfo.transition();
    };
    this.transition = function () {

        $("#upload-info").show();
        $(".upload-info-tab").addClass("tab-active").removeClass("tab-disabled");
        //A.ShowCaptcha();
    };
    this.ShowCaptcha = function () {
        Recaptcha.create("6LfRBO4SAAAAAPEyD96zJKHhZO_WGOtSRwNcT2lu", 'recaptchaDiv', {
            theme: "clean",
            callback: Recaptcha.focus_response_field
        });
    };
    this.tabClick = function () {
        window.applicationPage.setTabActive(".upload-info-tab");
        $(".tabcontent").hide();
        $("#upload-info").show();
        //A.ShowCaptcha();
    };
};

Employ.ApplicationPage = function () {
    var A = this;
    this.applicationForm;
    this.setTabActive = function (C) {
        var D = parseInt($(C).attr("id").substring(4));
        for (var B = D; B < 5; B++) {
            $("#step" + B).unbind("click").addClass("tab-disabled").removeClass("tab-visited").removeClass("tab-active")
        } $(C).addClass("tab-active").removeClass("tab-disabled")
    };
    this.init = function () {
        if ($("#applicationJSON").val() != "undefined" && $("#applicationJSON").val() != null) {
            this.applicationForm = JSON.parse($("#applicationJSON").val());
        }
        //alert(JSON.stringify(this.applicationForm));
        
        if (this.applicationForm != null) {
            this.personalinfo = new Employ.PersonalInfo();
            this.personalinfo.init();
            this.paymentinfo = new Employ.PaymentInfo();
            this.paymentinfo.init();
            this.qualificationinfo = new Employ.QualificationInfo();
            this.qualificationinfo.init();

            this.uploadinfo = new Employ.UploadInfo();
            this.uploadinfo.init();
            A.SetFormInit();
        }
        //this.bookingInfo = new WBFDC.BookingInfo();
        //this.bookingInfo.init();
        //this.customerDetails = new WBFDC.CustomerDetails();
        //this.customerDetails.init();
        //this.paymentDetails = new WBFDC.PaymentDetails();
        //this.paymentDetails.init();
        //$("#ddlLocation").change(A.PopulateProperty);
        
        //$("#txtBookingDate").change(A.BookingDateChanged);
        //$("#txtBookingDateTo").change(A.BookingDateChangedTo);
        //$("#ddlProperty").change(A.PopulateTariff);

    };
    this.SetApplicationAmount = function () {
        if ($("#ddlPayMode").val() != "") {
            var PWD = ($("#ddlPWD").val() == "" ? "N" : $("#ddlPWD").val());
            //alert(PWD);
            if ($("#ddlPayMode").val() == "O") {
                if (PWD == "N") {
                    if ($("#ddlCaste").val() != "C" && $("#ddlCaste").val() != "T") {

                        $("#tab-payment *").attr("disabled", true);
                        var B = window.applicationPage.applicationForm;
                        //alert(B.RequiredServiceCharge);

                        $("#txtApplAmt").val(B.RequiredApplicationAmount).attr('disabled', true);
                        $("#txtProcessingCharge").val(B.RequiredProcessingCharge).attr('disabled', true);
                        $("#txtBankCharge").val(0.00).attr('disabled', true);
                        B.TotalRequiredAmount = (parseFloat(B.RequiredApplicationAmount) + parseFloat(B.RequiredProcessingCharge));
                        $("#txtTotal").val(B.TotalRequiredAmount).attr('disabled', true);
                        //alert(B.TotalRequiredAmount);
                    } else {

                        $("#tab-payment *").attr("disabled", true);
                        var B = window.applicationPage.applicationForm;
                        //alert(B.RequiredServiceCharge);

                        $("#txtApplAmt").val(0.00).attr('disabled', true);
                        $("#txtProcessingCharge").val(B.RequiredProcessingCharge).attr('disabled', true);
                        $("#txtBankCharge").val(0.00).attr('disabled', true);
                        B.TotalRequiredAmount = (parseFloat(B.RequiredProcessingCharge));
                        $("#txtTotal").val(B.TotalRequiredAmount).attr('disabled', true);
                        //alert(B.TotalRequiredAmount);
                    }
                } else if (PWD == "Y") {



                    $("#tab-payment *").attr("disabled", true);
                    var B = window.applicationPage.applicationForm;
                    //alert(B.RequiredServiceCharge);

                    $("#txtApplAmt").val(0.00).attr('disabled', true);
                    $("#txtProcessingCharge").val(B.RequiredProcessingCharge).attr('disabled', true);
                    $("#txtBankCharge").val(0.00).attr('disabled', true);
                    B.TotalRequiredAmount = (parseFloat(0.00) + parseFloat(B.RequiredProcessingCharge));
                    $("#txtTotal").val(B.TotalRequiredAmount).attr('disabled', true);
                    //alert(B.TotalRequiredAmount);


                }

            } else if ($("#ddlPayMode").val() == "F") {
                if (PWD == "N") {
                    if ($("#ddlCaste").val() != "C" && $("#ddlCaste").val() != "T") {

                        $("#tab-payment *").attr("disabled", true);
                        var B = window.applicationPage.applicationForm;
                        //alert(B.RequiredServiceCharge);

                        $("#txtApplAmt").val(B.RequiredApplicationAmount).attr('disabled', true);
                        $("#txtProcessingCharge").val(B.RequiredProcessingCharge).attr('disabled', true);
                        $("#txtBankCharge").val(B.RequiredBankCharge).attr('disabled', true);
                        B.TotalRequiredAmount = parseFloat(B.RequiredApplicationAmount) + parseFloat(B.RequiredProcessingCharge) + parseFloat(B.RequiredBankCharge);
                        $("#txtTotal").val(B.TotalRequiredAmount).attr('disabled', true);
                        //alert(B.TotalRequiredAmount);
                    } else {

                        $("#tab-payment *").attr("disabled", true);
                        var B = window.applicationPage.applicationForm;
                        //alert(B.RequiredServiceCharge);

                        $("#txtApplAmt").val(0.00).attr('disabled', true);
                        $("#txtProcessingCharge").val(B.RequiredProcessingCharge).attr('disabled', true);
                        $("#txtBankCharge").val(B.RequiredBankCharge).attr('disabled', true);
                        B.TotalRequiredAmount = parseFloat(0.00) + parseFloat(B.RequiredProcessingCharge) + parseFloat(B.RequiredBankCharge);
                        $("#txtTotal").val(B.TotalRequiredAmount).attr('disabled', true);
                        //alert(B.TotalRequiredAmount);
                    }
                } else if (PWD == "Y") {



                    $("#tab-payment *").attr("disabled", true);
                    var B = window.applicationPage.applicationForm;
                    //alert(B.RequiredServiceCharge);

                    $("#txtApplAmt").val(0.00).attr('disabled', true);
                    $("#txtProcessingCharge").val(B.RequiredProcessingCharge).attr('disabled', true);
                    $("#txtBankCharge").val(B.RequiredBankCharge).attr('disabled', true);
                    B.TotalRequiredAmount = parseFloat(0.00) + parseFloat(B.RequiredProcessingCharge) + parseFloat(B.RequiredBankCharge);
                    $("#txtTotal").val(B.TotalRequiredAmount).attr('disabled', true);
                    //alert(B.TotalRequiredAmount);


                }

            }
        } else {
            $("#tab-payment *").attr("disabled", true);
            var B = window.applicationPage.applicationForm;
            //alert(B.RequiredServiceCharge);

            $("#txtApplAmt").val("").attr('disabled', true);
            $("#txtProcessingCharge").val("").attr('disabled', true);
            $("#txtBankCharge").val("").attr('disabled', true);
            //B.TotalRequiredAmount = parseFloat(0.00) + parseFloat("") + parseFloat(B.RequiredBankCharge);
            $("#txtTotal").val("").attr('disabled', true);
        }
    };
    
    this.SetFormInit = function () {
        var B = A.applicationForm.applicationMaster;
        var C = A.applicationForm.applicationPayment;
        if (B.ApplicationID > 0) {
            A.SetMasterInfo(B);
            //A.ShowPaymentDetails(C);
        } else {
            $("select#ddlPost").prop('selectedIndex', 1);
            $("#ddlPost").attr("disabled", true);
            window.applicationPage.personalinfo.PostChanged();
            $("#txtAdvNo").val(A.applicationForm.AdvertisementNo).attr("disabled", true);
            $("#txtCandidateEmail").val((B.Email == null ? "" : B.Email));
            //if (applMaster.Email != null) {
            //    $("#txtCandidateEmail").attr("disabled", true);
            //} else {
            //    $("#txtCandidateEmail").attr("disabled", false);
            //}
            $("#txtCandidateMobile").val((B.Mobile == null ? "" : B.Mobile));
            //if (applMaster.Mobile != null) {
            //    $("#txtCandidateMobile").attr("disabled", true);
            //} else {
            //    $("#txtCandidateMobile").attr("disabled", false);
            //}
        }

    };
    this.SetMasterInfo = function (applMaster) {
        //personal info
       
        //alert(applMaster.PostID);
        //alert(JSON.stringify(applMaster));
        $("#ddlPost").val((applMaster.PostID == "" ? "" : applMaster.PostID));
        A.PopulateQualification();
        //A.PopulateDesiredQualification();
        A.ShowHideQualTrainDesired();
        $("#txtAdvNo").val((applMaster.AdvertisementNo == null ? "" : applMaster.AdvertisementNo));
        $("#txtName").val((applMaster.Name == null ? "" : applMaster.Name));
        //if (applMaster.Name != null) {
        //    $("#txtName").attr("disabled", true);
        //} else {
        //    $("#txtName").attr("disabled", false);
        //}
        //$("#txtFHName").val((applMaster.FatherName == null ? (applMaster.HusbandName == null ? applMaster.MotherName : applMaster.HusbandName) : applMaster.FatherName));
        $("#txtFHName").val((applMaster.FatherName == null ? applMaster.GuardianName : applMaster.FatherName));
        $("#txtFHMobileNo").val((applMaster.FHMobile == null ? "" : applMaster.FHMobile));
        if (applMaster.FatherName != null) {
            //$("#rdH").prop('checked', true);
            $("#rdF").prop('checked', true);
            //$("#rdF").prop('checked', false);
        }
        if (applMaster.GuardianName != null) {
            $("#rdH").prop('checked',true );
            //$("#rdF").prop('checked', true);
        }
        //if (applMaster.MotherName != null) {
        //    //$("#rdH").prop('checked', false);
        //    $("#rdM").prop('checked', true);
        //}
        (applMaster.Nationality == "Y" ? $("#chkIndian").prop('checked', true) : $("#chkIndian").prop('checked', false));
        $("#ddlSex").val((applMaster.Sex == null ? "" : applMaster.Sex));

        $("#ddlCaste").val((applMaster.CasteID == null ? "" : applMaster.CasteID));
        $("#ddlPWD").val((applMaster.PWD == null ? "" : applMaster.PWD));
        $("#ddlStreamSpecialisation").val((applMaster.DepartmentName == null ? "" : applMaster.DepartmentName));
        $("#ddlMinority").val((applMaster.Minority == null ? "" : applMaster.Minority));
       // $("#ddlKMC").val((applMaster.KMC == null ? "" : applMaster.KMC));
        $("#txtDOB").val((applMaster.DOB == null ? "" : applMaster.DOB));
        //window.applicationPage.personalinfo.CheckDOB();
        if (applMaster.AgeAsOn != null) {
            var age = applMaster.AgeAsOn.split("-");
            $("#txtYY").val(age[0]);
            $("#txtMM").val(age[1]);
            $("#txtDD").val(age[2]);
        } 
        //else {
        //    $("#txtYY").val("");
        //    $("#txtMM").val("");
        //    $("#txtDD").val("");
        //}

        $("#txtCandidateEmail").val((applMaster.Email == null ? "" : applMaster.Email));
        
        $("#txtCandidateMobile").val((applMaster.Mobile == null ? "" : applMaster.Mobile));

        $("#txtMailAddress").val((applMaster.MailAddress == null ? "" : applMaster.MailAddress));
        $("#txtMailPO").val((applMaster.MailPO == null ? "" : applMaster.MailPO));
        $("#ddlMailState").val((applMaster.MailStateID == 0 ? "" : applMaster.MailStateID));

        $("#txtMailPS").val((applMaster.MailPS == 0 ? "" : applMaster.MailPS));


        $("#txtMailPincode").val((applMaster.MailPinCode == 0 ? "" : applMaster.MailPinCode));

        $("#txtPermAddress").val((applMaster.PermAddress == null ? "" : applMaster.PermAddress));
        $("#txtPermPO").val((applMaster.PermPO == null ? "" : applMaster.PermPO));
        $("#ddlPermState").val((applMaster.PermStateID == 0 ? "" : applMaster.PermStateID));

        $("#txtPermPS").val((applMaster.PermPS == 0 ? "" : applMaster.PermPS));

        $("#txtPermPincode").val((applMaster.PermPinCode == 0 ? "" : applMaster.PermPinCode));

        
        A.SetStep1Completed();
        
        

        //Indian Citizen by deafult
        // payment and other info

        $("#ddlPayMode").val((applMaster.PayMode == "" ? "" : applMaster.PayMode));
        A.SetApplicationAmount();
        A.SetStep2Completed();

        //qualication info
        
      
        
        
        
        (applMaster.QualificationID == 0 ? $("select#ddlQualification").prop('selectedIndex', 1) : $("#ddlQualification").val(applMaster.QualificationID));
        //$("#ddlQualification").val((applMaster.QualificationID == "" ? "" : applMaster.QualificationID));
        $("#txtYear").val((applMaster.Year == null ? "" : applMaster.Year));
        $("#txtDivision").val((applMaster.DivisionGrade == null ? "" : applMaster.DivisionGrade));
        $("#txtBoard").val((applMaster.Board == null ? "" : applMaster.Board));
        $("#txtRollNo").val((applMaster.RollNo == null ? "" : applMaster.RollNo));
        $("#txtPercent").val((applMaster.Percentage == 0.00 ? "" : applMaster.Percentage));

        if (applMaster.PostID == 3) {
            $("#txtQualificationName2").val((applMaster.QualificationName2 == null ? "Qualification in general nursing and midwifery from Nursing Council of West Bengal or its equivalent" : applMaster.QualificationName2));            
        } else if (applMaster.PostID == 4) {
            $("#txtQualificationName2").val((applMaster.QualificationName2 == null ? "Madhyamik or Equivalent" : applMaster.QualificationName2));
        } else {
            $("#txtQualificationName2").val((applMaster.QualificationName2 == null ? "" : applMaster.QualificationName2));
        }
        

        $("#txtYear2").val((applMaster.Year2 == null ? "" : applMaster.Year2));
        $("#txtDivision2").val((applMaster.DivisionGrade2 == null ? "" : applMaster.DivisionGrade2));
        $("#txtBoard2").val((applMaster.Board2 == null ? "" : applMaster.Board2));
        $("#txtRollNo2").val((applMaster.RollNo2 == null ? "" : applMaster.RollNo2));
        $("#txtPercent2").val((applMaster.Percentage2 == 0.00 ? "" : applMaster.Percentage2));

        if (applMaster.PostID == "4") {
            $("#txtQualificationName3").val((applMaster.QualificationName3 == null ? "Higher Secondary or Equivalent" : applMaster.QualificationName3));
        } else {
            $("#txtQualificationName3").val((applMaster.QualificationName3 == null ? "" : applMaster.QualificationName3));
        }
        

        $("#txtYear3").val((applMaster.Year3 == null ? "" : applMaster.Year3));
        $("#txtDivision3").val((applMaster.DivisionGrade3 == null ? "" : applMaster.DivisionGrade3));
        $("#txtBoard3").val((applMaster.Board3 == null ? "" : applMaster.Board3));
        $("#txtRollNo3").val((applMaster.RollNo3 == null ? "" : applMaster.RollNo3));
        $("#txtPercent3").val((applMaster.Percentage3 == 0.00 ? "" : applMaster.Percentage3));

        if (applMaster.PostID == "4") {
            $("#txtQualificationName4").val((applMaster.QualificationName4 == null ? "Bachelor of Education from a recognized University or equivalent NCTE Approved" : applMaster.QualificationName4));
        } else {
            $("#txtQualificationName4").val((applMaster.QualificationName4 == null ? "" : applMaster.QualificationName4));
        }


        $("#txtYear4").val((applMaster.Year4 == null ? "" : applMaster.Year4));
        $("#txtDivision4").val((applMaster.DivisionGrade4 == null ? "" : applMaster.DivisionGrade4));
        $("#txtBoard4").val((applMaster.Board4 == null ? "" : applMaster.Board4));
        $("#txtRollNo4").val((applMaster.RollNo4 == null ? "" : applMaster.RollNo4));
        $("#txtPercent4").val((applMaster.Percentage4 == 0.00 ? "" : applMaster.Percentage4));

        (applMaster.BengaliSpoken == "Y" ? $("#chkSpokenBengali").prop('checked', true) : $("#chkSpokenBengali").prop('checked', false));
        (applMaster.ExtensiveOfficial == "Y" ? $("#chkExtensive").prop('checked', true) : $("#chkExtensive").prop('checked', false));

        if (applMaster.PostID == 2) {
            $("#spTrainingName").html("One year`s practical training or studying or research or practical Engineering experience in Municipal Engineering work");
            $("#td_orgName").html("Organization/Institute/Municipality Name");
        } else if (applMaster.PostID == 4) {
            $("#spTrainingName").html("Teaching experience in school");
            $("#td_orgName").html("School Name");
        }


        $("#txtFromDate").val((applMaster.FromDate == null ? "" : applMaster.FromDate));
        $("#txtToDate").val((applMaster.ToDate == null ? "" : applMaster.ToDate));
        $("#txtInstitueName").val((applMaster.OrgInstMuniciName == null ? "" : applMaster.OrgInstMuniciName));

        if (applMaster.PostID == 3) {
            (applMaster.DesiredQualificationID == 0 ? $("#ddlDesiredQualification").val("") : $("#ddlDesiredQualification").val(applMaster.DesiredQualificationID));
            //$("#ddlDesiredQualification").val((applMaster.DesiredQualificationID == "" ? "" : applMaster.DesiredQualificationID));
            $("#ddlDesiredFlag").val((applMaster.DesiredQualificationFlag == "" ? "" : applMaster.DesiredQualificationFlag));
        } else {
            (applMaster.DesiredQualificationID == 0 ? $("select#ddlDesiredQualification").prop('selectedIndex', 1) : $("#ddlDesiredQualification").val(applMaster.DesiredQualificationID));
            //$("#ddlDesiredQualification").val((applMaster.DesiredQualificationID == "" ? "" : applMaster.DesiredQualificationID));
            $("#ddlDesiredFlag").val((applMaster.DesiredQualificationFlag == "" ? "" : applMaster.DesiredQualificationFlag));
        }
        

        if (applMaster.PostID == 1) {
            (applMaster.DesiredQualificationID2 == 0 ? $("select#ddlDesiredQualification2").prop('selectedIndex', 1) : $("#ddlDesiredQualification2").val(applMaster.DesiredQualificationID2));
            //$("#ddlDesiredQualification").val((applMaster.DesiredQualificationID == "" ? "" : applMaster.DesiredQualificationID));
            $("#ddlDesiredFlag2").val((applMaster.DesiredQualificationFlag2 == "" ? "" : applMaster.DesiredQualificationFlag2));
        } else if (applMaster.PostID == 4) {
            (applMaster.DesiredQualificationID2 == 0 ? $("select#ddlDesiredQualification2").prop('selectedIndex', 1) : $("#ddlDesiredQualification2").val(applMaster.DesiredQualificationID2));
            //$("#ddlDesiredQualification").val((applMaster.DesiredQualificationID == "" ? "" : applMaster.DesiredQualificationID));
            $("#ddlDesiredFlag2").val((applMaster.DesiredQualificationFlag2 == "" ? "" : applMaster.DesiredQualificationFlag2));
        } else {
            (applMaster.DesiredQualificationID2 == 0 ? $("#ddlDesiredQualification2").val("") : $("#ddlDesiredQualification2").val(applMaster.DesiredQualificationID2));
            //$("#ddlDesiredQualification").val((applMaster.DesiredQualificationID == "" ? "" : applMaster.DesiredQualificationID));
            $("#ddlDesiredFlag2").val((applMaster.DesiredQualificationFlag2 == "" ? "" : applMaster.DesiredQualificationFlag2));
        }
        
        if (applMaster.PostID == 4) {
            (applMaster.DesiredQualificationID3 == 0 ? $("select#ddlDesiredQualification3").prop('selectedIndex', 1) : $("#ddlDesiredQualification3").val(applMaster.DesiredQualificationID3));
            //$("#ddlDesiredQualification").val((applMaster.DesiredQualificationID == "" ? "" : applMaster.DesiredQualificationID));
            $("#ddlDesiredFlag3").val((applMaster.DesiredQualificationFlag3 == "" ? "" : applMaster.DesiredQualificationFlag3));
        } else {
            (applMaster.DesiredQualificationID3 == 0 ? $("#ddlDesiredQualification3").val("") : $("#ddlDesiredQualification3").val(applMaster.DesiredQualificationID3));
            //$("#ddlDesiredQualification").val((applMaster.DesiredQualificationID == "" ? "" : applMaster.DesiredQualificationID));
            $("#ddlDesiredFlag3").val((applMaster.DesiredQualificationFlag3 == "" ? "" : applMaster.DesiredQualificationFlag3));
        }

        $("#txtOrg1").val((applMaster.OrganizationName1 == null ? "" : applMaster.OrganizationName1));
        $("#txtDesg1").val((applMaster.Designation1 == null ? "" : applMaster.Designation1));
        $("#txtNature1").val((applMaster.NatureDuty1 == null ? "" : applMaster.NatureDuty1));
        $("#txtFromYear1").val((applMaster.FromYear1 == null ? "" : applMaster.FromYear1));
        $("#txtToYear1").val((applMaster.ToYear1 == null ? "" : applMaster.ToYear1));

        $("#txtOrg2").val((applMaster.OrganizationName2 == null ? "" : applMaster.OrganizationName2));
        $("#txtDesg2").val((applMaster.Designation2 == null ? "" : applMaster.Designation2));
        $("#txtNature2").val((applMaster.NatureDuty2 == null ? "" : applMaster.NatureDuty2));
        $("#txtFromYear2").val((applMaster.FromYear2 == null ? "" : applMaster.FromYear2));
        $("#txtToYear2").val((applMaster.ToYear2 == null ? "" : applMaster.ToYear2));
        
        
        //upload info
        $("#lblPhoto").val((applMaster.CandidatePhoto == null ? "" : applMaster.CandidatePhoto));        
        $("#lblSign").val((applMaster.CandidateSign == null ? "" : applMaster.CandidateSign));

        var html = (applMaster.CandidatePhoto == null ? "" : "<img src='images/check.png' alt='check' />");
        $("#imgPhoto").empty().html(html);
        var cnhtml = (applMaster.CandidatePhoto == null ? "" : "<img src='" + applMaster.CandidatePhoto + "' width='100px' height='116px' alt='canimg' />");
        $("#canPhoto").empty().html(cnhtml);

        html = (applMaster.CandidateSign == null ? "" : "<img src='images/check.png' alt='check' />")
        $("#imgSign").empty().html(html);
        var cnshtml = (applMaster.CandidateSign == null ? "" : "<img src='" + applMaster.CandidateSign + "' width='150px' height='27px' alt='cansimg' />");
        $("#canSign").empty().html(cnshtml);

        
        $("#txtPlace").val((applMaster.Place == null ? "" : applMaster.Place));
        (applMaster.Declaration == "Y" ? $("#chkaccept").prop('checked', true) : $("#chkaccept").prop('checked', false));

        A.SetNextStepAllowed();
        
    }
    this.ShowHideQualTrainDesired = function () {
        var B = window.applicationPage.applicationForm;
        if (B.applicationMaster.PostID == 1) {
            $("#ddlKMC").attr("disabled", false);
            $("#rdTr").html('');
            $("#rESQ").html('');
            $("#tr_training *").attr("disabled", true);
            $("#tr_BSWE *").attr("disabled", true);
            $("#tr_desQual3 *").attr("disabled", true);
            //$("#tr_training").hide();
            //$("#tr_desQual2").show();
            window.applicationPage.PopulateDesiredQualification();
            window.applicationPage.PopulateDesiredQualification2();
        } else if (B.applicationMaster.PostID == 2) {
            $("#ddlKMC").val("N").attr("disabled", true);
            $("#tr_BSWE *").attr("disabled", true);
            $("#rESQ").html('');
            $("#sp_train").html('Training Details');
            $("#tr_desQual2 *").attr("disabled", true);
            $("#tr_desQual3 *").attr("disabled", true);
            //$("#tr_desQual2").hide();
            //$("#tr_training").show();
            //$("#txtQualificationName2").attr("disabled", false);
            window.applicationPage.PopulateDesiredQualification();
        } else if (B.applicationMaster.PostID == 3) {
            $("#ddlKMC").val("N").attr("disabled", true);
            $("#tr_BSWE *").attr("disabled", true);
            $("#rdTr").html('');
            $("#rESQ").html('');
            $("#tr_training *").attr("disabled", true);
            $("#tr_desQualDet *").attr("disabled", true);
            //$("#tr_desQualDet").hide();
            //$("#tr_training").hide();
            //$("#txtQualificationName2").val("Qualification in general nursing and midwifery from Nursing Council of West Bengal or its equivalent");
            $("#txtQualificationName2").attr("disabled", true);
        } else if (B.applicationMaster.PostID == 4) {
            $("#ddlKMC").val("N").attr("disabled", true);
            $("#tr_BSWE *").attr("disabled", false);
            $("#sp_train").html('Teaching Experience in school');
            $("#rdTr").html('');
            //$("#tr_training *").attr("disabled", true);
            //$("#tr_desQualDet *").attr("disabled", true);
            //$("#tr_desQualDet").hide();
            //$("#tr_training").hide();
            //$("#txtQualificationName2").val("Qualification in general nursing and midwifery from Nursing Council of West Bengal or its equivalent");
            $("#txtQualificationName2").attr("disabled", true);
            $("#txtQualificationName3").attr("disabled", true);
            $("#txtQualificationName4").attr("disabled", true);
            window.applicationPage.PopulateDesiredQualification();
            window.applicationPage.PopulateDesiredQualification2();
            window.applicationPage.PopulateDesiredQualification3();
            
        }

    }
    this.ShowPaymentDetails = function (applicationPayment) {
        var html = '';
        if (applicationPayment != "undefined") {
            if (applicationPayment.length > 0) {
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td class='th1'></td>"
                    //+ "<td class='th1'>SlNo</td>"
                    + "<td class='th1'>Payment Type</td>"                    
                    + "<td class='th1'>IPO/DD No</td>"
                    + "<td class='th1'>P.O./Bank</td>"
                    + "<td class='th1'>B.B. Code</td>"
                    + "<td class='th1'>Amount</td>"
                    + "</tr>";
                var applDetails = Employ.Utils.fixArray(applicationPayment);
                for (var i in applDetails) {
                    html += "<tr id='ApplicationIPOID_" + applDetails[i].ApplicationIPOID + "'>"
                                    + "<td class='tr1'><div><a href='" + pageUrl + '/DeleteApplicationDetail' + "' onClick='return window.applicationPage.removeApplicationDetail(" + applDetails[i].ApplicationIPOID + ");'><img src='images/delete-icon.png' id='deleteApplicationIPOID_" + applDetails[i].ApplicationIPOID + "' /></a></div></td>"
                               // + "<td  class='tr1' >" + applDetails[i].ApplicationIPOID + "</td>"
                                + "<td  class='tr1' >" + applDetails[i].IPODemand + "</td>"                                
                                + "<td  class='tr1' >" + applDetails[i].IPODemandNo + "</td>"
                                + "<td  class='tr1' >" + applDetails[i].PostOfficeBank + "</td>"
                                + "<td  class='tr1' >" + applDetails[i].BankBranchCode + "</td>"
                                + "<td  class='tr1' >" + applDetails[i].IPODemandAmount  + "</td>";
                    html += "</tr>";
                }
                html += "<tr>";
                html += "<td colspan='5' class='tr1' >Total</td>"
                        + "<td class='tr1' >" + window.applicationPage.applicationForm.TotalIPO + "</td>";
                html += "</tr>";
                html += "</table>";
            }
        }
        $("#divTable").empty().html(html);
    };
    this.SetStep1Completed = function () {
        var B = window.applicationPage.applicationForm;
        if (B.applicationMaster.Step1 == "Y") {
            ($("#ddlPost").attr('disabled', true));
            $("#txtAdvNo").attr('disabled', true);
            $("#txtName").attr('disabled', true);
            $("#rdF").attr('disabled', true);
            $("#rdH").attr('disabled', true);
            $("#rdM").attr('disabled', true);
            $("#txtFHName").attr('disabled', true);
            $("#txtFHMobileNo").attr('disabled', true);
            ($("#chkIndian").attr('disabled', true));
            ($("#ddlSex").attr('disabled', true));
            $("#ddlCaste").attr('disabled', true);
            $("#ddlMinority").attr('disabled', true);
            $("#ddlStreamSpecialisation").attr('disabled', true);
          //  $("#ddlKMC").attr('disabled', true);
            $("#ddlPWD").attr('disabled', true);
            $("#txtDOB").attr('disabled', true);
            //alert($("#txtDOB").next().attr("class"));
            $("#txtDOB").next().attr("disabled", true);
            ($("#txtYY").attr('disabled', true));
            $("#txtMM").attr('disabled', true);
            $("#txtDD").attr('disabled', true);
            $("#txtCandidateEmail").attr('disabled', true);
            $("#txtCandidateMobile").attr('disabled', true);

            //*************************************************************//
            $("#txtMailAddress").attr('disabled', true);
            $("#txtMailPO").attr('disabled', true);
            $("#ddlMailState").attr('disabled', true);
            $("#txtMailPS").attr('disabled', true);
            $("#txtMailPincode").attr('disabled', true);


            $("#txtPermAddress").attr('disabled', true);
            $("#txtPermAddress").attr('disabled', true);
            $("#txtPermPO").attr('disabled', true);
            $("#ddlPermState").attr('disabled', true);
            $("#txtPermPincode").attr('disabled', true);
            //***********************************************************//
       
           
        }
    };
    this.SetStep2Completed = function () {
        var B = window.applicationPage.applicationForm;
        if (B.applicationMaster.Step2 == "Y") {
            ($("#ddlPayMode").attr('disabled', true));
            $("#tab-payment *").attr("disabled", true);
            $("#chkPayAccept").attr("checked", true).attr("disabled", true);
        }
    };
    this.SetNextStepAllowed = function () {
        var C = window.applicationPage.applicationForm;
        var B = window.applicationPage.applicationForm.applicationMaster;
        if (B.Step1 == "Y") {
            window.applicationPage.personalinfo.Navigate();
            //$(".personal-info-tab").addClass("tab-disabled").removeClass("tab-visited").removeClass("tab-active").unbind("click");
            if (B.Step2 == "Y") {
                $("#button_payment_details").hide();
                $("#button-pay-next-details").show();
                if (B.SubmitForPay == "Y" && B.PaymentSuccessful == "Y" && B.NextStepAllowed == "Y") {
                    //window.applicationPage.personalinfo.Navigate();
                    //$(".personal-info-tab").addClass("tab-disabled").removeClass("tab-visited");
                    window.applicationPage.paymentinfo.Navigate();                    
                    //$(".payment-info-tab").addClass("tab-disabled").removeClass("tab-visited").removeClass("tab-active").unbind("click");
                    
                } else if (B.SubmitForPay == "Y" && B.PaymentSuccessful == "N" && B.NextStepAllowed == "N") {
                    if (B.PayMode == "O") {
                        alert("Your Online payment was not successful. You are not allowed to move to next steps.");
                        window.location.href = C.DefaultPage;

                    } else if (B.PayMode=="F") {
                        alert("Your payment through challan has not been reconcile with bank. It will take 2 days from the date of payment on bank. You are not allowed to move to next steps.");
                        window.location.href = C.DefaultPage;
                    }
                }
            } 
        }
    };
    this.removeApplicationDetail = function (ApplicationIPOID) {
        //alert('in remove ' + EnquiryTokenDetID);
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#ApplicationIPOID_" + ApplicationIPOID;
        $(removeElemTr).append(ajaxLoader);
        //var ajaxUrl = pageUrl + '/DeleteEnquiryDetail';

        var B = {};
        //alert('b empty');
        B = window.applicationPage.applicationForm;

        A.findAndRemove(B.applicationPayment, 'ApplicationIPOID', ApplicationIPOID);
        //alert(JSON.stringify(B));
        A.ShowPaymentDetails(B.applicationPayment);
        window.applicationPage.paymentinfo.clearTextBox();
        //alert('b added');
        
        
        return false;
    };
    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                var B = window.applicationPage.applicationForm;
                //alert(result["IPODemandAmount"]);
                B.TotalIPO = parseInt(B.TotalIPO) - parseInt(result["IPODemandAmount"]);
                array.splice(index, 1);
                return false;

            }
        });
    };
    this.SubmitForm = function () {
        //alert('in submit');
        $(".loading-overlay").show();
        var B = window.applicationPage.applicationForm;
        var E = "{\'ApplicationForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SubmitApplication',
            data: E,
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                A.applicationForm = JSON.parse(response.d);
                //alert(JSON.stringify(bid));
                var B=A.applicationForm;
                var redirect = B.Redirect;
                if (redirect != null && redirect == "Y" && B.applicationMaster.ApplicationID!=null && B.applicationMaster.ApplicationID!=0) {
                    //alert(B.applicationMaster.ApplicationID);
                    alert('Your Application Submitted Successfully.');
                    $(".loading-overlay").hide();
                    $("#button-upload-details").attr("disabled", false);
                    //return true;
                    window.location.href = "ApplicationCompete.aspx?ApplicationID=" + B.applicationMaster.ApplicationID + "&ExamID=" + B.applicationMaster.ExamID;
                }
                //return false;
            },
            error: function (response) {
                alert('Some Error Occur');
                $("#button-upload-details").attr("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $("#button-upload-details").attr("disabled", false);
                $(".loading-overlay").hide();
            }
        });
    };
    this.PopulateQualification = function () {
        //alert('in prop');
        var B = window.applicationPage.applicationForm;
        $("#ddlQualification").attr("disabled", "disabled");

        if ($('#ddlPost').val() == "") {
            $('#ddlQualification').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlQualification').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                async: false,
                url: pageUrl + '/populateQualification',
                data: '{ExamID:'+B.applicationMaster.ExamID+',PostID:' + $('#ddlPost').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnQualificationPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };
    this.OnQualificationPopulated = function (response) {
        window.applicationPage.PopulateControl(response.d, $("#ddlQualification"));
        $("select#ddlQualification").prop('selectedIndex', 1);
    };

    this.PopulateDesiredQualification = function () {
        //alert('in prop');
        var B = window.applicationPage.applicationForm;
        $("#ddlDesiredQualification").attr("disabled", "disabled");

        if ($('#ddlPost').val() == "") {
            $('#ddlDesiredQualification').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlDesiredQualification').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                async: false,
                url: pageUrl + '/populateDesiredQualification',
                data: '{ExamID:' + B.applicationMaster.ExamID + ',PostID:' + $('#ddlPost').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnDesiredQualificationPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };
    this.PopulateDesiredQualification2 = function () {
        //alert('in prop');
        var B = window.applicationPage.applicationForm;
        $("#ddlDesiredQualification2").attr("disabled", "disabled");

        if ($('#ddlPost').val() == "") {
            $('#ddlDesiredQualification2').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlDesiredQualification2').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                async: false,
                url: pageUrl + '/populateDesiredQualification2',
                data: '{ExamID:' + B.applicationMaster.ExamID + ',PostID:' + $('#ddlPost').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnDesiredQualificationPopulated2,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };
    this.PopulateDesiredQualification3 = function () {
        //alert('in prop');
        var B = window.applicationPage.applicationForm;
        $("#ddlDesiredQualification3").attr("disabled", "disabled");

        if ($('#ddlPost').val() == "") {
            $('#ddlDesiredQualification3').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlDesiredQualification3').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                async: false,
                url: pageUrl + '/populateDesiredQualification3',
                data: '{ExamID:' + B.applicationMaster.ExamID + ',PostID:' + $('#ddlPost').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnDesiredQualificationPopulated3,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };
    this.OnDesiredQualificationPopulated = function (response) {
        window.applicationPage.PopulateControl(response.d, $("#ddlDesiredQualification"));
        $("select#ddlDesiredQualification").prop('selectedIndex',1);
        //document.getElementById('ddlDesiredQualification').selectedIndex = 1;
    };
    this.OnDesiredQualificationPopulated2 = function (response) {
        window.applicationPage.PopulateControl(response.d, $("#ddlDesiredQualification2"));
        $("select#ddlDesiredQualification2").prop('selectedIndex', 1);
        //document.getElementById('ddlDesiredQualification').selectedIndex = 1;
    };
    this.OnDesiredQualificationPopulated3 = function (response) {
        window.applicationPage.PopulateControl(response.d, $("#ddlDesiredQualification3"));
        $("select#ddlDesiredQualification3").prop('selectedIndex', 1);
        //document.getElementById('ddlDesiredQualification').selectedIndex = 1;
    };
    this.PopulatePermPS = function () {
        //alert('in prop');
        $("#ddlPermPS").attr("disabled", "disabled");
        
        if ($('#ddlPermDistrict').val() == "") {
            $('#ddlPermPS').empty().append('<option selected="selected" value="">Please select</option>');            
        }
        else {
            $('#ddlPermPS').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                async: false,
                url: pageUrl + '/populatePS',
                data: '{DistrictID: ' + $('#ddlPermDistrict').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnPermPSPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };
    this.PopulateMailPS = function () {
        //alert('in prop');
        $("#ddlMailPS").attr("disabled", "disabled");

        if ($('#ddlMailDistrict').val() == "") {
            $('#ddlMailPS').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlMailPS').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                async: false,
                url: pageUrl + '/populatePS',
                data: '{DistrictID: ' + $('#ddlMailDistrict').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnMailPSPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };
    this.OnPermPSPopulated = function (response) {
        window.applicationPage.PopulateControl(response.d, $("#ddlPermPS"));
    };
    this.OnMailPSPopulated = function (response) {
        window.applicationPage.PopulateControl(response.d, $("#ddlMailPS"));
    };
    this.PopulateControl = function (list, control) {
        //alert(JSON.stringify(list));
        if (list.length > 0) {
            control.removeAttr("disabled");
            control.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(list, function () {
                control.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
        else {
            control.empty().append('<option selected="selected" value="">Not Applicable<option>');
        }
    };
    this.PopulateControlInt = function (list, control) {
        //alert(JSON.stringify(list));
        if (list.length > 0) {
            control.removeAttr("disabled");
            control.empty().append('<option selected="selected" value="0">Please select</option>');
            $.each(list, function () {
                control.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
        else {
            control.empty().append('<option selected="selected" value="-1">Not available<option>');
        }
    };
};