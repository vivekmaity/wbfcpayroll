

var maxdate;
var maxStartDays;

var maxdt = new Date(-100000000 * 86400000);
var mindt = new Date(100000000 * 86400000);

function compare(key, value) {
    if (key == "BookingDate") {
        var arr = value.split("/");
        var dt = new Date();
        dt.setFullYear(arr[2], (arr[1] - 1), arr[0]);
        //alert("arrdt" + dt.getTime() + " min date" + mindt.getTime());
        //alert("   " + dt.getTime() < mindt.getTime());
        dt.setHours(0, 0, 0, 0);
        if (dt.getTime() < mindt.getTime())
            mindt = dt;
    }
}
function traverse(obj, fun) {
    for (prop in obj) {
        fun.apply(this, [prop, obj[prop]]);
        if (typeof (obj[prop]) == "object") {
            traverse(obj[prop], fun);
        }
    }
}

function CheckForImageFile(fupload) {
    //alert(fupload);
    var file = $(fupload);
    //alert(file.attr("id"));
    var fileName = file.val();
    //alert(fileName);
    //Checking for file browsed or not 
    if ($.trim(fileName) == '') {
        alert("Please select a file to upload!!!");
        file.focus();
        return false;
    }

    //Setting the extension array for diff. type of text files 
    //var extArray = new Array(".jpeg", ".jpg", ".png", ".bmp", ".tiff",".gif");
    var extArray = new Array(".jpg", ".png");

    //getting the file name
    while (fileName.indexOf("\\") != -1)
        fileName = fileName.slice(fileName.indexOf("\\") + 1);

    //Getting the file extension                     
    var ext = fileName.slice(fileName.indexOf(".")).toLowerCase();

    //matching extension with our given extensions.
    for (var i = 0; i < extArray.length; i++) {
        if (extArray[i] == ext) {
            return true;
        }
    }
    alert("Please only upload files that end in types:  "
      + (extArray.join("  ")) + "\nPlease select a new "
      + "file to upload again.");
    file.focus();
    return false;
}
function GetFileSize(fileid, type) {
    try {
        var fileSize = 0;
        //for IE
        if ($.browser.msie) {
            //before making an object of ActiveXObject, 
            //please make sure ActiveX is enabled in your IE browser
            var objFSO = new ActiveXObject("Scripting.FileSystemObject");
            var filePath = $("#" + fileid)[0].value;
            var objFile = objFSO.getFile(filePath);
            fileSize = objFile.size; //size in kb
            //fileSize = fileSize / 1048576; //size in mb 
        }
            //for FF, Safari, Opeara and Others
        else {
            fileSize = $("#" + fileid)[0].files[0].size //size in kb
            //fileSize = fileSize / 1048576; //size in mb 
        }
        //alert("Uploaded File Size is" + fileSize + " kb");
        if (type == 'P' && parseInt(fileSize) > 50000) {
            alert('File Size is not within the specified range.');
            return false;
        } else if (type == 'S' && parseInt(fileSize) > 20000) {
            alert('File Size is not within the specified range.');
            return false;
        }
        //alert('Size OK');
        return true;
    }
    catch (e) {
        alert("Error is :" + e);
    }
}


$(document).ready(function () {
    JSONDiff();
});
function JSONDiff()
{
    //var madrid = 
    //var barca = 

    filterJson();
   //alert(JSON.stringify(data));
}
function filterJson() {
    //var a = { id: 123, name: { first: "Johnny", last: "Johnson" } };
    //var b = { id: 123, name: { first: "John", last: "Johnson" }, age: 30 };
    //var changes = $.objectDiff(a, b); alert(JSON.stringify(changes));
    //var jsmsg = jquery.parseJSON(changes.d); alert(JSON.stringify(jsmsg));
}
(function ($) {
    $.objectDiff = function (a, b, c) {
        c = {};
        $.each([a, b], function (index, obj) {
            for (prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    if (typeof obj[prop] === "object" && obj[prop] !== null) {
                        c[prop] = $.objectDiff(a[prop], b[prop], c);
                    }
                    else {
                        if (a === undefined) a = {};
                        if (b === undefined) b = {};
                        if (a[prop] !== b[prop]) {
                            c[prop] = [a[prop], b[prop]];
                        }
                    }
                }
            }
        });
        return c;
    }
})(jQuery);


//============== Modify Record Unlock when user open other fprm or Logout or browser close ===========/ /
//function OnBeforeUnLoad() {
//        $.ajax({
//            type: "POST",
//            url: 'EmployeeMaster.aspx/Log_Out',
//            cache: false,
//            async: false,
//            contentType: "application/json; charset=utf-8",
//            success: function (D) {
//            }
//        });
//}

//$(document).ready(function () {


//   var CustomMessage = "";
//    var validNavigation = false;

//    function wireUpEvents() {
//        var dont_confirm_leave = 0; //set dont_confirm_leave to 1 when you want the user to be able to leave without confirmation
//        var leave_message = 'Are You sure you want to leave Now ?'
//        function goodbye(e) {
//            if (!validNavigation) {
               
//                if (dont_confirm_leave !== 1) {
                   
//                    if (!e) e = window.event;
//                    //e.cancelBubble is supported by IE - this will kill the bubbling process.
//                    e.cancelBubble = true;
//                    e.returnValue = leave_message;
//                    CustomMessage = leave_message;
//                    //e.stopPropagation works in Firefox.
//                    if (e.stopPropagation) {
//                        e.stopPropagation();
//                        e.preventDefault();
                       
//                    }
//                    //return works for Chrome and Safari
//                    return leave_message;
//                }
               
//            }
          
//        }
//        window.onbeforeunload = function () {
//            goodbye();
//        }

//        window.onunload = function () {
//            OnBeforeUnLoad();
//        }
//        //window.onbeforeunload = goodbye;

//        // Attach the event keypress to exclude the F5 refresh
//        $(document).bind('keypress', function (e) {
//            if (e.keyCode == 116) {
//                validNavigation = true;
//            }
//        });

//        // Attach the event click for all links in the page
//        $("a").bind("click", function () {
//            validNavigation = true;
//        });

//        // Attach the event submit for all forms in the page
//        $("form").bind("submit", function () {
//            validNavigation = true;
//        });

//        // Attach the event click for all inputs in the page
//        $("input[type=submit]").bind("click", function () {
//            validNavigation = true;
//        });

//    }

//    // Wire up the events as soon as the DOM tree is ready
//    $(document).ready(function () {
//            wireUpEvents();

//    });
//});




$(document).ready(function () {
    $(".loading-overlay").show();
    $(".loading-overlay").hide();
    $("#lblYears").hide();
    $("#lblMonths").hide();
    $("#lblDa").hide();
    $("#ddlIR").prop("disabled", true);
    $("#chkIR").prop("disabled", true);
    /*=======================================================================================================================*/

    /*=======================================================================================================================*/
    $(function () {
        $("[id$=txtDNI]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtDOB]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtDOR]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtDOJ]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtDOJDept]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtEffFrm]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtEffTo]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    $(function () {
        $("[id$=txtOrderDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
    });
    /*=======================================================================================================================*/
    
    $("#ddlPayScale").prop("disabled", false);
    $("#ddlDistrict").prop("disabled", false);

    if ($("#hdnDefaultMode").val() == "Insert") {
        $("#txtmHRA").hide(); $("#lblHra").hide();
    } else {
        $("#txtmHRA").show(); $("#lblHra").show();
    }
    /*----------------------------------------------------------*/
    if ($("#hdnDefaultMode").val() == "Insert") {
        $("#ddlHRACat").prop("disabled", true);
        //$("#txtOldbasic").prop("disabled", true);
        $("#txtPCTBasic").prop("disabled", true);
        $("#ddlSpouseQtr").prop("disabled", true);
    } else {
        if ($("#ddlQuatrAlt").val() == "Y") {
            $("#ddlSpouseQtr").prop("disabled", false);
            $("#ddlHRACat").prop("disabled", false);
            $("#ddlHouse").prop("disabled", false);
        }
        else {
            $("#ddlHRACat").prop("disabled", true);
            $("#ddlHouse").prop("disabled", true);
            //$("#txtOldbasic").prop("disabled", true);
            $("#txtPCTBasic").prop("disabled", true);
            $("#ddlSpouseQtr").prop("disabled", false);
        }
    }
    /*----------------------------------------------------------*/
    if ($("#hdnDefaultMode").val() == "Insert") {
        $("#ddlDA").prop("disabled", true);
    } else {
        if ($("#chkDA").is(':checked'))
            $("#ddlDA").prop("disabled", true);
        else
            $("#ddlDA").prop("disabled", false);
    }
    /*----------------------------------------------------------*/
    $(function () {
        $(':text').bind('keydown', function (e) {
            //on keydown for all textboxes
            if (e.target.className != "searchtextbox") {
                if (e.keyCode == 13) { //if this is enter key
                    e.preventDefault();
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        });
    });
    /*----------------------------------------------------------*/
    $(document).ready(function () {
        $("#ddlBank").change(function () {
            if ($("#ddlBank").val() != "" && $("#ddlBank").val() != null) {
                $("#txtIFSC").val('');
                $("#txtMICR").val('');
                //$("#txtBankCode").val('');
                $("#txtBankCode").prop("disabled", true);
                if ($("#ddlBank").val() != "Please select") {
                    var options = {};
                    options.url = "EmployeeMaster.aspx/BindBranch";
                    options.type = "POST";
                    options.data = "{BankID: " + $('#ddlBank').val() + "}";
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listbranch) {
                        var t = jQuery.parseJSON(listbranch.d);
                        var a = t.length;
                        $("#ddlBranch").empty();
                        $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                        if (a > 0) {
                            $.each(t, function (key, value) {
                                $("#ddlBranch").append($("<option></option>").val(value.BranchID).html(value.Branch));
                            });
                        }
                        $("#ddlBranch").prop("disabled", false);
                    };
                    options.error = function () { alert("Error retrieving BankName!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlBranch").empty();
                    $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                    $("#ddlBranch").prop("disabled", true);
                    $("#txtIFSC").val('');
                    $("#txtMICR").val('');
                }
            }
            else {
                $("#ddlBranch").empty();
                $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                $("#ddlBranch").prop("disabled", true);
                $("#txtIFSC").val('');
                $("#txtMICR").val('');
            }
        });
    });
    $(document).ready(function () {
        $("#ddlBranch").change(function () {
            var C = "{BranchID: " + $('#ddlBranch').val() + "}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetIFSCandMICR',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = jQuery.parseJSON(D.d);
                    //alert(JSON.stringify(t));
                    var ifsc = t[0].IFSC;
                    var micr = t[0].MICR;
                    $("#txtIFSC").val(ifsc);
                    $("#txtMICR").val(micr);
                    $("#txtBankCode").prop("disabled", false);
                },
                error: function (response) {
                    alert('');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        });
    });

    $('#ChkDo').click(function () {
        if ($("#ChkDo").is(':checked')) {
            $("#txtAddress_P").val($("#txtAddress").val());
            $("#ddlState_P").val($("#ddlState").val());
            //alert($("#ddlState").val());
            if ($("#ddlState").val() != "" && $("#ddlState").val() != null) {
                if ($("#ddlState_P").val() != "Please select") {
                    var options = {};
                    options.url = "EmployeeMaster.aspx/BindDistrictsforPermanentAddress";
                    options.type = "POST";
                    options.data = "{StateID: " + $('#ddlState_P').val() + "}";
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listdistrict) {
                        var t = jQuery.parseJSON(listdistrict.d);
                        var a = t.length;
                        $("#ddlDistrict_P").empty();
                        if (a > 0) {
                            $("#ddlDistrict_P").append("<option value=''>(Select District)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlDistrict_P").append($("<option></option>").val(value.DistID).html(value.District));
                                $("#ddlDistrict_P").val($("#ddlDistrict").val());
                            });
                        }
                        $("#ddlDistrict_P").prop("disabled", false);
                    };
                    options.error = function () { alert("Error retrieving Districts!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlDistrict_P").empty();
                    $("#ddlDistrict_P").prop("disabled", true);
                }
            }
            $("#txtCity_P").val($("#txtCity").val());
            $("#txtPin_P").val($("#txtPin").val());
            $("#txtPhone_P").val($("#txtPhone").val());

        }
        else {
            $("#txtAddress_P").val('');
            $("#ddlState_P").val('');
            $("#ddlDistrict_P").empty();
            $("#ddlDistrict_P").append("<option value=''>(Select District)</option>")
            $("#txtCity_P").val('');
            $("#txtPin_P").val('');
            $("#txtPhone_P").val('');

        }
    });


    $("#ddlHouse").prop("disabled", true);
    //$("#ddlSpouseQtr").prop("disabled", true);
    $("#chkHRA").attr("disabled", false);
    $("#btnEFDatePicker").prop("disabled", true);
    $("#btnETDatePicker").prop("disabled", true);
    document.getElementById("txtSpouseAmt").disabled = true;
    document.getElementById("txtHealthScheme").disabled = true;
    document.getElementById("txtEffFrm").disabled = true;
    document.getElementById("txtEffTo").disabled = true;
    document.getElementById("txtrentAmount").disabled = true;

    $('#cmdUpdate').hide();
    $('#cmdCancels').hide();
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);

    $('#txtDOB').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        duration: 'slow',
        changeYear: true,
        changeMonth: true,
        maxDate: -6566,
        onSelect: function (selected) {
            calculateDateofRetirement(selected);
            if ($('#txtDOJ').val() != '') {
                Validate_DOB_DOJ_PresentDOJ();
            }
        }
    });

    $("#btnDOBDatePicker").click(function () {
        $('#txtDOB').datepicker("show");
        return false;
    });

    $('#txtDOJ').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (selected) {
            if ($('#txtDOB').val() != '')
            {
             Validate_DOB_DOJ_PresentDOJ();
            }
            if ($('#txtDOJDept').val() != '')
            {
               Validate_DOJ_PresentDOJ();
            }
        }
    });

    $("#btnDOJDatePicker").click(function () {
        $('#txtDOJ').datepicker("show");
        return false;
    });

    //    $('#txtDOR').datepicker({
    //        showOtherMonths: true,
    //        selectOtherMonths: true,
    //        closeText: 'X',
    //        showAnim: 'drop',
    //        showButtonPanel: true,
    //        duration: 'slow'

    //    });

    //    $("#btnDORDatePicker").click(function () {
    //        $('#txtDOR').datepicker("show");
    //        return false;
    //    });

    //    $('#txtDNI').datepicker({
    //        showOtherMonths: true,
    //        selectOtherMonths: true,
    //        closeText: 'X',
    //        showAnim: 'drop',
    //        showButtonPanel: true,
    //        duration: 'slow'

    //    });

    //    $("#btnDNIDatePicker").click(function () {
    //        $('#txtDNI').datepicker("show");
    //        return false;
    //    });

    $('#txtEffFrm').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });

    $("#btnEFDatePicker").click(function () {
        $('#txtEffFrm').datepicker("show");
        return false;
    });

    $('#txtEffTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });

    $('#txtDOJDept').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (selected) {
            if ($('#txtDOJ').val() != '') {
                Validate_DOJ_PresentDOJ();
            }
        }

    });

    $('#txtEffeFrom').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#imgEffeFrom").click(function () {
        $('#txtEffeFrom').datepicker("show");
        return false;
    });

    $('#txtEffeTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#imgEffeTo").click(function () {
        $('#txtEffeTo').datepicker("show");
        return false;
    });


    $("#ddlPayScale").change(function () {
        var PSType = $("#ddlPayScaleType").val();
        var P = $('#ddlPayScale').find('option:selected').text();

        if (P != "(Select Pay Scale)") {
            if (PSType == "O") {
                var a = P.split('-');
                for (var i = 0; i < a.length; i++) {
                    var min = a[0]; var max = a[a.length - 1];
                }
            } else {
                var a = P.split('-');
                for (var i = 0; i < a.length; i++) {
                    var min = a[1]; var V = a[2];
                    var max = V.substring(0, V.indexOf('('));
                    var gp = V.substring(V.indexOf('(') + 1, V.indexOf(')'));
                }
                $("#txtGradePay").val(gp);
            }
        }
    });
    $("#txtEmpCodeSearch").autocomplete({
        source: function (request, response) {
            //alert(pageUrl);
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetEmpCodeWithName',
                data: "{prefix:'" + request.term + "',Sectorid:'" + $("#ddlSector").val() + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                        alert(label);
                    }))
                },
                error: function (response) {

                    alert('Some Error Occur');
                   // $(".loading-overlay").hide();
                },
                failure: function (response) {
                    alert(response.d);
                    //$(".loading-overlay").hide();
                }
            });
        },
        appendTo:"#AutoComplete-EmployeeNo",
        select: function (e, i) {
            $("#txtEmpCodeSearch").val(i.item.val);
            return false;
            //alert($("#hdnSearchEmpID").val(i.item.val));
            
        },
        minLength: 1
    });

    //This is for Bind DNI 
    $(document).ready(function () {
        $("#ddlEmpType").change(function () {
            var EmpType = $("#ddlEmpType").val();
            var today = new Date();
            var yearNow = today.getFullYear();
            var monthNow = today.getMonth();
            var dateNow = today.getDate();
            if (EmpType == "K") {
                $("#txtDNI").val("01/07/" + (parseInt(yearNow) + 1));
            }
            else {
                if (monthNow < 7) {
                    $("#txtDNI").val("01/07/" + yearNow);
                }
                else {
                    $("#txtDNI").val("01/07/" + (parseInt(yearNow) + 1));
                }
            }
        });
    });
    

    $("#txtEmpCodeSearch").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        //if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
        // (event.keyCode == 65 && event.ctrlKey == true) ||
        // Allow: home, end, left, right
        // (event.keyCode >= 35 && event.keyCode <= 39)) {
        //    // let it happen, don't do anything
        //    return;
        //}
        //else {
        //    // Ensure that it is a number and stop the keypress
        //    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
        //        event.preventDefault();
        //        $("#errmsg").html("Digits Only").show().fadeOut(1500);
        //        return false;

        //    }
        //}
    });

    $("#txtLWP").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
    (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                return false;

            }
        }
    });

    $("#cmdCancels").click(function () {
        $("#ddlEarnDeduction").val(0);
        $("#txtAmount").val('');
        $("#cmdAdd").show();
        $("#cmdUpdate").hide();
        $("#cmdCancels").hide();
        $("#ddlEarnDeduction").prop('disabled', false);
    });

    $("#btnETDatePicker").click(function () {
        $('#txtEffTo').datepicker("show");
        return false;
    });

    $("#btnDOJPDDatePicker").click(function () {
        $('#txtDOJDept').datepicker("show");
        return false;
    });

    $("#ddlEarnDeduction").change(function () {
	$("#txtAmount").val(0);
        var E = $("#ddlEarnDeduction").val();

        $("#hdnEDID").val(E);
        var H = $("#ddlHealth").val();
        if (E == 5) {
            alert("You can't select PTax-(D) !");
            $("select#ddlEarnDeduction").val('0');
            $("#txtAmount").val('');
            document.getElementById("txtAmount").disabled = false; return false;
        }

        if (E == 18) {
            alert("You can't select HR Deduction-(D) !");
            $("select#ddlEarnDeduction").val('0');
            $("#txtAmount").val('');
            document.getElementById("txtAmount").disabled = false; return false;
        }

        if (E == 14) {
            //if ($.trim($("#txtPFCode").val()) == "") {
            //    alert("Sorry ! Your PF Code is Blank.");
            //    $("select#ddlEarnDeduction").val('0');
            //    $("#txtAmount").val('');
            //    document.getElementById("txtAmount").disabled = false; return false;
            //}
           
            if ($.trim($("#hdnPFCode").val()) == "55" && $.trim($("#txtPFCode").val()) == "99") {

            }
            else {
                if ($.trim($("#hdnPFCode").val()) == "55") {

                }
                else {
                    alert("You Can't Select and add PF-(D) !");
                    $("select#ddlEarnDeduction").val('0');
                    $("#txtAmount").val('');
                    document.getElementById("txtAmount").disabled = false; return false;
                }
            }
            //var pfcode = $("#txtPFCode").val(); //alert(pfcode);
            //if (pfcode == "55" || pfcode == "" || pfcode == null) {
            //    alert("You can't select and add PF-(D) !");
            //    $("select#ddlEarnDeduction").val('0');
            //    $("#txtAmount").val('');
            //    document.getElementById("txtAmount").disabled = false; return false;
            //}
        }

        if (H == "Y" && E == 7) {
            alert("You can't select Medical Allowance because You are in Health Scheme !");
            $("select#ddlEarnDeduction").val('0');
            $("#txtAmount").val(''); return false;
        } else {
            window.empMaster.getPayVal();
        }

        var PanNo = $("#txtPAN").val();
        if (PanNo == "" && E == 1) {
            alert("Sorry ! You have not PAN NO. So you can not insert iTax-(D)."); $("#ddlEarnDeduction").val('');
            return false;
        }
        if (PanNo != "" && E == 1) 
        {
            //alert(PanNo);
            var isValid = false;
            var regex = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/;
            isValid = regex.test($("[id*=txtPAN]").val());
            if (isValid) {
                $("#hdnItax").val('Yes');
            }
            else {
                alert('You have Entered invalid PAN NO.'); $("#ddlEarnDeduction").val('');
                return true;
            }
        }

        var CoOpertiveMem = $("#txtCoMembership").val();
        if (CoOpertiveMem == "" && E == 22) {
            alert("Sorry ! You have not Co-Operative Membership No. So you can not insert CO-OP SUBSCRIPTION-(D)."); $("#ddlEarnDeduction").val('');
            return false;
        }
        //******************Is Round ****************************************
        //var RevEarnDedu = $("#ddlEarnDeduction option:selected").text().split("").reverse().join("").charAt(1);
        //$("#hdnIsRound").val(RevEarnDedu);

        var C = { EdId: E };
        $.ajax({
            type: "POST",
            url: "EmployeeMaster.aspx/CheckIsRound",
            data: JSON.stringify(C),
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var IsRoundId = JSON.parse(D.d);
                $("#hdnIsRound").val(IsRoundId);
            },
            error: function (response) {
                // alert('h');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
        //*********************************************************************

    });
    $("#txtAmount").keypress(function () {
        var IsRoundId = $("#hdnIsRound").val();
        if (IsRoundId == 1 || IsRoundId == "" || IsRoundId == null || IsRoundId == "No") {
            if (event.which == 58 ||( event.which <= 47 || event.which >= 59)) {
                event.preventDefault();
            }
        }
        if (IsRoundId == 0) {
            if (event.which != 46 && (event.which == 58 ||(event.which <= 47 || event.which >= 59))) {
                event.preventDefault();
                if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
                    event.preventDefault();
                }
            }
        }
    });
     $("#txtAmount").keyup(function () {
        var number = ($(this).val().split('.'));
        if (number[1].length > 2) {
            var salary = parseFloat($("#txtAmount").val());
            $("#txtAmount").val(salary.toFixed(2));
        }
    });
    $("#cmdSearch").click(function (event) {
        var serval = $('#txtEmpCodeSearch').val();
        var sector = $("#ddlSector").val();

        if (serval != "") {
            if (sector != 0) {
                $("#form1").validate().currentForm = '';
                return true;
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "EmployeeMaster.aspx/Admin_Search",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var t = jQuery.parseJSON(data.d); 
                        if (t != "2")
                        {
                            alert("Please Select the Sector to Search !");
                            $("#ddlSector").focus();
                            return false;
                        }
                    }
                });
            }
        }
        else {
            alert("Please Enter Employee No.!");
            $('#txtEmpCodeSearch').focus();
            return false;
        }

        if (serval != "") {

        }



    });

    $('#chkHRA').click(function () {
        if ($("#chkHRA").is(':checked')) {
            $("#txtmHRA").show();
            $("#lblHra").show();
            $("#ddlQuatrAlt").prop("disabled", true);
            $("#ddlQuatrAlt").val('');
            $("#ddlHRA").prop("disabled", true); $("#ddlHRA").val('');
            $("#ddlSpouseQtr").prop("disabled", true); $("#ddlSpouseQtr").val('');
            document.getElementById("txtOldbasic").disabled = true;
            document.getElementById("txtPCTBasic").disabled = true;
            document.getElementById("txtLic").disabled = true;
        }
        else {
            $("#txtmHRA").hide();
            $("#lblHra").hide();
            $("#ddlQuatrAlt").prop("disabled", false);
            $("#txtOldbasic").prop("disabled", false);

            $("#txtPCTBasic").prop("disabled", false);
            $("#txtLic").prop("disabled", false);

            //document.getElementById("txtOldbasic").disabled = false;
            //document.getElementById("txtPCTBasic").disabled = false;
            //document.getElementById("txtLic").disabled = false;
        }
    });

    $('#chkDA').click(function () {
        if ($("#chkDA").is(':checked')) {
            $("#ddlDA").prop("disabled", true);
            $("#ddlDA").val('');
        }
        else {
            $("#ddlDA").prop("disabled", false);
        }
    });

    $('#chkIR').click(function () {
        if ($("#chkIR").is(':checked')) {
            $("#ddlIR").prop("disabled", true);
            $("#ddlIR").val('0');
        }
        else {
            $("#ddlIR").prop("disabled", false);
        }
    });

    $('#chkHealthScheme').click(function () {
        if ($("#chkHealthScheme").is(':checked')) {
            $("#ddlHealth").prop("disabled", true);
            $("#ddlHealth").val('');
        }
        else {
            $("#ddlHealth").prop("disabled", false);
        }
    });

    $('#chkGradePay').click(function () {
        if ($("#chkGradePay").is(':checked')) {
            $("#ddlDA").prop("disabled", true);
            $("#ddlDA").val('');
        }
        else {
            $("#ddlDA").prop("disabled", false);
        }
    });

    $('#btnDo').click(function () {
        $("#txtDOJDept").val($("#txtDOJ").val());
    });

    $("#cmdAdd").click(function (event) {
        if ($("#form1").validate().currentForm != '')
            return false;
        else
            return true;
    });

    $("#cmdUpdate").click(function (event) {
        if ($("#form1").validate().currentForm != '')
            return false;
        else
            return true;
    });

    $("#cmdCancels").click(function (event) {
        if ($("#form1").validate().currentForm != '')
            return false;
        else
            return true;
    });

    $("#ddlStatus").change(function () {
        var SearchStatus = $("#hdnStatus").val(); //alert(SearchStatus);
        var status = $("#ddlStatus").val(); //alert(status);
        var B = window.empMaster.employeeForm;
        var EmpPayDetails = B.empAddPayDetails;

        if (SearchStatus == "S" && status == "Y" || SearchStatus == "W" && status == "Y")
        {
            var StatusText = $('#ddlStatus').find('option:selected').text();
            $("#txtStatus").val(StatusText);
            $("#overlay").show();
        }

        if (status == "W")
        {
            var StatusText = $('#ddlStatus').find('option:selected').text();
            $("#txtStatus").val(StatusText);
            $("#overlay").show();
        }

        if (status == "S")
        {
            var E = "{Status: '" + status + "'}";
                //alert(E);
                var options = {};
                options.url = "EmployeeMaster.aspx/BindEarnDedbyStatus";
                options.type = "POST";
                options.data = E;
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listEarnDed) {
                    var t = jQuery.parseJSON(listEarnDed.d);
                    var a = t.length;
                    $("#ddlEarnDeduction").empty();
                    if (a >= 0) {
                        $("#ddlEarnDeduction").append("<option value='0' selected='true'>(Select Earn/Deduction)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlEarnDeduction").append($("<option></option>").val(value.EDID).html(value.EarnDeduction));
                        });
                    }
                };
                options.error = function () { alert("Error in retrieving Earn Deduction !"); };
                $.ajax(options);
         
                var StatusText = $('#ddlStatus').find('option:selected').text();
                $("#txtStatus").val(StatusText);
                $("#overlay").show();
            window.empMaster.showPayDetailonSuspended(EmpPayDetails);
        }
        else
        {
            window.empMaster.showPayDetail(EmpPayDetails);
        }
    });



    $("#Close").click(function (e) {
        $("#overlay").hide();
        var SearchStatus = $("#hdnStatus").val();
        $("#ddlStatus").val(SearchStatus);
    });
  

    $('#txtEmployerPfRate,#txtEmployeePfRate').on('keypress', function (event) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
    $('#txtEmployerPfRate,#txtEmployeePfRate').bind('copy paste cut', function (e) {
        e.preventDefault(); //disable cut,copy,paste
        alertify.alert('Message', 'cut,copy & paste options are disabled !!');
    });



    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    window.empMaster = new KMDA.EmployeeMaster();
    window.empMaster.init();

});
function lpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = strToPad + originalstr;
    return originalstr;
}
function rpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = originalstr + strToPad;
    return originalstr;
}
function dateDiff() {
    var frmdate = null, toDate = null, totalDays = "";
    if ($.trim($("#txtFromDate").val())) {
        frmdate = new Date($("#txtFromDate").datepicker('getDate'));
    }
    if ($.trim($("#txtToDate").val())) {
        toDate = new Date($("#txtToDate").datepicker('getDate'));
    }

    if ((frmdate != null && toDate != null))
        totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;
    //alert(frmdate+'   '+ toDate+'  '+ totalDays);
    return totalDays;
}
function check(ch) {
    if (ch.id == "txtFromDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtToDate").datepicker("option", "minDate", '-100Y');
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtToDate").datepicker("option", "minDate", testm);
        }
    } else if (ch.id == "txtToDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtFromDate").datepicker("option", "maxDate", maxStartDays);
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtFromDate").datepicker("option", "maxDate", maxStartDays);
        }
    }
}
function chkEmployeeNo() {
    var EmpNo = $("#txtEmpNo").val();
    var C = { EmpNo: EmpNo };
    var Status = true;
    $.ajax({
        type: "POST",
        url: "EmployeeMaster.aspx/CheckEmpNoAvailable",
        data:JSON.stringify( C),
        contentType: "application/json; charset=utf-8",
        async:false,
        success: function (D) {
            var data = JSON.parse(D.d);
            // alert(JSON.stringify(data));
            var Count = data[0].Available;
            if (Count == 1) {
                alert("This Employee No. is already present. Please provide a different Employee No.");
                Status= false;
            }
            else { }
        },
        error: function (response) {
            // alert('h');
        },
        failure: function (response) {
            alert(response.d);
        }
    });
    return Status;
}
KMDA.MasterInfo = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#cmdSave").click(A.AddClicked);
        $("#cmdSave1").click(A.AddClicked);
        $("#btnUpdate").click(A.Update);
        $("#btnUpdate1").click(A.Update);
        $("#cmdAdd").click(A.AddPayDetail);
        $("#cmdUpdate").click(A.UpdatePayd);
        $("#btnTESearch").click(A.TransferEmp);
        $("#buttonUpload").click(A.UploadFilePhoto);
        $("#buttonUploadSign").click(A.UploadFileSign);
        $("#ddlHouse").change(function () {
            var E = $("#ddlEarnDeduction").val();
            A.HouseRent();
        });
        $("#btnStatusSave").click(A.SaveStatusPopupDetail);

    };
    this.getValidator = function () {

        return $("#form1").validate({
            rules: {
                txtEmpNo: {
                    required: function (element) {
                        if ($("#txtEmpNo").val() == "") {

                        }
                    }
                },
            },
            messages: {
                txtEmpNo: { required: "Please Enter Employee No." },
            }
        });


        //$("#form1").validate({ ignore: "" });
        ////$("#txtEmpNo").rules("add",{ required: true, messages: { required: "Please Enter Employee No." } });
        
        //$("#txtEmpNo").rules("add",
        //    {
        //        required: function (element) {
        //            $('#txtEmpNo').addClass("NormalBorderColor");
        //            if ($("#txtEmpNo").val() == "")
        //            {
        //                alert("BABAN");
        //                $("#tabs").tabs({
        //                    collapsible: true,
        //                    selected: 0
        //                });
        //                $('#txtEmpNo').addClass("ErrorBorderColor");
        //                return true;
        //            }
        //        }, messages: { required: "Please Enter Employee No." }
        //    });

        //$("#ddlEmpType").rules("add", { required: true, messages: { required: "Please Select Employee Type"} });
        //$("#ddlBank").rules("add", { required: true, messages: { required: "Please Select Bank" } });

        //return $("#form1").validate({ ignore: "" });
    }
    
    this.AddClicked = function () {
        if (A.validator.form()) {

            var gridLen = $("#detailTable tr").length;
            var Status = chkEmployeeNo();
            if (Status != true) {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtEmpNo").focus(); return true;
            }
	        else if ($("#txtEmpNo").val() == "")
            {
                alert("Please Provide Emp NO !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtEmpNo").focus(); return true;
            }
            else if ($("#ddlSector").val() == 0) {
                alert("Please Select Sector !"); return true;
            }
            else if ($("#ddlCenter").val() == "0") {
                alert("Please Select Branch !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#ddlCenter").focus(); return true;
            }
            else if ($("#ddlEmpType").val() == "")
            {
                alert("Please Select Employee Type !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#ddlEmpType").focus(); return true;
            }
            else if ($("#txtEmpName").val() == "") {
                alert("Please Enter Employee Name !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtEmpName").focus(); return true;
            }
            else if ($("#txtDOB").val() == "") {
                alert("Please Enter Date of Birth !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtDOB").focus(); return true;
            }
            else if ($("#ddlSex").val() == "") {
                alert("Please Select Gender !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#ddlSex").focus(); return true;
            }
            else if ($("#txtMobile").val() == "") {
                alert("Please Enter Mobile No. !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 1
                });
                $("#txtMobile").focus(); return true;
            }
            else if ($("#txtDOJ").val() == "") {
                alert("Please Enter Date of Joining !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#txtDOJ").focus(); return true;
            }
            else if ($("#ddlPhysical").val() == "") {
                alert("Please Select Physical Handicapped !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlPhysical").focus(); return true;
            }
            else if ($("#ddlDesig").val() == "") {
                alert("Please Select Designation!");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlDesig").focus(); return true;
            }
            else if ($("#ddlGroup").val() == "") {
                alert("Please Select Group!");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlGroup").focus(); return true;
            }

            else if ($("#chkDA").is(':unchecked') && $("#ddlDA").val() == "") {
                alert("Please Select DA Rate !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlDA").focus(); return true;
            }


            //else if ($("#txtPFCode").val() == "") {
            //    alert("Please Enter PF Code !");
            //    $("#tabs").tabs({
            //        collapsible: true,
            //        selected: 2
            //    });
            //    $("#txtPFCode").focus(); return true;
                //}
            else if($("txtEmployerPfRate").val()=="")
            {
                alert("Please enter Employee PF Rate");
                $("#tabs").tabs({
                    collapsible: true,
                    selected:2
                });
            }
            else if ($("txtEmployeePfRate").val() == "S") {
                alert("Please enter Employer PF Rate");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
            }
            else if ($("txtFatherName").val() == "") {
                alert("Please enter Fathers Name");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
            }
            else if ($("txtBranchInCharge").val() == "S") {
                alert("Please select Branch In Charge");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
            }
            else if ($("#ddlDepartment").val() == "S") {
                alert("Please select department");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
            }
            //else if ($("ddlPFRate").val() == "S") {
            //    alert("Select PF Rate");
            //    $("#tabs").tabs({
            //        collapsible: true,
            //        selected: 2
            //    });
            //}
            //else if ($("txtAddressRemarks").val() == "") {
            //    alert("Please enter Remarks In Address Tab");
            //    $("#tabs").tabs({
            //        collapsible: true,
            //        selected: 2
            //    });
            //}
            else if ($("#ddlPayScaleType").val() == "") {
                alert("Please Select PayScale Type !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlPayScaleType").focus(); return true;
            }
            else if ($("#ddlPayScale").val() == "0") {
                alert("Please Select PayScale !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlPayScale").focus(); return true;
            }
            else if ($("#ddlQuatrAlt").val() == "N" && $("#ddlHRA").val() == "0") {
                alert("Please Select HRA !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlHRA").focus(); return true;
            }
                //else if ($("#ddlHealth").val() == "") {
                //    if ($("#chkHealthScheme").is(':checked')) {
                    
                //    }
                //    else {
                //        alert("Please Select Health Scheme !");
                //        $("#tabs").tabs({
                //            collapsible: true,
                //            selected: 2
                //        });
                //        $("#ddlHealth").focus(); return true;
                //    }
                //}
            else if ($("#ddlHealth").val() == "Y" && $("#txtHealthScheme").val() == "" && $("#txtEffFrm").val() == "" && $("#txtEffTo").val() == "") {
                alert("Please Enter Complete Value for Health Scheme ! Because You have Selected Health Scheme- Yes.");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#ddlHealth").focus(); return true;
            }
            else if ($("#ddlBank").val() == "" || $("#ddlBank").val() == "0") {
                alert("Please Select Bank Name !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3
                });
                $("#ddlBank").focus(); return true;
            }
            else if ($("#ddlBranch").val() == "" || $("#ddlBranch").val() == "0") {
                alert("Please Select Branch Name !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3
                });
                $("#ddlBranch").focus(); return true;
            }
            else if ($("#txtIFSC").val() == "") {
                alert("Please Enter IFSC !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3
                });
                $("#txtIFSC").focus(); return true;
            }
            else if ($("#txtBankCode").val() == "") {
                alert("Please Enter Bank Account No. !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3
                });
                $("#txtBankCode").focus(); return true;
            }
            else if ($("#txtFatherName").val() == "") {
                alert("Please enter Father's Name");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtFatherName").focus(); return false;
            }
            else if ($("#txtBranchInCharge").val() == "S") {
                alert("Please Select Branch In Charge");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtBranchInCharge").focus(); return false;
            }
            //else if ($("ddlPFRate").val() == "S") {
            //    alert("Select PF Rate");
            //    $("#tabs").tabs({
            //        collapsible: true,
            //        selected: 2
            //    });
            //    $("#ddlPFRate").focus(); return false;
            //}
            else if (gridLen == "0") {
                alert("Please Enter Other Details. !"); return true;
            }
            else{
                var BankAccNo = $("#txtBankCode").val().trim();
                var F = "{BankAccNo: '" + BankAccNo.trim() + "'}"; 
                $.ajax({
                    type: "POST",
                    url: 'EmployeeMaster.aspx/Check_Exist',
                    data: F,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var data = JSON.parse(D.d);
                        if (data == "Yes") {
                            alert("Sorry ! Bank Account No is Already Available."); return false;
                        }
                        else {
                            A.setFormValues();
                        }
                    }
                });
            }
        }
    };
    this.Update = function () {
        var gridLen = $("#detailTable tr").length;

        var S = $("#ddlSector").val();
        if (S == 0) {
            alert("Please Select Sector !"); return true;
        }
	else if ($("#txtEmpNo").val() == "")
            {
                alert("Please Provide Emp NO !");
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtEmpNo").focus(); return true;
            }
        else if ($("#ddlCenter").val() == "0") {
            alert("Please Select Branch !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#ddlCenter").focus(); return true;
        }
        else if ($("#ddlEmpType").val() == "") {
            alert("Please Select Employee Type !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#ddlEmpType").focus(); return true;
        }
        else if ($("#txtEmpName").val() == "") {
            alert("Please Enter Employee Name !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#txtEmpName").focus(); return true;
        }
        else if ($("#txtDOB").val() == "") {
            alert("Please Enter Date of Birth !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#txtDOB").focus(); return true;
        }
        else if ($("#ddlSex").val() == "") {
            alert("Please Select Gender !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#ddlSex").focus(); return true;
        }
        else if ($("#txtMobile").val() == "") {
            alert("Please Enter Mobile No. !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 1
            });
            $("#txtMobile").focus(); return true;
        }
        else if ($("#txtDOJ").val() == "") {
            alert("Please Enter Date of Joining !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#txtDOJ").focus(); return true;
        }
        else if ($("#ddlPhysical").val() == "") {
            alert("Please Select Physical Handicapped !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlPhysical").focus(); return true;
        }
        else if ($("#ddlDesig").val() == "") {
            alert("Please Select Designation!");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlDesig").focus(); return true;
        }
        else if ($("#ddlGroup").val() == "") {
            alert("Please Select Group!");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlGroup").focus(); return true;
        }

        else if ($("#chkDA").is(':unchecked') && $("#ddlDA").val() == "") {
            alert("Please Select DA Rate !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlDA").focus(); return true;
        }


        //else if ($("#txtPFCode").val() == "") {
        //    alert("Please Enter PF Code !");
        //    $("#tabs").tabs({
        //        collapsible: true,
        //        selected: 2
        //    });
        //    $("#txtPFCode").focus(); return true;
        //}
        else if ($("#ddlPayScaleType").val() == "") {
            alert("Please Select PayScale Type !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlPayScaleType").focus(); return true;
        }
        else if ($("#ddlPayScale").val() == "0") {
            alert("Please Select PayScale !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlPayScale").focus(); return true;
        }
        else if ($("#ddlQuatrAlt").val() == "N" && $("#ddlHRA").val() == "0") {
            alert("Please Select HRA !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlHRA").focus(); return true;
        }
        //else if ($("#ddlHealth").val() == "" && $("#chkHealthScheme").is(':checked')) {
        //    //if ($("#chkHealthScheme").is(':checked')) {
        //    //    return true;
        //    //}
        //    //else {
        //    //    alert("Please Select Health Scheme !");
        //    //    $("#tabs").tabs({
        //    //        collapsible: true,
        //    //        selected: 2
        //    //    });
        //    //    $("#ddlHealth").focus(); return true;
        //    //}
        //    return true;
        //}
        else if ($("#ddlHealth").val() == "Y" && $("#txtHealthScheme").val() == "" && $("#txtEffFrm").val() == "" && $("#txtEffTo").val() == "") {
            alert("Please Enter Complete Value for Health Scheme ! Because You have Selected Health Scheme- Yes.");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlHealth").focus(); return true;
        }
        else if ($("#ddlBank").val() == "" || $("#ddlBank").val() == "0") {
            alert("Please Select Bank Name !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 3
            });
            $("#ddlBank").focus(); return true;
        }
        else if ($("#ddlBranch").val() == "" || $("#ddlBranch").val() == "0") {
            alert("Please Select Branch Name !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 3
            });
            $("#ddlBranch").focus(); return true;
        }
        else if ($("#txtIFSC").val() == "") {
            alert("Please Enter IFSC !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 3
            });
            $("#txtIFSC").focus(); return true;
        }
        else if ($("#txtBankCode").val() == "") {
            alert("Please Enter Bank Account No. !");
            $("#tabs").tabs({
                collapsible: true,
                selected: 3
            });
            $("#txtBankCode").focus(); return true;
        }
        else if ($("#txtFatherName").val() == "") {
            alert("Please enter Father's Name");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#txtFatherName").focus(); return false;
        }
        else if ($("#txtBranchInCharge").val() == "S") {
            alert("Please Select Branch In Charge");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#txtBranchInCharge").focus(); return false;
        }
        else if ($("#ddlDepartment").val() == "S") {
            alert("Please select department");
            $("#tabs").tabs({
                collapsible: true,
                selected: 2
            });
            $("#ddlDepartment").focus(); return false;
        }
        //else if ($("ddlPFRate").val() == "S") {
        //    alert("Select PF Rate");
        //    $("#tabs").tabs({
        //        collapsible: true,
        //        selected: 2
        //    });
        //    $("#ddlPFRate").focus(); return false;
        //}
        else if (gridLen == "0") {
            alert("Please Enter Other Details. !"); return true;
        }
        else {
            A.setFormValuesforUpdate();
        }
    };
    this.AddPayDetail = function () {
    
    if ($("#ddlPhysical").val() == "") {
        alert("Please Select Physical Handicapped !");
        $("#tabs").tabs({
            collapsible: true,
            selected: 2
        });
        $("#ddlPhysical").focus(); return false;
    }
    else if ($("#txtEmployerPfRate").val() == "") {
        alert("Please enter Employee PF Rate");
        $("#tabs").tabs({
            collapsible: true,
            selected: 2
        });
        $("#txtEmployerPfRate").focus(); return false;
    }
    else if ($("#txtEmployeePfRate").val() == "S") {
        alert("Please enter Employer PF Rate");
        $("#tabs").tabs({
            collapsible: true,
            selected: 2
        });
        $("#txtEmployeePfRate").focus(); return false;
    }
   
    //else if ($("#txtAddressRemarks").val() == "") {
    //    alert("Please enter Remarks In Address Tab");
    //    $("#tabs").tabs({
    //        collapsible: true,
    //        selected: 2
    //    });
    //    $("#txtAddressRemarks").focus(); return false;
    //}
    else { }

        var PSType = $("#ddlPayScaleType").val();
        var E = $("#ddlEarnDeduction").val();
        var F = $('#ddlEmpType').find('option:selected').text();
        var E = $('#ddlEarnDeduction').find('option:selected').text();

        var EarnDedType = E.substring(E.length - 1, E.length - 2);

        if (EarnDedType == "E" && $("#ddlEarnDeduction").val() == 2) {

            var P = $('#ddlPayScale').find('option:selected').text();
            var E = $("#ddlEarnDeduction").val();
            if (E == "2") {
                if (P != "(Select Pay Scale)" && F != "(Select Employee Type)") {
                    //alert(P); alert(PSType);
                    if (PSType == "O") {
                        var a = P.split('-'); //alert(a.length);
                        for (var i = 0; i < a.length; i++) {
                            var min = a[0]; var max = a[a.length - 1];
                        } //alert(min); alert(max);
                    }
                    else if (PSType == "K") {
                        var a = P.split('-');
                        for (var i = 0; i < a.length; i++) {
                            var min = a[1]; var V = a[2];
                            var max = V.substring(0, V.indexOf('('));
                        }
                    }
                    else if (PSType == "S") {
                        var a = P.split('-'); //alert(a); 
                        for (var i = 0; i < a.length; i++) {
                            var min = a[1]; var V = a[2];
                            var max = V.substring(0, V.indexOf('('));
                        }
                    }
                    else {
                        var a = P.split('-'); //alert(a); 
                        for (var i = 0; i < a.length; i++) {
                            var min = a[0]; var V = a[1];
                            var max = V.substring(0, V.indexOf('('));
                        }
                    }
                    // alert(min); alert(V); alert(max);

                    var amount = parseInt($('#txtAmount').val());
                    //alert(amount);
                    if (amount >= min && amount <= max) {
                        A.setFormValuesPayDetails();
                    }
                    else {
                        alert("Enter Amount is not within Selected -- Pay-Band -- !");
                        $("#txtAmount").focus();
                    }
                }
                else {
                    alert("Please Select -- Employee Type --  And -- Pay-Scale  -- !");
                }
            }
            else
            {
                A.setFormValuesPayDetails();
            }
        }
        else
        {
            if ($('#lblTotalEarning').text() > 0)
            {
                A.setFormValuesPayDetails();
            }
            else
            {
                alert("Please First Select PayBand-(E) for Total Earning !");
                $("#ddlEarnDeduction").focus();
            }

        }


    };
    this.UpdatePayd = function () {

        var status = $("#ddlStatus").val();
        
        if (status != "S") {
            var PSType = $("#ddlPayScaleType").val();
            var E = $("#ddlEarnDeduction").val();
            var F = $('#ddlEmpType').find('option:selected').text();
            var E = $('#ddlEarnDeduction').find('option:selected').text();

            var EarnDedType = E.substring(E.length - 1, E.length - 2);

            //if (EarnDedType == "E" && $("#ddlEarnDeduction").val() == 2) {

            var P = $('#ddlPayScale').find('option:selected').text();
            var E = $("#ddlEarnDeduction").val();

            if (E == "2") {
                if (P != "(Select Pay Scale)" && F != "(Select Employee Type)") {
                    //alert(P);
                    if (PSType == "O") {
                        var a = P.split('-');
                        for (var i = 0; i < a.length; i++) {
                            var min = a[0]; var max = a[a.length - 1];
                        }
                    }
                    else if (PSType == "K") {
                        var a = P.split('-');
                        for (var i = 0; i < a.length; i++) {
                            var min = a[1]; var V = a[2];
                            var max = V.substring(0, V.indexOf('('));
                        }
                    }
                    else if (PSType == "S") {
                        var a = P.split('-'); //alert(a); 
                        for (var i = 0; i < a.length; i++) {
                            var min = a[1]; var V = a[2];
                            var max = V.substring(0, V.indexOf('('));
                        }
                    }
                    else {
                        var a = P.split('-'); //alert(a); 
                        for (var i = 0; i < a.length; i++) {
                            var min = a[0]; var V = a[1];
                            var max = V.substring(0, V.indexOf('('));
                        }
                    }
                    //alert(min); alert(max);

                    var amount = parseInt($('#txtAmount').val());
                    //alert(amount);
                    if (amount >= min && amount <= max) {
                        window.empMaster.UpdateFinalPayDetails();
                        $("#btnUpdate").prop("disabled", false);
                        $("#btnUpdate1").prop("disabled", false);
                    }
                    else {
                        alert("Enter Amount is not within Selected -- Pay-Band -- !");
                    }
                }
                else {
                    alert("Please Select -- Pay-Scale --  And -- Employee Type -- !");
                }
            }
            else {
                window.empMaster.UpdateFinalPayDetails();
                $("#btnUpdate").prop("disabled", false);
                $("#btnUpdate1").prop("disabled", false);
            }
        }
        else {
            window.empMaster.UpdateSuspendedPayDetails();
            $("#btnUpdate").prop("disabled", false);
            $("#btnUpdate1").prop("disabled", false);
        }
    };
    this.SearchEmployee = function () {
        window.empMaster.SearchEmployee();
    };
    this.RoomAddClicked = function () {
        window.blockPage.SubmitForm();
    };
    this.SubmitClicked = function () {
        window.blockPage.addAfterValidate();
    };
    this.SubmitMaster = function () {
        window.blockPage.SubmitForm();
    };
    this.setFormValues = function () {
        var B = window.empMaster.employeeForm;
        //Personal Information
        // B.employeeMaster.EmployeeCode = $("#EmpCode").val();

        B.employeeMaster.EmployeeNo = $("#txtEmpNo").val();
        B.employeeMaster.EmployeeType = $("#ddlEmpType").val();

        var Classifi = $("#ddlClassifctn").val();
        if (Classifi == "") { B.employeeMaster.EmpClassification = ""; } else { B.employeeMaster.EmpClassification = Classifi; }

        B.employeeMaster.EmployeeName = $("#txtEmpName").val();

        var Religion = $("#ddlReligion").val();
        if (Religion == "") { B.employeeMaster.EmployeeReligion = ""; } else { B.employeeMaster.EmployeeReligion = Religion; }

        B.employeeMaster.EmployeeQualification = $("#ddlQualftn").val();
        //if (Qualifi == "") { B.employeeMaster.EmployeeQualification == ""; } else { B.employeeMaster.EmployeeQualification = Qualifi;}

        B.employeeMaster.EmployeeMaritalStatus = $("#ddlMarital").val();
        //if (Marital == "") { B.employeeMaster.EmployeeMaritalStatus == "" } else { B.employeeMaster.EmployeeMaritalStatus = Marital; }

        var Caste = $("#ddlCaste").val();
        if (Caste == "") { B.employeeMaster.EmployeeCaste = "" } else { B.employeeMaster.EmployeeCaste = Caste; }

        B.employeeMaster.EmployeeDOB = $("#txtDOB").val();

        B.employeeMaster.EmployeeGender = $("#ddlSex").val();
        //if (Sex == "") { B.employeeMaster.EmployeeGender == "" } else { B.employeeMaster.EmployeeGender = Sex; }

        //Address Information
        B.employeeMaster.EmployeePresentAddress = $("#txtAddress").val();

        var State = $("#ddlState").val();
        if (State == "") { B.employeeMaster.EmployeePresentState = ""; } else { B.employeeMaster.EmployeePresentState = State; }
        var Dist = $("#ddlDistrict").val();
        if (State == "") { B.employeeMaster.EmployeePresentDistrict = ""; } else { B.employeeMaster.EmployeePresentDistrict = Dist; }

        B.employeeMaster.EmployeePresentCity = $("#txtCity").val();
        var pin = $("#txtPin").val();
        B.employeeMaster.EmployeePresentPin = pin;
        B.employeeMaster.EmployeePresentPhone = $("#txtPhone").val();
        B.employeeMaster.EmployeePresentMobile = $("#txtMobile").val();
        B.employeeMaster.EmployeePresentEmail = $("#txtEmail").val();

        B.employeeMaster.EmployeePermanentAddress = $("#txtAddress_P").val();
        var State_P = $("#ddlState_P").val();
        if (State_P == "") { B.employeeMaster.EmployeePermanentState = ""; } else { B.employeeMaster.EmployeePermanentState = State_P; }
        var Dist_P = $("#ddlDistrict_P").val();
        if (State_P == "") { B.employeeMaster.EmployeePermanentDistrict = ""; } else { B.employeeMaster.EmployeePermanentDistrict = Dist_P; }

        B.employeeMaster.EmployeePermanentCity = $("#txtCity_P").val();
        B.employeeMaster.EmployeePermanentPin = $("#txtPin_P").val();
        B.employeeMaster.EmployeePermanentPhone = $("#txtPhone_P").val();
        ////Official Details
        B.employeeMaster.EmployeeDOJ = $("#txtDOJ").val();
        B.employeeMaster.EmployeeDOR = $("#txtDOR").val();
        B.employeeMaster.EmployeeDesignation = $("#ddlDesig").val();
        B.employeeMaster.EmployeeCooperative = $("#ddlCoop").val();


        var DA = $("#ddlDA").val();
        if ($("#chkDA").is(':checked')) {
            B.employeeMaster.EmployeeDA = 0;
        }
        else {
            B.employeeMaster.EmployeeDA = DA;
        }

        B.employeeMaster.PFCode = $("#txtPFCode").val();
        B.employeeMaster.DNI = $("#txtDNI").val();
        //alert($("#txtDNI").val());
        var HRA = $("#ddlHRA").val();
        if ($("#chkHRA").is(':checked')) {       //fIXED HRA
            B.employeeMaster.HRAFixedAmount = $("#txtmHRA").val();
            B.employeeMaster.HRAFlag = "Y";
            B.employeeMaster.QuarterAllot = "N";
            B.employeeMaster.HRA = 0;
            B.employeeMaster.SpouseQuarter = "N";
            B.employeeMaster.SpouseAmount = "";
            B.employeeMaster.HRACategory = "F";
            B.employeeMaster.House = "";
        }
        else {
            if ($("#ddlQuatrAlt").val() == "Y") {   //No HRA
                B.employeeMaster.HRAFixedAmount = "";
                B.employeeMaster.HRAFlag = "N";
                B.employeeMaster.QuarterAllot = "Y";
                B.employeeMaster.HRA = 0;
                B.employeeMaster.SpouseQuarter = "N";
                B.employeeMaster.SpouseAmount = "";
                B.employeeMaster.HRACategory = $("#ddlHRACat").val();
                B.employeeMaster.House = $("#ddlHouse").val();
            }
            else if ($("#ddlQuatrAlt").val() == "N" && $("#ddlSpouseQtr").val() == "Y") {  //Spouse HRA
                B.employeeMaster.HRAFixedAmount = "";
                B.employeeMaster.HRAFlag = "N";
                B.employeeMaster.QuarterAllot = "N";
                B.employeeMaster.HRA = $("#ddlHRA").val();
                B.employeeMaster.SpouseQuarter = "Y";
                B.employeeMaster.SpouseAmount = $("#txtSpouseAmt").val();
                B.employeeMaster.HRACategory = "N";
                B.employeeMaster.House = "";
            }
            else {
                //Normal HRA
                B.employeeMaster.HRAFixedAmount = "";
                B.employeeMaster.HRAFlag = "N";
                B.employeeMaster.QuarterAllot = "N";
                B.employeeMaster.HRA = $("#ddlHRA").val();
                B.employeeMaster.SpouseQuarter = "N";
                B.employeeMaster.SpouseAmount = "";
                B.employeeMaster.HRACategory = "N";
                B.employeeMaster.House = "";
            }
        }


        // B.employeeMaster.QuarterAllot = $("#ddlQuatrAlt").val();
        //B.employeeMaster.SpouseQuarter = $("#ddlSpouseQtr").val();
        // B.employeeMaster.SpouseAmount = $("#txtSpouseAmt").val();
        // B.employeeMaster.HRACategory = $("#ddlHRACat").val();
        B.employeeMaster.OLDBasic = ""; //$("#txtOldbasic").val('');
        // B.employeeMaster.House = $("#ddlHouse").val();
        B.employeeMaster.PCTBasic = $("#txtPCTBasic").val();
        B.employeeMaster.LicenceFees = $("#txtrentAmount").val();


        var health = $("#ddlHealth").val();
        if (health == "Y") {
            B.employeeMaster.HealthScheme = health;
            B.employeeMaster.HealthSchemeDetails = $("#txtHealthScheme").val();
            B.employeeMaster.EffectiveFrom = $("#txtEffFrm").val();
            B.employeeMaster.EffectiveTo = $("#txtEffTo").val();
        }
        else if (health == "N") {
            B.employeeMaster.HealthScheme = health;
            B.employeeMaster.HealthSchemeDetails = $("#txtHealthScheme").val();
            B.employeeMaster.EffectiveFrom = $("#txtEffFrm").val();
            B.employeeMaster.EffectiveTo = $("#txtEffTo").val();
        }
        else {
            B.employeeMaster.HealthScheme = "C";
            B.employeeMaster.HealthSchemeDetails = $("#txtHealthScheme").val();
            B.employeeMaster.EffectiveFrom = $("#txtEffFrm").val();
            B.employeeMaster.EffectiveTo = $("#txtEffTo").val();
        }


        B.employeeMaster.DOJPresentDept = $("#txtDOJDept").val();
        B.employeeMaster.PhysicallyChallenged = $("#ddlPhysical").val();


        B.employeeMaster.Group = $("#ddlGroup").val();
        // if (Group == "") { B.employeeMaster.Group == ""; } else { B.employeeMaster.Group = Group; }

        //B.employeeMaster.Category = $("#ddlCategory").val();
        B.employeeMaster.Category = "";
        // if (Catg == "") { B.employeeMaster.Category == ""; } else { B.employeeMaster.Category = Catg; }

        //*******************************Sup****************************************************
        B.employeeMaster.DepartmentId = $("#ddlDepartment").val();
        B.employeeMaster.EmployerPfRate = $("#txtEmployerPfRate").val();
        B.employeeMaster.EmployeePfRate = $("#txtEmployeePfRate").val();

        B.employeeMaster.FathersName = $("#txtFatherName").val();
        B.employeeMaster.AddressRemarks = $("#txtAddressRemarks").val();
        B.employeeMaster.BranchInCharge = $("#txtBranchInCharge").val();

        B.employeeMaster.PFRate = $("#ddlPFRate").val();

        //************************************************************************************

        B.employeeMaster.Status = $("#ddlStatus").val();
        //if (Status == "") { B.employeeMaster.Status == ""; } else { B.employeeMaster.Status = Status; }

        B.employeeMaster.PayScaleType = $("#ddlPayScaleType").val();
        B.employeeMaster.PayScale = $("#ddlPayScale").val();

        B.employeeMaster.CoopMembership = $("#txtCoMembership").val();
        //if (PayScale == "") { B.employeeMaster.PayScale == ""; } else { B.employeeMaster.PayScale = PayScale; }


        //if (HRACategory == "") { B.employeeMaster.HRACategory == ""; } else { B.employeeMaster.HRACategory = HRACategory; }

        B.employeeMaster.PANNO = $("#txtPAN").val(); 
        B.employeeMaster.LWP = $("#txtLWP").val();

        var IR = $("#ddlIR").val();
        if ($("#chkIR").is(':checked')) {
            B.employeeMaster.IR = 0;
        }
        else {
            B.employeeMaster.IR = IR;
        }
      


        ////Bank Details
        B.employeeMaster.BranchID = $("#ddlBranch").val();
        //if (BankID = "") { B.employeeMaster.BankID = ""; } else { B.employeeMaster.BankID = BankID; }
        B.employeeMaster.BankAccountCode = $("#txtBankCode").val();
        ////Phota and Signature
        B.employeeMaster.Photo = $.trim($("#lblPhoto").val());
        B.employeeMaster.Signature = $.trim($("#lblSign").val());
        //Others
        B.employeeMaster.SectorID = $("#ddlSector").val();
        B.employeeMaster.CenterID = $("#ddlCenter").val(); //alert(B.employeeMaster.CenterID);

        window.empMaster.SubmitForm();

    };
    this.setFormValuesforUpdate = function () {
       
        var B = window.empMaster.employeeForm;
        //alert(JSON.stringify(B));

        B.employeeMaster.EmployeeID = $("#hdnEmpID").val();
        B.employeeMaster.EmployeeNo = $("#txtEmpNo").val();
        B.employeeMaster.EmployeeType = $("#ddlEmpType").val();

        var Classifi = $("#ddlClassifctn").val();
        if (Classifi == "") { B.employeeMaster.EmpClassification = ""; } else { B.employeeMaster.EmpClassification = Classifi; }

        B.employeeMaster.EmployeeName = $("#txtEmpName").val();

        var Religion = $("#ddlReligion").val();
        if (Religion == "") { B.employeeMaster.EmployeeReligion = ""; } else { B.employeeMaster.EmployeeReligion = Religion; }

        B.employeeMaster.EmployeeQualification = $("#ddlQualftn").val();
        //if (Qualifi == "") { B.employeeMaster.EmployeeQualification == ""; } else { B.employeeMaster.EmployeeQualification = Qualifi;}

        B.employeeMaster.EmployeeMaritalStatus = $("#ddlMarital").val();
        //if (Marital == "") { B.employeeMaster.EmployeeMaritalStatus == "" } else { B.employeeMaster.EmployeeMaritalStatus = Marital; }

        var Caste = $("#ddlCaste").val();
        if (Caste == "") { B.employeeMaster.EmployeeCaste = "" } else { B.employeeMaster.EmployeeCaste = Caste; }

        B.employeeMaster.EmployeeDOB = $("#txtDOB").val();

        B.employeeMaster.EmployeeGender = $("#ddlSex").val();
        //if (Sex == "") { B.employeeMaster.EmployeeGender == "" } else { B.employeeMaster.EmployeeGender = Sex; }

        //Address Information
        B.employeeMaster.EmployeePresentAddress = $("#txtAddress").val();

        var State = $("#ddlState").val();
        if (State == "") { B.employeeMaster.EmployeePresentState = ""; } else { B.employeeMaster.EmployeePresentState = State; }
        var Dist = $("#ddlDistrict").val();
        if (State == "") { B.employeeMaster.EmployeePresentDistrict = ""; } else { B.employeeMaster.EmployeePresentDistrict = Dist; }

        B.employeeMaster.EmployeePresentCity = $("#txtCity").val();
        var pin = $("#txtPin").val();
        B.employeeMaster.EmployeePresentPin = pin;
        B.employeeMaster.EmployeePresentPhone = $("#txtPhone").val();
        B.employeeMaster.EmployeePresentMobile = $("#txtMobile").val();
        B.employeeMaster.EmployeePresentEmail = $("#txtEmail").val();
        B.employeeMaster.EmployeePermanentAddress = $("#txtAddress_P").val();

        var State_P = $("#ddlState_P").val();
        if (State_P == "") { B.employeeMaster.EmployeePermanentState = ""; } else { B.employeeMaster.EmployeePermanentState = State_P; }
        var Dist_P = $("#ddlDistrict_P").val();
        if (State_P == "") { B.employeeMaster.EmployeePermanentDistrict = ""; } else { B.employeeMaster.EmployeePermanentDistrict = Dist_P; }

        B.employeeMaster.EmployeePermanentCity = $("#txtCity_P").val();
        B.employeeMaster.EmployeePermanentPin = $("#txtPin_P").val();
        B.employeeMaster.EmployeePermanentPhone = $("#txtPhone_P").val();
        ////Official Details
        B.employeeMaster.EmployeeDOJ = $("#txtDOJ").val();
        B.employeeMaster.EmployeeDOR = $("#txtDOR").val();
        B.employeeMaster.EmployeeDesignation = $("#ddlDesig").val();
        B.employeeMaster.EmployeeCooperative = $("#ddlCoop").val();


        var DA = $("#ddlDA").val();
        if ($("#chkDA").is(':checked')) {
            B.employeeMaster.EmployeeDA = 0;
        }
        else {
            B.employeeMaster.EmployeeDA = DA;
        }

        B.employeeMaster.PFCode = $("#txtPFCode").val();
        B.employeeMaster.DNI = $("#txtDNI").val();

        var HRA = $("#ddlHRA").val();
        if ($("#chkHRA").is(':checked')) {       //fIXED HRA
            B.employeeMaster.HRAFixedAmount = $("#txtmHRA").val();
            B.employeeMaster.HRAFlag = "Y";
            B.employeeMaster.QuarterAllot = "N";
            B.employeeMaster.HRA = 0;
            B.employeeMaster.SpouseQuarter = "N";
            B.employeeMaster.SpouseAmount = "";
            B.employeeMaster.HRACategory = "F";
            B.employeeMaster.House = "";
        }
        else {
            if ($("#ddlQuatrAlt").val() == "Y") {   //No HRA
                B.employeeMaster.HRAFixedAmount = "";
                B.employeeMaster.HRAFlag = "N";
                B.employeeMaster.QuarterAllot = "Y";
                B.employeeMaster.HRA = 0;
                B.employeeMaster.SpouseQuarter = "N";
                B.employeeMaster.SpouseAmount = "";
                B.employeeMaster.HRACategory = $("#ddlHRACat").val();
                B.employeeMaster.House = $("#ddlHouse").val();
            }
            else if ($("#ddlQuatrAlt").val() == "N" && $("#ddlSpouseQtr").val() == "Y") {  //Spouse HRA
                B.employeeMaster.HRAFixedAmount = "";
                B.employeeMaster.HRAFlag = "N";
                B.employeeMaster.QuarterAllot = "N";
                B.employeeMaster.HRA = $("#ddlHRA").val();
                B.employeeMaster.SpouseQuarter = "Y";
                B.employeeMaster.SpouseAmount = $("#txtSpouseAmt").val();
                B.employeeMaster.HRACategory = "N";
                B.employeeMaster.House = "";
            }
            else {
                //Normal HRA
                B.employeeMaster.HRAFixedAmount = "";
                B.employeeMaster.HRAFlag = "N";
                B.employeeMaster.QuarterAllot = "N";
                B.employeeMaster.HRA = $("#ddlHRA").val();
                B.employeeMaster.SpouseQuarter = "N";
                B.employeeMaster.SpouseAmount = "";
                B.employeeMaster.HRACategory = "N";
                B.employeeMaster.House = "";
            }
        }


        //B.employeeMaster.QuarterAllot = $("#ddlQuatrAlt").val();
        //B.employeeMaster.SpouseQuarter = $("#ddlSpouseQtr").val();
        // B.employeeMaster.SpouseAmount = $("#txtSpouseAmt").val();
        // B.employeeMaster.HRACategory = $("#ddlHRACat").val();
        B.employeeMaster.OLDBasic = ""; //$("#txtOldbasic").val('');
        //B.employeeMaster.House = $("#ddlHouse").val();
        B.employeeMaster.PCTBasic = $("#txtPCTBasic").val();
        B.employeeMaster.LicenceFees = $("#txtrentAmount").val();


        var health = $("#ddlHealth").val();
        if (health == "Y") {
            B.employeeMaster.HealthScheme = health;
            B.employeeMaster.HealthSchemeDetails = $("#txtHealthScheme").val();
            B.employeeMaster.EffectiveFrom = $("#txtEffFrm").val();
            B.employeeMaster.EffectiveTo = $("#txtEffTo").val();
        }
        else if (health == "N") {
            B.employeeMaster.HealthScheme = health;
            B.employeeMaster.HealthSchemeDetails = $("#txtHealthScheme").val();
            B.employeeMaster.EffectiveFrom = $("#txtEffFrm").val();
            B.employeeMaster.EffectiveTo = $("#txtEffTo").val();
        }
        else {
            B.employeeMaster.HealthScheme = "C";
            B.employeeMaster.HealthSchemeDetails = $("#txtHealthScheme").val();
            B.employeeMaster.EffectiveFrom = $("#txtEffFrm").val();
            B.employeeMaster.EffectiveTo = $("#txtEffTo").val();
        }


        B.employeeMaster.DOJPresentDept = $("#txtDOJDept").val();
        B.employeeMaster.PhysicallyChallenged = $("#ddlPhysical").val();


        B.employeeMaster.Group = $("#ddlGroup").val();
        // if (Group == "") { B.employeeMaster.Group == ""; } else { B.employeeMaster.Group = Group; }

        B.employeeMaster.Category = $("#ddlCategory").val();
        // if (Catg == "") { B.employeeMaster.Category == ""; } else { B.employeeMaster.Category = Catg; }

        //*******************************Sup****************************************************
        B.employeeMaster.DepartmentId = $("#ddlDepartment").val();
        B.employeeMaster.EmployerPfRate = $("#txtEmployerPfRate").val();
        B.employeeMaster.EmployeePfRate = $("#txtEmployeePfRate").val();

        B.employeeMaster.FathersName = $("#txtFatherName").val();
        B.employeeMaster.AddressRemarks = $("#txtAddressRemarks").val();
        B.employeeMaster.BranchInCharge = $("#txtBranchInCharge").val();

        B.employeeMaster.PFRate = $("#ddlPFRate").val();


        //************************************************************************************

        B.employeeMaster.Status = $("#ddlStatus").val();
        //if (Status == "") { B.employeeMaster.Status == ""; } else { B.employeeMaster.Status = Status; }

        B.employeeMaster.PayScaleType = $("#ddlPayScaleType").val();
        B.employeeMaster.PayScale = $("#ddlPayScale").val();

        B.employeeMaster.CoopMembership = $("#txtCoMembership").val();
        //if (PayScale == "") { B.employeeMaster.PayScale == ""; } else { B.employeeMaster.PayScale = PayScale; }


        //if (HRACategory == "") { B.employeeMaster.HRACategory == ""; } else { B.employeeMaster.HRACategory = HRACategory; }

        B.employeeMaster.PANNO = $("#txtPAN").val();
        B.employeeMaster.LWP = $("#txtLWP").val();

        var IR = $("#ddlIR").val();
        if ($("#chkIR").is(':checked')) {
            B.employeeMaster.IR = 0;
        }
        else {
            B.employeeMaster.IR = IR;
        }


        ////Bank Details
        B.employeeMaster.BranchID = $("#ddlBranch").val();
        //if (BankID = "") { B.employeeMaster.BankID = ""; } else { B.employeeMaster.BankID = BankID; }
        B.employeeMaster.BankAccountCode = $("#txtBankCode").val();
        ////Phota and Signature
        B.employeeMaster.Photo = $.trim($("#lblPhoto").val());
        B.employeeMaster.Signature = $.trim($("#lblSign").val());
        //Others
        B.employeeMaster.SectorID = $("#ddlSector").val();
        B.employeeMaster.CenterID = $("#ddlCenter").val(); //alert(B.employeeMaster.CenterID);

        var status = $("#hdnStatus").val(); //alert(status);  //B.employeeMaster.Status; 
        var NetAmount = $('#lblTotalNet').text(); 
        if (NetAmount > 0)
        {
            if (status == 'Y' || status == 'W' )   //|| status == 'S'
            {
              
                var PSType = $("#ddlPayScaleType").val(); //alert(PSType);
                //var PSType =""; //alert(PSType);
                var P = $('#ddlPayScale').find('option:selected').text(); //alert(P);
                var F = $('#ddlEmpType').find('option:selected').text();

                    if (P != "(Select Pay Scale)" && F != "(Select Employee Type)") {
                        //alert(P);
                        if (PSType == "O") {
                            var a = P.split('-');
                            for (var i = 0; i < a.length; i++) {
                                var min = a[0]; var max = a[a.length - 1];
                            }
                        }
                        else if (PSType == "K") {
                            var a = P.split('-'); //alert(a);
                            for (var i = 0; i < a.length; i++) {
                                var min = a[1]; var V = a[2]; 
                                var max = V.substring(0, V.indexOf('('));
                                //alert(min); alert(V); alert(max);
                            }
                        }
                        else if (PSType == "S") {
                            var a = P.split('-'); //alert(a); 
                            for (var i = 0; i < a.length; i++) {
                                var min = a[1]; var V = a[2];
                                var max = V.substring(0, V.indexOf('('));
                            }
                        }
                        else {
                            var a = P.split('-'); //alert(a); 
                            for (var i = 0; i < a.length; i++) {
                                var min = a[0]; var V = a[1];
                                var max = V.substring(0, V.indexOf('('));
                            }
                        }
                        //alert(min); alert(max);
                        var amount = parseInt($("#hdnPayScaleAmount").val()); //alert(amount);
                        
                        if (amount >= min && amount <= max) {
                           window.empMaster.UpdateForm();
                        }
                        else {
                            alert("Enter Amount is not within Selected -- Pay-Band -- !");
                        }
                    }
                    else {
                        alert("Please Select -- Pay-Scale --  And -- Employee Type -- !");
                    }

                
            }
            else if (status == 'S')
            {
                
                window.empMaster.UpdateForm();
            }
            else
            {
                alert("This Employee is not ACTIVE. You are not allowed to Update.");
            }
        }
        else
        {

            alert("Sorry ! You can not Update Account \n Because Net Amount should not be Negative.");
        }


    };
    this.setFormValuesPayDetails = function () {
        var B = window.empMaster.employeeForm; 
        window.empMaster.AddPayDetails();
    };
    this.BookingDateChanged = function () {
        //alert('changed');
        //window.enquiryPage.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
    this.BookingDateChangedTo = function () {
        //alert('changed');
        //A.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
    this.UploadFilePhoto = function () {
        var B = window.empMaster.employeeForm;
        $(".loading-overlay").show();
        $("#lblPhoto").val('');
        $("#imgPhoto").empty().html("");
        $("#canPhoto").empty().html('');
        if ($("#fileToUpload").val() == "") {
            $(".loading-overlay").hide();
            alert('Select file before uploading');
            return false;
        }
        //if (!CheckForImageFile("#fileToUpload")) {
        //    alert('select');
        //    return false;
        //}
        //if (!GetFileSize("fileToUpload", 'P')) {
        //    //alert('select');
        //    return false;
        //}

        var fileUpload = $("#fileToUpload").get(0);
        var files = fileUpload.files;

        var formData = new window.FormData();
        var filename = files[0].name;

        var C = "{\'name\':\'" + filename + "\'}";

        $("#buttonUpload").hide();
        $.ajaxFileUpload(
            {
                url: 'AjaxFileUpload.ashx?picsFolder=UploadPhoto',
                //url: 'AjaxFileUpload.ashx',
                async: true,
                secureuri: false,
                fileElementId: 'fileToUpload',
                dataType: 'json',
                data: C,
                success: function (data, status) {
                    $(".loading-overlay").hide();
                    $('#buttonUpload').show();
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            var html = "<img src='images/cross.png' alt='cross' />";
                            $("#imgPhoto").empty().html(html);
                            $("#lblPhoto").val('');
                            $("#canPhoto").empty().html('');
                            alert(data.error);
                        } else {
                            var B = window.empMaster.employeeForm;
                            $("#lblPhoto").val(data.msg);
                            var html = "<img src='images/check.png' alt='tick' />";
                            $("#imgPhoto").empty().html(html);
                            var cnhtml = "<img src='" + data.msg + "' width='100px' height='116px' alt='canimg' />"
                            $("#canPhoto").empty().html(cnhtml);

                        }
                    } else {
                        var html = "<img src='images/cross.png' alt='cross' />";
                        $("#imgPhoto").empty().html(html);
                        $("#lblPhoto").val('');
                        $("#canPhoto").empty().html('');
                    }
                },
                error: function (data, status, e) {
                    alert(e);
                    //$('#buttonUpload').attr("disabled", false);
                    $('#buttonUpload').show();
                    var html = "<img src='images/cross.png' alt='cross' />";
                    $("#imgPhoto").empty().html(html);
                    $("#lblPhoto").val('');
                    $("#canPhoto").empty().html('');
                    $("#loading").hide();
                }
            }
           )
        //$('#buttonUpload').show();
        return false;
    };
    this.UploadFileSign = function () {
        var B = window.empMaster.employeeForm;
        $(".loading-overlay").show();
        $("#lblSign").val('');
        $("#imgSign").empty().html("");
        $("#canSign").empty().html('');
        if ($("#fileToUploadSign").val() == "") {
            $(".loading-overlay").hide();
            alert('Select file before uploading');
            return false;
        }
        //if (!CheckForImageFile("#fileToUploadSign")) {
        //    //alert('select');
        //    return false;
        //}
        //if (!GetFileSize("fileToUploadSign", 'S')) {
        //    //alert('select');
        //    return false;
        //}
        $("#lblSign").val('');
        var fileUpload = $("#fileToUploadSign").get(0);
        var files = fileUpload.files;

        var formData = new window.FormData();
        var filename = files[0].name;

        var C = "{\'name\':\'" + filename + "\'}";

        $('#buttonUploadSign').hide();
        $.ajaxFileUpload
        (
            {
                url: 'AjaxFileUpload.ashx?picsFolder=UploadSign',
                async: true,
                secureuri: false,
                fileElementId: 'fileToUploadSign',
                dataType: 'json',
                data: C,
                success: function (data, status) {
                    $(".loading-overlay").hide();
                    $("#loadingSign").hide();
                    //$('#buttonUploadSign').attr("disabled", false);
                    $('#buttonUploadSign').show();
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            var html = "<img src='images/cross.png' alt='cross' />";
                            $("#imgSign").empty().html(html);
                            $("#lblSign").val('');
                            $("#canSign").empty().html('');
                            alert(data.error);
                        } else {
                            var B = window.empMaster.employeeForm;
                            $("#lblSign").val(data.msg);
                            var html = "<img src='images/check.png' alt='tick' />";
                            $("#imgSign").empty().html(html);
                            var cnhtml = "<img src='" + data.msg + "' width='150px' height='27px' alt='canimg' />"
                            $("#canSign").empty().html(cnhtml);
                        }
                    } else {
                        var html = "<img src='images/cross.png' alt='cross' />";
                        $("#imgSign").empty().html(html);
                        $("#lblSign").val('');
                        $("#canSign").empty().html('');
                    }
                },
                error: function (data, status, e) {
                    alert(e);
                    $('#buttonUploadSign').show();
                    //$('#buttonUploadSign').attr("disabled", false);
                    var html = "<img src='images/cross.png' alt='cross' />";
                    $("#imgSign").empty().html(html);
                    $("#lblSign").val('');
                    $("#canSign").empty().html('');
                    $("#loadingSign").hide();
                }
            }
           )
        return false;
    };
    this.HouseRent = function () {
        window.empMaster.GetHouseRent();
    };
    this.TransferEmp = function () {
        window.empMaster.SearchEmployeeTransfer();
    };
    this.SaveStatusPopupDetail = function () {
        if ($("#txtOrderNo").val() != "")
        {
            if ($("#txtOrderDate").val() != "")
            {
                if ($("#txtRemarks").val() != "") {
                    window.empMaster.SavePopUpDetail();
                }
                else
                {
                    $("#txtRemarks").focus();
                    return true;
                }
            }
            else
            {
                $("#txtOrderDate").focus();
                return true;
            }
        }
        else
        {
            $("#txtOrderNo").focus();
            return true;
        }
    };

};
KMDA.EmployeeMaster = function () {
    var A = this;
    this.employeeForm;
    this.init = function () {
        this.employeeForm = JSON.parse($("#empJSON").val());
        this.masterInfo = new KMDA.MasterInfo();
        this.masterInfo.init();
        var B = window.empMaster.employeeForm;
        //alert(JSON.stringify(B));
      
        var E = B.empAddPayDetails;

            //for (var k = 0; k < E.length; k++)
            //{
            //    var id = E[k].EarnDeductionID; //alert(id);
            //    if(id==1)
            //        $("#hdnItax").val("Yes");
            //    else
            //        $("#hdnItax").val("No");
            //}

        var status = $("#hdnStatus").val(); 
        //alert(E.length);
        if (E.length > 0) {
            if (status != "S")
                A.showPayDetail(E);
            else
                window.empMaster.showPayDetailonSuspended(E);
        }
        else {
        }
        //var F = E; alert(F);
        if (E == "") {
            A.showPayDetail(E);
        }
        var B = window.empMaster.employeeForm;
        var G = B.employeeMaster;

        var cnhtmlP = "<img src='" + G.Photo + "' width='100px' height='116px'  />"
        $("#canPhoto").empty().html(cnhtmlP);

        var cnhtmlS = "<img src='" + G.Signature + "' width='150px' height='27px'  />"
        $("#canSign").empty().html(cnhtmlS);
    };

    this.SubmitForm = function () {
        $(".loading-overlay").show();
        var B = window.empMaster.employeeForm;

        var isNewEmp = "";
        if ($("#chkNewEmp").is(':checked'))
            isNewEmp = "New";
        else
            isNewEmp = "New";
       
        var C = "{\'employeeForm\':\'" + JSON.stringify(B) + "\', \'SecID\':\'" + $("#ddlSector").val() + "\', \'isNewEmp\':\'" + isNewEmp + "\'}";
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveEmployee',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var data = JSON.parse(D.d);
                if (data == "Not") {
                    alert('Sorry ! Your salary has been already Processed. \n You can not Save.');
                    return;
                }
                else {
                    window.empMaster.employeeForm = JSON.parse(D.d);
                    var A = window.empMaster.employeeForm;
                    var EmpNo = A.EmpNo;
                    
                    alert('Employee Details Saved Successfully ! Your Employee No is- ' + EmpNo);
                    window.location.href = "EmployeeMaster.aspx";
                }
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
    this.UpdateForm = function () {
        //alert('567');
        var data = '';
        var B = window.empMaster.employeeForm;
        //----------------------------------------------------------------------------------------------
        var SearchStatus = $("#hdnStatus").val(); //alert(SearchStatus);
        var status = $("#ddlStatus").val(); //alert(status);
        if (SearchStatus == "W" || SearchStatus == "S" || status == "W" || status == "S" || status == "Y" || status == "T" || status == "R" || status == "N" || status == "A" || status == "D" || status == "P" || status == "V" || status == "I") {
            var F = "{Status: '" + $('#ddlStatus').find('option:selected').text() + "', SearchStatus:'" + SearchStatus + "'}";
            //alert(F);
            $.ajax({
                type: "POST",
                url: pageUrl + '/Check_Status',
                data: F,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = JSON.parse(D.d);
                    if (t == "Matched")
                    {
                        //************************************************************************************************
                        //Check Pan Card No
                        //************************************************************************************************
                        //                              This is for PAN Card and ITax
                        //************************************************************************************************
                        var itxAfterAdd = $("#hdnItax").val(); 
                        var itax = "";
                        var PanNo = $("#txtPAN").val();
                        var B = window.empMaster.employeeForm; 
                        for (var k = 0; k < B.empAddPayDetails.length; k++) {
                            var id = B.empAddPayDetails[k].EarnDeductionID; //alert(id);
                            if (id == 1) {
                                itax = "Yes";
                                break;
                            }
                            else {
                                itax = "No";
                            }
                        }
                        //alert(itax); alert(PanNo);
                        if (PanNo == "") {
                            if (itax == "No" && itxAfterAdd == "No") {
                                $(".loading-overlay").show();
                                var C = "{\'employeeForm\':\'" + JSON.stringify(B) + "\', \'SecID\':\'" + $("#ddlSector").val() + "\'}";
                                $.ajax({
                                    type: "POST",
                                    url: pageUrl + '/UpdateEmployee',
                                    data: C,
                                    contentType: "application/json; charset=utf-8",
                                    success: function (D) {
                                        $(".loading-overlay").hide();
                                        data = JSON.parse(D.d);
                                        if (data == "Not") {
                                            alert('Sorry ! Your salary has been already Processed. \nYou can not Update.');
                                            return;
                                        }
                                        else {
                                            window.empMaster.employeeForm = JSON.parse(D.d);
                                            alert('Employee Details are Updated Successfully !');
                                            window.location.href = "EmployeeMaster.aspx";
                                            return true;
                                        }
                                    }
                                });
                            }
                            else {
                                alert('Please Enter PAN NO because you have selected ITAX.');
                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: [2],
                                    disabled: []
                                });
                                $("#txtPAN").focus();
                            }
                        }
                        else
                        {
                            if (itax == "No" || itax == "Yes") {
                                var isValid = false;
                                var regex = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/;
                                isValid = regex.test($("[id*=txtPAN]").val());
                                if (isValid) {
                                    $(".loading-overlay").show();
                                    var C = "{\'employeeForm\':\'" + JSON.stringify(B) + "\', \'SecID\':\'" + $("#ddlSector").val() + "\'}";
                                    $.ajax({
                                        type: "POST",
                                        url: pageUrl + '/UpdateEmployee',
                                        data: C,
                                        contentType: "application/json; charset=utf-8",
                                        success: function (D) {
                                            $(".loading-overlay").hide();
                                            data = JSON.parse(D.d);
                                            if (data == "Not") {
                                                alert('Sorry ! Your salary has been already Processed. \nYou can not Update.');
                                                return;
                                            }
                                            else {
                                                window.empMaster.employeeForm = JSON.parse(D.d);
                                                alert('Employee Details are Updated Successfully !');
                                                window.location.href = "EmployeeMaster.aspx";
                                                return true;
                                            }
                                        }
                                    });
                                }
                                else {
                                    alert('You have entered invalid PAN NO.');
                                    $("#tabs").tabs({
                                        collapsible: true,
                                        selected: [2],
                                        disabled: []
                                    });
                                    $("#txtPAN").focus();
                                }
                            }
                        }
                        //************************************************************************************************
                    }
                    else {
                        alert("Sorry ! You can not Update.\nPlease Select Status and Enter Proper Inputs.");

                    }
                }
            });
        }
    };
    this.AddPayDetails = function (EarnDeductionID, EarnDeduction, Amount, PayScale, PSType, Pid, DA, ptaxval, QA, HRACat, Houseid, chkHRAFlag, txtHRAValue, hrafromddl, LicFee, SQ, SQAmount, health, houseorbasic, rentAmount, pctBasic) {

        var status = $("#ddlStatus").val(); 
        if (status == "Y") {
            //alert("status");
            var html = '';
            var PayScale = ''; var TotalEarn = 0; var TotalDedu = 0; var Net = 0; var TotalEarning = 0;
            var DA = 0; var HRA = 0; var ptaxval = 0; var chkHRAFlag = ""; var LicFee = 0; var SQAmount = 0;
            var QA = ""; var HRACat = ""; var Houseid = 0; var noofrows = 0; var txtHRAValue = 0; var houseorbasic = ""; var pctBasic = "";
            var IR = '';

            var B = window.empMaster.employeeForm;

            //This is for Pay Scale
            var PSType = $("#ddlPayScaleType").val();
            var Pid = ($("#ddlPayScale").val() == "" ? 0 : $("#ddlPayScale").val()); //alert(Pid);
            var P = $('#ddlPayScale').find('option:selected').text();
            if (P == "(Select Pay Scale)") {
                PayScale = 0;
            }
            else { PayScale = P; }

            //This is for HRA Value
            if ($("#chkHRA").is(':checked')) {
                chkHRAFlag = "Y";
                txtHRAValue = $("#txtmHRA").val();
            } else {
                chkHRAFlag = "N";
                txtHRAValue = $("#txtmHRA").val();
            }

            var Q = $("#ddlQuatrAlt").val(); if (Q == 0) { QA = 0; } else { QA = Q; } //alert(QA);
            var HCat = $("#ddlHRACat").val(); if (HCat == 0) { HRACat = 0; } else { HRACat = HCat; } //alert(HRACat);
            var Hid = $("#ddlHouse").val(); if (Hid == "") { Houseid = 0; } else { Houseid = Hid; } //alert(Houseid);
            var hrafromddl = ($('#ddlHRA').find('option:selected').text() == ("(Select HRA)") ? 0 : $('#ddlHRA').find('option:selected').text()); //alert(hrafromddl);
            //var txtHRAValue = $("#txtmHRA").val();
            var L = $("#txtrentAmount").val(); if (L == "") { LicFee = 0; } else { LicFee = L; } //alert(LicFee);
            var SQ = $("#ddlSpouseQtr").val(); //alert(SQ);
            var S = $("#txtSpouseAmt").val(); if (S == "") { SQAmount = 0; } else { SQAmount = S; } //alert(SQAmount);
            var health = $("#ddlHealth").val();


            //This is for house or basic selection
            if ($("#rdHouse").is(':checked')) {
                houseorbasic = "House";
            } else {
                houseorbasic = "Basic";
                pctBasic = $("#txtPCTBasic").val();
            }
            var rentAmount = $("#txtrentAmount").val();

            //This is for DA Value
            if ($("#chkDA").is(':checked')) {
                DA = 0;
            } else {
                var daValue = $('#ddlDA').find('option:selected').text();
                DA = daValue == "(Select DA)" ? 0 : daValue;
            }

            //This is for PTax if Physically Challenged
            var PH = $('#ddlPhysical').val();
            if (PH == "Y") {
                ptaxval = 0;
            }
            else { ptaxval = 1; }

            var C = $("#ddlEarnDeduction").val();
            var E = $('#ddlEarnDeduction').find('option:selected').text();

            if (C == "1" || C == "22") {
                $("#hdnItax").val("Yes");
                $("#hdnCopSubs").val("Yes");
            }
            else {
                $("#hdnItax").val("No");
                $("#hdnCopSubs").val("No");
            }
            //alert($("#hdnItax").val());

            //This is for IR Value
            var empType = $("#ddlEmpType").val();
            if ($("#chkIR").is(':checked') && empType !="") {
                IR = 0;
            } else {
                var IRValue = $('#ddlIR').find('option:selected').text();
                IR = IRValue == "(Select IR)" ? 0 : IRValue;
            }
           
            var GpfAmount = $("#txtEmployeePfRate").val();
            var CpfAmount = $("#txtEmployerPfRate").val();

            var FathersName = $("#txtFatherName").val();
            var AddressRemarks = $("#txtAddressRemarks").val();
            var BranchInCharge = $("#txtBranchInCharge").val();
            var Array = [];

            //var PSType1 = "";
            var F = "{EarnDeductionID: " + C + ", EarnDeduction: '" + E + "', Amount: " + $('#txtAmount').val() + ", PayScale: '" + PayScale + "', Pid: " + Pid + ", PSType: '" + PSType + "', DA: '" + DA + "', ptaxval: '" + ptaxval + "', Houseid: '" + Houseid + "', QA: '" + QA + "', HRACat: '" + HRACat + "', Houseid: '" + Houseid + "', chkHRAFlag: '" + chkHRAFlag + "', txtHRAValue: '" + txtHRAValue + "', hrafromddl: '" + hrafromddl + "', LicFee: '" + LicFee + "', SQ: '" + SQ + "', SQAmount: '" + SQAmount + "', health: '" + health + "' , houseorbasic: '" + houseorbasic + "', rentAmount: '" + rentAmount + "',pctBasic: '" + pctBasic + "', IR: '" + IR + "',GpfRate:'" + GpfAmount + "',CpfRate:'" + CpfAmount + "',FathersName:'" + FathersName + "',AddressRemarks:'" + AddressRemarks + "',BranchInCharge:'" + BranchInCharge + "'}";
            //alert(F);
            $.ajax({
                type: "POST",
                url: pageUrl + '/AddDetails',
                data: F,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var data1 = JSON.parse(D.d);
                    $.each(data1, function (index, value) {
                        var objdata1 = {};
                        if (value.EarnDeductionType=="E")
                        {
                            objdata1.Amount = value.Amount;
                            objdata1.EarnDeduction = value.EarnDeduction;
                            objdata1.EarnDeductionID = value.EarnDeductionID;
                            objdata1.EarnDeductionType = value.EarnDeductionType;
                            Array.push(objdata1);

                        }
                    });
                    $.each(data1, function (index, value) {
                        var objdata2 = {};
                        if (value.EarnDeductionType == "D") {
                            objdata2.Amount = value.Amount;
                            objdata2.EarnDeduction = value.EarnDeduction;
                            objdata2.EarnDeductionID = value.EarnDeductionID;
                            objdata2.EarnDeductionType = value.EarnDeductionType;
                            Array.push(objdata2);

                        }

                    });
                    var data = Array;
                    if (data.length > 0) {
                        
                        //var EDType = data[0].EarnDeductionType;
                        html += " <table align='center' cellpadding='2' width='98%'>" + "\n"
                        // + "<tr><td colspan='5' class='labelCaption'><hr style='border:solid 1px lightblue' /></td></tr>"
                                  + "</table>";
                        html += "<table align='center' id='detailTable' class='divTable' border='0' cellpadding='2' width='98%'>" + "\n"

                              + "<tr><td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Edit</td>"
                              + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Delete</td>"
                              + "<td class='th' style= 'background-color:lightblue;'></td>"
                              + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Earn/Deduction</td>"
                              + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Amount</td>"
                                  + "</tr>";

                        html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                        
                        for (var i = 0; i < data.length; i++) {
                            var p = data[i].EarnDeductionID; //alertsup(p);
                            var T = data[i].EarnDeductionType;

                            

                            if (T == "E") {
                                var amts = data[i]["Amount"];
                                TotalEarn = parseFloat(TotalEarn) + parseFloat(amts);
                            }
                            else {
                                var amts = data[i]["Amount"];
                                TotalDedu = parseFloat(TotalDedu) + parseFloat(amts);
                            }
                            if (TotalDedu > TotalEarn) {
                                alert("Total Deduction amount should be not greater than Total Earning amount.");
                                delete data[i].EarnDeductionID;
                                delete data[i].EarnDeduction;
                                delete data[i].Amount;
                                delete data[i].EarnDeductionType;
                                return false;
                            }
                            

                            if (p == 9 || p == 3 || p == 4 || p == 5 || p == 7 || p == 18 || p == 22 || p==39) {

                                html += "<tr id='EarnDeductionID" + data[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + data[i].EarnDeductionID + ", " + data[i].Amount + " );'></a></div></td>"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + data[i].EarnDeductionID + ");'></a></div></td>"
                                        + "<td  class='tr' style='visibility:hidden;'>" + data[i].EarnDeductionID + "</td>"//style='visibility:hidden'
                                        + "<td  class='tr' >" + data[i].EarnDeduction + "</td>"
                                        + "<td  class='tr' >" + data[i].Amount + "</td>"
                                html += "</tr>";
                                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                                noofrows++;
                            }

                            else {
                                //================================================
                                if (p == 14)
                                {
                                    var SalMonth = $("#hdnSalMonth").val();
                                    if (SalMonth == 3) {
                                        html += "<tr id='EarnDeductionID" + data[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                            + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + data[i].EarnDeductionID + ", " + data[i].Amount + " );'><img src='images/Edit.jpg' id='deleteEnquiryTokenDetID_" + data[i].EarnDeductionID + "' /></a></div></td>"
                                            + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + data[i].EarnDeductionID + ");'></a></div></td>"
                                            + "<td  class='tr' style='visibility:hidden;' >" + data[i].EarnDeductionID + "</td>"//style='visibility:hidden'
                                            + "<td  class='tr' >" + data[i].EarnDeduction + "</td>"
                                            + "<td  class='tr' >" + data[i].Amount + "</td>"
                                        html += "</tr>";
                                        html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                                    }
                                    else
                                    {
                                        html += "<tr id='EarnDeductionID" + data[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                            + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + data[i].EarnDeductionID + ", " + data[i].Amount + " );'></a></div></td>"
                                            + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + data[i].EarnDeductionID + ");'></a></div></td>"
                                            + "<td  class='tr' style='visibility:hidden;' >" + data[i].EarnDeductionID + "</td>"//style='visibility:hidden'
                                            + "<td  class='tr' >" + data[i].EarnDeduction + "</td>"
                                            + "<td  class='tr' >" + data[i].Amount + "</td>"
                                        html += "</tr>";
                                        html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                                    }
                                }
                                    //================================================
                                else {

                                    if (p == 2) {
                                        var payAmount = data[i].Amount; //alert(payAmount);
                                        $("#hdnPayScaleAmount").val(payAmount);
                                    }

                                    html += "<tr id='EarnDeductionID" + data[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + data[i].EarnDeductionID + ", " + data[i].Amount + " );'><img src='images/Edit.jpg' id='deleteEnquiryTokenDetID_" + data[i].EarnDeductionID + "' /></a></div></td>"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + data[i].EarnDeductionID + ", " + noofrows + ");'><img src='images/delete.png' id='deleteEnquiryTokenDetID_" + data[i].EarnDeductionID + "' /></a></div></td>"
                                        + "<td  class='tr' style='visibility:hidden;' >" + data[i].EarnDeductionID + "</td>"//style='visibility:hidden'
                                        + "<td  class='tr' >" + data[i].EarnDeduction + "</td>"
                                        + "<td  class='tr' >" + data[i].Amount + "</td>"
                                    html += "</tr>";
                                    html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                                }
                            }

                            var Net = TotalEarn - TotalDedu;

                        }


                        html += "</table>";
                        $("#divTable").empty().html(html);
                        $('#lblTotalEarning').text(TotalEarn);
                        $('#lblTotalDeduction').text(TotalDedu);
                        $('#lblTotalNet').text(Net.toFixed(2));
                        $("select#ddlEarnDeduction").val('0');
                        $("#txtAmount").val('');
                    }
                    return data; //alert(JSON.stringify(data));
                },
                error: function (response) {
		    alert(response.d);
                },
                failure: function (response) {
                    alert(response.d);
                }

            });
        }
        else {
            window.empMaster.UpdateSuspendedPayDetails();
        }
    };
    this.removePayDetail = function (EarnDeductionID, noofrows) {
        var j = confirm("Are you sure you want to delete this ?");
        if (j == true) {

            var status = $("#ddlStatus").val();
            if (status != "S") {
                $(".loading-overlay").show();
                var f = 0;
                var ajaxUrl = pageUrl + '/DeletePayDetail';
                var B = {};
                B.employeeForm = window.empMaster.employeeForm;
                var F = window.empMaster.empAddPayDetails;

                var E = "{\'employeeForm\':\'" + JSON.stringify(B) + "\',\'EarnDeductionID\':\'" + EarnDeductionID + "\',\'noofrows\':\'" + noofrows + "\'}";
                //alert(E);
                $.ajax({
                    type: "POST",
                    url: ajaxUrl,
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        window.empMaster.employeeForm = JSON.parse(D.d);
                        var da = JSON.parse(D.d); //alert(JSON.stringify(da));

                        for (var k = 0; k < da.empAddPayDetails.length; k++) {
                            var id = da.empAddPayDetails[k].EarnDeductionID; //alert(id);
                            if (id == 1) {
                                $("#hdnItax").val('No');
                                break;
                            }
                        }

                        $("#divTable table[id='detailTable']").remove(); 
                        window.empMaster.showPayDetail(da.empAddPayDetails);

                    }
                });
            }
            else {
                $(".loading-overlay").show();
                var f = 0;
                var ajaxUrl = pageUrl + '/DeletePayDetailbySuspended';
                var B = {};
                B.employeeForm = window.empMaster.employeeForm;
                var F = window.empMaster.empAddPayDetails;
                var E = "{\'employeeForm\':\'" + JSON.stringify(B) + "\',\'EarnDeductionID\':\'" + EarnDeductionID + "\'}";
                //alert(E);
                $.ajax({
                    type: "POST",
                    url: ajaxUrl,
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        window.empMaster.employeeForm = JSON.parse(D.d);
                        var da = JSON.parse(D.d);



                        $("#divTable table[id='detailTable']").remove();
                        window.empMaster.showPayDetailonSuspended(da.empAddPayDetails);

                    }
                });
            }
        }
        else { return false; }
        return false;
    };
    this.getPayVal = function (EarnDeductionID) {
        var ajaxUrl = pageUrl + '/ExactPayValbyEDID';
        var C = $("#ddlEarnDeduction").val();
        var E = "{\'EarnDeductionID\':\'" + C + "\'}";
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d);
                if (data.length > 0) {
                    var amount = data[0].PayVal;
                    if (amount != null) {
                        $("#txtAmount").val(amount);
                        document.getElementById("txtAmount").disabled = true;
                    } else {
                        document.getElementById("txtAmount").disabled = false;
                    }

                }
            }
        });
        return false;
    };
    this.updatePayDetail = function (EarnDeductionID, Amount) {
       
        $('#cmdAdd').hide();
        $('#cmdUpdate').show();
        $('#cmdCancels').show();

        $("#ddlEarnDeduction").val(EarnDeductionID);
        $("#txtAmount").val(Amount);

        $("#btnUpdate").prop("disabled", true);
        $("#btnUpdate1").prop("disabled", true);

        $("#ddlEarnDeduction").prop('disabled', true);
        return false;
    };
    this.UpdateFinalPayDetails = function (C, E, Amount, PayScale, PSType, Pid, DA, ptaxval, QA, HRACat, Houseid, chkHRAFlag, txtHRAValue, hrafromddl, LicFee, SQ, SQAmount, health, houseorbasic, rentAmount, pctBasic,IR) {

        var PayScale = ''; var TotalEarn = 0; var TotalDedu = 0; var Net = 0; var TotalEarning = 0;
        var DA = 0; var HRA = 0; var ptaxval = 0; var chkHRAFlag = ""; var LicFee = 0; var SQAmount = 0;
        var QA = ""; var HRACat = ""; var Houseid = 0; var noofrows = 0; var houseorbasic = "";  var health = "";
        var IR = ""; var CpfRate = 0; var GpfRate = 0;


        var GpfAmount = $("#txtEmployeePfRate").val();
        var CpfAmount = $("#txtEmployerPfRate").val();

        var FathersName = $("#txtFatherName").val();
        var AddressRemarks = $("#txtAddressRemarks").val();
        var BranchInCharge = $("#txtBranchInCharge").val();

        var B = window.empMaster.employeeForm;
        var G = "{\'employeeForm\':\'" + JSON.stringify(B) + "\'}";
        //alert(JSON.stringify(G));

        //This is for Pay Scale
        var PSType = $("#ddlPayScaleType").val();
        var Pid = ($("#ddlPayScale").val() == "" ? 0 : $("#ddlPayScale").val());
        var P = $('#ddlPayScale').find('option:selected').text();
        if (P == "(Select Pay Scale)") {
            PayScale = 0;
        }
        else { PayScale = P; }

        //This is for HRA Value
        if ($("#chkHRA").is(':checked')) {
            chkHRAFlag = "Y";
            var txtHRAValue = $("#txtmHRA").val();
        } else {
            chkHRAFlag = "N";
            txtHRAValue = $("#txtmHRA").val();
        }

        var Q = $("#ddlQuatrAlt").val(); if (Q == 0) { QA = 0; } else { QA = Q; } //alert(QA);
        var HCat = $("#ddlHRACat").val(); if (HCat == 0) { HRACat = 0; } else { HRACat = HCat; } //alert(HRACat);
        var Hid = $("#ddlHouse").val(); if (Hid == "") { Houseid = 0; } else { Houseid = Hid; } //alert(Houseid);
        var hrafromddl = ($('#ddlHRA').find('option:selected').text() == ("(Select HRA)") ? 0 : $('#ddlHRA').find('option:selected').text()); //alert(hrafromddl);
        //var txtHRAValue = $("#txtmHRA").val();
        var L = $("#txtLic").val(); if (L == "") { LicFee = 0; } else { LicFee = L; } //alert(LicFee);
        var SQ = $("#ddlSpouseQtr").val(); //alert(SQ);
        var S = $("#txtSpouseAmt").val(); if (S == "") { SQAmount = 0; } else { SQAmount = S; } //alert(SQAmount);


       var healthScheme = $("#ddlHealth").val();
       if (healthScheme == "Y")
           health = "Y";
       else if (healthScheme == "N")
           health = "N";
       else
           health = "C";

        //This is for house or basic selection
        if ($("#rdHouse").is(':checked')) {
            houseorbasic = "House";
        } else {
            houseorbasic = "Basic";
            var pctBasic = $("#txtPCTBasic").val();
        }
        var rentAmount = $("#txtrentAmount").val();

        //This is for DA Value
        if ($("#chkDA").is(':checked')) {
            DA = 0;
        } else {
            var daValue = $('#ddlDA').find('option:selected').text();
            DA = daValue == "(Select DA)" ? 0 : daValue;
        }

        //This is for IR Value
        //if ($("#chkIR").is(':checked')) {
        //    IR = 0;
        //} else {
        //    var irValue = $('#ddlIR').find('option:selected').text();
        //    IR = irValue == "(Select IR)" ? 0 : irValue;
        //}

        //This is for IR Value
        var empType = $("#ddlEmpType").val();
        if ($("#chkIR").is(':checked') && empType != "") {
            IR = 0;
        } else {
            var IRValue = $('#ddlIR').find('option:selected').text();
            IR = IRValue == "(Select IR)" ? 0 : IRValue;
        }

        //This is for PTax if Physically Challenged
        var PH = $('#ddlPhysical').val();
        if (PH == "Y") {
            ptaxval = 0;
        }
        else { ptaxval = 1; }

        var C = $("#ddlEarnDeduction").val();
        var E = $('#ddlEarnDeduction').find('option:selected').text();
        var Amount = $('#txtAmount').val();

        var F = "{  EarnDeductionID: " + C + ", EarnDeduction: '" + E + "', Amount: " + $('#txtAmount').val() + ", PayScale: '" + PayScale + "', Pid: " + Pid + ", PSType: '" + PSType + "', DA: '" + DA + "', ptaxval: '" + ptaxval + "',  QA: '" + QA + "', HRACat: '" + HRACat + "', Houseid: '" + Houseid + "', chkHRAFlag: '" + chkHRAFlag + "', txtHRAValue: '" + txtHRAValue + "', hrafromddl: '" + hrafromddl + "', LicFee: '" + LicFee + "', SQ: '" + SQ + "', SQAmount: '" + SQAmount + "', health: '" + health + "' , houseorbasic: '" + houseorbasic + "', rentAmount: '" + rentAmount + "',pctBasic: '" + pctBasic + "',IR: '" + IR + "',CpfRate:'" + CpfAmount + "',GpfRate:'" + GpfAmount + "',FathersName:'" + FathersName + "',AddressRemarks:'" + AddressRemarks + "',BranchInCharge:'" + BranchInCharge + "'}";
        //alert(F);
        var ajaxUrl = pageUrl + '/UpdatePayDetail';
        var B = {};

        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: F,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //window.empMaster.employeeForm = JSON.parse(D.d);
                var da = JSON.parse(D.d);
                //alert(JSON.stringify(da));
                if (da.length > 0) {
                    $("#cmdUpdate").hide();
                    $("#cmdCancels").hide();
                    $("#cmdAdd").show();
                    $("#ddlEarnDeduction").val(0);
                    $("#txtAmount").val('');
                    $("#ddlEarnDeduction").prop('disabled', false);
                    window.empMaster.showPayDetail(da);
                }
            }
        });
        return false;
    };
    this.showPayDetail = function (PayDetail1) {
        var html = ''; var TotalEarn = 0; var TotalDedu = 0; var Net = 0; var noofrows = 0;
        var Array = [];
        if (PayDetail1 != "undefined") {
            //alert(JSON.stringify(PayDetail));

            //*******************************Supriyo Ghosh**************************************
            $.each(PayDetail1, function (index, value) {
                var objdata1 = {};
                if (value.EarnDeductionType == "E") {
                    objdata1.Amount = value.Amount;
                    objdata1.EarnDeduction = value.EarnDeduction;
                    objdata1.EarnDeductionID = value.EarnDeductionID;
                    objdata1.EarnDeductionType = value.EarnDeductionType;
                    Array.push(objdata1);

                }
            });
            $.each(PayDetail1, function (index, value) {
                var objdata2 = {};
                if (value.EarnDeductionType == "D") {
                    objdata2.Amount = value.Amount;
                    objdata2.EarnDeduction = value.EarnDeduction;
                    objdata2.EarnDeductionID = value.EarnDeductionID;
                    objdata2.EarnDeductionType = value.EarnDeductionType;
                    Array.push(objdata2);

                }

            });
            var PayDetail = Array;
            //**********************************************************************************




            if (PayDetail.length > 0) {

                html += " <table align='center' cellpadding='5' width='98%'>" + "\n"
                // + "<tr><td colspan='7' class='labelCaption'><hr style='border:solid 1px lightblue' /></td></tr>"
                          + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"

                          + "<tr><td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Edit</td>"
                          + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Delete</td>"
                          + "<td class='th' style= 'background-color:lightblue;'></td>"
                          + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Earn/Deduction</td>"
                          + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Amount</td>"
                          + "</tr>";
                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                for (var i = 0; i < PayDetail.length; i++) {

                    var p = PayDetail[i].EarnDeductionID;
                    var T = PayDetail[i].EarnDeductionType;
                    //alert(p); alert(T);

                    //" + (T == "E" ? "style='background-color:#5cf566;" : "style='background-color:#d4e7ee;'") + "

                    if (p == 9 || p == 3 || p == 4 || p == 5 || p == 7 || p == 18 || p == 22 || p == 19 || p == 22 || p == 39) {

                        html += "<tr id='EarnDeductionID" + PayDetail[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + PayDetail[i].EarnDeductionID + ", " + PayDetail[i].Amount + " );'></a></div></td>"
                                + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + PayDetail[i].EarnDeductionID + ");'></a></div></td>"
                                + "<td  class='tr' style='visibility:hidden;' >" + PayDetail[i].EarnDeductionID + "</td>"
                                + "<td  class='tr' >" + PayDetail[i].EarnDeduction + "</td>"
                                + "<td  class='tr' >" + parseFloat(PayDetail[i].Amount) + "</td>"
                        html += "</tr>";
                        html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                        noofrows++;
                        //alert(noofrows);
                    }

                    else {
                        //================================================
                        if (p == 14)
                        {
                            var SalMonth = $("#hdnSalMonth").val();
                            
                            if (SalMonth == "03") {
                                html += "<tr id='EarnDeductionID" + PayDetail[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                    + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + PayDetail[i].EarnDeductionID + ", " + PayDetail[i].Amount + " );'><img src='images/Edit.jpg' id='deleteEnquiryTokenDetID_" + PayDetail[i].EarnDeductionID + "' /></a></div></td>"
                                    + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + PayDetail[i].EarnDeductionID + ");'></a></div></td>"
                                    + "<td  class='tr' style='visibility:hidden;' >" + PayDetail[i].EarnDeductionID + "</td>"//style='visibility:hidden'
                                    + "<td  class='tr' >" + PayDetail[i].EarnDeduction + "</td>"
                                    + "<td  class='tr' >" + PayDetail[i].Amount + "</td>"
                                html += "</tr>";
                                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                            }
                            else {
                                html += "<tr id='EarnDeductionID" + PayDetail[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                    + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + PayDetail[i].EarnDeductionID + ", " + PayDetail[i].Amount + " );'></a></div></td>"
                                    + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + PayDetail[i].EarnDeductionID + ");'></a></div></td>"
                                    + "<td  class='tr' style='visibility:hidden;' >" + PayDetail[i].EarnDeductionID + "</td>"//style='visibility:hidden'
                                    + "<td  class='tr' >" + PayDetail[i].EarnDeduction + "</td>"
                                    + "<td  class='tr' >" + PayDetail[i].Amount + "</td>"
                                html += "</tr>";
                                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                            }
                        }
                        //================================================
                        else {
                            if (p == 2)
                            {
                                var payAmount = PayDetail[i].Amount; 
                                $("#hdnPayScaleAmount").val(payAmount); 
                            }

                            html += "<tr id='EarnDeductionID" + PayDetail[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + PayDetail[i].EarnDeductionID + ", " + PayDetail[i].Amount + " );'><img src='images/Edit.jpg' id='deleteEnquiryTokenDetID_" + PayDetail[i].EarnDeductionID + "' /></a></div></td>"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + PayDetail[i].EarnDeductionID + ", " + noofrows + " );'><img src='images/delete.png' id='deleteEnquiryTokenDetID_" + PayDetail[i].EarnDeductionID + "' /></a></div></td>"
                                        + "<td  class='tr' style='visibility:hidden;' >" + PayDetail[i].EarnDeductionID + "</td>"
                                        + "<td  class='tr' >" + PayDetail[i].EarnDeduction + "</td>"
                                        + "<td  class='tr' >" + parseFloat(PayDetail[i].Amount) + "</td>"
                            html += "</tr>";
                            html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                        }
                    }
                    if (T == "E") {
                        var amts = parseFloat(PayDetail[i]["Amount"]);
                        TotalEarn = TotalEarn + amts;
                    } else {
                        var amts = parseFloat(PayDetail[i]["Amount"]);
                        TotalDedu = TotalDedu + amts;
                    }
                    Net = parseFloat(TotalEarn) - parseFloat(TotalDedu);
                    
                    
                }
                html += "</table>";
                
                $('#lblTotalEarning').text(TotalEarn);
                $('#lblTotalDeduction').text(TotalDedu);
                $('#lblTotalNet').text(Net.toFixed(2));
                
                $("#divTable").empty().html(html);


            }

            var len = PayDetail.length;
            if (len == "") {
                var Total = $('#lblTotalEarning').text();
                var dudu = $('#lblTotalDeduction').text();
                var net = $('#lblTotalNet').text();
                TotalEarn = Total - Total;
                TotalDedu = dudu - dudu;
                Net = net - net;
                $('#lblTotalEarning').text(TotalEarn);
                $('#lblTotalDeduction').text(TotalDedu);
                $('#lblTotalNet').text(Net);
                
            } else
            { }
        }
    };
    this.SearchEmployee = function (EmployeeCode) {
        var B = window.empMaster.employeeForm;
        var fview = document.getElementById("#dv");
        //alert(JSON.stringify(fview));

        var C = $("#txtEmpCodeSearch").val();
        var E = "{EmployeeCode: " + $('#txtEmpCodeSearch').val() + "}";
        //alert(E);

        $.ajax({
            type: "POST",
            url: pageUrl + '/SearchEmployee',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d);

                //alert(JSON.stringify(data));
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }

        });
    };
    this.PageRedirection = function () {
        var B = window.empMaster.employeeForm;
        if (B.SMSResult == "SUCCESS") {
            alert("Hello");
            //window.location.href = "EnquiryComplete.aspx?EnquiryTokenID=" + B.enquiryMaster.EnquiryTokenID;
            window.location.href = "EmployeeMaster.aspx";

        }
    };
    this.AddDetails = function () {
        var B = {};
        B.employeeForm = window.empMaster.employeeForm;
        var E = "{\'employeeForm\':\'" + JSON.stringify(B.employeeForm) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/AddDetails',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.empMaster.employeeForm = JSON.parse(D.d);
                //window.blockPage.showEnquiryDetail(window.blockPage.blockForm.BlockDetail);
            },
            error: function (response) {
                alert(response.d);
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
    this.GetHouseRent = function () {
        $(".loading-overlay").show();
        var B = window.empMaster.employeeForm;
        var H = $("#ddlHouse").val();

        if (H != "") {
            var C = "{\'HouseID\':\'" + H + "\'}";

            $.ajax({
                type: "POST",
                url: pageUrl + '/GetHouseRent',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var data = JSON.parse(D.d);
                    var value = data[0].RentAmt;
                    if (data.length > 0) {
                        if (value != null) {
                            var E = $("#ddlHRACat").val();
                            if (E == 'L') {
                                $("#txtrentAmount").val(value);
                            } else {
                                $("#txtrentAmount").val(value);
                            }
                        } else {
                            $("#txtrentAmount").val(0);
                        }
                    }


                },
                error: function (response) {
                    alert('');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        } else {
            $("#txtrentAmount").val('');
            $(".loading-overlay").hide();
        }
    };
    this.SearchEmployeeTransfer = function () {
        $(".loading-overlay").show();
        var B = window.empMaster.employeeForm;

        var EFrom = $("#txtEffeFrom").val();
        var ETo = $("#txtEffeTo").val();

        var C = "{\'txtEffeFrom\':\'" + EFrom + "\', \'txtEffeTo\':\'" + ETo + "\'}";

        $.ajax({
            type: "POST",
            url: pageUrl + '/GetEmpTransferDetails',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var data = JSON.parse(D.d);
                if (data.length > 0) {

                    window.empMaster.ShowTransferEmployee(data);
                }
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
    this.ShowTransferEmployee = function (Detail) {
        var html = '';
        if (Detail != "undefined") {
            //alert(JSON.stringify(Detail));
            if (Detail.length > 0) {

                html += " <table align='center' border='2' cellpadding='5' width='98%'>" + "\n"
                          + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"
                //+ "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;'></td>"
                //+ "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;'></td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>EmpNo</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Emp Name</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Eff From</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Eff To</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Designation</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Sector</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Location</td>"
                          + "</tr>";
                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                for (var i = 0; i < Detail.length; i++) {

                    html += "<tr id='EmpNo_" + Detail[i].EmpNo + "'>"
                    //+ "<td class='tr'><div><a href='" + pageUrl + '/UpdateEmpDetail' + "' onClick='return window.empTransterMaster.updateEmpDetails(" + Detail[i].EmpNo + " );'><img src='images/Edit.jpg' id='deleteEnquiryTokenDetID_" + Detail[i].EmpNo + "' /></a></div></td>"
                    //+ "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + Detail[i].EmpNo + ");'><img src='images/delete.png' id='deleteEnquiryTokenDetID_" + Detail[i].EmpNo + "' /></a></div></td>"
                    //+ "<td  class='tr' style='width:210px;'>" + Detail[i].EmployeeID + "</td>"
                                + "<td  class='tr' style='width:80px;'>" + Detail[i].EmpNo + "</td>"
                                + "<td  class='tr' style='width:100px;'>" + Detail[i].EmpName + "</td>"
                                + "<td  class='tr' style='width:100px;'>" + Detail[i].InsertedOn + "</td>"
                                + "<td  class='tr' style='width:100px;'>" + Detail[i].ModifiedOn + "</td>"
                                + "<td  class='tr' style='width:100px;'>" + Detail[i].Designation + "</td>"
                                + "<td  class='tr' style='width:100px;'>" + Detail[i].SectorName + "</td>"
                                + "<td  class='tr' style='width:100px;'>" + Detail[i].CenterName + "</td>"
                    // + "<td  class='tr'>" + "<input type='text' id='txt_" + Detail[i].EmpNo + "' style='text-align:center;'  value='" + Detail[i].EDAmount + "' />" + "</td>" // onchange='return window.cooperativeMaster.ChangeValue(this)'

                    html += "</tr>";
                    html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                }
                html += "</table>";
                $("#divTables").empty().html(html);
            }
        }
    };
    this.showPayDetailonSuspended = function (PayDetail1) {
        var html = ''; var TotalEarn = 0; var TotalDedu = 0; var Net = 0; var noofrows = 0;

        if (PayDetail1 != "undefined") {
            var Array = [];
            //*******************************Supriyo Ghosh**************************************
            $.each(PayDetail1, function (index, value) {
                var objdata1 = {};
                if (value.EarnDeductionType == "E") {
                    objdata1.Amount = value.Amount;
                    objdata1.EarnDeduction = value.EarnDeduction;
                    objdata1.EarnDeductionID = value.EarnDeductionID;
                    objdata1.EarnDeductionType = value.EarnDeductionType;
                    Array.push(objdata1);

                }
            });
            $.each(PayDetail1, function (index, value) {
                var objdata2 = {};
                if (value.EarnDeductionType == "D") {
                    objdata2.Amount = value.Amount;
                    objdata2.EarnDeduction = value.EarnDeduction;
                    objdata2.EarnDeductionID = value.EarnDeductionID;
                    objdata2.EarnDeductionType = value.EarnDeductionType;
                    Array.push(objdata2);

                }

            });
            var PayDetail = Array;
            //**********************************************************************************

            if (PayDetail.length > 0) {
                html += " <table align='center' cellpadding='5' width='98%'>" + "\n"

                          + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"

                          + "<tr><td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Edit</td>"
                          + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Delete</td>"
                          + "<td class='th' style= 'background-color:lightblue;'></td>"
                          + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Earn/Deduction</td>"
                          + "<td class='th' style='background-color:lightblue;font-size:18px;font-weight:bold;'>Amount</td>"
                          + "</tr>";
                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                for (var i = 0; i < PayDetail.length; i++) {

                    var p = PayDetail[i].EarnDeductionID;
                    var T = PayDetail[i].EarnDeductionType;
                    //alert(p); alert(T);
 
                    html += "<tr id='EarnDeductionID" + PayDetail[i].EarnDeductionID + "' " + (T == "E" ? "style='background-color:#edf55c99;'" : "style='background-color:#ffbdf8ad;'") + ">"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/UpdatePayDetail' + "' onClick='return window.empMaster.updatePayDetail(" + PayDetail[i].EarnDeductionID + ", " + PayDetail[i].Amount + " );'><img src='images/Edit.jpg' id='deleteEnquiryTokenDetID_" + PayDetail[i].EarnDeductionID + "' /></a></div></td>"
                                        + "<td class='tr'><div><a href='" + pageUrl + '/DeletePayDetail' + "' onClick='return window.empMaster.removePayDetail(" + PayDetail[i].EarnDeductionID + ", " + noofrows + " );'><img src='images/delete.png' id='deleteEnquiryTokenDetID_" + PayDetail[i].EarnDeductionID + "' /></a></div></td>"
                                        + "<td  class='tr' style='visibility:hidden;' >" + PayDetail[i].EarnDeductionID + "</td>"
                                        + "<td  class='tr' >" + PayDetail[i].EarnDeduction + "</td>"
                                        + "<td  class='tr' >" + parseFloat(PayDetail[i].Amount) + "</td>"
                            html += "</tr>";
                            html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";

                    if (T == "E") {
                        var amts = parseFloat(PayDetail[i]["Amount"]);
                        TotalEarn = TotalEarn + amts; //alert(TotalEarn);
                    } else {
                        var amts = parseFloat(PayDetail[i]["Amount"]);
                        TotalDedu = TotalDedu + amts; //alert(TotalDedu);
                    }
                    Net = TotalEarn - TotalDedu; //alert(Net);
                }
                html += "</table>";
                //alert(TotalEarn); alert(TotalDedu); alert(Net);
                $('#lblTotalEarning').text(TotalEarn);
                $('#lblTotalDeduction').text(TotalDedu);
                $('#lblTotalNet').text(Net);
                $("#divTable").empty().html(html);

            }
            var len = PayDetail.length;
            if (len == "") {
                var Total = $('#lblTotalEarning').text();
                var dudu = $('#lblTotalDeduction').text();
                var net = $('#lblTotalNet').text();
                TotalEarn = Total - Total;
                TotalDedu = dudu - dudu;
                Net = net - net; 
                $('#lblTotalEarning').text(TotalEarn);
                $('#lblTotalDeduction').text(TotalDedu);
                $('#lblTotalNet').text(Net);
            } else { }
        }
    };
    this.UpdateSuspendedPayDetails = function () {
        
        var html = ''; var TotalEarn = 0; var TotalDedu = 0; var Net = 0; 
        var ErnID = $("#ddlEarnDeduction").val();
        var EarnDed = $('#ddlEarnDeduction').find('option:selected').text();
        var Amt = $("#txtAmount").val();
        var F = "{EarnDeductionID: " + ErnID + ", EarnDeduction: '" + EarnDed + "',  Amount: '" + Amt + "'}";
        //alert(F);
        $.ajax({
            type: "POST",
            url: pageUrl + '/AddSuspendedPayDetails',
            data: F,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d);
                if (data.length > 0) {
                    //alert(JSON.stringify(data));
                    window.empMaster.showPayDetailonSuspended(data);
                }
            }
        });

    };
    this.SavePopUpDetail = function () {

        var Status = $("#txtStatus").val();
        var OrderNo = $("#txtOrderNo").val();
        var OrderDate = $('#txtOrderDate').val();
        var Remarks = $("#txtRemarks").val();

        var F = "{Status: '" + Status + "', OrderNo: '" + OrderNo + "', OrderDate: '" + OrderDate + "',  Remarks: '" + Remarks + "'}";
        //alert(F);
        $.ajax({
            type: "POST",
            url: pageUrl + '/Save_StatusPopUpDetail',
            data: F,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d);
                     //alert(JSON.stringify(data));
                    if (data == "Saved")
                    {
                        $("#overlay").hide();
                        $("#txtOrderNo").val('');
                        $('#txtOrderDate').val('');
                        $("#txtRemarks").val('');
                    }
                    else
                    {
                        alert("Sorry ! Your Order Details are not Saved.\nPlease Try again.");
                    }
            }
        });

    };

};
function calculateDateofRetirement(selected) {
    var DOB = $('#txtDOB').val(); var DOR = ""; var DORYear = ""; var date = ""; var month = ""; var year = "";
    dorDate = ""; var dorMonth = ""; var dorYear = "";
    //alert(DOB);
    var a = DOB.split('/');
    for (var i = 0; i < a.length; i++) {
        date = a[0]; month = a[1]; year = a[a.length - 1];
    }
    DORYear = (parseInt(year) + parseInt(60)); //alert(DORYear);
    if (date == 1) {

        //alert(date); alert(month); alert(year);
        //var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var newdate = new Date(year, month - 1, date); //alert(newdate);
        var lastDay = new Date(newdate.getFullYear(), newdate.getMonth(), 0); //alert(lastDay.getMonth());
        dorDate = lastDay.getDate() < 10 ? '0' + lastDay.getDate() : lastDay.getDate(); //alert(dorDate);



        if (lastDay.getMonth() == 9)
            dorMonth = parseInt(lastDay.getMonth()) + 1;
        else
            dorMonth = lastDay.getMonth() < 10 ? '0' + (parseInt(lastDay.getMonth()) + 1) : (parseInt(lastDay.getMonth()) + 1); //alert(dorMonth);

        dorYear = lastDay.getFullYear();
        DOR = dorDate + '/' + dorMonth + '/' + DORYear;
        $("#txtDOR").val(DOR);
    } else {

        var newdate = new Date(year, month - 1, date); //alert(newdate);
        var lastDay = new Date(newdate.getFullYear(), newdate.getMonth() + 1, 0); //alert(lastDay);
        dorDate = lastDay.getDate() < 10 ? '0' + lastDay.getDate() : lastDay.getDate(); //alert(dorDate);

        var months = (parseInt(lastDay.getMonth()) + 1); //alert(months);
        if (months == 9)
            dorMonth = months < 10 ? '0' + months : months;
        else
            dorMonth = months < 10 ? '0' + months : months; //alert(dorMonth);


        dorYear = lastDay.getFullYear();
        DOR = dorDate + '/' + dorMonth + '/' + DORYear; //alert(DOR);
        $("#txtDOR").val(DOR);
    }


    if (DOB != "") {
        //var today = new Date(now.getYear(), now.getMonth(), now.getDate());
        var today = new Date();
        var yearNow = today.getFullYear(); //alert(yearNow);
        var monthNow = today.getMonth();
        var dateNow = today.getDate();

        var dob = new Date(DOB.substring(6, 10),
                           DOB.substring(3, 5) - 1,
                           DOB.substring(0, 2)
                           ); //alert(dob);

        var yearDob = dob.getFullYear(); //alert(yearDob);
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();
        //var birthyear = DOB.substring(6, 10);
        //var birthmonth = DOB.substring(3, 5) - 1;
        //var birthday = DOB.substring(0, 2); alert(birthday);

        var age = {};
        var ageString = "";
        var yearString = "";
        var monthString = "";
        var dayString = "";


        yearAge = yearNow - yearDob; //alert(yearAge);

        if (monthNow >= monthDob)
            var monthAge = monthNow - monthDob;
        else {
            yearAge--;
            var monthAge = 12 + monthNow - monthDob;
        }

        if (dateNow >= dateDob)
            var dateAge = dateNow - dateDob;
        else {
            monthAge--;
            var dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }

        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge
        };
        //alert(JSON.stringify(yearAge));

        $('#lblYears').show();
        $('#lblMonths').show();
        $('#lblDa').show();

        $('#lblYear').show();
        $('#lblMonth').show();
        $('#lblDay').show();

        $('#lblYear').text(age.years);
        $('#lblMonth').text(age.months);
        $('#lblDay').text(age.days);
    }
    else {
        $("#lblYears").hide();
        $("#lblMonths").hide();
        $("#lblDa").hide();

        $('#lblYear').hide();
        $('#lblMonth').hide();
        $('#lblDay').hide();
    }
    //$("#txtDOJ").datepicker("option", "minDate", selected)

}
function Validate_DOB_DOJ_PresentDOJ() {
    var DOB = $("#txtDOB").val();
    var DOJ = $("#txtDOJ").val();
    var C = "{DOB: '" + DOB + "',DOJ:'" + DOJ + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/Validate_DOB_DOJ',
        data: C,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = jQuery.parseJSON(D.d);
            var msg = t.MSg;
            var MsgCode = t.MsgCode;
            if (MsgCode=='0')
            {
                alert(msg);
                $("#txtDOJ").val('');
            }
        },
        error: function (response) {
            alert('');
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}
function Validate_DOJ_PresentDOJ() {
    var DOJ = $("#txtDOJ").val();
    var DOJDept = $("#txtDOJDept").val();
    var C = "{DOJ: '" + DOJ + "',DOJDept:'" + DOJDept + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/Validate__DOJ_DOJDept',
        data: C,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = jQuery.parseJSON(D.d);
            var msg = t.MSg;
            var MsgCode = t.MsgCode;
            if (MsgCode == '0') {
                alert(msg);
                $("#txtDOJDept").val('');
            }
        },
        error: function (response) {
            alert('');
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}














