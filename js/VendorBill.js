﻿// //Copy from vendorname to company name...
function CopyfromVendornametoCompanyname() {
    var copy = $("#txtVendorName").val();
    $("#txtCmpName").val(copy);
}


//autocomplete funtion
function AutoComplete_VendorName() {
  //  alert("Autocomplete function");
    $("#txtVendorNameSearch").autocomplete({
        source: function (request, response) {
            var VenName = $("#txtVendorNameSearch").val().trim();
            var W = "{'VenName':'" + VenName + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "VendorBillMaster.aspx/GET_AutoComplete_VendorName",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        //return {
                        //    label: item,
                        //    val: item,
                        //}
                        return {
                            label: item.split("|")[0],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        appendTo: "#autoContainerVendorNameSearch",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            $("#hdnVendorNameSearch").val(arr[1]);
            AutoSelect_companyNameAndPAN();        
            //return false;
        }
    }).click(function () {       
        $(this).data("autocomplete").search($(this).val());
    });
}
 
// company validation after 10th number digit enter..
function validationPANonkeypress() {
    var txtPAN = $("#txtPanNo").val();
    if (txtPAN.length == 10) {
        PANnumberfullVALIDATion();
    }
}
function PANnumberfullVALIDATion() {
    var isValid = false;
    var regex = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/;
    isValid = regex.test($("[id*=txtPanNo]").val());
    if (isValid == false) {
        var value = $('#txtPanNo').val();
        if (value == "" || value == " ") {
            var con = confirm("press [OK] to enter a PAN number or press [CANCEL] to exits from this form");
            if (con == true) {
                $('#txtPanNo').focus();
                return false;
            }
            else {
                $("#overlay").hide();
                return true;
            }
          //  alert("Enter a PAN number.");
          //  $('#txtPanNo').focus();
            return false;
        }
        else if (value.length != 10) {
            var remain = 10 - value.length;
            alert("You have missed"+remain+ "digit/digits of PAN number");
          //  $('#txtPanNo').focus();
            return false;
        }
        else {
            alert("You have entered a wrong PAN number");
          //  $('#txtPanNo').focus();
            return false;
        }
    }
    else {
        duplicate_check();
    }
}
// company validation on blur funtion....
$(document).ready(function () {
    $("#txtPanNo").blur(function (evt) {
        PANnumberfullVALIDATion();
    });
});
       
function duplicate_check() {
    var PANNO = $('#txtPanNo').val();
    var W = "{panno:'" + PANNO + "'}";
    $.ajax({
        type: "POST",
        url: 'VendorBillMaster.aspx/DuplicatePannumber_Checkings',
        data: W,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var item = jQuery.parseJSON(data.d);
            if (item == "Available") {
                alert(PANNO + " this PAN number already in record.. try another pan number");
               // $('#txtPanNo').focus();
                return false;
            }
            else {
                $("#txtVendorName").prop('disabled', false);
                $("#txtCmpName").prop('disabled', false);
                $("#txtVendorName").focus();
                return true;
            }
        }
    });
}
$(document).ready(function () {
    $("#txtPanNo").keyup(function () {
        var value = $("#txtPanNo").val();
        if (value.length != 10) {
            $("#txtVendorName").prop('disabled', true);
            $("#txtCompanyName").prop('disabled', true);
        }

    });

});

        // var VendorBillID = "";

        $(document).ready(function () {
            $.validator.addMethod("validEmail", function (value, element) {
                return this.optional(element) || value == value.match(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);//  /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
                // --                                    or leave a space here ^^
            }, "Not a Valid Email Address");
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });

            $(document).ready(function () {
                $.validator.addMethod("noquotes", function (value, element) {
                    return this.optional(element) || value == value.match(/^[^'"]*$/);//value == value.match(/'"/);
                    // --                                    or leave a space here ^^
                }, "No single or double quotes are allowed.");
                $(".DefaultButton").click(function (event) {
                    event.preventDefault();
                });
                AutoComplete_VendorName()
            });


           


        /*    $("#txtPanNo").blur(function (event) {
                ValidatePAN();
            });

            function ValidatePAN() {
                var letters = /^[A-Za-z]/;
                var value = $('#txtPanNo').val();

                var self = this;

                // alert(secval);
                //alert(thirdval);
                if (value.length != 10) {
                    alert("wrong pan no");
                    //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
                    //setTimeout(function () { self.focus(); }, 10);
                }
                else {
                    var firstval = value.substring(0, 5);
                    var secval = value.substring(5, 9);
                    var thirdval = value.substring(9, 10);
                }
                for (var i = 0; i <= firstval.length - 1; i++) {

                    var chkchar = firstval.substring(i, i + 1);
                    if (chkchar.match(letters)) {
                    }
                    //else {
                    //    //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
                    //    alert("wrong pan no");
                    //    //setTimeout(function () { self.focus(); }, 10);

                    //}
                }

                for (var i = 0; i <= secval.length - 1; i++) {

                    var chkchar1 = secval.substring(i, i + 1);
                    if ($.isNumeric(chkchar1)) {
                    }
                    //else {
                    //    //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
                    //    alert("wrong pan no");
                    //    //setTimeout(function () { self.focus(); }, 10);
                    //}
                }
                if (thirdval.match(letters)) {

                }
                else {
                    //$("#errmsg").html("Wrong PAN").show().fadeOut(3000);
                    alert("wrong pan no");
                    //setTimeout(function () { self.focus(); }, 10);
                }
            } */


            


            $('#txtBillDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true

            });
        });

$(document).ready(function () {
    $('#txtVendorBillDt').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true

    });
});

$(document).ready(function () {
    $('#cmdCancel').click(function () {
        window.location.href = "VendorBillMaster.aspx";
    });
});


function ClearVendorMast() {
    $("#txtVendorName").val("");
    $("#txtAddress").val("");
    $("#txtCompanyName").val("");
    $("#txtPanNo").val("");
    $("#txtEmail").val("");
    $("#txtPhone").val("");
    $("#txtCmpName").val("");
    $("#ddlBank").val("");
    $("#ddlBranch").val("");
    $("#txtIFSC").val("");
    $("#txtMICR").val("");
    $("#txtBankCode").val("");
    $("#ddlActiveFlag").val("");

}

function ClearVendorBillMast() {
    $("#txtLocationCd").val("");
    $("#txtBillNo").val("");
    $("#txtBillDate").val("");
    $("#ddlVendorName").val("");
    $("#txtVendorBillNo").val("");
    $("#txtVendorBillDt").val("");
    $("#txtCompanyName").val("");
    $("#txtCompanyPan").val("");
    $("#txtLocationDet").val("");
}

            
              

        
$(document).ready(function () {
    $('#cmdPopUpCancel').click(function () {
        // window.location.href = "VendorBillMaster.aspx";
        ClearVendorMast()
    });
});


$(document).ready(function () {
    $("#Close").click(function (e) {
        $("#overlay").hide();
                
        ClearVendorMast();
               
        PopulateMailPS();
               
    });
});



$(document).ready(function () {
    $("#cmdForm").click(function (e) {
        $("#overlay").show();
    });
});

        
       


$(document).ready(function () {
    $("#txtLocationCd").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: 'VendorBillMaster.aspx/FindLocationNumber',
                data: "{LocationNo:'" + request.term + "', SecID:'" + $("#ddlSector").val() + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var item = jQuery.parseJSON(data.d); //alert(item);
                    if (item != "NotAvailable") {
                        $("#txtLocationDet").val(item);
                        if (item != "") {

                        }
                    }
                    else {
                        alert("This is invalid Location No.");
                        $("#txtLocationCd").val("");
                    }

                }
            });


            //  }
        },
        minLength: 4
    });
});

//Start Search Keypress Coding Here
$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtLocationCd').keyup(function () {
        $('#tbl').find('tr:gt(0)').hide();
        var data = $('#txtLocationCd').val();
        var len = data.length;
        if (len > 0) {
            $('#tbl').find('tbody tr').each(function () {
                coldata = $(this).children().eq(2);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        }
        else {
            $('#tbl').find('tr:gt(0)').show();
        }
    });
});

$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
});


        


$(document).ready(function () {
    $("#cmdSave").click(function (event) {

        if ($('#txtLocationCd').val().trim() == '') {
            alert("Please Enter Location Number!");
            $('#txtLocationCd').focus();
            return false;
        }


        if ($('#txtLocationCd').val().length != 4) {
            alert("Location Code is Invalid.Please Enter Proper Location Code!");
            $('#txtLocationCd').focus();
            return false;
        }


        if ($('#txtBillNo').val().trim() == '') {
            alert("Please Enter Bill Number!");
            $('#txtBillNo').focus();
            return false;
        }

        if ($('#txtBillDate').val().trim() == '') {
            alert("Please Enter Bill Date!");
            $('#txtBillDate').focus();
            return false;
        }

        if ($('#txtVendorBillDt').val().trim() == '') {
            alert("Please Enter Vendor Bill Date!");
            $('#txtVendorBillDt').focus();
            return false;
        }

        if ($('#txtVendorBillNo').val().trim() == '') {
            alert("Please Enter Vendor Bill Number!");
            $('#txtVendorBillNo').focus();
            return false;
        }

        $("#form1").validate();
        $("#txtBillNo").rules("add", {
            noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },
        });
        $("#txtVendorBillNo").rules("add", {
            noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },
        });

        $("#txtBillNo").valid();
        $("#txtVendorBillNo").valid();
        //$("#txtAddress").valid();
        if (!$("#form1").valid()) {
            return false;
        }


        var LocationCode = $("#txtLocationCd").val();
        var BillNo = $("#txtBillNo").val();
        var BillDate = $("#txtBillDate").val();
        var VendorID = $("#ddlVendorName").val();
        var VendorBillNo = $("#txtVendorBillNo").val();
        var VendorBillDate = $("#txtVendorBillDt").val();
        var sectorid = $("#ddlSector").val();
        var VendorBillID = $("#hdnVendorID").val(); //alert(VendorBillID);

        var F = $("#cmdSave").val();
        //alert(F);
                
        if (F == 'Save') {

            var r = confirm("Do You Want to Save The Data?");
            //alert(2);
            if (r == true) {
                var E = "{LocationCode: '" + LocationCode + "', BillNo: '" + BillNo + "', BillDate: '" + BillDate + "', VendorID: '" + VendorID + "', VendorBillNo: '" + VendorBillNo + "', VendorBillDate: '" + VendorBillDate + "', sectorid: '" + sectorid + "'}";
                // alert(E);
                $.ajax({
                    type: "POST",
                    url: 'VendorBillMaster.aspx/Save_BillMaster',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (Data) {
                        var jsdata = JSON.parse(Data.d);
                        if (jsdata == "Saved") {
                            var r = confirm("Vendor Saved Successfully.");

                        }
                        ClearVendorBillMast()
                        window.location.href = "VendorBillMaster.aspx";
                    }
                });
            }
        }
        else if (F == 'Update') {
            var E = "{VendorBillID: '" + VendorBillID + "', LocationCode: '" + LocationCode + "', BillNo: '" + BillNo + "', BillDate: '" + BillDate + "', VendorID: '" + VendorID + "', VendorBillNo: '" + VendorBillNo + "', VendorBillDate: '" + VendorBillDate + "', sectorid: '" + sectorid + "'}";
            //alert(E);
            $.ajax({
                type: "POST",
                url: 'VendorBillMaster.aspx/Update_BillMaster',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));
                    if (jsdata == "Updated") {
                       // var r = confirm("Data is Updated Successfully.");
                        alert("Vendor Bill Updated Successfully.");
                        //if (r == true)
                            window.location.href = "VendorBillMaster.aspx";
                       // else
                            //window.location.href = "VendorBillMaster.aspx";
                    }
                    else {
                        alert("Vendor is not Updated Successfully.");
                    }
                    $("#txtLocationCd").val("");
                    $("#txtBillNo").val("");
                    $("#txtBillDate").val("");
                    $("#ddlVendorName").val("");
                    $("#txtVendorBillNo").val("");
                    $("#txtVendorBillDt").val("");
                    $("#txtCompanyName").val("");
                    $("#txtCompanyPan").val("");
                    $("#txtLocationDet").val("");


                }
            });
        }


    });
});

$(document).ready(function () {
    // $("#ddlPayScaleType").prop("disabled", false);
    $("#ddlSector").change(function () {
        var s = $('#ddlSector').val();
        if (s != 0) {
            if ($("#ddlSector").val() != "Please select") {
                var E = "{SecID: " + $('#ddlSector').val() + "}";
                //alert(E);
                var options = {};
                options.url = "EmployeeMaster.aspx/GetCenter";
                options.type = "POST";
                options.data = E;
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listCenter) {
                    var t = jQuery.parseJSON(listCenter.d);
                    var a = t.length;
                    $("#ddlCenter").empty();
                    if (a >= 0) {
                        $("#ddlCenter").append("<option value='0'>(Select Center)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                        });
                    }

                    $("#ddlCenter").prop("disabled", false);
                };
                options.error = function () { alert("Error in retrieving Center!"); };
                $.ajax(options);
            }
            else {
                $("#ddlCenter").empty();
                $("#ddlCenter").prop("disabled", true);
            }
        }
        else {
            $("#ddlCenter").empty();
            $("#ddlCenter").append("<option value=''>(Select Center)</option>")
        }
    });

});

//$(document).ready(function () {
//    $("#ddlVendorName").change(function () {
//        if ($("#ddlVendorName").val() != "") {
//            if ($("#ddlVendorName").val() != "Please select") {
//                var options = {};
//                options.url = "VendorBillMaster.aspx/Get_PanandCompanyName";
//                options.type = "POST";
//                options.data = "{VendorID: " + $('#ddlVendorName').val() + "}";
//                options.dataType = "json";
//                options.contentType = "application/json";
//                options.success = function (D) {
//                    var t = JSON.parse(D.d); //alert(JSON.stringify(t));
//                    var companyName = t[0].CompanyName;
//                    var companyPan = t[0].CompanyPAN;

//                    $("#txtCompanyName").val(companyName);
//                    $("#txtCompanyPan").val(companyPan);
//                };
//                options.error = function () { alert("Error retrieving BankName!"); };
//                $.ajax(options);
//            }
//            else {
//                $("#txtCompanyName").val('');
//                $("#txtCompanyPan").val('');
//            }
//        }
//        else {
//            $("#txtCompanyName").val('');
//            $("#txtCompanyPan").val('');
//        }
//    });
//});

//Select companyName and companyPan where vendorID=hdnVendorNameSearch
function AutoSelect_companyNameAndPAN() {
   // alert("AutoSelect_companyNameAndPAN()");
        if ($("#hdnVendorNameSearch").val() != "") {
            var options = {};
            options.url = "VendorBillMaster.aspx/Get_PanandCompanyName";
            options.type = "POST";
            options.data = "{VendorID: " + $('#hdnVendorNameSearch').val() + "}";
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (D) {
                var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                var companyName = t[0].CompanyName;
                var companyPan = t[0].CompanyPAN;

                $("#txtCompanyName").val(companyName);
                $("#txtCompanyPan").val(companyPan);
            };
            options.error = function () { alert("Error retrieving BankName!"); };
            $.ajax(options);
        }
        else {
            $("#txtCompanyName").val('');
            $("#txtCompanyPan").val('');
        }
    }
    


//This is for Popup Gridview delete button Click
// $(document).ready(function () {
$('#worktype a').live('click', function () {
    flag = $(this).attr('id');
    var VendorBillID = $(this).attr('itemid');
    $('#hdnVendorID').val(VendorBillID);
    var E = "{VendorBillID: '" + VendorBillID + "'}"; //alert(E);
    $.ajax({
        type: "POST",
        url: 'VendorBillMaster.aspx/Update_VendorDetail',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var data = jQuery.parseJSON(D.d); //alert(JSON.stringify(data));
            var CenterCode = data[0].CenterID; //alert(CenterCode)
            var CenterName = data[0].CenterName;
            var BillNo = data[0].BillNo;
            var BillDate = data[0].BillDate;
            var VendorName = data[0].VendorID;
            var CompanyName = data[0].CompanyName;
            var CompanyPan = data[0].CompanyPAN;
            var VendorBillNo = data[0].VendorBillNo;
            var VendorBillDate = data[0].VendorBillDate;



            $('#txtLocationCd').val(CenterCode);
            $('#txtLocationDet').val(CenterName);
            $('#txtBillNo').val(BillNo);
            $('#txtBillDate').val(BillDate);
            $('#ddlVendorName').val(VendorName);
            $('#txtCompanyName').val(CompanyName);
            $('#txtCompanyPan').val(CompanyPan);
            $('#txtVendorBillNo').val(VendorBillNo);
            $('#txtVendorBillDt').val(VendorBillDate);
            //alert("Ping");
            $("#cmdSave").attr('value', 'Update');
            //document.getElementById("cmdSave").attributes()

        }
    });

});
//});



      


//This is for Popup Gridview delete button Click
$(document).ready(function () {
    $('#del a').live('click', function () {
        var result = confirm("Are you sure you want to delete this ?");
        if (result == true) {
            flag = $(this).attr('id');
            var VendorBillID = $(this).attr('itemid');
            var E = "{VendorBillID: '" + VendorBillID + "'}"; //alert(E);
            $.ajax({
                type: "POST",
                url: 'VendorBillMaster.aspx/Delete_VendorDetail',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {

                    window.location.href = "VendorBillMaster.aspx";

                }
            });
        }
    });
});
$(document).ready(function () {
    $("#txtLocationCd").keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
    (event.keyCode == 65 && event.ctrlKey === true) ||
    (event.keyCode >= 35 && event.keyCode <= 39)) {

            return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
                $("#Span8").html("Digits Only").show().fadeOut(1500);
                return false;

            }
        }
    });
});
       



$(document).ready(function () {
    $("#txtPhone").keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 8 ||
    (event.keyCode == 65 && event.ctrlKey === true) ||
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            //alert(parseInt($('#txtPhone').val()).toString().length);
            if ((event.keyCode != 8)) {
                if (parseInt($('#txtPhone').val()).toString().length < 10) {
                    alert("Please insert a valid mobile no.");
                    $('#txtPhone').focus();
                    return false;
                }
                return;
            } else {
                return true;
            }
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) ) {
                event.preventDefault();
                $("#Span3").html("Digits Only").show().fadeOut(1500);
                return false;

            }
        }
                
    });
});




        
        
$(document).ready(function () {
    $("#txtBankCode").keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
    (event.keyCode == 65 && event.ctrlKey === true) ||
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            //if ($('#txtPhone').length > 10 || $('#txtPhone').length < 10) {
            //    alert("Please insert a valid mobile no.");
            //}
            return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
                $("#Span3").html("Digits Only").show().fadeOut(1500);
                return false;

            }
        }
    });
});


$(document).ready(function () {
    $("#txtVendorName").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 32 || (event.keyCode >47 && event.keyCode<58 )||
            // Allow: Ctrl+A
    (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 65 || event.keyCode > 90) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                return false;

            }
        }
    });
});




$(document).ready(function () {
    $("#txtCompanyName").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
    (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode <= 65 || event.keyCode >= 90) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                return false;

            }
        }
    });
});


           

$(document).ready(function () {
    $("#ddlBank").change(function () {
        if ($("#ddlBank").val() != "" && $("#ddlBank").val() != null) {
            $("#txtIFSC").val('');
            $("#txtMICR").val('');
            //$("#txtBankCode").val('');
            $("#txtBankCode").prop("disabled", true);
            if ($("#ddlBank").val() != "Please select") {
                var options = {};
                options.url = "VendorBillMaster.aspx/BindBranch";
                options.type = "POST";
                options.data = "{BankID: " + $('#ddlBank').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listbranch) {
                    var t = jQuery.parseJSON(listbranch.d);
                    var a = t.length;
                    $("#ddlBranch").empty();
                    $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                    if (a > 0) {
                        $.each(t, function (key, value) {
                            $("#ddlBranch").append($("<option></option>").val(value.BranchID).html(value.Branch));
                        });
                    }


                    $("#ddlBranch").prop("disabled", false);
                };
                options.error = function () { alert("Error retrieving BankName!"); };
                $.ajax(options);
            }
            else {
                $("#ddlBranch").empty();
                $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                $("#ddlBranch").prop("disabled", true);
                $("#txtIFSC").val('');
                $("#txtMICR").val('');
            }
        }
        else {
            $("#ddlBranch").empty();
            $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
            $("#ddlBranch").prop("disabled", true);
            $("#txtIFSC").val('');
            $("#txtMICR").val('');
        }
    });



});
$(document).ready(function () {
    $("#ddlBranch").change(function () {
        var C = "{BranchID: " + $('#ddlBranch').val() + "}"; //alert(C);
        $.ajax({
            type: "POST",
            url: 'VendorBillMaster.aspx/GetIFSCandMICR',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = jQuery.parseJSON(D.d);
                // alert(JSON.stringify(t));
                var ifsc = t[0].IFSC;
                var micr = t[0].MICR;
                $("#txtIFSC").val(ifsc);
                $("#txtMICR").val(micr);

                var branchID = $('#ddlBranch').val(); //alert(branchID);
                $('#txtBranchID').val(branchID);

                $("#txtBankCode").prop("disabled", false);
                $('#txtBankCode').focus();
                //return false;
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });
});

$(document).ready(function () {
    $("#cmdPopUpSave").click(function (event) {

        if ($('#txtVendorName').val().trim() == '') {
            alert("Please Enter Vendor Name!");
            $('#txtVendorName').focus();
            return false;
        }

        if ($('#txtCmpName').val().trim() == '') {
            alert("Please Enter Company Name!");
            $('#txtCmpName').focus();
            return false;
        }

        if ($('#txtPanNo').val().trim() == '') {
            alert("Please Enter Pan No!");
            ValidatePAN();
            $('#txtPanNo').focus();
            return false;
        }

        var value = $('#txtPanNo').val();
        if (value.length != 10) {
            alert("wrong pan no");
            $('#txtPanNo').focus();
            return false;
        }
                    
        if ($('#ddlBank').val().trim() == '') {
            alert("Please Select The Bank Name!");
            $('#ddlBank').focus();
            return false;
        }

        if ($('#ddlBranch').val().trim() == '') {
            alert("Please Select The Branch Name!");
            $('#ddlBranch').focus();
            return false;
        }



        if ($('#txtBankCode').val().trim() == '') {
            alert("Please Enter Bank  Account Number!");
            $('#txtBankCode').focus();
            return false;
        }


        if (parseInt($('#txtPhone').val()).toString().length < 10) {
            alert("Please insert a valid mobile no.");
            $('#txtPhone').focus();
            return false;
        }


                  
        $("#form1").validate();
        $("#txtEmail").rules("add", {
            validEmail: true, messages: { validEmail: "Please enter valid Email ID" },
        });

        $("#txtAddress").rules("add", {
            noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },
        });
        $("#txtCmpName").rules("add", {
            noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },

        });
        //$("#txtEmail").rules("add", {
        //    noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },

        //}); s
        //$("#txtBillNo").rules("add", {
        //    noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },

        //});
                    
                    
        $("#txtEmail").valid();
        $("#txtAddress").valid();
        $("#txtAddress").valid();
        if (!$("#form1").valid()) {
            return false;
        }


        // $("#form1").validate();
                   

        //$("#txtPanNo").rules("add", {
        //    noquotes: true, messages: { noquotes: "No single or double quotes are allowed." },

        //});


                   
        var VendorName = $("#txtVendorName").val();
        var Address = $("#txtAddress").val();
        var CompanyName = $("#txtCmpName").val();
        var PanNo = $("#txtPanNo").val();
        var Email = $("#txtEmail").val();
        var Phone = $("#txtPhone").val();
        var BranchDrp = $("#ddlBranch").val();
        var BranchId = $("#txtBranchID").val();
        var BankCode = $("#txtBankCode").val();
        var ActiveFlag = $("#ddlActiveFlag").val();
                    



        //var VendorBillID = $("#hdnVendorID").val(); //alert(VendorBillID);

        var F = $("#cmdSave").val();
        //alert(F);

        //if (F == 'Save') {
        var E = "{VendorName: '" + VendorName + "', Address: '" + Address + "', CompanyName: '" + CompanyName + "', PanNo: '" + PanNo + "', Email: '" + Email + "', Phone: '" + Phone + "', BranchDrp: '" + BranchDrp + "', BankCode: '" + BankCode + "', ActiveFlag: '" + ActiveFlag + "'}";
      //  alert(E);
        $.ajax({
            type: "POST",
            url: 'VendorBillMaster.aspx/Save_VendorMaster',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));
                           
                if (jsdata == "A")
                {
                    alert("Contact To Administartor");
                }
                else if (jsdata == "S")
                {
                    alert("Message Saved Succesfully");
                    ClearVendorMast()
                }

                else if (jsdata == "D")
                {
                    alert("This is Duplicate Pan No.Kindly Enter Proper Pan No");
                    $('#txtPanNo').focus();
                    return false;
                }
                //else
                //{
                //    htmlContent = "Unknown Error".ToJSON();
                //}
                            
            }
                         

            // }
        });
    });
});
// });

//$(document).ready(function () {
function PopulateMailPS () {
    //$("#ddlVendorName").prop('disabled', true);

    //if ($('#ddlVendorName').val() != "") {
    //    $('#ddlVendorName').append('<option selected="selected" value="">Please select</option>');
    //}
    // else {
    // $('#ddlVendorName').empty().append('<option selected="selected" value="">Loading...</option>');
    $.ajax({
        type: "POST",
        //async: false,
        url:'VendorBillMaster.aspx/populatePS',
        //   data: '{VendorName: ' + $('#ddlVendorName').val() + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success:OnMailPSPopulated,
        failure: function (response) {
            // alert(response.d);
        }

                        
    });
}
// }


function OnMailPSPopulated(response) {
    PopulateControl(response.d, $("#ddlVendorName"));
    //alert(response.d);
};


this.PopulateControl = function (list, control) {
    //alert(JSON.stringify(list));
    if (list.length > 0) {
        control.removeAttr("disabled");
        control.empty().append('<option selected="selected" value="">Please select</option>');
        $.each(list, function () {
            control.append($("<option></option>").val(this['Value']).html(this['Text']));
        });
    }
    else {
        control.empty().append('<option selected="selected" value="-1">Not available<option>');
    }
};



