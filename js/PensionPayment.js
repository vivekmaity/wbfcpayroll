

var TotalPaymentAmt = 0; var isAddorNot = 0;


//This is on Page Load for hide and show Gridview Image
$(function () {
    $.ajax({
        type: "POST",
        url: 'PensionPaymentDetail.aspx/Show_FailorSuccessImage',
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var DetailTable = JSON.parse(D.d); //alert(JSON.stringify(DetailTable));
            if (DetailTable.length > 0)
            {
                for (var j = 0; j < DetailTable.length; j++)
                {
                    var PEmpNo = DetailTable[j].EmpNo; //alert(PEmpNo);
                    var PaymentType = DetailTable[j].PaymentType; //alert(PaymentType);

                    var grid = document.getElementById("gvPensionPaymentDetail");
                    for (var row = 1; row < grid.rows.length; row++) {
                        var GridFrom_Cell = grid.rows[row].cells[0];
                        var lEmpNo = GridFrom_Cell.textContent.toString(); 

                        var lre = /^\s*/;
                        var rre = /\s*$/;
                        lEmpNo = lEmpNo.replace(lre, "");
                        lEmpNo = lEmpNo.replace(rre, "");
                      
                        if (lEmpNo == PEmpNo) {
                            //alert("MATCHED");
                            for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                                if (PaymentType == "C")
                                {
                                    //alert("CVP");
                                    var fail = $("img[id*=failCVP]");
                                    $(fail[row1]).hide();

                                    var success = $("img[id*=successCVP]");
                                    $(success[row1]).show();
                                }
                                else if (PaymentType == "G")
                                {
                                    //alert("GRATUIPTY");
                                    var fail = $("img[id*=failGratuity]"); 
                                    $(fail[row1]).hide();

                                    var success = $("img[id*=successGratuity]"); 
                                    $(success[row1]).show();

                                }
                                else if (PaymentType == "A") {
                                    //alert("Arrear");
                                    var fail = $("img[id*=failArrear]");
                                    $(fail[row1]).hide();

                                    var success = $("img[id*=successArrear]");
                                    $(success[row1]).show();

                                }
                            }
                        }
                    }
                }
            }
        }
    });
});

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$(document).ready(function () {
    $("[id*=successCVP]").hide();
    $("[id*=successGratuity]").hide();
    $("[id*=successArrear]").hide();
    $("#btnSave").hide();
    $("#spnTotal").hide();
});

$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
});

$(document).ready(function () {
    $("#Close").click(function (e) {
        $("#overlay").hide();
    });
});

//This is to Set Date on Popup Payment Date
$(document).ready(function () {
    $('#txtPaymentDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            var aa = $('#txtPaymentDate').val();
            var a = aa.split('/');
            for (var i = 0; i < a.length; i++) {
                date = a[0]; month = a[1]; year = a[a.length - 1];
            }
            $('#txtPaymentDate').val("01" + "/" + month + "/" + year);

        }
    });
});

//This is for checkBox Selection or DeSelection on CVP
$(document).ready(function () {
    $("[id*=chkPropersitionCVP]").live("click", function () {
        if ($(this).is(":checked")) {
            $("input[id*='chkPropersitionCVP']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
            $(this).attr("checked", true);
            var EmpNo = $(this).closest('tr').find('td:eq(0)').text();
            var PaymentType = $(this).closest('tr').find('td:eq(1)').text();

            var lre = /^\s*/;
            var rre = /\s*$/;
            EmpNo = EmpNo.replace(lre, "");
            EmpNo = EmpNo.replace(rre, "");

            var lre = /^\s*/;
            var rre = /\s*$/;
            PaymentType = PaymentType.replace(lre, "");
            PaymentType = PaymentType.replace(rre, "");

            var grid = document.getElementById("gvPensionPaymentDetail");
            for (var row = 1; row < grid.rows.length; row++) {
                var GridFrom_Cell = grid.rows[row].cells[0];
                var lEmpNo = GridFrom_Cell.textContent.toString(); 

                var lre = /^\s*/;
                var rre = /\s*$/;
                lEmpNo = lEmpNo.replace(lre, "");
                lEmpNo = lEmpNo.replace(rre, "");


                if (lEmpNo == EmpNo) {
                    for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                        var ValPOQty = $("input[id*=txtCVP]");
                        var TotalAmount= ValPOQty[row1].value;
                    }
                }
            }

            $("#txtTotalAmount").val(TotalAmount);
            $("#txtPaymentType").val(PaymentType);
            $("#txtEmpNo").val(EmpNo);
            $("#txtPaymentDate").val('');


            ShowSavedPayDetails(EmpNo, TotalAmount, PaymentType);

            $("#overlay").show();

            $("input[id*='chkPropersitionGratuity']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });

            $("input[id*='chkPropersitionArrear']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
        }
    });
});

//This is for checkBox Selection or DeSelection on Gratuity
$(document).ready(function () {
    $("[id*=chkPropersitionGratuity]").live("click", function () {
        if ($(this).is(":checked")) {
            $("input[id*='chkPropersitionGratuity']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
            $(this).attr("checked", true);
            var EmpNo = $(this).closest('tr').find('td:eq(0)').text();
            var PaymentType = $(this).closest('tr').find('td:eq(1)').text();

            var lre = /^\s*/;
            var rre = /\s*$/;
            EmpNo = EmpNo.replace(lre, "");
            EmpNo = EmpNo.replace(rre, "");

            var lre = /^\s*/;
            var rre = /\s*$/;
            PaymentType = PaymentType.replace(lre, "");
            PaymentType = PaymentType.replace(rre, "");

            var grid = document.getElementById("gvPensionPaymentDetail");
            for (var row = 1; row < grid.rows.length; row++) {
                var GridFrom_Cell = grid.rows[row].cells[0];
                var lEmpNo = GridFrom_Cell.textContent.toString();

                var lre = /^\s*/;
                var rre = /\s*$/;
                lEmpNo = lEmpNo.replace(lre, "");
                lEmpNo = lEmpNo.replace(rre, "");


                if (lEmpNo == EmpNo) {
                    for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                        var ValPOQty = $("input[id*=txtFinalGratuity]");
                        var TotalAmount = ValPOQty[row1].value;
                    }
                }
            }

            $("#txtTotalAmount").val(TotalAmount);
            $("#txtPaymentType").val(PaymentType);
            $("#txtEmpNo").val(EmpNo);
            $("#txtPaymentDate").val('');

            ShowSavedPayDetails(EmpNo, TotalAmount, PaymentType);
            $("#overlay").show();

            $("input[id*='chkPropersitionCVP']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });

            $("input[id*='chkPropersitionArrear']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
        }
    });
});

//This is for checkBox Selection or DeSelection on Arrear
$(document).ready(function () {
    $("[id*=chkPropersitionArrear]").live("click", function () {
        if ($(this).is(":checked")) {
            $("input[id*='chkPropersitionArrear']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
            $(this).attr("checked", true);
            var EmpNo = $(this).closest('tr').find('td:eq(0)').text();
            var PaymentType = $(this).closest('tr').find('td:eq(1)').text();

            var lre = /^\s*/;
            var rre = /\s*$/;
            EmpNo = EmpNo.replace(lre, "");
            EmpNo = EmpNo.replace(rre, "");

            var lre = /^\s*/;
            var rre = /\s*$/;
            PaymentType = PaymentType.replace(lre, "");
            PaymentType = PaymentType.replace(rre, "");

            var grid = document.getElementById("gvPensionPaymentDetail");
            for (var row = 1; row < grid.rows.length; row++) {
                var GridFrom_Cell = grid.rows[row].cells[0];
                var lEmpNo = GridFrom_Cell.textContent.toString();

                var lre = /^\s*/;
                var rre = /\s*$/;
                lEmpNo = lEmpNo.replace(lre, "");
                lEmpNo = lEmpNo.replace(rre, "");


                if (lEmpNo == EmpNo) {
                    for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                        var ValPOQty = $("input[id*=txtArrearPension]");
                        var TotalAmount = ValPOQty[row1].value;
                    }
                }
            }

            $("#txtTotalAmount").val(TotalAmount);
            $("#txtPaymentType").val(PaymentType);
            $("#txtEmpNo").val(EmpNo);
            $("#txtPaymentDate").val('');


            ShowSavedPayDetails(EmpNo, TotalAmount, PaymentType);
            $("#overlay").show();

            $("input[id*='chkPropersitionCVP']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });

            $("input[id*='chkPropersitionGratuity']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
        }
    });
});

//This is for Popup Add button Click
$(document).ready(function () {
    $("#btnAdd").live("click", function () {

        var dateDate = ""; var monthMonth = ""; var yearYear = ""; var TotalDate = "";
        var date = ""; var month = ""; var year = "";

        var TotalAmt = $("#txtTotalAmount").val();
        var PaymentType = $("#txtPaymentType").val();
        var ProportionAmt = $("#txtProportionAmt").val();
        //var ItaxAmt = $("#txtItax").val();
        var PaymentDate = $("#txtPaymentDate").val();


        if (ProportionAmt == "")
        {
            $("#txtProportionAmt").focus();
            return false;
        }

        if (PaymentDate == "") {
            $("#txtPaymentDate").focus();
            return false;
        }

        if (parseInt(ProportionAmt) > parseInt(TotalAmt)) {
            alert("Enter Proportion Amount should be not greater than Total Gross Amount.");
            $("#txtProportionAmt").focus();
            return false;
        }

        //if (parseInt(ProportionAmt) > parseInt(TotalAmt)) {
        //    alert("Enter Proportion Amount should be not greater than Total Gross Amount.");
        //    $("#txtProportionAmt").focus();
        //    return false;
        //}

        //if (parseInt(ItaxAmt) > parseInt(TotalAmt)) {
        //    alert("Enter Itax Amount should be not greater than Total Gross Amount.");
        //    $("#txtProportionAmt").focus();
        //    return false;
        //}

        var today = new Date();

        var a = PaymentDate.split('/');
        for (var i = 0; i < a.length; i++) {
            date = a[0]; month = a[1]; year = a[a.length - 1];
        }
        var newdate = new Date(year, month - 1, date);

        if (newdate < today) {
            alert("Sorry ! Invalid Date. Please Select greater Date from Today.");
            $("#txtPaymentDate").focus();
            return false;
        }
        else {
            var E = "{ProportionAmt: '" + ProportionAmt + "',  PaymentDate: '" + PaymentDate + "'}";
            $.ajax({
                type: "POST",
                url: 'PensionPaymentDetail.aspx/Add_PaymentDetail',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $("#txtProportionAmt").val('');
                    $("#txtPaymentDate").val('');
                    $('#tblPaymentAdd').html(D.d);
                    HideDeleteButton();

                    CountRows();
                    CountTotal();
                    //$("#btnSave").show();
                    //$("#spnTotal").show();
                    //$("#lblTotal").show();

                    var grid = document.getElementById("grvAddPaymentDetail");
                    for (var row = 1; row < grid.rows.length; row++) {
                        var GridFrom_Cell = grid.rows[row].cells[1];
                        var GridFrom_Cell2 = grid.rows[row].cells[2];

                        var lre = /^\s*/;
                        var rre = /\s*$/;
                        PropTotal = PropTotal.replace(lre, "");
                        PropTotal = PropTotal.replace(rre, "");

                    }
                    var totalbothAmt = parseInt(PropTotal);
                    $("#lblTotal").text(totalbothAmt);
                }
            });
        }
    });
});

//This is for Popup Save button Click
$(document).ready(function () {
    $("#btnSave").live("click", function () {
        var TotalAmt = $("#txtTotalAmount").val();

        var grid = document.getElementById("grvAddPaymentDetail");
        for (var row = 1; row < grid.rows.length; row++) {
            var GridFrom_Cell = grid.rows[row].cells[1];
            var GridFrom_Cell2 = grid.rows[row].cells[2];

            var PropTotal = GridFrom_Cell.textContent.toString();

            var lre = /^\s*/;
            var rre = /\s*$/;
            PropTotal = PropTotal.replace(lre, "");
            PropTotal = PropTotal.replace(rre, "");

        }
        

        var totalbothAmt = parseInt(PropTotal);

        if (parseInt(totalbothAmt) == parseInt(TotalAmt)) {
            var PayType = "";
            var EmpNo = $("#txtEmpNo").val();
            var PaymentType = $("#txtPaymentType").val();
            if (PaymentType == "CVP")
                PayType = "C";
            else if (PaymentType == "Gratuity")
                PayType = "G";
            else
                PayType = "A";

            var E = "{EmpNo: '" + EmpNo + "', PaymentType: '" + PayType + "'}";
            $.ajax({
                type: "POST",
                url: 'PensionPaymentDetail.aspx/Save_PaymentDetail',
                data:E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = JSON.parse(D.d);
                    if (t == "Inserted")
                    {
                        $.ajax({
                            type: "POST",
                            url: 'PensionPaymentDetail.aspx/Blank_PaymentDetail',
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#tblPaymentAdd').html(D.d);

                                $("#spnTotal").hide();
                                $("#lblTotal").hide();
                                $("#overlay").hide();

                            }
                        });

                        HideShowImages();
                        UncheckCheckbox();
                    }
                }
            });
        }
        else {
            alert("Sorry ! You can not Save this because your total Amount is not Equal.");
            return false;
        }
    });
});

//This is for Popup Reset button Click
$(document).ready(function () {
    $("#btnReset").click(function (e) {
        $("#txtProportionAmt").val('');
        $("#txtPaymentDate").val('');
    });
});

//This is for Popup Gridview delete button Click
$(document).ready(function () {
    $('#worktype a').live('click', function () {
        var result = confirm("Are you sure you want to delete this ?");
        if (result == true) {
            flag = $(this).attr('id');
            var SlNo = $(this).attr('itemid');
            var E = "{SlNo: '" + SlNo + "'}"; //alert(E);
            $.ajax({
                type: "POST",
                url: 'PensionPaymentDetail.aspx/Delete_PaymentDetail',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#tblPaymentAdd').html(D.d);
                    HideDeleteButton();
                    CountRows();
                    CountTotal();
                }
            });
        }
    });
});

//This is for Popup Reset button Click
$(document).ready(function () {
    $('#cmdCancel').live('click', function () {
        window.location.href = "PensionPaymentDetail.aspx";
    });
});

//This is for Employee TextBox text Change autocomplete event
$(document).ready(function () {
    $("#txtEmpNoSearch").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: 'PensionPaymentDetail.aspx/Find_PensionerPaymentDetailOnSearch',
                data: "{EmpNo:'" + request.term + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $('#gvPensionDetail').html(data.d);
                    HideShowImagesOnSearch();
                }
            });
        },
        minLength: 1
    });
});


function CountRows() {
    $.ajax({
        type: "POST",
        url: 'PensionPaymentDetail.aspx/CountAdd_PaymentDetail',
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = JSON.parse(D.d);
            if (t == "NoAvailable") {
                $('#btnSave').hide();
                $("#spnTotal").hide();
                $("#lblTotal").text('');
                $("#lblTotal").hide();
            }
            else {
                $('#btnSave').show();
                $("#spnTotal").show();
                $("#lblTotal").show();
            }
        }
    });
}

function CountTotal() {
    $.ajax({
        type: "POST",
        url: 'PensionPaymentDetail.aspx/CountTotal_PaymentDetail',
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = JSON.parse(D.d);
            if (t != "NoAvailable") {
                $("#spnTotal").show();
                $("#lblTotal").show();
                $("#lblTotal").text(t);
            }
        }
    });
}

function HideShowImages()
{
    $(function () {
        $.ajax({
            type: "POST",
            url: 'PensionPaymentDetail.aspx/Show_FailorSuccessImage',
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var DetailTable = JSON.parse(D.d); //alert(JSON.stringify(DetailTable));
                if (DetailTable.length > 0) {
                    for (var j = 0; j < DetailTable.length; j++) {
                        var PEmpNo = DetailTable[j].EmpNo; //alert(PEmpNo);
                        var PaymentType = DetailTable[j].PaymentType; //alert(PaymentType);

                        var grid = document.getElementById("gvPensionPaymentDetail");
                        for (var row = 1; row < grid.rows.length; row++) {
                            var GridFrom_Cell = grid.rows[row].cells[0];
                            var lEmpNo = GridFrom_Cell.textContent.toString();

                            var lre = /^\s*/;
                            var rre = /\s*$/;
                            lEmpNo = lEmpNo.replace(lre, "");
                            lEmpNo = lEmpNo.replace(rre, "");

                            if (lEmpNo == PEmpNo) {
                                //alert("MATCHED");
                                for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                                    if (PaymentType == "C") {
                                        //alert("CVP");
                                        var fail = $("img[id*=failCVP]");
                                        $(fail[row1]).hide();

                                        var success = $("img[id*=successCVP]");
                                        $(success[row1]).show();
                                    }
                                    else if (PaymentType == "G") {
                                        //alert("GRATUIPTY");
                                        var fail = $("img[id*=failGratuity]");
                                        $(fail[row1]).hide();

                                        var success = $("img[id*=successGratuity]");
                                        $(success[row1]).show();

                                    }
                                    else if (PaymentType == "A") {
                                        //alert("Arrear");
                                        var fail = $("img[id*=failArrear]");
                                        $(fail[row1]).hide();

                                        var success = $("img[id*=successArrear]");
                                        $(success[row1]).show();

                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    });
}

function UncheckCheckbox()
{
    $("input[id*='chkPropersitionCVP']:checkbox").each(function (index) {
        if ($(this).is(":checked")) {
            $(this).attr("checked", false);
        }
    });

    $("input[id*='chkPropersitionGratuity']:checkbox").each(function (index) {
        if ($(this).is(":checked")) {
            $(this).attr("checked", false);
        }
    });

    $("input[id*='chkPropersitionArrear']:checkbox").each(function (index) {
        if ($(this).is(":checked")) {
            $(this).attr("checked", false);
        }
    });
}

function HideDeleteButton() {
    var grid = document.getElementById("grvAddPaymentDetail");
    for (var row = 1; row < grid.rows.length; row++) {
        var GridFrom_Cell = grid.rows[row].cells[4];
        var GridVoucherNo = GridFrom_Cell.textContent.toString();

        var lre = /^\s*/;
        var rre = /\s*$/;
        GridVoucherNo = GridVoucherNo.replace(lre, "");
        GridVoucherNo = GridVoucherNo.replace(rre, "");

        if (GridVoucherNo != "") {
            for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                var del = $("[id*=delete]");
                $(del[row1]).hide();
            }
        }
    }
}

function ShowSavedPayDetails(EmpNo, TotalAmount, PaymentType) {
    var E = "{EmpNo: '" + EmpNo + "', PaymentType: '" + PaymentType + "'}";
    $.ajax({
        type: "POST",
        url: 'PensionPaymentDetail.aspx/Show_PaymentDetailbyEmpNo',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $("#tblPaymentAdd").empty().html(D.d);
            CountRows();
            CountTotal();
            HideDeleteButton();


           //$.ajax({
            //    type: "POST",
            //    url: 'PensionPaymentDetail.aspx/CountAdd_PaymentDetail',
            //    cache: false,
            //    contentType: "application/json; charset=utf-8",
            //    success: function (D) {
            //        var t = JSON.parse(D.d);
            //        if (t == "NoAvailable") {
            //            $('#btnSave').hide();
            //            $("#spnTotal").hide();
            //            $("#lblTotal").text('');
            //            $("#lblTotal").hide();
            //        }
            //        else {
            //            $("#btnSave").show();
            //            $("#spnTotal").show();
            //            $("#lblTotal").text(TotalAmount);
            //            $("#lblTotal").show();
            //        }
            //    }
            //});
        }
    });
}

function HideShowImagesOnSearch() {
    $(function () {
        $.ajax({
            type: "POST",
            url: 'PensionPaymentDetail.aspx/Show_FailorSuccessImage',
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var DetailTable = JSON.parse(D.d); //alert(JSON.stringify(DetailTable));
                if (DetailTable.length > 0) {
                    for (var j = 0; j < DetailTable.length; j++) {
                        var PEmpNo = DetailTable[j].EmpNo; //alert(PEmpNo);
                        var PaymentType = DetailTable[j].PaymentType; //alert(PaymentType);

                        var grid = document.getElementById("gvPensionPaymentDetail");
                        for (var row = 1; row < grid.rows.length; row++) {
                            var GridFrom_Cell = grid.rows[row].cells[0];
                            var lEmpNo = GridFrom_Cell.textContent.toString();

                            var lre = /^\s*/;
                            var rre = /\s*$/;
                            lEmpNo = lEmpNo.replace(lre, "");
                            lEmpNo = lEmpNo.replace(rre, "");
                            //alert(lEmpNo);
                            if (lEmpNo == PEmpNo) {
                                //alert("MATCHED");
                                for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                                    if (PaymentType == "C") {
                                        //alert("CVP");
                                        var fail = $("img[id*=failCVP]");
                                        $(fail[row1]).hide();

                                        var success = $("img[id*=successCVP]");
                                        $(success[row1]).show();

                                        var success = $("img[id*=successGratuity]");
                                        $(success[row1]).hide();

                                        var success = $("img[id*=successArrear]");
                                        $(success[row1]).hide();
                                    }
                                    else if (PaymentType == "G") {
                                        //alert("GRATUIPTY");
                                        var fail = $("img[id*=failGratuity]");
                                        $(fail[row1]).hide();

                                        var success = $("img[id*=successGratuity]");
                                        $(success[row1]).show();

                                        var success = $("img[id*=successCVP]");
                                        $(success[row1]).hide();

                                        var success = $("img[id*=successArrear]");
                                        $(success[row1]).hide();

                                    }
                                    else if (PaymentType == "A") {
                                        //alert("Arrear");
                                        var fail = $("img[id*=failArrear]");
                                        $(fail[row1]).hide();

                                        var success = $("img[id*=successArrear]");
                                        $(success[row1]).show();

                                        var success = $("img[id*=successCVP]");
                                        $(success[row1]).hide();

                                        var success = $("img[id*=successGratuity]");
                                        $(success[row1]).hide();

                                    }
                                }
                            }
                            else {
                                //alert("NOT MATCHED");
                                for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {

                                    var failCVP = $("img[id*=failCVP]");
                                    $(failCVP[row1]).show();

                                    var successCVP = $("img[id*=successCVP]");
                                    $(successCVP[row1]).hide();

                                    var failGratuity = $("img[id*=failGratuity]");
                                    $(failGratuity[row1]).show();

                                    var successGratuity = $("img[id*=successGratuity]");
                                    $(successGratuity[row1]).hide();

                                    var failArrear = $("img[id*=failArrear]");
                                    $(failArrear[row1]).show();

                                    var successArrear = $("img[id*=successArrear]");
                                    $(successArrear[row1]).hide();
                                }
                            }
                        }
                    }
                }
            }
        });
    });
}

