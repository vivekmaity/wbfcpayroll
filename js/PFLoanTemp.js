
$(document).ready(function () {

    document.getElementById("txtRefDate").disabled = true;
    //document.getElementById("txtLoanStartDate").disabled = true;
    document.getElementById("txtEmpName").disabled = true;
    document.getElementById("txtDesignation").disabled = true;
    document.getElementById("txtPayScale").disabled = true;
    document.getElementById("txtNetPay").disabled = true;
    document.getElementById("txtSector").disabled = true;
    document.getElementById("txtLocation").disabled = true;

    document.getElementById("txtOpeningBal").disabled = true;
    document.getElementById("txtCurrtBal").disabled = true;
    document.getElementById("txtRefLoan").disabled = true;
    document.getElementById("txtNonRefLoan").disabled = true;

    $("#lblMax").hide(); $("#lblMonth").hide();

    $('#rdRefundable').click(function () {
            $("#hdnRadioSelected").val("Refundable");
    });
    $('#rdNonRefundable').click(function () {
            $("#hdnRadioSelected").val("Non-Refundable");
    });

    $("#txtEmpNoSearch").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
    (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                event.preventDefault();
                return false;

            }
        }
    });
    $("#btnShowDetails").click(function (e) {

        $("#txtRefNo").val($("#txtEmpNoSearch").val());
        e.preventDefault();

        if ($("#rdRefundable").is(":checked")) {

            //============================================================================================================
            var EmpNo = $("#hdnEmpNo").val();
            var EmpNos = $("#txtEmpNoSearch").val();
            if (EmpNo == "" && EmpNos == "") {
                alert("Please Enter Employee No.");
                $("#txtEmpNoSearch").focus(); return;
            }
            else
            {
                var FEmpNo = (EmpNo == "" ? EmpNos : EmpNos);
            }
            GetLoanDetails(FEmpNo);
            //===================================================================================================================
            var RadiobtnText = ""; var isValid = "";
            if ($("#rdRefundable").is(":checked"))
                RadiobtnText = $("label[for=rdRefundable]").text();
            else
                RadiobtnText = $("label[for=rdNonRefundable]").text();

            var EmpNo = $("#txtEmpNoSearch").val();
            var C = "{EmpNo: '" + EmpNo + "', RadiobtnText: '" + RadiobtnText + "'}";
            //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/LoanDetailValidation',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var da = JSON.parse(D.d);
                    //alert(JSON.stringify(da));

                    var isval = da[0].IsValid; 
                    isValid = isval; //alert(isValid);

                    var isDataAvailable = da[0].Count; //alert(isDataAvailable);

                    if (isDataAvailable == 0) {
                        document.getElementById("btnSubmit").disabled = false;
                        document.getElementById("btnDelete").disabled = true;
                    }
                    else {
                        document.getElementById("btnSubmit").disabled = true;
                        document.getElementById("btnDelete").disabled = false;
                    }

                    if (isValid == 'Allowed') {

                        ShowDialog(false);

                        if (da[1].Result == "NoAnyData") {
                            var loanBalance = da[1].AdjustAmount == "" ? 0 : da[1].AdjustAmount;  //alert(loanBalance);
                            var prevLoanBalnce = $("#hdnLoanBalance").val() == "" ? loanBalance : $("#hdnLoanBalance").val(); //alert(prevLoanBalnce);
                            if (prevLoanBalnce != "" || prevLoanBalnce != null)
                                $("#txtAdjAmt").val(prevLoanBalnce);
                            else
                                $("#txtAdjAmt").val(loanBalance);
                        }
                        else {
                            var loanBalance = da[1].AdjustAmount == "" ? 0 : da[1].AdjustAmount;  //alert(loanBalance);
                            var prevLoanBalnce = $("#hdnLoanBalance").val() == "" ? loanBalance : $("#hdnLoanBalance").val(); //alert(prevLoanBalnce);
                            if (prevLoanBalnce != "" || prevLoanBalnce != null)
                                $("#txtAdjAmt").val(prevLoanBalnce);
                            else
                                $("#txtAdjAmt").val(loanBalance);

                            $("#txtRefNo").val();
                            $("#txtRefDate").val(da[1].ReferenceDate);
                            $("#txtLoanAmt").val(da[1].LoanAmount);


                            $("#txtDisburseAmt").val(da[1].DisburseAmt);
                            $("#txtTotInstNo").val(da[1].TotLoanInstall);
                            $("#txt1stInst").val(da[1].inst_amt1);
                            $("#txt2ndInst").val(da[1].inst_amt2);
                        }

                    }
                    else {
                        alert("Sorry ! This time you are not allowed to Entry any Loan for this Employee.");
                    }
                }
            });
            callfunction();
        } else if ($("#rdNonRefundable").is(":checked")) {

            var loanBalances = 0;
            $("#lblMax").hide(); $("#lblMonth").hide();
            $("#lblMaxMonth").hide();
            //============================================================================================================
            var EmpNo = $("#hdnEmpNo").val();
            var EmpNos = $("#txtEmpNoSearch").val();
            if (EmpNo == "" && EmpNos == "") {
                alert("Please Enter Employee No.");
                $("#txtEmpNoSearch").focus(); return;
            }
            else {
                var FEmpNo = (EmpNo == "" ? EmpNos : EmpNos);
            }
            GetLoanDetails(FEmpNo);
            //===================================================================================================================
            var RadiobtnText = "";
            if ($("#rdRefundable").is(":checked"))
                RadiobtnText = $("label[for=rdRefundable]").text();
            else
                RadiobtnText = $("label[for=rdNonRefundable]").text();

            var EmpNo = $("#txtEmpNoSearch").val();
            var C = "{EmpNo: '" + EmpNo + "', RadiobtnText: '" + RadiobtnText + "'}";
            //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/LoanDetailValidation',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var da = JSON.parse(D.d);
                    //alert(JSON.stringify(da));

                    var isval = da[0].IsValid; //alert(isval);
                    var isValid = isval;

                    var isDataAvailable = da[0].Count; //alert(isDataAvailable);vv
                    if (isDataAvailable == 0) {
                        document.getElementById("btnSubmit").disabled = false;
                        document.getElementById("btnDelete").disabled = true;
                    }
                    else {
                        document.getElementById("btnSubmit").disabled = true;
                        document.getElementById("btnDelete").disabled = false;
                    }

                    if (isValid == 'Allowed') {

                        ShowDialog(false);

                        if (da[1].Result == "NoAnyData") {
                            var loanBalance = da[1].AdjustAmount == "" ? 0 : da[1].AdjustAmount;  //alert(loanBalance);
                            var prevLoanBalnce = $("#hdnLoanBalance").val() == "" ? loanBalance : $("#hdnLoanBalance").val(); //alert(prevLoanBalnce);
                            if (prevLoanBalnce != "" || prevLoanBalnce != null)
                                $("#txtAdjAmt").val(prevLoanBalnce);
                            else
                                $("#txtAdjAmt").val(loanBalance);
                        }
                        else {
                            var loanBalance = da[1].AdjustAmount == "" ? 0 : da[1].AdjustAmount;  //alert(loanBalance);
                            var prevLoanBalnce = $("#hdnLoanBalance").val() == "" ? loanBalance : $("#hdnLoanBalance").val(); //alert(prevLoanBalnce);
                            if (prevLoanBalnce != "" || prevLoanBalnce != null)
                                $("#txtAdjAmt").val(prevLoanBalnce);
                            else
                                $("#txtAdjAmt").val(loanBalance);

                            $("#txtRefNo").val();
                            $("#txtRefDate").val(da[1].ReferenceDate);
                            $("#txtLoanAmt").val(da[1].LoanAmount);

                            var loanBalance = da[1].AdjustAmount; alert(loanBalance);
                            var prevLoanBalnce = $("#hdnLoanBalance").val() == "" ? loanBalance : 0; alert(prevLoanBalnce);
                            if (prevLoanBalnce != "" || prevLoanBalnce != null)
                                $("#txtAdjAmt").val(prevLoanBalnce);
                            else
                                $("#txtAdjAmt").val(da[1].AdjustAmount);


                            $("#txtDisburseAmt").val(da[1].DisburseAmt);
                            $("#txtTotInstNo").val(da[1].TotLoanInstall);
                            $("#txt1stInst").val(da[1].inst_amt1);
                            $("#txt2ndInst").val(da[1].inst_amt2);
                        }
                    }
                    else {
                        alert("Sorry ! This time you are not allowed to Entry any Loan for this Employee.");
                    }
                }
            });
            callfunction();
        }

    });

    $("#btnClose").click(function (e) {
        $("#txtRefNo").val(''); $("#txtRefDate").val(''); $("#txtLoanAmt").val('');
        $("#txtAdjAmt").val(''); $("#txtDisburseAmt").val(''); $("#txtTotInstNo").val(''); $("#txt1stInst").val('');
        $("#txt2ndInst").val(''); $("#txtLoanStartDate").val('');
        HideDialog();
        e.preventDefault();
    });
    $("#btnClose1").click(function (e) {
        HideDialog1();
        e.preventDefault();
    });
    $("#btnmsgClose").click(function (e) {
        HideDialog2();
        e.preventDefault();
    });

    $("#txtSalFinYear").change(function () {
        var salfinyear = $("#txtSalFinYear").val();
        var year = (salfinyear.substring(0, 4)); alert(year);
        $("#txtYear").val(year);
    });

    $('#txtRefDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow'
    });

    $("#btnRefDate").click(function () {
        var salfinyear = $("#txtSalFinYear").val();
        var year = (salfinyear.substring(0, 4)); //alert(year);
        var date = new Date(year, 2, 1); //alert(date);

        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear(); //alert(y); alert(m); alert(d);
        $("#txtRefDate").datepicker({
            minDate: new Date(y, m, d),
            maxDate: '+30D',
            dateFormat: "dd/MM/yyyy",
            defaultDate: new Date(y, m, d),
            changeMonth: true
        });
        $('#txtRefDate').datepicker("show");
        return false;
    });

    function GetLoanDetails(FEmpNo)
    {
        var C = "{ FEmpNo: '" + FEmpNo + "'}"; //alert(C);
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/LoanDetails',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".loading-overlay").hide();
                var da = JSON.parse(data.d);
                //alert(JSON.stringify(da));
                if (da == "nodata") {
                    $("#hdnLoanBalance").val('');
                } else {
                    var loanamount = da[0].LoanAmount;
                    var adjamount = da[0].AdjustAmount;
                    var disbamount = (parseInt(loanamount)) - (adjamount == null ? 0 : parseInt(adjamount));

                    var loanAmountPaid = da[0].LoanAmountPaid;
                    loanBalance = (parseInt(loanamount)) - (parseInt(loanAmountPaid)); //alert(loanBalance);
                    $("#hdnLoanBalance").val(loanBalance);
                }
            }
        });
    }


    function callfunction() {

        if (document.getElementById('rdRefundable').checked) {
            document.getElementById("txtTotInstNo").disabled = false;
            document.getElementById("txt1stInst").disabled = false;
            document.getElementById("txt2ndInst").disabled = false;
        }
        else if (document.getElementById('rdNonRefundable').checked) {
            document.getElementById("txtTotInstNo").disabled = true;
            document.getElementById("txt1stInst").disabled = true;
            document.getElementById("txt2ndInst").disabled = true;

        }
        else if (document.getElementById('rdFinalPayment').checked) {
            document.getElementById("txtTotInstNo").disabled = true;
            document.getElementById("txt1stInst").disabled = true;
            document.getElementById("txt2ndInst").disabled = true;
        }
        else {
            document.getElementById("txtTotInstNo").disabled = false;
            document.getElementById("txt1stInst").disabled = false;
            document.getElementById("txt2ndInst").disabled = false;
        }
    }

    function ShowDialog(modal) {
        $("#overlay").show();
        $("#dialog").fadeIn(300);

        if (modal) {
            $("#overlay").unbind("click");
        }
        else {
            $("#overlay").click(function (e) {
                HideDialog();
            });
        }
    }

    function HideDialog() {
        $("#overlay").hide();
        $("#dialog").fadeOut(300);
    }

    function HideDialog1() {
        $("#overlay1").hide();
        $("#dialog1").fadeOut(300);
    }

    function HideDialog2() {
        $("#overlay2").hide();
        $("#dialog2").fadeOut(300);
    }

    $("#txtLoanAmt").blur("change", function () {
        var RefDate = $("#txtRefDate").val();
        if (RefDate != "") {
            var A = $("#txtLoanAmt").val() == "" ? 0 : parseFloat($("#txtLoanAmt").val());
            var B = $("#txtAdjAmt").val() == "" ? 0 : parseFloat($("#txtAdjAmt").val());
            if (A == 0) {
                $("#txtLoanAmt").val(0); $("#txtAdjAmt").val(0); $("#txtDisburseAmt").val(0); $("#txtTotInstNo").val();
            } else {
                calLoanSanctionAmt();
            }
        }
        else {
            $("#txtRefDate").focus();
        }
    });
    $("#txtAdjAmt").blur("change", function () {
        var RefDate = $("#txtRefDate").val();
        if (RefDate != "") {
            var A = $("#txtLoanAmt").val() == "" ? 0 : parseFloat($("#txtLoanAmt").val());
            var B = $("#txtAdjAmt").val() == "" ? 0 : parseFloat($("#txtAdjAmt").val());

            calLoanSanctionAmt();
        }
        else {
            $("#txtRefDate").focus();
        }
    });

    $("#txtTotInstNo").blur("change", function () {
        var maxMonth = $("#lblMaxMonth").text();
        var EnterMonth = $("#txtTotInstNo").val();
        if (EnterMonth > maxMonth) {
            alert("Sorry ! Enter Total Loan Installment amount is not Valid.");
            $("#txtTotInstNo").focus();
        }
        else {
            calInst1st();
            calInst2nd();
        }
    });

    function calLoanSanctionAmt() {
        var cal = "";
        var LoanAmt = parseFloat($("#txtLoanAmt").val() == "" ? 0 : $("#txtLoanAmt").val()); //alert(LoanAmt);
        var AdjAmt = parseFloat($("#txtAdjAmt").val() == "" ? 0 : $("#txtAdjAmt").val()); //alert(AdjAmt);

        if (LoanAmt > AdjAmt) {
            $("#txtLoanAmt").val(LoanAmt);
            $("#txtAdjAmt").val(AdjAmt);
            $("#txtDisburseAmt").val(LoanAmt - AdjAmt);
            //$("#txtTotInstNo").val(24);

            var RadiobtnText = ""; 
            if ($("#rdRefundable").is(":checked"))
                RadiobtnText = $("label[for=rdRefundable]").text();
            else
                RadiobtnText = $("label[for=rdNonRefundable]").text();

            if (RadiobtnText == "Refundable")
                DatediffMonth();
            else
            {
                $("#lblMax").hide(); $("#lblMonth").hide();
                $("#lblMaxMonth").hide();
            }
        }
        else {
            $("#txtLoanAmt").val(parseFloat(LoanAmt) + parseFloat($("#txtAdjAmt").val()));
            $("#txtAdjAmt").val(AdjAmt);
            $("#txtDisburseAmt").val(parseFloat(LoanAmt));
            //$("#txtTotInstNo").val(24);

            var RadiobtnText = "";
            if ($("#rdRefundable").is(":checked"))
                RadiobtnText = $("label[for=rdRefundable]").text();
            else
                RadiobtnText = $("label[for=rdNonRefundable]").text();

            if (RadiobtnText == "Refundable")
                DatediffMonth();
            else {
                $("#lblMax").hide(); $("#lblMonth").hide();
                $("#lblMaxMonth").hide();
            }
        }
    }
    function calInst1st() {
       
        var cal = "";
        var LoanAmt = $("#txtLoanAmt").val(); //alert(LoanAmt);
        var TotInstNo = $("#txtTotInstNo").val(); //alert(TotInstNo);
        if (TotInstNo == "")
        {
            $("#txt1stInst").val(cal);
        }
        else if (TotInstNo > 24)
        {
            alert("Sorry ! Total Installment should be not more than 24.")
            $("#txtTotInstNo").focus();
            return true;
        }
        else {
            cal = parseFloat(LoanAmt) / parseFloat(TotInstNo); 
            var rcal = Math.round(cal);
            $("#txt1stInst").val(rcal);
            $("#txt2ndInst").val('');
        }
    }
    function calInst2nd() {
        var cal = "";
        var LoanAmt = $("#txtLoanAmt").val();
        var TotInstNo = $("#txtTotInstNo").val();
        var Inst1st = $("#txt1stInst").val();
        if (TotInstNo == "")
        {
            $("#txt1stInst").val(cal);
        }
        else if (TotInstNo > 24) {
            //alert("Sorry ! Total Installment should be not more than 24.")
            $("#txtTotInstNo").focus();
            return true;
        }
        else {
            cal = parseFloat(LoanAmt) - ((parseFloat(TotInstNo) - 1) * parseFloat(Inst1st));
            if (cal != Inst1st) {
                var rcal = Math.round(cal);
                $("#txt2ndInst").val(rcal);
                $("#txt1ndInst").val('');
            }
        }
    }

    function DatediffMonth()
    {
        var EmpNo = $("#txtEmpNoSearch").val();
        var RefDate = $("#txtRefDate").val();

        var E = "{EmpNo: '" + EmpNo + "', RefDate: '" + RefDate + "'}"; //alert(E);
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/Calculate_TotalServiceYear',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var t = JSON.parse(D.d); //alert(t);
                var arr = t.split(',');

                var years = arr[0]; //alert(years);
                var months = arr[1]; //alert(months);
                var days = arr[2]; //alert(days);

                var yearinmonth = parseInt(years) * 12;
                var totalmonths = parseInt(yearinmonth) + parseInt(months);
                if (totalmonths > 24) {
                    $("#lblMax").show(); $("#lblMonth").show();

                    $("#txtTotInstNo").val(24);
                    $("#lblMaxMonth").text(24);
                }
                else
                {
                    $("#lblMax").show(); $("#lblMonth").show();

                    $("#txtTotInstNo").val(totalmonths);
                    $("#lblMaxMonth").text(totalmonths);
                }




                calInst1st();
                calInst2nd();
            }
        });
    }

    $("#detailTable tr").click(function () {
        alert("You clicked ROW:\n\nID:  " + $(this).attr('id'));
    });

    $("#cmdCancel").click(function () {
        window.location.href = "PFLoanTemp.aspx";
    });

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    window.pfloanMaster = new KMDA.PFLoanTemp();
    window.pfloanMaster.init();


});

//$(function () {

//    $("#txtRefDate").datepicker({
//      defaultDate: new Date($("#hdnSalYear").val(), 2, 1),  //$("#txtSalFinYear").val().substring(0, 4)
//        minDate: new Date($("#hdnSalYear").val(), 2, 1)
//    });
//})

KMDA.MasterInfo = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#btnSubmit").click(A.AddClicked);
        $("#btnDelete").click(A.DeleteLoan);
        $("#btnRefundables").click(A.ShowPreviousLoan);

    };
    this.getValidator = function () {
        $("#form1").validate({ ignore: "" });
        //$("#ddlEmpType").rules("add", { required: true, messages: { required: "Please select employee type" } });
        return $("#form1").validate({ ignore: "" });
    }
    this.AddClicked = function () {
        var RadiobtnText = "";
        if ($("#rdRefundable").is(":checked")) 
            RadiobtnText = $("label[for=rdRefundable]").text(); 
    else
            RadiobtnText = $("label[for=rdNonRefundable]").text(); 

        //alert(RadiobtnText);
        var EmpNo = $("#hdnEmpNo").val();
        var EmpNos = $("#txtEmpNoSearch").val();
        var refNo = $("#txtRefNo").val(); var refDate = $("#txtRefDate").val(); var loanAmt = $("#txtLoanAmt").val();
        var adjAmt = $("#txtAdjAmt").val(); var loanDisburseAmt = $("#txtDisburseAmt").val(); var totalInstNo = $("#txtTotInstNo").val();
        var fstInst = $("#txt1stInst").val(); var sndInst = $("#txt2ndInst").val(); var loanStartDate = ""; //$("#txtLoanStartDate").val() == "" ? "" : $("#txtLoanStartDate").val();

        if (RadiobtnText == "Refundable") {
            if (refNo == "") {
                $("#txtRefNo").focus();
                return;
            } else if (refDate == "") {
                $("#txtRefDate").focus();
                return;
            } else if (loanAmt == "") {
                $("#txtLoanAmt").focus();
                return;
            } else if (totalInstNo == "") {
                $("#txtTotInstNo").focus();
                return;
            } else if (EmpNo == "" && EmpNos == "") {
                alert("Please Enter Employee No.");
                $("#txtEmpNoSearch").focus(); return;
            }
        }
        else 
        {
            var FEmpNo = (EmpNo == "" ? EmpNos : EmpNos);
            var C = "{FEmpNo: " + FEmpNo + ", refNo: '" + refNo + "', refDate: '" + refDate + "', loanAmt: '" + loanAmt + "', adjAmt: '" + adjAmt + "', loanDisburseAmt: '" + loanDisburseAmt + "', totalInstNo: '" + totalInstNo + "', fstInst: '" + fstInst + "', sndInst: '" + sndInst + "', loanStartDate: '" + loanStartDate + "', RadiobtnText: '" + RadiobtnText + "' }";  //
            //alert(C);
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: pageUrl + '/SaveLoanDetails',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    //alert(JSON.stringify(D));
                    var da = JSON.parse(D.d);
                    if (da == "success") {
                        alert("Loan Details are Saved Successfully !");
                        $("#txtRefNo").val(''); $("#txtRefDate").val(''); $("#txtLoanAmt").val('');
                        $("#txtAdjAmt").val(''); $("#txtDisburseAmt").val(''); $("#txtTotInstNo").val(''); $("#txt1stInst").val('');
                        $("#txt2ndInst").val(''); $("#txtLoanStartDate").val('');
                        $("#overlay").hide();
                        //$("#dialog").fadeOut(300);
                        $("#btnClose").trigger("click");
                    }
                }
            });
        }
        
    if (RadiobtnText == "Non-Refundable") {
        if (refNo == "") {
            $("#txtRefNo").focus();
            return;
        } else if (refDate == "") {
            $("#txtRefDate").focus();
            return;
        } else if (loanAmt == "") {
            $("#txtLoanAmt").focus();
            return;
        }
    }
    else
    {
        var FEmpNo = (EmpNo == "" ? EmpNos : EmpNos);
        var C = "{FEmpNo: " + FEmpNo + ", refNo: '" + refNo + "', refDate: '" + refDate + "', loanAmt: '" + loanAmt + "', adjAmt: '" + adjAmt + "', loanDisburseAmt: '" + loanDisburseAmt + "', totalInstNo: '" + totalInstNo + "', fstInst: '" + fstInst + "', sndInst: '" + sndInst + "', loanStartDate: '" + loanStartDate + "', RadiobtnText: '" + RadiobtnText + "' }";  //
        //alert(C);
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveLoanDetails',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                //alert(JSON.stringify(D));
                var da = JSON.parse(D.d);
                if (da == "success") {
                    alert("Loan Details are Saved Successfully !");
                    $("#txtRefNo").val(''); $("#txtRefDate").val(''); $("#txtLoanAmt").val('');
                    $("#txtAdjAmt").val(''); $("#txtDisburseAmt").val(''); $("#txtTotInstNo").val(''); $("#txt1stInst").val('');
                    $("#txt2ndInst").val(''); $("#txtLoanStartDate").val('');
                    $("#overlay").hide();
                    //$("#dialog").fadeOut(300);
                    $("#btnClose").trigger("click");
                }
            }
        });
    }
        
    };
    this.DeleteLoan = function () {
        window.pfloanMaster.DeleteLoanDetails();
    };
    this.ShowPreviousLoan = function () {
        window.pfloanMaster.ShowPreviousLoanDetails();
    };

};

KMDA.PFLoanTemp = function () {
    var A = this;
    this.pfloanForm;
    this.init = function () {
        //this.cooperativeForm = JSON.parse($("#empJSON").val());
        this.masterInfo = new KMDA.MasterInfo();
        this.masterInfo.init();
        var B = window.pfloanMaster.pfloanForm;
    };

    this.ShowDialogs = function () {
        //function ShowDialog1(modal) {
        $("#overlay1").show();
        $("#dialog1").fadeIn(300);

        if (modal) {
            $("#overlay1").unbind("click");
        }
        else {
            $("#overlay1").click(function (e) {
                HideDialog1();
            });
        }
        //}
    }
    this.ShowPreviousLoanDetails = function () {
        var B = window.pfloanMaster.pfloanForm;
        var EmpNo = $("#hdnEmpNo").val();
        var EmpNos = $("#txtEmpNoSearch").val();
        if (EmpNo == "" && EmpNos == "") {
            alert("Please Enter Employee No.");
            $("#txtEmpNoSearch").focus(); return;
        }
        else {
            var FEmpNo = (EmpNo == "" ? EmpNos : EmpNos);
            var C = "{ FEmpNo: '" + FEmpNo + "'}";
            //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/LoanDetails',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    //alert(JSON.stringify(D));
                    var da = JSON.parse(D.d);
                    //alert(JSON.stringify(da));
                    if (da == "nodata") {
                        alert("There is no any Previous Loan. Thank You !");
                        $("#txtEmpNoSearch").focus();
                    } else {
                        var LoanDate = ""; var LoanAmountLastPaid = "";

                        LoanDate = da[1].SalMonth; //alert(LoanDate);
                        var LoanAmountLastPaid = da[2].CummAmt; //alert(LoanAmountLastPaid);


                        $('#lblUpto').text(LoanDate);
                        $("#txtPRefNo").val(da[0].ReferenceNo); $("#txtPRefDate").val(da[0].ReferenceDate); $("#txtPLoanAmount").val(da[0].LoanAmount);

                        var loanamount = da[0].LoanAmount;
                        var adjamount = da[0].AdjustAmount;
                        var disbamount = (parseInt(loanamount)) - (adjamount == null ? 0 : parseInt(adjamount));

                        $("#txtPAdjAmount").val(adjamount == null ? 0 : adjamount);
                        $("#txtPDisburseAmt").val(disbamount);
                        $("#txtPTotInstNo").val(da[0].TotLoanInstall);

                        var loanAmountPaid = da[0].LoanAmountPaid;
                        var loanBalance = (parseInt(loanamount)) - (parseInt(loanAmountPaid));
                        $("#txtPLoanAmtPaid").val(da[0].LoanAmountPaid);
                        $("#txtPLoanBalance").val(loanBalance);
                        $("#txtPLoanStartDate").val(da[0].LoanDeducStartDate);

                        var isValid = $("#hdnIsValid").val(); 
                        if (isValid == 'Allowed') {
                            $("#hdnLoanBalance").val(loanBalance);
                            A.ShowDialogs();
                        }
                           
                    }
                }
            });
        }
    };
    this.DeleteLoanDetails = function () {
        var B = window.pfloanMaster.pfloanForm;
        var EmpNo = $("#hdnEmpNo").val();
        var EmpNos = $("#txtEmpNoSearch").val();
        if (EmpNo == "" && EmpNos == "") {
            alert("Please Enter Employee No.");
            $("#txtEmpNoSearch").focus(); return;
        }
        else {
            var RadiobtnText = ""; 
            if ($("#rdRefundable").is(":checked"))
                RadiobtnText = $("label[for=rdRefundable]").text();
            else
                RadiobtnText = $("label[for=rdNonRefundable]").text();

            var FEmpNo = (EmpNo == "" ? EmpNos : EmpNos);
            var C = "{ FEmpNo: '" + FEmpNo + "', RadiobtnText: '" + RadiobtnText + "'}";
            //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/DeleteLoan',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    //alert(JSON.stringify(D));
                    var da = JSON.parse(D.d);
                    //alert(JSON.stringify(da));
                    if (da == "Success")
                    {
                        alert("Loan Details are Deleted Successfully.");

                        $("#lblMax").hide(); $("#lblMonth").hide();
                        $("#lblMaxMonth").hide();
                        $("#btnClose").trigger("click");
                    }
                }
            });
        }
    };
};
















