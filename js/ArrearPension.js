


$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    //////////$('#txtFromdate').keypress(function (evt) {
    //////////    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //////////    if (iKeyCode != 13 || iKeyCode != 46) {
    //////////        alert("Manual Entry Not Allowed");
    //////////    }
    //////////});

    //////////$('#txtFromdate').keydown(function (evt) {
    //////////    $('#txtFromdate').val('');
    //////////    return false;
    //////////});

    //////////$('#txtTodate').keypress(function (evt) {
    //////////    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //////////    if (iKeyCode != 13 || iKeyCode != 46) {
    //////////        alert("Manual Entry Not Allowed");
    //////////    }
    //////////});

    //////////$('#txtTodate').keydown(function (evt) {
    //////////    //$('#txtTodate').val('');
    //////////    //return false;
    //////////});
});

$(document).ready(function () {
    $("#txtAmount").keypress(function (e) {

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            alert("Please Enter Numeric Value");
            return false;
        }
    });
});

$(document).ready(function () {
    $('#txtEmpNo').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            alert("Only Numbers Allowed !");
            return false;
        }
    });
});

$(document).ready(function () {

    $('#txtFromdate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy',
        onSelect: function (selected) {
            $("#txtTodate").datepicker("option", "minDate", selected)
        }
    });
});

$(document).ready(function () {
    $('#txtTodate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy',
        onSelect: function (selected) {
            $("#txtFromdate").datepicker("option", "maxDate", selected)

        }
    });
});

$(document).ready(function () {
    //$("#txtEmpNo").keypress(function () {
        $("#txtEmpNo").autocomplete({
            source: function (request, response) {
                var SectorID = $('#ddlSector').val();
                var E = "";
                var text = request.term;
                if (text != "") {
                    E = "{prefix:'" + request.term + "'}";
                    $.ajax({
                        type: "POST",
                        url: 'PensionArrear.aspx/AjaxGetGridCtrl',
                        data: E,
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var item = jQuery.parseJSON(data.d); 
                            if (item != "NoAvalil") {
                                var empName = item[0].EmployeeName;
                                var Designation = item[0].Designation;
                                var EmpId = item[0].EmployeeID;
                                var EmpSector = item[0].SectorID;

                                $("#txtEmpName").val(empName);
                                $("#txtEmpDsg").val(Designation);
                                $("#EmpID").val(EmpId);

                                SearchPensionArrearSalary(EmpId);
                            }
                            else {
                                alert("Please Enter Valid Employee No !");
                                $("#txtEmpNo").focus(); return false;
                            }
                        },
                        error: function (response) {
                            alert('Some Error Occur');
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }
            },
            minLength: 6
        });
    //});

   
});

$("#txtEmpNo").keydown(function (evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
    if (iKeyCode == 8) {
        $("#txtEmpName").val('');
        $("#txtEmpDsg").val('');

        $("#hdnSaveUpdate").val("Save");
        SearchPensionArrearSalary(0);
    }
});

function SearchPensionArrearSalary(EmpId) {
    E = "{EmpId:'" + EmpId + "'}";
    $.ajax({
        type: "POST",
        url: 'PensionArrear.aspx/GET_ArrearSalaryForUpdate',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var item = jQuery.parseJSON(data.d);
            if (item != "NotFound")   //item.length > 0 || 
            {
                var day = ""; var month = ""; var year = "";
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                var FromDate = item[0].ArrearFrom;

                var dateString = FromDate.substr(6);
                var currentTime = new Date(parseInt(dateString));

                day = currentTime.getDate() < 10 ? '0' + currentTime.getDate() : currentTime.getDate();
                if (currentTime.getMonth() == 9)
                    month = parseInt(currentTime.getMonth()) + 1;
                else
                    month = currentTime.getMonth() < 10 ? '0' + (parseInt(currentTime.getMonth()) + 1) : (parseInt(currentTime.getMonth()) + 1);

                year = currentTime.getFullYear();

                var Fdate = day + "/" + month + "/" + year;
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                var Tday = ""; var Tmonth = ""; var Tyear = "";
                var ToDate = item[0].ArrearTo;

                var dateString = ToDate.substr(6);
                var currentTime = new Date(parseInt(dateString));

                Tday = currentTime.getDate() < 10 ? '0' + currentTime.getDate() : currentTime.getDate();
                if (currentTime.getMonth() == 9)
                    Tmonth = parseInt(currentTime.getMonth()) + 1;
                else
                    Tmonth = currentTime.getMonth() < 10 ? '0' + (parseInt(currentTime.getMonth()) + 1) : (parseInt(currentTime.getMonth()) + 1);

                Tyear = currentTime.getFullYear();

                var Tdate = Tday + "/" + Tmonth + "/" + Tyear;
                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                var TotalEarning = item[0].TotalEarning;
                var Remarks = item[0].Remarks;

                $("#txtFromdate").val(Fdate);
                $("#txtTodate").val(Tdate);
                $("#txtAmount").val(TotalEarning);
                $("#txtRemarks").val(Remarks);

                $("#hdnSaveUpdate").val("Update");
            }
            else {
                //$("#txtFromdate").val('');
                //$("#txtTodate").val('');
                //$("#txtAmount").val('');
                //$("#txtRemarks").val('');
                $("#hdnSaveUpdate").val("Save");
                SearchPensionArrearSalary(0);
            }
        }
    });
}

//This if for Final Save Button Click
$(document).ready(function () {
    $("#cmdSave").click(function (event) {
        var EmpId = $("#EmpID").val(); 

        var FromDate = $("#txtFromdate").val();
        var ToDate = $("#txtTodate").val();
        var NetAmount = $("#txtAmount").val();
        var Remarks = $("#txtRemarks").val();

        var SaveUpdate = $("#hdnSaveUpdate").val();

        if (EmpId == "") {
            alert("Please Select Correct User Details !");
            $("#txtEmpNo").focus();
            return false;
        }

        if (FromDate == "") {
            alert("Please Select From Date !");
            $("#txtFromdate").focus();
            return false;
        }

        if (ToDate == "") {
            alert("Please Select To Date !");
            $("#txtTodate").focus();
            return false;
        }

        if (NetAmount =="") {
            alert("Please Enter Amount !");
            $("#txtAmount").focus();
            return false;
        }

        if (Remarks == "") {
            alert("Please Select Remarks !");
            $("#txtRemarks").focus();
            return false;
        }

        E = "{EmpId:'" + EmpId + "', FromDate:'" + FromDate + "', ToDate:'" + ToDate + "', NetAmount:'" + NetAmount + "', Remarks:'" + Remarks + "'}";

        var URL = "";
        if (SaveUpdate == "Save")
            URL = 'PensionArrear.aspx/Save_PensionArrearSalMaster';
        else
            URL = 'PensionArrear.aspx/Update_PensionArrearSalMaster';
       
        $.ajax({
            type: "POST",
            url: URL,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var t=JSON.parse(data.d);
                if(t=="Success")
                {
                    alert("Arrear Pension Detail is Inserted Successfully !");
                    Refresh();

                }
            }
        });

    });
});

function Refresh()
{
    window.location.href = "PensionArrear.aspx";
}

