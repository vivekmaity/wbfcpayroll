﻿$(function () {  
    var Selector = {
        txtAccFinYear: $("#txtAccFinYear"),
        hdnRateSlapID: $("#hdnRateSlapID"),
        txtFinYear: $("#txtFinYear"),
        ddlGender: $("#ddlGender"),
        txtAmountFrom: $("#txtAmountFrom"),
        txtAmountTo: $("#txtAmountTo"),
        txtAddAmount: $("#txtAddAmount"),
        txtPercentage: $("#txtPercentage"),
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        tbl: $("#tbl")
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    var xml = $.parseXML(responce.d);
                    callback(xml);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    var xml = $.parseXML(responce.d);
                    callback(xml);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    };
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeydown();
            objClientSide.Event.onBlur();
            objClientSide.Event.onChange();
        },
        Event: {
            onLoad: function () {
                objClientSide.Method.LoadFinYear();             
                objClientSide.Method.GetData();               
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    var ID = $(this).closest("tr").find(".clsID").text();
                    var FinYear = $(this).closest("tr").find(".clsFinYear").text();
                    var GenderID = $(this).closest("tr").find(".clsGenderID").text();
                    var AddAmount = $(this).closest("tr").find(".clsAddAmount").text();
                    var AmountFrom = $(this).closest("tr").find(".clsAmountFrom").text();
                    var AmountTo = $(this).closest("tr").find(".clsAmountTo").text();
                    var Percentage = $(this).closest("tr").find(".clsPercentage").text();
                    Selector.hdnRateSlapID.val(ID);
                    Selector.txtAddAmount.val(AddAmount);
                    Selector.txtAmountFrom.val(AmountFrom);
                    Selector.txtAmountTo.val(AmountTo);
                    Selector.txtPercentage.val(Percentage);
                    Selector.ddlGender.val(GenderID);
                    Selector.btnSave.val('Update');
                });
                $(document).on("click", ".clsDelete", function () {
                    var ID = $(this).closest("tr").find(".clsID").text();
                    if (Selector.hdnRateSlapID.val() == ID) {
                        alert("this Record is in Edit Mode. You can not Delete this record now..");
                        return false;
                    }

                    if (confirm("Are you sure want to Delete this Record ??")) {

                        objClientSide.Method.Delete(ID);
                    }
                });
            },
            onKeydown: function () {
                $(document).on('keypress', '.money', function (e) {
                    if (!(e.keyCode > 45 && e.keyCode < 58) || e.keyCode == 47 || (e.keyCode == 46 && $(this).val().indexOf('.') != -1)) {
                                return false;
                            }
                });               
            },
            onBlur: function () {
                Selector.txtAccFinYear.blur(function () {
                    objClientSide.Method.LoadFinYear();
                    objClientSide.Method.GetItexMaxAmount();
                });
            },
            onChange: function () {
                Selector.ddlGender.change(function () {
                    var genderID = $(this).val().trim();
                    objClientSide.Method.Search(genderID);
                });
            }
        },
        Method: {
            LoadFinYear: function () {
                Selector.txtFinYear.val(Selector.txtAccFinYear.val().trim());
            },           
            GetData: function () {
                var Data = { FinYear: Selector.txtAccFinYear.val().trim() };
                var url = "ItaxRateSlapMaster.aspx/Get_Data";
                console.log(Data);
                objServerSide.PostMehtod(url, Data, function (xml) {
                    var xmlTable = $(xml).find('Table');
                  //  console.log(jsdata);
                    objClientSide.Method.AppendTable(xmlTable);
                });
            },
            AppendTable: function (xmlTable) {
                Selector.tbl.find('tbody').empty();
                $.each(xmlTable, function (index, value) {
                    //   RateSlabID, AmountFrom, AmountTo, Percentage, AddAmount, FinYear, Sex,FullSex
                    index = index + 1;
                    Selector.tbl.find('tbody').append("<tr>"
                        + "<td class='clsID' style='display:none;'>" + $(value).find('RateSlabID').text() + "</td>"
                        + "<td >" + index + "</td>"
                        + "<td class=' clsFinYear' style='display:none;'>" + $(value).find('FinYear').text() + "</td>"
                        + "<td class=' clsGenderID' style='display:none;'>" + $(value).find('Sex').text() + "</td>"
                        + "<td class=' clsGender'>" +$(value).find('FullSex').text()  + "</td>"
                        + "<td class=' clsAddAmount'>" +$(value).find('AddAmount').text()  + "</td>"
                        + "<td class=' clsAmountFrom'>" +$(value).find('AmountFrom').text()  + "</td>"
                        + "<td class=' clsAmountTo'>" +$(value).find('AmountTo').text()+ "</td>"
                        + "<td class=' clsPercentage'>" +$(value).find('Percentage').text() + "</td>"
                        + "<td class=' clsUpdate text-center text-info' style='cursor:pointer;color:#007697;' >Edit</td>"
                        + "<td class=' clsDelete text-center text-default' style='cursor:pointer;color:#007697;'>Delete</td>"
                        + "</tr>");
                });
            },
            Save: function () {
                var rt = objClientSide.Method.Validation();
                if (rt==false) {
                    return false;
                }
           //     RateSlabID, AmountFrom, AmountTo, Percentage, Sex, AddAmount, FinYear
                var Data = {
                    RateSlabID: Selector.hdnRateSlapID.val(), AmountFrom: Selector.txtAmountFrom.val(),
                    AmountTo: Selector.txtAmountTo.val().trim(), Percentage: Selector.txtPercentage.val().trim(),
                    Sex: Selector.ddlGender.val(), AddAmount: Selector.txtAddAmount.val().trim(),FinYear: Selector.txtAccFinYear.val().trim()                                   
                };
                console.log(Data);
                var URL = "ItaxRateSlapMaster.aspx/Save";
                objServerSide.PostMehtod(URL, Data, function (xml) {
                    if (xml == 'error') {
                        alert('Error In Server side.. Operation Fail');
                        return false;
                    }
                    var xmlTable = $(xml).find('Table'); 
                    var Massage = '';
                    var returnCode = '';
                    $.each(xmlTable, function (index, value) {
                        Massage = $(value).find("Messege").text();
                        returnCode = $(value).find("ErrorCode").text();
                        alert(Massage);
                        if (returnCode == 1) {
                            window.location.reload();
                        }
                    });
                });
            },
            Delete: function (ID) {
                var Data = { ID: ID };
                var URL = "ItaxRateSlapMaster.aspx/Delete";
                objServerSide.PostMehtod(URL, Data, function (xml) {
                    if (xml == 'error') {
                        alert('Error In Server side.. Operation Fail');
                        return false;
                    }
                    var xmlTable = $(xml).find('Table');
                    var Massage = '';
                    var returnCode = '';
                    $.each(xmlTable, function (index, value) {
                        Massage = $(value).find("Massage").text();
                        returnCode = $(value).find("returnCode").text();
                        alert(Massage);
                        if (returnCode == 0) {
                            window.location.reload();
                        }
                    });
                });
            },
            Refresh: function () {
                Selector.txtAddAmount.val("");
                Selector.txtAmountFrom.val("");
                Selector.txtAmountTo.val("");
                Selector.txtPercentage.val("");
                Selector.hdnRateSlapID.val("");
                Selector.btnSave.val('Save');
                Selector.ddlGender.val('');
                objClientSide.Method.LoadFinYear();
                objClientSide.Method.GetData();
            },
            Validation: function () {
                if (Selector.txtFinYear.val().trim() == '') {
                    alert("Financial Year miss");
                    Selector.txtFinYear.focus();
                    return false;
                }
                if (Selector.ddlGender.val() == '') {
                    alert("Select a Gender");
                    Selector.ddlGender.focus();
                    return false;
                }
                if (Selector.txtAmountFrom.val().trim() == '') {
                    alert("Enter Amount From");
                    Selector.txtAmountFrom.focus();
                    return false;
                }
                if (Selector.txtAmountTo.val().trim() == '') {
                    alert("Enter Amount To");
                    Selector.txtAmountTo.focus();
                    return false;
                }
                if (Selector.txtAddAmount.val().trim() == '') {
                    alert("Enter Add Amount");
                    Selector.txtAddAmount.focus();
                    return false;
                }
                if (Selector.txtPercentage.val().trim() == '') {
                    alert("Enter Add Amount");
                    Selector.txtPercentage.focus();
                    return false;
                }
                return true;
            },
            Search: function (GenderID) {
                Selector.tbl.find('tbody > tr').find('.clsGenderID:contains(' + GenderID + ')').closest('tr').show();
                Selector.tbl.find('tbody > tr').find('.clsGenderID:not(:contains(' + GenderID + '))').closest('tr').hide();
            }
        }
    };

    objClientSide.Initialization();
});