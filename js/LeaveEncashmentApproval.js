﻿

$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });


    var GridItemDetail = document.getElementById("GridEmpLeaveEncash");
    GridItemDetail.deleteRow(1);

    $('#txtMemoDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });

});

$(document).ready(function () {
    $('#txtEmpNoSearch').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            alert("Only Numbers Allowed !");
            return false;
        }
    });
});


$(document).ready(function () {
    $('#divGrid').scroll(function () {
        var y = $(this).scrollTop();
        if (y > 10) {
            $('#GridView1').removeClass('HeadertableHide');
            $('#GridView1').addClass('HeadertableView');
        }
        else {
            $('#GridView1').addClass('HeadertableHide');
            $('#GridView1').removeClass('HeadertableView');
        }
    });
});

$(document).ready(function () {
    BindEmpLeaveEncashApproval();
});

function BindEmpLeaveEncashApproval() {
    $(".loading-overlay").show();
    var W = "{}";
    $.ajax({
        type: "POST",
        url: "LeaveEncashmentApproval.aspx/GET_GridInitialization",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                for (var i = 0; i < data.d.length; i++) {
                    $("#GridEmpLeaveEncash").append("<tr><td>" +
                    "<input type='checkbox' id='chkApp' value='" + data.d[i].EncashmentID + "'>" + "</td> <td>" +
                    data.d[i].EmpNo + "</td> <td>" +
                    data.d[i].EmpName + "</td> <td>" +
                    data.d[i].LeaveCount + "</td> <td>" +
                    parseFloat(data.d[i].Amount).toFixed(2) + "</td></tr>");
                }
            }
            $(".loading-overlay").hide();
            $(function () {
                $("[id*=GridEmpLeaveEncash] td").bind("click", function () {
                    var row = $(this).parent();
                    $("[id*=GridEmpLeaveEncash] tr").each(function () {
                        if ($(this)[0] != row[0]) {
                            $("td", this).removeClass("selected_row");
                        }
                    });
                    $("td", row).each(function () {
                        if (!$(this).hasClass("selected_row")) {
                            $(this).addClass("selected_row");
                        } else {
                            $(this).removeClass("selected_row");
                        }
                    });
                });
            });
        },

        error: function (result) {
            alert("Error Records Data");
            $(".loading-overlay").hide();
        }

    });
}

//============= Start Search Keypress Coding Here
$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtEmpNoSearch').keyup(function () {
        $('#GridEmpLeaveEncash').find('tr:gt(0)').hide();
        var data = $('#txtEmpNoSearch').val();
        var len = data.length;
        if (len > 0) {
            $('#GridEmpLeaveEncash').find('tbody tr').each(function () {
                coldata = $(this).children().eq(1);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        }
        else {
            $('#GridEmpLeaveEncash').find('tr:gt(0)').show();
        }
    });
});

function SaveApproval() {

    if ($("#txtMemoNo").val() == "") {
        alert("Please Select Memo Number!");
        $("#txtMemoNo").focus();
        return false;
    }

    if ($("#txtMemoDate").val() == "") {
        alert("Please Select Memo Date!");
        $("#txtMemoDate").focus();
        return false;
    }

    var ApprovalData = "";
    var Count = 0;
    $('#GridEmpLeaveEncash').find('tbody tr td input[id*="chkApp"][type=checkbox]:checked').each(function () {
        if (ApprovalData == "") {
            ApprovalData = $(this).val();
            Count++;
        }
        else {
            ApprovalData = ApprovalData + ',' + $(this).val();
            Count++;
        }
    });

    var MemoNo = $("#txtMemoNo").val();
    var MemoDate = $("#txtMemoDate").val();
    var con = confirm("Do You want to Save ?");

    if (ApprovalData != "") {
        if (con == true) {
            E = "{ApprovalData:'" + ApprovalData + "', MemoNo:'" + MemoNo + "',MemoDate:'" + MemoDate + "'}";
            $.ajax({
                type: "POST",
                url: 'LeaveEncashmentApproval.aspx/SaveMasterApproval',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == Count) {
                        alert(Count + " Leave Encashment Approval Updated Successfully!");
                        ClearFields();
                    }
                    else {
                        alert(Count + " Leave Encashment Approval Updated Successfully!" + Count - parseInt(data.d) + " Records Failed To Update!");
                        ClearFields();
                    }
                },
                error: function (response) {
                    alert('Connection Error! Please Try Again! [Contact Authorized Person If problem Persist!]');
                    BindEmpLeaveEncashApproval();
                    return false;
                }
            });
        }
    }
    else {
        alert("Please Select Atleast 1 Employee Leave Encashment Detail To Approve!");
        $('#txtEmpNoSearch').focus();
        return false;
    }
}

function ClearFields() {
    GridItemDetail = document.getElementById("GridEmpLeaveEncash");
    var i = parseInt($("#GridEmpLeaveEncash tr").length);
    i = i - 1;
    while (i > 0) {
        GridItemDetail.deleteRow(i);
        i--;
    }
    BindEmpLeaveEncashApproval();
    $('#txtEmpNoSearch').val('');
}