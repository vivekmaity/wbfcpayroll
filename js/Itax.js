﻿$(document).ready(function () {
    $("input:text").focus(function () { if ($(this).val() == 0) { $(this).val('') }; });

    $('.numeric').each(function (evt) {
        if ($(this).text() == 0) { $(this).text(''); }
     
    });
});

function myFunction() {
    if (document.getElementById('rbPercent').checked == true) {
        $('.Max').text('Percentage  ');
        $('.PMax').show();
        $('#txtPMaxAmount').show();
        $('.mndtry').show();
        
    }
    if (document.getElementById('rbValue').checked == true) {
        $('.Max').text('Max  Amount  ');
        $('.PMax').hide();
        $('#txtPMaxAmount').hide();
        $('.mndtry').hide();
        $('#txtPMaxAmount').val('');
    }
}
function deleteRow(Element) {
    var row = $(Element).closest('tr');
    $(row).remove();
    var Total = 0;
    if ($('#GridViewOther tr').length == 1) { $('#txtOtherSource').val(''); }
    $('.gridcls').each(function (evt) {
        Total = Total + parseInt($(this).text());
        $('#txtOtherSource').val(Total);
    });
    NetBalance();
}
$(document).ready(function () {
    $('.show4').hide();
    $('#btnSectionAdd').hide();
    
    if ($('#HiddenCheckGrid').val() == 1) {
        var F = "{hdnEmpID:'" + $('#hdnEmpID').val() + "',SalFinYear:'" + $("#txtSalaryFinYear").val() + "',AssesmentYear:'" + $("#txtAssessmentYear").val() + "'}";
        $.ajax({
            type: "POST",
            url: "ITaxEmployeeDeductionDetails.aspx/GetTaxBalAndChallan",
            data: F,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $('#btnSectionAdd').show();
                var xmlDoc = $.parseXML(D.d);
                var xml = $(xmlDoc);
                var Challan = xml.find("Table");
                var ItaxDetails = xml.find("Table1");
                $('#txtInterestFromSaving').val(Math.round($(ItaxDetails).find('OtherIncomeIntSaving').text()));
                $('#txtInterestFromFD').val(Math.round($(ItaxDetails).find('OtherIncomeIntFD').text()));
                $('#txtOtherS').val(Math.round($(ItaxDetails).find('OtherIncomeOther').text()));
                $('#txtSurcharge').val(Math.round($(ItaxDetails).find('Surcharge').text()));
                $('#txtUS89').val(Math.round($(ItaxDetails).find('ReliefUS89').text()));
                $('#txtDetails1').val(Math.round($(ItaxDetails).find('Total').text()));
            
                $.each(Challan, function () {
                    var customer = $(this);
                    $('#GridViewOther').append("<tr><td>" + $(this).find("ChallanNo").text() + "</td><td>" + $(this).find("BSRNo").text() + "</td><td>" + $(this).find("ChallanDate").text() + "</td><td style='text-align:right' class='gridcls'>" + Math.round($(this).find("TaxAmount").text()) + "</td><td style='text-align:right'><input id='btnDelete' onclick='deleteRow(this)' class='Btnclassname' type='button' value='Delete'/></td></tr>");
                });
                var Total = 0;
                $('.gridcls').each(function (evt) {
                    Total = Total + parseInt($(this).text());
                    $('#txtOtherSource').val(Total);
                });
                

            }
        });
        var F = "{hdnEmpID:'" + $('#hdnEmpID').val() + "'}";
        $.ajax({
            type: "POST",
            url: "ITaxEmployeeDeductionDetails.aspx/Grid2ndGrid",
            data: F,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $('#tblBeneficary_CHDetail').html(D.d);
                //console.log(D.d);
                Calcutate();
            }
        });
        GetOtherSourceIncomeDetail();
    }
    CalNetTaxPay();
});
function checkButton(t) {
    if ($(t).val() == 'Hide Details') {
        $(t).val('Show Details');
        var d = $(t).parent().parent().find('td').find('#lblId').val();
        var Trclass = "." + d;
        $(Trclass).hide(); return false;
    }
    if ($(t).val() == 'Show Details')
    {
        $(t).val('Hide Details');
        var d = $(t).parent().parent().find('td').find('#lblId').val();
        var Trclass = "." + d;
        $(Trclass).show();
    }
}
//function 
function NetBalance() {
    var TotalTaxLi = 0;
    var OtherPaid = 0;
    if ($('#txtOtherSource').val() != "") { OtherPaid = $('#txtOtherSource').val(); }
    // alert(Math.sign(7854242472));
    if ($('#txtNetTaxPay').val() != "") { TotalTaxLi = $('#txtNetTaxPay').val(); }
    var check = Math.sign(TotalTaxLi);
    var BalanceMinus = Math.round(parseFloat(TotalTaxLi) + (parseFloat($('#txtTDSAmt').val()) + parseFloat(OtherPaid))); if (BalanceMinus == 'NaN') { BalanceMinus = 0; }
    var BalancePlus = Math.round(parseFloat(TotalTaxLi) - (parseFloat($('#txtTDSAmt').val()) + parseFloat(OtherPaid))); if (BalancePlus == 'NaN') { BalancePlus = 0; }
    if (check == -1) {
        $('.balance').text('Balance Refundable  ');
        //$('#txtBalTax').val(BalanceMinus);
    }
    else {
        $('.balance').text('Balance Deductible ');
        //$('#txtBalTax').val(BalancePlus);

    }
    if (parseFloat($('#txtBalTax').val()) > 0) { $('#txtBalTax').val(BalancePlus); } else { $('#txtBalTax').val(BalanceMinus); }
}
function CalEduCess(Rebate) {
    var G = "";
    if (G = "") { G = 0; } else {
        G = Math.round(parseFloat($('#txtOnNetIncome').val()) - parseFloat($('#txtRabate').val()));;
    }
    if ($('#txtTaxableIncome').val() <= 500000) { $('#txtRabate').val(5000); G = 5000; }

    //Educess = 
    $('#txtEduCess').val(Math.round((G * 2) / 100));
    $('#txtHDCess').val(Math.round((G * 1) / 100));
}
function CalTaxPay() {
    var US89 = 0;
    var TotalLi = 0;
    if ($('#txtUS89').val() != '') { US89 = $('#txtUS89').val(); }
    if ($('#txtTaxLi').val() != '') { TotalLi = $('#txtTaxLi').val(); }
    var Total = parseFloat(TotalLi) - parseFloat(US89);
    if (Total == "NaN") {
        $('#txtNetTaxPay').val() = 0;
    }
    else {
        $('#txtNetTaxPay').val(Math.round(parseFloat(Total)));
    }
    NetBalance();
}
function TotTaxLi() {
    var NetIncome = 0;
    var Rebate = 0;
    var SurCharge = 0;

    if ($('#txtOnNetIncome').val() == '' || $('#txtOnNetIncome').val() == 'NaN') { NetIncome = 0; } else { NetIncome = parseFloat($('#txtOnNetIncome').val()); }

    if ($('#txtRabate').val() == '' || $('#txtRabate').val() == 'NaN') { Rebate = 0; } else { Rebate = parseFloat($('#txtRabate').val()); }

    if ($('#txtSurcharge').val() == '' || $('#txtSurcharge').val() == 'NaN') { SurCharge = 0; } else { SurCharge = parseFloat($('#txtSurcharge').val()); }



    $('#txtTaxLi').val(Math.round(((parseFloat(NetIncome) - parseFloat(Rebate))) + (parseFloat(SurCharge) + parseFloat($('#txtEduCess').val()) + parseFloat($('#txtHDCess').val()))));
    NetBalance();
    CalTaxPay();
}
$(document).ready(function () {
    var Date = $('#txtSalaryFinYear').val().split('-');
    Date = Date[0];
    $('#txtDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        duration: 'slow',
        minDate: '' + Date + '-03-01',
        maxDate: 'today',
        onClose: function () {
            $(this).focus();
        },
        dateFormat: 'yy-mm-dd'
    });
    $(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $("#GridViewOther tr:gt(0)").remove();
    $('#btnOtherSource').click(function () {

        load_Data();
        return false;
    });
    $('#btnOtherRefresh').click(function () {
        $("#GridViewOther tr:gt(0)").remove();
        $('#txtOtherSource').val('');
        NetBalance();
        return false;
    });
    $('#btnRefresh').click(function () {
        window.location.href = "ITaxEmployeeDeductionDetails.aspx";
        return false;
    });

});

function load_Data() {
    var Total = '';
    if ($('#txtChallanNo').val() == '') { $('#txtChallanNo').focus(); return false; }
    if ($('#txtBSRNo').val() == '') { $('#txtBSRNo').focus(); return false; }
    if ($('#txtDate').val() == '') { $('#txtDate').focus(); return false; }
    if ($('#txtAmount').val() == '') { $('#txtAmount').focus(); return false; }
   // $('#GridViewOther').append("<tr><td>" + $('#txtChallanNo').val() + "</td><td>" + $('#txtBSRNo').val() + "</td><td>" + $('#txtDate').val() + "</td><td style='text-align:right'>" + $('#txtAmount').val() + "</td></tr>");

    $('#GridViewOther').append("<tr><td>" + $('#txtChallanNo').val() + "</td><td>" + $('#txtBSRNo').val() + "</td><td>" + $('#txtDate').val() +
      "</td><td style='text-align:right'>" + $('#txtAmount').val() + "</td><td style='width:50px'> <input id='btnDelete' onclick='deleteRow(this)' type='button' class='Btnclassname' value='Delete'/></td></tr>");

    $("#grdEmployee tr").filter(":even").addClass("GridRowEven");
    $("#grdEmployee tr").filter(":odd").addClass("GridRowOdd");

    if ($('#txtOtherSource').val() == "") { $('#txtOtherSource').val(0); }
    $('#txtOtherSource').val(Math.round(parseFloat($('#txtOtherSource').val()) + parseFloat($('#txtAmount').val())));
    NetBalance();
    $('#txtChallanNo').val('');
    $('#txtBSRNo').val('');
    $('#txtDate').val('');
    $('#txtAmount').val('');
    return false;

}
function CkeckAmountCESS(Amount, AmountType, Per, lblID) {
    if (AmountType == 'P') { $('#' + lblID + '').text((parseFloat(Amount) * parseFloat(Per)) / 100); }
    else {
        $('#' + lblID + '').text(Math.round(parseFloat(Amount)));
    }
    var total = 0;
    var TDS = 0;
    $('.g2').each(function (evt) {
        if ($(this).text() == '') { $(this).text(0); }
        total = parseFloat($(this).text()) + total;
        alert(total);
        if ($('#txtTDS').val() != '' || $('#txtTDS').val() != 'NaN') { TDS = $('#txtTDS').val(); }
        $('#txtNetTaxPay').val(Math.round(parseFloat($('#txtTDS').val()) - parseFloat(total)));
    });
}

//function CkeckMaxAmount(thisval, lableMaxType, lableMaxAmt, txtcss) {
//    if (parseInt(thisval) > parseInt(lableMaxAmt)) { $(this).val(lableMaxAmt); alert(lableMaxAmt); }
//}

function CkeckAmount(Amount, TaxType, MaxAmount, LblID) {
    
    if (TaxType == 'P') {
        var AmountVal = 0; if (Amount != '') { AmountVal = Amount; }
        var MaxAmountVal = 0; if (MaxAmount != '') { MaxAmountVal = MaxAmount; }
        $('#' + LblID + '').text(Math.round((parseFloat(AmountVal) * parseFloat(MaxAmountVal)) / 100));
        var total = 0;
        $('.chklbl').each(function (evt) {
          
            if ($(this).text() == '') { $(this).text(0); }
            total = parseFloat($(this).text()) + total;
            $('#txtTotalDeduction').val(total);
        });
    }
    if (TaxType == 'V') {
        if (parseInt(Amount) > parseInt(MaxAmount)) { $('#' + LblID + '').text(MaxAmount); return false; }
        var AmountVal = 0; if (Amount != '') { AmountVal = Amount; }
        $('#' + LblID + '').text(AmountVal);
        var total = 0;
        $('.chklbl').each(function (evt) {
            //if ($(this).text() == '') { $(this).text(0); }
            var thisvalue = $(this).text(); if (thisvalue == '') { thisvalue = 0; }
            total = parseFloat($(this).text()) + total;
            $('#txtTotalDeduction').val(total);
        });
    }
    CalNetTaxPay();
}
function pageLoad(sender, args) {

    $(document).ready(function () {


        $('#btnDetails1').click(function () {
            $('.show1').toggle();
            if ($('#btnDetails1').val() == 'Hide Details') { $('#btnDetails1').val('Show Details'); return false; }
            if ($('#btnDetails1').val() == 'Show Details') { $('#btnDetails1').val('Hide Details'); }
        });
        $('#btnDetails2').click(function () {
            $('.show2').toggle();
            if ($('#btnDetails2').val() == 'Hide Details') { $('#btnDetails2').val('Show Details'); return false; }
            if ($('#btnDetails2').val() == 'Show Details') { $('#btnDetails2').val('Hide Details'); }
        });
        $('#btnDetails3').click(function () {
            $('.show3').toggle();
            if ($('#btnDetails3').val() == 'Hide Details') { $('#btnDetails3').val('Show Details'); return false; }
            if ($('#btnDetails3').val() == 'Show Details') { $('#btnDetails3').val('Hide Details'); }
        });


    });
}
function CalNetTaxPay() {

    if ($('#txtIncome').val() != '') {

        var total = parseFloat($('#txtIncome').val());
        total = total - parseFloat($('#txtTotalPTax').val());
        total = total - parseFloat($('#txtTotHRA').val());
        total = total - parseFloat($('#txtTotIHBL').val());
        total = total + parseFloat($('#txtDetails1').val());
        total = total - parseFloat($('#txtTotalDeduction').val());
        if (total == "NaN") {
            $('#txtTaxableIncome').val(0);
        }
        else {
            $('#txtTaxableIncome').val(total);
        }
        var Amount = "";
        var Gender = "";
        if ($('#txtTaxableIncome').val() == "NaN") { Amount = 0; } else { Amount = $('#txtTaxableIncome').val(); }
        if ($('#txtGender').val() == 'Male') { Gender = 'M'; } else { Gender = 'F'; }
        var Educess = '';

        var W = "{TaxableIncome:'" + Amount + "',FinYear:'" + $('#txtSalaryFinYear').val() + "',Gender:'" + Gender + "'}";

        $.ajax({
            type: "POST",
            url: "ITaxEmployeeDeductionDetails.aspx/TaxLiabilies",
            data: W,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d);
                var TaxLi = data.ITaxLi;
                var rebate = data.rebate;
                var EduCess = data.EduCess;
                var SecCess = data.SecCess;



                if (TaxLi == 'NaN') {
                    $('#txtOnNetIncome').val(0);
                }
                else { $('#txtOnNetIncome').val(Math.round(TaxLi)); }
                //Rebate
                if (rebate == 'NaN') {
                    $('#txtRabate').val(0);

                }
                else { $('#txtRabate').val(Math.round(rebate)); }
                //EduCess
                if (EduCess == 'NaN') {
                    $('#txtEduCess').val(0);

                }
                else { $('#txtEduCess').val(Math.round(EduCess)); }
                //HDCess
                if (SecCess == 'NaN') {
                    $('#txtHDCess').val(0);

                }
                else { $('#txtHDCess').val(Math.round(SecCess)); }

               

                //if ($('#txtOnNetIncome').val() != '') {
                //    if ($('#txtTaxableIncome').val() <= 500000) {
                //        $('#txtRabate').val(5000);
                //        Educess = Math.round(parseFloat($('#txtOnNetIncome').val()) - parseFloat($('#txtRabate').val()));
                //        CalEduCess(Educess);
                //    }
                //    else {
                //        $('#txtRabate').val(0);
                //        Educess = Math.round(parseFloat($('#txtOnNetIncome').val()) - parseFloat($('#txtRabate').val()));
                //        CalEduCess(Educess);
                //    }
                //}
                TotTaxLi();
            }
        });
    }
}
function NumberOnly() {
    var AsciiValue = event.keyCode
    if ((AsciiValue >= 48 && AsciiValue <= 57) || (AsciiValue == 8 || AsciiValue == 127))
        event.returnValue = true;
    else
        event.returnValue = false;
}
function Calcutate5(ctrl) {
    var total = 0;
    $('.cal3').each(function (evt) {
        //this.value = this.value.replace(/[^0-9\.]/g, '');
        var ThisTotal = '';
        if ($(this).val() == '') { ThisTotal = 0; } else { ThisTotal = $(this).val(); }
        total = parseFloat(ThisTotal) + total;
        $('#txtDetails1').val(Math.round(total));
    });
    CalNetTaxPay();
    TotTaxLi();
}
function Calcutate(ctrl) {
    var total = 0;
    var hddn = $(ctrl).siblings('#txtGAmounthid');
    var HamountId = ".txtHAmountChk" + hddn.val();
    var clssatextbox = ".atextbox" + hddn.val();
    var lblchk = "#lblchk" + hddn.val();
    var lblTotal = ".lblTotal" + hddn.val();
    var ChildMaxAmount = ".itaxSaveMaxAmount" + hddn.val();
    
    $(clssatextbox).each(function (ctrl) {
        var ThisTotal = '';
        if ($(this).val() == '') { ThisTotal = 0; } else { ThisTotal = $(this).val(); }
        total = parseFloat(ThisTotal) + total;
        TotTaxLi();
    });
    //**********************************************************************
    var cntr = $(ctrl);
    var ItaxSavingsAmount = 0;
    var ItaxSavingsAmountmp = cntr.parent().parent().find('td').find(".txtevent").val();
    if (ItaxSavingsAmountmp == '') {
        ItaxSavingsAmount = 0;
    }
    else {
        ItaxSavingsAmount = ItaxSavingsAmountmp;

    }
    var TaxTypeId = cntr.parent().parent().find('td').find("#HideChildID").val();
    var hidid = cntr.parent().parent().find('td').find("#txtGAmounthid").val();
    var Data = { ItaxSavingsAmount: ItaxSavingsAmount, TaxTypeId: TaxTypeId, lblTotal: Math.round(total) };
    $.ajax({
        type: "POST",
        url: 'ITaxEmployeeDeductionDetails.aspx/CheckMaxAmount',
        data: JSON.stringify(Data),
        cache: false,
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (D) {
            var data = JSON.parse(D.d);
            cntr.parent().parent().find('td').find(ChildMaxAmount).text(data.maxamt);
            $(lblchk).html(Math.round(data.totalamt));
            $(lblTotal).text(total);
            $(HamountId).val(total);
        }
    });
    //**********************************************************************
    var total1 = 0;
    
    $('.chklbl').each(function (ctrl) {
        var ThisVal1 = 0;
        if ($(this).text() == '') { $(this).text(0); } else {
            ThisVal1 = $(this).text();
        }
        if (ThisVal1 > 150000) {
            total1 = 150000 + total1;
        }
        else {
            total1 = parseFloat(ThisVal1) + total1;
        }
        $('#txtTotalDeduction').val(Math.round(total1));
    });
    CalNetTaxPay();
}
//This is for only NUMERIC VALUE Entry for All Grid TextBox.
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
$(document).ready(function () {


    $("#ddlSector").change(function () {
        if ($("#txtGender").val() != '') {
            location.href = location.href;
        }
    });

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    $(".txtpress").keypress(function (event) {
        if (event.keyCode === 13) {
            return false;
            event.preventDefault(); // Ensure it is only this code that rusn
        }
    });
    $(".textbox").keypress(function (event) {
        if (event.keyCode === 13) {
            return false;
            event.preventDefault(); // Ensure it is only this code that rusn
        }
    });
});
//This is add button click event
$(document).ready(function () {
    $('#btnDetails1').click(function () {
        $('.show1').toggle();
        if ($('#btnDetails1').val() == 'Hide Details') { $('#btnDetails1').val('Show Details'); return false; }
        if ($('#btnDetails1').val() == 'Show Details') { $('#btnDetails1').val('Hide Details'); }
    });
    $('#btnDetails2').click(function () {
        $('.show2').toggle();
        if ($('#btnDetails2').val() == 'Hide Details') { $('#btnDetails2').val('Show Details'); return false; }
        if ($('#btnDetails2').val() == 'Show Details') { $('#btnDetails2').val('Hide Details'); }
    });
    $('#btnDetails3').click(function () {
        $('.show3').toggle();
        if ($('#btnDetails3').val() == 'Hide Details') { $('#btnDetails3').val('Show Details'); return false; }
        if ($('#btnDetails3').val() == 'Show Details') { $('#btnDetails3').val('Hide Details'); }
    });
    $('#btnDetails4').click(function () {
        $('.show4').toggle();
        if ($('#btnDetails4').val() == 'Hide Details') { $('#btnDetails4').val('Hide Details'); return false; }
        if ($('#btnDetails4').val() == 'Show Details') { $('#btnDetails4').val('Show Details'); }
    });
    $("#btnViewTotCalculation").hide();
    $("#cmdAdd").click(function (event) {
        var taxTypeVal = $("#ddlTaxType").val();
        var SavingTypeVal = $("#ddlSavingType").val();
        var SavingAmt = $("#txtSavingAmount").val();

        var taxType = $('#ddlTaxType').find('option:selected').text();
        var SavingType = $('#ddlSavingType').find('option:selected').text();

        var TotalGPF = $("#hdnTotalGPF").val();

        if (taxTypeVal == "")
            $("#ddlTaxType").focus();
        else if (SavingTypeVal == "")
            $("#ddlSavingType").focus();
        else if (SavingAmt == "")
            $("#txtSavingAmount").focus();
        else {
            var F = "{taxTypeVal: " + taxTypeVal + ", taxType: '" + taxType + "', SavingTypeVal: '" + SavingTypeVal + "', SavingType: '" + SavingType + "', SavingAmt: '" + SavingAmt + "', TotalGPF:'" + TotalGPF + "'}";
            //alert(F);
            $.ajax({
                type: "POST",
                url: "ITaxEmployeeDeductionDetails.aspx/AddDetails",
                data: F,
                contentType: "application/json; charset=utf-8",
                success: function (D) {

                    $("#ITaxEmployeeDetail").html(D.d);
                    $("#btnViewTotCalculation").show();
                    $("#ddlTaxType").val('');
                    $("#ddlSavingType").val('');
                    $("#txtSavingAmount").val('');

                    ShowTotalSumofITaxType(taxTypeVal, taxType, SavingTypeVal, SavingType, SavingAmt);

                }
            });
        }
    });
});
function ShowTotalSumofITaxType(taxTypeVal, taxType, SavingTypeVal, SavingType, SavingAmt) {
    var ItaxMaxAmount = $("#hdnITaxTypeMaxAmount").val();
    var TotalGPF = $("#hdnTotalGPF").val();
    var MaleFemale = $("#hdnEmpGender").val();
    var SalYear = $("#txtAssessmentYear").val();


    var F = "{taxTypeVal: " + taxTypeVal + ", taxType: '" + taxType + "', SavingTypeVal: '" + SavingTypeVal + "', SavingType: '" + SavingType + "', SavingAmt: '" + SavingAmt + "', ItaxMaxAmount:'" + ItaxMaxAmount + "', TotalGPF:'" + TotalGPF + "', MaleFemale:'" + MaleFemale + "', SalYear:'" + SalYear + "'}";
    $.ajax({
        type: "POST",
        url: "ITaxEmployeeDeductionDetails.aspx/ItaxTypewiseTotal",
        data: F,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var strmsg = jQuery.parseJSON(D.d); //alert(strmsg);
            $("#Itax").html(strmsg);
        }
    });
}
//=========This is for add section on button click========//
$(document).ready(function () {
    $('#btnSectionAdd').click(function () {
        var Gender = "";
        var Type = "";
        if (document.getElementById("rbValue").checked == false && document.getElementById("rbPercent").checked == false) {
            alert("Please Select Type !");
            document.getElementById("rbValue").focus();
            return false;
        }
        if (document.getElementById("rbValue").checked == true) { Type = 'V' } else { Type = 'P'; }
        if (Type == 'P') {
            if (parseInt($('#txtMaxAmount').val()) > 100) {
                alert("Percent should not be greater than 100 !");
                $('#txtMaxAmount').val(100);
                return false;
            }
            if ($('#txtPMaxAmount').val()== "") {
                alert("Please Enter Max Amount!");
                $('#txtPMaxAmount').focus();
                return false;
            }
        }
        if ($('#txtGender').val() == 'Male') { Gender = 'M'; } else { Gender = 'F'; }
        if ($('#txtNewSection').val() == '') { alert("Please Enter Section!"); $('#txtNewSection').focus(); return false; } else {
            if ($('#txtMaxAmount').val() == '') { alert("Please Enter Max Amount!"); $('#txtMaxAmount').focus(); return false; } else {
                $.ajax({
                    type: "POST",
                    url: "ITaxEmployeeDeductionDetails.aspx/AddNewSection",
                    data: "{Section:'" + $('#txtNewSection').val() + "',MaxAmount:" + $('#txtMaxAmount').val() + ",FinYear:'" + $('#txtSalaryFinYear').val() + "',Gender:'" + Gender + "',hdnEmpID:'" + $('#hdnEmpID').val() + "',Type:'" + Type + "',MaxPercentageAmount:'" + $('#txtPMaxAmount').val() + "'}",
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        if (D.d == 'Fail') { alert('Unable to process request, please try again !'); } else {
                            var xmlDoc = $.parseXML(D.d);
                            var xml = $(xmlDoc);
                            var BindGrid = xml.find("Table");
                            var ID = 'lblchk' + $(BindGrid).find('SrNo').text();
                            var lblTotalClass = 'lblTotal' + $(BindGrid).find('ItaxTypeID').text();
                            $('#GridTaxDetails').append(
                                "<tr style='width:30px'><td>"  + "<label id='lblSerial' runat='server'>" + $(BindGrid).find('SrNo').text() + "</label>" +
                                "</td><td style='display:none'>" + "<input runat='server' type='Text' id='txtCheck" + $(BindGrid).find('SrNo').text() + "'  value= '0'></input>" +
                                "</td><td style='display:none'>" + "<input runat='server' type='Text' id='lblId'  value= " + $(BindGrid).find('ItaxTypeID').text() + "></input>" +
                                "</td><td align='center'>" + "<label id='lblDesc' runat='server'>" + $(BindGrid).find('TypeDesc').text() + "</label>" +
                                "</td><td align='center'>" + "<input type='button'  ID='btnDetails4'  OnClick='checkButton(this)' class='Btnclassname DefaultButton ddd' style='Width:90px' value='Show Details' />" +
                                //"</td><td >" + "<label style='' id='lableMaxType' runat='server'>" + $(BindGrid).find('MaxType').text() + "</label><asp:Button ID='btnDetails4' runat='server' ClientIDMode='Static' OnClientClick='checkButton(this)' CssClass='Btnclassname DefaultButton ddd' Width='90px' Text='Show Details' />" +
                                //"</td><td align='right'>" + "<label id='lableMaxAmt' style='display:none' runat='server'>" + $(BindGrid).find('MaxAmount').text() + "</label>" +
                                "</td><td align='right'>" + "<label class='chklbl' id='" + ID + "' runat='server'>" + $(BindGrid).find('lblAmount').text() + "</label> <input Style='text-align: right' runat='server' onkeypress='return NumberOnly()' Class='textbox allownumericwithdecimal txtHAmountChk" + $(BindGrid).find('SrNo').text() +
                                "' MaxLength='10' type='Text' id='txtHAmount' onkeyup=CkeckAmount($(this).val(),'" + $(BindGrid).find('MaxType').text() + "',\'" + $(BindGrid).find('MaxAmount').text() + "',\'" + ID + "') value='' disabled></input>" + "</td>"+
                                "<td></td></tr>");
                            $('#GridTaxDetails').append(
                                "<tr align='right'>"+
                                    "<td colspan='7'>" +
                                        "<div class='show4 " + $(BindGrid).find('ItaxTypeID').text() + "' style='max-height: 200px; overflow: auto; width: 100%; display: block;'> " +
                                            "<div>" +
                                                    "<table class='Grid1 " + $(BindGrid).find('ItaxTypeID').text() + "' cellspacing='0' cellpadding='0' rules='all' border='1' id='gv80C80CCD' style='font-weight: normal; border-collapse: collapse; overflow: auto; display: table;'>" +
                                                        "<tbody>"+
                                                        "<tr class='FixedHeader' style='background-color:#FFFF99;'>" +
                                                        "<th class='HideColumn' scope='col'>Serial number</th><th scope='col'>Serial number</th><th scope='col'>Saving Type</th><th scope='col' style='font-weight:normal;'>Amount</th>" +
                                                        "</tr>" +
                                                            "<tr>" +
                                                             "<td colspan='4'>No Records Found</td>"+
                                                            "</tr>" +
                                                            "<tr>" +
                                                             "<td class='HideColumn'>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style='background-color:AntiqueWhite;'>" +
                                                                "<span id='Label1' style='font-weight:bold;'>Total = </span>" +
                                                                "<span id='lblTotal1' class=" + lblTotalClass + " style='font-weight:bold;text-align: right'>0</span>" +
                                                             "</td"+
                                                            "</tr>"+
                                                         "</tbody>"+
                                                    "</table>" +
                                                "</div>"+
                                            "<table style='width:100%' id='tblNewSavingTypeAdd'>"+
                                                    "<tbody><tr>"+
                                                        "<td style='padding: 4px;width:110px'>" +
                                                            "<span class='headFont'>Abbv&nbsp;&nbsp;</span><span class='require'>*</span> "+
                                                            "<input name='' type='text' maxlength='50' id='txtAbbreviation' class='textbox' style='text-transform: uppercase;width:100px'>" +
                                                        "</td>"+
                                                        "<td style='padding: 4px;;width:150px'>" +
                                                            "<span class='headFont'>Description&nbsp;&nbsp;</span><span class='require'>*</span> "+
                                                            "<input name='' type='text' maxlength='50' id='txtDescription' class='textbox' style='text-transform: uppercase;'>"+
                                                        "</td>" +
                                                        "<td style='padding: 4px;width:150px'>"+
                                                             "<input type='radio' id='rdSavingsValue' name='gender' onchange='rdValuePercentageNewSavingsType(this)' value='V'  class='rdSavingsValue'> Value<br>"+
                                                             "<input type='radio' id='rdSavingsPercentage' name='gender' onchange='rdValuePercentageNewSavingsType(this)'  value='P' class='rdSavingsPercentage'> Percentage<br>"+
                                                        "</td>"+
                                                        "<td style='padding: 4px;width:130px;display:none' class='hidetdRdbWise'>"+
                                                            "<span class='headFont chngClass'>Percentage(%)</span><span class='require'>*</span>"+ 
                                                            "<input type='textbox' ID='txtPercentage' Style='text-transform: uppercase;' onkeypress='return NumberOnly()'  CssClass='textbox' MaxLength='50'></asp:TextBox>"+
                                                        "</td>"+
                                                        "<td style='padding: 4px;width:130px'>" +
                                                            "<span class='headFont'>Max Amount&nbsp;&nbsp;</span><span class='require'>*</span> "+
                                                            "<input name='' type='text' maxlength='50' id='txtMaxAmountsaving' class='textbox' onkeypress='return NumberOnly()' style='text-transform: uppercase;'>"+
                                                            "<input type='hidden' id='IDTaxType' value=" + $(BindGrid).find('ItaxTypeID').text() + ">"+
                                                        "</td>"+
                                                        "<td style='padding: 4px; width: 100px' align='center'>"+
                                                            "<span> New</span>"+
                                                            "<input type='button' name='' value='Add' class='Btnclassname DefaultButton btnAddNewSavingType'  style='width:80px;'>"+
                                                        "</td>"+
                                                    "</tr>"+
                                                "</tbody>"+
                                            "</table>" +

                                        "+</div>"+
                                    "</td>"+
                                 "</tr>"
                                );
                            // " + $(BindGrid).find('Amount').text() + "
                            $('#GridTaxDetails').addClass('Grid1');
                            document.getElementById('rbValue').checked = true;
                            $('#txtNewSection').val('');
                            $('#txtMaxAmount').val('');
                            $('#txtPMaxAmount').val('');
                            $('.Max').text('Max  Amount  ');
                            $('.PMax').hide();
                            $('#txtPMaxAmount').hide();
                            $('.mndtry').hide();
                        }
                    }
                });
            }
        }

    });
});

//===================================================== -- Save Data -- =========================================================//
var Table = [];
var Table2 = [];
var Table3 = [];
var Table4 = [];
$(document).ready(function () {
    $("#btnSave").click(function (e) {
      
        e.preventDefault();
        if ($('#ddlSector').val() == 0) {
            alert("Please select Sector...!");
            $('#ddlSector').focus();
            return false;
        }
        if ($('#hdnEmpID').val() == '') {
            alert("Please Enter Employee Number and Click Show Button...!");
            $('#txtEmpNo').focus();
            return false;
        }
        $('.loading-overlay').show();
        var tblchild = $("#gv80C80CCD");
        var tblParent = $("#GridTaxDetails");
        var tblChallan = $("#GridViewOther");
        var TableSourceOfOtherIncome = $("#tblSourceFromOtherIncome");

        $("#tblSourceFromOtherIncome tr").each(function(index,value) { 
                var OtherSourceId = $(this).find("td").eq(1).html();
                var AmountFinal = 0;
                var Amount = $(this).find("td").find(".txtOthersrcIncAmt").val();
                if (Amount !='')
                {
                    AmountFinal = Amount;
                }
                Table4.push({ 'OtherSourceId': OtherSourceId, 'Amount': AmountFinal });
        });


        var rowCount = tblChallan.find("tr").length;
        var i = 0;
        $.each(tblChallan.find("tr"), function (index, value) {
            if (index != 0) {
                var ChallanNo = $(this).find("td").eq(0).html();
                var BSRNo = $(this).find("td").eq(1).html();
                var ChallanDate = $(this).find("td").eq(2).html();
                var Amount = $(this).find("td").eq(3).html();

                Table3.push({ 'ChallanNo': ChallanNo, 'BSRNo': BSRNo, 'ChallanDate': ChallanDate, 'Amount': Amount });
                i++;
            }
        });
        getJsonObjectFromTable(tblchild);
        getJsonObjectFromTable2(tblParent);
    });
});
function getJsonObjectFromTable(table) {
    var TableParent = $("#GridTaxDetails");
    $.each(TableParent.find("tr"), function (index, value) {
        var FindPrntSt = $(this).find("td").find(".show4 ").find("#gv80C80CCD");
        if (FindPrntSt.length>0)
        {
            var sd = $(FindPrntSt).find('tr').eq(1).find('td').text();
            if (sd != "No Records Found") {  
                var rowCount = FindPrntSt.find("tr").length;
                $.each(FindPrntSt.find("tr"), function (index, value) {
                    if (index != rowCount - 1 && index != 0) {
                        var ChildID = $(this).find("input[id*=HideChildID]").val();
                        var ChildlblMaxAmt = $(this).find("#itaxSaveMaxAmount").text();
                        var ChildAmt = $(this).find("input[id*=txtGAmount]").val();
                        if (ChildAmt > 0) {
                            Table.push({ 'ChildID': ChildID, 'ChildlblMaxAmt': ChildlblMaxAmt, 'ChildAmt': ChildAmt });
                        }
                    }
                });
                console.log(Table);
            }
        }
    });
   
   
}
function getJsonObjectFromTable2(table2) {

    var rowCount = table2.find("tr").length;
    var Object = {};
    $.each(table2.find("tr"), function (index, value) {
        var chevk = $(this).find("td").find("#txtCheck");
        var chevkval = chevk.val();
        var Flag = index % 2;
        //&& chevkval == 0
        //alert(chevkval);
        if (index > 0 && index != 2 && chevkval == 0) {
            var ParentID = $(this).find("input[id*=lblId]").val();
            var ParentlblMaxAmt = $(this).find(".chklbl").text();
            var ParentAmt = $(this).find("input[id*=txtHAmount]").val();
            if (ParentAmt > 0)
            {
                Table2.push({ 'ParentID': ParentID, 'ParentlblMaxAmt': ParentlblMaxAmt, 'ParentAmt': ParentAmt });
            }
        }
    });
    //console.log(Table2);

    var hdnEmpID = $('#hdnEmpID').val();
    var HiddenCheckGrid = $('#HiddenCheckGrid').val();
    var hdnEmpGender = $('#hdnEmpGender').val();
    var hdnTotalGrossPay = $('#hdnTotalGrossPay').val();
    var hdnTotalPtax = $('#hdnTotalPtax').val();
    var hdnTotalITax = $('#hdnTotalITax').val();
    var hdnTotalGIS = $('#hdnTotalGIS').val();
    var hdnTotalGPF = $('#hdnTotalGPF').val();
    var hdnTotalHRA = $('#hdnTotalHRA').val();
    var hdnTotalGPFRec = $('#hdnTotalGPFRec').val();
    var hdnTotalIHBL = $('#hdnTotalIHBL').val();
    var hdnTotalHBL = $('#hdnTotalHBL').val();
    var hdnTotalOtherDe = $('#hdnTotalOtherDe').val();
    var hdnTotalNetPay = $('#hdnTotalNetPay').val();
    var hdnTotalArrearOT = $('#hdnTotalArrearOT').val();


    var EmpID = $('#hdnEmpID').val();
    var FinYear = $('#txtSalaryFinYear').val();
    var AssessmentYear = $('#txtAssessmentYear').val();
    var IncomeFromSalary = ''; if ($('#txtIncome').val() == '') { IncomeFromSalary = 0; } else { IncomeFromSalary = $('#txtIncome').val(); }
    var InterestFromSaving = '0'; //if ($('#txtInterestFromSaving').val() == '') { InterestFromSaving = 0; } else { InterestFromSaving = $('#txtInterestFromSaving').val(); }
    var InterestFromFD = '0'; //if ($('#txtInterestFromFD').val() == '') { InterestFromFD = 0; } else { InterestFromFD = $('#txtInterestFromFD').val(); }
    var InterestFromOther = '0';// if ($('#txtOtherS').val() == '') { InterestFromOther = 0; } else { InterestFromOther = $('#txtOtherS').val(); }
    var TotDeduChapVIA = ''; if ($('#txtTotalDeduction').val() == '') { TotDeduChapVIA = 0; } else { TotDeduChapVIA = $('#txtTotalDeduction').val(); }
    var TotTaxableIncome = ''; if ($('#txtTaxableIncome').val() == '') { TotTaxableIncome = 0; } else { TotTaxableIncome = $('#txtTaxableIncome').val(); }
    var TaxOnNetIncome = ''; if ($('#txtOnNetIncome').val() == '') { TaxOnNetIncome = 0; } else { TaxOnNetIncome = $('#txtOnNetIncome').val(); }
    var RabateSec87A = ''; if ($('#txtRabate').val() == '') { RabateSec87A = 0; } else { RabateSec87A = $('#txtRabate').val(); }
    var Surcharge = ''; if ($('#txtSurcharge').val() == '') { Surcharge = 0; } else { Surcharge = $('#txtSurcharge').val(); }
    var EducationCess = ''; if ($('#txtEduCess').val() == '') { EducationCess = 0; } else { EducationCess = $('#txtEduCess').val(); }
    var HSEducationCess = ''; if ($('#txtHDCess').val() == '') { HSEducationCess = 0; } else { HSEducationCess = $('#txtHDCess').val(); }
    var TaxLiabilities = ''; if ($('#txtTaxLi').val() == '') { TaxLiabilities = 0; } else { TaxLiabilities = $('#txtTaxLi').val(); }
    var ReliefUS89 = ''; if ($('#txtUS89').val() == '') { ReliefUS89 = 0; } else { ReliefUS89 = $('#txtUS89').val(); }
    var TaxPayabl = ''; if ($('#txtNetTaxPay').val() == '') { TaxPayabl = 0; } else { TaxPayabl = $('#txtNetTaxPay').val(); }
    var OtherITAX = ''; if ($('#txtOtherSource').val() == '') { OtherITAX = 0; } else { OtherITAX = $('#txtOtherSource').val(); }
    var BalanceTax = ''; if ($('#txtBalTax').val() == '') { BalanceTax = 0; } else { BalanceTax = $('#txtBalTax').val(); }
    var TableChallan = JSON.stringify(Table3);
    var TableChild = JSON.stringify(Table);
    var TableOtherSourceIncome = JSON.stringify(Table4);
   
    var TableParent = JSON.stringify(Table2);
    var W = "{TableChild: " + TableChild + ",TableParents: " + TableParent + ",TableChallan:" + TableChallan + ",FinYear:'" + FinYear + "',AssessmentYear:'" + AssessmentYear + "',EmpID:" + EmpID + ",hdnEmpGender:'" + hdnEmpGender + "',hdnTotalGrossPay:'" + hdnTotalGrossPay +
        "',hdnTotalPtax:'" + hdnTotalPtax + "',hdnTotalITax:'" + hdnTotalITax + "',hdnTotalGIS:'" + hdnTotalGIS + "',hdnTotalHRA:'" + hdnTotalHRA + "',hdnTotalGPF:'" + hdnTotalGPF + "',hdnTotalGPFRec:'" + hdnTotalGPFRec +
        "',hdnTotalHBL:'" + hdnTotalHBL + "',hdnTotalIHBL:'" + hdnTotalIHBL + "',hdnTotalHBL:'" + hdnTotalHBL + "',hdnTotalOtherDe:'" + hdnTotalOtherDe + "',hdnTotalNetPay:'" + hdnTotalNetPay +
        "',hdnTotalArrearOT:'" + hdnTotalArrearOT + "',IncomeFromSalary:'" + IncomeFromSalary + "',InterestFromSaving:'" + InterestFromSaving + "',IncomeFromSalary:'" + IncomeFromSalary +
        "',InterestFromFD:'" + InterestFromFD + "',InterestFromOther:'" + InterestFromOther + "',TotDeduChapVIA:'" + TotDeduChapVIA + "',TotDeduChapVIA:'" + TotDeduChapVIA + "',TotTaxableIncome:'" + TotTaxableIncome +
        "',TaxOnNetIncome:'" + TaxOnNetIncome + "',RabateSec87A:'" + RabateSec87A + "',Surcharge:'" + Surcharge + "',EducationCess:'" + EducationCess + "',HSEducationCess:'" + HSEducationCess +
        "',TaxLiabilities:'" + TaxLiabilities + "',ReliefUS89:'" + ReliefUS89 + "',TaxPayabl:'" + TaxPayabl + "',OtherITAX:'" + OtherITAX + "',BalanceTax:'" + BalanceTax + "',TableOtherSourceIncome:" + TableOtherSourceIncome + "}";

    $.ajax({
        type: "POST",
        url: "ITaxEmployeeDeductionDetails.aspx/SaveData",
        data: W,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (d) {
            Table = [];
            Table2 = [];
            Table3 = [];
            Table4 = [];
            $("#GridViewOther tr:gt(0)").remove();
            $('#txtOtherSource').val('');
            alert(d.d);
            window.location.href = "ITaxEmployeeDeductionDetails.aspx";
        },
        failure: function (response) {
            alert("Wrong");
        }
    });
}
//***********************Supriyo Ghosh (27-03-2018)*****************************//
$(document).ready(function () {
    $(document).on('change', '#txtSalaryFinYear', function () {
        var Data = { salaryFinYear: $("#txtSalaryFinYear").val() };
        $.ajax({
            type: "POST",
            url: 'ITaxEmployeeDeductionDetails.aspx/GetACCyearBySalFinYear',
            data: JSON.stringify(Data),
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d);
                if (data[0]["ErrorCode"] == "1") {
                    $("#txtAssessmentYear").val(data[0]["Assessment"]);
                }
                if (data[0]["ErrorCode"] == "0") {
                    alert(data[0]["Messege"]);
                    $("#txtSalaryFinYear").focus();
                    $("#txtSalaryFinYear").val('');
                    $("#txtAssessmentYear").val('');

                }
            }
        });
    });
    $(document).on('click', '#btnAddNewSrcIncome', function () {
        if ($("#txtAddNewOtherNewSourceIncome").val() == "") {
            return false;
        }
        else {
            if (confirm("Do you want to save !")) {
                SaveOtherIncomeSource();
            }
        }
    });
    $(document).on('click', '.btnAddNewSavingType', function () {
        var cntr = $(this);
        var TaxTypeId = cntr.parent().parent().find('td').find("#IDTaxType").val();
        AddNewSavingType(cntr,TaxTypeId);
    });
    $(document).on('change', '.txtevent', function () {
        var cntr = $(this);
        ValidateMaxAmountItaxType(cntr);
    });
});
function GetOtherSourceIncomeDetail() {
    var F = "{SalaryFinYear :'" + $("#txtSalaryFinYear").val() + "',AssessmentYear:'" + $("#txtAssessmentYear").val() + "',hdnEmpID:'" + $('#hdnEmpID').val() + "'}";
    $("#tblSourceFromOtherIncome tbody").empty();
    $.ajax({
        type: "POST",
        url: 'ITaxEmployeeDeductionDetails.aspx/PopulateTableValue',
        data: F,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.d.length > 0) {
                var data = JSON.parse(D.d);
                var i = 1;
                $.each(data, function (index, value) {
                    $("#tblSourceFromOtherIncome tbody").append("<tr " + (index % 2 == 0 ? "" : "style='background-color:#d4e7ee; solid gray'") + ">" +
                         "<td style='padding: 4px; background-color: #b1bee0;width: 15px;'>" + i + "</td>" +
                         "<td align='center' class='OtherSourceId' style='display:none'>" + value.OtherSourceId + "</td>" +
                         "<td align='center' class='OtherSourceName' style='padding-left: 10px; text-align: left'><label style='color:#003366'>" + value.OtherSourceName + "</label></td>" +
                         "<td style='border-left: 1px solid gray; padding: 4px; width: 253px' align='center'><input type='text' ID='txtInterestFromSaving' value=" + value.OtherIncome + "  ClientIDMode='Static' Style=text-align: right' onkeypress='return NumberOnly()' runat='server' onkeyup='Calcutate5(this)' class='textbox cal3 txtOthersrcIncAmt' MaxLength='10'></td></tr>");
                    i++;
                });
            }       
        }
    });
}
function GetNewAddedRowSourceIncomeDetail() {
    var F = "{SalaryFinYear :'" + $("#txtSalaryFinYear").val() + "',AssessmentYear:'" + $("#txtAssessmentYear").val() + "',hdnEmpID:'" + $('#hdnEmpID').val() + "'}";
    $.ajax({
        type: "POST",
        url: 'ITaxEmployeeDeductionDetails.aspx/PopulateNewAddedTableValue',
        data: F,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.d.length > 0) {
                var data = JSON.parse(D.d);
                var index = parseInt(data[0]["Srlno"]);
                $("#tblSourceFromOtherIncome tbody").append("<tr " + (index % 2 != 0 ? "" : "style='background-color:#d4e7ee; solid gray'") + ">" +
                     "<td style='padding: 4px; background-color: #b1bee0;width: 15px;'>" + data[0]["Srlno"] + "</td>" +
                     "<td align='center' class='OtherSourceId' style='display:none'>" + data[0]["OtherSourceId"] + "</td>" +
                     "<td align='center' class='OtherSourceName' style='padding-left: 10px; text-align: left'><label style='color:#003366'>" + data[0]["OtherSourceName"] + "</label></td>" +
                     "<td style='border-left: 1px solid gray; padding: 4px; width: 253px' align='center'><input type='text' ID='txtInterestFromSaving' value=" + data[0]["OtherIncome"] + "  ClientIDMode='Static' Style=text-align: right' onkeypress='return NumberOnly()' runat='server' onkeyup='Calcutate5(this)' class='textbox cal3 txtOthersrcIncAmt' MaxLength='10'></td></tr>");
            }
        }
    });
}
function SaveOtherIncomeSource() {
    var Data = { OtherSourceName: $("#txtAddNewOtherNewSourceIncome").val() };
    $.ajax({
        type: "POST",
        url: 'ITaxEmployeeDeductionDetails.aspx/SaveMediclaimDetails',
        data: JSON.stringify(Data),
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var data = JSON.parse(D.d);
            if (data.MessageCode == 1) {
                GetNewAddedRowSourceIncomeDetail();
                $("#txtAddNewOtherNewSourceIncome").val("");
            }
            if (data.MessageCode == 0) {
                alert(data.Message);
            }
        }
    });
}
function AddNewSavingType(t,TaxTypeId) {
    var Abbre = $(t).parent().parent().find('td').find("#txtAbbreviation").val();
    var txtDescription = $(t).parent().parent().find('td').find("#txtDescription").val();
    var MaxAmount = $(t).parent().parent().find('td').find("#txtMaxAmountsaving").val();
    var Percentage = $(t).parent().parent().find('td').find("#txtPercentage").val();
    var hdnEmpID = $('#hdnEmpID').val();
    var Type = "";
    if ($(t).parent().parent().find('td').find(".rdSavingsPercentage:checked").length == 0 && $(t).parent().parent().find('td').find(".rdSavingsValue:checked").length == 0) {
        alert("Please Select Type !");
        //document.getElementById("rdSavingsValue").focus();
        return false;
    }
    if ($(t).parent().parent().find('td').find(".rdSavingsPercentage:checked").length >0 ) { Type = 'P' } else { Type = 'V'; }
    if (Abbre=="")
    {
        $(t).parent().parent().find('td').find("#txtAbbreviation").focus();
        alert("Please Select Abbreviation !");
        return false;
    }
    if (txtDescription=="")
    {
        alert("Please Select Description !");
        $(t).parent().parent().find('td').find("#txtDescription").focus();
        return false;
    }
    if (MaxAmount == "") {
        alert("Please enter Max Amount !");
        $(t).parent().parent().find('td').find("#MaxAmount").focus();
        return false;
    }
    if (Type=="P")
    {
        if (Percentage == "") {
            alert("Please enter Percentage !");
            $(t).parent().parent().find('td').find("#txtPercentage").focus();
            return false;
        }
        else if (parseInt(Percentage) > 100) {
            alert("Please enter right Percentage !");
            $(t).parent().parent().find('td').find("#txtPercentage").val(100);
            return false;
        }

    }
    var Data = { Description: txtDescription, TaxTypeId: TaxTypeId, Abbre: Abbre, MaxAmount: MaxAmount, EmpID: hdnEmpID, type: Type, Percentage: Percentage };
    $.ajax({
        type: "POST",
        url: 'ITaxEmployeeDeductionDetails.aspx/SaveNewSavingType',
        data: JSON.stringify(Data),
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var data = JSON.parse(D.d);
            var Datad = data.data;
            var i = 1;
            var classs = "." + TaxTypeId;
            var newclassatextbox = 'atextbox' + TaxTypeId;
            if (data.MessageCode == 1) {
                var sd = $(classs).find('tr').eq(1).find('td').text();
                var Slno =parseInt($(classs).find('tr').length)-2;
                if (sd == "No Records Found")
                {
                    $(classs).find('tr').eq(1).remove();
                    Slno = 1;
                }
                var UsClass = 'itaxSaveMaxAmount' + TaxTypeId;
                $(classs).find('tr:last').prev().after("<tr>" +
                    "<td class='OtherSourceId' style='display:none'><input type='text' ID='HideChildID'  ClientIDMode='Static' Style=text-align: right' value='" + Datad[0]["ItaxSaveID"]  + "' runat='server'></td>" +
                    "<td align='left'><label style='color:#003366' id='lblSerial1'>" + Slno + "</label></td>" +
                    "<td align='center'>" + Datad[0]["SavingTypeDesc"] + "</td>" +
                    "<td align='right'><label ID='itaxSaveMaxAmount' class=" + UsClass + ">0</label><input type='text' Style='text-align: right ;width: 100px;' class='textbox txtevent " + newclassatextbox + " DefaultButton allownumericwithdecimal numeric' ID='txtGAmount'  ClientIDMode='Static' Style=text-align: right' onkeypress='return NumberOnly()' runat='server' onkeyup='Calcutate(this)'><input type='hidden' value=" + TaxTypeId + " ClientIDMode='Static' runat='server' ID='txtGAmounthid'/></td></tr>");
                $(t).parent().parent().find('td').find("#txtAbbreviation").val(' ');
                $(t).parent().parent().find('td').find("#txtDescription").val(' ');
                $(t).parent().parent().find('td').find("#txtMaxAmountsaving").val(' ');
                $(t).parent().parent().find('td').find("#txtPercentage").val(' ');

            }
            if (data.MessageCode == 0) {
                alert(data.Message);
            }
        }
    });
}
function rdValuePercentageNewSavingsType(ctrl)
{
    var currentRDButtonValue = $(ctrl).parent().parent().find('td').find('.rdSavingsPercentage:checked').length;
    //var currentRDButtonPercentage = $(ctrl).parent().parent().find('td').find('.rdSavingsPercentage');
    if (currentRDButtonValue > 0) {
        $('.hidetdRdbWise').show();
    }
    if (currentRDButtonValue == 0) {
        $('.hidetdRdbWise').hide();
        
    }
}
function ValidateMaxAmountItaxType(cntr) {
    
}

//*********************************END*********************************************//