


$(document).ready(function () {

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    $('#txtMemoDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });

    $('#txtMemoNo').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 34 || iKeyCode == 39) {
            alert("This Character Is Not Allowed!");
            return false;
        }
    });

    $('#txtMemoDate').keydown(function (e) {
        e.preventDefault();
        return false;
    });

    var GridItemDetail = document.getElementById("GridEmpPenApp");   //main Grid Initialization!
    GridItemDetail.deleteRow(1);

    $('#txtEmpNoSearch').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            alert("Only Numbers Allowed !");
            return false;
        }
    });
});


$(document).ready(function () {
    BindGridArrearPensionApproval();
});

function BindGridArrearPensionApproval() {
    $(".loading-overlay").show();
    var W = "{}";

    $.ajax({
        type: "POST",
        url: "ArrearPensionApproval.aspx/GET_GridInitialization",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                for (var i = 0; i < data.d.length; i++) {
                    $("#GridEmpPenApp").append("<tr><td>" +
                    "<input type='checkbox' id='chkApp' value='" + data.d[i].EmpNo + "'>" + "</td> <td>" +
                    data.d[i].EmpNo + "</td> <td>" +
                    data.d[i].EmpName + "</td> <td>" +
                    data.d[i].Period + "</td> <td>" +
                    parseFloat(data.d[i].TotalEarning).toFixed(2) + "</td> </tr>");
                }
            }
            $(".loading-overlay").hide();
            $(function () {
                $("[id*=GridEmpPenApp] td").bind("click", function () {
                    var row = $(this).parent();
                    $("[id*=GridEmpPenApp] tr").each(function () {
                        if ($(this)[0] != row[0]) {
                            $("td", this).removeClass("selected_row");
                        }
                    });
                    $("td", row).each(function () {
                        if (!$(this).hasClass("selected_row")) {
                            $(this).addClass("selected_row");
                        } else {
                            $(this).removeClass("selected_row");
                        }
                    });
                });
            });
        }

    });
}

//Start Search Keypress Coding Here
$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtEmpNoSearch').keyup(function () {
        $('#GridEmpPenApp').find('tr:gt(0)').hide();
        var data = $('#txtEmpNoSearch').val();
        var len = data.length;
        if (len > 0) {
            $('#GridEmpPenApp').find('tbody tr').each(function () {
                coldata = $(this).children().eq(1);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        }
        else {
            $('#GridEmpPenApp').find('tr:gt(0)').show();
        }
    });
});

function SaveApproval() {

    if ($("#txtMemoNo").val() == "") {
        alert("Please Select Memo Number!");
        $("#txtMemoNo").focus();
        return false;
    }

    if ($("#txtMemoDate").val() == "") {
        alert("Please Select Memo Date!");
        $("#txtMemoDate").focus();
        return false;
    }

    var ApprovalData = "";
    var Count = 0;
    $('#GridEmpPenApp').find('tbody tr td input[id*="chkApp"][type=checkbox]:checked').each(function () {
        if (ApprovalData == "") {
            ApprovalData = $(this).val();
            Count++;
        }
        else {
            ApprovalData = ApprovalData + ',' + $(this).val();
            Count++;
        }
    });

    var MemoNo = $("#txtMemoNo").val();
    var MemoDate = $("#txtMemoDate").val();
    var con = confirm("Do You Want To Save?");

    if (ApprovalData != "") {
        if (con == true) {
            E = "{ApprovalData:'" + ApprovalData + "',MemoNo:'" + MemoNo + "',MemoDate:'" + MemoDate + "'}";
            $.ajax({
                type: "POST",
                url: 'ArrearPensionApproval.aspx/SaveMasterApproval',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == Count) {
                        alert(Count + " Arrear Permissions Updated Successfully!");
                        ClearFields();
                    }
                    else {
                        alert(Count + " Arrear Permissions Updated Successfully!" + Count - parseInt(data.d) + " Records Failed To Update!");
                        ClearFields();
                    }
                },
                error: function (response) {
                    alert('Connection Error! Please Try Again! [Contact Authorized Person If problem Persist!]');
                    BindGridArrearPensionApproval();
                    return false;
                }
            });
        }
    }

    else {
        alert("Please Select Atleast 1 Employee Arrear Detail To Approve!");
        $('#txtEmpNoSearch').focus();
        return false;
    }
}

function ClearFields() {
    GridItemDetail = document.getElementById("GridEmpPenApp");
    var i = parseInt($("#GridEmpPenApp tr").length);
    i = i - 1;
    while (i > 0) {
        GridItemDetail.deleteRow(i);
        i--;
    }
    BindGridArrearPensionApproval();
    $('#txtEmpNoSearch').val('');
    $('#txtMemoNo').val('');
    $('#txtMemoDate').val('');
}