﻿
var TotalDays = []; var array = []; var ldays = ""; var ArrayNominee = []; var available = false; var arrayTotalGratuity = []; var count = 0;



$(document).ready(function () {

    $(document).ready(function () {
        // alert($("#hdnAfterResult").val());
        var res = $("#hdnAfterResult").val();
        if (res == "NotFOUND") {
            $("#txtNewEmpNo").val($("#txtEmpNo").val());  //txtEmpNo
            $("#overlay").show();
            $("#dialog").fadeIn(300);
        }
        else {
            $("#overlay").hide();
        }
    });
    $(document).ready(function () {
        $('#Close a').live('click', function () {
            $("#overlay").hide();
        });
    });

    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "UpdateOldDeathPensionerDetail.aspx/PensionPercentage",
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var data = JSON.parse(D.d); //alert(JSON.stringify(data));
                if (data.length > 0) {


                    var FiftyPerct = data[0].paramValue; //alert(FiftyPerct);
                    var ThirtyPerct = data[1].paramValue; //alert(ThirtyPerct);

                    $("#lbl30Perct").text(FiftyPerct);
                    $("#lbl50Perct").text(ThirtyPerct);
                }
            }
        });
    });
    //************************************************************************
    //This is for Populate Designation
    $(document).ready(function () {

        var options = {};
        options.url = "UpdateOldDeathPensionerDetail.aspx/PopulateDesignation";
        options.type = "POST";
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (listdesignation) {
            var t = jQuery.parseJSON(listdesignation.d);
            var a = t.length;
            $("#ddlDesig").empty();
            $("#ddlDesig").append("<option value=''>(Select Designation)</option>")
            if (a > 0) {
                $.each(t, function (key, value) {
                    $("#ddlDesig").append($("<option></option>").val(value.DesignationID).html(value.Designations));
                });
            }
            $("#ddlDesig").prop("disabled", false);
        }
        $.ajax(options);

    });
    //************************************************************************
    //This is for Populate Sector
    $(document).ready(function () {

        var options = {};
        options.url = "UpdateOldDeathPensionerDetail.aspx/BindSector";
        options.type = "POST";
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (listSector) {
            var t = jQuery.parseJSON(listSector.d); //alert(JSON.stringify(t));
            var a = t.length;
            $("#ddlSectors").empty();
            $("#ddlSectors").append("<option value=''>(Select Sector)</option>")
            if (a > 0) {
                $.each(t, function (key, value) {
                    $("#ddlSectors").append($("<option></option>").val(value.SectorID).html(value.SectorName));
                });
            }
            $("#ddlSectors").prop("disabled", false);
        }
        $.ajax(options);

    });
    //************************************************************************
    //This is for 'Location' Binding on the Base of 'Sector'
    $(document).ready(function () {
        // $("#ddlPayScaleType").prop("disabled", false);
        $("#ddlSectors").change(function () {
            var s = $('#ddlSectors').val();
            if (s != "") {
                if ($("#ddlSectors").val() != "Please select") {
                    var E = "{SecID: " + $('#ddlSectors').val() + "}";
                    //alert(E);
                    var options = {};
                    options.url = "UpdateOldDeathPensionerDetail.aspx/GetCenter";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listCenter) {
                        var t = jQuery.parseJSON(listCenter.d);
                        var a = t.length;
                        $("#ddlCenter").empty();
                        if (a >= 0) {
                            $("#ddlCenter").append("<option value=''>(Select Center)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        }

                        $("#ddlCenter").prop("disabled", false);
                    };
                    options.error = function () { alert("Error in retrieving Center!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlCenter").empty();
                    $("#ddlCenter").prop("disabled", true);
                }
            }
            else {
                $("#ddlCenter").empty();
                $("#ddlCenter").append("<option value=''>(Select Center)</option>")
            }
        });

    });
    //************************************************************************
    //This is for Populate PayScale
    $(document).ready(function () {

        var options = {};
        options.url = "UpdateOldDeathPensionerDetail.aspx/GetPayScale";
        options.type = "POST";
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (listPayScale) {
            var t = jQuery.parseJSON(listPayScale.d); //alert(JSON.stringify(t));
            var a = t.length;
            $("#ddlPayScale").empty();
            $("#ddlPayScale").append("<option value=''>(Select PayScale)</option>")
            if (a > 0) {
                $.each(t, function (key, value) {
                    $("#ddlPayScale").append($("<option></option>").val(value.PayScaleID).html(value.PayScale));
                });
            }
            $("#ddlPayScale").prop("disabled", false);
        }
        $.ajax(options);

    });
    //************************************************************************
    //This is for Save Popup Save Button Click
    $(document).ready(function () {
        $('#cmdSave').live('click', function () {
            var EmpNo = $("#txtNewEmpNo").val();
            var EmpName = $("#txtEmpNames").val();
            var DesigID = $("#ddlDesig").val(); //alert(DesigID);
            var dob = $("#txtDOBS").val(); //alert(dob);
            var doj = $("#txtDOJS").val(); //alert(doj);
            var dor = $("#txtDORS").val(); //alert(dor);
            var SecID = $("#ddlSectors").val(); //alert(dob);
            var CenID = $("#ddlCenter").val(); //alert(doj);
            var PayscaleID = $("#ddlPayScale").val(); //alert(dor);

            if ($("#txtNewEmpNo").val() == "")
                $("#txtNewEmpNo").focus();
            else if ($("#txtEmpNames").val() == "")
                $("#txtEmpNames").focus();
            else if ($("#ddlDesig").val() == "")
                $("#ddlDesig").focus();
            else if ($("#txtDOBS").val() == "")
                $("#txtDOBS").focus();
            else if ($("#txtDOJS").val() == "")
                $("#txtDOJS").focus();
            else if ($("#ddlSectors").val() == "")
                $("#ddlSectors").focus();
            else if ($("#ddlCenter").val() == "")
                $("#ddlCenter").focus();
            else if ($("#ddlPayScale").val() == "")
                $("#ddlPayScale").focus();
            else {

                var E = "{EmpNo: '" + EmpNo + "', EmpName: '" + $("#txtEmpNames").val() + "', DesigID: '" + $("#ddlDesig").val() + "', dob: '" + $("#txtDOBS").val() + "', doj: '" + $("#txtDOJS").val() + "',  dor: '" + $("#txtDORS").val() + "',  Sector: '" + $("#ddlSectors").val() + "',  Center: '" + $("#ddlCenter").val() + "', PayScaleID: '" + $("#ddlPayScale").val() + "' }"; //alert(E);
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/Insert_NewPensioner",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var data = JSON.parse(D.d); //alert(JSON.stringify(data));
                        if (data.length > 0) {

                            alert("Employee Details are Inserted Successfully.");
                            $("#overlay").hide();
                            BalankField();
                            //var EmpNos = data[0].EmpNo; //alert(EmpNos);
                            //var EmpNames = data[0].PensionerName; //alert(EmpNames);

                            //$("#txtEmpNo").val(EmpNos);
                            //$("#txtEmpName").val(EmpNames);
                        }
                    }
                });
            }
        });
    });
    function BalankField() {
        $("#txtNewEmpNo").val('');
        $("#txtEmpName").val('');
        $("#ddlDesig").val('');
        $("#txtDOB").val('');
        $("#txtDOJ").val('');
        $("#txtDOR").val('');
        $("#ddlSectors").val('');
        $("#ddlCenter").val('');
        $("#ddlPayScale").val('');
    }
    

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "PensionerCalculation.aspx/BindState",
            data: {},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var jsdata = JSON.parse(data.d);
                $("#ddlNomineeState").append("<option value=''>(Select State)</option>")
                $.each(jsdata, function (key, value) {
                    $("#ddlNomineeState").append($("<option></option>").val(value.StateId).html(value.State));
                });
            },
            error: function (data) {
                //alert("error found");
            }
        });
    });
    $(document).ready(function () {
        //$("#ddlDistrict").prop("disabled", true);
        $("#ddlNomineeState").change(function () {

            if ($("#ddlNomineeState").val() != "Please select") {
                var options = {};
                options.url = "PensionerCalculation.aspx/BindDistricts";
                options.type = "POST";
                options.data = "{StateID: " + $('#ddlNomineeState').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listdistrict) {
                    var t = jQuery.parseJSON(listdistrict.d);
                    var a = t.length;
                    $("#ddlNomineeDistrict").empty();
                    if (a > 0) {
                        $("#ddlNomineeDistrict").append("<option value=''>(Select District)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlNomineeDistrict").append($("<option></option>").val(value.DistID).html(value.District));
                        });
                    }
                    $("#ddlNomineeDistrict").prop("disabled", false);
                };
                options.error = function () { alert("Error retrieving Districts!"); };
                $.ajax(options);
            }
            else {
                $("#ddlNomineeDistrict").empty();
                $("#ddlNomineeDistrict").prop("disabled", true);
            }
        });
    });


    $("#nomineetab").click(function (event) {
        $(".loading-overlay").show();
        var E = "{EmpNo: '" + $("#txtEmpNo").val() + "'}";
        $.ajax({
            type: "POST",
            url: "UpdateOldDeathPensionerDetail.aspx/Loan_NomineeDetails", //pageUrl + '/Loan_NomineeDetails',
            data:E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#NomineeDetails').html(D.d);
            }
        });
    });

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $("#ddlRopa").change(function () {
        var ropaid = $("#ddlRopa").val();
        if (ropaid == 4)
        {
            $("#txtRopaRelief").val('');
            document.getElementById("txtRopaRelief").disabled = true;
        }
        else
            document.getElementById("txtRopaRelief").disabled = false;
    });

   /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                    //Pensioner Case with File No. Tab Details 

    $("#btnPenDetNext").click(function (event) {
     
        var FileNo = $("#txtFileNo").val(); 
        var PensionScheme = $("#ddlPensionScheme").val(); 
        var SlNo = $("#txtSlNo").val(); 
        var PageNo = $("#txtPageNo").val(); 
        var VolNo = $("#txtVolNo").val(); 
        var OpLocNo = $("#txtOpLocNo").val();

        var isAlive = $("#ddlIsAlive").val();
        var DateofExp = $("#txtExpiryDate").val();

        if (FileNo == "")
        {
            var msg = "Please Enter Case File No.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "fileNo";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
      else  if (PensionScheme == "") {
            var msg = "Please Select Pension Scheme.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "PensionScheme";
                    $('#hdnDialogResult').val(res);
                }
            });
      }
      else if (SlNo == "") {
          var msg = "Please Enter Sl No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "SlNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (PageNo == "") {
          var msg = "Please Enter Page No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "PageNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (VolNo == "") {
          var msg = "Please Enter Volume No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "VolNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (OpLocNo == "") {
          var msg = "Please Enter Opting Location No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "OpLocNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (isAlive == 1 && DateofExp=="") {
          var msg = "Please Enter Date of Expiry.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "dateofexpiry";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else {

          var LocationNo = $("#txtLocationCode").val();
          var A = "{LocationNo: '" + LocationNo + "'}";
          $(".loading-overlay").show();
          $.ajax({
              type: "POST",
              url: pageUrl + '/Check_LocationAvailability',
              data: A,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $(".loading-overlay").hide();
                  var da = JSON.parse(D.d); //alert(JSON.stringify(da));
                  if (da == "Available") {

                      var E = "{FileNo: '" + FileNo + "', PensionScheme: '" + PensionScheme + "', SlNo: '" + SlNo + "', PageNo: '" + PageNo + "', VolNo: '" + VolNo + "', OpLocNo: '" + OpLocNo + "', isAlive: '" + isAlive + "', DateofExp: '" + DateofExp + "'}";
                      $(".loading-overlay").show();
                      $.ajax({
                          type: "POST",
                          url: pageUrl + '/Update_PenPensionerFileNoTabDetail',
                          data: E,
                          cache: false,
                          contentType: "application/json; charset=utf-8",
                          success: function (D) {
                              $(".loading-overlay").hide();
                              var da = JSON.parse(D.d); //alert(JSON.stringify(da));
                          }
                      });

                      $("#tabs").tabs({
                          collapsible: true,
                          selected: 1,
                          disabled: [2, 3, 4, 5, 6, 7]
                      });
                      $("#btnPenDetNext").hide();
                      CalculateTotalServicePeriod();
                  }
                  else
                  {
                      var msg = "Sorry ! This Location is not Available.";
                      var E = "{Message: '" + msg + "'}";
                      $.ajax({
                          type: "POST",
                          url: pageUrl + '/MessageBox',
                          data: E,
                          cache: false,
                          contentType: "application/json; charset=utf-8",
                          success: function (D) {
                              $('#showMessage').html(D.d);
                              $("#btnOK").focus();
                              var res = "location";
                              $('#hdnDialogResult').val(res);
                          }
                      });
                  }
              }
          });
          


      }
    });
    $(document).ready(function () {
        $('#btnOK').live('click', function () {
            var result = $('#hdnDialogResult').val(); //alert(result);
            if (result == "empno") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtEmpNo").focus();
            }
            else if (result == "empname") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtEmpName").focus();
            }
            else if (result == "dob") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtDOB").focus();
            }
            else if (result == "doj") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtDOJ").focus();
            }
            else if (result == "doe") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 0
                });
                $("#txtExpiryDate").focus();
            }
            else if (result == "nominee") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 1
                });
            }
            else if (result == "50Perct") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#txt50Perct").focus();
            }
            else if (result == "effeUpto") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#txtEffectiveUpto").focus();
            }
            else if (result == "30Perct") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 2
                });
                $("#txt30Perct").focus();
            }
            else if (result == "bank") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3
                });
                $("#ddlBank").focus();
            }
            else if (result == "branch") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3
                });
                $("#ddlBranch").focus();
            }
            else if (result == "accountno") {
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3
                });
                $("#txtBankCode").focus();
            }
            
            else if (result == "roparelief")
                $("#txtRopaRelief").focus();
            else if (result == "effectFrom")
                $("#txtEfffectFrom").focus();
            else if (result == "Commutation")
                $("#ddlCommutation").focus();
            else if (result == "nomineeType")
                $("#ddlNomineeType").focus();
            else if (result == "nomineename")
                $("#txtNomineeName").focus();
            else if (result == "nomineerelation")
                $("#ddlNomineeRelation").focus();
            else if (result == "nomineerDOB")
                $("#txtNomineeDOB").focus();
            else if (result == "nomineerTypeRepeat")
                $("#ddlNomineeType").focus();
            else if (result == "nomineeridmark")
                $("#txtNomineeIDMark").focus();
            else if (result == "familyPension")
                $("#ddlFamilyPension").focus();
            else if (result == "gratuitypercentage")
                $("#txtGratuityPercentage").focus();
            else if (result == "nonomineedetails")
                $("#ddlNomineeType").focus();
            else if (result == "commutamt")
                $("#txtCommutationPerct").focus();
            else if (result == "CommutationPerct")
                $("#txtCommutationPerct").focus();
            else if (result == "nomineeservice")
                $("#ddlInService").focus();
            else if (result == "occupyquarter")
                $("#ddlOccupieQuarter").focus();
            else if (result == "dateofexpiry")
                $("#txtExpiryDate").focus();

        });
    }); 
    $("#ddlIsAlive").change(function () {
        var isalive = $("#ddlIsAlive").val();
        if (isalive == 0) {
            $("#txtExpiryDate").prop("disabled", true);
            $("#txtExpiryDate").val('');
        }
        else {
            $("#txtExpiryDate").prop("disabled", false);
        }
    }); 
    
                                    //This for Final Save
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                    // End This for Final Save

    $(document).ready(function () {
        $("#btnFinalSave").click(function (event) {
           
            var EmpNo = $("#txtEmpNo").val();
            var EmpName = $("#txtEmpName").val(); 
            var DOB = $("#txtDOB").val();
            var DOJ = $("#txtDOJ").val();
            var IsAlive = $("#ddlIsAlive").val();
            
            var NomineeTpeID = $("#hdnNomType").val();

            var FiftyPerct = $("#txt50Perct").val();
            var ThirtyPerct = $("#txt30Perct").val();
            

            if (EmpNo == "") {
                var msg = "Please Enter Employee No.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox", //pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "empno";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (EmpName == "") {
                var msg = "Please Enter Employee Name.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "empname";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (DOB == "") {
                var msg = "Please Enter Date of Birth (DOB).";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "dob";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (DOJ == "") {
                var msg = "Please Enter Date of Joining (DOJ).";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "doj";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (IsAlive == "1" && $('#txtExpiryDate').val()=="") {
                var msg = "Please Enter Date of Expiry (DOE).";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox", //pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                       $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "doe";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            //else if (NomineeTpeID == "") {
            //    var msg = "Please Add at least One Nominee Details.";
            //    var E = "{Message: '" + msg + "'}";
            //    $.ajax({
            //        type: "POST",
            //        url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
            //        data: E,
            //        cache: false,
            //        contentType: "application/json; charset=utf-8",
            //        success: function (D) {
            //            $('#showMessage').html(D.d);
            //            $("#btnOK").focus();
            //            var res = "nominee";
            //            $('#hdnDialogResult').val(res);
            //        }
            //    });
            //}
            //else if (FiftyPerct == "") {
            //    var msg = "Please Enter 50 % of Pension Amount.";
            //    var E = "{Message: '" + msg + "'}";
            //    $.ajax({
            //        type: "POST",
            //        url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
            //        data: E,
            //        cache: false,
            //        contentType: "application/json; charset=utf-8",
            //        success: function (D) {
            //            $('#showMessage').html(D.d);
            //            $("#btnOK").focus();
            //            var res = "50Perct";
            //            $('#hdnDialogResult').val(res);
            //        }
            //    });
            //}
            //else if ($("#txtEffectiveUpto").val() == "") {
            //    var msg = "Please Enter Effective Upto Date.";
            //    var E = "{Message: '" + msg + "'}";
            //    $.ajax({
            //        type: "POST",
            //        url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
            //        data: E,
            //        cache: false,
            //        contentType: "application/json; charset=utf-8",
            //        success: function (D) {
            //            $('#showMessage').html(D.d);
            //            $("#btnOK").focus();
            //            var res = "effeUpto";
            //            $('#hdnDialogResult').val(res);
            //        }
            //    });
            //}
            else if (ThirtyPerct == "") {
                var msg = "Please Enter 30 % of Pension Amount.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "30Perct";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if ($('#ddlBank').val() == "") {
                var msg = "Please Select Bank.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "bank";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if ($('#ddlBank').val() != "" && $('#ddlBranch').val()=="") {
                var msg = "Please Select Branch.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "branch";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if ($('#txtBankCode').val() == "" ) {
                var msg = "Please Enter Bank Account No.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "accountno";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else {

                $.ajax({
                    type: "POST",
                    url: 'UpdateOldDeathPensionerDetail.aspx/CountAdd_Detail',
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = JSON.parse(D.d);
                        if (t == "NoAvailable") {
                            alert("Please Enter Nominee Details.");
                            $("#tabs").tabs({
                                collapsible: true,
                                selected: 1
                            });
                            return false;
                        }
                        else {
                            var K = "{EmpNo: '" + EmpNo + "', EmpName: '" + EmpName + "', FileNo: '" + $('#txtFileNo').val() + "', DOB: '" + DOB + "', DOJ: '" + DOJ + "', DOR: '" + $('#txtDOR').val() + "',"
                                        + " IsAlive: '" + $('#ddlIsAlive').val() + "', DOE: '" + $('#txtExpiryDate').val() + "', FiftyPerctPen: '" + $('#txt50Perct').val() + "',"
                                        + " FiftyPerctValue: '" + $('#lbl50Perct').text() + "', EffecUpto: '" + $('#txtEffectiveUpto').val() + "', ThirtyPerctPen: '" + $('#txt30Perct').val() + "',"
                                        + " ThirtyPerctValue: '" + $('#lbl30Perct').text() + "',  BranchID: '" + $('#ddlBranch').val() + "',  BankAccCode: '" + $('#txtBankCode').val() + "', "
                                        + " RopaID: '" + $('#ddlRopa').val() + "', txtThirtyPerctEffective: '" + $('#txtEffective').val() + "' }";   //txtEffective
                            //alert(K);
                            $(".loading-overlay").show();
                            $.ajax({
                                type: "POST",
                                url: "UpdateOldDeathPensionerDetail.aspx/Update_PenPensioner",
                                data: K,
                                cache: false,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    $(".loading-overlay").hide();
                                    var da = JSON.parse(D.d);
                                    //alert(JSON.stringify(da));
                                    if (da == "Updated") {
                                        alert("Pensioner Details are Updated Successfully.");
                                        window.location.href = "UpdateOldDeathPensionerDetail.aspx";
                                    }
                                }
                            });
                        }
                    }
                });
            }

        });
    });

                                    //End of Pensioner Amount Tab Details 
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                        //Quali Tab Details

    $("#btnPenQualiNext").click(function (event) {

        var ropaID = $("#ddlRopa").val();
        var ropaRelief = $("#txtRopaRelief").val();

        var commtVal = $("#ddlCommutation").val();
        var commtPerct = $("#txtCommutationPerct").val();

        if (ropaID == "") {
            var msg = "Please Select Ropa.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "ropa";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if (ropaRelief == "" && ropaID!=4)
        {
            var msg = "Please Enter Ropa Relief.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "roparelief";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if ($('#txtEfffectFrom').val()=="") {
            var msg = "Please Enter Effect From Date.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "effectFrom";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if ($('#ddlCommutation').val() == "") {
            var msg = "Please Select Pension Commutation.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "Commutation";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if (commtVal == "Y" && commtPerct=="") {
            var msg = "Please Enter Commutation Percentage.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "CommutationPerct";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if (commtVal == "Y" && commtPerct != "") {
            var lblCommutPerct = $('#lblCommutingPercentage').text(); //alert(lblCommutPerct);
            if (lblCommutPerct == "") {
                var msg = "Please Calculate Pension Commutation First.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "CommutationPerct";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else
            {
                var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
                var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
                var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
                var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

                var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
                $(".loading-overlay").show();
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/GetRetiringGratuity',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        var da = JSON.parse(D.d);
                        //alert(JSON.stringify(da));

                        var agAmount = da[0].AG;
                        var gaAmount = da[0].GA;

                        $("#lblActualGratuity").text(agAmount);
                        $("#lblGratuityAmount").text(gaAmount);

                        $("#hdnCalculatedGratuity").val(agAmount);
                    }
                });
                //*************************************************************************************************************************************
                var ipfPageNo = $("#txtIPFPageNo").val();
                var ipfVolNo = $("#IPFVolNo").val();

                var totalServYear = $("#txtTotalServiceYear").val(); var totalServMonth = $("#txtTotalServiceMonth").val(); var totalServDay = $("#txtTotalServiceDay").val();
                var totalNonQualiServYear = $("#txtTotalNonQualiServiceYear").val(); var totalNonQualiServMonth = $("#txtTotalNonQualiServiceMonth").val(); var totalNonQualiServDay = $("#txtTotalNonQualiServiceDay").val();
                var totalQualiServYear = $("#txtTotalQualiServiceYear").val(); var totalQualiServMonth = $("#txtTotalQualiServiceMonth").val(); var totalQualiServDay = $("#txtTotalQualiServiceDay").val();
                var sixMonthPeriod = $("#lblMonthlyPeriod").text();
                var pensionPerMonth = $("#lblPensionPerMonth").text();
                var pensionEffecDate = $("#txtEfffectFrom").val();
                var commutation = $("#ddlCommutation").val();
                var commutationPerct = $("#txtCommutationPerct").val();
                var ageonnextBirthday = $("#hdnAgeOnNextBirthday").val();
                var commutationValue = $("#lblCommutedPensionAmount").text();
                var valueofCommutedPension = $("#lblCommutedFinalAmount").text();
                var netPension = $("#lblReducedCommutationAmount").text();
                var TotalLeapYear = $("#txtTotalLeapYear").val();

                var DOB = $("#txtDOB").val();
                var DOJ = $("#txtDOJ").val();
                var DOR = $("#txtDOR").val();

                var K = "{ropaID: '" + ropaID + "', ropaRelief: '" + ropaRelief + "', ipfPageNo: '" + ipfPageNo + "', ipfVolNo: '" + ipfVolNo + "',"
                          + " totalServYear: '" + totalServYear + "', totalServMonth: '" + totalServMonth + "', totalServDay: '" + totalServDay + "',"
                          + " totalNonQualiServYear: '" + totalNonQualiServYear + "', totalNonQualiServMonth: '" + totalNonQualiServMonth + "', totalNonQualiServDay: '" + totalNonQualiServDay + "',"
                          + " totalQualiServYear: '" + totalQualiServYear + "', totalQualiServMonth: '" + totalQualiServMonth + "', totalQualiServDay: '" + totalQualiServDay + "',"
                          + " sixMonthPeriod: '" + sixMonthPeriod + "',  pensionPerMonth: '" + pensionPerMonth + "', pensionEffecDate: '" + pensionEffecDate + "',"
                          + " commutation: '" + commutation + "', ageonnextBirthday: '" + ageonnextBirthday + "', commutationValue: '" + commutationValue + "',"
                          + " valueofCommutedPension: '" + valueofCommutedPension + "', netPension: '" + netPension + "', commutationPerct: '" + commutationPerct + "',"
                          + " TotalLeapYear: '" + TotalLeapYear + "', DOB: '" + DOB + "', DOJ: '" + DOJ + "', DOR: '" + DOR + "' }";
                //alert(E);
                $(".loading-overlay").show();
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/Update_NonQualiTabDetails',
                    data: K,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        var da = JSON.parse(D.d);
                        //alert(JSON.stringify(da));

                    }
                });
                //$("#btnPenQualiNext").prop("disabled", true);
                $("#btnPenQualiNext").hide();
                $("#tabs").tabs({
                    collapsible: true,
                    selected: 3,
                    disabled: [4, 5, 6,7]
                });
            }
        }
        else {
            var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
            var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
            var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
            var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

            var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetRetiringGratuity',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var da = JSON.parse(D.d);
                    //alert(JSON.stringify(da));

                    var agAmount = da[0].AG;
                    var gaAmount = da[0].GA;

                    $("#lblActualGratuity").text(agAmount);
                    $("#lblGratuityAmount").text(gaAmount);

                    $("#hdnCalculatedGratuity").val(agAmount);
                }
            });
            //*************************************************************************************************************************************
            var ipfPageNo = $("#txtIPFPageNo").val();
            var ipfVolNo = $("#IPFVolNo").val();

            var totalServYear = $("#txtTotalServiceYear").val(); var totalServMonth = $("#txtTotalServiceMonth").val(); var totalServDay = $("#txtTotalServiceDay").val();
            var totalNonQualiServYear = $("#txtTotalNonQualiServiceYear").val(); var totalNonQualiServMonth = $("#txtTotalNonQualiServiceMonth").val(); var totalNonQualiServDay = $("#txtTotalNonQualiServiceDay").val();
            var totalQualiServYear = $("#txtTotalQualiServiceYear").val(); var totalQualiServMonth = $("#txtTotalQualiServiceMonth").val(); var totalQualiServDay = $("#txtTotalQualiServiceDay").val();
            var sixMonthPeriod = $("#lblMonthlyPeriod").text();
            var pensionPerMonth = $("#lblPensionPerMonth").text();
            var pensionEffecDate = $("#txtEfffectFrom").val();
            var commutation = $("#ddlCommutation").val();
            var commutationPerct = $("#txtCommutationPerct").val();
            var ageonnextBirthday = $("#hdnAgeOnNextBirthday").val();
            var commutationValue = $("#lblCommutedPensionAmount").text();
            var valueofCommutedPension = $("#lblCommutedFinalAmount").text();
            var netPension = $("#lblReducedCommutationAmount").text();
            var TotalLeapYear = $("#txtTotalLeapYear").val();

            var DOB = $("#txtDOB").val();
            var DOJ = $("#txtDOJ").val();
            var DOR = $("#txtDOR").val();


            var K = "{ropaID: '" + ropaID + "', ropaRelief: '" + ropaRelief + "', ipfPageNo: '" + ipfPageNo + "', ipfVolNo: '" + ipfVolNo + "',"
                      + " totalServYear: '" + totalServYear + "', totalServMonth: '" + totalServMonth + "', totalServDay: '" + totalServDay + "',"
                      + " totalNonQualiServYear: '" + totalNonQualiServYear + "', totalNonQualiServMonth: '" + totalNonQualiServMonth + "', totalNonQualiServDay: '" + totalNonQualiServDay + "',"
                      + " totalQualiServYear: '" + totalQualiServYear + "', totalQualiServMonth: '" + totalQualiServMonth + "', totalQualiServDay: '" + totalQualiServDay + "',"
                      + " sixMonthPeriod: '" + sixMonthPeriod + "',  pensionPerMonth: '" + pensionPerMonth + "', pensionEffecDate: '" + pensionEffecDate + "',"
                      + " commutation: '" + commutation + "', ageonnextBirthday: '" + ageonnextBirthday + "', commutationValue: '" + commutationValue + "',"
                      + " valueofCommutedPension: '" + valueofCommutedPension + "', netPension: '" + netPension + "', commutationPerct: '" + commutationPerct + "',"
                       + " TotalLeapYear: '" + TotalLeapYear + "', DOB: '" + DOB + "', DOJ: '" + DOJ + "', DOR: '" + DOR + "' }";
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: pageUrl + '/Update_NonQualiTabDetails',
                data: K,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var da = JSON.parse(D.d);
                    //alert(JSON.stringify(da));

                }
            });
            //**************************************************************************************************************************************
            //$("#btnPenQualiNext").prop("disabled", true);
            $("#btnPenQualiNext").hide();
            $("#tabs").tabs({
                collapsible: true,
                selected: 3,
                disabled: [4,5,6,7]
            });
        }
    });
    $("#ddlCommutation").change(function () {
        var commut = $("#ddlCommutation").val();
        if (commut == "Y") {

            $("#txtCommutationPerct").show();
            $("#spnCommutationPerText").show();
            $("#btnPenCommuOK").show();
            $("#txtCommutationPerct").focus();
        }
        else {
            CalaculateCommutative(0);
            $("#txtCommutationPerct").val('');
            $("#txtCommutationPerct").hide();
            $("#spnCommutationPerText").hide();
            $("#btnPenCommuOK").hide();
            //$("#total").hide();
        }
    });
    $("#btnPenCommuOK").click(function (event) {
        var commuPerct = "";
        var commPerct = $("#txtCommutationPerct").val();
        if (commPerct != "") {
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: pageUrl + '/Load_MaxCommutativeAmount',
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                    commuPerct = t;
                    if (commPerct > commuPerct) {
                        //commPerct = 0;
                        //CalaculateCommutative(commPerct);
                        var msg = "Sorry ! Commutative Amount should not be more than " + commuPerct;
                        var E = "{Message: '" + msg + "'}";
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/MessageBox',
                            data: E,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#showMessage').html(D.d);
                                $("#btnOK").focus();
                                var res = "commutamt";
                                $('#hdnDialogResult').val(res);
                            }
                        });
                    }
                    else
                    {
                        CalaculateCommutative(commPerct);
                    }
                }
            });
        }
        else
        {
            $("#txtCommutationPerct").focus();
        }
    });
    function CalaculateCommutative(CommutatiePerct)
    {
        $("#total").show();
        var dob = new Date($("#txtDOB").datepicker('getDate')); //alert(dob);
        var today = new Date(); //alert(today);

        var diff_date = today - dob; //alert(diff_date);
        var years = Math.floor(diff_date / 31536000000); //alert(years);

        var AgeonNextYear = parseInt(years) + 1;
        var PensionPerMonth = $("#lblPensionPerMonth").text();
        $("#hdnAgeOnNextBirthday").val(AgeonNextYear);

        var C = "{PensionPerMonth: '" + PensionPerMonth + "', AgeonNextYear: '" + AgeonNextYear + "' , CommutativePerct: '" + CommutatiePerct + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/Get_Commutation',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var t = jQuery.parseJSON(D.d);
                //alert(JSON.stringify(t));
                var CommuPerctg = t[0].CommutingPercentage;// alert(CommuPerctg);
                var GPension = t[0].GrossPension;
                var CommutdPenAmount = t[0].CommutedPensionAmount;
                var CommutdRate = t[0].CommutedRate;
                var CommudFAmount = t[0].CommutedFinalAmount;
                var RedudCommutatAmount = t[0].ReducedCommutationAmount;

                $("#lblCommutingPercentage").text(CommuPerctg);
                $("#lblGrossPension").text(GPension);
                $("#lblCommutedPensionAmount").text(CommutdPenAmount);
                $("#lblCommutedPensionAmt").text(CommutdPenAmount);
                $("#lblComutedRate").text(CommutdRate);
                $("#lblCommutedFinalAmount").text(CommudFAmount);
                $("#lblCommutedPenAmt").text(CommutdPenAmount);
                $("#lblGrPension").text(GPension);
                $("#lblCommutdPenAmt").text(CommutdPenAmount);
                $("#lblReducedCommutationAmount").text(RedudCommutatAmount);
            }
        });
    }


                                     //End of Quali Tab Details
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                            //Nominee Details Tab Data Entry  

    $(document).ready(function () {
        $("#btnNomineeNext").click(function (event) {

           // var totalGrPct = $("#hdnTotalGratuityPerct").val(); //alert(totalGrPct);
            //if (totalGrPct != "") {
                //if (totalGrPct < 100) {
                //    var msg = "Your Total Gratuity Percentage is  " + totalGrPct + "%" + "  which is less than 100%.\nIt should be equal to 100%. ";
                //    var E = "{Message: '" + msg + "'}";
                //    $.ajax({
                //        type: "POST",
                //        url: pageUrl + '/MessageBox',
                //        data: E,
                //        cache: false,
                //        contentType: "application/json; charset=utf-8",
                //        success: function (D) {
                //            $('#showMessage').html(D.d);
                //            $("#btnOK").focus();
                //            //var res = "nomineename";
                //            //$('#hdnDialogResult').val(res);
                //        }
                //    });
                //}
                //else if (totalGrPct > 100) {
                //    var msg = "Your Total Gratuity Percentage is  " + totalGrPct + "%" + "more than 100%.\nIt should be equal to 100%.";
                //    var E = "{Message: '" + msg + "'}";
                //    $.ajax({
                //        type: "POST",
                //        url: pageUrl + '/MessageBox',
                //        data: E,
                //        cache: false,
                //        contentType: "application/json; charset=utf-8",
                //        success: function (D) {
                //            $('#showMessage').html(D.d);
                //            $("#btnOK").focus();
                //            //var res = "nomineename";
                //            //$('#hdnDialogResult').val(res);
                //        }
                //    });
                //}
                //else {
                    //alert("heelllo");
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Update_NomineeTabDetails',
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var jsdata = JSON.parse(D.d);// alert(JSON.stringify(jsdata));
                            gratuity();
                            if (jsdata == "Nodata") {
                                var msg = "Please Add at least one Nominee Details First.";
                                var E = "{Message: '" + msg + "'}";
                                $(".loading-overlay").show();
                                $.ajax({
                                    type: "POST",
                                    url: pageUrl + '/MessageBox',
                                    data: E,
                                    cache: false,
                                    contentType: "application/json; charset=utf-8",
                                    success: function (D) {
                                        $(".loading-overlay").hide();
                                        $('#showMessage').html(D.d);
                                        $("#btnOK").focus();
                                        var res = "nonomineedetails";
                                        $('#hdnDialogResult').val(res);
                                    }
                                });
                            }
                            else {
                                CalculateFamilyPensionCalculations();
                                gratuity();
                                //******************************************************************************************************
                                $(".loading-overlay").show();
                                $.ajax({
                                    type: "POST",
                                    url: pageUrl + '/Get_FamilyPensionYesorNot',
                                    data: E,
                                    cache: false,
                                    contentType: "application/json; charset=utf-8",
                                    success: function (D) {
                                        $(".loading-overlay").hide();
                                        var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));
                                        if (jsdata == 1) {
                                            //CalculateFamilyPensionCalculations();
                                            $("#btnNomineeNext").hide();
                                            $("#tabs").tabs({
                                                collapsible: true,
                                                selected: 4,
                                                disabled: [ 5, 6,7]
                                            });
                                        }
                                        else {
                                            $("#btnNomineeNext").hide();
                                            $("#tabs").tabs({
                                                collapsible: true,
                                                selected: 5,
                                                disabled: [4, 6,7]
                                            });
                                        }
                                    }
                                });

                                //******************************************************************************************************
                            }
                        }
                    });
                //}
           // }
            //else {
            //    var msg = "Please add Nominee Details First.";
            //    var E = "{Message: '" + msg + "'}";
            //    $.ajax({
            //        type: "POST",
            //        url: pageUrl + '/MessageBox',
            //        data: E,
            //        cache: false,
            //        contentType: "application/json; charset=utf-8",
            //        success: function (D) {
            //            $('#showMessage').html(D.d);
            //            $("#btnOK").focus();
            //            //var res = "nomineename";
            //            //$('#hdnDialogResult').val(res);
            //        }
            //    });
            //}
        });
    });
    $(document).ready(function () {
        $("#btnAddNominee").click(function (e) {
            var FamilyPension = "";
            var nomineeName = $("#txtNomineeName").val();
            var nomineeRelation = $("#ddlNomineeRelation").val();
            var nomineeDOB = $("#txtNomineeDOB").val();
            var NomiTypeID = $("#ddlNomineeType").val();
            var NomiType = $('#ddlNomineeType').find('option:selected').text();
            var NomiRelation = $('#ddlNomineeRelation').find('option:selected').text();
            var nomineeIDMark = $("#txtNomineeIDMark").val();
            var familyPension = $("#ddlFamilyPension").val();
            //var gratuityPerct = $("#txtGratuityPercentage").val();

            var fPension = $("#ddlFamilyPension").val();
            if (fPension == "1")
                FamilyPension = "Y";
            else
                FamilyPension = "N";


            var isGovtServ = $("#ddlInService").val();
            var OccupyQuart = $("#ddlOccupieQuarter").val();

            var familyPenRank = $("#txtFamilyPensionRank").val();
            var FPEffecFrom = $("#txtFamilyPenEffectFrom").val();
            var FPEffecTo = $("#txtFamilyPenEffectTo").val();
            //var gratuityRank = $("#txtGratuityRank").val();

            if (nomineeRelation == "") {
                var msg = "Please Select Nominee Relation.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineerelation";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (nomineeName == "") {
                var msg = "Please Enter Nominee Name.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineename";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
             
            else if (NomiTypeID == "") {
                    var msg = "Please Select Nominee Type.";
                    var E = "{Message: '" + msg + "'}";
                    $.ajax({
                        type: "POST",
                        url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $('#showMessage').html(D.d);
                            $("#btnOK").focus();
                            var res = "nomineeType";
                            $('#hdnDialogResult').val(res);
                        }
                    });
                }
            else if (nomineeDOB == "") {
                var msg = "Please Enter Nominee DOB.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineerDOB";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            //else if (nomineeIDMark == "") {
            //    var msg = "Please Enter Nominee Identification Mark.";
            //    var E = "{Message: '" + msg + "'}";
            //    $.ajax({
            //        type: "POST",
            //        url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
            //        data: E,
            //        cache: false,
            //        contentType: "application/json; charset=utf-8",
            //        success: function (D) {
            //            $('#showMessage').html(D.d);
            //            $("#btnOK").focus();
            //            var res = "nomineeridmark";
            //            $('#hdnDialogResult').val(res);
            //        }
            //    });
            //}
            else if (familyPension == "") {
                var msg = "Please Select Family Pension.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "familyPension";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (NomiTypeID == 1 && isGovtServ=="") {
                var msg = "Please Select, Is Nominee Service is in Govt/WBFC ?";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineeservice";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (NomiTypeID == 1 && OccupyQuart == "") {
                var msg = "Please Select, Is Nominee Occupy Govt Quarter ?";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "occupyquarter";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (familyPenRank == "") {
                var msg = "Please Enter Family Pension Rank ?";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "familyPensionRank";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else {
                
                //NomiTypeID
                if (NomiTypeID == 1) {
                    var hiddenNomineeType = $("#hdnNomType").val();
                    if (hiddenNomineeType == "") {
                        $("#hdnNomType").val(NomiTypeID);

                        //var gtotal = $("#hdnTotalGratuityPerct").val();
                        //gtotal = parseInt(gtotal) + parseInt(gratuityPerct);
                        //$("#hdnTotalGratuityPerct").val(gtotal);

                        var C = "{ NomineeID: '" + $("#ddlNomineeType").val() + "', NomineeType: '" + NomiType + "' ,  NomineeName: '" + $("#txtNomineeName").val() + "', NomineeRelationID: '" + $("#ddlNomineeRelation").val() + "', NomineeRelation: '" + NomiRelation + "' ," 
                         + "NomineeDOB: '" + $("#txtNomineeDOB").val() + "', NomineeAddress: '" + $("#txtNomineeAddress").val() + "', NomineeState: '" + $("#ddlNomineeState").val() + "', NomineeDistrict: '" + $("#ddlNomineeDistrict").val() + "',"
                         + "NomineeCity: '" + $("#txtNomineeCity").val() + "', NomineePin: '" + $("#txtNomineePin").val() + "', NomineePhone: '" + $("#txtNomineePhone").val() + "'," 
                         + "NomineeIDMark: '" + $("#txtNomineeIDMark").val() + "', FamilyPension: '" + FamilyPension + "' ,"
                         + "isGovtServ: '" + isGovtServ + "', OccupyQuart: '" + OccupyQuart + "', familyPenRank: '" + familyPenRank + "', familyPenEffeFrom: '" + FPEffecFrom + "', familyPenEffeTo: '" + FPEffecTo + "'}";

                        if ($("#ddlNomineeType").val() == 1 && $("#ddlFamilyPension").val() == 1) {
                            $("#hdnNomineeType").val(1);
                            $("#hdnisFamilyPension").val(1);
                        }
                        $(".loading-overlay").show();
                        $.ajax({
                            type: "POST",
                            url: "UpdateOldDeathPensionerDetail.aspx/AddNomineeDetails",
                            data: C,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                $(".loading-overlay").hide();
                                $('#NomineeDetails').html(data.d);
                                BlankNomineeFields();

                            }
                        });
                    }
                    else {
                        var msg = "You can not Select this more than one time.";
                        var E = "{Message: '" + msg + "'}";
                        $.ajax({
                            type: "POST",
                            url: "UpdateOldDeathPensionerDetail.aspx/MessageBox",
                            data: E,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#showMessage').html(D.d);
                                $("#btnOK").focus();
                                var res = "nomineerTypeRepeat";
                                $('#hdnDialogResult').val(res);
                            }
                        });
                    }
                }
                else {

                    $("#hdnNomType").val(NomiTypeID);
                    //var gtotal = $("#hdnTotalGratuityPerct").val(); //alert(gtotal);
                    //gtotal = parseInt(gtotal) + parseInt(gratuityPerct); //alert(gtotal);
                    //$("#hdnTotalGratuityPerct").val(gtotal);

                    var C = "{ NomineeID: '" + $("#ddlNomineeType").val() + "', NomineeType: '" + NomiType + "' ,  NomineeName: '" + $("#txtNomineeName").val() + "', NomineeRelationID: '" + $("#ddlNomineeRelation").val() + "',"
                        + "NomineeRelation: '" + NomiRelation + "' , NomineeDOB: '" + $("#txtNomineeDOB").val() + "', NomineeAddress: '" + $("#txtNomineeAddress").val() + "', NomineeState: '" + $("#ddlNomineeState").val() + "'," 
                        + "NomineeDistrict: '" + $("#ddlNomineeDistrict").val() + "', NomineeCity: '" + $("#txtNomineeCity").val() + "', NomineePin: '" + $("#txtNomineePin").val() + "', NomineePhone: '" + $("#txtNomineePhone").val() + "',"
                        + "NomineeIDMark: '" + $("#txtNomineeIDMark").val() + "', FamilyPension: '" + FamilyPension + "',  isGovtServ: '" + isGovtServ + "',"
                        + "OccupyQuart: '" + OccupyQuart + "', familyPenRank: '" + familyPenRank + "', familyPenEffeFrom: '" + FPEffecFrom + "', familyPenEffeTo: '" + FPEffecTo + "'}";

                    if ($("#ddlNomineeType").val() == 1 && $("#ddlFamilyPension").val() == 1) {
                        $("#hdnNomineeType").val(1);
                        $("#hdnisFamilyPension").val(1);
                    }
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: "UpdateOldDeathPensionerDetail.aspx/AddNomineeDetails",
                        data: C,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            $(".loading-overlay").hide();
                            $('#NomineeDetails').html(data.d);
                            BlankNomineeFields();

                        }
                    });
                }

            }
        });
    });
    $("#ddlNomineeType").change(function () {
        var nomineeType = $("#ddlNomineeType").val();
        var familypension = $("#ddlFamilyPension").val();

        if (nomineeType == 1 || nomineeType == 2 || nomineeType == 3 || nomineeType == 8) {
            $("#ddlFamilyPension").val('');
            document.getElementById("ddlFamilyPension").disabled = false;
            $("#ddlInService").prop("disabled", false);
            $("#ddlOccupieQuarter").prop("disabled", false);
        }
        else {
            $("#ddlFamilyPension").val('0');
            document.getElementById("ddlFamilyPension").disabled = true;
            $("#ddlInService").prop("disabled", true);
            $("#ddlInService").val('');
            $("#ddlOccupieQuarter").prop("disabled", true);
            $("#ddlOccupieQuarter").val('');
        }




        //var nomineeType = $("#ddlNomineeType").val();
        //var familypension = $("#ddlFamilyPension").val();

        //if (nomineeType != 1) {
        //    $("#ddlFamilyPension").val('0');
        //    document.getElementById("ddlFamilyPension").disabled = true;
        //    $("#ddlInService").prop("disabled", true);
        //    $("#ddlInService").val('');
        //    $("#ddlOccupieQuarter").prop("disabled", true);
        //    $("#ddlOccupieQuarter").val('');
        //}
        //else {
        //    $("#ddlFamilyPension").val('');
        //    document.getElementById("ddlFamilyPension").disabled = false;
        //    $("#ddlInService").prop("disabled", false);
        //    $("#ddlOccupieQuarter").prop("disabled", false);
        //}
    });
    function BlankNomineeFields()
    {
        $("#ddlNomineeType").val('');
        $("#txtNomineeName").val('');
        $("#ddlNomineeRelation").val('');
        $("#txtNomineeDOB").val('');

        //$("#txtNomineeAddress").val('');
        //$("#ddlNomineeState").val('');
        //$("#ddlNomineeDistrict").val('');
        //$("#txtNomineeCity").val('');
        //$("#txtNomineePin").val('');
        //$("#txtNomineePhone").val('');
        $("#ddlInService").val('');
        $("#ddlOccupieQuarter").val('');

        $("#txtNomineeIDMark").val('');
        $("#ddlFamilyPension").val('');

        $("#txtFamilyPensionRank").val('');
        $("#txtFamilyPenEffectFrom").val('');
        $("#txtFamilyPenEffectTo").val('');

                document.getElementById("ddlFamilyPension").disabled = false;
        document.getElementById("ddlInService").disabled = true;
        document.getElementById("ddlOccupieQuarter").disabled = true;
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $('#Nomineeworktype a').live('click', function () {
        var TotalGratuityAmt=0;

        flag = $(this).attr('id'); //alert(flag);

        var NomineeTypeID = $(this).attr('itemid'); //alert(NomineeTypeID);
        var GratuityPerct = $(this).closest('tr').find('td:eq(5)').text(); //alert(GratuityPerct);

        var lre = /^\s*/;
        var rre = /\s*$/;
        GratuityPerct = GratuityPerct.replace(lre, "");
        GratuityPerct = GratuityPerct.replace(rre, "");

        var totalGrtamt = $("#hdnTotalGratuityPerct").val();
        TotalGratuityAmt = parseInt(totalGrtamt) - parseInt(GratuityPerct); //alert(TotalGratuityAmt);
        $("#hdnTotalGratuityPerct").val(parseInt(TotalGratuityAmt));

        var E = "{NomineeTypeID: '" + NomineeTypeID + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/Delete_NomineeDetails',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#NomineeDetails').html(D.d);
                BlankNomineeFields();
            }
        });
    });



                                        //End of Nominee Details Tab Data Entry   
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    $("#cmdSearchCancel").click(function (event) {
        alert("cancel");
    });

    $('#txtDOB').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
        
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOJ').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOR').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtEfffectFrom').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    }); 
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtNomineeDOB').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtExpiryDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtFamilyPenEffectFrom').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtFamilyPenEffectTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtEffectiveUpto').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtEffective').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOBS').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOJS').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDORS').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    


});

function CalculateTotalServicePeriod() {

    if ($("#txtDOJ").val() != "" && $("#txtDOR").val() != "") {

        //var From_date = new Date($("#txtDOJ").datepicker('getDate')); //alert(From_date);
        //var To_date = new Date($("#txtDOR").datepicker('getDate')); //alert(To_date);
        //var diff_date = To_date - From_date; //alert(diff_date);

        //$("#hdnTotalServicePeriod").val(diff_date);
        //var years = Math.floor(diff_date / 31536000000);
        //var months = Math.floor((diff_date % 31536000000) / 2628000000);
        //var days = Math.floor(((diff_date % 31536000000) % 2628000000) / 86400000);
        
        ////diffc = To_date.getTime() - From_date.getTime();
        //////getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

        ////diffDays = Math.round(Math.abs(diffc / (1000 * 60 * 60 * 24)));
        //////this is the actual equation that calculates the number of days.

        ////alert(years + " year(s) " + months + " month(s) " + diffDays + " and day(s)");

        //$("#txtTotalServiceYear").val(years);
        //$("#txtTotalServiceMonth").val(months);
        //$("#txtTotalServiceDay").val(days);

        //$("#txtTotalNonQualiServiceYear").val(0);
        //$("#txtTotalNonQualiServiceMonth").val(0);
        //$("#txtTotalNonQualiServiceDay").val(0);

        //var lyear = years % 4; //alert(lyear);
        //if (lyear == 0) {
        //    ldays = years / 4;
        //    $("#txtTotalLeapYear").val(ldays);
        //}
        //else {
        //    var str = years / 4; //alert(str);
        //    ldays = Math.floor(str);
        //    $("#txtTotalLeapYear").val(ldays);
        //}

        //$("#txtTotalQualiServiceYear").val(years);
        //$("#txtTotalQualiServiceMonth").val(months);
        //$("#txtTotalQualiServiceDay").val(days);

        //CalculateSixMonthlyPeriod();
        var fromdate = $("#txtDOJ").val(); //alert(fromdate);
        var todate = $("#txtDOR").val(); //alert(todate);

        var E = "{fromdate: '" + fromdate + "', todate: '" + todate + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/Calculate_TotalServiceYear',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(t);
                var arr = t.split(',');
                var years = arr[0]; //alert(years);
                var months = arr[1]; //alert(months);
                var days = arr[2]; //alert(days);

                $("#txtTotalServiceYear").val(years);
                $("#txtTotalServiceMonth").val(months);
                $("#txtTotalServiceDay").val(days);

                $("#txtTotalNonQualiServiceYear").val(0);
                $("#txtTotalNonQualiServiceMonth").val(0);
                $("#txtTotalNonQualiServiceDay").val(0);

                $('#txtTotalQualiServiceYear').val(years);
                $('#txtTotalQualiServiceMonth').val(months);
                $('#txtTotalQualiServiceDay').val(days);

                CalculateTotalNonQualiServicePeriod();
            }
        });

    }
}


var TotalNonYear = 0; var TotalNonMonth = 0; var TotalNonDays = 0;
function CalculateTotalNonQualiServicePeriod() {
    var total = 0; 
    if ($("#txtFromDate").val() != "" && $("#txtToDate").val() != "") {

        var fromdate = $("#txtFromDate").val();
        var todate = $("#txtToDate").val();

        var E = "{fromdate: '" + fromdate + "', todate: '" + todate + "'}"; //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/Calculate_TotalServiceYear',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(t);
                var arr = t.split(',');
                var years = arr[0]; //alert(years);
                var months = arr[1]; //alert(months);
                var days = arr[2]; //alert(days);

                TotalNonYear = parseInt(TotalNonYear) + parseInt(years);
                TotalNonMonth = parseInt(TotalNonMonth) + parseInt(months);
                TotalNonDays = parseInt(TotalNonDays) + parseInt(days);

                $("#lblYears").show();
                $("#lblMonths").show();
                $("#lblDa").show();
                $('#lblYear').text(TotalNonYear);
                $('#lblMonth').text(TotalNonMonth);
                $('#lblDay').text(TotalNonDays);

                $("#txtTotalNonQualiServiceYear").val(TotalNonYear);
                $("#txtTotalNonQualiServiceMonth").val(TotalNonMonth);
                $("#txtTotalNonQualiServiceDay").val(TotalNonDays);

                CalculateTotalQualiServicePeriod();
                //CalculateSixMonthlyPeriod();
            }
        });

        //    var From_date = new Date($("#txtFromDate").datepicker('getDate'));
        //    var To_date = new Date($("#txtToDate").datepicker('getDate'));
        //    var diff_date = To_date - From_date;

        //    TotalDays.push(diff_date); //alert(TotalDays);
        //    for (var j = 0; j < TotalDays.length; j++) {
        //        total = total + TotalDays[j];
        //    }
        //    var t = $("#hdnTotalNonQualiServicePeriodOnSearch").val();
        //    if (t != "")
        //        total = parseInt(t) + parseInt(total);

        //    //tTOTAL = total + TT;
        //    //alert(total)
        //    $("#hdnTotalNonQualiServicePeriod").val(total);
        //    $("#TotalDays").val(total);
        //    var years = Math.floor(total / 31536000000);
        //    var months = Math.floor((total % 31536000000) / 2628000000);
        //    var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);
        //    //alert( years+" year(s) "+months+" month(s) "+days+" and day(s)");
        //    $("#txtTotalNonQualiServiceYear").val(years);
        //    $("#txtTotalNonQualiServiceMonth").val(months);
        //    $("#txtTotalNonQualiServiceDay").val(days);

        //    $("#lblYears").show();
        //    $("#lblMonths").show();
        //    $("#lblDa").show();
        //    $('#lblYear').text(years);
        //    $('#lblMonth').text(months);
        //    $('#lblDay').text(days);
        //}
        //else {
        //    alert("Please select dates");
        //    return false;
        //}
    }
    else {
        //alert("Enter");
        $("#txtTotalNonQualiServiceYear").val(0);
        $("#txtTotalNonQualiServiceMonth").val(0);
        $("#txtTotalNonQualiServiceDay").val(0);

        CalculateTotalQualiServicePeriod();
        //CalculateSixMonthlyPeriod();
    }
}


var totalQualiSerDay = 0; var totalQualiSerMonth = 0; var totalQualiSerYear = 0;
function CalculateTotalQualiServicePeriod() {

    var totalYear = $("#txtTotalServiceYear").val(); //alert(totalYear);
    var totalMonth = $("#txtTotalServiceMonth").val(); //alert(totalMonth);
    var totalDay = $("#txtTotalServiceDay").val(); //alert(totalDay);

    var totalNonYear = $("#txtTotalNonQualiServiceYear").val(); //alert(totalNonYear);
    var totalNonMonth = $("#txtTotalNonQualiServiceMonth").val(); //alert(totalNonMonth);
    var totalNonDay = $("#txtTotalNonQualiServiceDay").val(); //alert(totalNonDay);

    if (parseInt(totalDay) < parseInt(totalNonDay)) {

        totalQualiSerDay = parseInt((parseInt(totalDay) + 30)) - parseInt(totalNonDay);
    }
    else {
        totalQualiSerDay = parseInt(totalDay) - parseInt(totalNonDay); //alert(totalQualiSerDay);
    }


    if (parseInt(totalMonth) < parseInt(totalNonMonth)) {
        totalQualiSerMonth = parseInt(parseInt(totalMonth) + 12) - parseInt(totalNonMonth);
        totalQualiSerYear = parseInt(parseInt(totalYear) - 1) - parseInt(totalNonYear);
    }
    else {
        if (parseInt(totalDay) < parseInt(totalNonDay)) {
            totalQualiSerMonth = parseInt(parseInt(totalMonth) - 1) - parseInt(totalNonMonth);

        }
        else {
            totalQualiSerMonth = parseInt(totalMonth) - parseInt(totalNonMonth);
        }
       
        totalQualiSerYear = parseInt(totalYear) - parseInt(totalNonYear);
    }

    //alert(totalQualiSerYear); alert(totalQualiSerMonth); alert(totalQualiSerDay);

    $('#txtTotalQualiServiceYear').val(totalQualiSerYear);
    $('#txtTotalQualiServiceMonth').val(totalQualiSerMonth);
    $('#txtTotalQualiServiceDay').val(totalQualiSerDay);

    CalculateSixMonthlyPeriod();
    //var tsp = $("#hdnTotalServicePeriod").val(); //alert(tsp);
    //var tnqsp = $("#hdnTotalNonQualiServicePeriod").val(); //alert(tnqsp);

    //var total = tsp - tnqsp; //alert(total);
    //var years = Math.floor(total / 31536000000);
    //var months = Math.floor((total % 31536000000) / 2628000000);
    //var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);


    //var lyear = years % 4; //alert(lyear);
    //if (lyear == 0)
    //{
    //    ldays = years / 4;
    //    $("#txtTotalLeapYear").val(ldays);
    //}
    //else
    //{
    //    var str = years / 4; //alert(str);
    //    ldays = Math.floor(str);
    //    $("#txtTotalLeapYear").val(ldays);
    //}
    //var From_date = new Date($("#txtDOJ").datepicker('getDate'));
    //var To_date = new Date($("#txtDOR").datepicker('getDate')); //alert(To_date);
    //var yer = To_date.getFullYear(); //alert(yer);
    //var mon = To_date.getMonth() +1; //alert(mon);
    //var day = To_date.getDate(); //alert(day);

    //var newdate = new Date(parseInt(yer), parseInt(mon) - 1, day - ldays); //alert(newdate);
    //var diff_date = newdate - From_date; //alert(diff_date);

    //var nonqualiserv = diff_date - tnqsp;

    //var years = Math.floor(nonqualiserv / 31536000000);
    //var months = Math.floor((nonqualiserv % 31536000000) / 2628000000);
    //var days = Math.floor(((nonqualiserv % 31536000000) % 2628000000) / 86400000);
    //alert(years + " year(s) " + months + " month(s) " + days + " and day(s)");

    //$('#txtTotalQualiServiceYear').val(years);
    //$('#txtTotalQualiServiceMonth').val(months);
    //$('#txtTotalQualiServiceDay').val(days);
}

function CalculateSixMonthlyPeriod() {

    var RestMonth = 0; var TotalSixMonthPeriod = 0;
    var t1 = $("#txtTotalQualiServiceYear").val(); //alert(t1);
    var t2 = $("#txtTotalQualiServiceMonth").val(); //alert(t2);
    var t3 = $("#txtTotalQualiServiceDay").val(); //alert(t3);

    if ($("#txtTotalQualiServiceYear").val() != "" && $("#txtTotalQualiServiceMonth").val() != "" && $("#txtTotalQualiServiceDay").val() != "") {
        
        var yearvalue = ($("#txtTotalQualiServiceYear").val()) * 2; //alert(yearvalue);
        var MonthValue = $("#txtTotalQualiServiceMonth").val(); //alert(MonthValue);
        if (MonthValue > 6 || MonthValue == 6) {
            TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod) + parseInt(yearvalue) + 1; //alert(TotalSixMonthPeriod);
            RestMonth = parseInt(MonthValue) - 6; //alert(RestMonth);
            if (parseInt(RestMonth) > 3 || parseInt(RestMonth) == 3) {
                TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod) + 1;
            }
            else
                TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod);
        }
        else
        {
            if (MonthValue < 6)
            {
                RestMonth = parseInt(MonthValue); //alert(RestMonth);
                if (parseInt(RestMonth) > 3 || parseInt(RestMonth) == 3) {
                    TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod) + parseInt(yearvalue) + 1;
                }
                else
                    TotalSixMonthPeriod = parseInt(yearvalue);
            }
        }

        if (MonthValue == 12)
            TotalSixMonthPeriod = TotalSixMonthPeriod + parseInt(yearvalue) + 2;

        //TotalSixMonthPeriod = parseInt(yearvalue) + parseInt(monthvalue); //alert(TotalSixMonthPeriod);
        $("#lblMonthlyPeriod").append(" ");
        $('#lblMonthlyPeriod').text(TotalSixMonthPeriod);

        Calculation();
    }
}

function Calculation() {
    var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
    var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
    var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
    var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

    var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
    $(".loading-overlay").show();
    $.ajax({
        type: "POST",
        url: pageUrl + '/GetRetiringGratuity',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $(".loading-overlay").hide();
            var da = JSON.parse(D.d);
            //alert(JSON.stringify(da));

            var agAmount = da[0].AG;
            var gaAmount = da[0].GA;

            $("#lblActualGratuity").text(agAmount);
            $("#lblGratuityAmount").text(gaAmount);

            $("#hdnCalculatedGratuity").val(agAmount);
        }
    });

    var totalPBPlusGP = parseInt($("#lblGradePay").text()) + parseInt($("#lblPayintheBand").text()); //alert(totalPBPlusGP);
    var E = "{TotalServiceYear: '" + $("#txtTotalQualiServiceYear").val() + "', totalPBPlusGP: '" + totalPBPlusGP + "', SixMonthPeriod: '" + $("#lblMonthlyPeriod").text() + "'}";
    $(".loading-overlay").show();
    $.ajax({
        type: "POST",
        url: pageUrl + '/Calculate_MonthlyPension',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $(".loading-overlay").hide();
            var da = JSON.parse(D.d); //alert(JSON.stringify(da));

            var pernsionPerMonth = da[0].pensionAmount; //alert(pernsionPerMonth);
            $("#lblPensionPerMonth").text(pernsionPerMonth);
        }
    });
}

function CalculateEffectiveFrom()
{
    if ($("#txtDOR").val() != "") {
        var dorDate = ""; var dorMonth = ""; var dorYear = "";

        var RetireDate = new Date($("#txtDOR").datepicker('getDate')); //alert(RetireDate);

        var nextMonth = new Date(RetireDate.getFullYear(), RetireDate.getMonth() + 1, RetireDate.getDate()); //alert(nextMonth);

        var firstDay = new Date(nextMonth.getFullYear(), nextMonth.getMonth(), 1); //alert(firstDay);
        //var lastDay = new Date(nextMonth.getFullYear(), nextMonth.getMonth() + 1, 0); alert(lastDay);

        dorDate = firstDay.getDate() < 10 ? '0' + firstDay.getDate() : firstDay.getDate(); //alert(dorDate);

        if (firstDay.getMonth() == 9)
            dorMonth = parseInt(firstDay.getMonth()) + 1;
        else
            dorMonth = firstDay.getMonth() < 10 ? '0' + (parseInt(firstDay.getMonth()) + 1) : (parseInt(firstDay.getMonth()) + 1); //alert(dorMonth);

            dorYear = firstDay.getFullYear();

            var EffeDate = dorDate + '/' + dorMonth + '/' + dorYear; //alert(EffeDate);

            $("#txtEfffectFrom").val(EffeDate);
    }
}

function gratuity()
{
    var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
    var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
    var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
    var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

    var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/GetRetiringGratuity',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var da = JSON.parse(D.d);
            //alert(JSON.stringify(da));

            var agAmount = da[0].AG;
            var gaAmount = da[0].GA;

            $("#lblActualGratuity").text(agAmount); //alert(agAmount);
            $("#lblGratuityAmount").text(gaAmount); //alert(gaAmount);

            $("#hdnCalculatedGratuity").val(agAmount);
        }
    });
}

function CreateSelfNomineeGrid()
{
    var isAlive = $("#ddlIsAlive").val();


    if (isAlive == 0)
    {
        var nomineeName = $("#txtPensionerName").val();
        var nomineeDOB = $("#txtDOB").val();

        
        var C = "{NomineeName: '" + $("#txtNomineeName").val() + "',  NomineeDOB: '" + $("#txtNomineeDOB").val() + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: "PensionerCalculation.aspx/AddSelfNomineeGrid",
            data: C,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".loading-overlay").hide();
                $('#NomineeDetails').html(data.d);
            }
        });
    }
}


















