﻿
var TotalDays = []; var array = []; var ldays = ""; var ArrayNominee = []; var available = false; var arrayTotalGratuity = []; var count = 0;
var TabDescription = "";


$(document).ready(function () {
    FileNo();
    $("#lblYears").hide();
    $("#lblMonths").hide();
    $("#lblDa").hide();
    $("#total").hide();
    $("#txtCommutationPerct").hide();
    $("#spnCommutationPerText").hide();
    $("#btnPenCommuOK").hide();
    $("#ddlInService").prop("disabled", true);
    $("#ddlOccupieQuarter").prop("disabled", true);

    //************************************************************************

    function FileNo()
    {
        var EmpNo = $("#hdnEmpNo").val();
        var C = "{ EmpNo: '" + EmpNo + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/WatermarkforFileNo',
            cache: false,
            data:C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d);
                var mode = $("#hdnFormMode").val();
                if (t == "Available") {

                }
                else {
                    $("#txtFileNo").val('WBFC/PEN CELL/AP/');
                }
            }
        });
    }

    //This is for Populate PayScale
    $(document).ready(function () {

        var options = {};
        options.url = "PensionerCalculation.aspx/GetPayScale";
        options.type = "POST";
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (listPayScale) {
            var t = jQuery.parseJSON(listPayScale.d); //alert(JSON.stringify(t));
            var a = t.length;
            $("#ddlPayScale").empty();
            $("#ddlPayScale").append("<option value=''>(Select PayScale)</option>")
            if (a > 0) {
                $.each(t, function (key, value) {
                    $("#ddlPayScale").append($("<option></option>").val(value.PayScaleID).html(value.PayScale));
                });
            }
            $("#ddlPayScale").prop("disabled", false);
        }
        $.ajax(options);

    });
    
    var tabno = $("#hdnTabNo").val(); //alert(tabno);
    if (tabno != "") {
        var arr = tabno.split(","); //alert(arr);
        if (arr.length > 1) {
            for (var i = 0; i < arr.length; i++) {
                var res = arr[i]; //alert(res);
                if (res == 0) {
                    //alert("enter into 0");
                    $("#tabs").tabs({
                        collapsible: true,
                        selected: 0,
                        disabled: [1, 2, 3, 4, 5, 6,7]
                    });
                    //$("#btnPenDetNext").hide();
                }
                else if (res == 1) {
                    //alert("enter into 1");
                    $("#tabs").tabs({
                        collapsible: true,
                        selected: 1,
                        disabled: [2, 3, 4, 5,6,7]
                    });
                    //$("#btnPenDetailAmount").hide();
                    $("#tabs").tabs({
                        collapsible: true,
                        selected: 2,
                        disabled: [3,4,5,6,7]
                    });
                    //***************************************************************************************************
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Get_PenNonQualiServiebyEmpNo',
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            $('#ReasonbetDate').html(D.d);
                        }
                    });
                    //***************************************************************************************************
                    var comm = ($("#ddlCommutation").val()); //alert(comm);
                    var commPerct = ($("#hdnCommutationPerct").val()); //alert(commPerct);
                    
                    if (comm == "N")
                        CalaculateCommutative(0);
                    else if (comm == "Y")
                    {
                        CalaculateCommutative(commPerct);
                    }
                }
                else if (res == 2) {
                    //alert("enter into 2");
                     $("#tabs").tabs({
                         collapsible: true,
                         selected: 2,
                         disabled: [3,4,5,6,7]
                     });
                     //$("#btnPenQualiNext").hide();
                     $("#tabs").tabs({
                         collapsible: true,
                         selected: 3,
                         disabled: [4, 5, 6, 7]
                     });
                }
                else if (res == 3) {
                    //alert("enter into 3");
                    $("#tabs").tabs({
                        collapsible: true,
                        selected: 3,
                        disabled: [4,5,6,7]
                    });
                    ////************************************************************************************
                     //$("#btnAddNominee").prop("disabled", true);
                     //$("#btnNomineeNext").prop("disabled", true);
                     //$("#btnNomineeNext").hide();

                     $(".loading-overlay").show();
                     var C = "{ DOB: '" + $("#txtDOB").val() + "', EmpName:'" + $.trim($("#txtPensionerName").val()) + "'}";
                     $.ajax({
                         type: "POST",
                         url: pageUrl + '/Loan_NomineeDetails',
                         data: C,
                         cache: false,
                         contentType: "application/json; charset=utf-8",
                         success: function (D) {
                             $(".loading-overlay").hide();
                             $('#NomineeDetails').html(D.d);
                         }
                     });

                     var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
                     var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
                     var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
                     var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

                     var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
                     $(".loading-overlay").show();
                     $.ajax({
                         type: "POST",
                         url: pageUrl + '/GetRetiringGratuity',
                         data: E,
                         cache: false,
                         contentType: "application/json; charset=utf-8",
                         success: function (D) {
                             $(".loading-overlay").hide();
                             var da = JSON.parse(D.d);
                             //alert(JSON.stringify(da));

                             var agAmount = da[0].AG;
                             var gaAmount = da[0].GA;

                             $("#lblActualGratuity").text(agAmount);
                             $("#lblGratuityAmount").text(gaAmount);

                             $("#hdnCalculatedGratuity").val(agAmount);
                         }
                     });

                     var E = "{CalculatedGratuity: '" + $("#hdnCalculatedGratuity").val() + "'}"; //alert(E);
                     $(".loading-overlay").show();
                     $.ajax({
                         type: "POST",
                         url: pageUrl + '/Calculate_NomineeGratuity',
                         data: E,
                         cache: false,
                         contentType: "application/json; charset=utf-8",
                         success: function (D) {
                             $(".loading-overlay").hide();
                             //var jsdata = JSON.parse(D.d); alert(JSON.stringify(jsdata));
                             $('#GratuityNomineeDetails').html(D.d);
                         }
                     });
                    ////*******************************************************************************************
                     $(".loading-overlay").show();
                     $.ajax({
                         type: "POST",
                         url: pageUrl + '/Get_FamilyPensionYesorNot',
                         data: E,
                         cache: false,
                         contentType: "application/json; charset=utf-8",
                         success: function (D) {
                             $(".loading-overlay").hide();
                             var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));
                             if (jsdata == 1) {
                                 //$("#btnNomineeNext").hide();
                                 CalculateFamilyPensionCalculations();
                                 $("#tabs").tabs({
                                     collapsible: true,
                                     selected: 4,
                                     disabled: [5, 6,7]
                                 });
                             }
                             else {

                                 $("#tabs").tabs({
                                     collapsible: true,
                                     selected: 5,
                                     disabled: [4,6,7]
                                 });
                             }
                         }
                     });

                    ////******************************************************************************************************
                }
                else if (res == 4) {
                    //alert("enter into 4");
                    $("#tabs").tabs({
                        collapsible: true,
                        selected: 4,
                        disabled: [5,6,7]
                    });
                    //$("#btnFamilyNext").hide();
                    ////*******************************************************************************************
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Get_FamilyPensionYesorNot',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));
                            if (jsdata == 1) {
                                //$("#btnNomineeNext").hide();
                                CalculateFamilyPensionCalculations();
                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: 5,
                                    disabled: [6, 7]
                                });
                            }
                            else {

                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: 5,
                                    disabled: [4, 6, 7]
                                });
                            }
                        }
                    });

                    ////******************************************************************************************************

                }
                else if (res == 5) {
                    //alert("enter into 5");
                    //$("#btnGratuitySave").hide();
                    $("#tabs").tabs({
                        collapsible: true,
                        selected: 5,
                        disabled: [6,7]
                    });
                    ////*******************************************************************************************
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Get_FamilyPensionYesorNot',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));
                            if (jsdata == 1) {
                                //$("#btnNomineeNext").hide();
                                CalculateFamilyPensionCalculations();
                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: 5,
                                    disabled: [7]
                                });
                            }
                            else {

                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: 5,
                                    disabled: [4, 7]
                                });
                            }
                        }
                    });

                    ////******************************************************************************************************
                }
            }
        }
        else {
            //$("#btnPenDetNext").hide();
            $("#tabs").tabs({
                collapsible: true,
                selected: 0,
                disabled: [2,3, 4,5,6,7]
            });
        }
    }
    else {

        $(function () {
            $("#tabs").tabs({
                collapsible: true,
                selected: 0,
                disabled: [1,2,3, 4,5,6,7]
            });
        });
    }
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "PensionerCalculation.aspx/BindState",
            data: {},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var jsdata = JSON.parse(data.d);
                $("#ddlNomineeState").append("<option value=''>(Select State)</option>")
                $.each(jsdata, function (key, value) {
                    $("#ddlNomineeState").append($("<option></option>").val(value.StateId).html(value.State));
                });
            },
            error: function (data) {
                //alert("error found");
            }
        });
    });
    $(document).ready(function () {
        //$("#ddlDistrict").prop("disabled", true);
        $("#ddlNomineeState").change(function () {

            if ($("#ddlNomineeState").val() != "Please select") {
                var options = {};
                options.url = "PensionerCalculation.aspx/BindDistricts";
                options.type = "POST";
                options.data = "{StateID: " + $('#ddlNomineeState').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listdistrict) {
                    var t = jQuery.parseJSON(listdistrict.d);
                    var a = t.length;
                    $("#ddlNomineeDistrict").empty();
                    if (a > 0) {
                        $("#ddlNomineeDistrict").append("<option value=''>(Select District)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlNomineeDistrict").append($("<option></option>").val(value.DistID).html(value.District));
                        });
                    }
                    $("#ddlNomineeDistrict").prop("disabled", false);
                };
                options.error = function () { alert("Error retrieving Districts!"); };
                $.ajax(options);
            }
            else {
                $("#ddlNomineeDistrict").empty();
                $("#ddlNomineeDistrict").prop("disabled", true);
            }
        });
    });



    //On TABS CLICK EVENTS
    function QualiTab() {
        var isAlive = $("#ddlIsAlive").val();
        var DateofExp = $("#txtExpiryDate").val();

        if (isAlive == "1") {
            $("#txtDOR").val($("#txtExpiryDate").val());
        }
        CalculateTotalServicePeriod();
        //CalculateEffectiveFrom();
    }

    $("#qualitab").click(function (event) {
        QualiTab();
        UpdateDetailAmount();
        Calculation();
        });
    $("#nomineetab").click(function (event) {
        $(".loading-overlay").show();
        var C = "{ DOB: '" + $("#txtDOB").val() + "', EmpName:'" + $.trim($("#txtPensionerName").val()) + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/Loan_NomineeDetails',
            data: C,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#NomineeDetails').html(D.d);
            }
        });
    });
    $("#gratuity").click(function (event) {
        var E = "{CalculatedGratuity: '" + $("#hdnCalculatedGratuity").val() + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/Load_NomineeGratuity',
            data:E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#GratuityNomineeDetails').html(D.d);

                UpdateGratuityDetailAmount();

            }
        });
    });
    $("#detail").click(function (event) {
        $("#ddlPayScale").val($("#hdnPayScale").val());
        QualiTab();
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $("#btnaddReason").click(function (event) {
        var fromdate = $("#txtFromDate").val();
        var todate = $("#txtToDate").val();
        if (fromdate == "") {
            $("#txtFromDate").focus();
        }
        else if (todate == "") {
            $("#txtToDate").focus();
        }
        else
        {
            $("#OverlayNonQualiReason").show();
            $("#txtNonQualiReasons").focus();
            $.ajax({
                type: "POST",
                url: "PensionerCalculation.aspx/LoadNonQualiServiceReason",
                data: {},
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $('#ReasonDetails').html(data.d);
                }
            });
        }
    });

    $(document).ready(function () {
        $("#txtNonQualiReasons").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/FindNonQualiServiceReason',
                    data: "{prefix:'" + request.term + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        //var item = jQuery.parseJSON(data.d); //alert(item);
                        $('#ReasonDetails').html(data.d);
                        //$("#overlay").show();
                    }
                });
            },
            minLength: 0
        });
    });

    $("#cmdNonQualiReason").click(function (e) {
        var Reason = $("#txtNonQualiReasons").val();
        if (Reason != "") {
            $(".loading-overlay").show();
            var C = "{ Reason: '" + $("#txtNonQualiReasons").val() + "'}";
            //alert(C);
            $.ajax({
                type: "POST",
                url: 'PensionerCalculation.aspx/AddNonQualiReason',
                data: C,
                cache: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    $('#ReasonDetails').html(D.d);
                    $("#txtNonQualiReasons").val('');
                    $("#txtNonQualiReasons").focus();
                },
                error: function (response) {
                    alert('');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        else
            $("#txtNonQualiReasons").focus();
    });

    $("#btnNonQualiReasonClose").click(function (e) {
        $("#OverlayNonQualiReason").hide();

        var ReasonID=  $("#hdnReasonID").val();
        var Reason= $("#hdnReason").val();

        var fromDate = $("#txtFromDate").val();
        var toDate = $("#txtToDate").val();
        var C = "{ReasonID:'" + ReasonID + "', Reason: '" + Reason + "',fromDate: '" + fromDate + "',toDate: '" + toDate + "'}";
        $.ajax({
            type: "POST",
            url: "PensionerCalculation.aspx/BindNonQualiwithDate",
            data: C,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $('#ReasonbetDate').html(data.d);
                $('#txtNonQualiFromDate').val('');
                $('#txtNonQualiToDate').val('');
                $('#txtNonQualiReason').val('');
            }
        });

        CalculateTotalNonQualiServicePeriod();

    });

    $(document).ready(function () {
        $("[id*=chkSelect]").live("click", function () {
            if ($(this).is(":checked")) {
                $("input[id*='chkSelect']:checkbox").each(function (index) {
                    if ($(this).is(":checked")) {
                        $(this).attr("checked", false);
                    }
                });
                $(this).attr("checked", true);
                ReasonID = $(this).closest('tr').find('td:eq(1)').text();
                Reason = $(this).closest('tr').find('td:eq(2)').text();

                var lre = /^\s*/;
                var rre = /\s*$/;
                ReasonID = ReasonID.replace(lre, "");
                ReasonID = ReasonID.replace(rre, "");

                var lre = /^\s*/;
                var rre = /\s*$/;
                Reason = Reason.replace(lre, "");
                Reason = Reason.replace(rre, "");
                //alert(ReasonID); alert(Reason);

                $("#txtNonQualiReasons").val(Reason);
                $("#hdnReasonID").val(ReasonID);
                $("#hdnReason").val(Reason);

                //var fromDate = $("#txtFromDate").val();
                //var toDate = $("#txtToDate").val();
                //var C = "{ReasonID:'" + ReasonID + "', Reason: '" + Reason + "',fromDate: '" + fromDate + "',toDate: '" + toDate + "'}";
                //$.ajax({
                //    type: "POST",
                //    url: "PensionerCalculation.aspx/BindNonQualiwithDate",
                //    data: C,
                //    dataType: "json",
                //    contentType: "application/json; charset=utf-8",
                //    success: function (data) {
                //        $('#ReasonbetDate').html(data.d);
                //        $('#txtNonQualiFromDate').val('');
                //        $('#txtNonQualiToDate').val('');
                //        $('#txtNonQualiReason').val('');
                //    }
                //});

                //CalculateTotalNonQualiServicePeriod();
                //CalculateTotalQualiServicePeriod();
            }
        });
    });

    $("#ddlRopa").change(function () {
        var ropaid = $("#ddlRopa").val();
        if (ropaid == 4) {
            $("#txtRopaRelief").val('');
            document.getElementById("txtRopaRelief").disabled = true;

            //var dor = $("#txtDOR").val();
            CalculateRates(ropaid);
        }
        else {
            document.getElementById("txtRopaRelief").disabled = false;
            //var dor = $("#txtDOR").val();
            CalculateRates(ropaid);
        }
    });

    function CalculateRates(ropaid) {
        var E = "{ropaid: '" + ropaid + "'}";
        $.ajax({
            type: "POST",
            url: "PensionerCalculation.aspx/CalculateRate",  //pageUrl + '/CalculateRate',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d);
                $("#hdnRopaRate").val(t);
            }
        });
    }

    $("#ddlAdvanceCase").change(function () {

        var remarkID = $("#ddlAdvanceCase").val();
        var C = "{ remarkID: '" + remarkID + "'}";
        if (remarkID != "") {
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: 'PensionerCalculation.aspx/Add_AdvanceRemarks',
                data: C,
                cache: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var data = JSON.parse(D.d); //alert(data);
                    if (data.length > 0) {
                        $("#txtRemarks").val(data);
                    }
                    else {
                        $("#txtRemarks").val('');
                    }
                }
            });
        }
    });

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                    //Pensioner Case with File No. Tab Details 

    $("#btnPenDetNext").click(function (event) {
        $("#ddlPayScale").val($("#hdnPayScale").val());

        var FileNo = $("#txtFileNo").val(); 
        var PensionScheme = $("#ddlPensionScheme").val(); 
        var SlNo = $("#txtSlNo").val(); 
        var PageNo = $("#txtPageNo").val(); 
        var VolNo = $("#txtVolNo").val(); 
        var OpLocNo = $("#txtOpLocNo").val();
        var penScheme = $("#ddlPensionScheme").val();

        var isAlive = $("#ddlIsAlive").val();
        var DateofExp = $("#txtExpiryDate").val(); 

        if (isAlive == "1") {
            $("#txtDOR").val($("#txtExpiryDate").val()); 
        }
        
        if (FileNo == "")
        {
            var msg = "Please Enter Case File No.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "fileNo";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
      else  if (PensionScheme == "") {
            var msg = "Please Select Pension Scheme.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "PensionScheme";
                    $('#hdnDialogResult').val(res);
                }
            });
      }
      else if (penScheme==1 && SlNo == "") {
          var msg = "Please Enter Sl No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "SlNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (penScheme == 1 &&  PageNo == "") {
          var msg = "Please Enter Page No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "PageNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (penScheme == 1 &&  VolNo == "") {
          var msg = "Please Enter Volume No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "VolNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (penScheme == 1 && OpLocNo == "") {
          var msg = "Please Enter Opting Location No.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "OpLocNo";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else if (isAlive == 1 && DateofExp=="") {
          var msg = "Please Enter Date of Expiry.";
          var E = "{Message: '" + msg + "'}";
          $.ajax({
              type: "POST",
              url: pageUrl + '/MessageBox',
              data: E,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  $('#showMessage').html(D.d);
                  $("#btnOK").focus();
                  var res = "dateofexpiry";
                  $('#hdnDialogResult').val(res);
              }
          });
      }
      else {
          var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
          $.ajax({
              type: "POST",
              url: pageUrl + '/GetTabNo',
              data: C,
              cache: false,
              contentType: "application/json; charset=utf-8",
              success: function (D) {
                  var value = jQuery.parseJSON(D.d);
                  TabDescription = value;
                  if (TabDescription == "" || TabDescription==0)
                  {
                      TabDescription = 0;
                  }

                  var LocationNo = $("#txtLocationCode").val();
                  var A = "{LocationNo: '" + LocationNo + "'}";
                  $(".loading-overlay").show();
                  $.ajax({
                      type: "POST",
                      url: pageUrl + '/Check_LocationAvailability',
                      data: A,
                      cache: false,
                      contentType: "application/json; charset=utf-8",
                      success: function (D) {
                          $(".loading-overlay").hide();
                          var da = JSON.parse(D.d); //alert(JSON.stringify(da));
                          if (da == "Available") {

                              var E = "{FileNo: '" + FileNo + "', PensionScheme: '" + PensionScheme + "', SlNo: '" + SlNo + "', PageNo: '" + PageNo + "', VolNo: '" + VolNo + "', OpLocNo: '" + OpLocNo + "', isAlive: '" + isAlive + "', DateofExp: '" + DateofExp + "',  TabDescription: '" + TabDescription + "'}";
                              $(".loading-overlay").show();
                              $.ajax({
                                  type: "POST",
                                  url: pageUrl + '/Update_PenPensionerFileNoTabDetail',
                                  data: E,
                                  cache: false,
                                  contentType: "application/json; charset=utf-8",
                                  success: function (D) {
                                      $(".loading-overlay").hide();
                                      var da = JSON.parse(D.d); //alert(JSON.stringify(da));
                                  }
                              });

                              $("#tabs").tabs({
                                  collapsible: true,
                                  selected: 1,
                                  disabled: [2, 3, 4, 5, 6, 7]
                              });
                              //$("#btnPenDetNext").hide();
                              $("#btnPenDetNext").css('color', 'red');
                              CalculateTotalServicePeriod();
                          }
                          else {
                              var msg = "Sorry ! This Location is not Available.";
                              var E = "{Message: '" + msg + "'}";
                              $.ajax({
                                  type: "POST",
                                  url: pageUrl + '/MessageBox',
                                  data: E,
                                  cache: false,
                                  contentType: "application/json; charset=utf-8",
                                  success: function (D) {
                                      $('#showMessage').html(D.d);
                                      $("#btnOK").focus();
                                      var res = "location";
                                      $('#hdnDialogResult').val(res);
                                  }
                              });
                          }
                      }
                  });
              }
          });
      }
    });
    $(document).ready(function () {
        $('#btnOK').live('click', function () {
            var result = $('#hdnDialogResult').val(); //alert(result);
            if (result == "fileNo")
                $("#txtFileNo").focus();
            else if (result == "PensionScheme")
                $("#ddlPensionScheme").focus();
            else if (result == "SlNo")
                $("#txtSlNo").focus();
            else if (result == "PageNo")
                $("#txtPageNo").focus();
            else if (result == "VolNo")
                $("#txtVolNo").focus();
            else if (result == "OpLocNo")
                $("#txtOpLocNo").focus();
            else if (result == "ropa")
                $("#ddlRopa").focus();
            else if (result == "roparelief")
                $("#txtRopaRelief").focus();
            else if (result == "effectFrom")
                $("#txtEfffectFrom").focus();
            else if (result == "Commutation")
                $("#ddlCommutation").focus();
            else if (result == "nomineeType")
                $("#ddlNomineeType").focus();
            else if (result == "nomineename")
                $("#txtNomineeName").focus();
            else if (result == "nomineerelation")
                $("#ddlNomineeRelation").focus();
            else if (result == "nomineerDOB")
                $("#txtNomineeDOB").focus();
            else if (result == "nomineerTypeRepeat")
                $("#ddlNomineeType").focus();
            else if (result == "nomineeridmark")
                $("#txtNomineeIDMark").focus();
            else if (result == "familyPension")
                $("#ddlFamilyPension").focus();
            else if (result == "gratuitypercentage")
                $("#txtGratuityPercentage").focus();
            else if (result == "nonomineedetails")
                $("#ddlNomineeType").focus();
            else if (result == "commutamt")
                $("#txtCommutationPerct").focus();
            else if (result == "CommutationPerct")
                $("#txtCommutationPerct").focus();
            else if (result == "nomineeservice")
                $("#ddlInService").focus();
            else if (result == "occupyquarter")
                $("#ddlOccupieQuarter").focus();
            else if (result == "dateofexpiry")
                $("#txtExpiryDate").focus();
            else if (result == "finalcalculation") {
                //window.location.href = "PensionerSelection.aspx";
                var EmployeeNo = $("#hdnEmpNo").val();
                var OpenCondition = "FromPenCalculation";
                window.location.href = "NewPensionerBankDetail.aspx?EmpNo=" + EmployeeNo + "&OpenCondition=" + OpenCondition;
            }
            else if (result == "location")
                $("#txtLocationCode").focus();
            else if (result == "familyPensionRank")
                $("#txtFamilyPensionRank").focus();
            else if (result == "familyPensionEffectiveFrom")
                $("#txtFamilyPenEffectFrom").focus();
            else if (result == "familyPensionEffectiveTo")
                $("#txtFamilyPenEffectTo").focus();
            else if (result == "GratuityRank")
                $("#txtGratuityRank").focus();
        });
    }); 
    $("#ddlIsAlive").change(function () {
        var isalive = $("#ddlIsAlive").val();
        if (isalive == 0) {
            $("#txtExpiryDate").prop("disabled", true);
            $("#txtExpiryDate").val('');
        }
        else {
            $("#txtExpiryDate").prop("disabled", false);
        }
    });
    $("#txtLocationCode").blur(function (event) {
        //********************************************************************************
        var LocationNo = $("#txtLocationCode").val();
        var A = "{LocationNo: '" + LocationNo + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/Check_LocationAvailability',
            data: A,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var da = JSON.parse(D.d); //alert(JSON.stringify(da));
                if (da == "Available") {
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Get_SectorName',
                        data: A,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            var da = JSON.parse(D.d); //alert(JSON.stringify(da));
                            if (da != "") {
                                $("#lblCenterName").text(da);
                            }
                        }
                    });
                }
                else
                {
                    var msg = "Sorry ! This Location is not Available.";
                    var E = "{Message: '" + msg + "'}";
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/MessageBox',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $('#showMessage').html(D.d);
                            $("#btnOK").focus();
                            var res = "location";
                            $('#hdnDialogResult').val(res);
                        }
                    });
                }
            }
        });

        //********************************************************************************
    });
    $("#ddlPensionScheme").change(function () {
        var penScheme = $("#ddlPensionScheme").val(); 
        if (penScheme == 2 || penScheme ==3) {
            $("#txtSlNo").prop("disabled", true);
            $("#txtPageNo").prop("disabled", true);
            $("#txtVolNo").prop("disabled", true);
            $("#txtOpLocNo").prop("disabled", true);
            $("#txtSlNo").val(''); $("#txtPageNo").val(''); $("#txtVolNo").val(''); $("#txtOpLocNo").val('');
        }
        else {
            $("#txtSlNo").prop("disabled", false);
            $("#txtPageNo").prop("disabled", false);
            $("#txtVolNo").prop("disabled", false);
            $("#txtOpLocNo").prop("disabled", false);
        }
    });
    
                                    //End of Pensioner Case with File No. Tab Details End
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                    //Pensioner Amount Tab Details 

    $(document).ready(function () {
        $("#btnPenDetailAmount").click(function (event) {
            $(".loading-overlay").show();
            QualiTab();
            SavePayScaleID();

            var size = 0; var gridSize = 0;

            var grid = document.getElementById("grvPensionerDetail");
            gridSize = grid.rows.length - 2;
            for (var row = 0; row < grid.rows.length - 1; row++) {
                var txtAmountReceive = $("input[id*=txtEDAmount]");   //alert(txtAmountReceive[row].value);
                var EDAmount = txtAmountReceive[row].value; //alert(EDAmount);

                var lblEDID = grid.rows[row + 1].cells[0];
                var EDID = lblEDID.textContent.toString(); //alert(valueFromIndentNo);

                var lre = /^\s*/;
                var rre = /\s*$/;
                EDID = EDID.replace(lre, "");
                EDID = EDID.replace(rre, "");

                var lblEmpNo = grid.rows[row + 1].cells[1];
                var EmpNo = lblEmpNo.textContent.toString(); //alert(valueFromIndentNo);

                var lre = /^\s*/;
                var rre = /\s*$/;
                EmpNo = EmpNo.replace(lre, "");
                EmpNo = EmpNo.replace(rre, "");

                size = size + 1; //alert(size);

                var E = "{EDID: '" + EDID + "', EDAmount: '" + EDAmount + "', EmpNo: '" + EmpNo + "'}";
                //alert(E);
               
                $.ajax({
                    type: "POST",
                    url: "PensionerCalculation.aspx/Insert_PensionerDetailAmount",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                       
                        var t = JSON.parse(D.d); 
                        if (t == "save") {
                            if (size = gridSize) {
                                //This is for Update of qualitab detail amount after 2nd Tab detail amount change
                                UpdateDetailAmount();

                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: 2,
                                    disabled: [3, 4, 5, 6,7]
                                });
                                //$("#btnPenDetailAmount").hide();
                                $("#btnPenDetailAmount").css('color', 'red');
                            }
                        }
                    }
                });

            }
            //*****************************************************************************************
            var TabNo = 1; var TabDescr = false;
            var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetTabNo',
                data: C,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var value = jQuery.parseJSON(D.d);
                    TabDescription = value; //alert(TabDescription);

                    if (TabDescription == 0) {
                        TabDescription = TabDescription + ',' + TabNo;
                    }
                    else {
                        var arrs = TabDescription.split(","); //alert(arrs.length);
                        if (arrs.length > 1) {  
                            for (var k = 0; k < arrs.length; k++) {
                                var tab = arrs[k];
                                if (tab == TabNo) {
                                    //alert("match");
                                    TabDescr = true;
                                    break;
                                }
                                else {
                                    //alert("notmatch");
                                    TabDescr = false;
                                }
                            }
                            //alert(TabDescr);
                            if (TabDescr == true)
                                TabDescription = TabDescription;
                            else
                                TabDescription = TabDescription + ',' + TabNo;
                        }
                    }
                    var E = "{TabDescription: '" + TabDescription + "'}"; //alert(E);

                    $.ajax({
                        type: "POST",
                        data: E,
                        url: "PensionerCalculation.aspx/Update_PensionAmountTabDesc",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                        }
                    });

                }
            });

        });
    });
    function UpdateDetailAmount()
    {
        $(".loading-overlay").show();
        var S = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(S);
        $.ajax({
            type: "POST",
            data: S,
            url: "PensionerCalculation.aspx/Update_AfterChangeofDetailAmount",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                var gradePay = t[0].GradePay;
                var payScale = t[0].PayScale;
                var payBand = t[0].PayBand;

                $("#lblGradePay").text(gradePay);
                $("#lblPayBand").text(payScale);
                $("#lblPayintheBand").text(payBand);

                Calculation();
            }
        });
    }
    function SavePayScaleID()
    {
        var payscaleid = $("#ddlPayScale").val();
        var S = "{payscaleid: '" + payscaleid + "', EmpNo: '" + $("#hdnEmpNo").val() + "'}";
        $.ajax({
            type: "POST",
            data: S,
            url: "PensionerCalculation.aspx/Save_PayScaleID",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(JSON.stringify(t));
            }
        });
    }
    function BindPayScale()
    {
        $("#ddlPayScale").val($("#hdnPayScale").val());
    }
    $("#ddlPayScale").change(function () {
        var GradePay = '';
        var payscaleid = $("#ddlPayScale").val();
        var P = $('#ddlPayScale').find('option:selected').text();

        //=============================
        var S = "{payscaleid: " + payscaleid + "}"; 
        $.ajax({
            type: "POST",
            data: S,
            url: "PensionerCalculation.aspx/Get_PayBand",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var t = JSON.parse(D.d); //alert(JSON.stringify(t));
               
                var payBand = t[0].GradePay;

                var Grid = document.getElementById("grvPensionerDetail");
                for (var row = 1; row < Grid.rows.length; row++) {
                    var GridFrom_Cell = Grid.rows[row].cells[2];
                    var valueFromIndentNo = GridFrom_Cell.textContent.toString();
                    if (valueFromIndentNo == 'GRADE PAY') {
                        for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                            ValPOQty = $("input[id*=txtEDAmount]");
                            ValPOQty[row1].value = payBand;
                        }
                    }
                }
            }
        });
        //============================
        
       
    });

                                    //End of Pensioner Amount Tab Details 
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                        //Quali Tab Details

    $("#btnPenQualiNext").click(function (event) {

        var ropaID = $("#ddlRopa").val();
        var ropaRelief = $("#txtRopaRelief").val();

        var commtVal = $("#ddlCommutation").val();
        var commtPerct = $("#txtCommutationPerct").val();

        if (ropaID == "") {
            var msg = "Please Select Ropa.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "ropa";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if (ropaRelief == "" && ropaID!=4)
        {
            var msg = "Please Enter Ropa Relief.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "roparelief";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if ($('#txtEfffectFrom').val()=="") {
            var msg = "Please Enter Effect From Date.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "effectFrom";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if ($('#ddlCommutation').val() == "") {
            var msg = "Please Select Pension Commutation.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "Commutation";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if (commtVal == "Y" && commtPerct=="") {
            var msg = "Please Enter Commutation Percentage.";
            var E = "{Message: '" + msg + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/MessageBox',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#showMessage').html(D.d);
                    $("#btnOK").focus();
                    var res = "CommutationPerct";
                    $('#hdnDialogResult').val(res);
                }
            });
        }
        else if (commtVal == "Y" && commtPerct != "") {
            var lblCommutPerct = $('#lblCommutingPercentage').text(); //alert(lblCommutPerct);
            if (lblCommutPerct == "") {
                var msg = "Please Calculate Pension Commutation First.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "CommutationPerct";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else
            {
                var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
                var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
                var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
                var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

                var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
                $(".loading-overlay").show();
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/GetRetiringGratuity',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        var da = JSON.parse(D.d);
                        //alert(JSON.stringify(da));

                        var agAmount = da[0].AG;
                        var gaAmount = da[0].GA;

                        $("#lblActualGratuity").text(agAmount);
                        $("#lblGratuityAmount").text(gaAmount);

                        //$("#hdnCalculatedGratuity").val(agAmount);
                        $("#hdnCalculatedGratuity").val(gaAmount);
                    }
                });
                //*************************************************************************************************************************************
                var TabNo = 2; var TabDescr = false;
                var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/GetTabNo',
                    data: C,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var value = jQuery.parseJSON(D.d);
                        TabDescription = value; //alert(TabDescription);

                        if (TabDescription == 0) {
                            TabDescription = TabDescription + ',' + TabNo;
                        }
                        else {
                            var arrs = TabDescription.split(","); //alert(arrs.length);
                            if (arrs.length > 1) {
                                for (var k = 0; k < arrs.length; k++) {
                                    var tab = arrs[k];
                                    if (tab == TabNo) {
                                        //alert("match");
                                        TabDescr = true;
                                        break;
                                    }
                                    else {
                                        //alert("notmatch");
                                        TabDescr = false;
                                    }
                                }
                                //alert(TabDescr);
                                if (TabDescr == true)
                                    TabDescription = TabDescription;
                                else
                                    TabDescription = TabDescription + ',' + TabNo;
                            }
                        }
                        var ipfPageNo = $("#txtIPFPageNo").val();
                        var ipfVolNo = $("#IPFVolNo").val();

                        var totalServYear = $("#txtTotalServiceYear").val(); var totalServMonth = $("#txtTotalServiceMonth").val(); var totalServDay = $("#txtTotalServiceDay").val();
                        var totalNonQualiServYear = $("#txtTotalNonQualiServiceYear").val(); var totalNonQualiServMonth = $("#txtTotalNonQualiServiceMonth").val(); var totalNonQualiServDay = $("#txtTotalNonQualiServiceDay").val();
                        var totalQualiServYear = $("#txtTotalQualiServiceYear").val(); var totalQualiServMonth = $("#txtTotalQualiServiceMonth").val(); var totalQualiServDay = $("#txtTotalQualiServiceDay").val();
                        var sixMonthPeriod = $("#lblMonthlyPeriod").text();
                        var pensionPerMonth = $("#lblPensionPerMonth").text();
                        var pensionEffecDate = $("#txtEfffectFrom").val();
                        var commutation = $("#ddlCommutation").val();
                        var commutationPerct = $("#txtCommutationPerct").val();
                        var ageonnextBirthday = $("#hdnAgeOnNextBirthday").val();
                        var commutationValue = $("#lblCommutedPensionAmount").text();
                        var valueofCommutedPension = $("#lblCommutedFinalAmount").text();
                        var netPension = $("#lblReducedCommutationAmount").text();
                        var TotalLeapYear = $("#txtTotalLeapYear").val();

                        var DOB = $("#txtDOB").val();
                        var DOJ = $("#txtDOJ").val();
                        var DOR = $("#txtDOR").val();

                        var K = "{ropaID: '" + ropaID + "', ropaRelief: '" + ropaRelief + "', ipfPageNo: '" + ipfPageNo + "', ipfVolNo: '" + ipfVolNo + "',"
                                  + " totalServYear: '" + totalServYear + "', totalServMonth: '" + totalServMonth + "', totalServDay: '" + totalServDay + "',"
                                  + " totalNonQualiServYear: '" + totalNonQualiServYear + "', totalNonQualiServMonth: '" + totalNonQualiServMonth + "', totalNonQualiServDay: '" + totalNonQualiServDay + "',"
                                  + " totalQualiServYear: '" + totalQualiServYear + "', totalQualiServMonth: '" + totalQualiServMonth + "', totalQualiServDay: '" + totalQualiServDay + "',"
                                  + " sixMonthPeriod: '" + sixMonthPeriod + "',  pensionPerMonth: '" + pensionPerMonth + "', pensionEffecDate: '" + pensionEffecDate + "',"
                                  + " commutation: '" + commutation + "', ageonnextBirthday: '" + ageonnextBirthday + "', commutationValue: '" + commutationValue + "',"
                                  + " valueofCommutedPension: '" + valueofCommutedPension + "', netPension: '" + netPension + "', commutationPerct: '" + commutationPerct + "',"
                                  + " TotalLeapYear: '" + TotalLeapYear + "', DOB: '" + DOB + "', DOJ: '" + DOJ + "', DOR: '" + DOR + "', TabDescription:'" + TabDescription + "' }";
                        //alert(E);
                        $(".loading-overlay").show();
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/Update_NonQualiTabDetails',
                            data: K,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $(".loading-overlay").hide();
                                var da = JSON.parse(D.d);
                                //alert(JSON.stringify(da));

                            }
                        });


                        var C = "{ DOB: '" + $("#txtDOB").val() + "', EmpName:'" + $.trim($("#txtPensionerName").val()) + "'}";
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/Loan_NomineeDetails',
                            data: C,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#NomineeDetails').html(D.d);
                            }
                        });

                        //$("#btnPenQualiNext").hide();
                        $("#btnPenQualiNext").css('color', 'red');

                        $("#tabs").tabs({
                            collapsible: true,
                            selected: 3,
                            disabled: [4, 5, 6, 7]
                        });
                    }
                });
            }
        }
        else {
            var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
            var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
            var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
            var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

            var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetRetiringGratuity',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var da = JSON.parse(D.d);
                    //alert(JSON.stringify(da));

                    var agAmount = da[0].AG;
                    var gaAmount = da[0].GA;

                    $("#lblActualGratuity").text(agAmount);
                    $("#lblGratuityAmount").text(gaAmount);

                    //$("#hdnCalculatedGratuity").val(agAmount);
                    $("#hdnCalculatedGratuity").val(gaAmount);
                }
            });
            //*************************************************************************************************************************************
            var TabNo = 2; var TabDescr = false;
            var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetTabNo',
                data: C,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var value = jQuery.parseJSON(D.d);
                    TabDescription = value; //alert(TabDescription);

                    if (TabDescription == 0) {
                        TabDescription = TabDescription + ',' + TabNo;
                    }
                    else {
                        var arrs = TabDescription.split(","); //alert(arrs.length);
                        if (arrs.length > 1) {
                            for (var k = 0; k < arrs.length; k++) {
                                var tab = arrs[k];
                                if (tab == TabNo) {
                                    //alert("match");
                                    TabDescr = true;
                                    break;
                                }
                                else {
                                    //alert("notmatch");
                                    TabDescr = false;
                                }
                            }
                            //alert(TabDescr);
                            if (TabDescr == true)
                                TabDescription = TabDescription;
                            else
                                TabDescription = TabDescription + ',' + TabNo;
                        }
                    }
                    var ipfPageNo = $("#txtIPFPageNo").val();
                    var ipfVolNo = $("#IPFVolNo").val();

                    var totalServYear = $("#txtTotalServiceYear").val(); var totalServMonth = $("#txtTotalServiceMonth").val(); var totalServDay = $("#txtTotalServiceDay").val();
                    var totalNonQualiServYear = $("#txtTotalNonQualiServiceYear").val(); var totalNonQualiServMonth = $("#txtTotalNonQualiServiceMonth").val(); var totalNonQualiServDay = $("#txtTotalNonQualiServiceDay").val();
                    var totalQualiServYear = $("#txtTotalQualiServiceYear").val(); var totalQualiServMonth = $("#txtTotalQualiServiceMonth").val(); var totalQualiServDay = $("#txtTotalQualiServiceDay").val();
                    var sixMonthPeriod = $("#lblMonthlyPeriod").text();
                    var pensionPerMonth = $("#lblPensionPerMonth").text();
                    var pensionEffecDate = $("#txtEfffectFrom").val();
                    var commutation = $("#ddlCommutation").val();
                    var commutationPerct = $("#txtCommutationPerct").val();
                    var ageonnextBirthday = $("#hdnAgeOnNextBirthday").val();
                    var commutationValue = $("#lblCommutedPensionAmount").text();
                    var valueofCommutedPension = $("#lblCommutedFinalAmount").text();
                    var netPension = $("#lblReducedCommutationAmount").text();
                    var TotalLeapYear = $("#txtTotalLeapYear").val();

                    var DOB = $("#txtDOB").val();
                    var DOJ = $("#txtDOJ").val();
                    var DOR = $("#txtDOR").val();


                    var K = "{ropaID: '" + ropaID + "', ropaRelief: '" + ropaRelief + "', ipfPageNo: '" + ipfPageNo + "', ipfVolNo: '" + ipfVolNo + "',"
                              + " totalServYear: '" + totalServYear + "', totalServMonth: '" + totalServMonth + "', totalServDay: '" + totalServDay + "',"
                              + " totalNonQualiServYear: '" + totalNonQualiServYear + "', totalNonQualiServMonth: '" + totalNonQualiServMonth + "', totalNonQualiServDay: '" + totalNonQualiServDay + "',"
                              + " totalQualiServYear: '" + totalQualiServYear + "', totalQualiServMonth: '" + totalQualiServMonth + "', totalQualiServDay: '" + totalQualiServDay + "',"
                              + " sixMonthPeriod: '" + sixMonthPeriod + "',  pensionPerMonth: '" + pensionPerMonth + "', pensionEffecDate: '" + pensionEffecDate + "',"
                              + " commutation: '" + commutation + "', ageonnextBirthday: '" + ageonnextBirthday + "', commutationValue: '" + commutationValue + "',"
                              + " valueofCommutedPension: '" + valueofCommutedPension + "', netPension: '" + netPension + "', commutationPerct: '" + commutationPerct + "',"
                               + " TotalLeapYear: '" + TotalLeapYear + "', DOB: '" + DOB + "', DOJ: '" + DOJ + "', DOR: '" + DOR + "' , TabDescription:'" + TabDescription + "'}";
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Update_NonQualiTabDetails',
                        data: K,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var da = JSON.parse(D.d);
                            //alert(JSON.stringify(da));

                        }
                    });
                    //**************************************************************************************************************************************
                    //$("#btnPenQualiNext").hide();
                    $("#btnPenQualiNext").css('color', 'red');
                    $("#tabs").tabs({
                        collapsible: true,
                        selected: 3,
                        disabled: [4, 5, 6, 7]
                    });
                }
            });

       }
    });
    $("#ddlCommutation").change(function () {
        var commut = $("#ddlCommutation").val();
        if (commut == "Y") {

            $("#txtCommutationPerct").show();
            $("#spnCommutationPerText").show();
            $("#btnPenCommuOK").show();
            $("#txtCommutationPerct").focus();
        }
        else {
            CalaculateCommutative(0);
            $("#txtCommutationPerct").val('');
            $("#txtCommutationPerct").hide();
            $("#spnCommutationPerText").hide();
            $("#btnPenCommuOK").hide();
            //$("#total").hide();
        }
    });
    $("#btnPenCommuOK").click(function (event) {
        var commuPerct = "";
        var commPerct = $("#txtCommutationPerct").val();
        if (commPerct != "") {
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: pageUrl + '/Load_MaxCommutativeAmount',
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                    commuPerct = t;
                    if (commPerct > commuPerct) {
                        //commPerct = 0;
                        //CalaculateCommutative(commPerct);
                        var msg = "Sorry ! Commutative Amount should not be more than " + commuPerct;
                        var E = "{Message: '" + msg + "'}";
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/MessageBox',
                            data: E,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#showMessage').html(D.d);
                                $("#btnOK").focus();
                                var res = "commutamt";
                                $('#hdnDialogResult').val(res);
                            }
                        });
                    }
                    else
                    {
                        CalaculateCommutative(commPerct);
                    }
                }
            });
        }
        else
        {
            $("#txtCommutationPerct").focus();
        }
    });
    function CalaculateCommutative(CommutatiePerct)
    {
        $("#total").show();
        //var dob = new Date($("#txtDOB").datepicker('getDate')); alert(dob);
        //var today = new Date(); alert(today);

        //var diff_date = today - dob; alert(diff_date);
        //var years = Math.floor(diff_date / 31536000000); //alert(years);

        //var AgeonNextYear = parseInt(years) + 1;
        //var PensionPerMonth = $("#lblPensionPerMonth").text();
        //$("#hdnAgeOnNextBirthday").val(AgeonNextYear);
//**************************************************************************************************************************
        dorDate = ""; var dorMonth = ""; var dorYear = ""; var DOR = ""; var AgeonNextYear = "";
        var dob = $("#txtDOB").val(); //alert(dob);
        //var today = new Date(); //alert(today);
        var dor = $("#txtDOR").val(); //alert(dor);

        //dorDate = today.getDate() < 10 ? '0' + today.getDate() : today.getDate(); //alert(dorDate);

        //if (today.getMonth() == 9)
        //    dorMonth = parseInt(today.getMonth()) + 1;
        //else
        //    dorMonth = today.getMonth() < 10 ? '0' + (parseInt(today.getMonth()) + 1) : (parseInt(today.getMonth()) + 1); //alert(dorMonth);

        //    dorYear = today.getFullYear(); //alert(dorYear);
        //    DOR = dorDate + '/' + dorMonth + '/' + dorYear; alert(DOR);

        var E = "{fromdate: '" + dob + "', todate: '" + dor + "'}"; //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/Calculate_TotalServiceYear',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(t);
                var arr = t.split(',');
                var years = arr[0]; //alert(years);
                var months = arr[1]; //alert(months);
                var days = arr[2]; //alert(days);

                //********************************************************
                var arrs = dob.split('/');
                var da = arrs[0]; //alert(da);
                if (da == "01")
                {
                    AgeonNextYear = parseInt(years) + 2; //alert(AgeonNextYear);
                }
                else
                {
                    AgeonNextYear = parseInt(years) + 1; //alert(AgeonNextYear);
                }
               //********************************************************

                
                var PensionPerMonth = $("#lblPensionPerMonth").text();
                $("#hdnAgeOnNextBirthday").val(AgeonNextYear);

                var C = "{PensionPerMonth: '" + PensionPerMonth + "', AgeonNextYear: '" + AgeonNextYear + "' , CommutativePerct: '" + CommutatiePerct + "'}"; //alert(C);
                $(".loading-overlay").show();
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/Get_Commutation',
                    data: C,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        var t = jQuery.parseJSON(D.d);
                        //alert(JSON.stringify(t));
                        var CommuPerctg = t[0].CommutingPercentage;// alert(CommuPerctg);
                        var GPension = t[0].GrossPension;
                        var CommutdPenAmount = t[0].CommutedPensionAmount;
                        var CommutdRate = t[0].CommutedRate;
                        var CommudFAmount = t[0].CommutedFinalAmount;
                        var RedudCommutatAmount = t[0].ReducedCommutationAmount;

                        $("#lblCommutingPercentage").text(CommuPerctg);
                        $("#lblGrossPension").text(GPension);
                        $("#lblCommutedPensionAmount").text(CommutdPenAmount);
                        $("#lblCommutedPensionAmt").text(CommutdPenAmount);
                        $("#lblComutedRate").text(CommutdRate);
                        $("#lblCommutedFinalAmount").text(CommudFAmount);
                        $("#lblCommutedPenAmt").text(CommutdPenAmount);
                        $("#lblGrPension").text(GPension);
                        $("#lblCommutdPenAmt").text(CommutdPenAmount);
                        $("#lblReducedCommutationAmount").text(RedudCommutatAmount);
                    }
                });
            }
        });
        //*************************************************************************************************************88

    }


                                     //End of Quali Tab Details
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                            //Nominee Details Tab Data Entry  

    $(document).ready(function () {
        $("#btnNomineeNext").click(function (event) {
            var TabNo = 3; var TabDescr = false;
            var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetTabNo',
                data: C,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var value = jQuery.parseJSON(D.d);
                    TabDescription = value; //alert(TabDescription);

                    if (TabDescription == 0) {
                        TabDescription = TabDescription + ',' + TabNo;
                    }
                    else {
                        var arrs = TabDescription.split(","); //alert(arrs.length);
                        if (arrs.length > 1) {
                            for (var k = 0; k < arrs.length; k++) {
                                var tab = arrs[k];
                                if (tab == TabNo) {
                                    //alert("match");
                                    TabDescr = true;
                                    break;
                                }
                                else {
                                    //alert("notmatch");
                                    TabDescr = false;
                                }
                            }
                            //alert(TabDescr);
                            if (TabDescr == true)
                                TabDescription = TabDescription;
                            else
                                TabDescription = TabDescription + ',' + TabNo;
                        }
                    }
                    $(".loading-overlay").show();
                    var C = "{TabDescription: '" + TabDescription + "'}"; //alert(C);
                    $.ajax({
                        type: "POST",
                        data:C,
                        url: pageUrl + '/Update_NomineeTabDetails',
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var jsdata = JSON.parse(D.d);// alert(JSON.stringify(jsdata));
                            gratuity();
                            if (jsdata == "Nodata")
                            {
                                var msg = "Please Add at least one Nominee Details First.";
                                var E = "{Message: '" + msg + "'}";
                                $(".loading-overlay").show();
                                $.ajax({
                                    type: "POST",
                                    url: pageUrl + '/MessageBox',
                                    data: E,
                                    cache: false,
                                    contentType: "application/json; charset=utf-8",
                                    success: function (D) {
                                        $(".loading-overlay").hide();
                                        $('#showMessage').html(D.d);
                                        $("#btnOK").focus();
                                        var res = "nonomineedetails";
                                        $('#hdnDialogResult').val(res);
                                    }
                                });
                            }
                            else {
                                CalculateFamilyPensionCalculations();
                                gratuity();
                                //******************************************************************************************************
                                $(".loading-overlay").show();
                                $.ajax({
                                    type: "POST",
                                    url: pageUrl + '/Get_FamilyPensionYesorNot',
                                    data: E,
                                    cache: false,
                                    contentType: "application/json; charset=utf-8",
                                    success: function (D) {
                                        $(".loading-overlay").hide();
                                        var jsdata = JSON.parse(D.d);
                                        if (jsdata == 1) {
                                            //$("#btnNomineeNext").hide();
                                            $("#btnNomineeNext").css('color', 'red');
                                            $("#tabs").tabs({
                                                collapsible: true,
                                                selected: 4,
                                                disabled: [5, 6, 7]
                                            });
                                        }
                                        else {
                                            // $("#btnNomineeNext").hide();
                                            $("#btnNomineeNext").css('color', 'red');
                                            $("#tabs").tabs({
                                                collapsible: true,
                                                selected: 5,
                                                disabled: [4, 6, 7]
                                            });
                                        }
                                    }
                                });

                                //******************************************************************************************************
                            }
                        }
                    });
                }
            });
        });
    });
    $(document).ready(function () {
        $("#btnAddNominee").click(function (e) {
            var nomineeName = $("#txtNomineeName").val();
            var nomineeRelation = $("#ddlNomineeRelation").val();
            var nomineeDOB = $("#txtNomineeDOB").val();
            var NomiTypeID = $("#ddlNomineeType").val();
            var NomiType = $('#ddlNomineeType').find('option:selected').text();
            var NomiRelation = $('#ddlNomineeRelation').find('option:selected').text();
            var nomineeIDMark = $("#txtNomineeIDMark").val();
            var familyPension = $("#ddlFamilyPension").val();
            var gratuityPerct = $("#txtGratuityPercentage").val();

            var isGovtServ = $("#ddlInService").val();
            var OccupyQuart = $("#ddlOccupieQuarter").val();

            var familyPenRank = $("#txtFamilyPensionRank").val();
            var FPEffecFrom = $("#txtFamilyPenEffectFrom").val();
            var FPEffecTo = $("#txtFamilyPenEffectTo").val();
            var gratuityRank = $("#txtGratuityRank").val();

            if (nomineeRelation == "") {
                var msg = "Please Select Nominee Relation.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineerelation";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (nomineeName == "") {
                var msg = "Please Enter Nominee Name.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineename";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
             
            else if (NomiTypeID == "") {
                    var msg = "Please Select Nominee Type.";
                    var E = "{Message: '" + msg + "'}";
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/MessageBox',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $('#showMessage').html(D.d);
                            $("#btnOK").focus();
                            var res = "nomineeType";
                            $('#hdnDialogResult').val(res);
                        }
                    });
                }
            else if (nomineeDOB == "") {
                var msg = "Please Enter Nominee DOB.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineerDOB";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (nomineeIDMark == "") {
                var msg = "Please Enter Nominee Identification Mark.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineeridmark";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (familyPension == "") {
                var msg = "Please Select Family Pension.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "familyPension";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (gratuityPerct == "") {
                var msg = "Please Enter Gratuity Percentage.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "gratuitypercentage";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (NomiTypeID == 1 && isGovtServ=="") {
                var msg = "Please Select, Is Nominee Service is in Govt/WBFC ?";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "nomineeservice";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (NomiTypeID == 1 && OccupyQuart == "") {
                var msg = "Please Select, Is Nominee Occupy Govt Quarter ?";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "occupyquarter";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else if (familyPenRank == "") {
                var msg = "Please Enter Family Pension Rank ?";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "familyPensionRank";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            //else if (FPEffecFrom == "") {
            //    var msg = "Please Enter Family Pension Effective From Date.";
            //    var E = "{Message: '" + msg + "'}";
            //    $.ajax({
            //        type: "POST",
            //        url: pageUrl + '/MessageBox',
            //        data: E,
            //        cache: false,
            //        contentType: "application/json; charset=utf-8",
            //        success: function (D) {
            //            $('#showMessage').html(D.d);
            //            $("#btnOK").focus();
            //            var res = "familyPensionEffectiveFrom";
            //            $('#hdnDialogResult').val(res);
            //        }
            //    });
            //}
            //else if (FPEffecTo == "") {
            //    var msg = "Please Enter Family Pension Effective To Date.";
            //    var E = "{Message: '" + msg + "'}";
            //    $.ajax({
            //        type: "POST",
            //        url: pageUrl + '/MessageBox',
            //        data: E,
            //        cache: false,
            //        contentType: "application/json; charset=utf-8",
            //        success: function (D) {
            //            $('#showMessage').html(D.d);
            //            $("#btnOK").focus();
            //            var res = "familyPensionEffectiveTo";
            //            $('#hdnDialogResult').val(res);
            //        }
            //    });
            //}
            else if (gratuityRank == "") {
                var msg = "Please Enter Gratuity Rank.";
                var E = "{Message: '" + msg + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/MessageBox',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $('#showMessage').html(D.d);
                        $("#btnOK").focus();
                        var res = "GratuityRank";
                        $('#hdnDialogResult').val(res);
                    }
                });
            }
            else {
                
                //NomiTypeID
                if (NomiTypeID == 1) {
                    var hiddenNomineeType = $("#hdnNomType").val();
                    if (hiddenNomineeType == "") {
                        $("#hdnNomType").val(NomiTypeID);

                        var gtotal = $("#hdnTotalGratuityPerct").val();
                        gtotal = parseInt(gtotal) + parseInt(gratuityPerct);
                        $("#hdnTotalGratuityPerct").val(gtotal);

                        var C = "{ NomineeID: '" + $("#ddlNomineeType").val() + "', NomineeType: '" + NomiType + "' ,  NomineeName: '" + $("#txtNomineeName").val() + "', NomineeRelationID: '" + $("#ddlNomineeRelation").val() + "', NomineeRelation: '" + NomiRelation + "' ," 
                         + "NomineeDOB: '" + $("#txtNomineeDOB").val() + "', NomineeAddress: '" + $("#txtNomineeAddress").val() + "', NomineeState: '" + $("#ddlNomineeState").val() + "', NomineeDistrict: '" + $("#ddlNomineeDistrict").val() + "',"
                         + "NomineeCity: '" + $("#txtNomineeCity").val() + "', NomineePin: '" + $("#txtNomineePin").val() + "', NomineePhone: '" + $("#txtNomineePhone").val() + "'," 
                         + "NomineeIDMark: '" + $("#txtNomineeIDMark").val() + "', FamilyPension: '" + $("#ddlFamilyPension").val() + "', GratuityPerct: '" + $("#txtGratuityPercentage").val() + "' ," 
                         + "isGovtServ: '" + isGovtServ + "', OccupyQuart: '" + OccupyQuart + "', familyPenRank: '" + familyPenRank + "', familyPenEffeFrom: '" + FPEffecFrom + "', familyPenEffeTo: '" + FPEffecTo + "', gratuityRank: '" + gratuityRank + "'}";

                        if ($("#ddlNomineeType").val() == 1 && $("#ddlFamilyPension").val() == 1) {
                            $("#hdnNomineeType").val(1);
                            $("#hdnisFamilyPension").val(1);
                        }
                        $(".loading-overlay").show();
                        $.ajax({
                            type: "POST",
                            url: "PensionerCalculation.aspx/AddNomineeDetails",
                            data: C,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                $(".loading-overlay").hide();
                                $('#NomineeDetails').html(data.d);
                                BlankNomineeFields();

                            }
                        });
                    }
                    else {
                        var msg = "You can not Select this more than one time.";
                        var E = "{Message: '" + msg + "'}";
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/MessageBox',
                            data: E,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#showMessage').html(D.d);
                                $("#btnOK").focus();
                                var res = "nomineerTypeRepeat";
                                $('#hdnDialogResult').val(res);
                            }
                        });
                    }
                }
                else {
                    var gtotal = $("#hdnTotalGratuityPerct").val(); //alert(gtotal);
                    gtotal = parseInt(gtotal) + parseInt(gratuityPerct); //alert(gtotal);
                    $("#hdnTotalGratuityPerct").val(gtotal);

                    var C = "{ NomineeID: '" + $("#ddlNomineeType").val() + "', NomineeType: '" + NomiType + "' ,  NomineeName: '" + $("#txtNomineeName").val() + "', NomineeRelationID: '" + $("#ddlNomineeRelation").val() + "',"
                        + "NomineeRelation: '" + NomiRelation + "' , NomineeDOB: '" + $("#txtNomineeDOB").val() + "', NomineeAddress: '" + $("#txtNomineeAddress").val() + "', NomineeState: '" + $("#ddlNomineeState").val() + "'," 
                        + "NomineeDistrict: '" + $("#ddlNomineeDistrict").val() + "', NomineeCity: '" + $("#txtNomineeCity").val() + "', NomineePin: '" + $("#txtNomineePin").val() + "', NomineePhone: '" + $("#txtNomineePhone").val() + "',"
                        + "NomineeIDMark: '" + $("#txtNomineeIDMark").val() + "', FamilyPension: '" + $("#ddlFamilyPension").val() + "', GratuityPerct: '" + $("#txtGratuityPercentage").val() + "' , isGovtServ: '" + isGovtServ + "',"
                        + "OccupyQuart: '" + OccupyQuart + "', familyPenRank: '" + familyPenRank + "', familyPenEffeFrom: '" + FPEffecFrom + "', familyPenEffeTo: '" + FPEffecTo + "', gratuityRank: '" + gratuityRank + "'}";

                    if ($("#ddlNomineeType").val() == 1 && $("#ddlFamilyPension").val() == 1) {
                        $("#hdnNomineeType").val(1);
                        $("#hdnisFamilyPension").val(1);
                    }
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: "PensionerCalculation.aspx/AddNomineeDetails",
                        data: C,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            $(".loading-overlay").hide();
                            $('#NomineeDetails').html(data.d);
                            BlankNomineeFields();

                        }
                    });
                }

            }
        });
    });
    $("#ddlNomineeType").change(function () {
        var nomineeType = $("#ddlNomineeType").val();
        var familypension = $("#ddlFamilyPension").val();

        if (nomineeType == 1 || nomineeType == 2 || nomineeType == 3 || nomineeType == 8) {
            $("#ddlFamilyPension").val('');
            document.getElementById("ddlFamilyPension").disabled = false;
            $("#ddlInService").prop("disabled", false);
            $("#ddlOccupieQuarter").prop("disabled", false);
        }
        else {
            $("#ddlFamilyPension").val('0');
            document.getElementById("ddlFamilyPension").disabled = true;
            $("#ddlInService").prop("disabled", true);
            $("#ddlInService").val('');
            $("#ddlOccupieQuarter").prop("disabled", true);
            $("#ddlOccupieQuarter").val('');
        }

        //if (nomineeType != 1) {
        //    alert("1");
        //    $("#ddlFamilyPension").val('0');
        //    document.getElementById("ddlFamilyPension").disabled = true;
        //    $("#ddlInService").prop("disabled", true);
        //    $("#ddlInService").val('');
        //    $("#ddlOccupieQuarter").prop("disabled", true);
        //    $("#ddlOccupieQuarter").val('');
        //}
        //else {
        //        $("#ddlFamilyPension").val('');
        //        document.getElementById("ddlFamilyPension").disabled = false;
        //        $("#ddlInService").prop("disabled", false);
        //        $("#ddlOccupieQuarter").prop("disabled", false);
          
        //}
    });
    function BlankNomineeFields()
    {
        $("#ddlNomineeType").val('');
        $("#txtNomineeName").val('');
        $("#ddlNomineeRelation").val('');
        $("#txtNomineeDOB").val('');

        //$("#txtNomineeAddress").val('');
        //$("#ddlNomineeState").val('');
        //$("#ddlNomineeDistrict").val('');
        //$("#txtNomineeCity").val('');
        //$("#txtNomineePin").val('');
        //$("#txtNomineePhone").val('');
        $("#ddlInService").val('');
        $("#ddlOccupieQuarter").val('');

        $("#txtNomineeIDMark").val('');
        $("#ddlFamilyPension").val('');
        $("#txtGratuityPercentage").val('');

        $("#txtFamilyPensionRank").val('');
        $("#txtFamilyPenEffectFrom").val('');
        $("#txtFamilyPenEffectTo").val('');
        $("#txtGratuityRank").val('');

        document.getElementById("ddlFamilyPension").disabled = false;
        document.getElementById("ddlInService").disabled = true;
        document.getElementById("ddlOccupieQuarter").disabled = true;
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    function CalculateFamilyPensionCalculations() {

                    var totalPBPlusGP = parseInt($("#lblGradePay").text()) + parseInt($("#lblPayintheBand").text()); //alert(totalPBPlusGP);
                    var E = "{IsAlive: '" + $("#ddlIsAlive").val() + "', SixMonthlyPerd: '" + $("#lblMonthlyPeriod").text() + "', totalPBPlusGP: '" + totalPBPlusGP + "'}";
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Calculate_FamilyPension',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));

                            if (jsdata.length > 0) {
                                var familypenaft7yrsPct = jsdata[0].FamilyPensionAfter7yrsPCT; //alert(familypenaft7yrsPct);
                                var familypenaft7yrsAmt = jsdata[0].FamilyPensionAfter7yrs;

                                var familypenPct = jsdata[0].FamilyPensionAmtPCT;
                                var familypenAmt = jsdata[0].FamilyPensionAmt;

                                var FamilyPensionEffectiveUpto = jsdata[0].FamilyPensionEffectiveDate; //alert(FamilyPensionEffectiveUpto);
                                var date = new Date(parseInt(FamilyPensionEffectiveUpto.substr(6))); //alert(date);

                                var yer = date.getFullYear(); //alert(yer);
                                var mon = date.getMonth() + 1; //alert(mon);
                                var day = date.getDate(); //alert(day);

                                var finaldate = day + "/" + mon + "/" + yer; //alert(finaldate);

                                var pbplusgp = jsdata[0].lastPBplusGP;

                                $("#lblfamilyaft7yrPct1").text(familypenaft7yrsPct);
                                $("#lblpbplusGp").text(pbplusgp);
                                $("#lblfamilyaft7yrPct2").text(familypenaft7yrsPct);
                                $("#lblfamilyaft7yrAmt").text(familypenaft7yrsAmt);
                                $("#lblfamilyPenPrmonth").text(familypenaft7yrsAmt);


                                $("#lblFamilyPenPct1").text(familypenPct);
                                $("#lblpbplusGp2").text(pbplusgp);
                                $("#lblFamilyPenPct2").text(familypenPct);
                                $("#lblFamilyPenAmt").text(familypenAmt);
                                $("#lblEnhancedFamilyPension").text(familypenAmt);

                                $("#lblFamilyPenPct3").text(familypenPct);
                                $("#lblFamilyPenAmt1").text(familypenAmt);
                                $("#lblfamilyaft7yrPct3").text(familypenaft7yrsPct);
                                $("#lblfamilyaft7yrAmt1").text(familypenaft7yrsAmt);

                                $("#lblFamilyPensionEffectiveUpto").text(finaldate);
                            }
                        }
                    });
    }
    $('#Nomineeworktype a').live('click', function () {
        var TotalGratuityAmt=0;

        flag = $(this).attr('id'); //alert(flag);

        var NomineeTypeID = $(this).attr('itemid'); //alert(NomineeTypeID);
        var GratuityPerct = $(this).closest('tr').find('td:eq(5)').text(); //alert(GratuityPerct);

        var lre = /^\s*/;
        var rre = /\s*$/;
        GratuityPerct = GratuityPerct.replace(lre, "");
        GratuityPerct = GratuityPerct.replace(rre, "");

        var totalGrtamt = $("#hdnTotalGratuityPerct").val();
        TotalGratuityAmt = parseInt(totalGrtamt) - parseInt(GratuityPerct); //alert(TotalGratuityAmt);
        $("#hdnTotalGratuityPerct").val(parseInt(TotalGratuityAmt));

        var E = "{NomineeTypeID: '" + NomineeTypeID + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/Delete_NomineeDetails',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#NomineeDetails').html(D.d);
                BlankNomineeFields();
            }
        });
    });
    $(document).ready(function () {

        $("#txtGratuityRank").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/Check_GratuityRank',
                    data: "{GratuityRank:'" + request.term + "', gratuityPerct:'" + $("#txtGratuityPercentage").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var item = jQuery.parseJSON(data.d); //alert(item);

                        if (item == "notPossible")
                        {
                            var msg = "Gratuity Rank is wrong.";
                            var E = "{Message: '" + msg + "'}";
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/MessageBox',
                                data: E,
                                cache: false,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    $('#showMessage').html(D.d);
                                    $("#btnOK").focus();
                                    var res = "GratuityRank";
                                    $('#hdnDialogResult').val(res);
                                }
                            });
                        }

                    }
                });
            },
            minLength: 1
        });
    });


                                        //End of Nominee Details Tab Data Entry   
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                            //Family Pension Tab Data Entry  

    $("#btnFamilyNext").click(function (event) {
        var TabNo = 4; var TabDescr = false;
        var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/GetTabNo',
            data: C,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var value = jQuery.parseJSON(D.d);
                TabDescription = value; //alert(TabDescription);

                if (TabDescription == 0) {
                    TabDescription = TabDescription + ',' + TabNo;
                }
                else {
                    var arrs = TabDescription.split(","); //alert(arrs.length);
                    if (arrs.length > 1) {
                        for (var k = 0; k < arrs.length; k++) {
                            var tab = arrs[k];
                            if (tab == TabNo) {
                                //alert("match");
                                TabDescr = true;
                                break;
                            }
                            else {
                                //alert("notmatch");
                                TabDescr = false;
                            }
                        }
                        //alert(TabDescr);
                        if (TabDescr == true)
                            TabDescription = TabDescription;
                        else
                            TabDescription = TabDescription + ',' + TabNo;
                    }
                }
                var FamilyPensionRevisedPect = $("#lblfamilyaft7yrPct1").text();
                var FamilyPensionRevised = $("#lblfamilyaft7yrAmt").text();

                var FamilyPensionPect = $("#lblFamilyPenPct1").text();
                var FamilyPensionNormal = $("#lblFamilyPenAmt").text();

                var FamilyPensionEffectiveUpto = $("#lblFamilyPensionEffectiveUpto").text();

                var E = "{FamilyPensionRevisedPect: '" + FamilyPensionRevisedPect + "', FamilyPensionRevised: '" + FamilyPensionRevised + "', FamilyPensionPect: '" + FamilyPensionPect + "', FamilyPensionNormal: '" + FamilyPensionNormal + "', FamilyPensionEffectiveUpto: '" + FamilyPensionEffectiveUpto + "',  TabDescription: '" + TabDescription + "'}";
                //alert(E);
                $(".loading-overlay").show();
                $.ajax({
                    type: "POST",
                    url: 'PensionerCalculation.aspx/Update_FamilyPensionTabDetails',
                    data: E,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        var t = JSON.parse(D.d);
                        if (t == "Updated") {
                            $("#btnFamilyNext").css('color', 'red');
                            $("#tabs").tabs({
                                collapsible: true,
                                selected: 5,
                                disabled: [6, 7]
                            });
                            CalculateGratuityNominee();
                            UpdateGratuityDetailAmount();
                        }
                    }
                });
            }
        });
    });
    function UpdateGratuityDetailAmount() {
        var S = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(E);
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            data: S,
            url: "PensionerCalculation.aspx/Update_AfterChangeofDetailAmount",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                var gradePay = t[0].GradePay;
                var payScale = t[0].PayScale;
                var payBand = t[0].PayBand;
                var da = t[0].DA;

                $("#Label2").text(payBand);
                $("#Label3").text(gradePay);
                $("#Label4").text(da);

                var totalEmoluments = parseInt(payBand) + parseInt(gradePay) + parseInt(da)
                $("#lblTotalEmoluments").text(totalEmoluments);

                Calculation();
            }
        });
    }
    function CalculateGratuityNominee()
    {
        var E = "{CalculatedGratuity: '" + $("#hdnCalculatedGratuity").val() + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/Load_NomineeGratuity',
            data:E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#GratuityNomineeDetails').html(D.d);

                UpdateGratuityDetailAmount();

            }
        });
    }

                                        // End of Family Pension Tab Data Entry  
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                    //Gratuity Tab Details

    $(document).ready(function () {
        $("#btnGratuitySave").click(function (event) {
            var TabNo = 5; var TabDescr = false;
            var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetTabNo',
                data: C,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var value = jQuery.parseJSON(D.d);
                    TabDescription = value; //alert(TabDescription);

                    if (TabDescription == 0) {
                        TabDescription = TabDescription + ',' + TabNo;
                    }
                    else {
                        var arrs = TabDescription.split(","); //alert(arrs.length);
                        if (arrs.length > 1) {
                            for (var k = 0; k < arrs.length; k++) {
                                var tab = arrs[k];
                                if (tab == TabNo) {
                                    //alert("match");
                                    TabDescr = true;
                                    break;
                                }
                                else {
                                    //alert("notmatch");
                                    TabDescr = false;
                                }
                            }
                            //alert(TabDescr);
                            if (TabDescr == true)
                                TabDescription = TabDescription;
                            else
                                TabDescription = TabDescription + ',' + TabNo;
                        }
                    }
                    var CalculatedGratuity = $("#lblActualGratuity").text();
                    var FinalGratuity = $("#lblGratuityAmount").text();

                    var E = "{CalculatedGratuity: '" + CalculatedGratuity + "', FinalGratuity: '" + FinalGratuity + "' , TabDescription: '" + TabDescription + "'}";
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: 'PensionerCalculation.aspx/Update_GratuityTabDetails',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                        }
                    });
                    //$("#btnGratuitySave").hide();
                    $("#btnGratuitySave").css('color', 'red');
                    //******************************************************************************************************
                    $(".loading-overlay").show();
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Get_FamilyPensionYesorNot',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var jsdata = JSON.parse(D.d); //alert(JSON.stringify(jsdata));
                            if (jsdata == 1) {
                                //$("#btnGratuitySave").hide();
                                $("#btnGratuitySave").css('color', 'red');
                                //CalculateFamilyPensionCalculations();

                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: 6,
                                    disabled: [7]
                                });
                            }
                            else {

                                $("#tabs").tabs({
                                    collapsible: true,
                                    selected: 6,
                                    disabled: [4, 7]
                                });
                            }
                        }
                    });

                    //******************************************************************************************************
                }
            });
        });
    });

                                    // End of Gratuity Tab Details
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
                                        //Remarks Tab Details

    $(document).ready(function () {
        $("#btnRemarks").click(function (event) {
            var TabNo = 6; var TabDescr = false;
            var C = "{EmpNo: '" + $("#hdnEmpNo").val() + "'}"; //alert(C);
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetTabNo',
                data: C,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var value = jQuery.parseJSON(D.d);
                    TabDescription = value; //alert(TabDescription);

                    if (TabDescription == 0) {
                        TabDescription = TabDescription + ',' + TabNo;
                    }
                    else {
                        var arrs = TabDescription.split(","); //alert(arrs.length);
                        if (arrs.length > 1) {
                            for (var k = 0; k < arrs.length; k++) {
                                var tab = arrs[k];
                                if (tab == TabNo) {
                                    //alert("match");
                                    TabDescr = true;
                                    break;
                                }
                                else {
                                    //alert("notmatch");
                                    TabDescr = false;
                                }
                            }
                            //alert(TabDescr);
                            if (TabDescr == true)
                                TabDescription = TabDescription;
                            else
                                TabDescription = TabDescription + ',' + TabNo;
                        }
                    }
                    var revisedBasic = 0;
                    var commryn = $("#ddlCommutation").val();
                    if (commryn == "Y")
                        revisedBasic = $("#lblReducedCommutationAmount").text();
                    else
                        revisedBasic = $("#lblPensionPerMonth").text();

                    var orginalBasic = $("#lblPensionPerMonth").text();
                    var reliefAmt = $("#txtRopaRelief").val();

                    var isAlive = $("#ddlIsAlive").val();


                    var ropaid = $("#ddlRopa").val();
                   // var dor = $("#txtDOR").val();
                    var G = "{ropaid: '" + ropaid + "'}";
                    $.ajax({
                        type: "POST",
                        url: "PensionerCalculation.aspx/CalculateRate",  //pageUrl + '/CalculateRate',
                        data: G,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            var t = JSON.parse(D.d);
                            $("#hdnRopaRate").val(t);

                            var daRate = $("#hdnRopaRate").val();

                            var E = "{CalculatedGratuity: '" + $("#hdnCalculatedGratuity").val() + "', orginalBasic: '" + orginalBasic + "', revisedBasic: '" + revisedBasic + "', reliefAmt: '" + reliefAmt + "',  isAlive: '" + isAlive + "', daRate:'" + daRate + "'}";
                            $(".loading-overlay").show();
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/Save_FinalGratuity',
                                data: E,
                                cache: false,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    $(".loading-overlay").hide();
                                    $("#btnRemarks").prop("disabled", true);
                                    //****************************************************************************************
                                    //alert("enter");
                                    var fileNo = $("#txtFileNo").val(); var pensionerName = $("#txtPensionerName").val(); var locationCode = $("#txtLocationCode").val();
                                    var totalSerYear = $("#txtTotalServiceYear").val(); var totalSerMon = $("#txtTotalServiceMonth").val(); var totalSerDay = $("#txtTotalServiceDay").val();
                                    var totalNonQualiSerYear = $("#txtTotalNonQualiServiceYear").val(); var totalNonQualiSerMon = $("#txtTotalNonQualiServiceMonth").val(); var totalNonQualiSerDay = $("#txtTotalNonQualiServiceDay").val();
                                    var totalQualiSerYear = $("#txtTotalQualiServiceYear").val(); var totalQualiSerMon = $("#txtTotalQualiServiceMonth").val(); var totalQualiSerDay = $("#txtTotalQualiServiceDay").val();
                                    var RopaID = $("#ddlRopa").val(); var remark = $("#txtRemarks").val();
                                    var isAlive = $("#ddlIsAlive").val();
                                    var DateofExp = $("#txtExpiryDate").val();

                                    var dob = $("#txtDOB").val();
                                    var doj = $("#txtDOJ").val();
                                    var dor = $("#txtDOR").val();

                                    var E = "{fileNo: '" + fileNo + "', totalSerYear: '" + totalSerYear + "', totalSerMon: '" + totalSerMon + "' , totalSerDay: '" + totalSerDay + "', "
                                             + " totalNonQualiSerYear: '" + totalNonQualiSerYear + "', totalNonQualiSerMon: '" + totalNonQualiSerMon + "', totalNonQualiSerDay: '" + totalNonQualiSerDay + "',"
                                             + " totalQualiSerYear: '" + totalQualiSerYear + "', totalQualiSerMon: '" + totalQualiSerMon + "', totalQualiSerDay: '" + totalQualiSerDay + "',"
                                             + " RopaID: '" + RopaID + "', remark: '" + remark + "',  isAlive: '" + isAlive + "', DateofExp: '" + DateofExp + "', dob: '" + dob + "', doj: '" + doj + "', dor: '" + dor + "',"
                                             + " pensionerName: '" + pensionerName + "', locationCode: '" + locationCode + "', TabDescription: '" + TabDescription + "'}"
                                    //alert(E);
                                    $(".loading-overlay").show();
                                    $.ajax({
                                        type: "POST",
                                        url: 'PensionerCalculation.aspx/Update_MST_PenPensionerTable',
                                        data: E,
                                        cache: false,
                                        contentType: "application/json; charset=utf-8",
                                        success: function (D) {
                                            //var t = JSON.parse(D.d);
                                            $(".loading-overlay").hide();
                                            var msg = "Pensioner Calculation is Inserted Successfully.";
                                            var E = "{Message: '" + msg + "'}";
                                            $.ajax({
                                                type: "POST",
                                                url: pageUrl + '/MessageBox',
                                                data: E,
                                                cache: false,
                                                contentType: "application/json; charset=utf-8",
                                                success: function (D) {
                                                    $('#showMessage').html(D.d);
                                                    $("#btnOK").focus();
                                                    var res = "finalcalculation";
                                                    $('#hdnDialogResult').val(res);

                                                    $("#btnRemarks").hide();

                                                    $("#tabs").tabs({
                                                        collapsible: true,
                                                        selected: 7,
                                                        disabled: []
                                                    });
                                                }
                                            });
                                        }
                                    });
                                    //****************************************************************************************
                                }
                            });
                        }
                    });
                }
            });
        });
    });

                                        //End of Remarks Tab Details
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $("#cmdSearchCancel").click(function (event) {
        alert("cancel");
    });

    $('#txtDOB').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
        
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOJ').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            CalculateTotalServicePeriod();
            CalculateTotalNonQualiServicePeriod();
            CalculateTotalQualiServicePeriod();
            CalculateSixMonthlyPeriod();
    }
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOR').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            CalculateTotalServicePeriod();
            CalculateTotalNonQualiServicePeriod();
            //CalculateTotalQualiServicePeriod();
            //CalculateSixMonthlyPeriod();
        }
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtEfffectFrom').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    }); 
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtNomineeDOB').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtExpiryDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtFamilyPenEffectFrom').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtFamilyPenEffectTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
    });

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    


});

function CalculateTotalServicePeriod() {

    if ($("#txtDOJ").val() != "" && $("#txtDOR").val() != "") {

        //var From_date = new Date($("#txtDOJ").datepicker('getDate')); //alert(From_date);
        //var To_date = new Date($("#txtDOR").datepicker('getDate')); //alert(To_date);
        //var diff_date = To_date - From_date; //alert(diff_date);

        //$("#hdnTotalServicePeriod").val(diff_date);
        //var years = Math.floor(diff_date / 31536000000);
        //var months = Math.floor((diff_date % 31536000000) / 2628000000);
        //var days = Math.floor(((diff_date % 31536000000) % 2628000000) / 86400000);
        
        ////diffc = To_date.getTime() - From_date.getTime();
        //////getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

        ////diffDays = Math.round(Math.abs(diffc / (1000 * 60 * 60 * 24)));
        //////this is the actual equation that calculates the number of days.

        ////alert(years + " year(s) " + months + " month(s) " + diffDays + " and day(s)");

        //$("#txtTotalServiceYear").val(years);
        //$("#txtTotalServiceMonth").val(months);
        //$("#txtTotalServiceDay").val(days);

        //$("#txtTotalNonQualiServiceYear").val(0);
        //$("#txtTotalNonQualiServiceMonth").val(0);
        //$("#txtTotalNonQualiServiceDay").val(0);

        //var lyear = years % 4; //alert(lyear);
        //if (lyear == 0) {
        //    ldays = years / 4;
        //    $("#txtTotalLeapYear").val(ldays);
        //}
        //else {
        //    var str = years / 4; //alert(str);
        //    ldays = Math.floor(str);
        //    $("#txtTotalLeapYear").val(ldays);
        //}

        //$("#txtTotalQualiServiceYear").val(years);
        //$("#txtTotalQualiServiceMonth").val(months);
        //$("#txtTotalQualiServiceDay").val(days);

        //CalculateSixMonthlyPeriod();
        var fromdate = $("#txtDOJ").val(); //alert(fromdate);
        var todate = $("#txtDOR").val(); //alert(todate);

        var E = "{fromdate: '" + fromdate + "', todate: '" + todate + "'}";// alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/Calculate_TotalServiceYear',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(t);
                var arr = t.split(',');
                var years = arr[0]; //alert(years);
                var months = arr[1]; //alert(months);
                var days = arr[2]; //alert(days);

                $("#txtTotalServiceYear").val(years);
                $("#txtTotalServiceMonth").val(months);
                $("#txtTotalServiceDay").val(days);

                $("#txtTotalNonQualiServiceYear").val(0);
                $("#txtTotalNonQualiServiceMonth").val(0);
                $("#txtTotalNonQualiServiceDay").val(0);

                $('#txtTotalQualiServiceYear').val(years);
                $('#txtTotalQualiServiceMonth').val(months);
                $('#txtTotalQualiServiceDay').val(days);

                CalculateTotalNonQualiServicePeriod();
            }
        });

    }
}


var TotalNonYear = 0; var TotalNonMonth = 0; var TotalNonDays = 0;
function CalculateTotalNonQualiServicePeriod() {
    var total = 0; 
    if ($("#txtFromDate").val() != "" && $("#txtToDate").val() != "") {

        var fromdate = $("#txtFromDate").val();
        var todate = $("#txtToDate").val();

        var E = "{fromdate: '" + fromdate + "', todate: '" + todate + "'}"; //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/Calculate_TotalServiceYear',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(t);
                var arr = t.split(',');
                var years = arr[0]; //alert(years);
                var months = arr[1]; //alert(months);
                var days = arr[2]; //alert(days);

                TotalNonYear = parseInt(TotalNonYear) + parseInt(years);
                TotalNonMonth = parseInt(TotalNonMonth) + parseInt(months);
                TotalNonDays = parseInt(TotalNonDays) + parseInt(days);

                $("#lblYears").show();
                $("#lblMonths").show();
                $("#lblDa").show();
                $('#lblYear').text(TotalNonYear);
                $('#lblMonth').text(TotalNonMonth);
                $('#lblDay').text(TotalNonDays);

                $("#txtTotalNonQualiServiceYear").val(TotalNonYear);
                $("#txtTotalNonQualiServiceMonth").val(TotalNonMonth);
                $("#txtTotalNonQualiServiceDay").val(TotalNonDays);

                CalculateTotalQualiServicePeriod();
                //CalculateSixMonthlyPeriod();
            }
        });

        //    var From_date = new Date($("#txtFromDate").datepicker('getDate'));
        //    var To_date = new Date($("#txtToDate").datepicker('getDate'));
        //    var diff_date = To_date - From_date;

        //    TotalDays.push(diff_date); //alert(TotalDays);
        //    for (var j = 0; j < TotalDays.length; j++) {
        //        total = total + TotalDays[j];
        //    }
        //    var t = $("#hdnTotalNonQualiServicePeriodOnSearch").val();
        //    if (t != "")
        //        total = parseInt(t) + parseInt(total);

        //    //tTOTAL = total + TT;
        //    //alert(total)
        //    $("#hdnTotalNonQualiServicePeriod").val(total);
        //    $("#TotalDays").val(total);
        //    var years = Math.floor(total / 31536000000);
        //    var months = Math.floor((total % 31536000000) / 2628000000);
        //    var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);
        //    //alert( years+" year(s) "+months+" month(s) "+days+" and day(s)");
        //    $("#txtTotalNonQualiServiceYear").val(years);
        //    $("#txtTotalNonQualiServiceMonth").val(months);
        //    $("#txtTotalNonQualiServiceDay").val(days);

        //    $("#lblYears").show();
        //    $("#lblMonths").show();
        //    $("#lblDa").show();
        //    $('#lblYear').text(years);
        //    $('#lblMonth').text(months);
        //    $('#lblDay').text(days);
        //}
        //else {
        //    alert("Please select dates");
        //    return false;
        //}
    }
    else {
        //alert("Enter");
        $("#txtTotalNonQualiServiceYear").val(0);
        $("#txtTotalNonQualiServiceMonth").val(0);
        $("#txtTotalNonQualiServiceDay").val(0);

        CalculateTotalQualiServicePeriod();
        //CalculateSixMonthlyPeriod();
    }
}


var totalQualiSerDay = 0; var totalQualiSerMonth = 0; var totalQualiSerYear = 0;
function CalculateTotalQualiServicePeriod() {

    var totalYear = $("#txtTotalServiceYear").val(); //alert(totalYear);
    var totalMonth = $("#txtTotalServiceMonth").val(); //alert(totalMonth);
    var totalDay = $("#txtTotalServiceDay").val(); //alert(totalDay);

    var totalNonYear = $("#txtTotalNonQualiServiceYear").val(); //alert(totalNonYear);
    var totalNonMonth = $("#txtTotalNonQualiServiceMonth").val(); //alert(totalNonMonth);
    var totalNonDay = $("#txtTotalNonQualiServiceDay").val(); //alert(totalNonDay);

    if (parseInt(totalDay) < parseInt(totalNonDay)) {

        totalQualiSerDay = parseInt((parseInt(totalDay) + 30)) - parseInt(totalNonDay);
    }
    else {
        totalQualiSerDay = parseInt(totalDay) - parseInt(totalNonDay); //alert(totalQualiSerDay);
    }


    if (parseInt(totalMonth) < parseInt(totalNonMonth))
    {
        totalQualiSerMonth = parseInt(parseInt(totalMonth) + 12) - parseInt(totalNonMonth);
        totalQualiSerYear = parseInt(parseInt(totalYear) - 1) - parseInt(totalNonYear);
    }
    else
    {
        if (parseInt(totalDay) < parseInt(totalNonDay)) {
            totalQualiSerMonth = parseInt(parseInt(totalMonth) - 1) - parseInt(totalNonMonth);

        }
        else {
            totalQualiSerMonth = parseInt(totalMonth) - parseInt(totalNonMonth);
        }
       
        totalQualiSerYear = parseInt(totalYear) - parseInt(totalNonYear);
    }


    if (parseInt(totalMonth) == 2 && parseInt(totalDay) > 29 || parseInt(totalDay) == 29)
    {
        totalQualiSerMonth = parseInt(parseInt(totalMonth)) + 1;
        totalQualiSerDay = 0;
    }
    else
    {
        if (parseInt(totalDay) > 29)
        {
            totalQualiSerMonth = parseInt(parseInt(totalMonth)) + 1;
            totalQualiSerDay = 0;
        }
    }
    //alert(totalQualiSerYear); alert(totalQualiSerMonth); alert(totalQualiSerDay);

    $('#txtTotalQualiServiceYear').val(totalQualiSerYear);
    $('#txtTotalQualiServiceMonth').val(totalQualiSerMonth);
    $('#txtTotalQualiServiceDay').val(totalQualiSerDay);

    CalculateSixMonthlyPeriod();
    CalculateEffectiveFrom();
    //var tsp = $("#hdnTotalServicePeriod").val(); //alert(tsp);
    //var tnqsp = $("#hdnTotalNonQualiServicePeriod").val(); //alert(tnqsp);

    //var total = tsp - tnqsp; //alert(total);
    //var years = Math.floor(total / 31536000000);
    //var months = Math.floor((total % 31536000000) / 2628000000);
    //var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);


    //var lyear = years % 4; //alert(lyear);
    //if (lyear == 0)
    //{
    //    ldays = years / 4;
    //    $("#txtTotalLeapYear").val(ldays);
    //}
    //else
    //{
    //    var str = years / 4; //alert(str);
    //    ldays = Math.floor(str);
    //    $("#txtTotalLeapYear").val(ldays);
    //}
    //var From_date = new Date($("#txtDOJ").datepicker('getDate'));
    //var To_date = new Date($("#txtDOR").datepicker('getDate')); //alert(To_date);
    //var yer = To_date.getFullYear(); //alert(yer);
    //var mon = To_date.getMonth() +1; //alert(mon);
    //var day = To_date.getDate(); //alert(day);

    //var newdate = new Date(parseInt(yer), parseInt(mon) - 1, day - ldays); //alert(newdate);
    //var diff_date = newdate - From_date; //alert(diff_date);

    //var nonqualiserv = diff_date - tnqsp;

    //var years = Math.floor(nonqualiserv / 31536000000);
    //var months = Math.floor((nonqualiserv % 31536000000) / 2628000000);
    //var days = Math.floor(((nonqualiserv % 31536000000) % 2628000000) / 86400000);
    //alert(years + " year(s) " + months + " month(s) " + days + " and day(s)");

    //$('#txtTotalQualiServiceYear').val(years);
    //$('#txtTotalQualiServiceMonth').val(months);
    //$('#txtTotalQualiServiceDay').val(days);
}

function CalculateSixMonthlyPeriod() {

    var RestMonth = 0; var TotalSixMonthPeriod = 0;
    var t1 = $("#txtTotalQualiServiceYear").val(); //alert(t1);
    var t2 = $("#txtTotalQualiServiceMonth").val(); //alert(t2);
    var t3 = $("#txtTotalQualiServiceDay").val(); //alert(t3);

    if ($("#txtTotalQualiServiceYear").val() != "" && $("#txtTotalQualiServiceMonth").val() != "" && $("#txtTotalQualiServiceDay").val() != "") {
        
        var yearvalue = ($("#txtTotalQualiServiceYear").val()) * 2; //alert(yearvalue);
        var MonthValue = $("#txtTotalQualiServiceMonth").val(); //alert(MonthValue);
        if (MonthValue > 6 || MonthValue == 6) {
            TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod) + parseInt(yearvalue) + 1; //alert(TotalSixMonthPeriod);
            RestMonth = parseInt(MonthValue) - 6; //alert(RestMonth);
            if (parseInt(RestMonth) > 3 || parseInt(RestMonth) == 3) {
                TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod) + 1;
            }
            else
                TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod);
        }
        else
        {
            if (MonthValue < 6)
            {
                RestMonth = parseInt(MonthValue); //alert(RestMonth);
                if (parseInt(RestMonth) > 3 || parseInt(RestMonth) == 3) {
                    TotalSixMonthPeriod = parseInt(TotalSixMonthPeriod) + parseInt(yearvalue) + 1;
                }
                else
                    TotalSixMonthPeriod = parseInt(yearvalue);
            }
        }

        if (MonthValue == 12)
            TotalSixMonthPeriod = TotalSixMonthPeriod + parseInt(yearvalue) + 2;

        //TotalSixMonthPeriod = parseInt(yearvalue) + parseInt(monthvalue); //alert(TotalSixMonthPeriod);
        $("#lblMonthlyPeriod").append(" ");
        $('#lblMonthlyPeriod').text(TotalSixMonthPeriod);

        Calculation();
    }
}

function Calculation() {
    var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
    var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
    var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
    var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

    var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
    $(".loading-overlay").show();
    $.ajax({
        type: "POST",
        url: pageUrl + '/GetRetiringGratuity',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $(".loading-overlay").hide();
            var da = JSON.parse(D.d);
            //alert(JSON.stringify(da));

            var agAmount = da[0].AG; 
            var gaAmount = da[0].GA; 

            $("#lblActualGratuity").text(agAmount);
            $("#lblGratuityAmount").text(gaAmount);

            //$("#hdnCalculatedGratuity").val(agAmount);
            $("#hdnCalculatedGratuity").val(gaAmount);

        }
    });

    var totalPBPlusGP = parseInt($("#lblGradePay").text()) + parseInt($("#lblPayintheBand").text()); //alert(totalPBPlusGP);
    var isAlive = $("#ddlIsAlive").val();
    var E = "{TotalServiceYear: '" + $("#txtTotalQualiServiceYear").val() + "', totalPBPlusGP: '" + totalPBPlusGP + "', SixMonthPeriod: '" + $("#lblMonthlyPeriod").text() + "', isAlive: '" + isAlive + "'}";
    $(".loading-overlay").show();
    $.ajax({
        type: "POST",
        url: pageUrl + '/Calculate_MonthlyPension',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $(".loading-overlay").hide();
            var da = JSON.parse(D.d); //alert(JSON.stringify(da));

            var pernsionPerMonth = da[0].pensionAmount; //alert(pernsionPerMonth);
            $("#lblPensionPerMonth").text(pernsionPerMonth);
        }
    });
}

function CalculateEffectiveFrom()
{
    if ($("#txtDOR").val() != "") {
        var dorDate = ""; var dorMonth = ""; var dorYear = "";

        var RetireDate = new Date($("#txtDOR").datepicker('getDate')); //alert(RetireDate);

        //var today = Date.today();
        //var past = Date.today().add(-1).months();
        //var future = RetireDate.setMonth(RetireDate.getMonth() + 1, 1); alert(future);

        var nextMonth = new Date(RetireDate.getFullYear(), parseInt(RetireDate.getMonth()) + 1, 1); //alert(nextMonth);  //RetireDate.getDate()

        var firstDay = new Date(nextMonth.getFullYear(), nextMonth.getMonth(), 1); //alert(firstDay);
        //var lastDay = new Date(nextMonth.getFullYear(), nextMonth.getMonth() + 1, 0); alert(lastDay);

        dorDate = firstDay.getDate() < 10 ? '0' + firstDay.getDate() : firstDay.getDate(); //alert(dorDate);

        if (firstDay.getMonth() == 9)
            dorMonth = parseInt(firstDay.getMonth()) + 1;
        else
            dorMonth = firstDay.getMonth() < 10 ? '0' + (parseInt(firstDay.getMonth()) + 1) : (parseInt(firstDay.getMonth()) + 1); //alert(dorMonth);

            dorYear = firstDay.getFullYear();

            var EffeDate = dorDate + '/' + dorMonth + '/' + dorYear; //alert(EffeDate);

            $("#txtEfffectFrom").val(EffeDate);
    }
}

function gratuity()
{
    var gratuityStatus = $("#hdnGratuityStatus").val(); //alert(gratuityStatus);
    var totalServieYears = $("#txtTotalServiceYear").val(); //alert(totalServieYears);
    var lastEmoluments = $("#lblTotalEmoluments").text(); //alert(lastEmoluments);
    var sixMonthlyPeriod = $("#lblMonthlyPeriod").text(); //alert(sixMonthlyPeriod);

    var E = "{gratuityStatus: '" + gratuityStatus + "', totalServieYears: '" + totalServieYears + "', lastEmoluments: '" + lastEmoluments + "', sixMonthlyPeriod: '" + sixMonthlyPeriod + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/GetRetiringGratuity',
        data: E,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var da = JSON.parse(D.d);
            //alert(JSON.stringify(da));

            var agAmount = da[0].AG;
            var gaAmount = da[0].GA;

            $("#lblActualGratuity").text(agAmount); //alert(agAmount);
            $("#lblGratuityAmount").text(gaAmount); //alert(gaAmount);

           // $("#hdnCalculatedGratuity").val(agAmount);
            $("#hdnCalculatedGratuity").val(gaAmount);
        }
    });
}

function CreateSelfNomineeGrid()
{
    var isAlive = $("#ddlIsAlive").val();


    if (isAlive == 0)
    {
        var nomineeName = $("#txtPensionerName").val();
        var nomineeDOB = $("#txtDOB").val();

        
        var C = "{NomineeName: '" + $("#txtNomineeName").val() + "',  NomineeDOB: '" + $("#txtNomineeDOB").val() + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: "PensionerCalculation.aspx/AddSelfNomineeGrid",
            data: C,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".loading-overlay").hide();
                $('#NomineeDetails').html(data.d);
            }
        });
    }
}


















