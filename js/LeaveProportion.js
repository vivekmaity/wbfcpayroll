


$(document).ready(function () {
    $("#btnSave").hide();
    $("#spnTotal").hide();
});

$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    var GridItemDetail = document.getElementById("GridView1");   //main Grid Initialization!
    GridItemDetail.deleteRow(1);
    var GridItemDetail = document.getElementById("grvLeaveEncashmentPaymentDetial");
    GridItemDetail.deleteRow(1);
});


$(document).ready(function () {
    $('#divGrid').scroll(function () {
        var y = $(this).scrollTop();
        if (y > 10) {
            $('#GridView1').removeClass('HeadertableHide');
            $('#GridView1').addClass('HeadertableView');
        }
        else {
            $('#GridView1').addClass('HeadertableHide');
            $('#GridView1').removeClass('HeadertableView');
        }
    });
});

//This is for numeric value checking
$(document).ready(function () {
    var flag3 = 0;
    $("#txtProportionAmt").keypress(function (evt) {
        if (flag3 == 0) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag3 = 1;
        }
        if (evt.keyCode == 13) {
            $("#txtProportionAmt").focus();
            return false;
        }
    });
});
$(document).ready(function () {
    var flag3 = 0;
    $("#txtItax").keypress(function (evt) {
        if (flag3 == 0) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag3 = 1;
        }
        if (evt.keyCode == 13) {
            $("#txtItax").focus();
            return false;
        }
    });
});

$(document).ready(function () {
    BindEmpLeaveEncashPaymentDetail();
});

function BindEmpLeaveEncashPaymentDetail() {
    $(".loading-overlay").show();
    var W = "{}";
    $.ajax({
        type: "POST",
        url: "LeaveProportion.aspx/GET_GridInitialization",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                for (var i = 0; i < data.d.length; i++) {
                    $("#grvLeaveEncashmentPaymentDetial").append("<tr><td>" +
                    "<input type='checkbox' id='chkAppExcel' value='" + data.d[i].EncashmentID + "'>" + "</td> <td>" +
                    "<input type='checkbox' id='chkAppPayment' value='" + data.d[i].EncashmentID + "'>" + "</td> <td>" +
                    data.d[i].EmpNo + "</td> <td>" +
                    data.d[i].EmpName + "</td> <td>" +
                    data.d[i].SectorName + "</td> <td>" +
                    data.d[i].LeaveCount + "</td> <td>" +
                    parseFloat(data.d[i].Amount).toFixed(2) + "</td><td>" +
                    data.d[i].ApprovalOn + "</td></tr>");
                }
            }
            $(".loading-overlay").hide();
            $(function () {
                $("[id*=grvLeaveEncashmentPaymentDetial] td").bind("click", function () {
                    var row = $(this).parent();
                    $("[id*=grvLeaveEncashmentPaymentDetial] tr").each(function () {
                        if ($(this)[0] != row[0]) {
                            $("td", this).removeClass("selected_row");
                        }
                    });
                    $("td", row).each(function () {
                        if (!$(this).hasClass("selected_row")) {
                            $(this).addClass("selected_row");
                        } else {
                            $(this).removeClass("selected_row");
                        }
                    });
                });
            });
        },

        error: function (result) {
            alert("Error Records Data");
            $(".loading-overlay").hide();
        }

    });
}

//This is for checkBox Selection  Payment 
$(document).ready(function () {
    $("[id*=chkAppPayment]").live("click", function () {

        $("input[id*='chkAppExcel']:checkbox").each(function (index) {
            if ($(this).is(":checked")) {
                $(this).attr("checked", false);
            }
        });

        if ($(this).is(":checked")) {
            $("input[id*='chkAppPayment']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
            $(this).attr("checked", true);
            var EmpNo = $(this).closest('tr').find('td:eq(2)').text();
            var Amount = $(this).closest('tr').find('td:eq(5)').text();

            var lre = /^\s*/;
            var rre = /\s*$/;
            EmpNo = EmpNo.replace(lre, "");
            EmpNo = EmpNo.replace(rre, "");

            var lre = /^\s*/;
            var rre = /\s*$/;
            Amount = Amount.replace(lre, "");
            Amount = Amount.replace(rre, "");

            $("#txtTotalAmount").val(parseInt(Amount));
            $("#txtProportionAmt").val(parseInt(Amount));
            $("#txtEmpNo").val(EmpNo);
            $("#txtEncashmentID").val($(this).val());


            DataChecking(EmpNo);
            CountRows();
            CountTotal();

            $("#overlay").show();
        }
    });
});

//This is for checkBox Selection  Excel 
$(document).ready(function () {
    $("[id*=chkAppPayment]").live("click", function () {

        $("input[id*='chkAppExcel']:checkbox").each(function (index) {
            if ($(this).is(":checked")) {
                $(this).attr("checked", false);
            }
        });

        if ($(this).is(":checked")) {
            $("input[id*='chkAppPayment']:checkbox").each(function (index) {
                if ($(this).is(":checked")) {
                    $(this).attr("checked", false);
                }
            });
            $(this).attr("checked", true);
            var EmpNo = $(this).closest('tr').find('td:eq(2)').text();
            var Amount = $(this).closest('tr').find('td:eq(5)').text();

            var lre = /^\s*/;
            var rre = /\s*$/;
            EmpNo = EmpNo.replace(lre, "");
            EmpNo = EmpNo.replace(rre, "");

            var lre = /^\s*/;
            var rre = /\s*$/;
            Amount = Amount.replace(lre, "");
            Amount = Amount.replace(rre, "");

            $("#txtTotalAmount").val(parseInt(Amount));
            $("#txtProportionAmt").val(parseInt(Amount));
            $("#txtEmpNo").val(EmpNo);
            $("#txtEncashmentID").val($(this).val());


            DataChecking(EmpNo);
            CountRows();
            CountTotal();

            $("#overlay").show();
        }
    });
});

//This is for Excel GENERATE button Click
$(document).ready(function () {
    $("#btnExcel").live("click", function () {
        var EmpNo = 0; var ArrayList = []; 
        $("input[id*='chkAppExcel']:checkbox").each(function (index) {
            if ($(this).is(':checked')) {
                EmpNo = $(this).closest('tr').find('td:eq(2)').text();

                var lre = /^\s*/;
                var rre = /\s*$/;
                EmpNo = EmpNo.replace(lre, "");
                EmpNo = EmpNo.replace(rre, "");

                ArrayList.push(EmpNo);
            }
        });
        var a = ArrayList.length; //alert(ArrayList);
        if (a == 0) {
            alert("Please Select at least One Employee !");
            return false;
        }
        else {
            var ReportName = "LeaveProportionExcel";
            window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "&EmployeeList=" + ArrayList );
            return false;
        }
    });
});

function DataChecking(EmpNo) {

    var E = "{EmpNo: '" + EmpNo + "'}";
    $.ajax({
        type: "POST",
        url: 'LeaveProportion.aspx/UpdateLeavePayment',
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            $('#tblPaymentAdd').html(D.d);
            HideDeleteButton();
        }
    });
}

function HideDeleteButton()
{
    var grid = document.getElementById("grvAddPaymentDetail");
    for (var row = 1; row < grid.rows.length; row++) {
        var GridFrom_Cell = grid.rows[row].cells[4];
        var GridVoucherNo = GridFrom_Cell.textContent.toString();

        var lre = /^\s*/;
        var rre = /\s*$/;
        GridVoucherNo = GridVoucherNo.replace(lre, "");
        GridVoucherNo = GridVoucherNo.replace(rre, "");

        if (GridVoucherNo != "") {
            for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                var del = $("[id*=delete]");
                $(del[row1]).hide();
            }
        }
    }
}

//This is for Popup Add button Click
$(document).ready(function () {
    $("#btnAdd").live("click", function () {

        var dateDate = ""; var monthMonth = ""; var yearYear = ""; var TotalDate = "";
        var date = ""; var month = ""; var year = "";

        var TotalAmt = $("#txtTotalAmount").val();
        var EncashmentID = $("#txtEncashmentID").val();
        var ProportionAmt = $("#txtProportionAmt").val();
       // var ItaxAmt = $("#txtItax").val();
        var PaymentDate = $("#txtPaymentDate").val();


        if (ProportionAmt == "") {
            $("#txtProportionAmt").focus();
            return false;
        }

        if (PaymentDate == "") {
            $("#txtPaymentDate").focus();
            return false;
        }

        if (parseInt(ProportionAmt) > parseInt(TotalAmt)) {
            alert("Enter Proportion Amount should be not greater than Total Gross Amount.");
            $("#txtProportionAmt").focus();
            return false;
        }

        var today = new Date();

        var a = PaymentDate.split('/');
        for (var i = 0; i < a.length; i++) {
            date = a[0]; month = a[1]; year = a[a.length - 1];
        }
        var newdate = new Date(year, month - 1, date);

        //if (newdate < today) {
        //    alert("Sorry ! Invalid Date. Please Select greater Date from Today.");
        //    $("#txtPaymentDate").focus();
        //    return false;
        //}
        //else {
            var E = "{ProportionAmt: '" + ProportionAmt + "',  PaymentDate: '" + PaymentDate + "', EncashmentID:'" + EncashmentID + "'}";
            $.ajax({
                type: "POST",
                url: 'LeaveProportion.aspx/Add_PaymentDetail',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $("#txtProportionAmt").val('');
                    $("#txtPaymentDate").val('');
                    $("#txtItax").val('');
                    $('#tblPaymentAdd').html(D.d);
                    HideDeleteButton();
                    CountRows();
                    CountTotal();

                    //$("#btnSave").show();
                    //$("#spnTotal").show();
                    //$("#lblTotal").show();

                    var grid = document.getElementById("grvAddPaymentDetail");
                    for (var row = 1; row < grid.rows.length; row++) {
                        var GridFrom_Cell = grid.rows[row].cells[1];
                        var GridFrom_Cell2 = grid.rows[row].cells[2];

                        var PropTotal = GridFrom_Cell.textContent.toString();
                        var ItaxTotal = GridFrom_Cell2.textContent.toString();

                        var lre = /^\s*/;
                        var rre = /\s*$/;
                        PropTotal = PropTotal.replace(lre, "");
                        PropTotal = PropTotal.replace(rre, "");

                        var lre = /^\s*/;
                        var rre = /\s*$/;
                        ItaxTotal = ItaxTotal.replace(lre, "");
                        ItaxTotal = ItaxTotal.replace(rre, "");
                    }
                    var totalbothAmt = parseInt(PropTotal) + parseInt(ItaxTotal);
                    $("#lblTotal").text(totalbothAmt);
                }
            });
       // }
    });
});

//This is for Popup Gridview delete button Click
$(document).ready(function () {
    $('#worktype a').live('click', function () {
        var result = confirm("Are you sure you want to delete this ?");
        if (result == true) {
            flag = $(this).attr('id');
            var SlNo = $(this).attr('itemid');
            var E = "{SlNo: '" + SlNo + "'}"; //alert(E);
            $.ajax({
                type: "POST",
                url: 'LeaveProportion.aspx/Delete_PaymentDetail',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $('#tblPaymentAdd').html(D.d);
                    HideDeleteButton();
                    CountRows();
                    CountTotal();
                }
            });
        }
    });
});

//This is for Popup Save button Click
$(document).ready(function () {
    $("#btnSave").live("click", function () {
        var TotalAmt = $("#txtTotalAmount").val();

        var grid = document.getElementById("grvAddPaymentDetail");
        for (var row = 1; row < grid.rows.length; row++) {
            var GridFrom_Cell = grid.rows[row].cells[1];

            var PropTotal = GridFrom_Cell.textContent.toString();

            var lre = /^\s*/;
            var rre = /\s*$/;
            PropTotal = PropTotal.replace(lre, "");
            PropTotal = PropTotal.replace(rre, "");

        }


        var totalbothAmt = parseInt(PropTotal);

        if (parseInt(totalbothAmt) == parseInt(TotalAmt)) {

            var EmpNo = $("#txtEmpNo").val();

            var E = "{EmpNo: '" + EmpNo + "', EncashmentID:'" + $("#txtEncashmentID").val() + "' }";
            $.ajax({
                type: "POST",
                url: 'LeaveProportion.aspx/Save_PaymentDetail',
                data: E,
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = JSON.parse(D.d);
                    if (t == "Inserted") {
                        alert("Proportion Details are Inserted Successfully.");

                        $.ajax({
                            type: "POST",
                            url: 'LeaveProportion.aspx/Blank_PaymentDetail',
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#tblPaymentAdd').html(D.d);
                                $("#spnTotal").hide();
                                $("#lblTotal").hide();
                                $("#overlay").hide();
                                window.location.href="LeaveProportion.aspx";
                           }
                        });
                        
                    }
                }
            });
        }
        else {
            alert("Sorry ! You can not Save this because your total Amount is not Equal.");
            return false;
        }
    });
});

//This is for Popup Close
$(document).ready(function () {
    $("#Close").click(function (e) {
        $("#overlay").hide();
    });
});

//This is to Set Date on Popup Payment Date
$(document).ready(function () {
    $('#txtPaymentDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'
        //onSelect: function (dates) {
        //    var aa = $('#txtPaymentDate').val();
        //    var a = aa.split('/');
        //    for (var i = 0; i < a.length; i++) {
        //        date = a[0]; month = a[1]; year = a[a.length - 1];
        //    }
        //    $('#txtPaymentDate').val("01" + "/" + month + "/" + year);

       // }
    });
});

//This is for Popup Reset button Click
$(document).ready(function () {
    $("#btnReset").click(function (e) {
        $("#txtProportionAmt").val('');
        $("#txtItax").val('');
        $("#txtPaymentDate").val('');
    });
});

//Start Search Keypress Coding Here
$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtEmpNoSearch').keyup(function () {
        $('#grvLeaveEncashmentPaymentDetial').find('tr:gt(0)').hide();
        var data = $('#txtEmpNoSearch').val();
        var len = data.length;
        if (len > 0) {
            $('#grvLeaveEncashmentPaymentDetial').find('tbody tr').each(function () {
                coldata = $(this).children().eq(1);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        }
        else {
            $('#grvLeaveEncashmentPaymentDetial').find('tr:gt(0)').show();
        }
    });
});

function CountRows() {
    $.ajax({
        type: "POST",
        url: 'LeaveProportion.aspx/CountAdd_PaymentDetail',
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = JSON.parse(D.d);
            if (t == "NoAvailable") {
                $('#btnSave').hide();
                $("#spnTotal").hide();
                $("#lblTotal").text('');
                $("#lblTotal").hide();
            }
            else {
                $('#btnSave').show();
                $("#spnTotal").show();
                $("#lblTotal").show();
            }
        }
    });
}

function CountTotal() {
    $.ajax({
        type: "POST",
        url: 'LeaveProportion.aspx/CountTotal_PaymentDetail',
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = JSON.parse(D.d);
            if (t != "NoAvailable") {
                $("#spnTotal").show();
                $("#lblTotal").show();
                $("#lblTotal").text(t);
            }
        }
    });
}