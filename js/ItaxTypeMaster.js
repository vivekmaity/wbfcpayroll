﻿$(function () {
    var Selector = {
        txtAccFinYear: $("#txtAccFinYear"),
        hdnMaxSaveID: $("#hdnMaxSaveID"),
        txtFinYear: $("#txtFinYear"),
        txtDescription: $("#txtDescription"),
        chckAmount: $("#chckAmount"),
        chckPercentage: $("#chckPercentage"),
        ddlGender: $("#ddlGender"),
        txtAmount: $("#txtAmount"),
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        tbl: $("#tbl")
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    var xml = $.parseXML(responce.d);
                    callback(xml);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    var xml = $.parseXML(responce.d);
                    callback(xml);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    }
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeydown();
            objClientSide.Event.onBlur();
        },
        Event: {
            onLoad: function () {
                objClientSide.Method.LoadFinYear();
                objClientSide.Method.GetItexSavingType();               
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    var ID = $(this).closest("tr").find(".clsID").text();
                    var FinYear = $(this).closest("tr").find(".clsFinYear").text();
                    var GenderID = $(this).closest("tr").find(".clsGenderID").text();
                    var IsAmount = $(this).closest("tr").find(".clsIsAmount").text();
                    var Description = $(this).closest("tr").find(".clsDescription").text();
                    var Amount = $(this).closest("tr").find(".clsAmount").text();
                    Selector.hdnMaxSaveID.val(ID);
                    Selector.txtAmount.val(Amount);
                    Selector.txtDescription.val(Description);
                    console.log(IsAmount);
                    $("input[name=value]").filter("[value=" + IsAmount + "]").prop('checked', true);
                    Selector.ddlGender.val(GenderID);
                    Selector.btnSave.val('Update');
                });
                $(document).on("click", ".clsDelete", function () {
                    var ID = $(this).closest("tr").find(".clsID").text();
                    if (Selector.hdnMaxSaveID.val() == ID) {
                        alert("This Record  is in Edit Mode. You can not  Delete this record now..");
                        return false;
                    }

                    if (confirm("Are you sure want to Delete this Record ??")) {

                        objClientSide.Method.Delete(ID);
                    }
                });
            },
            onKeydown: function () {
                Selector.txtAmount.keypress(function (e) {
                    if (!(e.keyCode > 45 && e.keyCode < 58) || e.keyCode == 47 || (e.keyCode == 46 && $(this).val().indexOf('.') != -1)) {
                        return false;
                    }
                });
                Selector.txtDescription.keyup(function () {
                    objClientSide.Method.Search();
                });
            },
            onBlur: function () {
                Selector.txtAccFinYear.blur(function () {
                    objClientSide.Method.LoadFinYear();
                    objClientSide.Method.GetItexSavingType();
                });
                Selector.txtDescription.blur(function () {
                    objClientSide.Method.Search();
                });
            }
        },
        Method: {
            LoadFinYear: function () {
                Selector.txtFinYear.val(Selector.txtAccFinYear.val().trim());
            },            
            GetItexSavingType: function () {
                var Data = {
                     FinYear: Selector.txtAccFinYear.val().trim()                   
                };
                var url = "ItaxTypeMaster.aspx/Get_Data";
                //  console.log(Data);
                objServerSide.PostMehtod(url, Data, function (xml) {
                    var xmlTable = $(xml).find('Table');
                 //   console.log(jsdata);
                    objClientSide.Method.AppendTable(xmlTable);
                });
            },
            AppendTable: function (xmlTable) {
              //  console.log(jsdata);
                Selector.tbl.find('tbody').empty();
                $.each(xmlTable, function (index, value) {
                    index = index + 1;
                    //      ItaxTypeID,TypeDesc,MaxAmount,FinYear,MaxType,case MaxType when 'V' then 'Rupee' else 'Percentage' end as FullMaxType,Sex,FullSex                   
                    Selector.tbl.find('tbody').append("<tr>"
                        + "<td class='clsID' style='display:none;'>" + $(value).find('ItaxTypeID').text() + "</td>"
                        + "<td class=''>" + index + "</td>"
                        + "<td class='clsFinYear' style='display:none;'>" + $(value).find('FinYear').text() + "</td>"
                        + "<td class='clsGenderID' style='display:none;'>" + $(value).find('Sex').text() + "</td>"
                        + "<td class='clsGender'>" +$(value).find('FullSex').text()+ "</td>"
                        + "<td class='clsDescription'>" +$(value).find('TypeDesc').text() + "</td>"
                        + "<td class='clsIsAmount' style='display:none;'>" + $(value).find('MaxType').text() + "</td>"
                        + "<td class='clsAmount'>" +$(value).find('MaxAmount').text() + "</td>"
                        + "<td >" + $(value).find('FullMaxType').text() + "</td>"
                        + "<td class=' clsUpdate text-center text-info' style='cursor:pointer;color:#007697;' > Edit</td>"
                        + "<td class=' clsDelete text-center text-default' style='cursor:pointer;color:#007697;'> Delete</td>"
                        + "</tr>");
                });
            },
            Save: function () {
                var rt = objClientSide.Method.Validation();
                if (rt==false) {
                    return false;
                }
             //   ItaxTypeID, TypeDesc, MaxType, MaxAmount, FinYear, Sex
                var Data = {
                    ItaxTypeID: Selector.hdnMaxSaveID.val(), TypeDesc: Selector.txtDescription.val(),
                    MaxType: $("input[name='value']:checked").val(), MaxAmount: Selector.txtAmount.val().trim(),
                    FinYear: Selector.txtAccFinYear.val().trim(), Sex: Selector.ddlGender.val()
                };
                console.log(Data);
                var URL = "ItaxTypeMaster.aspx/Save";
                objServerSide.PostMehtod(URL, Data, function (xml) {
                    if (xml == 'error') {
                        alert('Error In Server side.. Operation Fail');
                        return false;
                    }
                    var xmlTable = $(xml).find('Table');
                    var Massage = '';
                    var returnCode = '';
                    $.each(xmlTable, function (index, value) {
                        Massage = $(value).find("Messege").text();
                        returnCode = $(value).find("ErrorCode").text();
                        alert(Massage);
                        if (returnCode == 1) {
                            window.location.reload();
                        }
                    });
                });
            },
            Delete: function (ID) {
                var Data = {ID: ID};
                var URL = "ItaxTypeMaster.aspx/Delete";
                objServerSide.PostMehtod(URL, Data, function (xml) {
                    if (xml == 'error') {
                        alert('Error In Server side.. Operation Fail');
                        return false;
                    }
                    var xmlTable = $(xml).find('Table');
                    var Massage = '';
                    var returnCode = '';
                    $.each(xmlTable, function (index, value) {
                        Massage = $(value).find("Massage").text();
                        returnCode = $(value).find("returnCode").text();
                        alert(Massage);
                        if (returnCode == 0) {
                            window.location.reload();
                        }
                    });
                });
            },
            Refresh: function () {
                Selector.txtAmount.val("");
                Selector.hdnMaxSaveID.val("");
                Selector.txtDescription.val("");
                Selector.btnSave.val('Save');
                Selector.ddlGender.val('');
                objClientSide.Method.LoadFinYear();
                objClientSide.Method.GetItexSavingType();
            },
            Validation: function () {
                if (Selector.txtFinYear.val() == '')
                {
                    alert('Financial year missing');
                    Selector.txtFinYear.focus();
                    return false;
                }
                if (Selector.ddlGender.val() == '') {
                    alert('Select a Gender');
                    Selector.ddlGender.focus();
                    return false;
                }
                if (Selector.txtDescription.val().trim() == '') {
                    alert('Write Type Description');
                    Selector.txtDescription.focus();
                    return false;
                }
                if (Selector.txtAmount.val().trim() == '') {
                    alert('Enter Value');
                    Selector.txtAmount.focus();
                    return false;
                }
                return true;
            },
            Search: function () {
                var input = Selector.txtDescription.val().trim().toUpperCase();
                var tr = Selector.tbl.find('tbody > tr');
                $.each(tr, function (index, value) {
                    var description = $(this).find('.clsDescription').text();
                    if (description.toUpperCase().indexOf(input) > -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        }
    };

    objClientSide.Initialization();
});