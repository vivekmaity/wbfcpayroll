﻿
var ArrayList = []; var tabarray = []; var TotalDays = []; var TotalDaysOnSearch = []; var TotalDaysOnAdd = []; var ArrayNominee = [];
var totalOnADD = 0; var available = false;
$(document).ready(function () {
    $("#lblYears").hide();
    $("#lblMonths").hide();
    $("#lblDa").hide();
    $("#imgChecked").hide();

    $("#btnAddNominee").prop("disabled", true);
    $("#btnNonQualiReason").prop("disabled", true);

    var defaultMode = $("#hdnDefaultMode").val(); //alert(defaultMode);
    if (defaultMode == "Edit")
    {
        var flags = $("#hdnCheckFlag").val();
        if (flags == "Y") {
            $("#imgChecked").show();
            $("#btnChecked").hide();
        }
        else {
            $("#imgChecked").hide();
            $("#btnChecked").show();
        }
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $("#btnAddNominee").click(function (e) {
        //$("#ddlNomineeType").change(function () {
            
            var NomiTypeID = $("#ddlNomineeType").val();
            var NomiType = $('#ddlNomineeType').find('option:selected').text();

            var NomiRelation = $('#ddlNomineeRelation').find('option:selected').text();

            ArrayNominee.push(NomiTypeID); //alert(ArrayNominee);
            var a = ArrayNominee.length; 
            if (a > 1) {
                for (j = 0; j < ArrayNominee.length; j++) {
                    var nom = ArrayNominee[j];
                    if (nom == 1) 
                        available = true;
                    else
                        available = false;
                }
            }
                //alert(ArrayNominee); alert(available);
            if (!available) {
                var C = "{ NomineeID: '" + $("#ddlNomineeType").val() + "', NomineeType: '" + NomiType + "' ,  NomineeName: '" + $("#txtNomineeName").val() + "', NomineeRelationID: '" + $("#ddlNomineeRelation").val() + "', NomineeRelation: '" + NomiRelation + "' , NomineeDOB: '" + $("#txtNomineeDOB").val() + "', NomineeAddress: '" + $("#txtNomineeAddress").val() + "', NomineeState: '" + $("#ddlNomineeState").val() + "', NomineeDistrict: '" + $("#ddlNomineeDistrict").val() + "', NomineeCity: '" + $("#txtNomineeCity").val() + "', NomineePin: '" + $("#txtNomineePin").val() + "', NomineePhone: '" + $("#txtNomineePhone").val() + "', NomineeIDMark: '" + $("#txtNomineeIDMark").val() + "', FamilyPension: '" + $("#ddlFamilyPension").val() + "', GratuityPerct: '" + $("#txtGratuityPerc").val() + "'}";
                //alert(C);
                $.ajax({
                    type: "POST",
                    url: "Pensioner.aspx/AddNomineeDetails",
                    data: C,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $('#NomineeDetails').html(data.d);
                        if(NomiTypeID=1)
                            $("#ddlFamilyPension").val("Y");
                        else
                            $("#ddlFamilyPension").val("N");
                    }
                });
            }
            else {
                alert("You can not select this more than one time.");
            }
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $("#txtGratuityPerc").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            // (event.keyCode == 65 && event.ctrlKey == true) ||
            // Allow: home, end, left, right
    (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                return false;

            }
        }
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        //CalculateTotalServicePeriod();
        //CalculateTotalQualiService();
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $("#ddlSec").change(function () {
            var s = $('#ddlSec').val();
            if (s != 0) {
                if ($("#ddlSec").val() != "Please select") {
                    var E = "{SecID: " + $('#ddlSec').val() + "}";
                    //alert(E);
                    var options = {};
                    options.url = "Pensioner.aspx/GetCenter";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listCenter) {
                        var t = jQuery.parseJSON(listCenter.d);
                        var a = t.length;
                        $("#ddlLocation").empty();
                        if (a >= 0) {
                            $("#ddlLocation").append("<option value='0'>(Select Center)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlLocation").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        }

                        $("#ddlLocation").prop("disabled", false);
                    };
                    options.error = function () { alert("Error in retrieving Center!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlLocation").empty();
                    $("#ddlLocation").prop("disabled", true);
                }
            }
            else {
                $("#ddlLocation").empty();
                $("#ddlLocation").append("<option value=''>(Select Center)</option>")
            }
        });

    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "Pensioner.aspx/BindState",
            data: {},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var jsdata = JSON.parse(data.d);
                $("#ddlState").append("<option value=''>(Select State)</option>")
                $("#ddlNomineeState").append("<option value=''>(Select State)</option>")
                $.each(jsdata, function (key, value) {
                    $("#ddlState").append($("<option></option>").val(value.StateId).html(value.State));
                    $("#ddlNomineeState").append($("<option></option>").val(value.StateId).html(value.State));
                });
            },
            error: function (data) {
                //alert("error found");
            }
        });
    });
    $(document).ready(function () {
        //$("#ddlDistrict").prop("disabled", true);
        $("#ddlState").change(function () {

            if ($("#ddlState").val() != "Please select") {

                var options = {};
                options.url = "Pensioner.aspx/BindDistricts";
                options.type = "POST";
                options.data = "{StateID: " + $('#ddlState').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listdistrict) {
                    var t = jQuery.parseJSON(listdistrict.d);
                    var a = t.length;
                    $("#ddlDistrict").empty();
                    if (a > 0) {
                        $("#ddlDistrict").append("<option value=''>(Select District)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlDistrict").append($("<option></option>").val(value.DistID).html(value.District));
                        });
                    }
                    $("#ddlDistrict").prop("disabled", false);
                };
                options.error = function () { alert("Error retrieving Districts!"); };
                $.ajax(options);
            }
            else {
                $("#ddlDistrict").empty();
                $("#ddlDistrict").prop("disabled", true);
            }
        });
    });
    $(document).ready(function () {
        //$("#ddlDistrict").prop("disabled", true);
        $("#ddlNomineeState").change(function () {

            if ($("#ddlNomineeState").val() != "Please select") {

                var options = {};
                options.url = "Pensioner.aspx/BindDistricts";
                options.type = "POST";
                options.data = "{StateID: " + $('#ddlNomineeState').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listdistrict) {
                    var t = jQuery.parseJSON(listdistrict.d);
                    var a = t.length;
                    $("#ddlNomineeDistrict").empty();
                    if (a > 0) {
                        $("#ddlNomineeDistrict").append("<option value=''>(Select District)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlNomineeDistrict").append($("<option></option>").val(value.DistID).html(value.District));
                        });
                    }
                    $("#ddlNomineeDistrict").prop("disabled", false);
                };
                options.error = function () { alert("Error retrieving Districts!"); };
                $.ajax(options);
            }
            else {
                $("#ddlNomineeDistrict").empty();
                $("#ddlNomineeDistrict").prop("disabled", true);
            }
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "Pensioner.aspx/BindStateforPermanentAddress",
            data: {},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var jsdata = JSON.parse(data.d);
                $("#ddlState_P").append("<option value=''>(Select State)</option>")
                $.each(jsdata, function (key, value) {
                    $("#ddlState_P").append($("<option></option>").val(value.StateId).html(value.State));
                });
            },
            error: function (data) {
                // alert("error found");
            }
        });
    });
    $(document).ready(function () {
        $("#ddlState_P").change(function () {

            if ($("#ddlState_P").val() != "Please select") {

                var options = {};
                options.url = "Pensioner.aspx/BindDistrictsforPermanentAddress";
                options.type = "POST";
                options.data = "{StateID: " + $('#ddlState_P').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listdistrict) {
                    var t = jQuery.parseJSON(listdistrict.d);
                    var a = t.length;
                    $("#ddlDistrict_P").empty();
                    if (a > 0) {
                        $("#ddlDistrict_P").append("<option value=''>(Select District)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlDistrict_P").append($("<option></option>").val(value.DistID).html(value.District));
                        });
                    }
                    $("#ddlDistrict_P").prop("disabled", false);
                };
                options.error = function () { alert("Error retrieving Districts!"); };
                $.ajax(options);
            }
            else {
                $("#ddlDistrict_P").empty();
                $("#ddlDistrict_P").prop("disabled", true);
            }
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    //This is for 'Pay Scale' Binding on the Base of Pay Scale Type
    $(document).ready(function () {
        $("#ddlPayScaleType").change(function () {

            if ($("#ddlPayScaleType").val() != "Please select") {
                var options = {};
                options.url = "Pensioner.aspx/BindPayScale";
                options.type = "POST";
                options.data = "{EmpType: '" + $('#ddlPayScaleType').val() + "'}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listPayScale) {
                    var t = jQuery.parseJSON(listPayScale.d);
                    var a = t.length;
                    $("#ddlPayScale").empty();
                    if (a > 0) {
                        $("#ddlPayScale").append("<option value='0'>(Select Pay Scale)</option>")
                        $.each(t, function (key, value) {
                            $("#ddlPayScale").append($("<option></option>").val(value.PayScaleID).html(value.PayScale));
                        });
                    } else {
                        $("#ddlPayScale").append("<option value='0'>(Select Pay Scale)</option>")
                    }
                    $("#ddlPayScale").prop("disabled", false);
                };
                options.error = function () { alert("Error retrieving Pay Scale!"); };
                $.ajax(options);
            }
            else {
                $("#ddlPayScale").empty();
                $("#ddlPayScale").prop("disabled", true);
            }
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $("#txtDOB").change(function (event) {
        var DOB = $('#txtDOB').val(); var DOR = ""; var DORYear = ""; var date = ""; var month = ""; var year = "";
        dorDate = ""; var dorMonth = ""; var dorYear = "";
        //alert(DOB);
        var a = DOB.split('/');
        for (var i = 0; i < a.length; i++) {
            date = a[0]; month = a[1]; year = a[a.length - 1];
        }
        DORYear = (parseInt(year) + parseInt(60)); //alert(DORYear);
        if (date == 1) {
            //alert(date); alert(month); alert(year);
            //var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var newdate = new Date(year, month - 1, date); //alert(newdate);
            var lastDay = new Date(newdate.getFullYear(), newdate.getMonth(), 0); //alert(lastDay);
            dorDate = lastDay.getDate() < 10 ? '0' + lastDay.getDate() : lastDay.getDate(); //alert(dorDate);
            dorMonth = lastDay.getMonth() < 10 ? '0' + (parseInt(lastDay.getMonth()) + 1) : (parseInt(lastDay.getMonth()) + 1); //alert(dorMonth);
            dorYear = lastDay.getFullYear();
            DOR = dorDate + '/' + dorMonth + '/' + DORYear; //alert(DOR);
            $("#txtDOR").val(DOR);
        } else {
            var newdate = new Date(year, month - 1, date); //alert(newdate);
            var lastDay = new Date(newdate.getFullYear(), newdate.getMonth() + 1, 0); //alert(lastDay);
            dorDate = lastDay.getDate() < 10 ? '0' + lastDay.getDate() : lastDay.getDate(); //alert(dorDate);

            var months = (parseInt(lastDay.getMonth()) + 1);
            dorMonth = months < 10 ? '0' + months : months; //alert(dorMonth);
            dorYear = lastDay.getFullYear();
            DOR = dorDate + '/' + dorMonth + '/' + DORYear; //alert(DOR);
            $("#txtDOR").val(DOR);
        }
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $("#ddlBank").change(function () {
            if ($("#ddlBank").val() != "" && $("#ddlBank").val() != null) {
                $("#txtIFSC").val('');
                $("#txtMICR").val('');
                //$("#txtBankCode").val('');
                $("#txtBankCode").prop("disabled", true);
                if ($("#ddlBank").val() != "Please select") {
                    var options = {};
                    options.url = "Pensioner.aspx/BindBranch";
                    options.type = "POST";
                    options.data = "{BankID: " + $('#ddlBank').val() + "}";
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listbranch) {
                        var t = jQuery.parseJSON(listbranch.d);
                        var a = t.length;
                        $("#ddlBranch").empty();
                        $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                        if (a > 0) {
                            $.each(t, function (key, value) {
                                $("#ddlBranch").append($("<option></option>").val(value.BranchID).html(value.Branch));
                            });
                        }
                        $("#ddlBranch").prop("disabled", false);
                    };
                    options.error = function () { alert("Error retrieving BankName!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlBranch").empty();
                    $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                    $("#ddlBranch").prop("disabled", true);
                    $("#txtIFSC").val('');
                    $("#txtMICR").val('');
                }
            }
            else {
                $("#ddlBranch").empty();
                $("#ddlBranch").append("<option value=''>(Select Branch)</option>")
                $("#ddlBranch").prop("disabled", true);
                $("#txtIFSC").val('');
                $("#txtMICR").val('');
                $("#txtBankCode").prop("disabled", true);
            }
        });
    });
    $(document).ready(function () {
        $("#ddlBranch").change(function () {
            var C = "{BranchID: " + $('#ddlBranch').val() + "}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetIFSCandMICR',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = jQuery.parseJSON(D.d);
                    //alert(JSON.stringify(t));
                    var ifsc = t[0].IFSC;
                    var micr = t[0].MICR;
                    $("#txtIFSC").val(ifsc);
                    $("#txtMICR").val(micr);
                    $("#txtBankCode").prop("disabled", false);
                },
                error: function (response) {
                    alert('');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $("#btnNonQualiReason").click(function (e) {
        var fromDate = $("#txtNonQualiFromDate").val();
        var toDate = $("#txtNonQualiToDate").val();
        var fileNo = $("#txtFileNo").val();
        var fileDate = $("#txtFileDate").val();

        if (fileNo != "" && fileDate !== "") {
            if (fromDate != "") {
                if (toDate != "") {
                    $("#OverlayNonQualiReason").show();
                    e.preventDefault();
                }
                else {
                    alert("Please Enter ToDate.");
                    $("#txtNonQualiToDate").focus();
                }
            }
            else {
                alert("Please Enter FromDate.");
                $("#txtNonQualiFromDate").focus();
            }
        }
        else {
            alert("Please Enter File Number and File Date first.");
            $("#tabs").tabs({
                collapsible: true,
                selected: 0
            });
            $("#txtFileNo").focus();
        }
    });
    $("#btnNonQualiReasonClose").click(function (e) {
        $("#OverlayNonQualiReason").hide();
    });
    $("#cmdNonQualiReason").click(function (e) {
        var Reason = $("#txtNonQualiReasons").val();
        if (Reason != "")
            window.pensionMaster.AddNonQualiReason();
        else
            $("#txtNonQualiReasons").focus();
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $("[id*=chkSelect]").live("click", function () {
            if ($(this).is(":checked")) {
                $("input[id*='chkSelect']:checkbox").each(function (index) {
                    if ($(this).is(":checked")) {
                        $(this).attr("checked", false);
                    }
                });
                $(this).attr("checked", true);

                ReasonID = $(this).closest('tr').find('td:eq(1)').text();
                Reason = $(this).closest('tr').find('td:eq(2)').text();

                var lre = /^\s*/;
                var rre = /\s*$/;
                ReasonID = ReasonID.replace(lre, "");
                ReasonID = ReasonID.replace(rre, "");

                var lre = /^\s*/;
                var rre = /\s*$/;
                Reason = Reason.replace(lre, "");
                Reason = Reason.replace(rre, "");
                //alert(ReasonID); alert(Reason);

                $("#txtNonQualiReason").val(Reason);
                $("#OverlayNonQualiReason").hide();

                var fromDate = $("#txtNonQualiFromDate").val();
                var toDate = $("#txtNonQualiToDate").val();
                var C = "{ReasonID:'" + ReasonID + "', Reason: '" + Reason + "',fromDate: '" + fromDate + "',toDate: '" + toDate + "'}";
                $.ajax({
                    type: "POST",
                    url: "Pensioner.aspx/BindNonQualiwithDate",
                    data: C,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $('#ReasonbetDate').html(data.d);
                        $('#txtNonQualiFromDate').val('');
                        $('#txtNonQualiToDate').val('');
                        $('#txtNonQualiReason').val('');
                    }
                });

                CalculateTotalNonQualiServicePeriod(); 
                CalculateTotalQualiServicePeriod();
           }
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).on("click", "[id*=lnkDelete]", function (e) {
        e.preventDefault();
        var j = confirm("Are you sure you want to delete this Non-Quali Service Reason ?");
        if (j == true) {

            var B = {};
            B.pensionForm = window.pensionMaster.pensionForm;
            //alert(JSON.stringify(B));
            ReasonID = $(this).closest('tr').find('td:eq(2)').text();
            var lre = /^\s*/;
            var rre = /\s*$/;
            ReasonID = ReasonID.replace(lre, "");
            ReasonID = ReasonID.replace(rre, "");

            var E = "{\'pensionForm\':\'" + JSON.stringify(B) + "\',\'ReasonID\':\'" + ReasonID + "\'}";
            //alert(E);
            $.ajax({
                type: "POST",
                url: pageUrl + '/DeleteNonQualiReason',
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    //$("#btnNonQualiReason").trigger("click");
                    $('#ReasonDetails').html(D.d);
                }
            });
        }
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "Pensioner.aspx/LoadNonQualiServiceReason",
            data: {},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $('#ReasonDetails').html(data.d);
            }
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    
    $("#btnChecked").click(function (e) {

        var E = "{EmpNo: '" + $("#txtEmpCodeSearch").val() + "'}";
        $.ajax({
            type: "POST",
            url: "Pensioner.aspx/Update_Mst_PenPensionerTable",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                if (t == "Updated") {
                    $("#imgChecked").show();
                    $("#btnChecked").hide();
                    $("#hdnFinalCheckedFlag").val("YES");
                }
                else {
                    $("#imgChecked").hide();
                    $("#btnChecked").show();
                    $("#hdnFinalCheckedFlag").val("NO");
                }
            }
        });
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $('#txtNonQualiFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (selected) {
            $("#txtNonQualiToDate").datepicker("option", "minDate", selected);
    }

    });
    $("#btnNonQualiFromDate").click(function () {
        $('#txtNonQualiFromDate').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtNonQualiToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (selected) {
            $("#txtNonQualiFromDate").datepicker("option", "maxDate", selected);
    }
    });
    $("#btnNonQualiToDate").click(function () {
        $('#txtNonQualiToDate').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtSancDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnSancDate").click(function () {
        $('#txtSancDate').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtPaymentDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnPaymentDate").click(function () {
        $('#txtPaymentDate').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOJ').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            CalculateTotalServicePeriod();
    }
    });
    $("#btnDOJ").click(function () {
        $('#txtDOJ').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOR').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        onSelect: function (dates) {
            CalculateTotalServicePeriod();
        }
    });
    $("#btnDOR").click(function () {
        $('#txtDOR').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtExpiryDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnExpiryDate").click(function () {
        $('#txtExpiryDate').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtMediEffFrom').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnMediEffFrom").click(function () {
        $('#txtMediEffFrom').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtMediEffTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnMediEffTo").click(function () {
        $('#txtMediEffTo').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtDOB').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnDOB").click(function () {
        $('#txtDOB').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtFileDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnFileDate").click(function () {
        $('#txtFileDate').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $('#txtNomineeDOB').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow'

    });
    $("#btnNomineeDOB").click(function () {
        $('#txtNomineeDOB').datepicker("show");
        return false;
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $("#cmdSearchCancel").click(function () {
        window.location.href = "PensionerSelection.aspx";
    });
    $("#cmdPersonalCancel").click(function () {
        window.location.href = "PensionerSelection.aspx";
    });
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    DisableFields();
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    window.pensionMaster = new PENSION.Pensioner();
    window.pensionMaster.init();

});

function CalculateTotalQualiServicePeriod()
{
    var tsp = $("#hdnTotalServicePeriod").val(); //alert(tsp);
    var tnqsp = $("#hdnTotalNonQualiServicePeriod").val(); //alert(tnqsp);

    var total = tsp - tnqsp; //alert(total);
   var years = Math.floor(total / 31536000000);
   var months = Math.floor((total % 31536000000) / 2628000000);
   var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);

   $('#txtTotalQualiServiceYear').val(years);
   $('#txtTotalQualiServiceMonth').val(months);
   $('#txtTotalQualiServiceDay').val(days);
}
function CalculateTotalNonQualiServicePeriod() {
    var total = 0;
    if ($("#txtNonQualiFromDate").val() != "" && $("#txtNonQualiToDate").val() != "") {

        var From_date = new Date($("#txtNonQualiFromDate").datepicker('getDate'));
        var To_date = new Date($("#txtNonQualiToDate").datepicker('getDate'));
        var diff_date = To_date - From_date; 
        
        TotalDays.push(diff_date); //alert(TotalDays);
        for (var j = 0; j < TotalDays.length; j++)
        {
            total = total + TotalDays[j];
        }
        var t = $("#hdnTotalNonQualiServicePeriodOnSearch").val();
        if(t!="")
            total = parseInt(t) + parseInt(total); 

        //tTOTAL = total + TT;
        //alert(total)
        $("#hdnTotalNonQualiServicePeriod").val(total);
        $("#TotalDays").val(total);
        var years = Math.floor(total / 31536000000);
        var months = Math.floor((total % 31536000000) / 2628000000);
        var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);
        //alert( years+" year(s) "+months+" month(s) "+days+" and day(s)");
        $("#txtTotalNonQualiServiceYear").val(years);
        $("#txtTotalNonQualiServiceMonth").val(months);
        $("#txtTotalNonQualiServiceDay").val(days);

        $("#lblYears").show();
        $("#lblMonths").show();
        $("#lblDa").show();
        $('#lblYear').text(years);
        $('#lblMonth').text(months);
        $('#lblDay').text(days);
    }
    else {
        alert("Please select dates");
        return false;
    }
}
function CalculateTotalServicePeriod() {

    if ($("#txtDOJ").val() != "" && $("#txtDOR").val() != "") {

        var From_date = new Date($("#txtDOJ").datepicker('getDate')); 
        var To_date = new Date($("#txtDOR").datepicker('getDate'));
        var diff_date = To_date - From_date; 

        $("#hdnTotalServicePeriod").val(diff_date);
        var years = Math.floor(diff_date / 31536000000); 
        var months = Math.floor((diff_date % 31536000000) / 2628000000);
        var days = Math.floor(((diff_date % 31536000000) % 2628000000) / 86400000);
        //alert( years+" year(s) "+months+" month(s) "+days+" and day(s)");
        $("#txtTotalServiceYear").val(years);
        $("#txtTotalServiceMonth").val(months);
        $("#txtTotalServiceDay").val(days);
    }
    //else {
    //    alert("Please select dates");
    //    return false;
    //}
}
function dateDiff() {
    var frmdate = null, toDate = null, totalDays = "";
    if ($.trim($("#txtDOJ").val())) {
        frmdate = new Date($("#txtDOJ").datepicker('getDate'));
    }
    if ($.trim($("#txtDOR").val())) {
        toDate = new Date($("#txtDOR").datepicker('getDate'));
    }

    if ((frmdate != null && toDate != null))
        totalDays = Math.floor((toDate - frmdate)); /// (1000 * 60 * 60 * 24)) + 1;
    //alert(frmdate+'   '+ toDate+'  '+ totalDays);
    return totalDays;

}
function CalculateNonQualiwithDateOnSearch(ReasonFrom, ReasonTo) {
    var total = 0;
    if (ReasonFrom != "" && ReasonTo != "") {
        var From_date = new Date(ReasonFrom);
        var To_date = new Date(ReasonTo);
        var diff_date = To_date - From_date;

        TotalDaysOnSearch.push(diff_date); //alert(TotalDays);
        for (var j = 0; j < TotalDaysOnSearch.length; j++) {
            total = total + TotalDaysOnSearch[j];
        }
        $("#hdnTotalNonQualiServicePeriodOnSearch").val(total);
        var years = Math.floor(total / 31536000000);
        var months = Math.floor((total % 31536000000) / 2628000000);
        var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);
        //alert( years+" year(s) "+months+" month(s) "+days+" and day(s)");
        $("#txtTotalNonQualiServiceYear").val(years);
        $("#txtTotalNonQualiServiceMonth").val(months);
        $("#txtTotalNonQualiServiceDay").val(days);

        $("#lblYears").show();
        $("#lblMonths").show();
        $("#lblDa").show();
        $('#lblYear').text(years);
        $('#lblMonth').text(months);
        $('#lblDay').text(days);
    }
}
function DisableFields()
{
    $("#txtEmpNo").prop("disabled", true);
    $("#ddlSec").prop("disabled", true);
    $("#ddlLocation").prop("disabled", true);
    $("#txtPensionerName").prop("disabled", true);
    $("#ddlReligion").prop("disabled", true);
    $("#ddlQualification").prop("disabled", true);
    $("#ddlMarital").prop("disabled", true);
    $("#ddlCaste").prop("disabled", true);
    $("#ddlGender").prop("disabled", true);
    $("#ddlGroup").prop("disabled", true);

    $("#ddlDesig").prop("disabled", true);
    $("#ddlPhysical").prop("disabled", true);
    $("#ddlCategory").prop("disabled", true);
    $("#txtDOB").prop("disabled", true);
    $("#btnDOB").prop("disabled", true);
    $("#txtDOJ").prop("disabled", true);
    $("#btnDOJ").prop("disabled", true);
    $("#txtDOR").prop("disabled", true);
    $("#btnDOR").prop("disabled", true);
    $("#txtPFCode").prop("disabled", true);
    $("#ddlPayScaleType").prop("disabled", true);
    $("#ddlPayScale").prop("disabled", true);
    $("#txtPAN").prop("disabled", true);
    $("#ddlMediclaim").prop("disabled", true);
    $("#txtMediEffFrom").prop("disabled", true);
    $("#btnMediEffFrom").prop("disabled", true);
    $("#txtMediEffTo").prop("disabled", true);
    $("#txtStatus").prop("disabled", true);

}
function CalculateTotalQualiService()
{
    var tsp = $("#hdnTotalServicePeriod").val(); //alert(tsp);

    var tnqsp = $("#hdnTotalNonQualiServicePeriodOnSearch").val(); //alert(tnqsp);

    var total = tsp - tnqsp; //alert(total);
    var years = Math.floor(total / 31536000000);
    var months = Math.floor((total % 31536000000) / 2628000000);
    var days = Math.floor(((total % 31536000000) % 2628000000) / 86400000);

    $('#txtTotalQualiServiceYear').val(years);
    $('#txtTotalQualiServiceMonth').val(months);
    $('#txtTotalQualiServiceDay').val(days);
}
function removeDuplicates(inputArray) {
    var i;
    var len = inputArray.length;
    var outputArray = [];
    var temp = {};

    for (i = 0; i < len; i++) {
        temp[inputArray[i]] = 0;
    }
    for (i in temp) {
        outputArray.push(i);
    }
    return outputArray;
}

PENSION.MasterInfo = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#cmdPersonalSave").click(A.AddClicked);
        $("#cmdNomineeSave").click(A.AddClicked);
        $("#buttonUpload").click(A.UploadFilePhoto);
        $("#buttonUploadSign").click(A.UploadFileSign);
    };
    this.getValidator = function () {
        $("#form1").validate();

        $("#txtFileNo").rules("add", {
            required: function (element) {
            if ($("#txtFileNo").val() == "")
            {
                ArrayList.push("tabs-1");
                return true;
            }
            else
            {
                return false;
            }
        }, messages: { required: "Please Enter File No." }
        });

        $("#txtFileDate").rules("add", {
            required: function (element) {
                if ($("#txtFileDate").val() == "") { ArrayList.push("tabs-1"); return true; } else { return false; }
            }, messages: { required: "Please Enter File Date." }
        });

        $("#ddlBank").rules("add", {
            required: function (element) {
                if ($("#ddlBank").val() == "") { ArrayList.push("tabs-2"); return true; } else { return false; }
            }, messages: { required: "Please Select Bank." }
        });

        $("#ddlBranch").rules("add", {
            required: function (element) {
                if ($("#ddlBranch").val() == "") { ArrayList.push("tabs-2"); return true; } else { return false; }
            }, messages: { required: "Please Select Branch." }
        });

        $("#txtBankCode").rules("add", {
            required: function (element) {
                if ($("#txtBankCode").val() == "") { ArrayList.push("tabs-2"); return true; } else { return false; }
            }, messages: { required: "Please Enter Bank Account No." }
        });


        return $("#form1").validate();
    }
    
    this.AddClicked = function () {

        var Condition = $(this).attr("value");
        //This is for Save Pension Master
      if (Condition == "Create") {
          var tt = tabarray.length; //alert(tt);
          if (A.validator.form()) {
              if (tt == 0) {
                  A.setFormValues();
              }
              else {
                  function removeDuplicates(inputArray) {
                      var i;
                      var len = inputArray.length;
                      var outputArray = [];
                      var temp = {};

                      for (i = 0; i < len; i++) {
                          temp[inputArray[i]] = 0;
                      }
                      for (i in temp) {
                          outputArray.push(i);
                      }
                      return outputArray;
                  }

                  ouputArray = removeDuplicates(tabarray); //alert(ouputArray);
                  var a = ouputArray.length; //alert(a);

                  tabarray = ouputArray; //alert(tabarray);
                  if (a > 0) {
                      var TabName = "";
                      for (var j = 0; j < a ; j++) {
                          TabName = tabarray[0];
                      }
                      //alert(TabName);
                      if (TabName == "tabs-1") {
                          window.pensionMaster.tabClick(TabName, 0, tabarray);
                          A.validator.form();
                      }
                      else if (TabName == "tabs-2") {
                          window.pensionMaster.tabClick(TabName, 1, tabarray);
                          A.validator.form();
                      }
                      else if (TabName == "tabs-3") {
                          window.pensionMaster.tabClick(TabName, 2, tabarray);
                          A.validator.form();
                      }
                  }
              }
          }

          else {
              function removeDuplicates(inputArray) {
                  var i;
                  var len = inputArray.length;
                  var outputArray = [];
                  var temp = {};

                  for (i = 0; i < len; i++) {
                      temp[inputArray[i]] = 0;
                  }
                  for (i in temp) {
                      outputArray.push(i);
                  }
                  return outputArray;
              }

              ouputArray = removeDuplicates(ArrayList); //alert(ouputArray);
              var a = ouputArray.length; //alert(a);

              tabarray = ouputArray; //alert(tabarray);
              if (a > 0) {
                  var TabName = "";
                  TabName = tabarray[0];
                  //alert(TabName);
              }

              if (TabName == "tabs-1") {
                  window.pensionMaster.tabClick(TabName, 0, tabarray);
                  A.validator.form();
              }
              else if (TabName == "tabs-2") {
                  window.pensionMaster.tabClick(TabName, 1, tabarray);
                  A.validator.form();
              }
              else if (TabName == "tabs-3") {
                  window.pensionMaster.tabClick(TabName, 2, tabarray);
                  A.validator.form();
              }
          }
      }
          //This is for Update Pension Master
      else {
          var tt = tabarray.length; //alert(tt);
          if (A.validator.form()) {
              if (tt == 0) {
                  A.setFormUpdateValues();
              }
              else {
                  ouputArray = removeDuplicates(tabarray); //alert(ouputArray);
                  var a = ouputArray.length; //alert(a);

                  tabarray = ouputArray; //alert(tabarray);
                  if (a > 0) {
                      var TabName = "";
                      TabName = tabarray[0];
                      //alert(TabName);
                      
                      if (TabName == "tabs-1") {
                          window.pensionMaster.tabClick(TabName, 0, tabarray);
                          A.validator.form();
                      }
                      else if (TabName == "tabs-2") {
                          window.pensionMaster.tabClick(TabName, 1, tabarray);
                          A.validator.form();
                      }
                      else if (TabName == "tabs-3") {
                          window.pensionMaster.tabClick(TabName, 2, tabarray);
                          A.validator.form();
                      }
                  }
              }
          }

          else {
              ouputArray = removeDuplicates(ArrayList); //alert(ouputArray);
              var a = ouputArray.length; //alert(a);

              tabarray = ouputArray; //alert(tabarray);
              if (a > 0) {
                  var TabName = "";
                  TabName = tabarray[0];
                  //alert(TabName);
              }

              if (TabName == "tabs-1") {
                  window.pensionMaster.tabClick(TabName, 0, tabarray);
                  A.validator.form();
              }
              else if (TabName == "tabs-2") {
                  window.pensionMaster.tabClick(TabName, 1, tabarray);
                  A.validator.form();
              }
              else if (TabName == "tabs-3") {
                  window.pensionMaster.tabClick(TabName, 2, tabarray);
                  A.validator.form();
              }
          }
      }
    };
    this.setFormValues = function () {
        var B = window.pensionMaster.pensionForm;
        //alert(JSON.stringify(B));
        //File Information
        B.objPensionMaster.FileNo = $("#txtFileNo").val(); 
        B.objPensionMaster.FileDate = $("#txtFileDate").val();
        //Bank Details
        B.objPensionMaster.BankID = $("#ddlBank").val();
        B.objPensionMaster.BranchID = $("#ddlBranch").val();
        B.objPensionMaster.BankAccount = $("#txtBankCode").val();
        //Personal Details
        B.objPensionMaster.EmpNo = $("#txtEmpNo").val();
        B.objPensionMaster.SectorID = $("#ddlSec").val();
        B.objPensionMaster.LocationID = $("#ddlLocation").val();
        B.objPensionMaster.PensionerName = $("#txtPensionerName").val();
        B.objPensionMaster.ReligionID = $("#ddlReligion").val();
        B.objPensionMaster.QualificationID = $("#ddlQualification").val();
        B.objPensionMaster.MaritalStatusID = $("#ddlMarital").val();
        B.objPensionMaster.CastID = $("#ddlCaste").val();
        B.objPensionMaster.DOB = $("#txtDOB").val();
        B.objPensionMaster.Gender = $("#ddlGender").val();
        B.objPensionMaster.RetirementTypeID = $("#ddlRetirementType").val();
        B.objPensionMaster.GroupID = $("#ddlGroup").val();
        //Address Information
        B.objPensionMaster.PresentAddress = $("#txtAddress").val();
        B.objPensionMaster.PresentState = $("#ddlState").val();
        B.objPensionMaster.PresentDistrict = $("#ddlDistrict").val();
        B.objPensionMaster.PresentCity = $("#txtCity").val();
        B.objPensionMaster.PresentPin = $("#txtPin").val();
        B.objPensionMaster.PresentPhone = $("#txtPhone").val();
        B.objPensionMaster.PresentMobile = $("#txtMobile").val();
        B.objPensionMaster.PresentEmail = $("#txtEmail").val();

        B.objPensionMaster.PermanentAddress = $("#txtAddress_P").val();
        B.objPensionMaster.PermanentState = $("#txtAddress_P").val();
        B.objPensionMaster.PermanentDistrict = $("#txtAddress_P").val();
        B.objPensionMaster.PermanentCity = $("#txtCity_P").val();
        B.objPensionMaster.PermanentPin = $("#txtPin_P").val();
        B.objPensionMaster.PermanentPhone = $("#txtPhone_P").val();
        //Official Information
        B.objPensionMaster.DOJ = $("#txtDOJ").val();
        B.objPensionMaster.DOR = $("#txtDOR").val();
        B.objPensionMaster.DesignationID = $("#ddlDesig").val();
        B.objPensionMaster.PhysicalHandicappedID = $("#ddlPhysical").val();
        B.objPensionMaster.CategoryID = $("#ddlCategory").val();
        B.objPensionMaster.IsAliveID = $("#ddlIsAlive").val();
        B.objPensionMaster.ExpiryDate = $("#txtExpiryDate").val();
        B.objPensionMaster.RetirementAge = $("#txtRetirementAge").val();
        B.objPensionMaster.TotalServiceYear = $("#txtTotalServiceYear").val();
        B.objPensionMaster.TotalServiceMonth = $("#txtTotalServiceMonth").val();
        B.objPensionMaster.TotalServiceDay = $("#txtTotalServiceDay").val();
        B.objPensionMaster.TotalNonQualiServiceYear = $("#txtTotalNonQualiServiceYear").val();
        B.objPensionMaster.TotalNonQualiServiceMonth = $("#txtTotalNonQualiServiceMonth").val();
        B.objPensionMaster.TotalNonQualiServiceDay = $("#txtTotalNonQualiServiceDay").val();
        B.objPensionMaster.TotalQualiServiceYear = $("#txtTotalQualiServiceYear").val();
        B.objPensionMaster.TotalQualiServiceMonth = $("#txtTotalQualiServiceMonth").val();
        B.objPensionMaster.TotalQualiServiceDay = $("#txtTotalQualiServiceDay").val();
        B.objPensionMaster.IsPensionerinGovtService = $("#ddlIsPensionerGovtService").val();
        B.objPensionMaster.PFCode = $("#txtPFCode").val();
        B.objPensionMaster.PayScaleType = $("#ddlPayScaleType").val();
        B.objPensionMaster.PayScaleID = $("#ddlPayScale").val();
        B.objPensionMaster.PAN = $("#txtPAN").val();
        B.objPensionMaster.Mediclaim = $("#ddlMediclaim").val();
        B.objPensionMaster.MediEffFrom = $("#txtMediEffFrom").val();
        B.objPensionMaster.MediEffTo = $("#txtMediEffTo").val();
        B.objPensionMaster.RopaID = $("#ddlRopa").val();
        //Sanction Details
        B.objPensionMaster.PenSanOrderNo = $("#txtPenSanOrderNo").val();
        B.objPensionMaster.PenSanDate = $("#txtSancDate").val();
        B.objPensionMaster.PenSanBy = $("#txtPenSanBy").val();
        B.objPensionMaster.PaymentDate = $("#txtPaymentDate").val();
        B.objPensionMaster.PensionerType = $("#PensionerType").val();
        B.objPensionMaster.PPONo = $("#txtPPONo").val();
        B.objPensionMaster.Accepted = $("#ddlAccepted").val();
        B.objPensionMaster.NormalAdvance = $("#txtNormalAdvance").val();
        B.objPensionMaster.Remarks = $("#txtRemarks").val();
        //Phota and Signature
        B.objPensionMaster.Photo = $("#lblPhoto").val();
        B.objPensionMaster.Signature = $("#lblSign").val();
       
        window.pensionMaster.SubmitForm();

    };
    this.setFormUpdateValues = function () {
        var B = window.pensionMaster.pensionForm;
        //alert(JSON.stringify(B));
        //File Information
        B.objPensionMaster.PensionerID = $("#hdnEmpID").val();
        B.objPensionMaster.FileNo = $("#txtFileNo").val();
        B.objPensionMaster.FileDate = $("#txtFileDate").val();
        //Bank Details
        B.objPensionMaster.BankID = $("#ddlBank").val();
        B.objPensionMaster.BranchID = $("#ddlBranch").val();
        B.objPensionMaster.BankAccount = $("#txtBankCode").val();
        //Personal Details
        B.objPensionMaster.EmpNo = $("#txtEmpNo").val();
        B.objPensionMaster.SectorID = $("#ddlSec").val();
        B.objPensionMaster.LocationID = $("#ddlLocation").val();
        B.objPensionMaster.PensionerName = $("#txtPensionerName").val();
        B.objPensionMaster.ReligionID = $("#ddlReligion").val();
        B.objPensionMaster.QualificationID = $("#ddlQualification").val();
        B.objPensionMaster.MaritalStatusID = $("#ddlMarital").val();
        B.objPensionMaster.CastID = $("#ddlCaste").val();
        B.objPensionMaster.DOB = $("#txtDOB").val();
        B.objPensionMaster.Gender = $("#ddlGender").val();
        B.objPensionMaster.RetirementTypeID = $("#ddlRetirementType").val();
        B.objPensionMaster.GroupID = $("#ddlGroup").val();
        //Address Information
        B.objPensionMaster.PresentAddress = $("#txtAddress").val();
        B.objPensionMaster.PresentState = $("#ddlState").val();
        B.objPensionMaster.PresentDistrict = $("#ddlDistrict").val();
        B.objPensionMaster.PresentCity = $("#txtCity").val();
        B.objPensionMaster.PresentPin = $("#txtPin").val();
        B.objPensionMaster.PresentPhone = $("#txtPhone").val();
        B.objPensionMaster.PresentMobile = $("#txtMobile").val();
        B.objPensionMaster.PresentEmail = $("#txtEmail").val();

        B.objPensionMaster.PermanentAddress = $("#txtAddress_P").val();
        B.objPensionMaster.PermanentState = $("#ddlState_P").val();
        B.objPensionMaster.PermanentDistrict = $("#ddlDistrict_P").val();
        B.objPensionMaster.PermanentCity = $("#txtCity_P").val();
        B.objPensionMaster.PermanentPin = $("#txtPin_P").val();
        B.objPensionMaster.PermanentPhone = $("#txtPhone_P").val();
        //Official Information
        B.objPensionMaster.DOJ = $("#txtDOJ").val();
        B.objPensionMaster.DOR = $("#txtDOR").val();
        B.objPensionMaster.DesignationID = $("#ddlDesig").val();
        B.objPensionMaster.PhysicalHandicappedID = $("#ddlPhysical").val();
        B.objPensionMaster.CategoryID = $("#ddlCategory").val();
        B.objPensionMaster.IsAliveID = $("#ddlIsAlive").val();
        B.objPensionMaster.ExpiryDate = $("#txtExpiryDate").val();
        B.objPensionMaster.RetirementAge = $("#txtRetirementAge").val();
        B.objPensionMaster.TotalServiceYear = $("#txtTotalServiceYear").val();
        B.objPensionMaster.TotalServiceMonth = $("#txtTotalServiceMonth").val();
        B.objPensionMaster.TotalServiceDay = $("#txtTotalServiceDay").val();
        B.objPensionMaster.TotalNonQualiServiceYear = $("#txtTotalNonQualiServiceYear").val();
        B.objPensionMaster.TotalNonQualiServiceMonth = $("#txtTotalNonQualiServiceMonth").val();
        B.objPensionMaster.TotalNonQualiServiceDay = $("#txtTotalNonQualiServiceDay").val();
        B.objPensionMaster.TotalQualiServiceYear = $("#txtTotalQualiServiceYear").val();
        B.objPensionMaster.TotalQualiServiceMonth = $("#txtTotalQualiServiceMonth").val();
        B.objPensionMaster.TotalQualiServiceDay = $("#txtTotalQualiServiceDay").val();
        B.objPensionMaster.IsPensionerinGovtService = $("#ddlIsPensionerGovtService").val();
        B.objPensionMaster.PFCode = $("#txtPFCode").val();
        B.objPensionMaster.PayScaleType = $("#ddlPayScaleType").val();
        B.objPensionMaster.PayScaleID = $("#ddlPayScale").val();
        B.objPensionMaster.PAN = $("#txtPAN").val();
        B.objPensionMaster.Mediclaim = $("#ddlMediclaim").val();
        B.objPensionMaster.MediEffFrom = $("#txtMediEffFrom").val();
        B.objPensionMaster.MediEffTo = $("#txtMediEffTo").val();
        B.objPensionMaster.RopaID = $("#ddlRopa").val();
        //Sanction Details
        B.objPensionMaster.PenSanOrderNo = $("#txtPenSanOrderNo").val();
        B.objPensionMaster.PenSanDate = $("#txtSancDate").val();
        B.objPensionMaster.PenSanBy = $("#txtPenSanBy").val();
        B.objPensionMaster.PaymentDate = $("#txtPaymentDate").val();
        B.objPensionMaster.PensionerType = $("#PensionerType").val();
        B.objPensionMaster.PPONo = $("#txtPPONo").val();
        B.objPensionMaster.Accepted = $("#ddlAccepted").val();
        B.objPensionMaster.NormalAdvance = $("#txtNormalAdvance").val();
        B.objPensionMaster.Remarks = $("#txtRemarks").val();
        //Phota and Signature
        B.objPensionMaster.Photo = $("#lblPhoto").val();
        B.objPensionMaster.Signature = $("#lblSign").val();

        if (A.validator.form()) {

            window.pensionMaster.UpdateForm();
        }

    };
    this.UploadFilePhoto = function () {
        var B = window.pensionMaster.pensionForm;
        $(".loading-overlay").show();
        $("#lblPhoto").val('');
        $("#imgPhoto").empty().html("");
        $("#canPhoto").empty().html('');
        if ($("#fileToUpload").val() == "") {
            $(".loading-overlay").hide();
            alert('Select file before uploading');
            return false;
        }
        //if (!CheckForImageFile("#fileToUpload")) {
        //    alert('select');
        //    return false;
        //}
        //if (!GetFileSize("fileToUpload", 'P')) {
        //    //alert('select');
        //    return false;
        //}

        var fileUpload = $("#fileToUpload").get(0);
        var files = fileUpload.files;

        var formData = new window.FormData();
        var filename = files[0].name;

        var C = "{\'name\':\'" + filename + "\'}";

        $("#buttonUpload").hide();
        $.ajaxFileUpload(
            {
                url: 'AjaxFileUpload.ashx?picsFolder=UploadPhoto',
                //url: 'AjaxFileUpload.ashx',
                async: true,
                secureuri: false,
                fileElementId: 'fileToUpload',
                dataType: 'json',
                data: C,
                success: function (data, status) {
                    $(".loading-overlay").hide();
                    $('#buttonUpload').show();
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            var html = "<img src='images/cross.png' alt='cross' />";
                            $("#imgPhoto").empty().html(html);
                            $("#lblPhoto").val('');
                            $("#canPhoto").empty().html('');
                            alert(data.error);
                        } else {
                            var B = window.pensionMaster.pensionForm;
                            $("#lblPhoto").val(data.msg);
                            var html = "<img src='images/check.png' alt='tick' />";
                            $("#imgPhoto").empty().html(html);
                            var cnhtml = "<img src='" + data.msg + "' width='100px' height='116px' alt='canimg' />"
                            $("#canPhoto").empty().html(cnhtml);

                        }
                    } else {
                        var html = "<img src='images/cross.png' alt='cross' />";
                        $("#imgPhoto").empty().html(html);
                        $("#lblPhoto").val('');
                        $("#canPhoto").empty().html('');
                    }
                },
                error: function (data, status, e) {
                    alert(e);
                    //$('#buttonUpload').attr("disabled", false);
                    $('#buttonUpload').show();
                    var html = "<img src='images/cross.png' alt='cross' />";
                    $("#imgPhoto").empty().html(html);
                    $("#lblPhoto").val('');
                    $("#canPhoto").empty().html('');
                    $("#loading").hide();
                }
            }
           )
        //$('#buttonUpload').show();
        return false;
    };
    this.UploadFileSign = function () {
        var B = window.pensionMaster.pensionForm;
        $(".loading-overlay").show();
        $("#lblSign").val('');
        $("#imgSign").empty().html("");
        $("#canSign").empty().html('');
        if ($("#fileToUploadSign").val() == "") {
            $(".loading-overlay").hide();
            alert('Select file before uploading');
            return false;
        }
        //if (!CheckForImageFile("#fileToUploadSign")) {
        //    //alert('select');
        //    return false;
        //}
        //if (!GetFileSize("fileToUploadSign", 'S')) {
        //    //alert('select');
        //    return false;
        //}
        $("#lblSign").val('');
        var fileUpload = $("#fileToUploadSign").get(0);
        var files = fileUpload.files;

        var formData = new window.FormData();
        var filename = files[0].name;

        var C = "{\'name\':\'" + filename + "\'}";

        $('#buttonUploadSign').hide();
        $.ajaxFileUpload
        (
            {
                url: 'AjaxFileUpload.ashx?picsFolder=UploadSign',
                async: true,
                secureuri: false,
                fileElementId: 'fileToUploadSign',
                dataType: 'json',
                data: C,
                success: function (data, status) {
                    $(".loading-overlay").hide();
                    $("#loadingSign").hide();
                    //$('#buttonUploadSign').attr("disabled", false);
                    $('#buttonUploadSign').show();
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            var html = "<img src='images/cross.png' alt='cross' />";
                            $("#imgSign").empty().html(html);
                            $("#lblSign").val('');
                            $("#canSign").empty().html('');
                            alert(data.error);
                        } else {
                            var B = window.pensionMaster.pensionForm;
                            $("#lblSign").val(data.msg);
                            var html = "<img src='images/check.png' alt='tick' />";
                            $("#imgSign").empty().html(html);
                            var cnhtml = "<img src='" + data.msg + "' width='150px' height='27px' alt='canimg' />"
                            $("#canSign").empty().html(cnhtml);
                        }
                    } else {
                        var html = "<img src='images/cross.png' alt='cross' />";
                        $("#imgSign").empty().html(html);
                        $("#lblSign").val('');
                        $("#canSign").empty().html('');
                    }
                },
                error: function (data, status, e) {
                    alert(e);
                    $('#buttonUploadSign').show();
                    //$('#buttonUploadSign').attr("disabled", false);
                    var html = "<img src='images/cross.png' alt='cross' />";
                    $("#imgSign").empty().html(html);
                    $("#lblSign").val('');
                    $("#canSign").empty().html('');
                    $("#loadingSign").hide();
                }
            }
           )
        return false;
    };
};

PENSION.Pensioner = function () {
    var A = this;
    this.pensionForm;
    this.init = function () {
        if ($("#penJSON").val() != "undefined" && $("#penJSON").val() != null) {
            this.pensionForm = JSON.parse($("#penJSON").val());
            //alert(JSON.stringify(this.pensionForm));
        }
        if (this.pensionForm != null) {
            this.masterInfo = new PENSION.MasterInfo();
            this.masterInfo.init();

            var B = window.pensionMaster.pensionForm;
            //alert(JSON.stringify(B));

            NonQuResDate = B.objNonqualiReasonWithDate; //alert(JSON.stringify(NonQuResDate));
            if (NonQuResDate !=null)
            {
                var ArrayOnSearch = [];
                if (NonQuResDate.length > 0) {
                    for (var j = 0; j < NonQuResDate.length; j++)
                    {
                        var ReasonID = NonQuResDate[j].NonQualiReasonID; 
                        var ReasonFrom = NonQuResDate[j].NonQualiReasonFrom;
                        var ReasonTo = NonQuResDate[j].NonQualiReasonTo;
                        var Reason = NonQuResDate[j].NonQualiReason;

                        CalculateNonQualiwithDateOnSearch(ReasonFrom, ReasonTo);

                        var C = "{ ReasonID: '" + ReasonID + "',Reason: '" + Reason + "',fromDate: '" + ReasonFrom + "',toDate: '" + ReasonTo + "'}";
                        $.ajax({
                            type: "POST",
                            url: 'Pensioner.aspx/BindNonQualiwithDate',
                            data: C,
                            cache: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#ReasonbetDate').html(D.d);
                            }
                        });

                        
                    }
                    
                }
            }
            //This is for Nominee Details Binding....

            Nominee = B.objNomineeDetails; //alert(JSON.stringify(Nominee));
            if (Nominee != null) {
                var ArrayOnNomineeSearch = [];
                if (Nominee.length > 0) {
                    for (var j = 0; j < Nominee.length; j++) {
                        var NomineeTypeID = Nominee[j].NomineeTypeID;
                        var NomineeType = Nominee[j].NomineeType;
                        var NomineeName = Nominee[j].NomineeName;
                        var NomineeRelation = Nominee[j].NomineeRelation;
                        var NomineeDOB = Nominee[j].NomineeDOB;
                        var FamilyPension = Nominee[j].FamilyPension;
                        var GratuityPerct = Nominee[j].GratuityPerct;

                        var C = "{NomineeTypeID: '" + NomineeTypeID + "', NomineeType: '" + NomineeType + "',NomineeName: '" + NomineeName + "',NomineeRelation: '" + NomineeRelation + "',NomineeDOB: '" + NomineeDOB + "',FamilyPension: '" + FamilyPension + "',GratuityPerct: '" + GratuityPerct + "'}";
                        //alert(C);
                        $.ajax({
                            type: "POST",
                            url: 'Pensioner.aspx/LoadNomineeOnSearch',
                            data: C,
                            cache: false,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#NomineeDetails').html(D.d);
                            }
                        });
                    }
                }
            }





            var G = B.objPensionMaster; //alert(JSON.stringify(G));

            var cnhtmlP = "<img src='" + G.Photo + "' width='100px' height='116px'  />"
            $("#canPhoto").empty().html(cnhtmlP);

            var cnhtmlS = "<img src='" + G.Signature + "' width='150px' height='27px'  />"
            $("#canSign").empty().html(cnhtmlS);


            //alert($("#hdnDefaultMode").val());
            //if ($("#hdnDefaultMode").val()=="Insert")
            //    $("#cmdPersonalSave").attr("value", "Create");
            //else
            //    $("#cmdPersonalSave").attr("value","Update");
            $("#cmdPersonalSave").attr("value", "Update");
            $("#cmdNomineeSave").attr("value", "Update");

            if ($("#hdnPensionType").val() == "Pensioners" && $("#hdnCond").val() == "Select") {
                $("#cmdPersonalSave").prop("disabled", true);
                $("#btnNonQualiReason").prop("disabled", true);
            }
            else if ($("#hdnPensionType").val() == "New Pensioners" && $("#hdnCond").val() == "Accept") {
                $("#cmdPersonalSave").prop("disabled", false);
                $("#btnNonQualiReason").prop("disabled", false);
            }
        }


    };

    this.tabClick = function (TabName, TabPosition, tabarray) {
        $("#tabs").tabs({
            collapsible: true,
            selected: TabPosition
        });

        for (var i = 0; i <= tabarray.length; i++) {
            if (tabarray[i] === TabName) {
                tabarray.splice(i, 1);
            } 
        }
        //alert(tabarray);
    };
    this.SubmitForm = function () {
        
        $(".loading-overlay").show();
        var B = window.pensionMaster.pensionForm;
        var C = "{\'pensionForm\':\'" + JSON.stringify(B) + "\'}";
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/SavePensioner',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                var data = JSON.parse(D.d);
                    window.pensionMaster.pensionForm = JSON.parse(D.d);
                    alert('Pensioner Details are Saved Successfully !');
                    window.location.href = "Pensioner.aspx";
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
    this.UpdateForm = function () {
        var checkValue = "";
        var E = "{EmpNo: '" + $("#txtEmpCodeSearch").val() + "'}"; //alert(E);
        $.ajax({
            type: "POST",
            url: 'Pensioner.aspx/Get_PensionerCheckedValue',
            data: E,
            cache: false,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = JSON.parse(D.d); //alert(JSON.stringify(t));
                checkValue = t; //alert(checkValue);
                if (checkValue == "0") {
                    $(".loading-overlay").show();
                    var B = window.pensionMaster.pensionForm;
                    var C = "{\'pensionForm\':\'" + JSON.stringify(B) + "\'}";
                    //alert(JSON.stringify(C));
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/UpdatePensioner',
                        data: C,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $(".loading-overlay").hide();
                            var data = JSON.parse(D.d);
                            window.pensionMaster.pensionForm = JSON.parse(D.d);
                            alert('Pensioner Details are Updated Successfully !');
                            window.location.href = "PensionerSelection.aspx";
                        },
                        error: function (response) {
                            alert('');
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }
                else {
                    alert("Sorry ! You can not Update because you have already checked your records.");
                }
            }
        });
    };
    this.AddNonQualiReason = function () {
        $(".loading-overlay").show();
        var C = "{ Reason: '" + $("#txtNonQualiReasons").val() + "'}";
        //alert(C);
        $.ajax({
            type: "POST",
            url: 'Pensioner.aspx/AddNonQualiReason',
            data: C,
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $(".loading-overlay").hide();
                $('#ReasonDetails').html(D.d);
                $("#txtNonQualiReasons").val('');
                $("#txtNonQualiReasons").focus();
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
};
















