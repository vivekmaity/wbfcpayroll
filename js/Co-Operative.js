$(function () {
    $(':text').bind('keydown', function (e) {
        //on keydown for all textboxes
        if (e.target.className != "searchtextbox") {
            if (e.keyCode == 13) { //if this is enter key
                e.preventDefault();
                return false;
            }
            else
                return true;
        }
        else
            return true;
    });
});
$(document).ready(function () {

    document.getElementById("txtPayMonth").disabled = true;
    //$("#txtSearchEmp").keydown(function (event) {
    //    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
    //        (event.keyCode == 65 && event.ctrlKey === true) ||
    //        (event.keyCode >= 35 && event.keyCode <= 39)) {

    //        return;
    //    }
    //    else {
    //        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
    //            event.preventDefault();
    //            $("#Span8").html("Digits Only").show().fadeOut(1500);
    //            return false;

    //        }
    //    }
    //});
    //This is for 'Location' Binding on the Base of 'Sector'
    $(document).ready(function () {
        // $("#ddlPayScaleType").prop("disabled", false);
        $("#ddlSector").change(function () {
            var s = $('#ddlSector').val(); 
            if (s != 0) {
                if ($("#ddlSector").val() != "Please select") {
                    var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                    //alert(E);
                    var options = {};
                    options.url = "CoOperativeCalculation.aspx/GetCenter";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listCenter) {
                        var t = jQuery.parseJSON(listCenter.d);
                        var PayMon = t[0]["PayMonths"];
                        var a = t.length;
                        $("#ddlCenter").empty();
                        $('#txtPayMonth').val(PayMon);
                        if (a >= 0) {
                            $("#ddlCenter").append("<option value=''>(Select Center)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        }

                        $("#ddlCenter").prop("disabled", false);
                    };
                    options.error = function () { alert("Error in retrieving Center!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlCenter").empty();
                    $("#ddlCenter").prop("disabled", true);
                }
            }
            else {
                $("#ddlCenter").empty();
                $("#ddlCenter").append("<option value=''>(Select Center)</option>")
            }
            });

    });

    $("#detailTable tr").click(function () {
        alert("You clicked ROW:\n\nID:  " + $(this).attr('id'));
    });

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    window.cooperativeMaster = new KMDA.CoOperativeCalculation();
    window.cooperativeMaster.init();


});
KMDA.MasterInfo = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#cmdSearch").click(A.AddClicked);
        //$("#btnUpdate").click(A.UpdateDetail);
        $("#btnSearchEmp").click(A.SearchEmpValidation);
        $("#btnReset").click(A.SearchReset); 
    };
    this.getValidator = function () {
        $("#form1").validate({ ignore: "" });
        //$("#ddlEmpType").rules("add", { required: true, messages: { required: "Please select employee type" } });
        return $("#form1").validate({ ignore: "" });
    }
    this.AddClicked = function () {
        var Sec = $('#ddlSector').val();
        var Cen = $('#ddlCenter').val();
        if (Sec != 0) {
            if (Cen != "") {
                window.cooperativeMaster.SearchForm();
            } else {
                 //   alert("Please select Center.");
                //                $('#ddlCenter').focus();
                window.cooperativeMaster.SearchForm();
            }
        } else {
            alert("Please select Sector First.");
            $('#ddlSector').focus();
        }
    };
    this.SearchEmpValidation = function () {
        var EmpNo = $('#txtSearchEmp').val();
        if (EmpNo == "")
        { $('#txtSearchEmp').focus(); }
        else
        { window.cooperativeMaster.SearchFormbyEmpNo(); }
    };
    this.SearchReset = function () {
        window.cooperativeMaster.SearchForm();
        $('#txtSearchEmp').val('');
    };
    this.UpdateDetail = function () {
        var B = window.cooperativeMaster.cooperativeForm;

        for (var i = 0; i < B.length; i++) {

            var EmpNo = B[i].EmpNo;
            var textBox = '#txt_' + EmpNo;
            var textBoxValue = ($(textBox).val());
            B[i].EDAmount = textBoxValue;
            //alert(B[i].EDAmount);
        }
        var C = window.cooperativeMaster.cooperativeForm;
        window.cooperativeMaster.SaveAfterUpdate(C);
    };


};
KMDA.CoOperativeCalculation = function () {
    var A = this;
    this.cooperativeForm;
    this.init = function () {
        //this.cooperativeForm = JSON.parse($("#empJSON").val());
        this.masterInfo = new KMDA.MasterInfo();
        this.masterInfo.init();
        var B = window.cooperativeMaster.cooperativeForm;
    };
    this.SearchForm = function () {
        var B = window.cooperativeMaster.cooperativeForm;
        var SecID = $("#ddlSector").val();
        var SalFinYear = $("#txtSalFinYear").val();
        var CenterID = "";
        CenterID = ($("#ddlCenter").val() == "" ? 0 : $("#ddlCenter").val()); //alert(CenterID);
        var C = "{ SecID: " + SecID + ",  CenterID: " + CenterID + "}";
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/SearchDetails',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //alert(JSON.stringify(D));
                window.cooperativeMaster.cooperativeForm = JSON.parse(D.d);
                var da = JSON.parse(D.d);
                window.cooperativeMaster.showDetail(da);
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
    this.SearchFormbyEmpNo = function () {
        var B = window.cooperativeMaster.cooperativeForm;
        var SecID = $("#ddlSector").val();
        var SalFinYear = $("#txtSalFinYear").val();
        var CenterID = "";
        CenterID = ($("#ddlCenter").val() == "" ? 0 : $("#ddlCenter").val());
        var EmpNo = $("#txtSearchEmp").val();

        var C = "{ SecID: " + SecID + ",  CenterID: " + CenterID + ", EmpNo: '" + EmpNo + "'}";
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/SearchDetailsbyEmpNo',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //alert(JSON.stringify(D));
                window.cooperativeMaster.cooperativeForm = JSON.parse(D.d);
                var da = JSON.parse(D.d);
                window.cooperativeMaster.showDetail(da);
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
    this.SaveAfterUpdate = function (detail) {
        var B = window.cooperativeMaster.cooperativeForm;
        var C = "{\'cooperativeForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/UpdateDetails',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var result = JSON.parse(D.d);
                if (result == "Success") {
                    alert("Co-Operative Amount is Updated Successfully.");
                    window.cooperativeMaster.CloseContent();
                }
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    };
    this.showDetail = function (Detail) {
        var html = '';
        if (Detail != "undefined") {
            //alert(JSON.stringify(Detail));
            if (Detail.length > 0) {
                var edname = Detail[0].EDName;
                html += " <table align='center' border='2' cellpadding='5' width='98%'>" + "\n"
                          + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;'>Sl No.</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Employee No</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Employee Name</td>"
                           //+ "<td class='th IsNct' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>Projected ITAX</td>"
                          + "<td class='th' style='background-color:lightblue;height:20px;font-weight:bold;font-size:17px;'>" + edname + "</td>"
                          + "</tr>";
                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                var j = 0;
                for (var i = 0; i < Detail.length; i++) {
                    j = j + 1;
                    html += "<tr id='EmpNo_" + Detail[i].EmpNo + "'>"
                                + "<td class='tr' style='width:100px; padding:8px;'>" + j + "</td>"
                                + "<td  class='tr' style='width:140px;'>" + Detail[i].EmpNo + "</td>"
                                + "<td  class='tr' style='width:210px;'>" + Detail[i].EmpName + "</td>"
                               // + "<td class='tr IsNct' style='width:210px'>" + "<input type='text' value='" + Detail[i].ItaxForThisMonth + "' disabled />" + "</td>"
                                + "<td  class='tr'>" + "<input type='text' id='" + Detail[i].EmpNo + "' style='text-align:center;'  value='" + Detail[i].EDAmount + "' onchange='return window.cooperativeMaster.ChangeValue(this)' onkeydown = 'return (event.keyCode!=13)' />" + "</td>" // onchange='return window.cooperativeMaster.ChangeValue(this)'

                    html += "</tr>";
                    html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";

                }
                html += "</table>";
               
                $("#divTable").empty().html(html);
                if (edname != "ITAX") {
                    $(".IsNct").hide();
                }
                $("#loadBookingDet").show();
            }
        }
    };
    this.CloseContent = function () {
        $("#txtSearchEmp").val('');
        $("#loadBookingDet").hide();
        return false;
    };
    this.ChangeValue = function (txt) {
        var $this = $(txt);
        var id = $this.attr("id");
        var textBoxID = '#' + id;
        
        var Amount = ($(textBoxID).val());
        // alert(id); alert(textBoxValue);
        var C = "{ id: '" + id + "',  Amount: '" + Amount + "', SecID: '" + $("#ddlSector").val() + "'}";

        $.ajax({
            type: "POST",
            url: pageUrl + '/UpdateDetailsRowWise',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var result = JSON.parse(D.d);
                if (result == "Not") {
                    alert('Sorry ! Your salary has been already Processed. \n You can not Save.');
                    var SecID = $("#ddlSector").val();
                    var SalFinYear = $("#txtSalFinYear").val();
                    var CenterID = "";
                    CenterID = ($("#ddlCenter").val() == "" ? 0 : $("#ddlCenter").val()); //alert(CenterID);
                    var K = "{ SecID: " + SecID + ",  CenterID: " + CenterID + "}";
                    //alert(K);
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/SearchDetails',
                        data: K,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            //alert(JSON.stringify(D));
                            window.cooperativeMaster.cooperativeForm = JSON.parse(D.d);
                            var da = JSON.parse(D.d);
                            window.cooperativeMaster.showDetail(da);
                        },
                        error: function (response) {
                            alert('');
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                    return;
                }
                else
                {
                    $(textBoxID).css("background-color", "lightgreen");
                }
                //if (result == "success") {
                    //$(textBoxID).css("background-color", "lightgreen");
                    //alert("Co-Operative Amount is Updated Successfully.");
                    //window.cooperativeMaster.CloseContent();
                //}
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    };

};
















