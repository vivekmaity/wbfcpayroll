﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class GridviewTemplete : System.Web.UI.UserControl
{
    public string PagingChangeValue = "";
    public string EmpNo { get; set; }
    /*==================================================*/
    /*PopulateGridfroEmpDesig()*/
    public int ReportID { get; set; }
    public string Parmflag { get; set; }
    public string InsertedBy { get; set; }
    /*==================================================*/
    /* PopulateGPFandCPF*/
    public string PFCode { get; set; }
    public string SalMonthID { get; set; }
    public string EmpType { get; set; }
    public string SecID { get; set; }
    //public string PFType { get; set; }
    public string Type { get; set; }
    public string Condition { get; set; }
    /*==================================================*/
    /* PopulateGPFEmployeeWiseF*/
    public string GPFEmpWiseSalMonthID { get; set; }
    public string GPFEmpWiseSectorID { get; set; }
    public string GPFEmpPFCode { get; set; }
    public string GPFEmpNo { get; set; }
    /*==================================================*/
    /* PopulateInstrument*/
    public string InstrumentType { get; set; }
    public string BankName { get; set; }
    public string BranchName { get; set; }
    /*==================================================*/
    /*SetSessionValueNull*/
    public string SetSessionValuenull { get; set; }
    /*==================================================*/
    /*PopulateLockFieldPermission*/
    public string FieldPermission { get; set; }
    /*==================================================*/
    /*PopulateGridUserWise*/
    public string UserWise { get; set; }
    public string UserID { get; set; }
    /*==================================================*/
    /*PopulateBranchList*/
    public string Prefix { get; set; }
    public string BankID { get; set; }
    public string BankNam { get; set; }
    public string BranchCondition { get; set; }
    /*==================================================*/
    /*PopulateBranchByPaging*/
    public int PageSize { get; set; }
    public int PageIndex { get; set; }
    public string PagingCondition { get; set; }
    public int PagingBankID { get; set; }
    public string PagingBankName { get; set; }
    /*==================================================*/
    /*PopulateMessageBox*/
    public string Message { get; set; }
    public string MessageButtonText { get; set; }
    /*==================================================*/
    /*PopulateSanction*/
    public string EditMode { get; set; }
    public string SancEmpNo { get; set; }
    public string RowIndex { get; set; }
    /*==================================================*/
    /*PopulatePensionerDetails*/
    public string SalID { get; set; }
    public string Subject { get; set; }
    /*==================================================*/
    /*PopulateUpdateDaHraMa*/
    public string DaHraMaEmpNo { get; set; }
    public string DaHraMaEarningType { get; set; }
    public string DaHraMaValue { get; set; }
    public string DaHraMaEmpType { get; set; }
    public string DaHraMaSecID { get; set; }
    public string DaHraMaPayMonthID { get; set; }
    public string DaHraMaAdminID { get; set; }
    /*==================================================*/
    /*PopulateVoucherDetails*/
    public string BankorCash { get; set; }
    public string BankorCashType { get; set; }
    /*==================================================*/
    /*PopulateVoucherDetailsOnSearch*/
    public string AccountDetOnSearchprefix { get; set; }
    /*==================================================*/
    /*PopulatePFBankReceipt*/
    public Boolean PFBankReceiptBolResult { get; set; }
    public string PFBankReceiptEditMode { get; set; }
    /*==================================================*/
    /*PopulatePFPaymentDetailforEmp*/
    public DataTable dtPFPaymentDet { get; set; }
    /*==================================================*/
    /*PopulatePensionPaymentDetail*/
    public string PensionMode { get; set; }
    public string PensionModeEmpNo { get; set; }
    /*==================================================*/
    /*AddPensionPaymentAmount*/
    public DataTable dtPensionPaymentDetail { get; set; }
    

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                //PopulateGrid();
                /*===========================================================================================================================*/
                if (ReportID != 0 && Parmflag != "")
                    PopulateGridfroEmpDesig(ReportID, Parmflag);
                /*===========================================================================================================================*/
                if (PFCode != "" && SalMonthID != "" && EmpType !=""  && Type == "M" && Condition != "")
                    PopulateGPF(PFCode, SalMonthID, EmpType,  Condition);
                /*============================================================================================================================*/
                if (GPFEmpWiseSalMonthID != "" && GPFEmpWiseSectorID != "" && Type == "E" && GPFEmpPFCode != "" && GPFEmpNo != "")
                    PopulateGPFEmployeeWise(GPFEmpPFCode, GPFEmpWiseSalMonthID, GPFEmpWiseSectorID, GPFEmpNo);
                /*============================================================================================================================*/
                if (InstrumentType != "" && BankName != "" && BranchName != "" && InstrumentType != null && BankName != null && BranchName != null)
                    PopulateInstrument(InstrumentType, BankName, BranchName);
                /*============================================================================================================================*/
                if (SetSessionValuenull != "" && SetSessionValuenull != null)
                    SetSessionValueNull(SetSessionValuenull);
                /*============================================================================================================================*/
                if (FieldPermission != "" && FieldPermission != null)
                    PopulateLockFieldPermission(FieldPermission);
                /*============================================================================================================================*/
                if (UserWise != "" && UserWise != null && UserID != "" && UserID != null)
                    PopulateGridUserWise(UserWise, UserID);
                /*============================================================================================================================*/
                if (Prefix != "" && BankID != null && Prefix != "" && BankID != null && BranchCondition != "" && BranchCondition !=null)
                    PopulateBranchList(Prefix, BankID, BankNam, BranchCondition);
                /*============================================================================================================================*/
                if (PageSize != 0 && PageIndex != null  && PageSize != null && PagingCondition != null && PagingCondition != "")
                    PopulateBranchByPaging(PageSize, PageIndex, PagingCondition, PagingBankID, PagingBankName);
                /*============================================================================================================================*/
                if (Message != "" && Message != null)
                    PopulateMessageBox(Message, MessageButtonText == null ? "" : MessageButtonText);
                /*=============================================================================================================================*/
                if (EditMode != "" &&  SancEmpNo !=null)
                    PopulateSanction(EditMode, SancEmpNo, RowIndex);
                /*=============================================================================================================================*/
                if (SalID != null && SalID!="" && Subject != "" && Subject != null)
                    PopulatePensionerDetails(SalID, Subject);
                /*=============================================================================================================================*/
                if (DaHraMaValue != null && DaHraMaValue != "" && DaHraMaEmpType != "" && DaHraMaEmpType != null && DaHraMaSecID != "" && DaHraMaSecID != null && DaHraMaPayMonthID != "" && DaHraMaPayMonthID != null)
                    PopulateUpdateDaHraMa(DaHraMaEarningType, DaHraMaValue, DaHraMaEmpType, DaHraMaSecID, DaHraMaPayMonthID, DaHraMaAdminID, DaHraMaEmpNo);
                /*=============================================================================================================================*/
                if (BankorCash != null && BankorCash != "" && BankorCashType != null && BankorCashType != "")
                    PopulateVoucherDetails(BankorCash, BankorCashType);
                /*=============================================================================================================================*/
                if (AccountDetOnSearchprefix != null && AccountDetOnSearchprefix != "")
                    PopulateVoucherDetailsOnSearch(AccountDetOnSearchprefix);
                /*=============================================================================================================================*/
                if (PFBankReceiptEditMode != null && PFBankReceiptEditMode != "")
                    PopulatePFBankReceipt(PFBankReceiptEditMode);
                /*=============================================================================================================================*/
                if (dtPFPaymentDet == null)
                { dtPFPaymentDet = new DataTable(); DataRow dr = dtPFPaymentDet.NewRow(); }
                if (dtPFPaymentDet.Rows.Count != 0)
                    PopulatePFPaymentDetailforEmp(dtPFPaymentDet);
                /*============================================================================================================================*/
                if (PensionMode != "" && PensionMode != null )
                    PopulatePensionPaymentDetail(PensionMode, PensionModeEmpNo);
                /*============================================================================================================================*/
                if (dtPensionPaymentDetail == null)
                { dtPensionPaymentDetail = new DataTable(); DataRow dr = dtPensionPaymentDetail.NewRow(); }
                if (dtPensionPaymentDetail.Rows.Count != 0)
                    AddPensionPaymentAmount(dtPensionPaymentDetail);

            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateGrid()
    {
        try
        {
            DataTable dtsector = DBHandler.GetResult("Get_EmpdetailforacceptLPC", 1);
            if (dtsector.Rows.Count > 0)
            {
                //tblEmpdetailss.DataSource = dtsector;
                //tblEmpdetailss.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateGridfroEmpDesig(int ReportID, string Parmflag)
    {
        string RepParam="";
        try
        {
            if (Parmflag == "N")
                RepParam = "";
            else
                RepParam = "";

            DataSet dsRepQry = DBHandler.GetResults("returnReportQuery", ReportID, Parmflag, RepParam, InsertedBy);
            DataTable dtRepQry = dsRepQry.Tables[0];

            if (dtRepQry.Rows.Count > 0)
            {
                GridView gvEmpDetforDesig = new GridView();
                gvEmpDetforDesig.ID = "EmployeeGridView";
                gvEmpDetforDesig.GridLines = GridLines.Both;
                gvEmpDetforDesig.CssClass = "Grid";
                gvEmpDetforDesig.AlternatingRowStyle.CssClass = "alt";
                gvEmpDetforDesig.PagerStyle.CssClass = "pgr";
                gvEmpDetforDesig.AlternatingRowStyle.BackColor=System.Drawing.Color.LightYellow; 
                gvEmpDetforDesig.AutoGenerateColumns = false;
                gvEmpDetforDesig.EnableViewState = true;
                
                if (dsRepQry != null)
                {
                    for (int i = 0; i < dtRepQry.Columns.Count; i++)
                    {
                        BoundField boundfield = new BoundField();
                        boundfield.DataField = dtRepQry.Columns[i].ColumnName.ToString();
                        boundfield.HeaderText = dtRepQry.Columns[i].ColumnName.ToString();
                        boundfield.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        gvEmpDetforDesig.Columns.Add(boundfield);
                    }
                    gvEmpDetforDesig.DataSource = dtRepQry;
                    gvEmpDetforDesig.DataBind();

                    PlaceHolder1.Controls.Add(gvEmpDetforDesig);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateGPF(string PFCode, string SalMonthID, string EmpType,  string Condition)
    {
        try
        {
            /***************************************************************************************************************/
            //First insert total sector wise details in "Temp_DAT_PF" table.

            DataTable dtEmpGPFSectorWise = DBHandler.GetResult("Get_GPFDetailSectorWise", PFCode, SalMonthID, "", "",
                                           Condition, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            /***************************************************************************************************************/
            DataTable dtGPF = DBHandler.GetResult("Get_GPFandCPF", SalMonthID, EmpType, PFCode);
            if (dtGPF.Rows.Count > 0)
            {
                gvGPF.DataSource = dtGPF;
                gvGPF.DataBind();

                int TotalEmp = dtGPF.AsEnumerable().Sum(row => row.Field<Int32>("Total_Employee"));
                gvGPF.FooterRow.Cells[2].Text = "Total";
                gvGPF.FooterRow.Cells[2].Font.Bold = true;
                gvGPF.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                gvGPF.FooterRow.BackColor = System.Drawing.Color.Aqua;

                gvGPF.FooterRow.Cells[3].Text = TotalEmp.ToString();
                gvGPF.FooterRow.Cells[3].Font.Bold = true;
                gvGPF.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;
                gvGPF.FooterRow.BackColor = System.Drawing.Color.Aqua;

                decimal TotalSubs = dtGPF.AsEnumerable().Sum(row => row.Field<decimal>("Total_PF_Subscription"));
                gvGPF.FooterRow.Cells[4].Text = TotalSubs.ToString();
                gvGPF.FooterRow.Cells[4].Font.Bold = true;
                gvGPF.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;
                gvGPF.FooterRow.BackColor = System.Drawing.Color.Aqua;

                decimal TotalRec = dtGPF.AsEnumerable().Sum(row => row.Field<decimal>("Total_PF_Recover"));
                gvGPF.FooterRow.Cells[5].Text = TotalRec.ToString();
                gvGPF.FooterRow.Cells[5].Font.Bold = true;
                gvGPF.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;
                gvGPF.FooterRow.BackColor = System.Drawing.Color.Aqua;

                decimal TotalGPF = dtGPF.AsEnumerable().Sum(row => row.Field<decimal>("Total_GPF"));
                gvGPF.FooterRow.Cells[6].Text = TotalGPF.ToString();
                gvGPF.FooterRow.Cells[6].Font.Bold = true;
                gvGPF.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;
                gvGPF.FooterRow.BackColor = System.Drawing.Color.Aqua;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateGPFEmployeeWise(string GPFEmpPFCode, string GPFEmpWiseSalMonthID, string GPFEmpWiseSectorID, string GPFEmpNo)
    {
        try
        {
            string Condition = Convert.ToInt32(GPFEmpNo) > 0 ? "Search" : "";

            DataTable dtGPF = DBHandler.GetResult("Get_GPFDetailSectorWise", GPFEmpPFCode, GPFEmpWiseSalMonthID, GPFEmpNo, GPFEmpWiseSectorID, Condition, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (dtGPF.Rows.Count > 0)
            {
                gvGPFEmployeeWise.DataSource = dtGPF;
                gvGPFEmployeeWise.DataBind();

                //int TotalEmp = dtGPF.AsEnumerable().Sum(row => row.Field<Int32>("Total_Employee"));
                gvGPFEmployeeWise.FooterRow.Cells[3].Text = "Total";
                gvGPFEmployeeWise.FooterRow.Cells[3].Font.Bold = true;
                gvGPFEmployeeWise.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                gvGPFEmployeeWise.FooterRow.BackColor = System.Drawing.Color.Aqua;

                decimal TotalSubs = dtGPF.AsEnumerable().Sum(row => row.Field<decimal>("Total_PF_Subscription"));
                gvGPFEmployeeWise.FooterRow.Cells[4].Text = TotalSubs.ToString();
                gvGPFEmployeeWise.FooterRow.Cells[4].Font.Bold = true;
                gvGPFEmployeeWise.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;
                gvGPFEmployeeWise.FooterRow.BackColor = System.Drawing.Color.Aqua;

                decimal TotalRec = dtGPF.AsEnumerable().Sum(row => row.Field<decimal>("Total_PF_Recover"));
                gvGPFEmployeeWise.FooterRow.Cells[5].Text = TotalRec.ToString();
                gvGPFEmployeeWise.FooterRow.Cells[5].Font.Bold = true;
                gvGPFEmployeeWise.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;
                gvGPFEmployeeWise.FooterRow.BackColor = System.Drawing.Color.Aqua;

                decimal TotalGPF = dtGPF.AsEnumerable().Sum(row => row.Field<decimal>("Total_GPF"));
                gvGPFEmployeeWise.FooterRow.Cells[6].Text = TotalGPF.ToString();
                gvGPFEmployeeWise.FooterRow.Cells[6].Font.Bold = true;
                gvGPFEmployeeWise.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;
                gvGPFEmployeeWise.FooterRow.BackColor = System.Drawing.Color.Aqua;

                foreach (GridViewRow grdRow in gvGPFEmployeeWise.Rows)
                {
                    Label Sanval = (Label)(gvGPFEmployeeWise.Rows[grdRow.RowIndex].Cells[7].FindControl("lblDecline"));
                    string Decline = Sanval.Text;
                    if (Decline == "Y")
                    {
                        grdRow.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        grdRow.BackColor = System.Drawing.Color.LightYellow;
                    }
                }
                PlaceHolder2.Controls.Add(gvGPFEmployeeWise);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateInstrument(string InstrumentType, string BankName, string BranchName)
    {
        try
        {
            
            if (HttpContext.Current.Session["Instrument"] != null)
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["Instrument"];
                DataRow dtrow = dt.NewRow();
                dtrow["Type"] = InstrumentType;
                dtrow["Bank/Branch"] = BankName + "(" + BranchName + ")";
                //dtrow["BranchName"] = BranchName;
                dt.Rows.Add(dtrow);

                GvInstrument.DataSource = dt;
                GvInstrument.DataBind();
                HttpContext.Current.Session["Instrument"] = dt;
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Type");
                dt.Columns.Add("Bank/Branch");
                //dt.Columns.Add("BranchName");

                DataRow dtrow = dt.NewRow();
                dtrow["Type"] = InstrumentType;
                dtrow["Bank/Branch"] = BankName + "(" + BranchName + ")";
                //dtrow["BranchName"] = BranchName;
                dt.Rows.Add(dtrow);

                GvInstrument.DataSource = dt;
                GvInstrument.DataBind();
                HttpContext.Current.Session["Instrument"] = dt;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void SetSessionValueNull(string SetSessionValuenull)
    {
        try
        {
            if (SetSessionValuenull == "NullSession")
            {
                HttpContext.Current.Session["Instrument"] = null;
                DataTable dt = new DataTable();
                dt.Columns.Add("Type");
                dt.Columns.Add("Bank/Branch");

                DataRow dtrow = dt.NewRow();
                dtrow["Type"] = "";
                dtrow["Bank/Branch"] = "";
                dt.Rows.Add(dtrow);

                GvInstrument.DataSource = dt;
                GvInstrument.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateLockFieldPermission(string FieldPermission)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_EmpFieldPermission");
            if (dt.Rows.Count > 0)
            {
                grdLockPermission.DataSource = dt;
                grdLockPermission.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateGridUserWise(string UserWise, string UserID)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_EmpFieldPermissionUserWise", UserID);
            if (dt.Rows.Count > 0)
            {
                grdLockPermission.DataSource = dt;
                grdLockPermission.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateBranchList(string Prefix, string BankID, string BankNam, string BranchCondition)
    {
        DataTable dt = new DataTable(); string Event = "true";
        try
        {
            if(BranchCondition =="PageLoad")
                dt = DBHandler.GetResult("Get_Branch");

            else if(BranchCondition =="OnTextChange")
                if (Prefix != "")
                    dt = DBHandler.GetResult("Get_GetBranchList", Prefix, BankID);
                else
                {
                    dt = DBHandler.GetResult("Get_BranchbyBankID", BankID);
                    if (dt.Rows.Count > 0)
                    {
                        dt.Columns.Add("BankName");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dt.Rows[i]["BankName"] = BankNam;
                        }
                        tblBranch.DataSource = dt;
                        tblBranch.DataBind();
                        Event = "false";
                    }
                }


            else if (BranchCondition == "OnBankSelection")
            {
                dt = DBHandler.GetResult("Get_BranchbyBankID", BankID);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns.Add("BankName");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["BankName"] = BankNam;
                    }
                    tblBranch.DataSource = dt;
                    tblBranch.DataBind();
                    Event = "false";
                }
            }

            //else if (BranchCondition == "AllBank")
            //{
            //    dt = DBHandler.GetResult("Get_BranchbyBankID", Prefix);
            //    if (dt.Rows.Count > 0)
            //    {
            //        tblBranch.DataSource = dt;
            //        tblBranch.DataBind();
            //    }
            //}

            if (Event == "true")
            {
                if (dt.Rows.Count > 0)
                {
                    tblBranch.DataSource = dt;
                    tblBranch.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateBranchByPaging(int PageSize, int PageIndex, string PagingCondition, int PagingBankID, string PagingBankName)
    {
        try
        {
            List<Branches> lstBranches = new List<Branches>();
            if (PagingCondition == "PageLoad")
            {
                DataTable dt = DBHandler.GetResult("Get_Branch");
                if (dt.Rows.Count > 0)
                {
                    for (int j = 1; j <= dt.Rows.Count - 1; j++)
                    {
                        lstBranches.Add(new Branches
                        {
                            BranchID = Convert.ToInt32(dt.Rows[j]["BranchID"].ToString()),
                            Branch = dt.Rows[j]["Branch"].ToString(),
                            IFSC = dt.Rows[j]["IFSC"].ToString(),
                            MICR = dt.Rows[j]["MICR"].ToString(),
                            BankID = Convert.ToInt32(dt.Rows[j]["BankID"].ToString()),
                            BankName = dt.Rows[j]["BankName"].ToString()
                        });
                    }
                }

                lstBranches = lstBranches.Skip(this.PageIndex * this.PageSize).Take(this.PageSize).ToList();

                tblBranch.DataSource = lstBranches;
                tblBranch.DataBind();
            }
            else if (PagingCondition == "OnBankSelection")
            {
                DataTable dt = DBHandler.GetResult("Get_BranchbyBankID", PagingBankID);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns.Add("BankName");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["BankName"] = PagingBankName;
                    }
                    for (int j = 1; j <= dt.Rows.Count - 1; j++)
                    {
                        lstBranches.Add(new Branches
                        {
                            BranchID = Convert.ToInt32(dt.Rows[j]["BranchID"].ToString()),
                            Branch = dt.Rows[j]["Branch"].ToString(),
                            IFSC = dt.Rows[j]["IFSC"].ToString(),
                            MICR = dt.Rows[j]["MICR"].ToString(),
                            BankID = Convert.ToInt32(dt.Rows[j]["BankID"].ToString()),
                            BankName = dt.Rows[j]["BankName"].ToString()
                        });
                    }
                    lstBranches = lstBranches.Skip(this.PageIndex * this.PageSize).Take(this.PageSize).ToList();

                        tblBranch.DataSource = lstBranches;
                        tblBranch.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public class Branches
    {
        public int BranchID { get; set; }
        public string Branch { get; set; }
        public string IFSC { get; set; }
        public string MICR { get; set; }
        public int BankID { get; set; }
        public string BankName { get; set; }
    }

    protected void PopulateMessageBox(string Message, string MessageButtonText)
    {
        try
        {
            string filePath = Server.MapPath("~/msgbox.tpl");
            MessageBox msgbox = new MessageBox(filePath);
            msgbox.SetTitle("Confirmation");
            msgbox.SetIcon("images/msg_icon_1.png");
            msgbox.SetMessage(Message);
            if (MessageButtonText=="")
            msgbox.SetOKButton("msg_button_class");
            else
                msgbox.SetYesNoButton("msg_button_class");

            msgboxpanel.InnerHtml = msgbox.ReturnObject();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    protected void PopulateSanction(string EditMode, string SancEmpNo, string RowIndex)
    {
        try
        {
            if (EditMode == "PageLoad")
            {
                DataTable dtstr = DBHandler.GetResult("Get_LoanDetailsforSanction");
                if (dtstr.Rows.Count > 0)
                {
                    tblSanction.DataSource = dtstr;
                    tblSanction.DataBind();
                    foreach (GridViewRow grdRow in tblSanction.Rows)
                    {
                        TextBox Sanval = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));
                        LinkButton btnUpd = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Edit"));
                        LinkButton btnPrint = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Print"));
                        btnUpd.Visible = false;
                        btnPrint.Visible = true;

                        CheckBox chkSelect = (CheckBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("chkSelect"));
                        if (Sanval.Text != "")
                        {
                            LinkButton btnSan = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Sanction"));
                            TextBox Sandate = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));
                            btnSan.Visible = false;
                            Sandate.Enabled = false;
                            btnUpd.Visible = true;
                            //btnInstrument.Enabled = true;
                            if (Sandate.Text != "")
                            {
                                DateTime dtSanc = Convert.ToDateTime(Sandate.Text);
                                string dtsandate = dtSanc.ToString("dd/MM/yyyy");
                                Sandate.Text = dtsandate;
                            }
                            grdRow.BackColor = System.Drawing.Color.LightPink;

                            if (grdRow.BackColor == System.Drawing.Color.LightPink)
                            {
                                chkSelect.Enabled = true;
                                btnPrint.Visible = true;
                            }
                        }
                        else
                        {
                            if (grdRow.BackColor != System.Drawing.Color.LightPink)
                            {
                                chkSelect.Enabled = false;
                                btnPrint.Visible = false;
                            }
                        }
                           
                    }
                }
            }
            else if (EditMode == "Edit" )  //&& SancEmpNo == ""
            {
                int rowIndex = Convert.ToInt32(RowIndex);
                DataTable dtstr = DBHandler.GetResult("Get_LoanDetailsforSanction");
                if (dtstr.Rows.Count > 0)
                {
                    tblSanction.DataSource = dtstr;
                    tblSanction.DataBind();
                    foreach (GridViewRow grdRow in tblSanction.Rows)
                    {
                        TextBox Sanval = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));
                        LinkButton btnUpd = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Edit"));
                        LinkButton btnPrint = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Print"));
                        LinkButton btnSan = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Sanction"));
                        TextBox Sandate = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));

                        CheckBox chkSelect = (CheckBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("chkSelect"));
                        if (Sanval.Text != "" && grdRow.RowIndex == rowIndex - 1)//&& grdRow.RowIndex == rowIndex - 1
                        {
                            grdRow.BackColor = System.Drawing.Color.LightPink;
                            btnUpd.Visible = false;
                            btnPrint.Visible = false;
                            btnSan.Visible = true;
                            Sandate.Enabled = true;
                            string date = Sandate.Text;
                            if (date != "")
                            {
                                DateTime dtSanc = Convert.ToDateTime(date);
                                string dtsandate = dtSanc.ToString("dd/MM/yyyy");
                                Sandate.Text = dtsandate;
                            }
                            if (grdRow.BackColor == System.Drawing.Color.LightPink)
                            {
                                chkSelect.Enabled = true;
                                btnPrint.Visible = true;
                            }
                        }
                        else
                        {
                            if (Sanval.Text != "")
                            {
                                grdRow.BackColor = System.Drawing.Color.LightPink;
                                btnUpd.Visible = true;
                                btnSan.Visible = false;
                                btnPrint.Visible = false;
                                Sandate.Enabled = false;
                                string date = Sandate.Text;
                                if (date != "")
                                {
                                    DateTime dtSanc = Convert.ToDateTime(date);
                                    string dtsandate = dtSanc.ToString("dd/MM/yyyy");
                                    Sandate.Text = dtsandate;
                                }
                                if (grdRow.BackColor == System.Drawing.Color.LightPink)
                                {
                                    chkSelect.Enabled = true;
                                    btnPrint.Visible = true;
                                }
                            }
                            else
                            {
                                //grdRow.BackColor = System.Drawing.Color.LightPink;
                                btnSan.Visible = true;
                                btnUpd.Visible = false;
                                btnPrint.Visible = false;
                                Sandate.Enabled = true;

                                if (grdRow.BackColor != System.Drawing.Color.LightPink)
                                {
                                    chkSelect.Enabled = false;
                                    btnPrint.Visible = false;
                                }
                            }
                        }
                    }
                }
            }

            #region Search by EmpNo
            else if (SancEmpNo != "" && EditMode == "Search")
            {
                string EmployeeNo = SancEmpNo;
                DataSet ds = DBHandler.GetResults("Get_LoanDetailsforSanctionbyEmpNo", EmployeeNo);
                DataTable dt = ds.Tables[0];

                if (dt.Rows.Count > 0)
                {
                    tblSanction.DataSource = null;
                    tblSanction.DataSource = dt;
                    tblSanction.DataBind();
                    foreach (GridViewRow grdRow in tblSanction.Rows)
                    {
                        TextBox Sanval = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));
                        LinkButton btnUpd = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Edit"));
                        LinkButton btnPrint = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Print"));
                        btnUpd.Visible = false;
                        btnPrint.Visible = true;
                        CheckBox chkSelect = (CheckBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("chkSelect"));
                        if (Sanval.Text != "")
                        {
                            LinkButton btnSan = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Sanction"));
                            TextBox Sandate = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));
                            btnSan.Visible = false;
                            Sandate.Enabled = false;
                            btnUpd.Visible = true;
                            //btnInstrument.Enabled = true;
                            
                            if (Sandate.Text != "")
                            {
                                DateTime dtSanc = Convert.ToDateTime(Sandate.Text);
                                string dtsandate = dtSanc.ToString("dd/MM/yyyy");
                                Sandate.Text = dtsandate;
                            }
                            grdRow.BackColor = System.Drawing.Color.LightPink;

                            if (grdRow.BackColor == System.Drawing.Color.LightPink)
                            {
                                chkSelect.Enabled = true;
                                btnPrint.Visible = true;
                            }
                        }
                        else
                        {
                            if (grdRow.BackColor != System.Drawing.Color.LightPink)
                            {
                                chkSelect.Enabled = false;
                                btnPrint.Visible = false;
                            }
                        }
                    }
                }
            }
            # endregion

            #region Search by EmpNo
            else if (SancEmpNo != "" && EditMode == "OnSearchEdit")
            {
                int rowIndex = Convert.ToInt32(RowIndex);
                string EmployeeNo = SancEmpNo;
                DataSet ds = DBHandler.GetResults("Get_LoanDetailsforSanctionbyEmpNo", EmployeeNo);
                DataTable dt = ds.Tables[0];

                if (dt.Rows.Count > 0)
                {
                    tblSanction.DataSource = dt;
                    tblSanction.DataBind();
                    foreach (GridViewRow grdRow in tblSanction.Rows)
                    {
                        TextBox Sanval = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));
                        LinkButton btnUpd = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Edit"));
                        LinkButton btnPrint = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Print"));
                        LinkButton btnSan = (LinkButton)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("Sanction"));
                        TextBox Sandate = (TextBox)(tblSanction.Rows[grdRow.RowIndex].Cells[0].FindControl("txtSancDate"));

                        if (Sanval.Text != "" || grdRow.RowIndex == rowIndex - 1)
                        {
                            grdRow.BackColor = System.Drawing.Color.LightPink;
                            btnUpd.Visible = false;
                            btnPrint.Visible = false;
                            btnSan.Visible = true;
                            Sandate.Enabled = true;
                            string date = Sandate.Text;
                            if (date != "")
                            {
                                DateTime dtSanc = Convert.ToDateTime(date);
                                string dtsandate = dtSanc.ToString("dd/MM/yyyy");
                                Sandate.Text = dtsandate;
                            }
                        }
                        //else
                        //{
                        //    grdRow.BackColor = System.Drawing.Color.LightPink;
                        //    btnUpd.Visible = true;
                        //    btnSan.Visible = false;
                        //    btnPrint.Visible = false;
                        //    Sandate.Enabled = false;
                        //    string date = Sandate.Text;
                        //    if (date != "")
                        //    {
                        //        DateTime dtSanc = Convert.ToDateTime(date);
                        //        string dtsandate = dtSanc.ToString("dd/MM/yyyy");
                        //        Sandate.Text = dtsandate;
                        //    }
                        //}
                    }
                }
            }
            # endregion
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void Grid_PreRender(object sender, EventArgs e)
    {
        if (PagingChangeValue == "LoanSanction")
        {
            tblSanction.UseAccessibleHeader = false;
            tblSanction.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        //else if (PagingChangeValue == "PFBankReceipt")
        //{
        //    gvPFBankReceipt.UseAccessibleHeader = false;
        //    gvPFBankReceipt.HeaderRow.TableSection = TableRowSection.TableHeader;
        //}
    }

    protected void PopulatePensionerDetails(string SalID, string Subject)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_PensionerDetails", SalID);
            if (dt.Rows.Count > 0)
            {
                gvPensionerDetail.DataSource = dt;
                gvPensionerDetail.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateUpdateDaHraMa(string DaHraMaEarningType, string DaHraMaValue, string DaHraMaEmpType, string DaHraMaSecID, string DaHraMaPayMonthID, string DaHraMaAdminID, string DaHraMaEmpNo)
    {
        try
        {
            DataTable dtresult = DBHandler.GetResult("Get_ForUpdateEmpDaHraMa", DaHraMaEarningType, DaHraMaValue, DaHraMaEmpType, DaHraMaSecID, DaHraMaPayMonthID, DaHraMaAdminID);
            if (dtresult.Rows.Count > 0)
            {
                DataTable dt=new DataTable();
                if(DaHraMaEmpNo =="0")
                    dt = DBHandler.GetResult("Get_ForEmpDaHraMaDetail", DaHraMaSecID, DaHraMaEarningType, DaHraMaEmpNo, DaHraMaEmpType);
                else
                    dt = DBHandler.GetResult("Get_ForEmpDaHraMaDetail", DaHraMaSecID, DaHraMaEarningType, DaHraMaEmpNo, DaHraMaEmpType);

                if (dt.Rows.Count > 0)
                {
                    grvUpdateDaHraMa.DataSource = dt;
                    grvUpdateDaHraMa.DataBind();

                    foreach (GridViewRow grdRow in grvUpdateDaHraMa.Rows)
                    {
                        Label decline = (Label)(grvUpdateDaHraMa.Rows[grdRow.RowIndex].Cells[6].FindControl("lblDecline"));
                        string Decline = decline.Text;
                        if (Decline == "Y")
                        {
                            grdRow.BackColor = System.Drawing.Color.LightPink;
                        }
                        else
                        {
                            grdRow.BackColor = System.Drawing.Color.LightYellow;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateVoucherDetails(string BankorCash, string BankorCashType)
    {
        DataTable dtDetail=new DataTable();
        try
        {
            dtDetail = DBHandler.GetResult("Get_AccountDetailsbyVoucherType", BankorCash, BankorCashType);

            if (dtDetail.Rows.Count > 0)
            {
                grvVoucherDetail.DataSource = dtDetail;
                grvVoucherDetail.DataBind();
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateVoucherDetailsOnSearch(string AccountDetOnSearchprefix)
    {
        DataTable dtDetail = new DataTable();  string prefix="";
        try
        {
            if(AccountDetOnSearchprefix==null || AccountDetOnSearchprefix=="")
            prefix= "All";
            else
                prefix = AccountDetOnSearchprefix;

            dtDetail = DBHandler.GetResult("Load_AccountDescriptionOnSearch", prefix);

                if (dtDetail.Rows.Count > 0)
                {
                    grvVoucherDetail.DataSource = dtDetail;
                    grvVoucherDetail.DataBind();
                }

       }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulatePFBankReceipt(string PFBankReceiptEditMode)
    {
        DataTable dtDetail = new DataTable(); 
        try
        {
            if (PFBankReceiptEditMode == "PageLoad")
            {
                dtDetail = DBHandler.GetResult("Get_PFBankReceiptDetail");
                if (dtDetail.Rows.Count > 0)
                {
                    PagingChangeValue = "PFBankReceipt";
                    gvPFBankReceipt.DataSource = dtDetail;
                    gvPFBankReceipt.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulatePFPaymentDetailforEmp(DataTable dtPFPaymentDet)
    {
        DataTable dtDetail = new DataTable(); int TotalAmount = 0;
        try
        {
            dtPFPaymentDet.Columns.Add("SlNo");
            if (dtPFPaymentDet.Rows.Count > 0)
            {
                for (int i = 0; i < dtPFPaymentDet.Rows.Count; i++)
                {
                    dtPFPaymentDet.Rows[i]["SlNo"] = i + 1;
                }
                gvPFPaymentDetail.DataSource = dtPFPaymentDet;
                gvPFPaymentDetail.DataBind();

                for (int j = 0; j < dtPFPaymentDet.Rows.Count; j++)
                {
                    TotalAmount = TotalAmount + Convert.ToInt32(dtPFPaymentDet.Rows[j]["DisburseAmt"]);
                }

                //int TotalEmp = dtPFPaymentDet.AsEnumerable().Sum(row => row.Field<Int32>("DisburseAmt"));
                gvPFPaymentDetail.FooterRow.Cells[2].Text = "Total";
                gvPFPaymentDetail.FooterRow.Cells[2].Font.Bold = true;
                gvPFPaymentDetail.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                gvPFPaymentDetail.FooterRow.BackColor = System.Drawing.Color.Aqua;

                gvPFPaymentDetail.FooterRow.Cells[3].Text = TotalAmount.ToString();
                gvPFPaymentDetail.FooterRow.Cells[3].Font.Bold = true;
                gvPFPaymentDetail.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;
                gvPFPaymentDetail.FooterRow.BackColor = System.Drawing.Color.Aqua;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulatePensionPaymentDetail(string PensionMode, string PensionModeEmpNo)
    {
        try
        {
            if (PensionMode == "PageLoad" && PensionModeEmpNo=="")
            {
                DataTable dtRepQry = DBHandler.GetResult("Get_PensionPaymentDetail", PensionModeEmpNo);
                if (dtRepQry.Rows.Count > 0)
                {
                    gvPensionPaymentDetail.DataSource = dtRepQry;
                    gvPensionPaymentDetail.DataBind();
                }
            }
            else if (PensionMode == "OnSearch" && PensionModeEmpNo != "")
            {
                DataTable dtRepQry = DBHandler.GetResult("Get_PensionPaymentDetail", PensionModeEmpNo);
                if (dtRepQry.Rows.Count > 0)
                {
                    gvPensionPaymentDetail.DataSource = dtRepQry;
                    gvPensionPaymentDetail.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void AddPensionPaymentAmount(DataTable dtPensionPaymentDetail)
    {
        int PropTotal = 0; 
        try
        {
            if (dtPensionPaymentDetail.Rows.Count>0)
            {
                grvAddPaymentDetail.DataSource = dtPensionPaymentDetail;
                grvAddPaymentDetail.DataBind();


                for (int i = 0; i < dtPensionPaymentDetail.Rows.Count; i++)
                {
                    PropTotal = PropTotal + Convert.ToInt32(dtPensionPaymentDetail.Rows[i]["ProportionAmount"].ToString());
                    //ItaxTotal = ItaxTotal + Convert.ToInt32(dtPensionPaymentDetail.Rows[i]["ItaxAmt"].ToString());
                }
                //Total = dtGPF.AsEnumerable().Sum(row => row.Field<Int32>("Total_Employee"));
                grvAddPaymentDetail.FooterRow.Cells[0].Text = "Total";
                grvAddPaymentDetail.FooterRow.Cells[0].Font.Bold = true;
                grvAddPaymentDetail.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                grvAddPaymentDetail.FooterRow.BackColor = System.Drawing.Color.Aqua;

                grvAddPaymentDetail.FooterRow.Cells[1].Text = PropTotal.ToString();
                grvAddPaymentDetail.FooterRow.Cells[1].Font.Bold = true;
                grvAddPaymentDetail.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                grvAddPaymentDetail.FooterRow.BackColor = System.Drawing.Color.Aqua;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

}