﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BillMaster.aspx.cs" Inherits="BillMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtBillN").val('');
            $('#txtBillDate').val('');
            $('#ddlSector').val(0);
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
        });
        $(function () {
            $("[id$=txtBillDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });

        function ChangeDate(datee) {
            var d = new Date(datee);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = day + "/" + month + "/" + year;
            return date;
        }
/*=================================================================================================*/
        $(document).ready(function () {
            $('#txtBillDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow'
            });
            $("#btnBillDate").click(function () {
                $('#txtBillDate').datepicker("show");
                return false;
            });
        });
        /*=================================================================================================*/
        var hdnSaveorUpdate = '';
        var hdnBillSlNofor = '';
        var hdnPayMonthID = '';

        $(document).ready(function () {
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        //alert(E);
                        var options = {};
                        options.url = "BillMaster.aspx/GetCenter";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            var PayMonID = t[0]["PayMonthsID"];
                            var PayMon = t[0]["PayMonths"];
                            var BillSl = t[0]["BillSl"];
                            var BillNo = t[0]["BillNo"];
                            var BillDate = t[0]["BillDate"];
                            var SaveorUpdate = t[0]["SaveorUpdate"];

                            $('#txtSalMonth').val(PayMon);
                            $('#txtBillN').val(BillNo);
                            if (BillDate != "") {
                                $('#txtBillDate').val(ChangeDate(BillDate));
                            }
                            else {
                                $('#txtBillDate').val('');
                            }
                           // $('#hdnSaveorUpdate').val(SaveorUpdate);
                          //  $('#hdnBillSlNo').val(BillSl);
                           // $('#hdnPayMonID').val(PayMonID);
                            hdnSaveorUpdate = SaveorUpdate;
                            hdnBillSlNofor = BillSl;
                            hdnPayMonthID = PayMonID;
                        };
                        options.error = function () { alert("Error in retrieving SalMonth!"); };
                        $.ajax(options);
                    }
                }
                else {
                    $('#txtSalMonth').val('');
                    $('#txtBillN').val('');
                    $('#txtBillDate').val('');
                }
            });
        });
/*=================================================================================================*/
        $(document).ready(function () {
            $("#cmdSave").click(function () {
                var s = $('#ddlSector').val();
                if (s != 0) {
                    var SaveorUpdate = hdnSaveorUpdate;
                        //$("#hdnSaveorUpdate").val();

                    if (SaveorUpdate == "Save") {
                        var j = confirm("Are you sure you want to Save this ?");
                        if (j == true) {
                            //var PayMontID = $('#hdnPayMonID').val();
                            var PayMontID = hdnPayMonthID;
                            var SecID = $('#ddlSector').val();
                            //var SalMonth = $('#txtSalMonth').val();
                            var BillNo = $('#txtBillN').val();
                            var BillDate = $('#txtBillDate').val();

                            var C = "{SecID: '" + SecID + "', BillNo: '" + BillNo + "',BillDate: '" + BillDate + "', PayMontID: '" + PayMontID + "'}";

                            $.ajax({
                                type: "POST",
                                url: 'BillMaster.aspx/Insert_BillMaster',
                                data: C,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    var data = jQuery.parseJSON(D.d);
                                    alert(data);
                                    if (data = "success") {
                                        alert("Bill Master is Saved Successfully.");
                                    }
                                    else {

                                    }
                                },
                                error: function (response) {
                                    // alert('h');
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        }
                    }
                    else {
                        var j = confirm("Are you sure you want to Update this ?");
                        if (j == true) {
                            //var PayMontID = $('#hdnPayMonID').val();
                            var PayMontID = hdnPayMonthID;
                            var SecID = $('#ddlSector').val();
                            var SalMonth = $('#txtSalMonth').val();
                          //  var BillSl = $('#hdnBillSlNo').val();
                            var BillSl = hdnBillSlNofor;
                            var BillNo = $('#txtBillN').val();
                            var BillDate = $('#txtBillDate').val();

                            var C = "{SecID: '" + SecID + "', SalMonth: '" + SalMonth + "',BillNo: '" + BillNo + "',BillDate: '" + BillDate + "', BillSl: '" + BillSl + "',PayMontID: '" + PayMontID + "'}";
                            $.ajax({
                                type: "POST",
                                url: 'BillMaster.aspx/Update_BillMaster',
                                data: C,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    var data = jQuery.parseJSON(D.d);
                                    //alert(data);
                                    if (data = "success") {
                                        alert("Bill Master is Updated Successfully.");
                                    }
                                    else {

                                    }
                                },
                                error: function (response) {
                                    // alert('h');
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        }
                    }
                }
                else {
                    alert("Please Select Sector First.");
                    $('#ddlSector').focus();
                }
            });
            });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">

    <%--<asp:HiddenField ID="hdnPayMonID" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnBillSlNo" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnSaveorUpdate" runat="server" ClientIDMode="Static" Value="" />--%>

    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Bill  Master</h2>
                    </section>

                   <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <div id="tabs-1" style="float: left;">
                                                    <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Salary Month&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    :
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtSalMonth" ClientIDMode="Static" runat="server" CssClass="textbox" Enabled="false" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                              </tr>
                                                            <tr>
                                                               
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bill No.&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    :
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBillN" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                        
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                               
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bill Date&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    :
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBillDate" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                     
                                                                    <asp:ImageButton ID="btnBillDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                                        AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                                                </td>
                                                                
                                                            </tr>
                                                        </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" width="100%">
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px;">
                                                <span class="require">*</span> indicates Mandatory Field
                                            </td>
                                            <td style="padding: 5px;">&nbsp;
                                            </td>
                                            <td style="padding: 5px;" align="left">
                                                <div style="float: left; margin-left: 200px;">
                                                    <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" Width="100px"
                                                        CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' />
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Refresh" Width="100px" CssClass="Btnclassname"
                                                        OnClick="cmdCancel_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

