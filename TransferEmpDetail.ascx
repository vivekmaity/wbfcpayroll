﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransferEmpDetail.ascx.cs" Inherits="TransferEmpDetail" %>

<link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        .SubTotalRowStyle{
            border:solid 1px Black;
            background-color:#F5A9BC;
            font-weight:bold;
        }
        .GrandTotalRowStyle{
            border:solid 1px Black;  
            background-color:Gray;
            font-weight:bold;
        }
         .GroupHeaderStyle{
            border:solid 1px Black;
            background-color:#81BEF7;
            font-weight:bold;
        }
        .Spliter
        {
          border-left:none;
          border-right:none;
          background-color:Aqua;
        }
        .transparent
        {
            visibility:hidden;
            width:0px;
        }
        .SetWidth
        {
            width:0px;
        }
    </style>

<form id="form1" runat="server">
    <div>
    <asp:GridView ID="grdViewEmp" runat="server" align="center" AutoGenerateColumns="False" ClientIDMode="Static"
        Width="100%" DataKeyNames="EmpNo"  CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
         <AlternatingRowStyle BackColor="#FFFACD" />

        <Columns>
            <asp:TemplateField  Visible="true">
                <ItemTemplate>
                    <asp:CheckBox CommandName='Select' runat="server" ID="chkSelect" CommandArgument='<%# Eval("EmpNo") %>' /> 
                    <asp:HiddenField ID="hdnEmpNo" ClientIDMode="Static" runat="server" Value='<%# Eval("EmpNo") %>' />                                    
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="SectorID" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSectorID" runat="server" Visible="true" Text='<%# Bind("SectorID") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"  />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CenterID" >
                <ItemTemplate>
                    <asp:Label ID="lblCenterID" runat="server"   Text='<%# Bind("CenterID") %>'></asp:Label>
                    <asp:HiddenField ID="hdnCenterID" ClientIDMode="Static" runat="server" Value='<%# Eval("CenterID") %>' />   
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"  />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpName" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Designation" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDesignatione" runat="server" Visible="true" Text='<%# Bind("Designation") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"  />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Previous Sector" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSectorName" runat="server" Visible="true" Text='<%# Bind("SectorName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Previous Location" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblCenterName" runat="server" Visible="true" Text='<%# Bind("CenterName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="#F7F7DE" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" />
        <FooterStyle BackColor="#CCCC99" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" />
        <AlternatingRowStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
    </asp:GridView>
</div>
    <div>
   <%-- <div id="lblPrint"> <a onclick="window.print();" style="cursor:pointer; color:Blue;"  id="print">Print</a></div>--%>
    <asp:GridView ID="grdEmp" runat="server" align="center" AutoGenerateColumns="False" ClientIDMode="Static"
        Width="100%" DataKeyNames="EmpNo"  CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
         <AlternatingRowStyle BackColor="#FFFACD" />

        <Columns>

            <asp:TemplateField HeaderText="SectorID" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblSectorID" runat="server" Visible="false" Text='<%# Bind("SectorID") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CenterID" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblCenterID" runat="server" Visible="false" Text='<%# Bind("CenterID") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpName" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Designation" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDesignatione" runat="server" Visible="true" Text='<%# Bind("Designation") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"  />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Previous Sector" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSectorName" runat="server" Visible="true" Text='<%# Bind("SectorName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Previous Location" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblCenterName" runat="server" Visible="true" Text='<%# Bind("CenterName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="#F7F7DE" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" />
        <FooterStyle BackColor="#CCCC99" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" />
        <AlternatingRowStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
    </asp:GridView>
    </div>
<%--    <div id="pgingFooter">
        <a href="javascript:void(0);" id="prev">Previous</a> &nbsp;|&nbsp; <a href="javascript:void(0);"
            id="next">Next</a>
    </div>--%>

    <div>
    <asp:GridView ID="gvTransferEmpDetails" runat="server" align="center" AutoGenerateColumns="False" ClientIDMode="Static"
        Width="100%" DataKeyNames="EmpNo"  CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
         <AlternatingRowStyle BackColor="#FFFACD" />

        <Columns>

            <asp:TemplateField HeaderText="Emp No" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Name" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="From" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblFrom" runat="server" Visible="true" Text='<%# Bind("FROM") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="To" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblTo" runat="server" Visible="true" Text='<%# Bind("TO") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Visible="true" Text='<%# Bind("STATUS") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>

        <div>
    <asp:GridView ID="gvLocationNotAvailable" runat="server" align="center" AutoGenerateColumns="False" ClientIDMode="Static"
        Width="100%" DataKeyNames="EmpNo"  CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
         <AlternatingRowStyle BackColor="#FFFACD" />

        <Columns>

            <asp:TemplateField HeaderText="Emp No" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sector Name" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSectorName" runat="server" Visible="true" Text='<%# Bind("SectorName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sector Name" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSectorName" runat="server" Visible="true" Text='<%# Bind("status") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>


    
    <div>
        <asp:GridView ID="gvPFMoreorLess" runat="server" align="center" AutoGenerateColumns="False" ClientIDMode="Static"
            Width="100%" DataKeyNames="EmpNo" CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
            <AlternatingRowStyle BackColor="#FFFACD" />
            <Columns>
                <asp:TemplateField HeaderText="Emp No" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Emp Name" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" Visible="true" Text='<%# Bind("Name") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Basic Pay" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblBasicpay" runat="server" Visible="true" Text='<%# Bind("Basicpay") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PF" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblPF" runat="server" Visible="true" Text='<%# Bind("PF") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MinPF" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblMinPF" runat="server" Visible="true" Text='<%# Bind("MinPF") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</form>
