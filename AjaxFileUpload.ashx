﻿<%@ WebHandler Language="C#" Class="AjaxFileUpload" %>

using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Runtime.Serialization.Json;


public class AjaxFileUpload : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            string picsFolder = context.Request.QueryString["picsFolder"];
            string foldername = "";
            //string orgFileFolder = "";

            foldername = "EmployeePhoto";




            string path = context.Server.MapPath("~/" + foldername);
            //string orgPath = context.Server.MapPath("~/" + orgFileFolder);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            //if (!Directory.Exists(orgPath))
            //    Directory.CreateDirectory(orgPath);

            var file = context.Request.Files[0];
            string msg = "";
            System.Drawing.Image imgPhotoVert = System.Drawing.Image.FromStream(context.Request.Files[0].InputStream);
            System.Drawing.Image imgPhotoOrg = imgPhotoVert;
            bool flag = false;
            if (picsFolder == "UploadPhoto")
            {
                //if (file.ContentLength >= 50000)//file.ContentLength >= 20000 &&
               // {
                if (imgPhotoVert.Width >= 138 && imgPhotoVert.Height >= 177)
                    {
                    flag = true;
                    //}
                    //else
                    //{
                    //    flag = false;
                    }

                //}
                else
                {
                    flag = false;
                }

            }
            if (picsFolder == "UploadSign")
            {
               // if (file.ContentLength <= 100)//file.ContentLength >= 10000 &&
               // {
                if (imgPhotoVert.Width >= 250 && imgPhotoVert.Height >= 45)
                    {
                    flag = true;
                    //}
                    //else
                    //{
                    //    flag = false;
                    }
                //}
                else
                {
                    flag = false;
                }
            }

            //if (file.ContentLength <= 60000)
            if (flag)
            {

                ReSizeImage res = new ReSizeImage();
                string fName = "";
                System.Drawing.Image imgPhoto1 = null;
                if (picsFolder == "UploadPhoto")
                {
                    fName = "IMGPH" + DateTime.Now.ToString("yyMMddhhmmss");
                    if (imgPhotoVert.Width >= 138 && imgPhotoVert.Height >= 177)
                    {
                        imgPhoto1 = res.FixedSize(imgPhotoVert, 138, 177, System.Drawing.Color.White);
                    }
                    else
                    {
                        imgPhoto1 = imgPhotoVert;
                        imgPhoto1 = res.FixedSize(imgPhotoVert, imgPhotoVert.Width, imgPhotoVert.Height, System.Drawing.Color.White); ;
                    }

                }
                else if (picsFolder == "UploadSign")
                {
                    fName = "IMSIG" + DateTime.Now.ToString("yyMMddhhmmss");
                    if (imgPhotoVert.Width >= 250 && imgPhotoVert.Height >= 45)
                    {
                        imgPhoto1 = res.FixedSize(imgPhotoVert, 250, 45, System.Drawing.Color.White);
                    }
                    else
                    {
                        imgPhoto1 = res.FixedSize(imgPhotoVert, imgPhotoVert.Width, imgPhotoVert.Height, System.Drawing.Color.White); ;
                    }
                }

                res = null;

                string fileName;


                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                {
                    string[] files = file.FileName.Split(new char[] { '\\' });
                    fileName = files[files.Length - 1];
                }
                else
                {
                    fileName = file.FileName;
                }
                //if (HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID] != null)
                //{
                //    fileName = Path.GetFileNameWithoutExtension(fileName).Replace(" ", "_") + "_ApplicantID_" + HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID] + Path.GetExtension(fileName);
                //}
                //else
                //{
                //    fileName = Path.GetFileNameWithoutExtension(fileName).Replace(" ", "_") + "_ApplicantID_" + HttpContext.Current.Session.SessionID + Path.GetExtension(fileName);
                //}


                /************************* Filename of the uploaded image changed to imgPhot and imgSign accordingly ****************/

                fileName = Path.GetFileNameWithoutExtension(fileName).Replace(" ", "").Replace("'", "") + "_U" + ".jpg";//Path.GetExtension(fileName);
                fileName = fName + "_U" + ".jpg";//Path.GetExtension(fileName);
                try
                {
                    string OrgFileName = fileName;
                    //OrgFileName = Path.Combine(orgPath, OrgFileName);
                    System.IO.File.Delete(OrgFileName);
                    imgPhotoOrg.Save(OrgFileName);
                }
                catch (Exception err)
                {

                }
                string strFileName = fileName;
                fileName = Path.Combine(path, fileName);
                System.IO.File.Delete(fileName);
                imgPhoto1.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);


                //file.SaveAs(fileName);


                msg = "{";
                msg += string.Format("error:'{0}',\n", string.Empty);
                msg += string.Format("msg:'{0}'\n", foldername + "/" + strFileName);
                msg += "}";

            }
            else
            {
                msg = "{";
                msg += string.Format("error:'{0}',\n", "File Size is not within the specified range.");
                msg += string.Format("msg:'{0}'\n", String.Empty);
                msg += "}";
            }

            context.Response.Write(msg);


        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}