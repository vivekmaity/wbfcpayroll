﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BranchMaster.aspx.cs" Inherits="BranchMaster" %>

<%@ Register TagPrefix="art" TagName="BranchList" Src="GridviewTemplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    <style>
        .FooterStyle
        {
            /*background-color:#F0F0F0;*/
            font-weight: bold;
            color: #029A07;
            font-family: Verdana;
            font-size: 15px;
        }

        .uppercase
        {
            text-transform: uppercase;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtBranch").keyup(function () {
                var $th = $(this);
                $th.val($th.val().replace(/(\s{2,})|[^a-zA-Z 0-9 ,.-]/g, ' '));
                $th.val($th.val().replace(/^\s*/, ''));
              });
            $("#txtBranch").blur(function () {
                var $th = $(this);
                $th.val($th.val().trim());
            });
        });

        $(document).ready(function () {
            $('#txtMICR,#txtIFSC,#txtBranch').bind("cut copy paste", function (e) {
                e.preventDefault();
            });

            $("#txtMICR").change(function () {
                var name = $(this).val();
                var dname_without_space = $("#txtMICR").val().replace(/ /g, "");
                var name_without_special_char = dname_without_space.replace(/[^a-zA-Z 0-9]+/g, "");
                $(this).val(name_without_special_char);
            });
          
            $("#txtIFSC").change(function () {
                var name = $(this).val();
                var dname_without_space = $("#txtIFSC").val().replace(/ /g, "");
                var name_without_special_char = dname_without_space.replace(/[^a-zA-Z 0-9]+/g, "");
                $(this).val(name_without_special_char);
            });

        });
        function AllowIFSC() {
            var ifsc = $("#txtIFSC").val();
            if (ifsc != '') {
                var reg = /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/;

                if (ifsc.match(reg)) {
                    return true;
                }
                else {
                    alert("You Entered Wrong IFSC Code \n\n ------ or------ \n\n IFSC code should be count 11 \n\n-> Starting 4 should be only alphabets[A-Z] \n\n-> Remaining 7 should be accepting only alphanumeric");
                    $("#txtIFSC").val('');
                    return false;
                }

            }
        }
        //$(document).ready(function () {

        //    $("#cmdSave").click(function (event) {
        //        beforeSave();
        //    });
        //});
        //function beforeSave() {
        //    $("#form1").validate();
        //    alert("aaaaaa");
        //    $("#txtBranch").rules("add", { required: true, messages: { required: "Please enter Branch Name" } });
        //    $("#txtMICR").rules("add", { required: true, messages: { required: "Please enter Bank MICR" } });
        //    $("#txtIFSC").rules("add", { required: true, messages: { required: "Please enter Bank IFSC" } }); 
        //    if ($("#ddlBank").val() == "") {

        //        alert("Please select Bank Type!"); return false;
        //    }
        //}
        //function beforeCreate() {
        //    var n = document.getElementById('txtBranch').value;
        //    if (n.length < 1) {
        //        window.alert("Field is blank");
        //        return false;
        //    }
        //}
        $(document).ready(function () {
            $("#txtBranch").autocomplete({
                source: function (request, response) {
                    var E = "";
                    var text = request.term; //alert(tex);
                    if (text != "") {
                        var condition = "OnTextChange";
                        var BankName = "";
                        var BankID = $("#ddlBank").val();
                        if (BankID != "") {
                            E = "{prefix:'" + request.term + "', BankID:'" + $("#ddlBank").val() + "', BankName:'" + BankName + "', BranchCondition:'" + condition + "'}";
                            //else {
                            //    var BranchCondition = "AllBank";
                            //    E = "{prefix:'" + request.term + "', , BankID:'" + 0 + "', BankName:'" + BankName + "', BranchCondition:'" + BranchCondition + "'}";
                            //}
                            //alert(E);
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/AjaxGetGridCtrl',
                                data: E,
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    var item = data.d; //alert(item);
                                    $("#gvtbl").html(item);
                                },
                                error: function (response) {
                                    alert('Some Error Occur');
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        }
                        else { }
                    }
                    else {
                        var BankID = $("#ddlBank").val();
                        if (BankID != "") {
                            var E = "{BankID: '" + $('#ddlBank').val() + "', BankName: '" + $('#ddlBank').find('option:selected').text() + "'}";
                            //alert(E);
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/GetBranchDetailbyBank',
                                data: E,
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    var item = data.d; //alert(item);
                                    $("#gvtbl").html(item);
                                },
                                error: function (response) {
                                    alert('Some Error Occur');
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        }
                        else { }
                    }
                },
                minLength: 0
            });
        });
        var hdbankId = '';
        var hdbankname = '';
        $(document).ready(function () {
            $("#ddlBank").change(function () {
                var s = $('#ddlBank').val();
                if (s != "") {
                    if ($("#ddlBank").val() != "Please select") {
                        //// var E = "{BankID: '" + $('#ddlBank').val() + "', BankName: '" + $('#ddlBank').find('option:selected').text() + "'}";
                        //var E = "{BankID: '" + $('#ddlBank').val() + "', BankName: '" + $('#ddlBank').find('option:selected').text() + "'}";
                        ////alert(E);
                        //var options = {};
                        //options.url = "BranchMaster.aspx/GetBranchDetailbyBank";
                        //options.type = "POST";
                        //options.data = E;
                        //options.cache = false;
                        //options.dataType = "json";
                        //options.contentType = "application/json; charset=utf-8",
                        //options.success = function (data) {
                        //    var item = data.d; //alert(item);
                        //    $("#gvtbl").html(item);
                        //    $("#txtBranch").val('');
                        //    $("#txtIFSC").val('');
                        //    $("#txtMICR").val('');
                        //};
                        //options.error = function () { alert("Error in retrieving Center!"); };
                        //    $.ajax(options);
                        var PagingCondition = "OnBankSelection";
                        $("#hdnPagingCondition").val(PagingCondition);
                        // $("#hdnBankID").val($('#ddlBank').val());
                        hdbankId = $('#ddlBank').val();
                     //   $("#hdnBankName").val($('#ddlBank').find('option:selected').text());
                        hdbankname = $('#ddlBank').find('option:selected').text()

                        $.ajax({
                            type: 'POST',
                            url: 'BranchMaster.aspx/AjaxPaging',
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            success: function (data) {
                                $('#gvtbl').html(data.d);
                            },
                            data: "{pgIndex:'" + 0 + "', pgSize:'" + 10 + "', PagingCondition:'" + PagingCondition + "', BankID: '" + $('#ddlBank').val() + "', BankName: '" + $('#ddlBank').find('option:selected').text() + "'}"
                        });
                    }

                }
                else
                    $('#txtPayMonth').val('');

            });
        });
        //===============================================================================================================================================
        $(document).ready(function () {
            $('#worktype a').click('click', function () {
                flag = $(this).attr('id'); //alert(flag);

                var BankandBranchID = $(this).attr('itemid');
                var arr = BankandBranchID.split("#");
                var BranchID = arr[0];
                var BankID = arr[1];
                hdnBranchID=BranchID;
                SetBankID(1, hdnBranchID);
                var BranchName = $(this).closest('tr').find('td:eq(1)').text();
                var IFSC = $(this).closest('tr').find('td:eq(2)').text();
                var MICR = $(this).closest('tr').find('td:eq(3)').text();
                var BankName = $(this).closest('tr').find('td:eq(4)').text();
                //alert(BranchID); alert(BranchName);

                $("#ddlBank").val(BankID);
                $("#txtBranch").val(BranchName);
                $("#txtIFSC").val(IFSC);
                $("#txtMICR").val(MICR);
                $("#cmdSave").prop('value', 'Update');
            });
        });

        function SetBankID(type, hdnBranchID) {
            var W = "{type:" + type + ",BranchID:'" + hdnBranchID + "'}";
            $.ajax({
                type: "POST",
                url: "BranchMaster.aspx/GET_SetBranchID",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    return false;
                },
                error: function (result) {
                    alert("Something Missing...");
                    return false;
                }
            });
        }



        $(document).ready(function () {
            $('#cmdSave').click(function () {

                var buttonText = $("#cmdSave").attr("value"); //alert(buttonText);

                if (buttonText == "Create") {
                    var BankID = $("#ddlBank").val(); if (BankID == "") { alert("Please select bank Name"); $("#ddlBank").focus(); return false; }
                    var BranchName = $("#txtBranch").val(); if (BranchName == "") { alert("Please enter Branch Name"); $("#txtBranch").focus(); return false; }
                    var IFSC = $("#txtIFSC").val(); if (IFSC == "") { alert("Please enter Bank IFSC"); $("#txtIFSC").focus(); return false; }
                    //if (MICR == "") { alert("Please enter Bank MICR"); $("#txtMICR").focus(); return false; }
                    var MICR = $("#txtMICR").val(); 
                    var K = "{BankID: '" + BankID + "',BranchName: '" + BranchName + "',IFSC: '" + IFSC + "',MICR: '" + MICR + "'}";

                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Save_Baranch',
                        data: K,
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            //var item = data.d; alert(item);
                            alert('Branch Details are Inserted Successfully !');

                            var condition = "PageLoad";
                            var BankName = "";
                            var E = "{prefix:'" + 0 + "', BankID:'" + 0 + "', BankName:'" + BankName + "', BranchCondition:'" + condition + "'}";
                            //alert(E);
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/AjaxGetGridCtrl',
                                data: E,
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    var item = data.d; //alert(item);
                                    $("#gvtbl").html(item);
                                    window.location.href = "BranchMaster.aspx";
                                }
                            });
                        }
                    });
                }
                else {
                    var BranchID = hdnBranchID;//$("#hdnBranchID").val();
                    var BankID = $("#ddlBank").val(); if (BankID == "") { alert("Please select bank Name"); $("#ddlBank").focus(); return false; }
                    var BranchName = $("#txtBranch").val(); if (BranchName == "") { alert("Please enter Branch Name"); $("#txtBranch").focus(); return false; }
                    var IFSC = $("#txtIFSC").val(); if (IFSC == "") { alert("Please enter Bank IFSC"); $("#txtIFSC").focus(); return false; }
                    var MICR = $("#txtMICR").val(); if (MICR == "") { alert("Please enter Bank MICR"); $("#txtMICR").focus(); return false; }


                    var E = "{BranchID: '" + BranchID + "',BranchName: '" + BranchName + "',IFSC: '" + IFSC + "',MICR: '" + MICR + "',BankID: '" + BankID + "'}";
                    //alert(E);
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/UpdateBranch',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var item = data.d; //alert(item);
                            if (item == 'Success')
                                alert('Branch Details are Updated Successfully !');
                            $("#cmdSave").prop('value', 'Create');

                            $("#ddlBank").val('');
                            $("#txtBranch").val('');
                            $("#txtIFSC").val('');
                            $("#txtMICR").val('');

                            var condition = "PageLoad";
                            var BankName = "";
                            var E = "{prefix:'" + 0 + "', BankID:'" + 0 + "', BankName:'" + BankName + "', BranchCondition:'" + condition + "'}";
                            //alert(E);
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/AjaxGetGridCtrl',
                                data: E,
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    var item = data.d; //alert(item);
                                    $("#gvtbl").html(item);
                                    window.location.href = "BranchMaster.aspx";

                                }
                            });
                        }
                    });
                }
            });

        });


        $(document).ready(function () {
            $('#cmdCancel').click(function () {
                window.location.href = "BranchMaster.aspx";
            });
        });
        /*----------------------------------------------------------------------------------------------------------------------*/
        var txtPgSize = '10';
        var txtPgIndex = '0';
        var hdnBranchID = '';
        $(document).ready(function () {

            $('#pgingFooter a').click('click', function () {
                flag = $(this).attr('id'); //alert(flag);
                FillGrid(flag);
            });

        });

        function FillGrid(flag) {

            var pgIndex = 0;
            var pgCurrentIndex = txtPgIndex;
            //var pgCurrentIndex = $('#txtPgIndex').attr('value');
            //   var pgSize = $('#txtPgSize').attr('value');
            var pgSize = txtPgSize;
            //  var PagingCondition = $("#hdnPagingCondition").val();
            var PagingCondition = "OnBankSelection";
            var BankID = hdbankId;
            var BankName = hdbankname;
            //alert(flag);
            switch (flag) {
                case 'prev':
                    pgIndex = parseInt(pgCurrentIndex) - 1; //alert(pgIndex);
                    if (pgIndex == 0)
                        $("#prev").prop("disabled", true);
                    else
                        $("#prev").prop("disabled", false);
                    break;
                case 'next':
                    pgIndex = parseInt(pgCurrentIndex) + 1;
                    if (pgIndex > 0)
                        $("#prev").prop("disabled", false);
                    else

                        break;
            }

            $('#txtPgIndex').attr('value', pgIndex);
            var E = "{pgIndex:'" + pgIndex + "', pgSize:'" + pgSize + "', PagingCondition:'" + PagingCondition + "', BankID: '" + BankID + "', BankName: '" + BankName + "'}";
            //alert(E);
            $.ajax({
                type: 'POST',
                url: 'BranchMaster.aspx/AjaxPaging',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    $('#gvtbl').html(data.d);
                },
                data: E
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" runat="Server">
    <%--<asp:HiddenField ID="hdnBranchID" ClientIDMode="Static" runat="server" Value="" />
    <asp:HiddenField ID="hdnPagingCondition" ClientIDMode="Static" runat="server" Value="" />
    <asp:HiddenField ID="hdnBankID" ClientIDMode="Static" runat="server" Value="" />
    <asp:HiddenField ID="hdnBankName" ClientIDMode="Static" runat="server" Value="" />
    <asp:HiddenField ID="hdnFalsePaging" ClientIDMode="Static" runat="server" Value="" />--%>
    <div>
      <%--  <input id="txtPgSize" type="hidden" value="10" />
        <input id="txtPgIndex" type="hidden" value="0" />--%>
    </div>
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Bank Branch  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlBank" Width="220px" Height="23px" runat="server" DataSource='<%#drpload("Bank") %>' DataValueField="BankID" AutoPostBack="false" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bank Branch Name &nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBranch" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>

                                                                </td>
                                                            </tr>
                                                            <tr>

                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">IFSC&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtIFSC" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                        MaxLength="11" onblur="return AllowIFSC();" Style='text-transform: uppercase' autocomplete="off"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">MICR&nbsp;&nbsp;</span><span class="require"></span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtMICR" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                        MaxLength="20" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </InsertItemTemplate>
                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlBank" Width="220px" Height="22px" runat="server" DataSource='<%#drpload("Bank") %>' DataValueField="BankID" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Bank)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bank Branch Name &nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBranch" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>

                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">IFSC&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtIFSC" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("IFSC") %>'
                                                                       MaxLength="11" onblur="return AllowIFSC();" Style='text-transform: uppercase' autocomplete="off"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">MICR&nbsp;&nbsp;</span><span class="require"></span>
                                                                </td>
                                                                <td style="padding: 5px;">:
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtMICR" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("MICR") %>'
                                                                        MaxLength="20" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="require">*</span> indicates Mandatory Field
                                                                </td>
                                                                <td style="padding: 5px;">&nbsp;
                                                                </td>
                                                                <%-- OnClientClick='javascript: return beforeSave();'--%>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" OnClientClick='javascript: return beforeCreate();' CommandName="Add" Width="100px"
                                                                            CssClass="Btnclassname DefaultButton" />
                                                                        <asp:Button ID="cmdCancel" runat="server" ClientIDMode="Static" Text="Refresh" Width="100px" CssClass="Btnclassname DefaultButton" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="gvtbl" clientidmode="Static" runat="server" style="overflow-x: auto; max-height: 250px; width: 100%">
                                                </div>
                                                <%-- <div id="pgingFooter" class="FooterStyle">
                                                    <a href="javascript:void(0);"  id="prev">Previous</a> &nbsp;&nbsp;<a href="javascript:void(0);" id="next">Next</a>
                                                </div>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>

