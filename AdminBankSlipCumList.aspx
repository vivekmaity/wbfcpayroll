﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AdminBankSlipCumList.aspx.cs" Inherits="AdminBankSlipCumList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        
        $(document).ready(function () {
            $('#myTable').hide();
        });

        function opentab() {
            var SalMonth;
            var SalMonthID;
            var ReportType;
            var SalFinYear;
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            var ReportType1 = '';
            //alert("hii");
            FormName = "AdminBankSlipCumList.aspx";
            ReportName = 'Admin_bank_slip_cum_list_running';   //Admin_bank_slip_cum_list
            ReportType = 'Admin bank slip cum list';
            //SalMonthID = $('#hdnSalMonthID').val();
            SalMonth = $('#txtPayMonth').val();
            SalMonthID = 0;
            ReportType1 = $("#ddlReportType").val();
            SalFinYear = $("#txtSalFinYear").val();
            if (SalMonth == "") {
                alert("Salary Month Invalid");
                return false;
            }

            if (ReportType1 == "0") {
                alert("Please select Report Type");
                $('#ddlReportType').focus();
                return false;
            }

            if (ReportType1 == "1" && hdnRadiobtntxt == "") {
                alert("Please Check RadioButton for Report Type Format.");
                return false;
            }
            if (ReportType1 == "2" && hdnRadiobtntxt == "") {
                alert("Please Check RadioButton for Report Type Format.");
                return false;
            }

            if (hdnRadiobtntxt == "PDF" && ReportType1 == "1") {
                var D = "{SalMonthID:'" + SalMonthID + "'}";
                $.ajax({
                    type: "POST",
                    url: "AdminBankSlipCumList.aspx/Check_All_bank",
                    data: D,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        if (jsmsg == "N") {
                            var result = confirm("You can not generate Bank Slip.\nTo view details press 'Ok'");
                            if (result == true)
                                window.open("SalaryGenerationInfo.aspx");
                        }
                        else {
                            //alert(ReportName);
                            var E = "{SalMonth:'" + SalMonth + "',SalMonthID:" + SalMonthID + ",SalFinYear:'" + SalFinYear + "',FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                            $(".loading-overlay").show();
                            $.ajax({
                                type: "POST",
                                url: "AdminBankSlipCumList.aspx/Report_Paravalue",
                                data: E,
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (msg) {
                                    jsmsg = JSON.parse(msg.d);
                                    window.open("ReportView1.aspx");
                                    $(".loading-overlay").hide();
                                }
                            });
                        }
                    }
                });
            }
            else if (hdnRadiobtntxt == "Excel" && ReportType1 == "1") {
                var D = "{SalMonthID:'" + SalMonthID + "'}";
                $.ajax({
                    type: "POST",
                    url: "AdminBankSlipCumList.aspx/Check_All_bank",
                    data: D,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        if (jsmsg == "N") {
                            var result = confirm("You can not generate Bank Slip.\nTo view details press 'Ok'");
                            if (result == true)
                                window.open("SalaryGenerationInfo.aspx");
                        }
                        else {
                            var ReportName = "AdminBankSlipCumList";
                            //alert(ReportName);
                            window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "");
                            return false;
                        }
                    }
                });
            }
            else if (hdnRadiobtntxt == "PDF" && ReportType1 == "2") {
                //var D = "{SalMonthID:'" + SalMonthID + "'}";
                //$.ajax({
                //    type: "POST",
                //    url: "AdminBankSlipCumList.aspx/Check_All_bank",
                //    data: D,
                //    dataType: "json",
                //    contentType: "application/json; charset=utf-8",
                //    success: function (msg) {
                //        jsmsg = JSON.parse(msg.d);
                //        if (jsmsg == "N") {
                //            var result = confirm("You can not generate Bank Slip.\nTo view details press 'Ok'");
                //            if (result == true)
                //                window.open("SalaryGenerationInfo.aspx");
                //        }
                //        else {
                //            //alert(ReportName);
                //            var E = "{SalMonth:'" + SalMonth + "',SalMonthID:" + SalMonthID + ",SalFinYear:'" + SalFinYear + "',FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                //            $(".loading-overlay").show();
                //            $.ajax({
                //                type: "POST",
                //                url: "AdminBankSlipCumList.aspx/Report_Paravalue",
                //                data: E,
                //                dataType: "json",
                //                contentType: "application/json; charset=utf-8",
                //                success: function (msg) {
                //                    jsmsg = JSON.parse(msg.d);
                //                    window.open("ReportView1.aspx");
                //                    $(".loading-overlay").hide();
                //                }
                //            });
                //        }
                //    }
                //});
                alert("Only Excel");

            }
            else if (hdnRadiobtntxt == "Excel" && ReportType1 == "2") {
                var D = "{SalMonthID:'" + SalMonthID + "'}";
                $.ajax({
                    type: "POST",
                    url: "AdminBankSlipCumList.aspx/Check_All_bank",
                    data: D,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        if (jsmsg == "N") {
                            var result = confirm("You can not generate Bank Slip.\nTo view details press 'Ok'");
                            if (result == true)
                                window.open("SalaryGenerationInfo.aspx");
                        }
                        else {
                            var ReportName = "AdminBankSlipCumListNewFormat";
                            window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "");
                            return false;
                        }
                    }
                });
            }

        }


        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


        //This is for RadioButton checked or unchecked
        var hdnRadiobtntxt = '';
        $(document).ready(function () {
            $('#rdPDF').click(function () {
                if (document.getElementById('rdExcel').checked) {
                    document.getElementById('rdExcel').checked = false;
                }
                var ReportTypes2 = "PDF";
                //$('#hdnRadioButtonText').val(ReportTypes2);
                hdnRadiobtntxt = ReportTypes2;
            });

            $('#rdExcel').click(function () {
                if (document.getElementById('rdPDF').checked) {
                    document.getElementById('rdPDF').checked = false;
                }
                var ReportTypes3 = "Excel";
               // $('#hdnRadioButtonText').val(ReportTypes3);
                hdnRadiobtntxt = ReportTypes3;
            });
        });
    </script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Admin Bank Slip Cum List</h2>						
						</section>
                        <asp:HiddenField ID="hdnRadioButtonText" ClientIDMode="Static" runat="server" Value="" />






                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" >
                                        <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>

                                        </td>
                                        <td style="padding:5px;" class="style3"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                        <asp:DropDownList ID="ddlReportType" Width="280px" Height="28px"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">Select Report Type</asp:ListItem>
                                            <asp:ListItem Value="1">Bank Slip</asp:ListItem>
                                            <asp:ListItem Value="2">Bank Slip Benificiary Wise</asp:ListItem>
                                        </asp:DropDownList>
                                            <asp:RadioButton ID="rdPDF" autocomplete="off" ClientIDMode="Static" runat="server" Text="PDF" Checked="false" GroupName="a" Value="pdf" />&nbsp;&nbsp;&nbsp;&nbsp;   
                                            <asp:RadioButton ID="rdExcel" autocomplete="off" ClientIDMode="Static" runat="server" Text="Excel" Checked="false" GroupName="a"  />     
                                        </td>
 
                                       <%--<asp:HiddenField ID="hdnSalMonthID" runat="server"  />--%>
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;" /> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>

                                      
                                        
                                    </div>
                                        <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                        </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

