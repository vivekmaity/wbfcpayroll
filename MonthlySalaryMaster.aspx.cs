﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Globalization;

public partial class MonthlySalaryMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dtstr = DBHandler.GetResult("Get_MST_MonthlySalary");
            tbl.DataSource = dtstr;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_MST_MonthlySalaryByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    DropDownList ddlEmployeeName = (DropDownList)dv.FindControl("ddlEmployeeName");
                    ddlEmployeeName.Enabled = false;
                    DropDownList ddlEmployeeSalary = (DropDownList)dv.FindControl("ddlEmployeeSalary");
                    ddlEmployeeSalary.Enabled = false;
                    ((DropDownList)dv.FindControl("ddlEmployeeName")).SelectedValue = dtResult.Rows[0]["EmployeeID"].ToString();
                    
                    ((DropDownList)dv.FindControl("ddlEmployeeSalary")).SelectedValue = dtResult.Rows[0]["SalMonthID"].ToString();
                    
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_MST_MonthlySalary", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtGrossPay = (TextBox)dv.FindControl("txtGrossPay");
                TextBox txtTotalDeduction = (TextBox)dv.FindControl("txtTotalDeduction");
                TextBox txtNetPay = (TextBox)dv.FindControl("txtNetPay");


                DropDownList ddlEmployeeName = (DropDownList)dv.FindControl("ddlEmployeeName");
                DropDownList ddlEmployeeSalary = (DropDownList)dv.FindControl("ddlEmployeeSalary");

                DataTable dtcheck = DBHandler.GetResult("Check_MST_MonthlySalary", Convert.ToInt32(ddlEmployeeName.SelectedValue), Convert.ToInt32(ddlEmployeeSalary.SelectedValue));
                if (dtcheck.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This employee already contains record for this salary month. Please update the record.')</script>");
                }
                else
                {
                    DBHandler.Execute("Insert_MST_MonthlySalary", Convert.ToInt32(ddlEmployeeName.SelectedValue), Convert.ToInt32(ddlEmployeeSalary.SelectedValue),
                        Convert.ToDouble(txtGrossPay.Text), Convert.ToDouble(txtTotalDeduction.Text), Convert.ToDouble(txtNetPay.Text),
                        Session[SiteConstants.SSN_INT_USER_ID]);
                    cmdSave.Text = "Add";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
                }
            }
            else if (cmdSave.CommandName == "Edit")
            {
                TextBox txtGrossPay = (TextBox)dv.FindControl("txtGrossPay");
                TextBox txtTotalDeduction = (TextBox)dv.FindControl("txtTotalDeduction");
                TextBox txtNetPay = (TextBox)dv.FindControl("txtNetPay");
                DropDownList ddlEmployeeName = (DropDownList)dv.FindControl("ddlEmployeeName");
                
                ddlEmployeeName.Enabled = false;
                DropDownList ddlEmployeeSalary = (DropDownList)dv.FindControl("ddlEmployeeSalary");
                ddlEmployeeName.Enabled = false;


                string ID = ((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("Update_MST_MonthlySalary", ID, Convert.ToDouble(txtGrossPay.Text), Convert.ToDouble(txtTotalDeduction.Text),
                    Convert.ToDouble(txtNetPay.Text),
                    Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void ddlLoanType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            PopulateLoanDesc();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void PopulateLoanDesc()
    {
        try
        {
            DropDownList ddlLoanType = (DropDownList)dv.FindControl("ddlLoanType");
            DropDownList ddlLoanDesc = (DropDownList)dv.FindControl("ddlLoanDesc");
            DataTable dt = DBHandler.GetResult("Get_LoanDesc", ddlLoanType.SelectedValue);
            ddlLoanDesc.Items.Clear();
            if (dt.Rows.Count > 0)
            {
                ddlLoanDesc.Items.Insert(0, new ListItem("(Select Loan Description)", "0"));
                ddlLoanDesc.DataSource = dt;
                ddlLoanDesc.DataTextField = "LoanDesc";
                ddlLoanDesc.DataValueField = "LoanTypeID";
                ddlLoanDesc.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload1()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_EmployeeDdl");
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected DataTable drpload2()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_SalaryMonth1");
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlEmployeeNameSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //PopulateLoanDesc();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}