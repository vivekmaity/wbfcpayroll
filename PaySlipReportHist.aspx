﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always"  CodeFile="PaySlipReportHist.aspx.cs" Inherits="PaySlipReportHist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        //This is for 'Location' Binding on the Base of 'Sector'
        $(document).ready(function () {
            $("#ddlSector").change(function () {      
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        var options = {};
                        options.url = "PaySlipReportHist.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;                   
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            //alert(JSON.stringify(t));
                            //var PayMon = t[0]["PayMonths"];
                            //var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                            $("#ddlPaymonthType").empty();
                            //$('#txtPayMonth').val(PayMon);
                            //$('#hdnSalMonthID').val(PayMonID);
                            if (a >= 0) {
                                $("#ddlPaymonthType").append("<option value=''>Select Sal Month</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlPaymonthType").append($("<option></option>").val(value.SalMonthID).html(value.SalMonth));
                                });
                            }
                            //if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                            //    document.getElementById("ddlLocation").disabled = false;

                            //}

                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }

                }
                else {
                    //$('#txtPayMonth').val('');
                    $("#ddlPaymonthType").empty();
                    $("#ddlPaymonthType").append("<option value=''>Select sal month</option>")
                    $("#ddlPaymonthType").val(0);
                }

            });

        });

        $(document).ready(function () {

            $("#ddlReportType").change(function () {
                if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '') {
                    document.getElementById("ddlLocation").disabled = false;

                    var s = $('#ddlSector').val();
                    if (s != 0) {
                        if ($("#ddlSector").val() != "Please select") {
                            var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                            var options = {};
                            options.url = "PaySlipReportHist.aspx/GetLocation";
                            options.type = "POST";
                            options.data = E;
                            options.dataType = "json";
                            options.contentType = "application/json";
                            options.success = function (listLocation) {
                                var t = jQuery.parseJSON(listLocation.d);
                                //alert(JSON.stringify(t));
                                //var PayMon = t[0]["PayMonths"];
                                //var PayMonID = t[0]["PayMonthsID"];
                                var a = t.length;
                                $("#ddlLocation").empty();
                                //$('#txtPayMonth').val(PayMon);
                                //$('#hdnSalMonthID').val(PayMonID);
                                if (a >= 0) {
                                    $("#ddlLocation").append("<option value='0'>Select Location</option>")
                                    $.each(t, function (key, value) {
                                        $("#ddlLocation").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                                    });
                                }
                                //if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                                //    document.getElementById("ddlLocation").disabled = false;

                                //}

                            };
                            options.error = function () { alert("Error in retrieving Location!"); };
                            $.ajax(options);
                        }

                    }
                    else {
                        //$('#txtPayMonth').val('');
                        $("#ddlLocation").empty();
                        $("#ddlLocation").append("<option value=''>Select Location</option>")
                        $("#ddlLocation").val(0);
                    }

                }
                //else {
                //    $("#ddlLocation").val(0);
                //    document.getElementById("ddlLocation").disabled = true;

                //}

            });
        });
        $(document).ready(function () {

            $("#ddlReportType").change(function () {
                if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '')
                {
                    document.getElementById("ddlLocation").disabled = false;
                }
                else
                {
                    $("#ddlLocation").val(0);
                    document.getElementById("ddlLocation").disabled = true;
                }
            });
        });

        function opentab() {
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            var StrFinYr = '';
            var StrSecID;
            var StrSalMonth;
            var StrSalMonthID;
            var StrCenID;
            //var ReportType = $("#ddlReportType").val();
            var StrReportTyp = '';
            StrReportTyp = $('#ddlReportType').val();
            StrSecID = $("#ddlSector").val();
            StrSalMonthID = $('#ddlPaymonthType').val();
            //StrSalMonth = $('#txtPayMonth').val();
            StrCenID = $('#ddlLocation').val();

          

            if (StrReportTyp == '0') {
                alert("please select Report Type");
                $('#ddlReportType').focus();
                return false;

            }
            if (StrReportTyp == '1') {
                ReportName = "History_PaySlip_new_LocationWISE_A5";

            }
            if (StrReportTyp == '2') {
                ReportName = "PaySummarySecWiseHistory";

            }
            if (StrReportTyp == '3') {
                ReportName = "PaySummarySecWiseLocationWise_History";

            }
            FormName = "PaySlipReportHist.aspx";

            ReportType = "PaySlipReportHist";
            $(".loading-overlay").show();
            var E = '';
            E = "{StrSecID:" + StrSecID + ",StrCenID:" + StrCenID + ",StrSalMonthID:" + StrSalMonthID + ", FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
            //alert(E);
            $.ajax({

                type: "POST",
                url: "PaySlipReportHist.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    $(".loading-overlay").hide();
                    window.open("ReportView1.aspx?");
                }
            });
            
        }
                
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>         
							<h2>Pay Report History</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  " bgcolor="#669999"  >
                    <tr>
                        <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" bgcolor="White" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" bgcolor="White" >
                                        <asp:DropDownList ID="ddlPaymonthType" Width="200px" Height="28px" runat="server"  CssClass="textbox"  Enabled="true"
                                            DataTextField="SalMonth" DataValueField="SalMonthID" AppendDataBoundItems="true" autocomplete="off">
                                            <asp:ListItem Text="Select Salary Month" Selected="True" Value="0"></asp:ListItem>
                                        </asp:DropDownList>

                                        </td>
                                        <td style="padding:5px;" class="style3" bgcolor="White"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;" bgcolor="White">:</td>
                                        <td style="padding:5px;" align="left" bgcolor="White" >
                                        <asp:DropDownList ID="ddlReportType" Width="200px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">(Select Report Type)</asp:ListItem>
                                            <asp:ListItem Value="1">Pay Slip</asp:ListItem>
                                            <asp:ListItem Value="2">Pay Summary</asp:ListItem>
                                            <asp:ListItem Value="3">Pay Summary Location Wise</asp:ListItem>
                                            <asp:ListItem Value="4">Pay Register</asp:ListItem>
                                        </asp:DropDownList>   
                                        </td>

                                         <td style="padding:5px;" class="style4" bgcolor="White"><span class="headFont">Location </span></td>
                                         <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" bgcolor="White" >
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="headFont" Width="230px" Height="28px" CssClass="textbox" DataValueField="CenterID" DataTextField="CenterName"
                                        AppendDataBoundItems="true" Enabled="false" ClientIDMode="Static" autocomplete="off">
                                        <asp:ListItem Text="Select Location" Selected="True" Value="0"></asp:ListItem>
                                        </asp:DropDownList>   
                                        </td>
                                         
                                       <asp:HiddenField ID="hdnSalMonthID" runat="server"  />
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" bgcolor="White" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;" /> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                        
                                    </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

