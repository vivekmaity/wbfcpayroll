﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="DAMaster.aspx.cs" Inherits="DAMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    <script src="js/ValidationTextBox.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtEffFrm').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow',
                onSelect: function (dates) {
                    var aa = $('#txtEffFrm').val();
                    var a = aa.split('/');
                    for (var i = 0; i < a.length; i++) {
                        date = a[0]; month = a[1]; year = a[a.length - 1];
                    }
                    $('#txtEffFrm').val("01" + "/" + month + "/" + year);

                }
            });

            $("#btnEFDatePicker").click(function () {
                $('#txtEffFrm').datepicker("show");
                return false;
            });

            $('#txtEffTo').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow'
            });

            $("#btnETDatePicker").click(function () {
                $('#txtEffTo').datepicker("show");
                return false;
            });

            $('#txtOrderDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow'
            });

            $("#btnOrderDate").click(function () {
                $('#txtOrderDate').datepicker("show");
                return false;
            });

            $(".dpDate").datepicker("option", "dateFormat", "dd/mm/yy");
            $(".calendar").click(function () {
                $(".dpdate").datepicker("show");
                return false;
            });
        });

        function beforeSave() {
            $("#form1").validate();
            $("#ddlEmpType").rules("add", { required: true, messages: { required: "Please Select Employee Type" } });
            $("#txtOrderNo").rules("add", { required: true, messages: { required: "Please Enter Order No." } });
            $("#txtOrderDate").rules("add", { required: true, messages: { required: "Please Enter Order Date" } });
            $("#txtDARate").rules("add", { required: true, messages: { required: "Please Enter DA Rate" } });
            $("#txtMaxAmt").rules("add", { required: true, messages: { required: "Please Enter DA Maximum Amount" } });
            $("#txtEffFrm").rules("add", { required: true, messages: { required: "Please Enter Effectie From Date" } });
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>DA Rate Master</h2>
                    </section>
                    <%--<asp:HiddenField ID="hdnValidReason" runat="server" Value="" ClientIDMode="Static"/>--%>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <%--<td style="padding:5px;" ><span class="headFont">DA Code &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtDACode" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3"></asp:TextBox>                            
                                        </td>--%>
                                                                <td style="padding: 5px;"><span class="headFont">Employee Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlEmpType" Width="220px" Height="24px" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEmpType_SelectedIndexChanged" AutoPostBack="true" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                                        <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                                        <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                                        <asp:ListItem Text="WBFC Employee" Value="K"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Order No. &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtOrderNo" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off" ></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Order Date&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">

                                                                    <nobr>
                                                <asp:TextBox ID="txtOrderDate" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" autocomplete="off"> </asp:TextBox>
                                                <asp:ImageButton ID="btnOrderDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">DARate &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtDARate" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Maximum Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtMaxAmt" ClientIDMode="Static" runat="server" CssClass="textbox" Text="10000000" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Effective From&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <nobr>
                                                <asp:TextBox ID="txtEffFrm" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnEFDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                                                </td>
                                                                <%--<td style="padding:5px;" ><span class="headFont">Effective To&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtEffTo" ClientIDMode="Static" runat="server" CssClass="dpDate inputbox2"></asp:TextBox>
                                                <asp:ImageButton ID="btnETDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>--%>
                                                            </tr>
                                                        </table>
                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <%--                                        <td style="padding:10px;"> <span class="headFont">DA Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtDACode" Text='<%# Eval("DACode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" Enabled="false"></asp:TextBox>                            
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("DAID") %>'/>
                                        </td>--%>
                                                                <td style="padding: 5px;"><span class="headFont">Employee Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlEmpType" Width="220px" Height="24px" runat="server" AppendDataBoundItems="true" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                                        <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                                        <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                                        <asp:ListItem Text="KMDA Employee" Value="K"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Order No. &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtOrderNo" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("OrderNo") %>' onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                    <%--<asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("DAID") %>' />--%>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Order Date&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">

                                                                    <nobr>
                                                <asp:TextBox ID="txtOrderDate" ClientIDMode="Static" runat="server" Text='<%# Eval("OrderDate") %>' CssClass="dpDate textbox" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnOrderDate" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">DA Rate&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtDARate" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("DARate") %>' onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Maximum Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtMaxAmt" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("MaxAmount") %>' onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Effective From&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <nobr>
                                                <asp:TextBox ID="txtEffFrm" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" Text='<%# Eval("EffectiveFrom") %>' autocomplete="off"></asp:TextBox>

                                                <asp:ImageButton ID="btnEFDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                                                </td>
                                                             <%--   <td style="padding: 5px;"><span class="headFont">Effective To&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <nobr>
                                                <asp:TextBox ID="txtEffTo" ClientIDMode="Static" runat="server" CssClass="dpDate textbox" Text='<%# Eval("EffectiveTo") %>'></asp:TextBox>
                                                <asp:ImageButton ID="btnETDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                                                </td>--%>
                                                            </tr>
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                                <td style="padding: 5px;">&nbsp;</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"
                                                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();'
                                                                            OnClick="cmdSave_Click" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Refresh"
                                                                            Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="overflow-y: scroll; height: auto; width: 100%">
                                                    <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                                        DataKeyNames="DAID" OnRowCommand="tbl_RowCommand" AllowPaging="false"
                                                        CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">

                                                        <AlternatingRowStyle BackColor="#FFFACD" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last"
                                                            Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />

                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="TableHeader" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                        runat="server" ID="btnEdit" CommandArgument='<%# Eval("DAID") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="TableHeader" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                        OnClientClick='return Delete();'
                                                                        CommandArgument='<%# Eval("DAID") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DAID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="DAID" />

                                                            <asp:TemplateField HeaderText="Emp Type" Visible="true" HeaderStyle-CssClass="TableHeader">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpType" runat="server" Visible="true" Text='<%# Bind("EmpType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>


                                                            <asp:BoundField DataField="OrderNo" HeaderStyle-CssClass="TableHeader" HeaderText="Order No" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="OrderDate" HeaderStyle-CssClass="TableHeader" HeaderText="Order Date" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="DARate" HeaderStyle-CssClass="TableHeader" HeaderText="DA Rate" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="MaxAmount" HeaderStyle-CssClass="TableHeader" HeaderText="Maximum Amount" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="EffectiveFrom" HeaderStyle-CssClass="TableHeader" HeaderText="Effective From" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="EffectiveTo" HeaderStyle-CssClass="TableHeader" HeaderText="Effective To" ItemStyle-HorizontalAlign="Center" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

