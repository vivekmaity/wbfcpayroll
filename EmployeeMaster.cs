﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmployeeMaster
/// </summary>
public class EmployeeMasters
{
    //Personal Information
    public int EmployeeID { get; set; }
    public string EmployeeCode { get; set; }
    public string EmployeeNo { get; set; }
    public string EmployeeType { get; set; }
    public string EmpClassification { get; set; }
    public string EmployeeName { get; set; }
    public string EmployeeReligion { get; set; }
    public string EmployeeQualification { get; set; }
    public string EmployeeMaritalStatus { get; set; }
    public string EmployeeCaste { get; set; }
    public string EmployeeDOB { get; set; }
    public string EmployeeGender { get; set; }
    ////Address Information
    public string EmployeePresentAddress { get; set; }
    public string EmployeePresentState { get; set; }
    public string EmployeePresentDistrict { get; set; }
    public string EmployeePresentCity { get; set; }
    public string EmployeePresentPin { get; set; }
    public string EmployeePresentPhone { get; set; }
    public string EmployeePresentMobile { get; set; }
    public string EmployeePresentEmail { get; set; }
    public string EmployeePermanentAddress { get; set; }
    public string EmployeePermanentState { get; set; }
    public string EmployeePermanentDistrict { get; set; }
    public string EmployeePermanentCity { get; set; }
    public string EmployeePermanentPin { get; set; }
    public string EmployeePermanentPhone { get; set; }
    ////Official Details
    public string EmployeeDOJ { get; set; }
    public string EmployeeDOR { get; set; }
    public string EmployeeDesignation { get; set; }
    public string EmployeeCooperative { get; set; }
    public string EmployeeDA { get; set; }
    public string PFCode { get; set; }
    public string DNI { get; set; }
    public string HRA { get; set; }
    public string HRAFlag { get; set; }
    public string HRAFixedAmount { get; set; }
    public string EffectiveFrom { get; set; }
    public string QuarterAllot { get; set; }
    public string OLDBasic { get; set; }
    public string LicenceFees { get; set; }
    public string SpouseAmount { get; set; }
    public string HealthScheme { get; set; }
    public string DOJPresentDept { get; set; }
    public string PhysicallyChallenged { get; set; }
    public string Group { get; set; }
    public string Category { get; set; }

    public int DepartmentId { get; set; }
    public decimal EmployerPfRate { get; set; }
    public decimal EmployeePfRate { get; set; }

    public string FathersName { get; set; }
    public string AddressRemarks { get; set; }
    public string BranchInCharge { get; set; }

    public string PFRate { get; set; }

    public string Status { get; set; }
    public string PayScaleType { get; set; }
    public string PayScale { get; set; }
    public string CoopMembership { get; set; }
    public string HRACategory { get; set; }
    public string PANNO { get; set; }
    public string EffectiveTo { get; set; }
    public string House { get; set; }
    public string PCTBasic { get; set; }
    public string SpouseQuarter { get; set; }
    public string LWP { get; set; }
    public string IR { get; set; }
    public string HealthSchemeDetails { get; set; }
    ////Bank Details
    public string BranchID { get; set; }
    public string BankAccountCode { get; set; }
    ////Phota and Signature
    public string Photo { get; set; }
    public string Signature { get; set; }
    //Others
    public int SectorID { get; set; }
    public int CenterID { get; set; }

    public EmployeeMasters()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}