﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;            
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;             
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class PaySlipReportHist : System.Web.UI.Page
{
    public string str1 = "";
    static string StrFormula = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                //PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string GetSalMonth(int SecID, string SalFinYear)
    {
        //DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonthHistSecWise", SecID, SalFinYear);    //Get_SalPayMonthHist
        //DataTable dt = ds.Tables[0];
        DataTable dtSalMonth = ds1.Tables[0];
        //DataTable dtSalMonthID = ds1.Tables[0];

        List<SectorbyMonth> listSalMonth = new List<SectorbyMonth>();

        if (dtSalMonth.Rows.Count > 0)
        {
            for (int i = 0; i < dtSalMonth.Rows.Count; i++)
            {
                SectorbyMonth objst = new SectorbyMonth();

                //objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                //objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                objst.SalMonth = Convert.ToString(dtSalMonth.Rows[i]["SalMonth"]);
                objst.SalMonthID = Convert.ToInt32(dtSalMonth.Rows[i]["SalMonthID"]);
                listSalMonth.Insert(i, objst);
            }
        }
        
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class SectorbyMonth
    {
        //public int CenterID { get; set; }
        //public string CenterName { get; set; }
        public string SalMonth { get; set; }
        public int SalMonthID { get; set; }
    }
    [WebMethod]
     public static string GetLocation(int SecID, string SalFinYear)
    {
        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        //DataSet ds1 = DBHandler.GetResults("Get_SalPayMonthHist", SecID);
        //DataTable dt = ds.Tables[0];
        DataTable dtLocation = ds.Tables[0];
        //DataTable dtSalMonthID = ds1.Tables[0];

        List<CenterbyReportType> listLocation = new List<CenterbyReportType>();

        if (dtLocation.Rows.Count > 0)
        {
            for (int i = 0; i < dtLocation.Rows.Count; i++)
            {
                CenterbyReportType objst = new CenterbyReportType();

                //objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                //objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                objst.CenterName = Convert.ToString(dtLocation.Rows[i]["CenterName"]);
                objst.CenterID = Convert.ToInt32(dtLocation.Rows[i]["CenterID"]);
                listLocation.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listLocation);

    }
     public class CenterbyReportType
     {
         //public int CenterID { get; set; }
         //public string CenterName { get; set; }
         public string CenterName { get; set; }
         public int CenterID { get; set; }
     }


    //protected void PopulateCenter(int SecID, string SalFinYear)
    //{
    //    try        
    //    {

    //        string SecId = "";
    //        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
    //        DataTable dtSector = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
    //        SecId = dtSector.Rows[0]["SectorId"].ToString();
    //        if (dtSector.Rows.Count == 1)
    //        {
    //            DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth",Convert.ToInt32(SecId), SalFinYear);
    //            if (dtSalMonth.Rows.Count > 0)
    //            {
    //                txtPayMonth.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
    //                hdnSalMonthID.Value = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString(); 
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    //[WebMethod]
    //public static void Report_Paravalue(int SectorID, int SalMonth)
    //{
    //    //DataTable dtcr = DBHandler.GetResult("SalaryProcess", SectorID, SalMonth);

    //}
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(int StrSecID, int StrCenID, int StrSalMonthID, string FormName, string ReportName, string ReportType)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        String Msg = "";
        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
       
        if (ReportName == "PaySummarySecWiseHistory")
        {
            StrFormula = "{tmp_Summary_PayDetails_sectorWise_History.Secid}=" + StrSecID + " and {tmp_Summary_PayDetails_sectorWise_History.MonthID}=" + StrSalMonthID + " and {tmp_Summary_PayDetails_sectorWise_History.insertedBy}=" +UserID+ "";

        }
        if (ReportName == "History_PaySlip_new_LocationWISE_A5")
        {
            StrFormula = "{tmp_payslipHistory_monthly.SecID}=" + StrSecID + " and {MST_SalaryMonth.SalMonthID}=" + StrSalMonthID + " and {tmp_payslipHistory_monthly.inserted_By}=" + UserID + " and {tmp_mst_employee.tabInsertedBy}=" + UserID + " and {tmp_mst_employee.Status} in ['Y','S']";

        }
        if (ReportName == "PaySummarySecWiseLocationWise_History")
        {
            if (StrCenID == 0)
            {
                StrFormula = "{tmp_Summary_PayDetails_sectorWise_centerWise_History.Secid}=" + StrSecID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.MonthID}=" + StrSalMonthID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.insertedBy}=" + UserID + "";
            }
            else
            {
                StrFormula = "{tmp_Summary_PayDetails_sectorWise_centerWise_History.Secid}=" + StrSecID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.MonthID}=" + StrSalMonthID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.Centerid}=" + StrCenID + " and {tmp_Summary_PayDetails_sectorWise_centerWise_History.insertedBy}=" + UserID + "";
            }

        }
        
        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

        //DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Update_RptParameterValue", UserID, FormName, ReportName, ReportType, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9, Parameter10);

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }

}