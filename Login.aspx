﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always"  CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style> 
inputs:-webkit-input-placeholder {
    color: #b5b5b5;
}

inputs-moz-placeholder {
    color: #b5b5b5;
}

.inputs {
    padding: 15px 25px;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-weight: 400;
    font-size: 14px;
    color: #9D9E9E;
    text-shadow: 1px 1px 0 rgba(256, 256, 256, 1.0);
    background: #FFF;
    border: 1px solid #FFF;
    border-radius: 5px;
    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.50);
    -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.50);
    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.50);
}

.inputs:focus {
    background: #006E8B;
    color: #006E8B;
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.25);
    -moz-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.25);
    -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.25);
    outline: 0;
}

.inputs:hover {
    background: #DFE9EC;
    color: #414848;
}
.main-div 
	{
	    height:200px;
	    width:auto;
	    border-style: solid;
	    border-color: #ffffff;
	    border-width: medium;
        /*margin:10px;*/
        padding-left:35px;
    }


.log-button {
	-moz-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	box-shadow:inset 0px 1px 0px 0px #bee2f9;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #63b8ee), color-stop(1, #468ccf) );
	background:-moz-linear-gradient( center top, #63b8ee 5%, #468ccf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#63b8ee', endColorstr='#468ccf');
	background-color:#63b8ee;
	-webkit-border-top-left-radius:9px;
	-moz-border-radius-topleft:9px;
	border-top-left-radius:9px;
	-webkit-border-top-right-radius:9px;
	-moz-border-radius-topright:9px;
	border-top-right-radius:9px;
	-webkit-border-bottom-right-radius:9px;
	-moz-border-radius-bottomright:9px;
	border-bottom-right-radius:9px;
	-webkit-border-bottom-left-radius:9px;
	-moz-border-radius-bottomleft:9px;
	border-bottom-left-radius:9px;
	text-indent:8px;
	border:1px solid #3866a3;
	display:inline-block;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:40px;
	line-height:40px;
	width:100px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 38px #7cacde;
}
.log-button:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #468ccf), color-stop(1, #63b8ee) );
	background:-moz-linear-gradient( center top, #468ccf 5%, #63b8ee 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#468ccf', endColorstr='#63b8ee');
	background-color:#468ccf;
}.log-button:active {
	position:relative;
	top:1px;
}
</style> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						
						<section>
							<h2>Login Page</h2>						
						</section>

                        
                            <div class="main-div">
                                <asp:TextBox ID="empcode" autocomplete="off" runat="server"  class="inputs" placeholder="Emp Code" ></asp:TextBox><br />
                                <asp:TextBox ID="pass" autocomplete="off" runat="server" class="inputs" placeholder="Password"  ></asp:TextBox><br /><br />
                                <asp:Button ID="btnLogin" runat="server" CssClass="log-button" Text="Login"  OnClick="btnLogin_Click"/>
                            </div>	
					</div>
				</div>
			</div>
		</div>
</asp:Content>

