﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;


public partial class MenuMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulatePopup();
       }
        try
        {


        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    public void PopulatePopup()
    {
        DataTable dt = new DataTable();
        dt.Rows.Add();
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    [WebMethod]
    public static string[] AutoComplete_ParentMenu(string txtParentMenu)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_Menu", txtParentMenu, 1);  //HttpContext.Current.Session["MenuID"]
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["MenuName"].ToString().Trim(), dtRow["MenuCode"].ToString().Trim()));
        }
        return result.ToArray();
    }

    [WebMethod]
    public static string[] AutoComplete_MenuCode(string txtMenuCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_Menu", txtMenuCode, 2);  //HttpContext.Current.Session["MenuID"]
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["MenuName"].ToString().Trim(), dtRow["MenuCode"].ToString().Trim()));
        }
        return result.ToArray();
    }

    [WebMethod]
    public static string[] AutoComplete_UserID(string txtUserID)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_User", txtUserID);  //HttpContext.Current.Session["MenuID"]
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["UserName"].ToString().Trim(), dtRow["UserID"].ToString().Trim()));
        }
        return result.ToArray();
    }

    [WebMethod]
    public static string SaveMenu(string txtMenuName, string hdnParentMenuID, string txtPageName, string txtRank, string txtModuleCode, string txtMenuType)
    {
        string ConString = ""; string msg = "";
        ConString = DataAccess.DBHandler.GetConnectionString();

        SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());

        SqlCommand cmd = new SqlCommand();
        System.Data.SqlClient.SqlTransaction transaction;
        db.Open();
        transaction = db.BeginTransaction();
        try
        {
            try
            {
                cmd = new SqlCommand("Insert_MenuMaster '" + txtMenuName + "', " + (hdnParentMenuID.Equals("")?DBNull.Value:(object)Convert.ToInt32(hdnParentMenuID)) +
                                    ", '" + txtPageName + "', " + (txtModuleCode.Equals("")?DBNull.Value:(object)Convert.ToInt32(txtModuleCode)) + 
                                    ", '" + (txtMenuType.Equals("")?DBNull.Value:(object)Convert.ToInt32(txtMenuType)) + 
                                    "', " + (txtRank.Equals("")?DBNull.Value:(object)Convert.ToInt32(txtRank)) +"", db, transaction);
                                    //"', " + Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]) + "", db, transaction);
                cmd.ExecuteNonQuery();
                    msg = "1";

                transaction.Commit();
                db.Close();
            }
            catch (SqlException sqlError)
            {
                transaction.Rollback();
                msg = "0";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return msg.ToString();
    }

    [WebMethod]
    public static string UpdateMenu(string txtMenuName, string hdnParentMenuID, string txtPageName, string txtRank, string txtModuleCode, string txtMenuType, string hdnSearchMenuID)
    {
        string ConString = ""; string msg = "";
        ConString = DataAccess.DBHandler.GetConnectionString();

        SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());

        SqlCommand cmd = new SqlCommand();
        System.Data.SqlClient.SqlTransaction transaction;
        db.Open();
        transaction = db.BeginTransaction();
        try
        {
            try
            {
                cmd = new SqlCommand("Update_MenuMaster '" + txtMenuName + "', " + (hdnParentMenuID.Equals("") ? DBNull.Value : (object)Convert.ToInt32(hdnParentMenuID)) +
                                    ", '" + txtPageName + "', " + (txtModuleCode.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtModuleCode)) +
                                    ", '" + (txtMenuType.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtMenuType)) +
                                    "', " + (txtRank.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtRank)) + ", " + Convert.ToInt32(hdnSearchMenuID) + "", db, transaction);
                //"', " + Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]) + "", db, transaction);
                cmd.ExecuteNonQuery();
                msg = "1";

                transaction.Commit();
                db.Close();
            }
            catch (SqlException sqlError)
            {
                transaction.Rollback();
                msg = "0";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return msg.ToString();
    }

    [WebMethod]
    public static string SaveUser(string hdnMenuCodeID, string hdnUserID)
    {
        string ConString = ""; string msg = "";
        ConString = DataAccess.DBHandler.GetConnectionString();

        SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());

        SqlCommand cmd = new SqlCommand();
        System.Data.SqlClient.SqlTransaction transaction;
        db.Open();
        transaction = db.BeginTransaction();
        try
        {
            try
            {
                cmd = new SqlCommand("Insert_Dat_UserMenu " + Convert.ToInt32(hdnMenuCodeID) + ", " + Convert.ToInt32(hdnUserID) + "", db, transaction);
                //"', " + Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]) + "", db, transaction);
                cmd.ExecuteNonQuery();
                msg = "1";

                transaction.Commit();
                db.Close();
            }
            catch (SqlException sqlError)
            {
                transaction.Rollback();
                msg = "0";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return msg.ToString();
    }

    [WebMethod]
    public static string Search_SelectMenu(string hdnSearchMenuID)
    {
        DataSet ds = DBHandler.GetResults("Get_AllMenuInformation", hdnSearchMenuID, 0);
        string xml = ds.GetXml();
        return xml;
    }

    [WebMethod]
    public static string DeleteUser(string userid, string hdnSearchMenuID)
    {
        DataTable dt = DBHandler.GetResult("Get_BackPermission_UserMenu", userid, hdnSearchMenuID);
        string msg = "Record Deleted Successfully !!";
        return msg.ToString();
    }

    [WebMethod]
    public static string parent_Details(string hdnParentMenuID)
    {
        DataSet ds = DBHandler.GetResults("Get_AllMenuInformation", hdnParentMenuID, 1);
        string xml = ds.GetXml();
        return xml;
    }



}//










