﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;                         
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;

public partial class BankSlipCumListRptForm : System.Web.UI.Page
{
    public string str1 = "";
    static string PanNoReq = "";
    string display = "Pan No. Required For Those Employees who are Eligible For Income Tax!";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
    }       

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
          
    [WebMethod]
    public static string GetSalMonth(int SecID, string SalFinYear)
    {
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID, SalFinYear);
        DataTable dt = ds1.Tables[0];
        DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonthID = ds1.Tables[0];

        List<MaxSalMonth> listSalMonth = new List<MaxSalMonth>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MaxSalMonth objst = new MaxSalMonth();

                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                objst.PayMonthsID = Convert.ToInt32(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                listSalMonth.Insert(i, objst);
            }
        }
        
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class MaxSalMonth
    {
       public string PayMonths { get; set; }
       public int PayMonthsID { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string EncodeValue(int SalMonthID, int SectorID) 
    
    {
       //ScriptManager.RegisterStartupScript(this, GetType(), "unique_key","element.onclick = function(){ return confirm('Are you sure you want to delete?'); };",true);

        String ConnString = "";
        ConnString = DataAccess.DBHandler.GetConnectionString();

        String Encodeval = "";
        String Msg = "";
        SqlConnection con = new SqlConnection(ConnString);
        //SqlConnection con = new SqlConnection("server=192.168.1.25; database=db_kmda; uid= db_kmda;pwd=infotech");
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter("Select EncodeValue from MST_SalaryMonth where SecId=" + SectorID + " and SalMonthID=" + SalMonthID + "", con);
        adp.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["EncodeValue"].ToString() != "")
            {
                Encodeval = dt.Rows[0]["EncodeValue"].ToString();
                Msg = "Your Salary has been already been processed";

            }
            else
            {
                    int UserId =Convert.ToInt32( HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    DataSet ds1 = DBHandler.GetResults("SalaryEncrypt", SalMonthID, SectorID, UserId);
                    Msg = "Your Salary has been processed";
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);

    }

    protected void PopulateCenter(int SecID, string SalFinYear)
    {
        try
        {          
                        
            string SecId = "";
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
            DataTable dtSector = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
            
            if (dtSector.Rows.Count == 1)
            {
                SecId = dtSector.Rows[0]["SectorId"].ToString();
                DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth", Convert.ToInt32(SecId), SalFinYear);
                if (dtSalMonth.Rows.Count > 0)
                {
                    txtPayMonth.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
                    hdnSalMonthID.Value = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string PanNo_Required1(string SalFinYear, string curmonth, int SecID)
    {
                 
        String Msg1 = "";
        DataTable dtPanNo = DBHandler.GetResult("Get_PanNoRequired", SecID, curmonth, SalFinYear);
        PanNoReq = dtPanNo.Rows[0]["panno"].ToString();
        if (PanNoReq != "0")
        {
            Msg1 = "Pan No. Required For Those Employees who are Eligible For Income Tax !";
        }
        else           
        {
            Msg1 = "Pan No";
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg1);

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Check_LPCforBankList(int SectorID)
    {
        string JSONVal = ""; 
        try
        {
            DataTable dt = DBHandler.GetResult("LPCCheckForSalbank", SectorID);
            if (dt.Rows.Count > 0)
            {
                int Count=Convert.ToInt32(dt.Rows[0]["CountResult"].ToString());
                if (Count == 0)
                {
                    string res = "Generate";  
                    JSONVal = res.ToJSON();
                }
                else
                {
                    string res = "NotGenerate";  
                    JSONVal = res.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Show_LPCforBankList(string SectorID)
    {
        string JSONVal = "";
        try
        {

                var page = new Page();
                var userControl = (TransferEmpDetail)page.LoadControl("~/TransferEmpDetail.ascx");
                userControl.LPCBankListSecID = SectorID;

                page.Controls.Add(userControl);
                String htmlContent = "";

                using (var textWriter = new StringWriter())
                {
                    HttpContext.Current.Server.Execute(page, textWriter, false);
                    htmlContent = textWriter.ToString();
                }
                return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Get_EmployeeSectorStatus(int SectorID)
    {
        string JSONVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("get_employeeSectorStatus", SectorID);
            if (dt.Rows.Count > 0)
            {
                int Result = Convert.ToInt32(dt.Rows[0]["Result"].ToString());
                if (Result == 0)
                {
                    string res = "Avail";
                    JSONVal = res.ToJSON();
                }
                else
                {
                    string res = "NotAvail";
                    JSONVal = res.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Show_EmployeeSectorStatus(int SectorID)
    {
        string JSONVal = "";
        try
        {
                var page = new Page();
                var userControl = (TransferEmpDetail)page.LoadControl("~/TransferEmpDetail.ascx");
                userControl.LPCLocationnotAvailable =  SectorID.ToString();

                page.Controls.Add(userControl);
                String htmlContent = "";

                using (var textWriter = new StringWriter())
                {
                    HttpContext.Current.Server.Execute(page, textWriter, false);
                    htmlContent = textWriter.ToString();
                }
                return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Generate_PFmoreorLess(int SectorID, string SalMonthID)
    {
        string htmlContent = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Load_PFpercCheckForSalbank_grid", SectorID, SalMonthID);
            if (dt.Rows.Count > 0)
            {
                htmlContent = "NoGenerateReport".ToJSON();
            }
            else
                htmlContent = "GenerateReport".ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Show_PFmoreorLess(int SectorID, string SalMonthID)
    {
        string htmlContent = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Load_PFpercCheckForSalbank_grid", SectorID, SalMonthID);
            if (dt.Rows.Count > 0)
            {
                var page = new Page();
                var userControl = (TransferEmpDetail)page.LoadControl("~/TransferEmpDetail.ascx");
                userControl.dtPFMoreorLess = dt;

                page.Controls.Add(userControl);

                using (var textWriter = new StringWriter())
                {
                    HttpContext.Current.Server.Execute(page, textWriter, false);
                    htmlContent = textWriter.ToString();
                }
                return htmlContent;
            }
            else
                htmlContent = "GenerateReport".ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;
    }
}