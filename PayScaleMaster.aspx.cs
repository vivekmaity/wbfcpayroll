﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class PayScaleMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                    PopulateGrid();
            }
                DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
                if (ddlEmpType.SelectedValue == "S" || ddlEmpType.SelectedValue == "C" || ddlEmpType.SelectedValue == "K")
                    PopulateGridbyEmpType();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dtstr = DBHandler.GetResult("Get_PayScale");
            tbl.DataSource = dtstr;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void PopulateGridbyEmpType()
    {
        try
        {
            DataTable dt = new DataTable();
            tbl.DataSource = dt;
            tbl.DataBind();
            DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
            DataTable dtstr = DBHandler.GetResult("Get_PayScalebyEmployeeType", ddlEmpType.SelectedValue.ToString());
            if (dtstr.Rows.Count > 0)
            {
                tbl.DataSource = dtstr;
                tbl.DataBind();

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_PayScaleByID", ID);
                Session["PayScaleID"] = dtResult.Rows[0]["PayScaleID"].ToString();
                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlEmpType")).SelectedValue = dtResult.Rows[0]["EmpType"].ToString();
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_PayScaleByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                //TextBox txtPayScaleCode = (TextBox)dv.FindControl("txtPayScaleCode");
                DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
                TextBox txtPayScale = (TextBox)dv.FindControl("txtPayScale");
                TextBox txtGradePay = (TextBox)dv.FindControl("txtGradePay");

                DBHandler.Execute("Insert_PayScale", DBNull.Value, ddlEmpType.SelectedValue, txtPayScale.Text,
                    Convert.ToDouble(txtGradePay.Text), Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {
                //TextBox txtPayScaleCode = (TextBox)dv.FindControl("txtPayScaleCode");
                DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
                TextBox txtPayScale = (TextBox)dv.FindControl("txtPayScale");
                TextBox txtGradePay = (TextBox)dv.FindControl("txtGradePay");

                string ID = Session["PayScaleID"].ToString();

                DBHandler.Execute("Update_PayScale", ID, DBNull.Value, ddlEmpType.SelectedValue, txtPayScale.Text,
                    Convert.ToDouble(txtGradePay.Text), Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void ddlEmpType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (IsPostBack)
            {
                PopulateGridbyEmpType();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tbl.PageIndex = e.NewPageIndex;
        PopulateGrid();
    }
}