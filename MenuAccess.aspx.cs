﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;

public partial class MenuAccess : System.Web.UI.Page
{
    DataTable dtMenu;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //PopulateGrid();
                propulateTreeView();
                ddluser2.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void ddluser2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddluser2.SelectedValue.ToString() != "")
            {
                PopulateGrid(ddluser2.SelectedValue.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                //PopulateGrid();
                ddluser2.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid(string user)
    {
        try
        {
            DataTable dtPopulate = DBHandler.GetResult("Get_MenuByUser", Convert.ToInt32(user));
            tbl.DataSource = dtPopulate;
            tbl.DataBind();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void tbl_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView grd = (GridView)e.Row.FindControl("grdMenu");
                DataTable dt = DBHandler.GetResult("Get_AdminMenuAccessListByID", ((DataRowView)e.Row.DataItem)["UserID"].ToString());
               grd.DataSource = dt;
                grd.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_UserByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddluser")).SelectedValue = dtResult.Rows[0]["UserID"].ToString();
                    propulateTreeView();
                    PopulateSelectUserMenu(ID);
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("DELETE_MENUACCESSBYUSER", ID);
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();                
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                propulateTreeView();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }

    private void PopulateSelectUserMenu(string ID)
    {
        try
        {
            DataTable dtUserMenu = DBHandler.GetResult("Get_SelectedMenuByUser", ID);
            TreeView treeview = (TreeView)dv.FindControl("treeView_admin");
            foreach (DataRow row in dtUserMenu.Rows)
            {
                foreach (TreeNode node in treeview.Nodes)
                {
                    CheckUserMenu(Convert.ToInt32(row["MenuCode"].ToString()), node);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CheckUserMenu(int p, TreeNode node)
    {
        try
        {
            if (p == Convert.ToInt32(node.Value))
            {
                node.Checked = true;
            }
            else
            {
                foreach (TreeNode node2 in node.ChildNodes)
                {
                    CheckUserMenu(p, node2);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                DropDownList ddluser = (DropDownList)dv.FindControl("ddluser");
                DataTable dt = DBHandler.GetResult("CheckMenuAccessByUser", ddluser.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", "<script>alert('Menu access for this user is already created. Please edit & update record for the user.'); window.location='MenuAccess.aspx';</script>");
                }
                else
                {
                    TreeView tv = (TreeView)dv.FindControl("treeView_admin");
                    CheckInsertUserPermission(tv, Convert.ToInt32(ddluser.SelectedValue));
                    cmdSave.Text = "Add";
                    dv.ChangeMode(FormViewMode.Insert);
                    //PopulateGrid();
                    //PopulateNewTree(Session[SiteConstants.SSN_INT_USER_ID]);
                    dv.DataBind();
                    //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('')</script>");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", "<script>alert('Record Created Successfully.'); window.location='MenuAccess.aspx';</script>");
                    //Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
                }

            }
            else if (cmdSave.CommandName == "Edit")
            {
                DropDownList ddluser = (DropDownList)dv.FindControl("ddluser");
                TreeView tv = (TreeView)dv.FindControl("treeView_admin");
                CheckInsertUserPermission(tv, Convert.ToInt32(ddluser.SelectedValue));
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                //PopulateNewTree(Session[SiteConstants.SSN_INT_USER_ID]);
                dv.DataBind();
                //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Error", "<script>alert('Record Updated Successfully.'); window.location='MenuAccess.aspx';</script>");
                //Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void CheckInsertUserPermission(TreeView tree, int UserID)
    {
        try
        {
            DBHandler.Execute("Delete_MenuAccessByUser", UserID);
            foreach (TreeNode node in tree.Nodes)
            {
                InsertUserPermission(node, UserID);
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void InsertUserPermission(TreeNode node, int UserID)
    {
        try
        {
            if (node.Checked)
            {
                DBHandler.Execute("Insert_MenuAccess", node.Value, UserID, Session[SiteConstants.SSN_INT_USER_ID]);
            }
            foreach (TreeNode node2 in node.ChildNodes)
            {
                InsertUserPermission(node2, UserID);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected DataTable drpload()
    {
        DataTable dt = DBHandler.GetResult("Get_UserDdl");
        return dt;
    }

    private void propulateTreeView()
    {
        try
        {
            TreeView treeview = (TreeView)dv.FindControl("treeView_admin");
            dtMenu = DBHandler.GetResult("Get_MenuTree");
            DataTable objt = new DataTable();
            objt = GetData(0);
            foreach (DataRow dr in objt.Rows)
            {
                TreeNode node = new TreeNode();
                node.Text = "&nbsp;" + dr["MenuName"].ToString();
                node.Value = dr["MenuCode"].ToString();
                node.SelectAction = TreeNodeSelectAction.None;
                AddNodes(node);
                treeview.Nodes.Add(node);
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void AddNodes(TreeNode node)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = GetData(Convert.ToInt32(node.Value));
            foreach (DataRow row in dt.Rows)
            {
                TreeNode nNode = new TreeNode();
                nNode.Value = row["MenuCode"].ToString();
                nNode.Text = "&nbsp;" + row["MenuName"].ToString();
                nNode.SelectAction = TreeNodeSelectAction.None;
                AddNodes(nNode);
                node.ChildNodes.Add(nNode);
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable GetData(int intPID)
    {
        DataTable objT = new DataTable();
        objT.Columns.Add("MenuCode");
        objT.Columns.Add("MenuName");
        objT.Columns.Add("ParentMenuCode");
        objT.Columns.Add("PageName");
        foreach (DataRow dr in dtMenu.Rows)
        {
            if (dr[2].ToString() != intPID.ToString())
            {
                continue;
            }
            DataRow r = objT.NewRow();
            r[0] = dr[0];
            r[1] = dr[1];
            r[2] = dr[2];
            r[3] = dr[3];
            objT.Rows.Add(r);
        }
        return objT;
    }
}