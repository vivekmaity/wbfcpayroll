﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;           
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;          
using System.IO;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class AdminBankSlipCumList : System.Web.UI.Page
{
    public string str1 = "";
    public string strSalFin = "";
    static string StrFormula = "";
    static string strSalMonthID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        strSalFin = Convert.ToString(Session[SiteConstants.SSN_SALFIN]);
        //ddlReportType.SelectedIndex = 1;
        DataSet ds = DBHandler.GetResults("Get_SalPayYear_WithoutSec_GPF",strSalFin);
        DataTable dt = ds.Tables[0];
        DataTable dtSalMonth = ds.Tables[0];
        DataTable dtSalMonthID = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            txtPayMonth.Text = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
            //hdnSalMonthID.Value = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonthID"]);
            strSalMonthID = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonthID"]);
        }
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)                   
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public  static string Check_All_bank(int SalMonthID)
    
    {
        string JSONVal="";
        DataTable DTCheck = DBHandler.GetResult("Check_All_Sector_Bank", strSalMonthID);

        if (DTCheck.Rows.Count>0)
        { 
            string Check=DTCheck.Rows[0]["msg"].ToString();
            if (Check == "N")
                JSONVal = Check.ToJSON();
            else
                JSONVal = Check.ToJSON();

        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]       
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(int SalMonthID, string SalMonth, string SalFinYear, string FormName, string ReportName, string ReportType)
    {
        String Msg = "";            
        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        //if (ReportName == "GPFCPFAND_ITAX")
        //{
        //    DataTable dtcr = DBHandler.GetResult("Get_GPF_CPF_ITAX_Summary", SalMonth, SalMonthID, SalFinYear);
        //    StrFormula = "";
        //}
        //else if (ReportName == "SectorwiseGross_gpf_with_ITAX_PTAX")
        //{
        //    DataTable dtcr = DBHandler.GetResult("Get_GPF_Summary_ITAX_PTAX", SalMonth, SalMonthID);
        //    StrFormula = "";
        //}
        //else if (ReportName == "Admin_ITAX")
        //{
        //    StrFormula = "{DAT_MonthlySalary.EDID}= 1 and {MST_SalaryMonth.SalMonthID}=" + SalMonthID;
        //}
        

        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }






}