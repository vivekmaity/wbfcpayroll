﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;               
using System.Text;
using System.Web.Script.Services;        
using System.Globalization;
using System.Web.Script;               
using System.Web.Script.Serialization;                
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class AllCoOpPFLoanDeduReport : System.Web.UI.Page
{
    public string str1 = "";
    public static string hdnsalmonth = "";           
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)                       
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
        ddlEmpType.SelectedValue = "1";
        ddlStatus.SelectedValue = "1";
       
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {         
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string GetSalMonth(int SecID, string SalFinYear)
    {
        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID, SalFinYear);
        DataTable dt = ds.Tables[0];
        DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonthID = ds1.Tables[0];

        List<CenterbySector> listCenter = new List<CenterbySector>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CenterbySector objst = new CenterbySector();

                objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                objst.PayMonthsID = Convert.ToInt32(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                listCenter.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listCenter);

    }
    public class CenterbySector
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
        public string PayMonths { get; set; }
        public int PayMonthsID { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(string SalaryFinYear, int SalMonthID, int SectorID, int CenterID, string EmpType, string EmpStatus, int DeduEdid, int LoanEdid, int ReportID, int UserId)
    {
        String Msg = "";
        UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        if (ReportID == 1)             
        {
            DataTable dtcr = DBHandler.GetResult("Get_PaySchedule_PF_COOP", SalaryFinYear, hdnsalmonth, SectorID, CenterID, EmpType, EmpStatus, DeduEdid, LoanEdid, UserId);
        }
        else {
            DataTable dtcr1 = DBHandler.GetResult("Get_PaySchedule_CPF_all_PF_state", SalaryFinYear, hdnsalmonth, SectorID, CenterID, EmpType, EmpStatus, DeduEdid, LoanEdid, UserId);
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
        
    }

    protected void PopulateCenter(int SecID, string SalFinYear)
    {
        try
        {     

            string SecId = "";
            ddlLocation.Items.Clear();
            ddlLocation.Items.Add("Select Location");
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
            DataTable dtSector = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
            SecId = dtSector.Rows[0]["SectorId"].ToString();
            if (dtSector.Rows.Count == 1)
            {
                DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth", Convert.ToInt32(SecId), SalFinYear);
                if (dtSalMonth.Rows.Count > 0)
                {
                    ddlLocation.DataSource = ds.Tables[0];
                    ddlLocation.DataTextField = "CenterName";
                    ddlLocation.DataValueField = "CenterID";
                    ddlLocation.DataBind();

                    txtPayMonth.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
                    // hdnSalMonthID.Value = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();
                    hdnsalmonth= dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

}