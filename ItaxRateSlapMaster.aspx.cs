﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using DataAccess;

public partial class ItaxRateSlapMaster : System.Web.UI.Page
{
  
    protected void Page_Load(object sender, EventArgs e)
    {

    }   

    [WebMethod]
    public static string Get_Data(string FinYear)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("LoadItaxRateSlab",FinYear);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    [WebMethod]
    public static string Save(string RateSlabID, string AmountFrom, string AmountTo, string Percentage, string Sex, string AddAmount, string FinYear )
    {
        try
        {
            DataSet ds = DBHandler.GetResults("InstUpdtItaxRateSlab", RateSlabID.Equals("")?0:(object)RateSlabID, AmountFrom, AmountTo, Percentage, Sex, AddAmount, FinYear, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string Delete(string ID)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("Delete_ItaxRateSlap",ID);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}