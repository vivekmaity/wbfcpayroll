﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class Co_OperativeCalculation : System.Web.UI.Page
{
    static int EDID = 0; static int MenuID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() !="")
            {
                PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }

            if (HttpContext.Current.Request.QueryString["EDID"] != null)
            {
                EDID = Convert.ToInt32(HttpContext.Current.Request.QueryString["EDID"]);
                MenuID = Convert.ToInt32(HttpContext.Current.Request.QueryString["mid"]);

                Session["EDID"] = EDID;
                Session["MenuID"] = MenuID;
            }
            DataTable dtEDName = DBHandler.GetResult("Get_EarnDeductionByEDID", EDID);
            lblHeader.Text = dtEDName.Rows[0][1].ToString() + "_" + "Master" + "_"+ "Calculation";
        }

    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SearchDetails(int SecID, int CenterID)
    {
        string JSONVal = "";
        try
        {
            int UserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            int VEdid = Convert.ToInt32(HttpContext.Current.Session["EDID"].ToString());
            int VmenuID = Convert.ToInt32(HttpContext.Current.Session["MenuID"].ToString());
            string SALFIN = HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString();
            int PAYMONTHID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_PAYMONTHID].ToString());

            int Edid = VEdid;
            int menuID = VmenuID;

            DataTable dtDetail = DBHandler.GetResult("Get_SalMonthandCooperativeDetails", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID, CenterID, Edid, menuID, PAYMONTHID, SALFIN, UserId);

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                if (dtDetail.Rows.Count > 0)
                {
                    Dictionary<string, object> row;

                    foreach (DataRow r in dtDetail.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtDetail.Columns)
                        {
                            row.Add(col.ColumnName, r[col]);
                        }
                        rows.Add(row);
                    }

                }
                JSONVal = rows.ToJSON();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SearchDetailsbyEmpNo(int SecID, int CenterID, string EmpNo)
    {
        string JSONVal = "";
        try
        {
	    int VEdid = Convert.ToInt32(HttpContext.Current.Session["EDID"].ToString());
            int VmenuID = Convert.ToInt32(HttpContext.Current.Session["MenuID"].ToString());

            int Edid = VEdid; int menuID = VmenuID;
            DataTable dtDetail = DBHandler.GetResult("Get_SalMonthandCooperativeDetailsbyEmpNo", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID, CenterID, Edid, menuID, EmpNo);

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            if (dtDetail.Rows.Count > 0)
            {
                Dictionary<string, object> row;

                foreach (DataRow r in dtDetail.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtDetail.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }

            }
            JSONVal = rows.ToJSON();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    //[WebMethod(EnableSession = true)]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public static string UpdateDetails(string cooperativeForm)
    //{
    //    string JSONVal = ""; 
    //    try
    //    {
    //        string noNewLines = cooperativeForm.Replace("\n", "");
    //        JavaScriptSerializer Serializer = new JavaScriptSerializer();

    //        List<CoOperativeDetail> lstEmployee = Serializer.Deserialize<List<CoOperativeDetail>>(noNewLines);
    //        string result = EmployeeDataHandling.UpdateCoOperativeDetails(lstEmployee);
    //        JSONVal = result.ToJSON();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //    return JSONVal;
    //}

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateDetailsRowWise(string id, string Amount, string SecID)
    {
        string JSONVal = "";
        try
        {
            DataTable dtEncodeValue = DBHandler.GetResult("Get_SalPayMonth", SecID, HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);
            if (dtEncodeValue.Rows.Count > 0)
            {
                string EncValue = dtEncodeValue.Rows[0]["EncodeValue"].ToString();
                if (EncValue == "" || EncValue == null)
                {
		    int VEdid = Convert.ToInt32(HttpContext.Current.Session["EDID"].ToString());
            	    
                    string result = EmployeeDataHandling.UpdateCoOperativeDetails(id, VEdid, Amount);
                    JSONVal = result.ToJSON();
                }
                else
                {
                    string result = "Not";
                    JSONVal = result.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

   protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
   public static string GetCenter(int SecID, string SalFinYear)
    {
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
            DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID, SalFinYear);
            DataTable dt = ds.Tables[0]; DataTable dtSalMonth = ds1.Tables[0];

                List<CenterbySector> listCenter = new List<CenterbySector>();

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CenterbySector objst = new CenterbySector();

                    objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                    objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                    objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);

                    listCenter.Insert(i, objst);
                }
            }
        

            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(listCenter);
        
    }
    public class CenterbySector
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
        public string PayMonths { get; set; }
    }

    protected void PopulateCenter(int SecID, string SalFinYear)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
            DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID, SalFinYear);
            DataTable dt = ds.Tables[0]; DataTable dtSalMonth = ds1.Tables[0];

            if (dt.Rows.Count > 0)
            {
                ddlCenter.DataSource = dt;
                ddlCenter.DataTextField = "CenterName";
                ddlCenter.DataValueField = "CenterID";
                ddlCenter.DataBind();

                if (dtSalMonth.Rows.Count>0)
                txtPayMonth.Text=dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}