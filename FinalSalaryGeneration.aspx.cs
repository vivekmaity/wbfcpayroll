﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Threading;

public partial class FinalSalaryGeneration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
    }

    protected void PopulateCenter(int SecID, string SalFinYear)
    {
        try
        {

            string SecId = "";
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
            DataTable dtSector = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
            SecId = dtSector.Rows[0]["SectorId"].ToString();
            if (dtSector.Rows.Count == 1)
            {
                DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth", Convert.ToInt32(SecId), SalFinYear);
                if (dtSalMonth.Rows.Count > 0)
                {
                    txtPayMonth.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
                    hdnSalMonthID.Value = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();

                    hdnSalMonth.Value = txtPayMonth.Text;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string GetSalMonth(int SecID, string SalFinYear)
    {
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID, SalFinYear);
        DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonthID = ds1.Tables[0];

        List<MaxSalMonth> listSalMonth = new List<MaxSalMonth>();

        if (dtSalMonth.Rows.Count > 0)
        {
            for (int i = 0; i < dtSalMonth.Rows.Count; i++)
            {
                MaxSalMonth objst = new MaxSalMonth();


                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                objst.PayMonthsID = Convert.ToInt32(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                listSalMonth.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class MaxSalMonth
    {
        public string PayMonths { get; set; }
        public int PayMonthsID { get; set; }
    }

    [WebMethod]
    public static string GetSalMonthbySalFinYear(string SalFinYear)
    {
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", HttpContext.Current.Session[SiteConstants.SSN_SECTORID], SalFinYear);
        DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonthID = ds1.Tables[0];

        List<MaxSalMonths> listSalMonth = new List<MaxSalMonths>();

        if (dtSalMonth.Rows.Count > 0)
        {
            for (int i = 0; i < dtSalMonth.Rows.Count; i++)
            {
                MaxSalMonths objst = new MaxSalMonths();


                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                objst.PayMonthsID = Convert.ToInt32(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                listSalMonth.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class MaxSalMonths
    {
        public string PayMonths { get; set; }
        public int PayMonthsID { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetEncodedValue(string salmonth)
    {
        string JSONVal = "";
        try
        {
            string SecID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
            DataTable dtEncodeValue = DBHandler.GetResult("Get_SalPayMonth", SecID, HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);

            if (dtEncodeValue.Rows.Count > 0)
            {
                string EncodedValue = dtEncodeValue.Rows[0]["EncodeValue"].ToString();
                if (EncodedValue != null || EncodedValue != "")
                {
                    JSONVal = EncodedValue.ToJSON();
                }
                else
                {
                    string encodevalue = "";
                    JSONVal = encodevalue.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Get_ForNextMonth(string salmonth, string SecID)
    {
        string JSONVal = "";
        try
        {
            DataTable dtValue = DBHandler.GetResult("Check_NextMonthProcess", salmonth, SecID);

            if (dtValue.Rows.Count > 0)
            {
                string Value = dtValue.Rows[0]["Result"].ToString();
                if (Value != null || Value != "")
                {
                    JSONVal = Value.ToJSON();
                }
                else
                {
                    string encodevalue = "";
                    JSONVal = encodevalue.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GoForNextMonth(string salmonthid, string secid)
    {
        string JSONVal = ""; string SecID = "";
        try
        {
           
            string SessionSecID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
            if (SessionSecID == "1")
            {
                SecID = secid;
            }
            else
            {
                SecID = HttpContext.Current.Session[SiteConstants.SSN_SECTORID].ToString();
            }
            string UserID = HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID].ToString();

            DBHandler.Execute("Sal_Generation", salmonthid, UserID, "Y", SecID);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }
    [System.Web.Services.WebMethod]
    public static string GetText()
    {
        for (int i = 0; i < 10; i++)
        {
            // In actual projects this action may be a database operation.    
            //For demsonstration I have made this loop to sleep.    
            //Thread.Sleep(2600);
        }
        return "Download Complete...";
    }
    protected static void progress()
    {
       
        //Progressbar1.Minimum = 1;
        //Progressbar1.Maximum = dt.Rows.Count;
        //Progressbar1.Value = 1;
        //Progressbar1.Step = 1;

        //for (int i = 1; i <= dt.Rows.Count; i++)
        //{
        //    bool result = InsertUserdetails(Username);
        //    if (result == true)
        //    {
        //        Progressbar1.PerformStep();
        //    }
        //}
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string MessageBoxYesNo(string Message, string buttonText)
    {
        try
        {
            string controlLocation = "~/GridviewTemplete.ascx";
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.Message = Message;
            userControl.MessageButtonText = buttonText;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string MessageBox(string Message)
    {
        try
        {
            string controlLocation = "~/GridviewTemplete.ascx";
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.Message = Message;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}