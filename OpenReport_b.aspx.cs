﻿using System;
using System.Configuration;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

public partial class OpenReport_b : System.Web.UI.Page
{
    ExportFormatType formatType = ExportFormatType.NoFormat;
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
    string conString = DataAccess.DBHandler.GetConnectionString();
    //ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
    Dictionary<string, string> Params;
    IEncryptDecrypt chiperAlgorithm;
    protected void Page_Load(object sender, EventArgs e)
    {
        //string QueryStringData = Request.QueryString["Data"];
        //chiperAlgorithm = new AES_Algorithm("EIIPL@201^");
        //string decryptedURL = chiperAlgorithm.Decrypt(QueryStringData);
        //string urlDecodedData = HttpUtility.UrlDecode(decryptedURL);
        //JavaScriptSerializer js = new JavaScriptSerializer();
        ////var serializedData = js.Deserialize<QueryData>(urlDecodedData);

        var keys = Request.QueryString["ReportName"];
        if (keys != null)
        {
            JObject obj = JObject.Parse(keys);
            var master = (JArray)obj.SelectToken("Master");
            var detal = (JArray)obj.SelectToken("Detail");

            GenerateReport(master, detal);
        }
    }
    private void GenerateReport(JArray master, JArray detal)
    {
        string reportName = master[0]["ReportName"].ToString();
        string FileName = master[0]["FileName"].ToString();

        crystalReport.Load(Server.MapPath("~/Reports/" + reportName));
        crystalReport.Refresh();

        string server = Utility.ServerName(conString);
        string database = Utility.DatabaseName(conString);
        string userid = Utility.UserID(conString);
        string password = Utility.Password(conString);

        crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
        crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
        crystalReport.SetDatabaseLogon(server, database, userid, password);
crystalReport.VerifyDatabase();
        for (int i = 0; i < crystalReport.Subreports.Count; i++)
        {
            crystalReport.Subreports[i].DataSourceConnections[0].SetConnection(server, database, userid, password);
            crystalReport.Subreports[i].DataSourceConnections[0].IntegratedSecurity = false;
            crystalReport.Subreports[i].SetDatabaseLogon(server, database, userid, password);
        }
        

        foreach (var item in detal)
        {
            string a = item.ToString();
            string noNewLines = a.Replace("\n", "");
            Params = JsonConvert.DeserializeObject<Dictionary<string, string>>(noNewLines);
        }

        foreach (KeyValuePair<string, string> entry in Params)
        {
            crystalReport.SetParameterValue("@" + entry.Key, entry.Value);
        }

        formatType = ExportFormatType.PortableDocFormat;
        crystalReport.ExportToHttpResponse(formatType, Response, false, FileName);

        crystalReport.Close();
        crystalReport.Dispose();
        GC.Collect();
    }
}