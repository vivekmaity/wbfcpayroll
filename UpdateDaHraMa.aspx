﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="UpdateDaHraMa.aspx.cs" Inherits="UpdateDaHraMa" %>

<%@ Register TagPrefix="art" TagName="EmpDaHraMa" Src="GridviewTemplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtMa").hide();
        });

        //This is for Bind DA on the Basis of EmployeeType
        $(document).ready(function () {
            $("#ddlEmpType").change(function () {
                if ($("#ddlEmpType").val() != "Please select") {
                    var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
                    var EarType = $('#ddlEarningType').val();
                    var URL = "";
                    if (EarType == "D")
                        URL = "UpdateDaHraMa.aspx/BindDA";
                    else if (EarType == "H")
                        URL = "UpdateDaHraMa.aspx/BindHRA";
                    else
                        URL = "UpdateDaHraMa.aspx/BindMA";
                    //alert(E);
                    var options = {};
                    options.url = URL;
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listdA) {
                        var t = jQuery.parseJSON(listdA.d); //alert(JSON.stringify(t));
                        var a = t.length;
                        $("#ddlDA").empty();
                        if (EarType == "D") {
                            if (a >= 0) {
                                $("#ddlDA").append("<option value=''>(Select)</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlDA").append($("<option selected='true'></option>").val(value.DAID).html(value.DARate));

                                });
                            }
                        }
                        else if (EarType == "H") {
                            $("#ddlDA").append("<option value='0'>(Select)</option>")
                            $.each(t, function (key, value) {
                                $("#ddlDA").append($("<option selected='true'></option>").val(value.HRAID).html(value.HRA));
                            });
                        }
                        else {
                            var payval = t[0].PayVal; 
                            $("#txtMa").val(payval);
                        }
                        //$("#ddlDA").prop("disabled", false);
                    };
                    options.error = function () { alert("Error in retrieving DA/HRA!"); };
                    $.ajax(options);
                }
                else {
                    $("#ddlDA").empty();
                    $("#ddlDA").prop("disabled", true);
                }
            });
        });

        $(document).ready(function () {
            $("#ddlEarningType").change(function () {
                var EarType = $("#ddlEarningType").val();
                if (EarType == "M") {
                    $("#txtMa").show();
                    $("#ddlDA").hide();
                }
                else {
                    $("#txtMa").hide();
                    $("#ddlDA").show();
                }

                $("#ddlEmpType").val('');
                $("#ddlDA").val('');
            });
        });

        function beforeSave() {
            //$("#form1").validate();
            $("#ddlEarningType").rules("add", { required: true, messages: { required: "Please Select Earning Type" } });
            $("#ddlEmpType").rules("add", { required: true, messages: { required: "Please Select Employee Type" } });
            //$("#ddlDA").rules("add", { required: true, messages: { required: "Please Select DA/HRA/MA" } });

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        $(document).ready(function () {
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
        });

        $(document).ready(function () {
            $('#cmdSave').on('click', function () {
                var EarningType = $('#ddlEarningType').find('option:selected').text();
                var EmpType = $("#ddlEmpType").val(); //alert(EmpType);
                var DaHraMaValue = $('#ddlDA').find('option:selected').text(); //alert(DaHraMa);
                var MaValue = $('#txtMa').val();
                
                if (EmpType != "" || DaHraMaValue != "" || MaValue != "" && $("#ddlEarningType").val() != "") {
                    if ($('#ddlEarningType').val() == "D" || $('#ddlEarningType').val() == "H")
                        var C = "{EarningType: '" + EarningType + "', DaHraMaValue: '" + DaHraMaValue + "', EmpType: '" + EmpType + "', EmpNo: '" + 0 + "'}";
                    else
                        var C = "{EarningType: '" + EarningType + "', DaHraMaValue: '" + MaValue + "', EmpType: '" + EmpType + "', EmpNo: '" + 0 + "'}";
                    //alert(C);
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/AjaxGetGridCtrl',
                        data: C,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            //alert(JSON.stringify(D));
                            $('#EmpDetail').html(D.d);
                            //******************************************************
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/CountTotalEmployee',
                                data: C,
                                cache: false,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    var TotalEmp = jQuery.parseJSON(D.d);
                                    $('#lblTotalEmp').text(TotalEmp);
                                }
                            });
                            //********************************************************
                            $("#overlay").show();
                        }
                    });
                    
                }
                else {
                    alert("Please Enter Total Fields.");
                    $("#ddlEarningType").focus();
                }
            });
        });

        $(document).ready(function () {
            $("#Close").click(function (e) {
                $("#overlay").hide();
            });
        });

        $(document).ready(function () {
        $('#btnAccept').on('click', function () {
            var EmpNo = 0; var ArrayList = []; var ArrayListDecline = [];
            $("input[id*='chkSelect']:checkbox").each(function (index) {
                if ($(this).is(':checked')) {
                    EmpNo = $(this).closest('tr').find('td:eq(1)').text();
                    Decline = $(this).closest('tr').find('td:eq(7)').text();

                    var lre = /^\s*/;
                    var rre = /\s*$/;
                    EmpNo = EmpNo.replace(lre, "");
                    EmpNo = EmpNo.replace(rre, "");

                    var lre = /^\s*/;
                    var rre = /\s*$/;
                    Decline = Decline.replace(lre, "");
                    Decline = Decline.replace(rre, "");

                    ArrayList.push(EmpNo);
                    ArrayListDecline.push(Decline);
                }
            });
            //alert(ArrayList); alert(ArrayListDecline);
            var a = ArrayList.length;
            if (a != 0) {
                if (a > 1) {
                    alert("Please Select One Employee at a Time.");
                    return;
                }
                var decline = $('#ddlDecline').val();
                if (decline != "") {
                    var remarks = $('#txtRemarks').val();
                    if (decline == "Y") {
                        if (remarks == "") {
                            alert("Please Enter Remarks.");
                            $('#txtRemarks').focus();
                            return;
                        }
                        else {
                            var EmpNos = ArrayList[0];
                            var F = "{EmpNos: '" + EmpNos + "', decline: '" + decline + "', remarks: '" + remarks + "'}";
                            //alert(F);
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/UpdateEmpDaHraMa',
                                data: F,
                                cache: false,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    //alert(JSON.stringify(D));
                                    var da = jQuery.parseJSON(D.d);
                                    if (da = 'Updated') {
                                        alert("This Employee is Declined Successfully.");
                                        $("#cmdSave").trigger("click");
                                        $('#ddlDecline').val('');
                                        $('#txtRemarks').val('');
                                    }
                                },
                                error: function (response) {
                                    alert('');
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        }
                    }
                    else {
                        var EmpNos = ArrayList[0];
                        //alert(EmpNos)
                        var F = "{EmpNos: '" + EmpNos + "', decline: '" + decline + "', remarks: '" + remarks + "'}";
                        //alert(F);
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/UpdateEmpDaHraMa',
                            data: F,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                //alert(JSON.stringify(D));
                                var da = jQuery.parseJSON(D.d);
                                if (da = 'Updated') {
                                    alert("This Employee is Declined Successfully.");
                                }
                            },
                            error: function (response) {
                                alert('');
                            },
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }
                }
                else {
                    alert("Please Select Decline Either 'Yes' or 'No'.");
                    $('#ddlDecline').focus();
                    return;
                }
            }
            else {
                alert("Please Select CheckBox.");
                return;
            }
        });
        });

        $(document).ready(function () {
            $("#btnGenerate").click(function (e) {
                var dialogResult = confirm("Are You sure you want to Generate DA/HRA for all employee.");
                if (dialogResult == true) {
                    var daid = $("#ddlDA").val();
                    var ma = $("#txtMa").val();
                    var earType = $("#ddlEarningType").val();

                    if(earType=="D" || earType=="H")
                        var F = "{Daid: '" + daid + "'}";
                    else
                        var F = "{Daid: '" + ma + "'}";

                    //alert(F);
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/UpdateFinalEmpDaHraMa',
                        data: F,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            //alert(JSON.stringify(D));
                            var da = jQuery.parseJSON(D.d);
                            if (da == "Updated") {
                                alert("Employee DAID is Updated Successfylly.");
                                //$("#cmdSave").trigger("click");
                                $("#overlay").hide();
                                window.location.href = "SalaryGeneration.aspx";
                            }
                        }
                    });
                }
            });

        });

        $(document).ready(function(){
        $("#btnSearchEmp").click(function (e) {
            var EmpNo = $('#txtSearchEmp').val();
            if (EmpNo == "")
            { $('#txtSearchEmp').focus(); }
            else {
                var EarningType = $('#ddlEarningType').find('option:selected').text();
                var EmpType = $("#ddlEmpType").val(); //alert(EmpType);
                var DaHraMaValue = $('#ddlDA').find('option:selected').text(); //alert(DaHraMa);
                var MaValue = $('#txtMa').val();

                if (EarningType == "D" && EarningType == "H")
                    var E = "{EarningType: '" + EarningType + "', DaHraMaValue: '" + DaHraMaValue + "', EmpType: '" + EmpType + "', EmpNo: '" + EmpNo + "'}";
                else
                    var E = "{EarningType: '" + EarningType + "', DaHraMaValue: '" + MaValue + "', EmpType: '" + EmpType + "', EmpNo: '" + EmpNo + "'}";
                //alert(E);
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/AjaxGetGridCtrl',
                    cache: false,
                    data:E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        //alert(JSON.stringify(D));
                        console.log(D.d);
                        $('#EmpDetail').html(D.d);
                        //******************************************************
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/CountTotalEmployee',
                            data: C,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                var TotalEmp = jQuery.parseJSON(D.d);
                                $('#lblTotalEmp').text(TotalEmp);
                            }
                        });
                        //********************************************************
                        $("#overlay").show();
                    },
                    error: function (response) {
                        alert('');
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        });
        });

        $(document).ready(function () {
            $("#btnReset").click(function (e) {
                $('#txtSearchEmp').val('');
                $("#cmdSave").trigger("click");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Update DA HRA Master</h2>
                    </section>

                    <div class="content-overlayforUpdateDaHraMa" id='overlay'>
                        <div class="wrapper-outerforUpdateDaHraMa">
                            <div class="wrapper-innerforUpdateDaHraMa">
                                <div class="loadContentforUpdateDaHraMa">
                                    <div id="Close" class="close-contentforUpdateDaHraMa" ><a href="javascript:void(0);" id="prev" style="color:red;">Close</a></div>
                                    <div style="float: left;">
                                        &nbsp;&nbsp; <span class="headFont" style="font-weight: bold;">Emp No.&nbsp;&nbsp;</span>
                                        <asp:TextBox ID="txtSearchEmp"  ClientIDMode="Static" Style="padding-left: 8px;" CssClass="textbox" runat="server"
                                            Width="70px" autocomplete="off" Height="13px" MaxLength="6"></asp:TextBox>
                                        <asp:Button ID="btnSearchEmp" ClientIDMode="Static" runat="server" Width="55px" Text="Search" CssClass="DefaultButton" BackColor="#deedf7" />
                                        <asp:Button ID="btnReset" ClientIDMode="Static" runat="server" Width="50px" Text="Reset" CssClass="DefaultButton" BackColor="#deedf7" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblSectorName" ClientIDMode="Static" runat="server"  Text="Total Employee" class="headFont" style="font-weight: bold;" />
                                        <span class="headFont" style="font-weight: bold;">(</span>
                                        <asp:Label ID="lblTotalEmp" ClientIDMode="Static" runat="server"  Text="" class="headFont" style="font-weight: bold;" />
                                        <span class="headFont" style="font-weight: bold;">)</span>
                                    </div><br /><br />
                                    <table  cellpadding="5" style=" padding-bottom: 10px; padding-right:10px; width: 650px; overflow: scroll;" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table align="center" cellpadding="5">
                                                    <tr>
                                                        <td>
                                                            <div id="EmpDetail" runat="server"  clientidmode="Static" style="width: 980px; height: 440px;
                                                                overflow-y:scroll;" >
                                                                </div>
                                                            <div class="container">
                                                                <table>
                                                                    <tr>
                                                                        <td style="padding: 5px;"></td>
                                                                        <td style="padding: 10px; align-items: center;">
                                                                            <asp:Button ID="btnAccept" runat="server" Text="Update" CommandName="Add"
                                                                                Width="115px" CssClass="Btnclassname DefaultButton" />
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Decline :</span>&nbsp;&nbsp;<span class="require">*</span></td>
                                                                        <td style="padding: 10px; align-items: center;">
                                                                            <asp:DropDownList ID="ddlDecline" Width="100px" Height="22px" runat="server" ClientIDMode="Static"
                                                                                AppendDataBoundItems="true" autocomplete="off">
                                                                                <asp:ListItem Text="(Decline)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                               </asp:DropDownList>
                                                                        </td>
                                                                        <td style="padding: 5px;"><span class="headFont">Remarks :</span>&nbsp;&nbsp;<span class="require">*</span></td>
                                                                        <td style="padding: 5px;">
                                                                            <asp:TextBox ID="txtRemarks" autocomplete="off" CssClass="textbox" Width="450px" runat="server" ></asp:TextBox>
                                                                            
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                                <div >
                                                                    <p style="text-align:center; width:950px;">
                                                                        <asp:Button ID="btnGenerate" runat="server" Text="Generate" CommandName="Add"
                                                                                    Width="115px" CssClass="Btnclassname DefaultButton"  />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="float: right;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                           </div>
                        </div>
                    </div>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Earning Type&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlEarningType" Width="220px" Height="23px" runat="server"  DataValueField="BankID" AutoPostBack="false" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" >
                                                                        <asp:ListItem Text="(Select Earning Type)" Selected="True" Value=""></asp:ListItem>
                                                                        <asp:ListItem Text="DA" Value="D"></asp:ListItem>
                                                                        <asp:ListItem Text="HRA" Value="H"></asp:ListItem>
                                                                        <%--<asp:ListItem Text="MA" Value="M"></asp:ListItem>--%>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                              <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Employee Type&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlEmpType" Width="220px" Height="23px" runat="server"  DataValueField="BankID" AutoPostBack="false" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" >
                                                                        <asp:ListItem Text="(Select Employee Type)" Selected="True" Value=""></asp:ListItem>
                                                                        <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                                        <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                                        <asp:ListItem Text="WBFC Employee" Value="K"></asp:ListItem>
                                                                     </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">New DA/HRA Rate&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlDA" Width="220px" Height="23px" runat="server"  DataValueField="BankID" AutoPostBack="false" DataTextField="BankName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off" >
                                                                         <asp:ListItem Text="(Select)" Selected="True" Value=""></asp:ListItem>
                                                                     </asp:DropDownList>

                                                                    <asp:TextBox ID="txtMa" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="textbox" ></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </InsertItemTemplate>
                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                               
                                                                <td style="padding: 5px;">
                                                                    <span class="headFont">Bank Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    :
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtBankName"  ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                        MaxLength="20" Text='<%# Eval("BankName") %>' autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;">
                                                                    <span class="require">*</span> indicates Mandatory Field
                                                                </td>
                                                                <td style="padding: 5px;">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                             <asp:Button ID="cmdSave" runat="server" Text="Show Changes" CommandName="Add" Width="100px"
                                                                      CssClass="Btnclassname DefaultButton" />

                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Refresh" Width="100px" CssClass="Btnclassname"
                                                                              />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="overflow-y: scroll; height: auto; width: 100%">
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
</div> 
</asp:Content>

