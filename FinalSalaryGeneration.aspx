﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="FinalSalaryGeneration.aspx.cs" Inherits="FinalSalaryGeneration" %>
<%@ Register TagPrefix="art" TagName="FinalSalGen" Src="GridviewTemplete.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <script src="js/jquery.progress.js"></script>
    <link href="css/ProgressBar.css" rel="stylesheet" />

        <style type="text/css">
        .style1
        {
            width: 64px;
        }
        .style2
        {
            width: 3px;
        }
    </style>


    <script type="text/javascript">
        $(document).ready(function () {
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
        });

        $(document).ready(function () {
            $("#txtSalFinYear").change(function () {
                var E = "{SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                $.ajax({
                    type: "POST",
                    url: "FinalSalaryGeneration.aspx/GetSalMonthbySalFinYear",
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (listSalMonth) {
                        var t = jQuery.parseJSON(listSalMonth.d);

                        var PayMon = t[0]["PayMonths"];
                        var PayMonID = t[0]["PayMonthsID"];
                        var a = t.length;
                        $('#txtPayMonth').val(PayMon);
                        $('#hdnSalMonthID').val(PayMonID);

                        $('#txtPayMonths').val(PayMon);
                    }
                });
            });
        });

        $(document).ready(function () {
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        var options = {};
                        options.url = "FinalSalaryGeneration.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);

                            var PayMon = t[0]["PayMonths"];
                            var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                            $('#txtPayMonth').val(PayMon);
                            $('#hdnSalMonthID').val(PayMonID);

                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }

                }

            });

        });

        $(document).ready(function () {
            $('#cmdnextMonth').on('click', function () {

                var salMonth = $("#txtPayMonth").val();
                if (salMonth != "") {
                    var msg = "Do You really want to go in next month ?";
                    var buttonText = "YesNo";
                    var E = "{Message: '" + msg + "', buttonText: '" + buttonText + "'}";
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/MessageBoxYesNo',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $('#showMessage').html(D.d);
                            $("#grvVoucherDetail").hide();

                            var res = "Yes";
                            $('#hdnDialogResult').val(res);
                            GoTo_NextMonth();
                        }
                    });
                }
                else
                {
                    var msg = "Salary Month should be not blank.";
                    var E = "{Message: '" + msg + "'}";
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/MessageBox',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $('#showMessage').html(D.d);
                            $("#btnOK").focus();
                        }
                    });
                }

            });
        });

        function GoTo_NextMonth() {
            var result = $('#hdnDialogResult').val();
            if (result == "Yes") {
                if ($("#chkSelect").is(':checked')) {
                    var secid = $("#ddlSector").val(); //alert(secid);
                    if (secid != 0) {

                        $(".loading-overlay").show();
                        var salmonthid = $("#hdnSalMonthID").val();


                        var F = "{salmonthid: '" + salmonthid + "', secid: '" + secid + "'}";
                        //alert(F);
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/GoForNextMonth',
                            data: F,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $(".loading-overlay").hide();
                                //var data = JSON.parse(D.d);

                                var msg = "You have Successfully inserted into next month for salary process.";
                                var E = "{Message: '" + msg + "'}";
                                $.ajax({
                                    type: "POST",
                                    url: pageUrl + '/MessageBox',
                                    data: E,
                                    cache: false,
                                    contentType: "application/json; charset=utf-8",
                                    success: function (D) {
                                        $('#showMessage').html(D.d);
                                        $("#grvVoucherDetail").hide();

                                        var res = "success";
                                        $('#hdnDialogResult').val(res);
                                        location.reload();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        var msg = "Please select Sector.";
                        var E = "{Message: '" + msg + "'}";
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/MessageBox',
                            data: E,
                            cache: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $('#showMessage').html(D.d);
                                $("#grvVoucherDetail").hide();

                                //var res = "checkbox";
                                //$('#hdnDialogResult').val(res);
                            }
                        });
                    }
                }
                else {
                    var msg = "Please select checkbox first.";
                    var E = "{Message: '" + msg + "'}";
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/MessageBox',
                        data: E,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            $('#showMessage').html(D.d);
                            $("#grvVoucherDetail").hide();

                            var res = "checkbox";
                            $('#hdnDialogResult').val(res);
                        }
                    });
                }
            }
        }
        //$(document).ready(function () {
        //    $('#btnYes').on('click', function () {
                
        //    });
        //});
        //$(document).ready(function () {
        //    $('#btnYes').live('click', function () {
        //        var result = $('#hdnDialogResult').val();
        //        if (result == "Yes") {
        //            if ($("#chkSelect").is(':checked')) {
        //                $(".loading-overlay").show();
        //                progress();
                        
        //           }
        //            else {
        //                var msg = "Please select checkbox first.";
        //                var E = "{Message: '" + msg + "'}";
        //                $.ajax({
        //                    type: "POST",
        //                    url: pageUrl + '/MessageBox',
        //                    data: E,
        //                    cache: false,
        //                    contentType: "application/json; charset=utf-8",
        //                    success: function (D) {
        //                        $('#showMessage').html(D.d);
        //                        $("#grvVoucherDetail").hide();

        //                        var res = "checkbox";
        //                        $('#hdnDialogResult').val(res);
        //                    }
        //                });
        //            }
        //        }
        //    });
        //});
        $(document).ready(function () {
            $('#btnOK').on('click', function () {
                var result = $('#hdnDialogResult').val(); //alert(result);
                if (result == "success")
                    window.location.href = "FinalSalaryGeneration.aspx";
            });
        });

        $(document).ready(function () {
            var salmonth = $("#hdnSalMonth").val(); //alert(salmonth);
            var arr = salmonth.split('/');
            var salmon = arr[0]; //alert(salmon);
            var salyear = arr[1]; //alert(salyear);

            var Monthdate = getLastDateOfMonth(parseInt(salyear), parseInt(salmon)); //alert(Monthdate);
            var lastdate = Monthdate.getDate(); //alert(lastdate);
            var lasttwodate = parseInt(lastdate) +1; //alert(lasttwodate);

            var newdate = new Date(parseInt(salyear), parseInt(salmon) - 1, lasttwodate); //alert(newdate);
            var today = new Date(); //alert(today);

            var EncodeValue = ""; var NextFlag = "";
            var F = "{salmonth: '" + salmonth + "'}";
            //alert(F);
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetEncodedValue',
                data: F,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var data = JSON.parse(D.d);
                    EncodeValue = data;

                    var K = "{salmonth: '" + salmonth + "', SecID: '" + $("#ddlSector").val() + "'}";
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/Get_ForNextMonth',
                        data: K,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            var data = JSON.parse(D.d);
                            NextFlag = data; //alert(NextFlag);

                            if (NextFlag == "1") {
                                //alert("false");
                                $("#chkSelect").attr("disabled", false);
                                $("#cmdnextMonth").attr("disabled", false);
                            }
                            else {
                                //alert("true");
                                $("#chkSelect").attr("disabled", true);
                                $("#cmdnextMonth").attr("disabled", true);
                            }
                        }
                    });

                }
            });
        });
        function getLastDateOfMonth(Year, Month) {
            return (new Date((new Date(Year, Month, 1)) - 1));
        }


    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
        <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Final Salary Generation</h2>						
						</section>

                        <asp:HiddenField ID="hdnSalMonthID" runat="server" ClientIDMode="Static" Value=""  />
                         <asp:HiddenField ID="hdnSalMonth" runat="server" ClientIDMode="Static" Value=""   />
                        <asp:HiddenField ID="hdnDialogResult" runat="server" ClientIDMode="Static" Value="" />

                    <div id="showMessage" runat="server" clientidmode="Static" style="z-index: 1300 !important;position: fixed;_position: absolute; "></div>

                        <div class="loading-overlay">
                            <div class="loadwrapper">
                                <div class="ajax-loader-outer">Processing...</div>
                            </div>
                        </div>


                   
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" ><span class="headFont">Sal Month</span> </td>
                                        <td style="padding:5px;" class="style2" >:</td>
                                        <td style="padding:5px; width:210px;" align="left" >
                                        <asp:TextBox ID="txtPayMonth" ClientIDMode="Static" autocomplete="off" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>
                                        </td>

                                        <td><asp:CheckBox ID="chkSelect" runat="server" ClientIDMode="Static" Text="Next Month" class="headFont" style="float:left;text-align:left;" /></td>
                                       
                                    </tr>
                                </table>

                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdnextMonth" runat="server" Text="Go to Next Month"   
                                        Width="120px" CssClass="Btnclassname DefaultButton"/> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>

                                    </div>       
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                                     
                     
                  
                   
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

