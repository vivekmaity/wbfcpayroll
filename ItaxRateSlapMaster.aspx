﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ItaxRateSlapMaster.aspx.cs" Inherits="ItaxRateSlapMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">
    <script src="js/ItaxRateSlapMaster.js"></script>
     <style>
        #tbl {
            border-collapse: collapse;
            width: 100%;
        }

        #tbl th, #tbl td {
            text-align: left;
            padding: 8px;
        }

       #tbl tbody tr:nth-child(even) {
            background-color: #d2d5d8;
        }

        #tbl thead {
            background-color: #007697;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2 class="headFont">Income Tax Rate Slap Master</h2><label class="headFont"> (  <span style="color: red">*</span> Mandatory Fields )</label>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label class="headFont">Financial Year</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" id="txtFinYear" name="FinYear" class="textbox" style="width: 100px;" autocomplete="off" disabled />
                                                            <input type="hidden" id="hdnRateSlapID" name="RateSlapID" />

                                                        </td>
                                                        <td>
                                                            <label class="headFont">Gender</label>
                                                            <span style="color: red">*</span>
                                                            <select id="ddlGender" name="Gender" class="textbox"  style="height: 25px; width: 150px;">
                                                                <option value="">Select a Gender</option>
                                                                <option value="M">Male</option>
                                                                <option value="F">Female</option>
                                                                <option value="S">Senior Citizen</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="headFont">Amount From</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" id="txtAmountFrom" name="AmountFrom"  autocomplete="off" style="width: 100px;" class="textbox money" />
                                                        </td>

                                                        <td>
                                                            <label class="headFont">Amount To</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" id="txtAmountTo" name="AmountTo"  autocomplete="off" style="width: 100px;" class="textbox money" />
                                                        </td>
                                                        <td>
                                                            <label class="headFont">Add Amount</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" id="txtAddAmount" autocomplete="off" name="AddAmount" style="width: 100px;" class="textbox money" />
                                                        </td>
                                                        <td>
                                                            <label class="headFont">Percentage</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" id="txtPercentage" autocomplete="off" name="percentage" style="width: 100px;" class="textbox money" />
                                                        </td>
                                                        <td style="padding-top: 10px;">
                                                            <input type="button" id="btnSave" value="Save" name="Save" class="Btnclassname" />
                                                            <input type="button" id="btnRefresh" name="Refresh" class="Btnclassname" value="Refresh" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="max-height: 500px; overflow-y: scroll;">
                                                    <table id="tbl">
                                                        <thead>
                                                            <tr>
                                                                <td style="display:none;">
                                                                    <label>ID</label>
                                                                </td>
                                                                <td>
                                                                    <label>Sl. No.</label>
                                                                </td>
                                                                <td style="display:none;">
                                                                    <label>Financial Year</label>
                                                                </td>
                                                                <td style="display:none;">
                                                                    <label>GenderID</label>
                                                                </td>
                                                                <td>
                                                                    <label>Gender</label>
                                                                </td>
                                                                <td>
                                                                    <label>Amount</label>
                                                                </td>
                                                                <td>
                                                                    <label>Amount From </label>
                                                                </td>
                                                                <td>
                                                                    <label>Amount To</label>
                                                                </td>
                                                                <td>
                                                                    <label>Percentage</label>
                                                                </td>
                                                                <td>
                                                                    <label>Update</label>
                                                                </td>
                                                                <td>
                                                                    <label>Delete</label>
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

