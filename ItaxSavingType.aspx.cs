﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using DataAccess;

public partial class ItaxSavingType : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }   

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public static string Get_Data()
    {
        try
        {
            DataSet ds = DBHandler.GetResults("LoadItaxSavingType");
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    [WebMethod]
    public static string Save(string ItaxSaveID,string SavingTypeAbbr, string SavingTypeDesc, string MaxAmount, string ItaxTypeID, string IsActive)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("InstUpdtItaxSavingType", ItaxSaveID.Equals("")?0:(object)ItaxSaveID, SavingTypeAbbr, SavingTypeDesc,
                MaxAmount, ItaxTypeID, IsActive, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string Delete(string ID)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("Delete_ItaxSavingType",ID);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}