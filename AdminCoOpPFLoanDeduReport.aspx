﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AdminCoOpPFLoanDeduReport.aspx.cs" Inherits="AdminCoOpPFLoanDeduReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

     <script type="text/javascript"> 

         //This is for 'Location' Binding on the Base of 'Sector'
         var hdnpaymonthID = '';
        $(document).ready(function () {
            $("#ddlSector").change(function () {    
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                              
                        var options = {};
                        options.url = "AdminCoOpPFLoanDeduReport.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;                      
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            var PayMon = t[0]["PayMonths"];
                            var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                            $('#txtPayMonth').val(PayMon);
                          //  $('#hdnSalMonthID').val(PayMonID);
                            hdnpaymonthID = PayMonID;

                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }

                }              
                else {
                    $('#txtPayMonth').val('');
                }

            });

        });

        function opentab() {
            var SalaryFinYear = "";
            var SalMonth = "";
            var SectorID = "";
            var EmpType = "";
            var EmpStatus = "";          
            var DeduEdid = "";
            var LoanEdid = "";
            var UserId = "";
            var ReportName = "";
            UserId = 1;
            SalaryFinYear = $("#txtSalFinYear").val();
            SalMonth = hdnpaymonthID;
            SectorID = $("#ddlSector").val();
           
            ReportName = $("#ddlReportName").val();
            if (SectorID == "0") {
                alert("Please select Sector");
                $('#ddlSector').focus();
                return false;
            }

            if (SalMonth == "") {
                alert("Please Select Sector");
                $('#ddlSector').focus();
                return false;
            }

            if ($('#ddlEmpType').val() == "0") {
                alert("Please select Employee Type");
                $('#ddlEmpType').focus();
                return false;
            }

            if ($('#ddlStatus').val() == "0") {
                alert("Please select Status");
                $('#ddlStatus').focus();
                return false;
            }

            if (ReportName == "0") {
                alert("Please select Report Name");
                $('#ddlReportName').focus();
                return false;
            }

            if ($('#ddlEmpType').val() == "1") {
                EmpType = "K";
            }
            else if ($('#ddlEmpType').val() == "2") {
                EmpType = "S";
            }
            else if ($('#ddlEmpType').val() == "3") {
                EmpType = "C";          
            }

            if ($('#ddlStatus').val() == "1") {           
                EmpStatus = "Y";
            }
            else if ($('#ddlStatus').val() == "2") {
                EmpStatus = "S";
            }
            else if ($('#ddlStatus').val() == "3") {
                EmpStatus = "R";
            }
            else if ($('#ddlStatus').val() == "4") {
                EmpStatus = "Y,S";
            }
            if (ReportName == 1) {
                DeduEdid = 22;
                LoanEdid = 315;
            }           
            else if (ReportName == 2) {
                DeduEdid = 14;
                LoanEdid = 311;       
            }
            else if (ReportName == 3) {
                DeduEdid = 33;
                LoanEdid = 311;
            }

            var E = "{SalaryFinYear:'" + SalaryFinYear + "',SalMonth:" + SalMonth + ",EmpType:'" + EmpType + "',EmpStatus:'" + EmpStatus + "',DeduEdid:" + DeduEdid + ",LoanEdid:" + LoanEdid + ",UserId:" + UserId + ",ReportID:" + ReportName + "}";

            $.ajax({
                type: "POST",
                url: "AdminCoOpPFLoanDeduReport.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8"

            });

            window.open("AdminCoOpPFLoanDeduReportView.aspx?SalaryFinYear=" + SalaryFinYear + "&SalMonthId=" + SalMonth + "&EmpType=" + EmpType + "&EmpStatus=" + EmpStatus + "&DeduEdid=" + DeduEdid + "&LoanEdid=" + LoanEdid + "&UserId=" + UserId + "&ReportID=" + ReportName + "");
        }
                
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 99px;
        }
        .style3
        {
            width: 113px;
        }
        .style4
        {
            width: 46px;
        }
        .style5
        {
            width: 157px;
        }
        .style6
        {
            width: 158px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Admin CO-OP ,CPF & PF (Deduction & Loan)</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" >
                                        <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>

                                        </td>
                                        <td style="padding:5px;" class="style3"><span class="headFont">Employee Type</span></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" class="style5" >
                                        <asp:DropDownList ID="ddlEmpType" Width="180px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">Select Employee Type</asp:ListItem>
                                            <asp:ListItem Value="1">WBFC</asp:ListItem>
                                            
                                        </asp:DropDownList>   
                                        </td>

                                         <td style="padding:5px;" class="style4"><span class="headFont">Status </span></td>
                                         <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" class="style6" >
                                        <asp:DropDownList ID="ddlStatus" Width="160px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">Select Status</asp:ListItem>
                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                            <asp:ListItem Value="2">Suspended</asp:ListItem>
                                            <asp:ListItem Value="3">Re-Employed</asp:ListItem>
                                            <asp:ListItem Value="4">Active & Suspended</asp:ListItem>
                                        </asp:DropDownList>    

                                        </td>
                                         
                                      <td style="padding:5px;" class="style2"><span class="headFont">Report Name</span></td>
                                      <td style="padding:5px;" >:</td>
                                      <td style="padding:5px;" align="left" >
                                    <asp:DropDownList ID="ddlReportName" Width="160px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">Select Report Name</asp:ListItem>
                                            <asp:ListItem Value="1">CO-OP</asp:ListItem>
                                            <asp:ListItem Value="2">PF</asp:ListItem>
                                            <asp:ListItem Value="3">CPF</asp:ListItem>
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                       <asp:HiddenField ID="hdnSalMonthID" runat="server"  />
                                   
                                    </td>
                                   
                                </tr>
                                
                                </table>
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>