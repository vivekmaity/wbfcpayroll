﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridviewTemplete.ascx.cs"  Inherits="GridviewTemplete" %>

<link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />


       <style type="text/css">           
            .ModeAction
            {
                  font-size:14px; font-weight:bold; text-decoration:none;      
            }
           .HideColumn {
               display: none;
           }
        </style>
<script type="text/javascript">

$(document).ready(function () {
        $(".DefaultButton").click(function (event) {
            event.preventDefault();
        });
});



</script>

<form id="form1" runat="server">
<%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="tblEmpdetailss" runat="server" align="center" AutoGenerateColumns="False"
            ClientIDMode="Static" Width="100%" DataKeyNames="EmpNo" >

            <Columns>
                <asp:TemplateField HeaderText="SectorID" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblSectorID" runat="server" Visible="false" Text='<%# Bind("SectorID") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CenterID" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblCenterID" runat="server" Visible="false" Text='<%# Bind("CenterID") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmpNo" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmpName" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Designation" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblDesignatione" runat="server" Visible="true" Text='<%# Bind("Designation") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Previous Sector" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblSectorName" runat="server" Visible="true" Text='<%# Bind("SectorName") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Previous Location" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblCenterName" runat="server" Visible="true" Text='<%# Bind("CenterName") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </div>
<%--/*=============================================================*/--%>
    <div>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </div>
<%--/*=============================================================*/--%>
    <div>
        <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
    </div>
<%--/*=============================================================*/--%>
    <div>
        <asp:GridView ID="gvGPF" runat="server" align="center" AutoGenerateColumns="False"
            ClientIDMode="Static" Width="100%" DataKeyNames="SectorID" ShowFooter="true"
            CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" >

            <AlternatingRowStyle BackColor="#FFFACD" />

            <Columns>
                <asp:TemplateField HeaderStyle-Width="20px" HeaderText="" ShowHeader="true">
                    <HeaderTemplate>
                        <asp:CheckBox runat="server" ID="chkSelectAll" ClientIDMode="static" AutoPostBack="false" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkSector" CommandArgument='<%# Eval("SectorID") %>'
                            ClientIDMode="static" AutoPostBack="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SectorID" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblSectorID" runat="server" Visible="true" Text='<%# Bind("SectorID") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sector" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblSector" runat="server" Visible="true" Text='<%# Bind("Location") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Employee" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblTotalEmployee" runat="server" Visible="true" Text='<%# Bind("Total_Employee") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"  />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total PF Subscription (A)" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblTotalPFSubscription" runat="server" Visible="true" Text='<%# Bind("Total_PF_Subscription") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total PF Recover (B)" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblTotalPFRecover" runat="server" Visible="true" Text='<%# Bind("Total_PF_Recover") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total GPF (A+B)" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblTotalGPF" runat="server" Visible="true" Text='<%# Bind("Total_GPF") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
               <asp:TemplateField Visible="true">
                        <ItemTemplate>
                            <div id="worktype">
                                        <a   href="javascript:void(0);" id="view"    itemid='<%# Eval("SectorID") +"#" + Eval("Location") +"#" + Eval("Total_Employee") %>' >View</a>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                        <asp:Button ID="btnUpdateCheque" runat="server" Text="Update Cheque" Width="115px" CssClass="Btnclassname DefaultButton"  />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" BackColor="White" />
                    </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
<%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="gvGPFEmployeeWise" runat="server" align="center" AutoGenerateColumns="False"
        ClientIDMode="Static" Width="100%" DataKeyNames="EmpNo" ShowFooter="true" CssClass="Grid"
        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" PageSize="100" >
        <%--<AlternatingRowStyle BackColor="#FFFACD" />--%>
        <Columns>
            <asp:TemplateField HeaderStyle-Width="20px" HeaderText="">
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="chkSelect" CommandArgument='<%# Eval("EmpNo") %>'
                        ClientIDMode="static" AutoPostBack="false"  />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpName" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Designation" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDesignation" runat="server" Visible="true" Text='<%# Bind("Designation") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total PF Subscription (A)" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblTotalPFSubscription" runat="server" Visible="true" Text='<%# Bind("Total_PF_Subscription") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total PF Recover (B)" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblTotalPFRecover" runat="server" Visible="true" Text='<%# Bind("Total_PF_Recover") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total GPF (A+B)" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblTotalGPF" runat="server" Visible="true" Text='<%# Bind("Total_GPF") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Decline" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDecline" runat="server" Visible="true" Text='<%# Bind("Declined") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
<%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="GvInstrument" runat="server" Width="100%" align="center" GridLines="Both"
        AutoGenerateColumns="false" DataKeyNames="Type" CssClass="Grid" PagerStyle-CssClass="pgr"
        AlternatingRowStyle-CssClass="alt">
        <AlternatingRowStyle BackColor="#FFFACD" />
        <Columns>
            <asp:BoundField DataField="Type" HeaderText="Type" />
            <asp:BoundField DataField="Bank/Branch" HeaderText="Bank/Branch" />
            <%--<asp:BoundField DataField="BranchName" HeaderText="BranchName" />--%>
        </Columns>
    </asp:GridView>
</div>
<%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="grdLockPermission" runat="server" Width="100%" align="center" GridLines="Both"
        AutoGenerateColumns="false" DataKeyNames="FieldID" CssClass="Grid" PagerStyle-CssClass="pgr"
        AlternatingRowStyle-CssClass="alt">
        <AlternatingRowStyle BackColor="#FFFACD" />
        <Columns>
            <asp:TemplateField Visible="true">
                <ItemTemplate>
                    <div id="worktype">
                        <a href="javascript:void(0);" id="delete" itemid='<%# Eval("FieldID") %>'>Delete</a>
                    </div>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="FieldID" HeaderText="FieldID" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="TableName" HeaderText="TableName" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="TableField" HeaderText="Table Field" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="FieldName" HeaderText="Field Name" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="LockFlag" HeaderText="LockFlag" ItemStyle-HorizontalAlign="Center" />
        </Columns>
    </asp:GridView>
</div>
<%--/*=============================================================*/--%>
    <div>
        <asp:GridView ID="tblBranch" runat="server" Width="100%" align="center" GridLines="Both"
            AutoGenerateColumns="false" DataKeyNames="BranchID" 
            AllowPaging="true" PageSize="10" 
            CssClass="Grid"  AlternatingRowStyle-CssClass="alt" >
            <AlternatingRowStyle BackColor="#FFFACD" />
            <%--<PagerSettings  FirstPageText="First" LastPageText="Last"
                Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />--%>
            <Columns>
               <asp:TemplateField Visible="true">
                        <ItemTemplate>
                            <div id="worktype">
                                        <a   href="javascript:void(0);" id="edit"    itemid='<%# Eval("BranchID") +"#" + Eval("BankID") %>' >Edit</a>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                <asp:BoundField DataField="Branch" HeaderText="Branch Name" />
                <asp:BoundField DataField="IFSC" HeaderText="IFSC" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="MICR" HeaderText="MICR" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="BankName" HeaderText="Bank Name" />

            </Columns>
        </asp:GridView>
    </div>
    <%--/*=============================================================*/--%>
        <div id="msgboxpanel" runat="server"></div>
    <%--/*=============================================================*/--%>
    <div id="tabEmpdetail" runat="server">
        <div style="height: auto; width: 100%">
            <asp:GridView ID="tblSanction" runat="server" Width="100%" align="center" GridLines="Both"
                AutoGenerateColumns="false" CssClass="Grid" PagerStyle-CssClass="pgr"  OnPreRender="Grid_PreRender"
                DataKeyNames="EmployeeID"  PageSize="15"
                AllowPaging="false" 
                AlternatingRowStyle-CssClass="alt" >
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="20px" HeaderText="" ShowHeader="true">
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkSelect" CommandArgument='<%# Eval("EmployeeID") %>'
                                ClientIDMode="static" AutoPostBack="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="EmployeeID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblEmployeeID" runat="server" Visible="false" Text='<%# Bind("EmployeeID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EmpNo">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EmpNo") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EmployeeName">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                            <itemstyle font-names="Verdana" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Loan Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblLoanAmount" runat="server" Text='<%# Bind("LoanAmount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjust Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustAmount" runat="server" Text='<%# Bind("AdjustAmount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Disburse Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblDisburseAmt" runat="server" Text='<%# Bind("DisburseAmt") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total LoanInstall">
                        <ItemTemplate>
                            <asp:Label ID="lblTotLoanInstall" runat="server" Text='<%# Bind("TotLoanInstall") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sanction Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtSancDate" CssClass="textbox" Height="17px" Text='<%# Eval("LoanSancDate") %>'
                                runat="server" Width="100px"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPaymentDate" CssClass="textbox" Height="17px" Text='<%# Eval("PaymentDate") %>'
                                runat="server" Width="100px" Enabled="false"></asp:TextBox>
                          
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sanction">
                        <ItemTemplate>
                            <div id="workSanction">
                                <asp:LinkButton ID="Sanction" CssClass="ModeAction DefaultButton" runat="server" CommandArgument='<%# Eval("EmpNo") %>'>Sanction</asp:LinkButton>
                                <asp:HiddenField ID="hdnSancEmpNo" runat="server" Value='<%# Eval("LoanSancDate") %>' />
                                <%--<a href="javascript:void(0);" id="Sanction" runat="server" itemid='<%# Eval("EmployeeID") %>'>Sanction</a>--%>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <div id="workEdit">
                                <asp:LinkButton ID="Edit" runat="server"  CssClass="ModeAction DefaultButton" CommandArgument='<%# Eval("EmpNo") %>'>Edit</asp:LinkButton>
                                <asp:HiddenField ID="hdnEditEmpNo" runat="server" Value='<%# Eval("EmpNo") %>' />
                                <%--<a href="javascript:void(0);" id="Edit" itemid='<%# Eval("EmployeeID") %>'>Edit</a>--%>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sanction Order">
                        <ItemTemplate>
                            <div id="workInst">
                                <asp:LinkButton ID="Print" runat="server" CssClass="ModeAction DefaultButton" CommandArgument='<%# Eval("EmpNo") %>'>Print</asp:LinkButton>
                                <asp:HiddenField ID="hdnInstEmpNo" runat="server" Value='<%# Eval("EmpNo") %>' />
                                <%--<a href="javascript:void(0);" id="Instrument" itemid='<%# Eval("EmployeeID") %>'>Instrument Details</a>--%>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            


        </div>
    </div>
    <%--/*=============================================================*/--%>
    <div id="PFBankReceipt" runat="server">
        <div style="height: auto; width: 100%">
            <asp:GridView ID="gvPFBankReceipt" runat="server" Width="100%" align="center" GridLines="Both"
                AutoGenerateColumns="false" CssClass="Grid" PagerStyle-CssClass="pgr"  
                DataKeyNames="EmployeeID"  PageSize="8"
                AllowPaging="false" 
                AlternatingRowStyle-CssClass="alt" >
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="20px" HeaderText="">
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkPFBankReceipt" CommandArgument='<%# Eval("EmpNo") %>'
                                ClientIDMode="static" AutoPostBack="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EmployeeID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblEmployeeID" runat="server" Visible="false" Text='<%# Bind("EmployeeID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EmpNo">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EmpNo") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EmployeeName">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                            <itemstyle font-names="Verdana" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Loan Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblLoanAmount" runat="server" Text='<%# Bind("LoanAmount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Adjust Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAdjustAmount" runat="server" Text='<%# Bind("AdjustAmount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Disburse Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblDisburseAmt" runat="server" Text='<%# Bind("DisburseAmt") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Font-Names="Verdana" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sanction Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtSancDate" CssClass="textbox"  Height="17px" Enabled="false" DataFormatString="{0:dd/MM/yyyy}"  Text='<%#Eval("BankGenerateDate","{0:dd/MM/yyyy}") %>'
                                runat="server" Width="100px"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </div>
    </div>
    <%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="gvPensionerDetail" runat="server" Width="100%" align="center" GridLines="Both"
        AutoGenerateColumns="false" DataKeyNames="EmpNo" CssClass="Grid" PagerStyle-CssClass="pgr"
        AlternatingRowStyle-CssClass="alt">
        <AlternatingRowStyle BackColor="#FFFACD" />
        <Columns>
            <asp:TemplateField Visible="true">
                <ItemTemplate>
                    <div id="worktype">
                        <a href="javascript:void(0);" id="delete" itemid='<%# Eval("EmpNo") %>'>View Salary Details</a>   |   <a href="javascript:void(0);" id="A1" itemid='<%# Eval("EmpNo") %>'>Accept</a>
                    </div>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpName" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Designation" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDesignation" runat="server" Visible="true" Text='<%# Bind("Designation") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="SectorName" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSectorName" runat="server" Visible="true" Text='<%# Bind("SectorName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DOB" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDOB" runat="server" Visible="true" Text='<%# Eval("DOB", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DOJ" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDOJ" runat="server" Visible="true" Text='<%# Eval("DOJ", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DOR" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDOR" runat="server" Visible="true" Text='<%# Eval("DOR", "{0:dd/MM/yyyy}") %>' ></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"  />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
<%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="grvUpdateDaHraMa" runat="server" Width="100%" align="center" GridLines="Both"
        AutoGenerateColumns="false" DataKeyNames="EmpNo" CssClass="Grid" PagerStyle-CssClass="pgr"
        AlternatingRowStyle-CssClass="alt">
        <AlternatingRowStyle BackColor="#FFFACD" />
        <Columns>
            <asp:TemplateField HeaderStyle-Width="20px" HeaderText="">
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="chkSelect" CommandArgument='<%# Eval("EmpNo") %>'
                        ClientIDMode="static" AutoPostBack="false"  />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpName" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblCenterName" runat="server" Visible="true" Text='<%# Bind("CenterName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Old DA/HRA" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblOldDa" runat="server" Visible="true" Text='<%# Bind("OldDa") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="New DA/HRA" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblNewDa" runat="server" Visible="true" Text='<%# Bind("NewDa") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Decline" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDecline" runat="server" Visible="true" Text='<%# Bind("Declined") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
    <%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="grvVoucherDetail" runat="server" Width="100%" align="center" GridLines="Both"
        AutoGenerateColumns="false" DataKeyNames="AccountCode" CssClass="Grid" PagerStyle-CssClass="pgr"
        AlternatingRowStyle-CssClass="alt">
        <AlternatingRowStyle BackColor="#FFFACD" />
        <Columns>
            <asp:TemplateField HeaderText="Old AccountCode" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblOldAccountCode" runat="server" Visible="true" Text='<%# Bind("OldAccountHead") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Account Description" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblAccDesc" runat="server" Visible="true" Text='<%# Bind("AccountDescription") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="left" Width="350px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Account Code" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblAccountCode" runat="server" Visible="true" Text='<%# Bind("AccountCode") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
    <%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="gvPFPaymentDetail" runat="server" align="center" AutoGenerateColumns="False"
        ClientIDMode="Static" Width="100%" DataKeyNames="EmpNo" ShowFooter="true" CssClass="Grid"
        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" PageSize="100" >
        <%--<AlternatingRowStyle BackColor="#FFFACD" />--%>
        <Columns>
            <asp:TemplateField HeaderText="Sl No." Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSlNo" runat="server" Visible="true" Text='<%# Bind("SlNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EmpName" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Total PF Subscription (A)" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDisburseAmt" runat="server" Visible="true" Text='<%# Bind("DisburseAmt") %>'></asp:Label><br />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
        <%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="gvPensionPaymentDetail" runat="server" align="center" AutoGenerateColumns="False"
        ClientIDMode="Static" Width="100%" DataKeyNames="EmpNo" ShowFooter="false" CssClass="Grid"
        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" PageSize="100" >
        <AlternatingRowStyle BackColor="#FFFACD" />
        <Columns>
            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pensioner Name" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblPensionerName" runat="server" Visible="true" Text='<%# Bind("PensionerName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DOR" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDOR" runat="server" Visible="true" Text='<%# Bind("DOR") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Total" Visible="true">
                <ItemTemplate>
                    <table border="0" style="width:100%;">
                        <%--This is for CVP--%>
                        <tr>
                            <td style="background-color:aqua;font-weight:bold; visibility:hidden;">
                                <asp:Label ID="lblCVP" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>' ></asp:Label>
                            </td>
                            <td style="background-color:aqua;font-weight:bold;">
                                <span>CVP</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCVP" CssClass="textbox" Height="16px" Enabled="false" Text='<%# Bind("CVP") %>'
                                    runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="background-color:aqua;">
                                <img id="failCVP" src="img/icons/fail.gif" alt="CVPS" title="CVP" style="Height:16px; Width:15px;" />
                                <img id="successCVP" src="img/icons/success.gif" alt="CVPS" title="CVP" style="Height:16px; Width:15px;" />
                            </td>
                            <td style="background-color:aqua;">
                                <asp:CheckBox ID="chkPropersitionCVP" ClientIDMode="Static" Text="Proportion" runat="server" style="font-weight:bold;" />
                            </td>
                        </tr>
                         <%--This is for Gratuity--%>
                        <tr>
                            <td style="background-color:aqua;font-weight:bold;visibility:hidden;">
                                <asp:Label ID="lblGratuity" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                            </td>
                            <td style="background-color:aqua;font-weight:bold;">
                                <span>Gratuity</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFinalGratuity" CssClass="textbox" Height="16px" Enabled="false" Text='<%# Bind("FinalGratuity") %>'
                                    runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="background-color:aqua;">
                                <img id="failGratuity" src="img/icons/fail.gif" alt="Gratuitys" title="Gratuity" style="Height:16px; Width:15px;" />
                                <img id="successGratuity" src="img/icons/success.gif" alt="Gratuitys" title="Gratuity" style="Height:16px; Width:15px;" />
                            </td>
                            <td style="background-color:aqua;">
                                <asp:CheckBox ID="chkPropersitionGratuity" ClientIDMode="Static" Text="Proportion" runat="server" style="font-weight:bold;"  />
                            </td>
                        </tr>
                        <%--This is for Arrear Pension--%>
                        <tr>
                            <td style="background-color: aqua; font-weight: bold; visibility: hidden;">
                                <asp:Label ID="lblArrear" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                            </td>
                            <td style="background-color: aqua; font-weight: bold;">
                                <span>Arrear</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtArrearPension" CssClass="textbox" Height="16px" Enabled="false" Text='<%# Bind("ArrearPension") %>'
                                    runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="background-color: aqua;">
                                <img id="failArrear" src="img/icons/fail.gif" alt="Arrears" title="Arrear" style="Height: 16px; Width: 15px;" />
                                <img id="successArrear" src="img/icons/success.gif" alt="Arrears" title="Arrear" style="Height: 16px; Width: 15px;" />
                            </td>
                            <td style="background-color: aqua;">
                                <asp:CheckBox ID="chkPropersitionArrear" ClientIDMode="Static" Text="Proportion" runat="server" Style="font-weight: bold;" />
                            </td>
                        </tr>
                        <%--This is for Total--%>
                        <tr>
                            <td style="background-color:aqua;font-weight:bold;visibility:hidden;"></td>
                            <td style="background-color:aqua; font-weight:bold;">
                                <span>Total</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTotal" CssClass="textbox" Height="16px" Enabled="false" Text='<%# Bind("Total") %>'
                                    runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td style="background-color:aqua;"></td><td style="background-color:aqua;"></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
        <%--/*=============================================================*/--%>
    <div>
    <asp:GridView ID="grvAddPaymentDetail" runat="server" align="center" AutoGenerateColumns="False"
        ClientIDMode="Static" Width="100%" DataKeyNames="SlNo" ShowFooter="true" CssClass="Grid"
        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" PageSize="100" >
        <Columns>
            <asp:TemplateField HeaderText="Sl No." Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblSlNo" runat="server" Visible="true" Text='<%# Bind("SlNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Net Amount" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblProportionAmount" runat="server" Visible="true" Text='<%# Bind("ProportionAmount") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Date" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDate" runat="server" Visible="true" Text='<%# Bind("Date") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField Visible="true">
                <ItemTemplate>
                    <div id="worktype">
                        <a href="javascript:void(0);" id="delete" itemid='<%# Eval("SlNo") %>'>
                            <img src="images/delete.png" alt="delete" title="Delete" style="Height: 15px; Width: 15px;" />
                        </a>
                    </div>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText="PaymentVoucherNo" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblPaymentVoucherNo" runat="server" Visible="true" Text='<%# Bind("PaymentVoucherNo") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" CssClass="HideColumn" />
                <HeaderStyle CssClass="HideColumn" />
                <FooterStyle CssClass="HideColumn" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>


    <div id="dvData">
        <table id="myTable" ></table>
    </div>
</form>