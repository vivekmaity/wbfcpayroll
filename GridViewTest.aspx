﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="GridViewTest.aspx.cs" Inherits="GridViewTest" %>

<%@ Register TagPrefix="arts" TagName="EmpRecord" Src="GridViewTemplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <script type="text/javascript">
        var MoveRowUp = "table[id*=grdEmp]"; //Instance of MoveUp btn
        var MoveRowDown = "table[id*=grdEmp]";//Instance of MoveDown btn

        $(document).ready(function () {
            //DisableRow(); //This function disables first and last row
            //This function Moves up the GridView row
            $(MoveRowUp).click(function () {               
                $(this).parents("tr").insertBefore($(this).parents("tr").prev())            
                //DisableRow();
            });

            //This function Moves down the Gridview row
            $(MoveRowDown).click(function () {
                $(this).parents("tr").insertAfter($(this).parents("tr").next());               
                //DisableRow();
            });
            //This function disables first and last row
//            function DisableRow() {
//                $("#<%=grdEmp.ClientID%> tr:has(td)).attr("disabled", false);
//                $("#<%=grdEmp.ClientID%> tr:has(td):first input[id*='btnUp']").attr("disabled", true);
//                $("#<%=grdEmp.ClientID%> tr:has(td) input[id*='btnDown']").attr("disabled", false);
//                $("#<%=grdEmp.ClientID%> tr:last input[id*='btnDown']").attr("disabled", true);
//            }
        });       
                 
    </script>


    <script type="text/javascript">
    var SelectedRow = null;
    var SelectedRowIndex = null;
    var UpperBound = null;
    var LowerBound = null;


    $(document).ready(function () {
        window.onload = function () {
            UpperBound = parseInt('<%= this.grdEmp.Rows.Count %>') - 1; //alert(UpperBound);
            LowerBound = 0;
            SelectedRowIndex = 1;

            for (var i = 0; i < UpperBound; i++) {
                SelectRow(this, i);
                //SelectSibling(e);
                //return false;
            }
        }
    });



    function SelectRow(CurrentRow, RowIndex)
    {
        if(SelectedRow == CurrentRow || RowIndex > UpperBound || RowIndex < LowerBound)
            return;
         
        if(SelectedRow != null)
        {
            SelectedRow.style.backgroundColor = SelectedRow.originalBackgroundColor;
            SelectedRow.style.color = SelectedRow.originalForeColor;
        }
        
        if(CurrentRow != null)
        {
            CurrentRow.originalBackgroundColor = CurrentRow.style.backgroundColor;
            CurrentRow.originalForeColor = CurrentRow.style.color;
            CurrentRow.style.backgroundColor = '#DCFC5C';
            CurrentRow.style.color = 'Black';
        } 
        
        SelectedRow = CurrentRow;
        SelectedRowIndex = RowIndex;
        setTimeout("SelectedRow.focus();",0); 
    }

    function SelectSibling(e)
    { 
        var e = e ? e : window.event;
        var KeyCode = e.which ? e.which : e.keyCode;
        
        if(KeyCode == 40)
            SelectRow(SelectedRow.nextSibling, SelectedRowIndex + 1);
        else if(KeyCode == 38)
            SelectRow(SelectedRow.previousSibling, SelectedRowIndex - 1);
        
        return false;
    }

   </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#grdEmp tbody tr').keydown(function (e) {
                var KeyCode = e.which;
                if (KeyCode == 13) {
                    var EmpNo = $(this).children("td:eq(0)").text();

                    var lre = /^\s*/;
                    var rre = /\s*$/;
                    EmpNo = EmpNo.replace(lre, "");
                    EmpNo = EmpNo.replace(rre, "");
                    //alert(EmpNo);

                    var E = "{EmpNo: '" + EmpNo + "'}";
                    $.ajax({
                        type: 'POST',
                        data: E,
                        url: 'GridViewTest.aspx/AjaxGetGridCtrl',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        success: function (D) {
                            //alert(JSON.stringify(D.d));
                            $('#mainDiv').html(D.d);
                        }
                    });
                }
            });


            //=====================================================================================================

        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
<br />
    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <div id="tabs-1" style="float: left; border: solid 2px lightblue; width: 50%; height:207px">
                                                    <asp:GridView ID="grdEmp" runat="server" align="center" AutoGenerateColumns="False"
                                                        ClientIDMode="Static" Width="100%" DataKeyNames="EmpNo" OnRowCreated="grdEmp_OnRowCreated">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SectorID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSectorID" runat="server" Visible="false" Text='<%# Bind("SectorID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CenterID" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCenterID" runat="server" Visible="false" Text='<%# Bind("CenterID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="EmpNo" Visible="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Visible="true" Text='<%# Bind("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="EmpName" Visible="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpName" runat="server" Visible="true" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Designation" Visible="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignatione" runat="server" Visible="true" Text='<%# Bind("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Previous Sector" Visible="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSectorName" runat="server" Visible="true" Text='<%# Bind("SectorName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Previous Location" Visible="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCenterName" runat="server" Visible="true" Text='<%# Bind("CenterName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div id="tabs-2" style="float: right; border: solid 2px lightblue; width: 49%; height:207px;">
                                                    <div id="mainDiv" runat="server" clientidmode="Static" 
                                                        >
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
</asp:Content>

