﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class HistoryGeneration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string GetSalMonth(int SecID, string SalFinYear)
    {
        //DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonthHistProcess", SecID, SalFinYear);
        //DataTable dt = ds.Tables[0];
        DataTable dtSalMonth = ds1.Tables[0];
        //DataTable dtSalMonthID = ds1.Tables[0];

        List<SectorbyMonth> listSalMonth = new List<SectorbyMonth>();

        if (dtSalMonth.Rows.Count > 0)
        {
            for (int i = 0; i < dtSalMonth.Rows.Count; i++)
            {
                SectorbyMonth objst = new SectorbyMonth();

                //objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                //objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                objst.SalMonth = Convert.ToString(dtSalMonth.Rows[i]["SalMonth"]);
                objst.SalMonthID = Convert.ToInt32(dtSalMonth.Rows[i]["SalMonthID"]);
                listSalMonth.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class SectorbyMonth
    {
        //public int CenterID { get; set; }
        //public string CenterName { get; set; }
        public string SalMonth { get; set; }
        public int SalMonthID { get; set; }
    }

    [WebMethod]
    public static string Report_Paravalue( int SectorID, int SalMonth)
    {
        DataSet ds1 = DBHandler.GetResults("SalaryProcessHistory", SalMonth, SectorID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        String Msg = "History Generation has been Completed Successfully";
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);

    }
}