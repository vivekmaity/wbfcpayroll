﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="CoOperativeCalculation.aspx.cs" Inherits="Co_OperativeCalculation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <link href="css/style.css" rel="stylesheet" />
    <script src="js/Co-Operative.js" type="text/javascript"></script>
    <script type="text/javascript">

        var pageUrl = '<%=ResolveUrl(Page.AppRelativeVirtualPath)%>';
        if (typeof KMDA == 'undefined') {
            KMDA = {};
        };
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2><asp:Label ID="lblHeader" runat="server" Text="" ></asp:Label></h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" align="center" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 2px; ">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <%--<div id="tabs-1" style="border: solid 2px lightblue; width: 100%;">--%>
                                                    <table align="right" width="100%">
                                                        <tr>
                                                            <td style="padding: 2px; float:right;">
                                                                <span class="headFont">Center Name&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;">
                                                                :
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:DropDownList ID="ddlCenter" Width="217px" Height="22px" runat="server" DataValueField="ClassificationID"
                                                                    DataTextField="Classification" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                                    <asp:ListItem Text="(Select Location)" Value=""></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px; float:right;">
                                                                <span class="headFont">Pay Month&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;">
                                                                :
                                                            </td>
                                                            <td style="padding: 3px;" align="left">
                                                                <asp:TextBox ID="txtPayMonth" CssClass="textbox" runat="server" autocomplete="off"></asp:TextBox>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <div style="float: left; margin-left: 200px;">
                                                                    <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="Add" Width="100px"
                                                                        CssClass="Btnclassname DefaultButton" />
                                                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" Width="100px" CssClass="Btnclassname"
                                                                        OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                </div>
                                                           </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <hr style="border: solid 1px lightblue" />
                                                            </td>
                                                        </tr>
                                                            <tr>
                                                            <td style="padding: 10px;">
                                                                <span class="require">*</span> indicates Mandatory Field
                                                            </td>
                                                            <td style="padding: 10px;">
                                                                &nbsp;
                                                            </td>

                                                        </tr>
                                                    </table>
                                                <%--</div>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-overlay" id='loadBookingDet'>
                        <div class="wrapper-outer">
                            <div class="wrapper-inner">
                                <div class="loadContent">
                                    <div class="close-content" onclick="return window.cooperativeMaster.CloseContent()">close</div>
                                    <div style="float: left;">
                                        &nbsp;&nbsp; <span class="headFont" style="font-weight: bold;">Emp No.&nbsp;&nbsp;</span>
                                        <%--<asp:TextBox ID="txtSearchEmp" ClientIDMode="Static" Style="padding-left: 8px;"  runat="server" CssClass="textbox"
                                            Width="70px" Height="13px" MaxLength="6"></asp:TextBox>--%>
                                        
                                        <asp:TextBox ID="txtSearchEmp" ClientIDMode="Static" runat="server" CssClass="textbox"
                                            MaxLength="6" autocomplete="off" ></asp:TextBox>
                                       
                                        <asp:Button ID="btnSearchEmp" ClientIDMode="Static" runat="server" Width="50px" Text="Search" CssClass="DefaultButton" BackColor="#deedf7" />
                                        <asp:Button ID="btnReset" ClientIDMode="Static" runat="server" Width="50px" Text="Reset" CssClass="DefaultButton" BackColor="#deedf7" />
                                    </div><br />
                                    <table cellpadding="5" style="border: solid 1px lightblue; padding-bottom: 10px; width: 650px; overflow: scroll;" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table align="center" cellpadding="5">
                                                    <tr>
                                                        <td>
                                                            <div class='divCaption' id='divCaption'></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="divTable" class="headFont" style=" width:650px; height:440px; overflow:scroll;"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="float: right;">
<%--                                                    <br />
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" ToolTip="Update Details"
                                                        CssClass="save-button DefaultButton Btnclassname" />--%>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                           </div>
                        </div>
                    </div>
</asp:Content>

