﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="PaySlipReport.aspx.cs" Inherits="PaySlipReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        //This is for 'Location' Binding on the Base of 'Sector'
        $(document).ready(function () {
            fin_year();
            ReportType();
            $("#ddlSector").change(function () {
                fin_year();
                return;
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        var options = {};
                        options.url = "PaySlipReport.aspx/GetSalMonth";
                        options.type = "POST";
                        options.data = E;                   
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listSalMonth) {
                            var t = jQuery.parseJSON(listSalMonth.d);
                            var PayMon = t[0]["PayMonths"];
                            var PayMonID = t[0]["PayMonthsID"];
                            var a = t.length;
                            $("#ddlLocation").empty();
                            $('#txtPayMonth').val(PayMon);
                          //  $('#hdnSalMonthID').val(PayMonID);
                            if (a >= 0) {
                                $("#ddlLocation").append("<option value=''>Select Location</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlLocation").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                                });
                            }
                            if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                                document.getElementById("ddlLocation").disabled = false;

                            }

                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }

                }
                else {
                    $('#txtPayMonth').val('');
                    $("#ddlLocation").empty();
                    $("#ddlLocation").append("<option value=''>Select Location</option>")
                    $("#ddlLocation").val(0);
                }

            });
            $('#ddlfinyear').change(function () {
                sal_month();
            });
            $("#ddlReportType").change(function () {
                 var val = $("#ddlReportType").val();
                if (val == '') {
                    //alert('Ple'); return;
                    $('#ddlLocation').val(''); return false;
                }
                var ed = val.split('#')[1];
                $("#rptid").val(val.split('#')[0]);
                if (ed == 1) { document.getElementById("ddlLocation").disabled = false; Location(); }
                else { document.getElementById("ddlLocation").disabled = true; $('#ddlLocation').val(''); }
            });
        });

        $(document).ready(function () {

            //$("#ddlReportType").change(function () {
            //    if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != "0") {
            //        document.getElementById("ddlLocation").disabled = false;

            //    }
            //    else {
            //        $("#ddlLocation").val(0);
            //        document.getElementById("ddlLocation").disabled = true;

            //    }

            //});
        });
        function fin_year() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var E = "{sectorid: " + $('#ddlSector').val() + "}";
            var options = {};
            options.url = "PaySlipReport.aspx/FinYearForReport";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (FinYear) {
                var t = jQuery.parseJSON(FinYear.d);
                var a = t.length;
                if (a >= 0) {
                    //$("#ddlfinyear").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#ddlfinyear").append($("<option></option>").val(value.SalaryFinYear).html(value.SalaryFinYear));
                    });
                    var myVal = $('#ddlfinyear option:last').val();
                    $('#ddlfinyear').val(myVal);
                    sal_month();
                }
            };
            options.error = function () { alert("Error in retrieving Ina year!"); };
            $.ajax(options);
        }

        function sal_month() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var fn_year=$('#ddlfinyear').val();
            if (fn_year == '') {
                alert('Plese Final year'); return;
            }
            var E = "{sectorid: " + $('#ddlSector').val() + ", finYear: '" + fn_year + "'}";
            var options = {};
            options.url = "PaySlipReport.aspx/GetSalMonth";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (salmonth) {
                var t = jQuery.parseJSON(salmonth.d);
                var a = t.length;
                if (a >= 0) {
                    //$("#txtPayMonth").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#txtPayMonth").append($("<option></option>").val(value.Salmonthid+'#'+value.SalaryMonth).html(value.SalaryMonth));
                    });
                    var myVal = $('#txtPayMonth option:last').val();
                    $('#txtPayMonth').val(myVal);
                }
            };
            options.error = function () { alert("Error in retrieving SalMonth!"); };
            $.ajax(options);
        }
        function ReportType() {
            
            var E = "{parentID: " + 1 + ", ReportType: '" + 0 + "'}";
            var options = {};
            options.url = "PaySlipReport.aspx/get_allrpt";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (salmonth) {
                var t = jQuery.parseJSON(salmonth.d);
                var a = t.length;
                if (a >= 0) {
                    $("#ddlReportType").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#ddlReportType").append($("<option></option>").val(value.ReportID).html(value.ReportName));
                    });
                }
            };
            options.error = function () { alert("Error in retrieving Report!"); };
            $.ajax(options);
        }
        function Location() {

            var E = "{SecID: " + $('#ddlSector').val() + "}";
            var options = {};
            options.url = "PaySlipReport.aspx/get_allLocation";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (salmonth) {
                console.log(salmonth);
                var t = jQuery.parseJSON(salmonth.d);
                var a = t.length;
                if (a >= 0) {
                    $("#ddlLocation").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#ddlLocation").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                    });
                }
            };
            options.error = function () { alert("Error in retrieving Report!"); };
            $.ajax(options);
        }
        
        function opentab() {
            //Load_dynamic();
            var SectorID;
            var Centerid;
            var SalMonth;
            var SalMonthID;
            var ReportType = $("#ddlReportType").val().split('#')[0];
            
            SectorID = $("#ddlSector").val();
            SalMonthID = $('#txtPayMonth').val().split('#')[0];
            SalMonth = $('#txtPayMonth').val().split('#')[2];
            Centerid = $('#ddlLocation').val();
            if (Centerid == 'null') { Centerid = 0;}
            if (SectorID == "0") {
                alert("Please select Sector");
                $('#ddlSector').focus();
                return false;
            }

            if (SalMonthID == "") {
                alert("Please Select Sector");
                $('#ddlSector').focus();
                return false;
            }
         
            if (ReportType == "0") {
                alert("Please select Report Type");
                $('#ddlReportType').focus();
                return false;
            }

//            var E = "{SectorID:" + SectorID + ",SalMonth:" + SalMonth + "}";
//          
//            $.ajax({
//                type: "POST",
//                url: "PaySlipReport.aspx/Report_Paravalue",
//                data: E,
//                dataType: "json",
//                contentType: "application/json; charset=utf-8"

//            });           
            if (ReportType == "1") {
                window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&Centerid=" + 0 + "&SalMonth=" + SalMonth + "");
                return false;
            }
            else if (ReportType == "2") {
                window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&Centerid=" + 0 + "&SalMonth=" + SalMonth + "");
                return false;
            }
            else if (ReportType == "3") {
                if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != 0) {
                    window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&Centerid=" + Centerid + "&SalMonth=" + SalMonth + "");
                    return false;
                }
                else {

                    window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&Centerid=" + Centerid + "&SalMonth=" + SalMonth + "");
                    return false;
                }
            }
            else if (ReportType == "4" || ReportType == "41") {
                window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&Centerid=" + 0 + "&SalMonth=" + SalMonth + "");
                return false;
            }
            
        }
        function Load_dynamic() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var fn_year = $('#ddlfinyear').val();
            if (fn_year == '') {
                alert('Plese Select Final year'); return;
            }
            if ($('#txtPayMonth').val() == '') { alert('Plese Select SalMonth'); return; }
            if ($('#ddlReportType').val() == '') { alert('Plese Select Report'); return; }
            var rptID = $("#ddlReportType").val().split("#")[1];
            if (rptID == 1) { if ($("#ddlLocation").val() == '') { alert("Please Select Location"); return; } }
            var CenterID;
            if (rptID == 1) { CenterID = $("#ddlLocation").val(); } else { CenterID = 0;}
            //var E = "{SalmonthID: " + $('#txtPayMonth').val().split("#")[0] + ", salmonth: '" + $('#txtPayMonth').val().split("#")[2] + "'}";
            var E = "{SalmonthID: " + $('#txtPayMonth').val().split("#")[0] + ", salmonth: '" + $('#txtPayMonth').val().split("#")[2] + "', salaryFinYear: '" + $("#ddlfinyear").val() + "', ChildreportID: " + $("#ddlReportType").val().split("#")[0] + ", reportHisCur: '" + $('#txtPayMonth').val().split("#")[1] + "',CenterID:" + CenterID + ",sectorid: " + $('#ddlSector').val() + "}";
		$(".loading-overlay").show();
            var options = {};
            options.url = "PaySlipReport.aspx/get_lodeDynamicRpt";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (salmonth) {
                var t = jQuery.parseJSON(salmonth.d);
                var a = t.length;
                $(".loading-overlay").hide();
                opentab();
            };
            options.error = function () { alert("Error in retrieving SalMonth!"); };
            $.ajax(options);
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>         
							<h2>Pay Report</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  " bgcolor="#669999"  >
                        <tr>
                             <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" bgcolor="White" ><span class="headFont">Fin Year &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" bgcolor="White" >
                                             <asp:DropDownList ID="ddlfinyear" Width="200px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                                <%--<asp:ListItem Value="0">(Select)</asp:ListItem>--%>
                                            </asp:DropDownList> 
                                        </td>

                                        <td style="padding:5px;" class="style1" bgcolor="White" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" bgcolor="White" >
                                            <asp:DropDownList ID="txtPayMonth" Width="165px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                                <%--<asp:ListItem Value="0">(Select)</asp:ListItem>--%>
                                            </asp:DropDownList> 
                                        <%--<asp:TextBox ID="txtPayMonth" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>--%>

                                        </td>
                                        <td style="padding:5px;" class="style3" bgcolor="White"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;" bgcolor="White">:</td>
                                        <td style="padding:5px;" align="left" bgcolor="White" >
                                            <asp:TextBox ID="rptid" autocomplete="off" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" style="display:none;"></asp:TextBox>
                                        <asp:DropDownList ID="ddlReportType" Width="200px" Height="28px" CssClass="textbox"  Enabled="True" runat="server">
                                           <%-- <asp:ListItem Value="0">(Select Report Type)</asp:ListItem>
                                            <asp:ListItem Value="1">Pay Slip</asp:ListItem>
                                            <asp:ListItem Value="2">Pay Summary</asp:ListItem>
                                            <asp:ListItem Value="3">Pay Summary Location Wise</asp:ListItem>
                                             <asp:ListItem Value="4">Pay Register</asp:ListItem>--%>
                                        </asp:DropDownList>   
                                        </td>

                                         <td style="padding:5px;" class="style4" bgcolor="White"><span class="headFont">Location </span></td>
                                         <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" bgcolor="White" >
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="headFont" Width="230px" Height="28px" CssClass="textbox" DataValueField="CenterID" DataTextField="CenterName"
                                        AppendDataBoundItems="true" Enabled="false" ClientIDMode="Static" autocomplete="off">
<asp:ListItem Value="0">(Select Report Type)</asp:ListItem>
                                        </asp:DropDownList>   
                                        </td>
                                         
                                       <%--<asp:HiddenField ID="hdnSalMonthID" runat="server"  />--%>
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" bgcolor="White" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="Load_dynamic(); return false;" /> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                        
                                    </div>
                                    <div class="loading-overlay">
                                        <div class="loadwrapper">
                                            <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                    </div>  
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                            </td>
                        </tr>
                      
                    </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

