﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="RPT_PaySchedule.aspx.cs" Inherits="RPT_PaySchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
           
        $(document).ready(function () {
            var SalFinYear = $("#txtSalFinYear").val();
            $("#txtSalFinalYear").val(SalFinYear);
            $("#hdnSalFinYear").val(SalFinYear);
            
        });
                
        $(document).ready(function () {          
            $.ajax({
                      
                type: "POST",
                url: "RPT_PaySchedule.aspx/GetCenter",
                data: {},
                                 
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var jsdata = JSON.parse(data.d);
//                    $("#ddlCenter").empty();
//                    $("#ddlCenter").append("<option value=''>Select Center</option>")
                    $.each(jsdata, function (key, value) {
//                        $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                    });
                },
                error: function (data) {
                }
            });
        });             

        $(document).ready(function () {
            $.ajax({

                type: "POST",
                url: "RPT_PaySchedule.aspx/GetDeduction",
                data: {},

                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //console.log(data);
                    var jsdata = JSON.parse(data.d);
                    $("#ddlSched").empty();
                    $("#ddlSched").append("<option value=''>Select Schedule Deduction</option>")
                    $.each(jsdata, function (key, value) {
                        $("#ddlSched").append($("<option></option>").val(value.ReportID).html(value.ReportName));
                    });
                },
                error: function (data) {
                }
            });
        });

        $(document).ready(function () {
            fin_year();
            $("#ddlSector").change(function () {
                fin_year(); return;
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "") {
                        var EA = "{SalFinYear: '" + $('#txtSalFinYear').val() + "', SectorId: " + s + "}";
                       
                        $.ajax({
                            type: "POST",
                            url: "RPT_PaySchedule.aspx/GetSalMonth",
                            data: EA,
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (listCenter) {
                                var t = jQuery.parseJSON(listCenter.d);
                                var PayMon = t[0]["PayMonths"];

                                $('#txtCurrMonth').val(PayMon);
                                $('#hdnCurMonth').val(PayMon);


                            },
                            error: function (listCenter) {
                                alert("error found");
                            }
                        });
                    }
                }
            });
        });

        $(document).ready(function () {

            $("#ddlSector1").change(function () {
                return;
                if ($("#ddlSector1").val() != "Please select") {
                    var E = "{SecID: '" + $('#ddlSector1').val() + "'}";
                    var options = {};
                    options.url = "RPT_PaySchedule.aspx/GetSecByCent";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listSecbycent) {
                        var t = jQuery.parseJSON(listSecbycent.d);
                        var a = t.length;
                        $("#ddlCenter").empty();
                        if (a >= 0) {
                            $("#ddlCenter").append("<option value=''>Select Center</option>")
                            $.each(t, function (key, value) {
                                $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        }
                    };
                    options.error = function () { alert("Error in retrieving Center!"); };
                    $.ajax(options);
                }

            });
        });


        function checksector() {

            if (document.getElementById('ChkSectorAll').checked) {
                document.getElementById("ddlSector1").disabled = true;
                $("#ddlSector1").val('0');
                $(document).ready(function () {
                    $.ajax({

                        type: "POST",
                        url: "RPT_PaySchedule.aspx/GetCenter",
                        data: {},

                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlCenter").empty();
                            $("#ddlCenter").append("<option value=''>Select Center</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlCenter").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                            });
                        },
                        error: function (data) {
                        }
                    });
                });
               
            }

            else {
                document.getElementById("ddlSector1").disabled = false;
            }
        }

        function checkcenter() {
            if (document.getElementById('ChkCenterAll').checked) {
                document.getElementById("ddlCenter").disabled = true;
                $("#ddlCenter").val('0');
            }

            else {
                document.getElementById("ddlCenter").disabled = false;
                $("#ddlCenter").append("<option value=''>(Select Center)</option>")
            }
        }

        function checkemptype() {
            if (document.getElementById('ChkEmpTypeAll').checked) {
                document.getElementById("ddlEmpType").disabled = true;
                $("#ddlEmpType").val('0');
               
            }

            else {
                document.getElementById("ddlEmpType").disabled = false;
  
            }
        }

        function checkstatus() {
            if (document.getElementById('ChkStatusAll').checked) {
                document.getElementById("ddlStatus").disabled = true;
                $("#ddlStatus").val('0');

            }

            else {
                document.getElementById("ddlStatus").disabled = false;
            }
        }

        function deduction() {
            if (document.getElementById('RadBDeduction').checked) {
                document.getElementById("RadBLoan").checked = false;
                $(document).ready(function () {
                    $.ajax({

                        type: "POST",
                        url: "RPT_PaySchedule.aspx/GetDeduction",
                        data: {},

                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlSched").empty();
                            $("#ddlSched").append("<option value=''>Select Schedule Deduction</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlSched").append($("<option></option>").val(value.ReportID).html(value.ReportName));
                            });
                        },
                        error: function (data) {
                            //alert("error found");
                        }
                    });
                });
            }

            else {
                document.getElementById("RadBLoan").checked = true;
            }
        }

        function loan() {
            
            if (document.getElementById('RadBLoan').checked) {
                document.getElementById("RadBDeduction").checked = false;

                $(document).ready(function () {
                    $.ajax({
                       type: "POST",
                        url: "RPT_PaySchedule.aspx/GetLoan",
                        data: {},
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlSched").empty();
                            $("#ddlSched").append("<option value=''>Select Schedule Loan</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlSched").append($("<option></option>").val(value.EDID).html(value.EDName));
                            });
                        },
                        error: function (data) { 
                        }
                    });
                });
                
              }


            else {
                document.getElementById("RadBDeduction").checked = true;
            }
        }

        function beforeSave() {
            $("#form1").validate();
                      
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }


        function PrintOpentab() {
            //$(".loading-overlay").show();
            var SalaryFinYear;
            var SalMonth;
            var SectorID;
            var CenterID;
            var EmpType;
            var Status;
            var SchType;
            var SectorGroup;
            var CenterGroup;
            var EmpTypeGroup;
            var StatusGroup;
            var EDID;
            var UserID;

            SalaryFinYear = $('#txtSalFinalYear').val();
            SalMonth = $('#txtCurrMonth').val().split("#")[2];
           
            if (document.getElementById('ChkEmpTypeAll').checked) {
                EmpType = 0;
                EmpTypeGroup = '';
            }
            else {
                if ($("#ddlEmpType").val() == 0) {
                    alert("Please Employee Type");
                    $("#ddlEmpType").focus();
                }
                else {
                    EmpType = $("#ddlEmpType").val();
                    EmpTypeGroup = 'NOTALL';
                } 
            }
            if (document.getElementById('ChkStatusAll').checked) {
                Status = 0;
                StatusGroup = 'ALL';
            }
            else {
                if ($("#ddlStatus").val() == 0) {
                    alert("Please Status");
                    $("#ddlStatus").focus();
                }
                else {
                    Status = $("#ddlStatus").val();
                    StatusGroup = 'NOTALL';
                }
            }
            if (document.getElementById('RadBDeduction').checked) {
                SchType = 'DEDUCTION';
            }
            else {
                SchType = 'LOAN';
            }
            EDID = $('#ddlSched').val();
            if (EDID == "") {
                alert("Please Select Schedule Type");
                $('#ddlSched').focus();
                return false;
            }
            if (document.getElementById('ChkCenterWise').checked) {
                CenterGroup = 'ALL';
            }
            else {
                CenterGroup = 'NOTALL';
            }
             UserID = 1;
             var salmonthID = $('#txtCurrMonth').val().split('#')[0];
             var E = "{SalmonthID: " + salmonthID + ",SalaryFinYear:'" + SalaryFinYear + "',SalMonth:'" + SalMonth + "',SectorID: " + $('#ddlSector').val() + ",CenterID: 0,EmpType: " + EmpType + ",Status: '" + Status + "',EDID: " + EDID.split('#')[2] + ",ChildreportID:" + EDID.split('#')[0] + ",reportHisCur:'" + $('#txtCurrMonth').val().split('#')[1] + "',isPDFExcel:1}";
             
             $(".loading-overlay").show();
             $.ajax({
                
                    type: "POST",
                    url: "RPT_PaySchedule.aspx/Report_Paravalue",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        window.open("ReportView_b.aspx?ReportID=" + EDID.split('#')[0] + "&SalMonth=" + SalMonth + "&ReportTypeID=2");
                        $(".loading-overlay").hide();
                        }     
                });
                
                
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        //Add Bidhan

        function fin_year() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var E = "{sectorid: " + $('#ddlSector').val() + "}";
            var options = {};
            options.url = "PaySlipReport.aspx/FinYearForReport";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (FinYear) {
                var t = jQuery.parseJSON(FinYear.d);
                var a = t.length;
                if (a >= 0) {
                    //$("#ddlfinyear").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#txtSalFinalYear").append($("<option></option>").val(value.SalaryFinYear).html(value.SalaryFinYear));
                    });
                    var myVal = $('#txtSalFinalYear option:last').val();
                    $('#txtSalFinalYear').val(myVal);
                    sal_month();
                }
            };
            options.error = function () { alert("Error in retrieving Ina year!"); };
            $.ajax(options);
        }
        function sal_month() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var fn_year = $('#txtSalFinalYear').val();
            if (fn_year == '') {
                alert('Plese Final year'); return;
            }
            var E = "{sectorid: " + $('#ddlSector').val() + ", finYear: '" + fn_year + "'}";
            var options = {};
            options.url = "PaySlipReport.aspx/GetSalMonth";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (salmonth) {
               
                var t = jQuery.parseJSON(salmonth.d);
                var a = t.length;
                if (a >= 0) {
                    //$("#txtPayMonth").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#txtCurrMonth").append($("<option></option>").val(value.Salmonthid + '#' + value.SalaryMonth).html(value.SalaryMonth));
                    });
                    var myVal = $('#txtCurrMonth option:last').val();
                    $('#txtCurrMonth').val(myVal);
                }
            };
            options.error = function () { alert("Error in retrieving SalMonth!"); };
            $.ajax(options);
        }
    </script>
    <style type="text/css">
        .style7
        {
            height: 31px;
        }
        .style16
        {
            height: 31px;
            width: 129px;
        }
        .style17
        {
            width: 129px;
        }
        .style18
        {
            width: 97px;
        }
        .style19
        {
            width: 341px;
        }
        .style20
        {
            height: 31px;
            width: 341px;
        }
        .style21
        {
            height: 16px;
            width: 129px;
        }
        .style22
        {
            height: 16px;
        }
        .style23
        {
            height: 16px;
            width: 341px;
        }
        .style24
        {
            width: 97px;
            height: 16px;
        }
    </style>
</asp:Content>
          
<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Pay Schedule (Deduction & Loan)</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                        <tr>
                            <td style="padding:15px;">
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style17" ><span class="headFont">Salary Financial Year &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" class="style19" >
                                        <%--<asp:TextBox ID="txtSalFinalYear" Width="190px" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>--%>
                                        <asp:DropDownList ID="txtSalFinalYear" Width="200px" Height="25px" CssClass="textbox" Enabled="true" autocomplete="off" runat="server">
                                        
                                        </asp:DropDownList>

                                        </td>
                                        <td style="padding:5px;" class="style18"><span class="headFont" >Current Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;">:</td>
                                         <td class="style22" >
                                        <%--<asp:TextBox ID="txtCurrMonth" Width="100px" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100" Enabled="false"></asp:TextBox>--%>   
                                        <asp:DropDownList ID="txtCurrMonth" Width="200px" Height="25px" CssClass="textbox" Enabled="true" runat="server" autocomplete="off">
                                        
                                        </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                         <td>
                                        </td>
                                    </tr>
                                    <tr style="display:none;">
                                        <td style="padding:5px;" class="style21"><span class="headFont">Sector&nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" class="style22">:</td>
                                        <td style="padding:5px;" align="left" class="style23" >
                                            <asp:DropDownList ID="ddlSector1" Width="200px" Height="25px" runat="server" DataValueField="SectorID"
                                                        DataTextField="SectorName" Enabled="false" AppendDataBoundItems="true" CssClass="textbox"
                                                        ClientIDMode="Static" autocomplete="off">
                                                        <asp:ListItem Text="Select Sector" Selected="True" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:CheckBox ID="ChkSectorAll" Checked ="true" AutoPostBack="false" onchange ='checksector();'
                                                        CssClass="headFont"  ClientIDMode="Static" runat="server"  Text="All"></asp:CheckBox>
                                                                                            
                                                    <asp:CheckBox ID="ChkSectorWise" CssClass="headFont" Checked="true" runat="server" Text="Group Sector"></asp:CheckBox>
                                        </td>
                                        <td style="padding:5px;" class="style24"><span class="headFont">Center &nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;" class="style22">:</td>
                                        <td class="style22" >
                                            <asp:DropDownList ID="ddlCenter" Width="217px" Height="25px" runat="server" DataValueField="CenterID"
                                                DataTextField="CenterName" Enabled="false" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="Select Center" Selected="True" Value=""></asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:CheckBox ID="ChkCenterAll" Checked ="true" bgcolor="#CCFF66" AutoPostBack="false" onchange ='checkcenter();'
                                                CssClass="headFont"  ClientIDMode="Static" runat="server"  Text="All"></asp:CheckBox>
                                            <asp:CheckBox ID="ChkCenterWise" Checked="true" CssClass="headFont" ClientIDMode="Static" runat="server" Text="Group Center"></asp:CheckBox>
                                        </td>
                                        <td class="style22">
                                        <asp:HiddenField ID="hdnCurMonth" runat="server"  />
                                        </td>
                                       <td class="style22">
                                        </td>
                                    </tr>
                                    <tr>

                                        <td style="padding:5px;" class="style21"><span class="headFont">Employee Type&nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" class="style22">:</td>
                                        <td style="padding:5px;" align="left" class="style23" >
                                            <asp:DropDownList ID="ddlEmpType" Width="200px" Height="25px" CssClass="textbox" Enabled="false" runat="server" autocomplete="off">
                                                <asp:ListItem Value="0">Select Employee Type</asp:ListItem>
                                                <asp:ListItem Value="1">WBFC</asp:ListItem>
                                                <asp:ListItem Value="2">State</asp:ListItem>
                                                <asp:ListItem Value="3">Central</asp:ListItem>
                                            </asp:DropDownList>

                                                <asp:CheckBox ID="ChkEmpTypeAll" Checked ="true" CssClass="headFont" 
                                                    runat="server" AutoPostBack ="false" onchange ='checkemptype();' Text="All"></asp:CheckBox>
                                            </td>
                                        <td style="padding:5px;" class="style24"><span class="headFont">Status &nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;" class="style22">:</td>
                                        <td class="style22" >
                                            <asp:DropDownList ID="ddlStatus" Width="217px" Height="25px" CssClass="textbox" Enabled="false" runat="server" autocomplete="off">
                                                <asp:ListItem Value="0">Select Status</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="2">Suspended</asp:ListItem>
                                                <asp:ListItem Value="3">Re-Employed</asp:ListItem>
                                            </asp:DropDownList>

                                                <asp:CheckBox ID="ChkStatusAll" Checked ="true" CssClass="headFont" 
                                                    runat="server" AutoPostBack="false" Text="All" 
                                                    onchange ='checkstatus();'></asp:CheckBox>

                                            </td>
                                       
                                        <td class="style22">
                                        <asp:HiddenField ID="hdnSalFinYear" runat="server"  />
                                                 
                                        </td>
                                        <td class="style22">
                                        </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:5px;" class="style16"><span class="headFont">Schedule Type&nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" class="style20">
                                            <asp:RadioButton ID="RadBDeduction" CssClass="headFont" runat="server" 
                                                onchange ='deduction();' Checked ="false" ClientIDMode="Static" AutoPostBack="false"
                                                Text="Deduction"></asp:RadioButton>
                                            <asp:RadioButton ID="RadBLoan" CssClass="headFont" runat="server" AutoPostBack="false" 
                                               onchange ='loan();' Checked ="false" ClientIDMode="Static" Text="Loan"></asp:RadioButton>
                                        </td>
                                        <td style="padding:5px;" class="style18" ><span class="headFont">Select Schedule &nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;">:</td>
                                          <td class="style22" >
                                         <asp:DropDownList ID="ddlSched" Width="217px" Height="25px"  runat="server" DataValueField="EDID"
                                                DataTextField="EDName"  Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static"  autocomplete="off">
                                           <asp:ListItem Text="Select Schedule" Selected="True" Value=""></asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                       <td class="style7">
                                        <asp:HiddenField ID="HiddenField2" runat="server"  />
                                        </td>
                                       <td class="style7">
                                        
                                                 
                                        </td>
                                       
                                    </tr> 
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    
                                    <td style="padding:5px;" class="style1"><span class="headFont">* indicates Mandatory Field &nbsp;&nbsp;</span> </td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdShow" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="PrintOpentab(); return false;" 
                                            BorderColor="#669999"/> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Reset"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdReset_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                   <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                        </div>    
                                   
                                    </td>
                                   
                                </tr>
                                
                                </table>
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

