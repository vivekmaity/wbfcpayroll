﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                                                                <div id="tabs-5">
                                                                <%--Upload Photo and Signature--%>
                                                                 <table align="center" id='table1' cellpadding="5" width="100%">
                                                            <tr>
                                                            
                                                                <td class="headFont">
                                                                <div style="float:left; margin:0px;padding:0px; position:absolute;" >
                                                                Upload Photo -&nbsp;&nbsp;<span class="requirefield">*</span>
                                                                Size between 20KB to 50KB (Dimension 138 W X 177 H)  (4.5 cm Height x 3.5 cm Width)
                                                                </div>

                                                                </td>
                                                                
                                                                <td class="labelCaptionbold">:</td>
                                                                <td align="left">
                                                                    <div style="float:left;width:540px;" >
                                                                        <div style="float:left;width:350px;">
                                                                            <input id="fileToUpload" type="file" runat="server"  name="fileToUpload" class="headFont" Text='<%# Eval("Photo") %>' />
                                                                            <button id="buttonUpload" class="button01 DefaultButton">Upload</button>
                                                                            <img id="loading" src="images/ajax-loader_new.gif" style="display: none;">
                                                                            <span id="imgPhoto" style="width: 26px; height: 24px;"></span>
                                                                            <asp:TextBox ID="lblPhoto" runat="server" Style="cursor: not-allowed;" CssClass="text_field01" ReadOnly="true" Text=""></asp:TextBox>
                                                                        </div>
                                                                        <div style="float:left;">
                                                                            <div id="canPhoto" style="width:100px;height: 116px;border: solid 1px #6d760b;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td  class="headFont"> Upload Full Signature -&nbsp;&nbsp;<span class="requirefield">*</span>
                                                                    Size between 10 KB to 20KB (Dimension 250 W X 45 H) (1.1cm Height X 6.6 cm Width)
                                                                </td>
                                                                <td class="labelCaptionbold">:</td>
                                                                <td align="left">
                                                                    <div style="float:left;width:540px;" >
                                                                        <div style="float:left;width:350px;">
                                                                            <input id="fileToUploadSign" type="file"  name="fileToUploadSign" class="headFont" Text='<%# Eval("Signature") %>' />
                                                                            <button id="buttonUploadSign" class="button01 DefaultButton">Upload</button>
                                                                            <img id="loadingSign" src="images/ajax-loader_new.gif" style="display: none;">
                                                                            <span id="imgSign" style="width: 26px; height: 24px;"></span>                                                                    
                                                                            <asp:TextBox ID="lblSign" runat="server" Style="cursor: not-allowed;" CssClass="text_field01" ReadOnly="true" Text=""></asp:TextBox>
                                                                        </div>
                                                                        <div style="float:left;">
                                                                            <div id="canSign" style="width:150px;height: 27px;border: solid 1px #6d760b;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                                </table>
                                                            </div>
    </div>
    </form>
</body>
</html>
