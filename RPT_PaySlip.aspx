﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="RPT_PaySlip.aspx.cs" Inherits="RPT_PaySlip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtPayMonth").prop("disabled", true);
            var SalFinYear = $("#txtSalFinYear").val();
            $("#txtSalFinalYear").val(SalFinYear);
            //            alert(SalFinYear);

        });

      
        //This is for 'Location' Binding on the Base of 'Sector'
        $(document).ready(function () {
            // $("#ddlPayScaleType").prop("disabled", false);
            $("#ddlSector").change(function () {
                var s = $('#ddlSector').val();
                if (s != 0) {
                    if ($("#ddlSector").val() != "Please select") {
                        var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                        //alert(E);
                        var options = {};
                        options.url = "RPT_PaySlip.aspx/GetCenter";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listCenter) {
                            var t = jQuery.parseJSON(listCenter.d);
                          //  alert(JSON.stringify(t));
                            var PayMon = t[0]["PayMonths"];
                            var PayMonID = t[0]["PayMonthsID"];
                           // alert(PayMonID);

                            var a = t.length;
                            $("#ddlLocation").empty();
                            $('#txtPayMonth').val(PayMon);
                            //$('#hdnSalMonthID').val(PayMonID);

                            if (a >= 0) {

                                $("#txtSector").val($("#ddlSector").find('option:selected').text());
                                $("#ddlLocation").append("<option value=''>(Select Location)</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlLocation").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                                });
                            }

                            $("#ddlLocation").prop("disabled", false);
                        };
                        options.error = function () { alert("Error in retrieving Location!"); };
                        $.ajax(options);
                    }
                    else {
                        $("#ddlLocation").empty();
                        $("#ddlLocation").prop("disabled", true);
                    }
                }
                else {
                    $("#ddlLocation").empty();
                    $("#txtSector").val("");
                    $("#ddlLocation").append("<option value=''>(Select Location)</option>")
                }
            });

        });

        $(document).ready(function () {
            $('#txtEffFrm').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow'
            });

            $("#btnEFDatePicker").click(function () {
                $('#txtEffFrm').datepicker("show");
                return false;
            });

            $('#txtEffTo').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow'
            });

            $("#btnETDatePicker").click(function () {
                $('#txtEffTo').datepicker("show");
                return false;
            });

            $(".dpDate").datepicker("option", "dateFormat", "dd/mm/yy");
            $(".calendar").click(function () {
                $(".dpdate").datepicker("show");
                return false;
            });
        });

        function beforeSave() {
            $("#form1").validate();
            
            $("#ddlSector").rules("add", { required: true, messages: { required: "Please select Sector"} });
           
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Pay Slip</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Sector &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                        <asp:DropDownList ID="ddlSector" runat="server" class="headFont" Width="170px" CssClass="textbox" DataValueField="SectorID" DataTextField="SectorName"
                                                         AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                         <asp:ListItem Text="Select Sector" Selected="True" Value="0"></asp:ListItem>
                                        </asp:DropDownList>                            
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Pay Month&nbsp;&nbsp;</span></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>
                                        </td>
                                       <%--<asp:HiddenField ID="hdnSalMonthID" runat="server"  />--%>
                                    </tr>
                                </table>
                            </InsertItemTemplate>

     
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <%--<asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>--%>
                                        <asp:Button ID="cmdSave" runat="server" Text="Generate Report"   
                                        Width="100px" CssClass="Btnclassname"  
                                        OnClick="cmdPrint_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                        
                                    </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <%= string.IsNullOrEmpty("") ? "" + str1 + "" : "" + str1 + ""%>      
                                            
                        
                        </td>
                    </tr>
                                     
                     
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:auto;width:100%">
                            <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="DAID" OnRowCommand="tbl_RowCommand" AllowPaging="false" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("DAID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                                OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("DAID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DAID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="DAID" />
                                    <asp:BoundField DataField="DACode" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="DA Code" />
                                    <asp:BoundField DataField="EmpType" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Employee Type" />
                                    <asp:BoundField DataField="DARate" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="DA Rate" />
                                    <asp:BoundField DataField="MaxAmount" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Maximum Amount" />
                                    <asp:BoundField DataField="EffectiveFrom" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Effective From" />
                                    <asp:BoundField DataField="EffectiveTo" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Effective To" />
                                </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

