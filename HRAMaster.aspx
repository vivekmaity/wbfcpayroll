﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="HRAMaster.aspx.cs" Inherits="HRAMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtEffFrm').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                showButtonPanel: true,
                duration: 'slow'
            });

            $("#btnEFDatePicker").click(function () {
                $('#txtEffFrm').datepicker("show");
                return false;
            });

            $('#txtEffTo').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow'
            });

            $("#btnETDatePicker").click(function () {
                $('#txtEffTo').datepicker("show");
                return false;
            });

            $(".dpDate").datepicker("option", "dateFormat", "dd/mm/yy");
            $(".calendar").click(function () {
                $(".dpdate").datepicker("show");
                return false;
            });
        });

        function beforeSave() {
            $("#form1").validate();
            $("#txtHRACode").rules("add", { required: true, messages: { required: "Please Enter HRA Code"} });
            $("#ddlEmpType").rules("add", { required: true, messages: { required: "Please select Employee Type"} });
            $("#txtHRA").rules("add", { required: true, messages: { required: "Please Select HRA"} });
            $("#txtMaxAmt").rules("add", { required: true, messages: { required: "Please Enter Maximum Amount"} });
        }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>HRA  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <%--<td style="padding:5px;" ><span class="headFont">HRA Code &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtHRACode" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3"></asp:TextBox>                            
                                        </td>--%>
                                        <td style="padding:5px;"><span class="headFont">Employee Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmpType" Width="217px" Height="22px" runat="server" AppendDataBoundItems="true" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                <asp:ListItem Text="WBFC Employee" Value="K"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">HRA &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtHRA" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="25" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Maximum Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtMaxAmt" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Effective From&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtEffFrm" ClientIDMode="Static" Width="215px" Height="18px" runat="server" CssClass="dpDate inputbox2" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnEFDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                        <%--<td style="padding:5px;" ><span class="headFont">Effective To&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtEffTo" ClientIDMode="Static" runat="server" Width="215px" Height="18px" CssClass="dpDate inputbox2" autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnETDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>--%>
                                    </tr>
                                </table>
                            </InsertItemTemplate>

                            <EditItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                        <%--<td style="padding:10px;"> <span class="headFont">HRA Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtHRACode" Text='<%# Eval("HRACode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" Enabled="false" autocomplete="off"></asp:TextBox>                            
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("HRAID") %>'/>
                                        </td>--%>
                                        <td style="padding:5px;"><span class="headFont">Employee Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmpType" Width="217px" runat="server" Height="22px" AppendDataBoundItems="true" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                <asp:ListItem Text="WBFC Employee" Value="K"></asp:ListItem>

                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">HRA&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtHRA" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="25" Text='<%# Eval("HRA") %>' onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Maximum Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtMaxAmt" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("MaxAmount") %>' onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Effective From&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtEffFrm" ClientIDMode="Static" runat="server" Width="215px" Height="18px" CssClass="dpDate inputbox2" Text='<%# Eval("EffectiveFrom") %>' autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnEFDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>
                                        <%--<td style="padding:5px;" ><span class="headFont">Effective To&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <nobr>
                                                <asp:TextBox ID="txtEffTo" ClientIDMode="Static" runat="server" Width="215px" Height="18px" CssClass="dpDate inputbox2" Text='<%# Eval("EffectiveTo") %>' autocomplete="off"></asp:TextBox>
                                                <asp:ImageButton ID="btnETDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                            </nobr>
                                        </td>--%>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="HRAID" OnRowCommand="tbl_RowCommand" AllowPaging="true" 
                                PageSize="10" CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                        <AlternatingRowStyle BackColor="#FFFACD" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("HRAID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Delete">
                                         <HeaderStyle />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                                OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("HRAID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="HRAID" Visible="false" HeaderText="HRAID" />
                                    <%--<asp:BoundField DataField="HRACode"  HeaderText="HRA Code" ItemStyle-HorizontalAlign="Center" />--%>
                                    <asp:BoundField DataField="EmpType"  HeaderText="Employee Type" />
                                    <asp:BoundField DataField="HRA"  HeaderText="HRA" ItemStyle-HorizontalAlign="Center"  />
                                    <asp:BoundField DataField="EffectiveFrom"  HeaderText="Effective From" ItemStyle-HorizontalAlign="Center"  />
                                    <asp:BoundField DataField="EffectiveTo"  HeaderText="Effective To" ItemStyle-HorizontalAlign="Center"  />
                                </Columns>
                            </asp:GridView> 
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

