﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ItaxSavingType.aspx.cs" Inherits="ItaxSavingType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">
    <script src="js/ItaxSavingType.js"></script>
    <style>
        #tbl {
            border-collapse: collapse;
            width: 100%;
        }

        #tbl th, #tbl td {
            text-align: left;
            padding: 8px;
        }

       #tbl tbody tr:nth-child(even) {
            background-color: #d2d5d8;
        }

        #tbl thead {
            background-color: #007697;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2 class="headFont">Income Tax Saving Type Master</h2><label class="headFont"> (  <span style="color: red">*</span> Mandatory Fields )</label>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label class="headFont">Itax Type</label>
                                                            <span style="color: red">*</span>
                                                            <select id="ddlItaxType" name="Gender" class="textbox" style="height: 25px; width: 150px;"></select>
                                                        </td>
                                                        <td>
                                                            <label class="headFont">Abbraviation</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" autocomplete="off" id="txtAbbraviation" name="Abbraviation" class="textbox" />
                                                            <input type="hidden" id="hdnItaxSavingTypeID" name="ItaxSavingTypeID" />
                                                        </td>
                                                        <td>
                                                            <label class="headFont">Description</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" id="txtDescription" autocomplete="off" name="Description" class="textbox" />
                                                        </td>
                                                        <td>
                                                            <label class="headFont">Max Amount</label>
                                                            <span style="color: red">*</span>
                                                            <input type="text" id="txtMaxAmount" autocomplete="off" name="MaxAmount" style="width: 100px;" class="textbox" />
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" id="chckIsActive" name="IsActive" checked />&nbsp;&nbsp;<span class="headFont">IsActive</span>
                                                        </td>
                                                        <td>
                                                            <input type="button" id="btnSave" value="Save" name="Save" class="Btnclassname" />
                                                            <input type="button" id="btnRefresh" name="Refresh" class="Btnclassname" value="Refresh" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="max-height: 500px; overflow-y: scroll;">
                                                    <table id="tbl">
                                                        <thead>
                                                            <tr>
                                                                <td style="display:none;">
                                                                    <label>ID</label>
                                                                </td>
                                                                <td>
                                                                    <label>Sl. No.</label>
                                                                </td>
                                                                <td style="display:none;">
                                                                    <label>TypeID</label>
                                                                </td>
                                                                <td>
                                                                    <label>Type</label>
                                                                </td>
                                                                <td>
                                                                    <label>Abbraviation</label>
                                                                </td>
                                                                <td>
                                                                    <label>Description</label>
                                                                </td>
                                                                <td>
                                                                    <label>Max Amount</label>
                                                                </td>
                                                                <td>
                                                                    <label>IsActive</label>
                                                                </td>
                                                                <td style="display:none;">
                                                                    <label>IsActiveFlag </label>
                                                                </td>
                                                                <td>
                                                                    <label>Update</label>
                                                                </td>
                                                                <td>
                                                                    <label>Delete</label>
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

