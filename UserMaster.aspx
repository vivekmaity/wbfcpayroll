﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" ViewStateEncryptionMode="Always" AutoEventWireup="true" CodeFile="UserMaster.aspx.cs" Inherits="UserMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            $("#txtUserName").rules("add", { required: true, messages: { required: "Please enter User Name"} });
            $("#txtPass").rules("add", { required: true, messages: { required: "Please enter Password"} });
            $("#txtEmpCode").rules("add", { required: true, messages: { required: "Please enter Employee Code"} });
        }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>User Master</h2>						
						</section>

                    <table width="98%" style="border:solid 2px lightblue;" >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                            <InsertItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                        <td style="padding:10px;" ><span class="headFont">Employee Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployee" Width="200px" runat="server" DataSource='<%#drpload("Employee") %>'
                                                DataValueField="EmployeeID" DataTextField="EmpName" autocomplete="off" AppendDataBoundItems="true" ClientIDMode="Static">
                                                <asp:ListItem Text="(Select Employee)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:10px;" ><span class="headFont">User Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:DropDownList ID="ddlUserType" Width="200px"  runat="server" DataSource='<%#drpload("UserType") %>'
                                                DataValueField="TypeCode" DataTextField="TypeName" autocomplete="off" AppendDataBoundItems="true" ClientIDMode="Static">
                                                <asp:ListItem Text="(Select User Type)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px;" ><span class="headFont">User Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtUserName" ClientIDMode="Static" runat="server" autocomplete="off"  CssClass="textbox" MaxLength="50"></asp:TextBox>                            
                                        </td>
                                        <td style="padding:10px;" ><span class="headFont">Password&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtPass" TextMode="Password" ClientIDMode="Static" runat="server" autocomplete="off"  CssClass="textbox" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px;" ><span class="headFont">Sector&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:DropDownList ID="ddlSector" Width="200px" runat="server" DataSource='<%#drpload("Sector") %>'
                                                DataValueField="SectorID" DataTextField="SectorName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off" >
                                                <asp:ListItem Text="(Select Employee)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:10px;" ><span class="headFont">User Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtUserCode" ClientIDMode="Static" runat="server" autocomplete="off"  CssClass="textbox" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px;" ><span class="headFont">SM ID&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtSMID" ClientIDMode="Static" runat="server" autocomplete="off"  CssClass="textbox" MaxLength="1"></asp:TextBox>
                                        </td>
                                        <td style="padding:10px;" ><span class="headFont">Lock User&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:CheckBox ID="chkLock" runat="server" ClientIDMode="Static" />
                                        </td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>
                            <EditItemTemplate>
                                <table align="center" width="100%">                           
                                   <tr>
                                        <td style="padding:10px;" ><span class="headFont">Employee Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployee" Width="200px" runat="server" DataSource='<%#drpload("Employee") %>'
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off" >
                                                <asp:ListItem Text="(Select Employee)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("UserID") %>'/>
                                        </td>
                                        <td style="padding:10px;" ><span class="headFont">User Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:DropDownList ID="ddlUserType" Width="200px" runat="server" DataSource='<%#drpload("UserType") %>'
                                                DataValueField="TypeCode" DataTextField="TypeName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off" >
                                                <asp:ListItem Text="(Select User Type)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px;" ><span class="headFont">User Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtUserName" ClientIDMode="Static" autocomplete="off"  runat="server" CssClass="textbox" MaxLength="50" Text='<%# Eval("UserName") %>'></asp:TextBox>                            
                                        </td>
                                        <%--<td style="padding:10px;" ><span class="headFont">Password&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtPass" TextMode="Password" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10"></asp:TextBox>
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px;" ><span class="headFont">Sector&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:DropDownList ID="ddlSector" Width="200px" runat="server" DataSource='<%#drpload("Sector") %>'
                                                DataValueField="SectorID" DataTextField="SectorName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off" >
                                                <asp:ListItem Text="(Select Employee)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:10px;" ><span class="headFont">User Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" > 
                                            <asp:TextBox ID="txtUserCode" ClientIDMode="Static" runat="server" CssClass="textbox" autocomplete="off"  MaxLength="10" Text='<%# Eval("UserCode") %>' Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px;" ><span class="headFont">SM ID&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtSMID" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="1" Text='<%# Eval("SMID") %>'></asp:TextBox>
                                        </td>
                                        <td style="padding:10px;" ><span class="headFont">Lock User&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;" >:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:CheckBox ID="chkLock" runat="server" autocomplete="off"  ClientIDMode="Static" Checked='<%# Eval("LockFlag").ToString().Equals("Y")  %>' />
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:10px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:10px;">&nbsp;</td>
                                    <td style="padding:10px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                    
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:auto;width:100%">
                    
                            <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" 
                            AutoGenerateColumns="false" DataKeyNames="UserID" OnRowCommand="tbl_RowCommand"  AllowPaging="false" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                        
                                    <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                    
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("UserID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                   
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                            OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("UserID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UserID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="User ID" />
                                    <asp:BoundField DataField="UserName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="User Name" />
                                    <asp:BoundField DataField="EmployeeID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="EmployeeID" />                  
                                    <asp:BoundField DataField="EmpName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Employee Name" />
                                    <asp:BoundField DataField="SectorID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SectorID" />
                                    <asp:BoundField DataField="SectorName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sector Name" />
                                    <asp:BoundField DataField="SMID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SM ID" />
                                    <asp:BoundField DataField="UserCode" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="User Code" />
                                    <asp:BoundField DataField="TypeCode" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Employee Name" />
                                    <asp:BoundField DataField="TypeName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Type Name" />
                                </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
                           
                        
                        				
					</div>
				</div>
			</div>
		</div>
</asp:Content>

