﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;


public partial class GroupMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            
            {
                //PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdGenerate_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdProcess_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox txsm = (TextBox)dv.FindControl("txtSalMonth");
            DropDownList ddlfpFlag = (DropDownList)dv.FindControl("ddlFPFlag");
            String salMonth = txsm.Text;
            String insertedBy = Session[SiteConstants.SSN_INT_USER_ID].ToString();
            String finalFlag = ddlfpFlag.SelectedValue;
            
            DBHandler.Execute("Sal_Generation", 
                txsm.Text.Equals("") ? DBNull.Value : (object)(txsm.Text),
                Session[SiteConstants.SSN_INT_USER_ID].ToString(),
                ddlfpFlag.SelectedValue);

            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Salary Process Completed Successfully.')</script>");
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    

    //private void PopulateGrid()
    //{
    //    try
    //    {
    //        DataTable dtstr = DBHandler.GetResult("Get_Group");
    //        tbl.DataSource = dtstr;
    //        tbl.DataBind();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_GroupByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Group", ID);
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtGroupCode = (TextBox)dv.FindControl("txtGroupCode");

                TextBox txtGroupName = (TextBox)dv.FindControl("txtGroupName");

                DBHandler.Execute("Insert_Group", txtGroupCode.Text, txtGroupName.Text, Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {
                TextBox txtGroupCode = (TextBox)dv.FindControl("txtGroupCode");

                TextBox txtGroupName = (TextBox)dv.FindControl("txtGroupName");
              
                string ID = ((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("Update_Group", ID, txtGroupName.Text, Session[SiteConstants.SSN_INT_USER_ID]);

                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}