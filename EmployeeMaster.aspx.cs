﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;



public partial class EmployeeMaster : System.Web.UI.Page
{
    DataTable dtMaster;
    DataTable dtDetail;
    DataTable dtAddPayDetails;
    DataTable AddPayDetailsafterDelete;
    DataTable dtPopUpStatus;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SetPageNoCache();
            Button btnupdate = (Button)dv.FindControl("btnUpdate");
            btnupdate.Visible = false;
            Button btnUpdate1 = (Button)dv1.FindControl("btnUpdate1");
            btnUpdate1.Visible = false;
            TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
            txtBankCode.Enabled = false;
            HttpContext.Current.Session["AddPayDetails"] = null;
            HttpContext.Current.Session["EmployeeMaster"] = null;
            HttpContext.Current.Session["PayDetails"] = null;
            HttpContext.Current.Session["PopUpStatus"] = null;

            if (Session["EmployeeMaster"] == null)
            {
                dtMaster = new DataTable();
                dtMaster = EmployeeDataHandling.CreateEmpMasterTable(dtMaster);
                HttpContext.Current.Session["EmployeeMaster"] = dtMaster;
            }
            if (Session["EmployeeDetail"] == null)
            {
                dtDetail = new DataTable();
                dtDetail = EmployeeDataHandling.CreateEmpDetailTable(dtDetail);
                HttpContext.Current.Session["EmployeeDetail"] = dtDetail;
            }
            if (HttpContext.Current.Session["AddPayDetails"] == null)
            {
                dtAddPayDetails = new DataTable();
                dtAddPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtAddPayDetails);
                HttpContext.Current.Session["AddPayDetails"] = dtAddPayDetails;
            }
            if (HttpContext.Current.Session["AddPayDetailsafterDelete"] == null)
            {
                AddPayDetailsafterDelete = new DataTable();
                AddPayDetailsafterDelete = EmployeeDataHandling.CreatePayDetailsTableafterDelete(AddPayDetailsafterDelete);
                HttpContext.Current.Session["AddPayDetailsafterDelete"] = AddPayDetailsafterDelete;
            }
            if (HttpContext.Current.Session["PopUpStatus"] == null)
            {
                dtPopUpStatus = new DataTable();
                dtPopUpStatus = EmployeeDataHandling.CreatePopUpStatusTable(dtPopUpStatus);
                HttpContext.Current.Session["PopUpStatus"] = dtPopUpStatus;
            }
            if (!IsPostBack)
            {
                if (Session[SiteConstants.SSN_INT_USER_ID] != null)
                {
                    DataTable dt = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count == 1)
                            PopulateCenter(dt.Rows[0]["SectorID"].ToString());
                    }
                }
                populateJSON();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
     {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                dv.ChangeMode(FormViewMode.Insert);
                dv1.ChangeMode(FormViewMode.Insert);
            }
            Button cmdSearch = (Button)sender;
            if (cmdSearch.CommandName == "Search")
            {
                string UserTypeCode = Session[SiteConstants.SSN_USERTYPECODE].ToString();
                if (UserTypeCode == "2")
                {
                    PopulatebyAdmin(txtEmpCodeSearch.Text);
                }
                else
                {
                    string EmployeeNo = txtEmpCodeSearch.Text;
                    DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
                    int Secid = Convert.ToInt32(Sectors.SelectedValue.ToString());

                    DataSet ds = DBHandler.GetResults("Get_EmployeeDetails", EmployeeNo, Secid, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    DataTable dtss = ds.Tables[0]; DataTable dts = ds.Tables[1]; DataTable dtresult = ds.Tables[2];
                    if (dtss.Rows.Count == 0)
                    {

                        if (dtresult.Rows[0][0].ToString() == "Y")
                        {
                            string display = "Please select the appropriate Sector of this Employee.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                            return;
                        }
                        else
                        {
                            Button cmdsave = (Button)dv.FindControl("cmdSave");
                            Button btnupdate = (Button)dv.FindControl("btnUpdate");
                            cmdsave.Visible = false;
                            btnupdate.Visible = true;


                            Button cmdSave1 = (Button)dv1.FindControl("cmdSave1");
                            Button btnUpdate1 = (Button)dv1.FindControl("btnUpdate1");
                            cmdSave1.Visible = false;
                            btnUpdate1.Visible = true;

                            string display = "Sorry ! You are not authorised.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");

                            List<AddEmployeePayDetails> emppd = new List<AddEmployeePayDetails>();
                            HttpContext.Current.Session["AddPayDetails"] = dts;
                            if (HttpContext.Current.Session["AddPayDetails"] != null)
                            {
                                DataTable dtDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                                foreach (DataRow dr1 in dtDetail.Rows)
                                {
                                    AddEmployeePayDetails bc = new AddEmployeePayDetails();
                                    bc.EarnDeductionID = Int32.Parse(dr1["EarnDeductionID"].ToString());
                                    bc.EarnDeduction = dr1["EarnDeduction"].ToString();
                                    bc.Amount = dr1["Amount"].ToString();
                                    bc.EarnDeductionType = dr1["EarnDeductionType"].ToString();
                                    emppd.Add(bc);
                                }
                                Employee emp = new Employee();
                                emp.empAddPayDetails = emppd.ToArray();
                                ltlj.Text = "<input type='hidden' value='" + emp.ToJSON() + "' id='empJSON' name='empJSON'/>";
                                return;
                            }
                        }
                    }
                    else
                    {
                        string EMPID = ds.Tables[0].Rows[0]["EmployeeID"].ToString();
                        hdnEmpID.Value = EMPID;
                        hdnStatus.Value = ds.Tables[0].Rows[0]["Status"].ToString();
                        hdnPayScaleID.Value = ds.Tables[0].Rows[0]["PayScaleID"].ToString();

                        dv.ChangeMode(FormViewMode.Edit);
                        hdnDefaultMode.Value = "Edit";

                        dv.DataSource = dtss;
                        dv.DataBind();

                        TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                        txtBankCode.Enabled = true;
                        /*=================================================================================*/
                        EnableDisableField(EmployeeNo);
                        /*=================================================================================*/
                        if (dv.CurrentMode == FormViewMode.Edit)
                        {
                            Button cmdsave = (Button)dv.FindControl("cmdSave");
                            Button btnupdate = (Button)dv.FindControl("btnUpdate");
                            cmdsave.Visible = false;
                            btnupdate.Visible = true;


                            Button cmdSave1 = (Button)dv1.FindControl("cmdSave1");
                            Button btnUpdate1 = (Button)dv1.FindControl("btnUpdate1");
                            cmdSave1.Visible = false;
                            btnUpdate1.Visible = true;



                            ((DropDownList)dv.FindControl("ddlEmpType")).SelectedValue = ds.Tables[0].Rows[0][3].ToString();
                            if (ds.Tables[0].Rows[0][3].ToString() != "")
                            {
                                OnSearchBindDA(ds.Tables[0].Rows[0][3].ToString());
                                OnSearchBindHRA(ds.Tables[0].Rows[0][3].ToString());
                                OnSearchBindPayScaleType(ds.Tables[0].Rows[0][3].ToString());
                                OnSearchBindIR(ds.Tables[0].Rows[0][3].ToString());
                            }
                            ((DropDownList)dv.FindControl("ddlPayScaleType")).SelectedValue = ds.Tables[0].Rows[0]["PayScaleType"].ToString();
                            OnSearchBindCenter(Secid);
                            ((DropDownList)dv.FindControl("ddlCenter")).SelectedValue = ds.Tables[0].Rows[0]["CenterID"].ToString();
                            ((DropDownList)dv.FindControl("ddlClassifctn")).SelectedValue = ds.Tables[0].Rows[0][4].ToString();
                            ((DropDownList)dv.FindControl("ddlReligion")).SelectedValue = ds.Tables[0].Rows[0][6].ToString();
                            ((DropDownList)dv.FindControl("ddlQualftn")).SelectedValue = ds.Tables[0].Rows[0][7].ToString();
                            ((DropDownList)dv.FindControl("ddlMarital")).SelectedValue = ds.Tables[0].Rows[0][8].ToString();
                            ((DropDownList)dv.FindControl("ddlCaste")).SelectedValue = ds.Tables[0].Rows[0][9].ToString();
                            ((DropDownList)dv.FindControl("ddlSex")).SelectedValue = ds.Tables[0].Rows[0][11].ToString();
                            OnSearchBindState();
                            string stateid = ds.Tables[0].Rows[0][13].ToString();
                            string districtid = ds.Tables[0].Rows[0][14].ToString();
                            ((DropDownList)dv.FindControl("ddlState")).SelectedValue = stateid;
                            if (stateid == "") { }
                            else
                            {
                                OnSearchBindDistrict(Convert.ToInt32(stateid));
                                ((DropDownList)dv.FindControl("ddlDistrict")).SelectedValue = districtid;
                            }

                            OnsearchBindPermState();
                            string Perstateid = ds.Tables[0].Rows[0][21].ToString();
                            string Perdistrictid = ds.Tables[0].Rows[0][22].ToString();
                            ((DropDownList)dv.FindControl("ddlState_P")).SelectedValue = Perstateid;
                            if (Perstateid == "") { }
                            else
                            {
                                OnSearchBindPermDistrict(Convert.ToInt32(Perstateid));//supp
                                ((DropDownList)dv.FindControl("ddlDistrict_P")).SelectedValue = Perdistrictid;
                            }
                            //for official details
                            ((DropDownList)dv.FindControl("ddlPhysical")).SelectedValue = ds.Tables[0].Rows[0][29].ToString();
                            ((DropDownList)dv.FindControl("ddlDesig")).SelectedValue = ds.Tables[0].Rows[0][30].ToString();
                            ((DropDownList)dv.FindControl("ddlGroup")).SelectedValue = ds.Tables[0].Rows[0][31].ToString();
                            ((DropDownList)dv.FindControl("ddlCoop")).SelectedValue = ds.Tables[0].Rows[0][32].ToString();
                            //((DropDownList)dv.FindControl("ddlCategory")).SelectedValue = ds.Tables[0].Rows[0][33].ToString();
                            ((DropDownList)dv.FindControl("ddlDepartment")).SelectedValue = ds.Tables[0].Rows[0]["DepartmentId"].ToString();
                            ((DropDownList)dv.FindControl("txtEmployeePfRate")).SelectedValue = ds.Tables[0].Rows[0]["EmployeePfrate"].ToString();
                            ((TextBox)dv.FindControl("txtEmployerPfRate")).Text = ds.Tables[0].Rows[0]["EmployerPfRate"].ToString();
                            ((TextBox)dv.FindControl("txtFatherName")).Text = ds.Tables[0].Rows[0]["FathersName"].ToString();
                            ((DropDownList)dv.FindControl("txtBranchInCharge")).SelectedValue = ds.Tables[0].Rows[0]["BranchInCharge"].ToString();
                            ((TextBox)dv.FindControl("txtAddressRemarks")).Text = ds.Tables[0].Rows[0]["AddressRemarks"].ToString();

                            ((DropDownList)dv.FindControl("ddlPFRate")).SelectedValue = ds.Tables[0].Rows[0]["PFRate"].ToString();


                            string da = ds.Tables[0].Rows[0][34].ToString();
                            if (da == "" || da == "0")
                            {
                                CheckBox chkda = (CheckBox)dv.FindControl("chkDA"); chkda.Checked = true;
                                ((DropDownList)dv.FindControl("ddlDA")).Enabled = true;
                                ((DropDownList)dv.FindControl("ddlDA")).SelectedValue = "";
                            }
                            else
                            {
                                ((DropDownList)dv.FindControl("ddlDA")).SelectedValue = da;
                            }
                            ((DropDownList)dv.FindControl("ddlStatus")).SelectedValue = ds.Tables[0].Rows[0][35].ToString();
                            //if (ds.Tables[0].Rows[0][37].ToString() != "")
                            //{
                            //    OnSearchBindPayScaleTypebyPayScaleID(ds.Tables[0].Rows[0][37].ToString());
                            //}
                            OnSearchBindPayScalebyPayScaleType(((DropDownList)dv.FindControl("ddlPayScaleType")).SelectedValue);
                            ((DropDownList)dv.FindControl("ddlPayScale")).SelectedValue = ds.Tables[0].Rows[0][37].ToString();

                            string hraFlag = ds.Tables[0].Rows[0][40].ToString();
                            string hraFixedAmt = ds.Tables[0].Rows[0][41].ToString();
                            if (hraFlag == "Y")
                            {
                                CheckBox chkHRA = (CheckBox)dv.FindControl("chkHRA"); chkHRA.Checked = true;
                                ((TextBox)dv.FindControl("txtmHRA")).Visible = true;
                                ((TextBox)dv.FindControl("txtmHRA")).Text = hraFixedAmt;
                                ((Label)dv.FindControl("lblHra")).Visible = true;
                                ((DropDownList)dv.FindControl("ddlQuatrAlt")).SelectedValue = "";
                                ((DropDownList)dv.FindControl("ddlQuatrAlt")).Enabled = false;
                                ((DropDownList)dv.FindControl("ddlHRA")).SelectedValue = "0";
                                ((DropDownList)dv.FindControl("ddlHRA")).Enabled = false;
                                ((DropDownList)dv.FindControl("ddlHRACat")).Enabled = false;
                            }
                            else
                            {
                                ((DropDownList)dv.FindControl("ddlHRA")).SelectedValue = ds.Tables[0].Rows[0]["HRAID"].ToString();
                            }

                            string Qallt = ds.Tables[0].Rows[0][48].ToString();
                            ((DropDownList)dv.FindControl("ddlQuatrAlt")).SelectedValue = Qallt;
                            if (Qallt == "Y")
                            {
                                ((DropDownList)dv.FindControl("ddlHRA")).SelectedValue = "0";
                                ((DropDownList)dv.FindControl("ddlHRA")).Enabled = false;
                                ((DropDownList)dv.FindControl("ddlHRACat")).Enabled = true;
                                OnSearchBindHouse(ds.Tables[0].Rows[0][39].ToString());
                            }

                            ((DropDownList)dv.FindControl("ddlSpouseQtr")).SelectedValue = ds.Tables[0].Rows[0][53].ToString();
                            ((DropDownList)dv.FindControl("ddlHRACat")).SelectedValue = ds.Tables[0].Rows[0][39].ToString();
                            if (ds.Tables[0].Rows[0]["HouseID"].ToString() != "")
                            {
                                ((DropDownList)dv.FindControl("ddlHouse")).SelectedValue = ds.Tables[0].Rows[0]["HouseID"].ToString();
                                RadioButton rdHouse = (RadioButton)dv.FindControl("rdHouse"); rdHouse.Checked = true;
                            }
                            else
                            {
                                RadioButton rdBasic = (RadioButton)dv.FindControl("rdHouse"); rdBasic.Checked = true;
                            }

                            OnSearchfindRentAmount(EmployeeNo);

                            string healthScheme = ds.Tables[0].Rows[0]["HealthScheme"].ToString();
                            if (healthScheme == "C")
                            {
                                CheckBox chkHealthScheme = (CheckBox)dv.FindControl("chkHealthScheme"); chkHealthScheme.Checked = true;
                                ((DropDownList)dv.FindControl("ddlHealth")).Enabled = false;
                                ((DropDownList)dv.FindControl("ddlHealth")).SelectedValue = "";
                            }
                            else
                            {
                                ((DropDownList)dv.FindControl("ddlHealth")).SelectedValue = healthScheme;
                            }
                            //FOR IR
                            string ir = ds.Tables[0].Rows[0]["IRID"].ToString();
                            if (ir == "" || ir == "0")
                            {
                                CheckBox chkIR = (CheckBox)dv.FindControl("chkIR"); chkIR.Checked = true;
                                //((DropDownList)dv.FindControl("ddlIR")).Enabled = true;
                                ((DropDownList)dv.FindControl("ddlIR")).SelectedValue = "0";
                            }
                            else
                            {
                                ((DropDownList)dv.FindControl("ddlIR")).SelectedValue = ir;
                                //((DropDownList)dv.FindControl("ddlIR")).Enabled = true;
                            }

                            string BranchID = ds.Tables[0].Rows[0]["BranchID"].ToString();
                            OnSearchBindBankandBranch(BranchID);
                            //TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                            txtBankCode.Text = ds.Tables[0].Rows[0]["BankAccCode"].ToString();
                            // txtBankCode.Enabled = true;
                        }

                        EmployeeMasters emps = new EmployeeMasters();

                        emps.Photo = ds.Tables[0].Rows[0][58].ToString();
                        emps.Signature = ds.Tables[0].Rows[0][59].ToString();
                        //Other
                        //((DropDownList)dv.FindControl("ddlSector")).SelectedValue = ds.Tables[0].Rows[0][59].ToString();

                        List<AddEmployeePayDetails> emppd = new List<AddEmployeePayDetails>();
                        HttpContext.Current.Session["AddPayDetails"] = dts;

                        if (HttpContext.Current.Session["AddPayDetails"] != null)
                        {
                            DataTable dtDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                            foreach (DataRow dr1 in dtDetail.Rows)
                            {
                                AddEmployeePayDetails bc = new AddEmployeePayDetails();
                                bc.EarnDeductionID = Int32.Parse(dr1["EarnDeductionID"].ToString());
                                bc.EarnDeduction = dr1["EarnDeduction"].ToString();
                                bc.Amount = dr1["Amount"].ToString();
                                bc.EarnDeductionType = dr1["EarnDeductionType"].ToString();
                                emppd.Add(bc);
                            }
                        }

                        Employee emp = new Employee();
                        emp.employeeMaster = emps;
                        emp.empAddPayDetails = emppd.ToArray();
                        ltlj.Text = "<input type='hidden' value='" + emp.ToJSON() + "' id='empJSON' name='empJSON'/>";

                    }
                }

            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }
    public void PopulatebyAdmin(string EmployeeNo)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("Get_EmployeeDetailsforAdmin", EmployeeNo, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable dtss = ds.Tables[0]; DataTable dts = ds.Tables[1]; DataTable dtresult = ds.Tables[2];
            if (dtss.Rows.Count == 0)
            {
                if (dtresult.Rows[0][0].ToString() == "Y")
                {
                    string display = "Please select the appropriate Sector of this Employee.";
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                    return;
                }
                else
                {
                    Button cmdsave = (Button)dv.FindControl("cmdSave");
                    Button btnupdate = (Button)dv.FindControl("btnUpdate");
                    cmdsave.Visible = false;
                    btnupdate.Visible = true;


                    Button cmdSave1 = (Button)dv1.FindControl("cmdSave1");
                    Button btnUpdate1 = (Button)dv1.FindControl("btnUpdate1");
                    cmdSave1.Visible = false;
                    btnUpdate1.Visible = true;

                    string display = "Sorry ! You are not authorised.";
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");

                    List<AddEmployeePayDetails> emppd = new List<AddEmployeePayDetails>();
                    HttpContext.Current.Session["AddPayDetails"] = dts;
                    if (HttpContext.Current.Session["AddPayDetails"] != null)
                    {
                        DataTable dtDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                        foreach (DataRow dr1 in dtDetail.Rows)
                        {
                            AddEmployeePayDetails bc = new AddEmployeePayDetails();
                            bc.EarnDeductionID = Int32.Parse(dr1["EarnDeductionID"].ToString());
                            bc.EarnDeduction = dr1["EarnDeduction"].ToString();
                            bc.Amount = dr1["Amount"].ToString();
                            bc.EarnDeductionType = dr1["EarnDeductionType"].ToString();
                            emppd.Add(bc);
                        }
                        Employee emp = new Employee();
                        emp.empAddPayDetails = emppd.ToArray();
                        ltlj.Text = "<input type='hidden' value='" + emp.ToJSON() + "' id='empJSON' name='empJSON'/>";
                        return;
                    }
                }
            }
            else
            {
                 DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
                 Sectors.SelectedValue = ds.Tables[0].Rows[0]["SectorID"].ToString();
               

                string EMPID = ds.Tables[0].Rows[0]["EmployeeID"].ToString();
                hdnEmpID.Value = EMPID;
                hdnStatus.Value = ds.Tables[0].Rows[0]["Status"].ToString();
                hdnPayScaleID.Value = ds.Tables[0].Rows[0]["PayScaleID"].ToString();

                dv.ChangeMode(FormViewMode.Edit);
                hdnDefaultMode.Value = "Edit";

                dv.DataSource = dtss;
                dv.DataBind();

                TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                txtBankCode.Enabled = true;
                /*=================================================================================*/
                EnableDisableField(EmployeeNo);
                /*=================================================================================*/
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    Button cmdsave = (Button)dv.FindControl("cmdSave");
                    Button btnupdate = (Button)dv.FindControl("btnUpdate");
                    cmdsave.Visible = false;
                    btnupdate.Visible = true;


                    Button cmdSave1 = (Button)dv1.FindControl("cmdSave1");
                    Button btnUpdate1 = (Button)dv1.FindControl("btnUpdate1");
                    cmdSave1.Visible = false;
                    btnUpdate1.Visible = true;



                    ((DropDownList)dv.FindControl("ddlEmpType")).SelectedValue = ds.Tables[0].Rows[0][3].ToString();
                    if (ds.Tables[0].Rows[0][3].ToString() != "")
                    {
                        OnSearchBindDA(ds.Tables[0].Rows[0][3].ToString());
                        OnSearchBindHRA(ds.Tables[0].Rows[0][3].ToString());
                        OnSearchBindPayScaleType(ds.Tables[0].Rows[0][3].ToString());
                        OnSearchBindIR(ds.Tables[0].Rows[0][3].ToString());
                    }
                    ((DropDownList)dv.FindControl("ddlPayScaleType")).SelectedValue = ds.Tables[0].Rows[0]["PayScaleType"].ToString();
                    OnSearchBindCenter(Convert.ToInt32(ds.Tables[0].Rows[0]["SectorID"].ToString()));
                    ((DropDownList)dv.FindControl("ddlCenter")).SelectedValue = ds.Tables[0].Rows[0]["CenterID"].ToString();
                    ((DropDownList)dv.FindControl("ddlClassifctn")).SelectedValue = ds.Tables[0].Rows[0][4].ToString();
                    ((DropDownList)dv.FindControl("ddlReligion")).SelectedValue = ds.Tables[0].Rows[0][6].ToString();
                    ((DropDownList)dv.FindControl("ddlQualftn")).SelectedValue = ds.Tables[0].Rows[0][7].ToString();
                    ((DropDownList)dv.FindControl("ddlMarital")).SelectedValue = ds.Tables[0].Rows[0][8].ToString();
                    ((DropDownList)dv.FindControl("ddlCaste")).SelectedValue = ds.Tables[0].Rows[0][9].ToString();
                    ((DropDownList)dv.FindControl("ddlSex")).SelectedValue = ds.Tables[0].Rows[0][11].ToString();
                    OnSearchBindState();
                    string stateid = ds.Tables[0].Rows[0][13].ToString();
                    string districtid = ds.Tables[0].Rows[0][14].ToString();
                    ((DropDownList)dv.FindControl("ddlState")).SelectedValue = stateid;
                    if (stateid == "") { }
                    else
                    {
                        OnSearchBindDistrict(Convert.ToInt32(stateid));
                        ((DropDownList)dv.FindControl("ddlDistrict")).SelectedValue = districtid;
                    }

                    OnsearchBindPermState();
                    string Perstateid = ds.Tables[0].Rows[0][21].ToString();
                    string Perdistrictid = ds.Tables[0].Rows[0][22].ToString();
                    ((DropDownList)dv.FindControl("ddlState_P")).SelectedValue = Perstateid;
                    if (Perstateid == "") { }
                    else
                    {
                        OnSearchBindPermDistrict(Convert.ToInt32(Perstateid));//sup
                        ((DropDownList)dv.FindControl("ddlDistrict_P")).SelectedValue = Perdistrictid;
                    }
                    //for official details
                    ((DropDownList)dv.FindControl("ddlPhysical")).SelectedValue = ds.Tables[0].Rows[0][29].ToString();
                    ((DropDownList)dv.FindControl("ddlDesig")).SelectedValue = ds.Tables[0].Rows[0][30].ToString();
                    ((DropDownList)dv.FindControl("ddlGroup")).SelectedValue = ds.Tables[0].Rows[0][31].ToString();
                    ((DropDownList)dv.FindControl("ddlCoop")).SelectedValue = ds.Tables[0].Rows[0][32].ToString();
                    ((DropDownList)dv.FindControl("ddlCategory")).SelectedValue = ds.Tables[0].Rows[0][33].ToString();
                    string da = ds.Tables[0].Rows[0][34].ToString();
                    if (da == "" || da == "0")
                    {
                        CheckBox chkda = (CheckBox)dv.FindControl("chkDA"); chkda.Checked = true;
                        ((DropDownList)dv.FindControl("ddlDA")).Enabled = true;
                        ((DropDownList)dv.FindControl("ddlDA")).SelectedValue = "";
                    }
                    else
                    {
                        ((DropDownList)dv.FindControl("ddlDA")).SelectedValue = da;
                    }
                    ((DropDownList)dv.FindControl("ddlStatus")).SelectedValue = ds.Tables[0].Rows[0][35].ToString();
                    //if (ds.Tables[0].Rows[0][37].ToString() != "")
                    //{
                    //    OnSearchBindPayScaleTypebyPayScaleID(ds.Tables[0].Rows[0][37].ToString());
                    //}
                    OnSearchBindPayScalebyPayScaleType(((DropDownList)dv.FindControl("ddlPayScaleType")).SelectedValue);
                    ((DropDownList)dv.FindControl("ddlPayScale")).SelectedValue = ds.Tables[0].Rows[0][37].ToString();

                    string hraFlag = ds.Tables[0].Rows[0][40].ToString();
                    string hraFixedAmt = ds.Tables[0].Rows[0][41].ToString();
                    if (hraFlag == "Y")
                    {
                        CheckBox chkHRA = (CheckBox)dv.FindControl("chkHRA"); chkHRA.Checked = true;
                        ((TextBox)dv.FindControl("txtmHRA")).Visible = true;
                        ((TextBox)dv.FindControl("txtmHRA")).Text = hraFixedAmt;
                        ((Label)dv.FindControl("lblHra")).Visible = true;
                        ((DropDownList)dv.FindControl("ddlQuatrAlt")).SelectedValue = "";
                        ((DropDownList)dv.FindControl("ddlQuatrAlt")).Enabled = false;
                        ((DropDownList)dv.FindControl("ddlHRA")).SelectedValue = "0";
                        ((DropDownList)dv.FindControl("ddlHRA")).Enabled = false;
                        ((DropDownList)dv.FindControl("ddlHRACat")).Enabled = false;
                    }
                    else
                    {
                        ((DropDownList)dv.FindControl("ddlHRA")).SelectedValue = ds.Tables[0].Rows[0]["HRAID"].ToString();
                    }

                    string Qallt = ds.Tables[0].Rows[0][46].ToString();
                    ((DropDownList)dv.FindControl("ddlQuatrAlt")).SelectedValue = Qallt;
                    if (Qallt == "Y")
                    {
                        ((DropDownList)dv.FindControl("ddlHRA")).SelectedValue = "0";
                        ((DropDownList)dv.FindControl("ddlHRA")).Enabled = false;
                        ((DropDownList)dv.FindControl("ddlHRACat")).Enabled = true;
                        OnSearchBindHouse(ds.Tables[0].Rows[0][39].ToString());
                    }

                    ((DropDownList)dv.FindControl("ddlSpouseQtr")).SelectedValue = ds.Tables[0].Rows[0][51].ToString();
                    ((DropDownList)dv.FindControl("ddlHRACat")).SelectedValue = ds.Tables[0].Rows[0][39].ToString();
                    if (ds.Tables[0].Rows[0]["HouseID"].ToString() != "")
                    {
                        ((DropDownList)dv.FindControl("ddlHouse")).SelectedValue = ds.Tables[0].Rows[0]["HouseID"].ToString();
                        RadioButton rdHouse = (RadioButton)dv.FindControl("rdHouse"); rdHouse.Checked = true;
                    }
                    else
                    {
                        RadioButton rdBasic = (RadioButton)dv.FindControl("rdHouse"); rdBasic.Checked = true;
                    }

                    OnSearchfindRentAmount(EmployeeNo);

                    string healthScheme = ds.Tables[0].Rows[0]["HealthScheme"].ToString();
                    if (healthScheme == "C")
                    {
                        CheckBox chkHealthScheme = (CheckBox)dv.FindControl("chkHealthScheme"); chkHealthScheme.Checked = true;
                        ((DropDownList)dv.FindControl("ddlHealth")).Enabled = false;
                        ((DropDownList)dv.FindControl("ddlHealth")).SelectedValue = "";
                    }
                    else
                    {
                        ((DropDownList)dv.FindControl("ddlHealth")).SelectedValue = healthScheme;
                    }

                    string ir = ds.Tables[0].Rows[0]["IRID"].ToString();
                    if (ir == "" || ir == "0")
                    {
                        CheckBox chkIR = (CheckBox)dv.FindControl("chkIR"); chkIR.Checked = true;
                        // ((DropDownList)dv.FindControl("ddlIR")).Enabled = false;
                        ((DropDownList)dv.FindControl("ddlIR")).SelectedValue = "0";
                    }
                    else
                    {
                        ((DropDownList)dv.FindControl("ddlIR")).SelectedValue = ir;
                        //((DropDownList)dv.FindControl("ddlIR")).Enabled = false;
                    }

                    string BranchID = ds.Tables[0].Rows[0]["BranchID"].ToString();
                    OnSearchBindBankandBranch(BranchID);
                    //TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                    txtBankCode.Text = ds.Tables[0].Rows[0]["BankAccCode"].ToString();
                    // txtBankCode.Enabled = true;
                }

                EmployeeMasters emps = new EmployeeMasters();

                emps.Photo = ds.Tables[0].Rows[0][58].ToString();
                emps.Signature = ds.Tables[0].Rows[0][59].ToString();
                //Other
                //((DropDownList)dv.FindControl("ddlSector")).SelectedValue = ds.Tables[0].Rows[0][59].ToString();

                List<AddEmployeePayDetails> emppd = new List<AddEmployeePayDetails>();
                HttpContext.Current.Session["AddPayDetails"] = dts;

                if (HttpContext.Current.Session["AddPayDetails"] != null)
                {
                    DataTable dtDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                    foreach (DataRow dr1 in dtDetail.Rows)
                    {
                        AddEmployeePayDetails bc = new AddEmployeePayDetails();
                        bc.EarnDeductionID = Int32.Parse(dr1["EarnDeductionID"].ToString());
                        bc.EarnDeduction = dr1["EarnDeduction"].ToString();
                        bc.Amount = dr1["Amount"].ToString();
                        bc.EarnDeductionType = dr1["EarnDeductionType"].ToString();
                        emppd.Add(bc);
                    }
                }

                Employee emp = new Employee();
                emp.employeeMaster = emps;
                emp.empAddPayDetails = emppd.ToArray();
                ltlj.Text = "<input type='hidden' value='" + emp.ToJSON() + "' id='empJSON' name='empJSON'/>";

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    public void EnableDisableField(string EmployeeNo)
    {
        try
        {
            DataSet ds = DBHandler.GetResults("Get_FieldPermission", EmployeeNo, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable dtMst_Employee_Flag = ds.Tables[0];
            DataTable dtField_Permission = ds.Tables[1];
            DataTable dtField_Permission_Count = ds.Tables[2];

            if (dtMst_Employee_Flag.Rows.Count > 0)
            {
                string lockFlag = dtMst_Employee_Flag.Rows[0]["LockFlag"].ToString();
                if (lockFlag == "Y")
                {
                    if (dtField_Permission_Count.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtField_Permission.Rows.Count; i++)
                        {
                            string Tablename = dtField_Permission.Rows[i]["TableName"].ToString();
                            string Lockflag = dtField_Permission.Rows[i]["LockFlag"].ToString();

                            if (Tablename == "MST_Employee" && Lockflag == "Y")
                            {
                                string Fieldname = dtField_Permission.Rows[i]["FieldName"].ToString();
                                string prefix = Fieldname.Substring(0, 3);
                                if (prefix == "txt")
                                {
                                    if (dv.CurrentMode == FormViewMode.Edit)
                                    {
                                        TextBox txt = (TextBox)dv.FindControl(Fieldname);
                                        txt.Enabled = false;
                                    }
                                }
                                else
                                {
                                    if (Fieldname == "ddlBranch")
                                    {
                                        DropDownList ddl = (DropDownList)dv.FindControl(Fieldname);
                                        ddl.Enabled = false;
                                        DropDownList ddlbank = (DropDownList)dv.FindControl("ddlBank");
                                        ddlbank.Enabled = false;
                                    }
                                    else
                                    {
                                        DropDownList ddl = (DropDownList)dv.FindControl(Fieldname);
                                        ddl.Enabled = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void SetPageNoCache()
    {
        try
        {
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            //Response.Cache.SetAllowResponseInBrowserHistory(false);
            //Response.Cache.SetNoStore();
            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Response.AppendHeader("Expires", "0");

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddSeconds(-1));
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            Response.Cache.SetNoStore();
            HttpResponse.RemoveOutputCacheItem("/EmployeeMaster.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void PopulateCenter(string SecID)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_CenterbySector", SecID);
            if (dt.Rows.Count > 0)
            {
                DropDownList ddlcenter = (DropDownList)dv.FindControl("ddlCenter");
                ddlcenter.DataSource = dt;
                ddlcenter.DataTextField = "CenterName";
                ddlcenter.DataValueField = "CenterID";
                ddlcenter.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void chkHRA_Click(object sender, EventArgs e)
    {
        ((TextBox)dv.FindControl("txtmHRA")).Visible = true;
        ((Label)dv.FindControl("lblHra")).Visible = true;
    }
    protected void OnSearchBindDA(string EmpType)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_DA", EmpType);
                 DropDownList DA = (DropDownList)dv.FindControl("ddlDA");
                DA.DataSource = dt;
                DA.DataBind();
                if (dt.Rows.Count > 0)
                DA.SelectedValue = dt.Rows[0][0].ToString();
                else
                    DA.SelectedValue = "";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindHRA(string EmpType)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_HRA", EmpType);
                DropDownList HRA = (DropDownList)dv.FindControl("ddlHRA");
                HRA.DataSource = dt;
                HRA.DataBind();
                //if (dt.Rows.Count > 0)
                //    HRA.SelectedValue = dt.Rows[0][0].ToString();
                //else
                //    HRA.SelectedValue = "";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindIR(string EmpType)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_IR", EmpType);
                DropDownList IR = (DropDownList)dv.FindControl("ddlIR");
                IR.DataSource = dt;
                IR.DataBind();
                if (dt.Rows.Count > 0)
                    IR.SelectedValue = dt.Rows[0][0].ToString();
                else
                    IR.SelectedValue = "";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindPayScaleType(string EmpType)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("get_PayScaleByEmpType", EmpType);
                DropDownList PSType = (DropDownList)dv.FindControl("ddlPayScaleType");
                PSType.DataSource = dt;
                PSType.DataBind();
             }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindPayScaleTypebyPayScaleID(string PayScaleID)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("get_PayScaleByEmpType", PayScaleID);   //Load_PayScaleType
                DropDownList PST = (DropDownList)dv.FindControl("ddlPayScaleType");
                //PST.DataSource = dt;
                //PST.DataBind();
                if (dt.Rows.Count > 0)
                {
                    PST.SelectedValue = dt.Rows[0][1].ToString();
                }
                else
                {
                    PST.SelectedValue = "";
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindState()
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_State");
                DropDownList state = (DropDownList)dv.FindControl("ddlState");
                state.DataSource = dt;
                state.DataBind();

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindDistrict(int StateID)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_District", StateID);
                DropDownList district = (DropDownList)dv.FindControl("ddlDistrict");
                district.DataSource = dt;
                district.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnsearchBindPermState()
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_State");
                DropDownList state = (DropDownList)dv.FindControl("ddlState_P");
                state.DataSource = dt;
                state.DataBind();

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindPermDistrict(int PerStateID)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_District", PerStateID);
                DropDownList district = (DropDownList)dv.FindControl("ddlDistrict_P");
                district.DataSource = dt;
                district.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindCenter(int Secid)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Get_CenterbySector", Secid);
                DropDownList ddlCenter = (DropDownList)dv.FindControl("ddlCenter");
                ddlCenter.DataSource = dt;
                ddlCenter.DataBind();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindPayScalebyPayScaleType(string PayScaleType)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_PayScalebyPayScaleType", PayScaleType);
                DropDownList PS = (DropDownList)dv.FindControl("ddlPayScale");
                if (dt.Rows.Count > 0)
                {
                    PS.DataSource = dt;
                    PS.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindHouse(string HouseCatg)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_House", HouseCatg);
                DropDownList H = (DropDownList)dv.FindControl("ddlHouse");
                if (dt.Rows.Count > 0)
                {
                    H.DataSource = dt;
                    H.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchfindRentAmount(string EmpNo)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dt = DBHandler.GetResult("Load_HouserentbyEmpNo", EmpNo);
                TextBox txtrentAmount = (TextBox)dv.FindControl("txtrentAmount");
                if (dt.Rows.Count > 0)
                {
                    txtrentAmount.Text = dt.Rows[0]["Amount"].ToString(); ;
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void OnSearchBindBankandBranch(string BranchID)
    {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                DataTable dtBank = DBHandler.GetResult("Load_Bank");
                if (dtBank.Rows.Count > 0)
                {
                    DropDownList ddlBank = (DropDownList)dv.FindControl("ddlBank");
                    ddlBank.DataSource = dtBank;
                    ddlBank.DataTextField = "BankName";
                    ddlBank.DataValueField = "BankID";
                    ddlBank.DataBind();
                }

                DataTable dt = DBHandler.GetResult("Get_BankandBranch", BranchID);
                if (dt.Rows.Count > 0)
                {
                    ((DropDownList)dv.FindControl("ddlBank")).SelectedValue = dt.Rows[0]["BankID"].ToString();

                    DataTable dtBranch = DBHandler.GetResult("Get_BranchbyBankID", dt.Rows[0]["BankID"].ToString());
                    if (dtBranch.Rows.Count > 0)
                    {
                        DropDownList ddlBranch = (DropDownList)dv.FindControl("ddlBranch");
                        ddlBranch.DataSource = dtBranch;
                        ddlBranch.DataTextField = "Branch";
                        ddlBranch.DataValueField = "BranchID";
                        ddlBranch.DataBind();
                    }
                    ((DropDownList)dv.FindControl("ddlBranch")).SelectedValue = BranchID;

                    TextBox txtIFSC = (TextBox)dv.FindControl("txtIFSC");
                    TextBox txtMICR = (TextBox)dv.FindControl("txtMICR");
                    txtIFSC.Text = dt.Rows[0]["IFSC"].ToString();
                    txtMICR.Text = dt.Rows[0]["MICR"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void populateJSON()
    {
        try
        {
            EmployeeMasters empMaster = new EmployeeMasters();
            dtMaster = (DataTable)Session["EmployeeMaster"];
            if (Session["EmployeeMaster"] != null)
            {
                foreach (DataRow dr in dtMaster.Rows)
                {
                    //Personal Information
                    empMaster.EmployeeID = Int32.Parse(dtMaster.Rows[0]["EmployeeID"].ToString());
                    empMaster.EmployeeCode = dr["EmployeeCode"].ToString();
                    empMaster.EmployeeNo = dr["EmployeeNo"].ToString();
                    empMaster.EmployeeType = dr["EmployeeType"].ToString();
                    empMaster.EmpClassification = dr["EmpClassification"].ToString();
                    empMaster.EmployeeName = dr["EmployeeName"].ToString();
                    empMaster.EmployeeReligion = dr["EmployeeReligion"].ToString();
                    empMaster.EmployeeQualification = dr["EmployeeQualification"].ToString();
                    empMaster.EmployeeMaritalStatus = dr["EmployeeMaritalStatus"].ToString();
                    empMaster.EmployeeCaste = dr["EmployeeCaste"].ToString();
                    empMaster.EmployeeDOB = dr["EmployeeDOB"].ToString();
                    empMaster.EmployeeGender = dr["EmployeeGender"].ToString();
                    ////Address Information
                    empMaster.EmployeePresentAddress = dr["EmployeePresentAddress"].ToString();
                    empMaster.EmployeePresentState = dr["EmployeePresentState"].ToString();
                    empMaster.EmployeePresentDistrict = dr["EmployeePresentDistrict"].ToString();
                    empMaster.EmployeePresentCity = dr["EmployeePresentCity"].ToString();
                    empMaster.EmployeePresentPin = dr["EmployeePresentPin"].ToString();
                    empMaster.EmployeePresentPhone = dr["EmployeePresentPhone"].ToString();
                    empMaster.EmployeePresentMobile = dr["EmployeePresentMobile"].ToString();
                    empMaster.EmployeePresentEmail = dr["EmployeePresentEmail"].ToString();
                    empMaster.EmployeePermanentAddress = dr["EmployeePermanentAddress"].ToString();
                    empMaster.EmployeePermanentState = dr["EmployeePermanentState"].ToString();
                    empMaster.EmployeePermanentDistrict = dr["EmployeePermanentDistrict"].ToString();
                    empMaster.EmployeePermanentCity = dr["EmployeePermanentCity"].ToString();
                    empMaster.EmployeePermanentPin = dr["EmployeePermanentPin"].ToString();
                    empMaster.EmployeePermanentPhone = dr["EmployeePermanentPhone"].ToString();
                    //////Official Details
                    empMaster.EmployeeDOJ = dr["EmployeeDOJ"].ToString();
                    empMaster.EmployeeDOR = dr["EmployeeDOR"].ToString();
                    empMaster.EmployeeDesignation = dr["EmployeeDesignation"].ToString();
                    empMaster.EmployeeCooperative = dr["EmployeeCooperative"].ToString();
                    empMaster.EmployeeDA = dr["EmployeeDA"].ToString();
                    empMaster.PFCode = dr["PFCode"].ToString();
                    empMaster.DNI = dr["DNI"].ToString();
                    empMaster.HRAFlag = dr["HRAFlag"].ToString();
                    empMaster.HRAFixedAmount = dr["HRAFixedAmount"].ToString();
                    empMaster.HRA = dr["HRA"].ToString();
                    empMaster.EffectiveFrom = dr["EffectiveFrom"].ToString();
                    empMaster.QuarterAllot = dr["QuarterAllot"].ToString();
                    empMaster.OLDBasic = dr["OLDBasic"].ToString();
                    empMaster.LicenceFees = dr["LicenceFees"].ToString();
                    empMaster.SpouseAmount = dr["SpouseAmount"].ToString();
                    empMaster.HealthScheme = dr["HealthScheme"].ToString();
                    empMaster.DOJPresentDept = dr["DOJPresentDept"].ToString();
                    empMaster.PhysicallyChallenged = dr["PhysicallyChallenged"].ToString();
                    empMaster.Group = dr["Group"].ToString();
                    empMaster.Category = dr["Category"].ToString();
                    empMaster.Status = dr["Status"].ToString();
                    empMaster.PayScaleType = dr["PayScaleType"].ToString();
                    empMaster.PayScale = dr["PayScale"].ToString();
                    empMaster.CoopMembership = dr["CoopMembership"].ToString();
                    empMaster.HRACategory = dr["HRACategory"].ToString();
                    empMaster.PANNO = dr["PANNO"].ToString();
                    empMaster.EffectiveTo = dr["EffectiveTo"].ToString();
                    empMaster.House = dr["House"].ToString();
                    empMaster.PCTBasic = dr["PCTBasic"].ToString();
                    empMaster.SpouseQuarter = dr["SpouseQuarter"].ToString();
                    empMaster.LWP = dr["LWP"].ToString();
                    empMaster.IR = dr["IR"].ToString();
                    empMaster.HealthSchemeDetails = dr["HealthSchemeDetails"].ToString();
                    ////Bank Details
                    empMaster.BranchID = dr["BranchID"].ToString();
                    empMaster.BankAccountCode = dr["BankAccountCode"].ToString();
                    ////Phota and Signature
                    empMaster.Photo = dr["Photo"].ToString();
                    empMaster.Signature = dr["Signature"].ToString();
                    //Others
                    empMaster.SectorID = Int32.Parse(dr["SectorID"].ToString());
                    empMaster.CenterID = Int32.Parse(dr["CenterID"].ToString());
                }
                //EmployeePayDetails empPayDetail = new EmployeePayDetails();
                //dtPayDetails = (DataTable)Session["PayDetails"];
                //if (Session["PayDetails"] != null)
                //{
                //    foreach (DataRow dr in dtPayDetails.Rows)
                //    {
                //        empPayDetail.EmployeePayDetailID = Int32.Parse(dtPayDetails.Rows[0]["EmployeePayDetailID"].ToString());
                //        empPayDetail.EmployeeID = Int32.Parse(dr["EmployeeID"].ToString());
                //        empPayDetail.EarnDeductionID = Int32.Parse(dr["EarnDeductionID"].ToString());
                //        empPayDetail.Amount = dr["Amount"].ToString();
                //    }
                //}
                List<AddEmployeePayDetails> emppd = new List<AddEmployeePayDetails>();
                if (HttpContext.Current.Session["AddPayDetails"] != null)
                {
                    DataTable dtDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                    foreach (DataRow dr1 in dtDetail.Rows)
                    {
                        AddEmployeePayDetails bc = new AddEmployeePayDetails();
                        bc.EmployeePayDetailID = Int32.Parse(dr1["EmployeePayDetailID"].ToString());
                        bc.EmployeeID = Int32.Parse(dr1["EmployeeID"].ToString());
                        bc.EarnDeductionID = Int32.Parse(dr1["EarnDeductionID"].ToString());
                        bc.EarnDeduction = dr1["EarnDeduction"].ToString();
                        bc.Amount = dr1["Amount"].ToString();
                    }
                }
                Employee emp = new Employee();
                emp.employeeMaster = empMaster;
                emp.empAddPayDetails = emppd.ToArray();
                ltlj.Text = "<input type='hidden' value='" + emp.ToJSON()+ "' id='empJSON' name='empJSON'/>";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlEarn = (DropDownList)dv1.FindControl("ddlEarnDeduction");
            TextBox txtAmount = (TextBox)dv1.FindControl("Amount");
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("Earn/Deduction");
            dt.Columns.Add("Amount");
            DataRow dr = dt.NewRow();
            dr["Earn/Deduction"] = ddlEarn.SelectedValue;
            dr["Amount"] = txtAmount.Text;
            dt.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AddDetails(int EarnDeductionID, string EarnDeduction, string Amount, string PayScale, string PSType, int Pid, string DA, int ptaxval, string QA,
                                           string HRACat, int Houseid, string chkHRAFlag, string txtHRAValue, string hrafromddl, string LicFee, string SQ, string SQAmount,
                                           string health, string houseorbasic, string rentAmount, string pctBasic, string IR, string GpfRate, string CpfRate, string FathersName, string AddressRemarks, string BranchInCharge)
     {
        DataTable dtAddPayDetails; double TotlDeduction = 0; double Total = 0.00;
        string JSONVal = ""; 
        try
        {
            Employee emp = new Employee();
            if (HttpContext.Current.Session["AddPayDetails"] == null)
            {
                dtAddPayDetails = new DataTable();
                dtAddPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtAddPayDetails);
                HttpContext.Current.Session["AddPayDetails"] = dtAddPayDetails;
            }
            else
            {
                dtAddPayDetails  = (DataTable)HttpContext.Current.Session["AddPayDetails"] ;
            }
            string a = EarnDeduction;
            string EarnDedType = a.Substring(a.Length - 2, 2 - 1);
            var val = dtAddPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
            if (val.Length == 0)
            {
                string PayBand = ""; double minScale = 0; double maxScale = 0; double GradePay = 0; double BasicPay = 0.00; 
                double FinalDAAmount = 0; double FinalHRAAmount = 0;
                double PTax = 0; double finalHRA = 0; double HRADeduction = 0; double IRAmt = 0; double cpfAmountEar; double GpfAmountDed; Double CpfAmount;
                


                if (EarnDedType == "E")
                {
                    if (EarnDeductionID == 2)
                    {
                        double amt = Convert.ToDouble(Amount);
                        string PayS = PayScale;
                        if (PayS != "0")
                        {
                            if (PSType == "O")
                            {
                                string[] lines = PayS.Split('-');
                                for (int i = 0; i <= lines.Length; i++)
                                {
                                    minScale = Int32.Parse(lines[0].ToString());
                                    maxScale = Int32.Parse(lines[lines.Length-1].ToString());
                                }
                                GradePay = 0;
                            }
//------------------------------------------------------------------------------------------------
                            else if (PSType == "K")
                            {
                                string[] lines = PayS.Split('-');
                                for (int i = 0; i <= lines.Length; i++)
                                {
                                    PayBand = lines[0].ToString();
                                    minScale = Int32.Parse(lines[1].ToString());
                                    string VAL = lines[2].ToString();
                                    maxScale = Int32.Parse(VAL.ToString().Substring(0, VAL.ToString().IndexOf('('))); ;
                                }
                                DataTable dtgdpay = DBHandler.GetResult("Get_GrayPay", Pid);
                                GradePay = Convert.ToDouble(dtgdpay.Rows[0][0].ToString());

                                IRAmt = Math.Round(((Convert.ToDouble(Amount)) * Convert.ToDouble(IR)) / 100);
                            }
                            else if (PSType == "S")
                            {
                                string[] lines = PayS.Split('-');
                                for (int i = 0; i <= lines.Length; i++)
                                {
                                    PayBand = lines[0].ToString();
                                    minScale = Int32.Parse(lines[1].ToString());
                                    string VAL = lines[2].ToString();
                                    maxScale = Int32.Parse(VAL.ToString().Substring(0, VAL.ToString().IndexOf('('))); ;
                                }
                                DataTable dtgdpay = DBHandler.GetResult("Get_GrayPay", Pid);
                                GradePay = Convert.ToDouble(dtgdpay.Rows[0][0].ToString());

                                IRAmt = Math.Round(((Convert.ToDouble(Amount)) * Convert.ToDouble(IR)) / 100);
                            }
                         else
                            {
                                string[] lines = PayS.Split('-');
                                for (int i = 0; i <= lines.Length; i++)
                                {
                                    //PayBand = lines[0].ToString();
                                    minScale = Int32.Parse(lines[0].ToString());
                                    string VAL = lines[1].ToString();
                                    maxScale = Int32.Parse(VAL.ToString().Substring(0, VAL.ToString().IndexOf('('))); ;
                                }
                                DataTable dtgdpay = DBHandler.GetResult("Get_GrayPay", Pid);
                                GradePay = Convert.ToDouble(dtgdpay.Rows[0][0].ToString());

                                IRAmt = Math.Round(((Convert.ToDouble(Amount)) * Convert.ToDouble(IR)) / 100);
                            }
//----------------------------------------------------------------------------------------------------
                           if (amt >= minScale && amt <= maxScale)
                            {
                                BasicPay = amt + GradePay;
                                double da = Convert.ToDouble(DA); double hra = Convert.ToDouble(hrafromddl);
                                double finalDA = Convert.ToDouble((BasicPay * da) / 100);

                                if (QA == "Y")
                                {
                                    if (HRACat == "G" || HRACat == "V")
                                    {
                                        finalHRA = 0;
                                    }
                                    else if (HRACat == "A")
                                    {
                                        DataTable dtHra = DBHandler.GetResult("Get_RentAmountforHRAHouse", Houseid);
                                        if (dtHra.Rows.Count > 0 && houseorbasic == "House")
                                        {
                                            double HRAHouseRent = Convert.ToDouble(dtHra.Rows[0]["RentAmt"].ToString());
                                            string HouseCat = dtHra.Rows[0]["HouseCategory"].ToString();
                                                finalHRA = HRAHouseRent;
                                                HRADeduction = HRAHouseRent;
                                        }
                                        else
                                        {
                                            double basic = Convert.ToDouble((BasicPay * Convert.ToDouble(pctBasic.Equals("") ? "0" : pctBasic)) / 100);
                                                finalHRA = Convert.ToDouble(basic);
                                                HRADeduction = Convert.ToDouble(basic);
                                        }
                                    }
                                    else if (HRACat == "L")
                                    {
                                        DataTable dtHra = DBHandler.GetResult("Get_RentAmountforHRAHouse", Houseid);
                                        if (dtHra.Rows.Count > 0 && houseorbasic == "House")
                                        {
                                            double HRAHouseRent = Convert.ToDouble(dtHra.Rows[0]["RentAmt"].ToString());
                                            string HouseCat = dtHra.Rows[0]["HouseCategory"].ToString();
                                            //finalHRA = HRAHouseRent;
                                            HRADeduction = Convert.ToDouble(HRAHouseRent);
                                        }
                                        else
                                        {
                                            double basic = Convert.ToDouble((BasicPay * Convert.ToDouble(pctBasic.Equals("") ? "0" : pctBasic)) / 100);
                                            //finalHRA = Convert.ToDouble(rentAmount);
                                            HRADeduction = Convert.ToDouble(basic);
                                        }
                                    }
                                }
                                else
                                {
                                    if (chkHRAFlag == "Y")
                                    {
                                        finalHRA = Convert.ToDouble(txtHRAValue);
                                    }
                                    else
                                    {
                                        if (SQ == "Y")
                                        {
                                            double SPAmt = Convert.ToDouble(SQAmount);
                                            double HraCals = Convert.ToDouble((BasicPay * hra) / 100);
                                            DataTable dts = DBHandler.GetResult("Get_MaxHRAAmount", hra);
                                            double MaxAmounts = Convert.ToDouble(dts.Rows[0]["MaxAmount"].ToString());
                                            double valamt = SPAmt + HraCals;
                                            if (valamt > MaxAmounts)
                                            {
                                                finalHRA = MaxAmounts - SPAmt;
                                            }
                                            else
                                            {
                                                finalHRA = HraCals;
                                            }
                                        }
                                        else
                                        {
                                            DataTable dt = DBHandler.GetResult("Get_MaxHRAAmount", hra);
                                            double MaxAmount = Convert.ToDouble(dt.Rows[0]["MaxAmount"].ToString());
                                            double HraCal = Convert.ToDouble((BasicPay * hra) / 100);
                                            if (HraCal > MaxAmount)
                                            {
                                                finalHRA = MaxAmount;
                                            }
                                            else
                                            {
                                                finalHRA = HraCal;
                                            }
                                        }
                                    }
                                }

                                FinalDAAmount = Math.Round(finalDA);
                                FinalHRAAmount = Math.Round(finalHRA);

                            }
                        }
                        DataRow dr = dtAddPayDetails.NewRow();
                        dr["EarnDeductionID"] = 2;
                        dr["EarnDeduction"] = "Band Pay-(E)";
                        dr["Amount"] = Convert.ToInt32(amt);
                        dr["EarnDeductionType"] = EarnDedType;
                        dtAddPayDetails.Rows.Add(dr);

                        if (PSType == "O")
                        {

                        }
                        else
                        {
                            DataRow dr1 = dtAddPayDetails.NewRow();
                            dr1["EarnDeductionID"] = 9;
                            dr1["EarnDeduction"] = "Grade Pay-(E)";
                            dr1["Amount"] = GradePay;
                            dr1["EarnDeductionType"] = "E";
                            dtAddPayDetails.Rows.Add(dr1);
                        }
			DataRow dr2 = dtAddPayDetails.NewRow();
                        dr2["EarnDeductionID"] = 3;
                        dr2["EarnDeduction"] = "Da-(E)";
                        dr2["Amount"] = FinalDAAmount;
                        dr2["EarnDeductionType"] = "E";
                        dtAddPayDetails.Rows.Add(dr2);
                        //*******************Sup*******************************************
                       // "(band pay+ grade pay+ da)* @cpf";
                        CpfAmount = Math.Round((Convert.ToDouble((amt + GradePay + FinalDAAmount) * Convert.ToDouble(CpfRate)))/100);

                        DataRow dr9 = dtAddPayDetails.NewRow();
                        dr9["EarnDeductionID"] = 33;
                        dr9["EarnDeduction"] = "Company PF-(D)";
                        dr9["Amount"] = CpfAmount;
                        dr9["EarnDeductionType"] = "D";
                        dtAddPayDetails.Rows.Add(dr9);

                        //"(band pay+ grade pay+ da)* @gpf";

                        GpfAmountDed = Math.Round((Convert.ToDouble((amt+ GradePay + FinalDAAmount ) * Convert.ToDouble(GpfRate)))/100);

                        DataRow dr10= dtAddPayDetails.NewRow();
                        dr10["EarnDeductionID"] = 14;
                        dr10["EarnDeduction"] = "EPF-(D)";
                        dr10["Amount"] = GpfAmountDed;
                        dr10["EarnDeductionType"] = "D";
                        dtAddPayDetails.Rows.Add(dr10);

                        cpfAmountEar = Math.Round((Convert.ToDouble((amt + GradePay + FinalDAAmount) * Convert.ToDouble(CpfRate))) / 100);

                        DataRow dr11 = dtAddPayDetails.NewRow();
                        dr11["EarnDeductionID"] = 41;
                        dr11["EarnDeduction"] = "Company PF-(E)";
                        dr11["Amount"] = cpfAmountEar;
                        dr11["EarnDeductionType"] = "E";
                        dtAddPayDetails.Rows.Add(dr11);

                        //*****************************************************************
                        

                        DataRow dr3 = dtAddPayDetails.NewRow();
                        dr3["EarnDeductionID"] = 4;
                        dr3["EarnDeduction"] = "Hra-(E)";
                        dr3["Amount"] = FinalHRAAmount;
                        dr3["EarnDeductionType"] = "E";
                        dtAddPayDetails.Rows.Add(dr3);

                        if (health != "Y")
                        {
                            DataTable dtMedicalAllow = DBHandler.GetResult("Get_ExactPayValbyEDID", 7);
                            if (dtMedicalAllow.Rows.Count > 0)
                            {
                                double MedicalAllowAmount = Convert.ToDouble(dtMedicalAllow.Rows[0]["PayVal"].ToString());
                                DataRow dr4 = dtAddPayDetails.NewRow();
                                dr4["EarnDeductionID"] = 7;
                                dr4["EarnDeduction"] = "MEDICAL ALLOWANCE-(E)";
                                dr4["Amount"] = Math.Round(MedicalAllowAmount);
                                dr4["EarnDeductionType"] = "E";
                                dtAddPayDetails.Rows.Add(dr4);
                            }
                        }

                        if (HRACat == "A")
                        {
                            DataRow dr5 = dtAddPayDetails.NewRow();
                            dr5["EarnDeductionID"] = 18;
                            dr5["EarnDeduction"] = "HRA Deduction-(D)";
                            dr5["Amount"] = HRADeduction;
                            dr5["EarnDeductionType"] = "D";
                            dtAddPayDetails.Rows.Add(dr5);
                        }
                        if (HRACat == "L")
                        {
                            DataRow dr6 = dtAddPayDetails.NewRow();
                            dr6["EarnDeductionID"] = 18;
                            dr6["EarnDeduction"] = "HRA Deduction-(D)";
                            dr6["Amount"] = HRADeduction;
                            dr6["EarnDeductionType"] = "D";
                            dtAddPayDetails.Rows.Add(dr6);
                        }

                    }
                    else
                    {
                        DataRow dr = dtAddPayDetails.NewRow();
                        dr["EarnDeductionID"] = EarnDeductionID;
                        dr["EarnDeduction"] = EarnDeduction;
                        dr["Amount"] = Amount;
                        dr["EarnDeductionType"] = EarnDedType;
                        dtAddPayDetails.Rows.Add(dr);
                        HttpContext.Current.Session["AddPayDetails"] = dtAddPayDetails;
                    }
                }
                else
                {
                    DataRow dr = dtAddPayDetails.NewRow();
                    dr["EarnDeductionID"] = EarnDeductionID;
                    dr["EarnDeduction"] = EarnDeduction;
                    dr["Amount"] = Amount;
                    dr["EarnDeductionType"] = EarnDedType;
                    dtAddPayDetails.Rows.Add(dr);
                    HttpContext.Current.Session["AddPayDetails"] = dtAddPayDetails;
                }

                for (int i = 0; i < dtAddPayDetails.Rows.Count; i++)
                {
                    string Edtype = dtAddPayDetails.Rows[i]["EarnDeductionType"].ToString();
                    double t = Convert.ToDouble(dtAddPayDetails.Rows[i]["Amount"]);
                    if (Edtype == "E")
                    {
                        Total = Total + t;
                    }
                    else
                    {
                        TotlDeduction = TotlDeduction + t;
                    }
                }


                if (ptaxval == 1)
                {
                    DataTable dtslab = DBHandler.GetResult("Get_Slab", Total);
                    PTax = Convert.ToDouble(dtslab.Rows[0][2].ToString());
                }
                else
                {
                    PTax = 0;
                }

                var vals = dtAddPayDetails.Select("EarnDeductionID=" + 5);
                if (vals.Length == 0)
                {
                    DataRow dr4 = dtAddPayDetails.NewRow();
                    dr4["EarnDeductionID"] = 5;
                    dr4["EarnDeduction"] = "PTax-(D)";
                    dr4["Amount"] = PTax;
                    dr4["EarnDeductionType"] = "D";
                    dtAddPayDetails.Rows.Add(dr4);
                }
                else
                {
                    DataRow[] dr2 = dtAddPayDetails.Select("EarnDeductionID=" + 5);
                    foreach (DataRow row in dr2)
                    {
                        row["EarnDeductionID"] = 5;
                        row["EarnDeduction"] = "PTax-(D)";
                        row["Amount"] = PTax;
                        row["EarnDeductionType"] = "D";
                        row.AcceptChanges();
                    }
                }

                var valsq = dtAddPayDetails.Select("EarnDeductionID=" + 39);
                if (valsq.Length == 0)
                {
                    DataRow dr8 = dtAddPayDetails.NewRow();
                    dr8["EarnDeductionID"] = 39;
                    dr8["EarnDeduction"] = "IR-(E)";
                    dr8["Amount"] = IRAmt;
                    dr8["EarnDeductionType"] = "E";
                    dtAddPayDetails.Rows.Add(dr8);
                }
                //else
                //{
                //    DataRow[] dr2 = dtAddPayDetails.Select("EarnDeductionID=" + 5);
                //    foreach (DataRow row in dr2)
                //    {
                //        row["EarnDeductionID"] = 39;
                //        row["EarnDeduction"] = "IR-(E)";
                //        row["Amount"] = IRAmt;
                //        row["EarnDeductionType"] = "E";
                //        row.AcceptChanges();
                //    }
                //}

            }

                    if (dtAddPayDetails.Rows.Count > 0)
                    {
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row;

                        foreach (DataRow r in dtAddPayDetails.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dtAddPayDetails.Columns)
                            {
                                row.Add(col.ColumnName, r[col]);
                            }
                            rows.Add(row);
                        }

                        JSONVal = rows.ToJSON();
                             
                    }
                    if (TotlDeduction > Total)
                    {
                        var value = dtAddPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                        if (value.Length != 0)
                        {
                            DataRow[] dr2 = dtAddPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                            foreach (DataRow row in dr2)
                            {
                                row.Delete();
                            }

                        }
                    }
         }
        catch (Exception ex)
         {
            throw new Exception(ex.Message);
        }
        return JSONVal; 

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveEmployee(string employeeForm, string SecID, string isNewEmp)
    {
        string JSONVal = "";
        try
        {
            DataTable dtEncodeValue = DBHandler.GetResult("Get_SalPayMonth", SecID, HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);
            if (dtEncodeValue.Rows.Count > 0)
            { 
            string EncValue=dtEncodeValue.Rows[0]["EncodeValue"].ToString();
            if (EncValue == "" || EncValue == null)
            {
                string noNewLines = employeeForm.Replace("\n", "");
                string smsResult = "";
                Employee emp = JSONJavascriptHelper.FromJSON<Employee>(noNewLines.ToString());
                EmployeeMasters master = emp.employeeMaster;
                AddEmployeePayDetails[] payDetails = emp.empAddPayDetails;
                DataTable dtMaster = (DataTable)HttpContext.Current.Session["EmployeeMaster"];
                if (dtMaster == null)
                {
                    dtMaster = new DataTable();
                    dtMaster = EmployeeDataHandling.CreateEmpMasterTable(dtMaster);
                    EmployeeDataHandling.InsertMaster(dtMaster, master);
                    HttpContext.Current.Session["EmployeeMaster"] = dtMaster;
                }
                else
                {
                    if (master != null)
                    {
                        if (master.EmployeeID <= 0)
                        {
                            EmployeeDataHandling.InsertMaster(dtMaster, master);
                        }
                        else
                        {
                            if (dtMaster != null && master != null)
                                EmployeeDataHandling.UpdateMaster(dtMaster, master);
                        }
                    }
                }
                DataTable dtPayDetails = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                if (dtPayDetails == null)
                {
                    dtPayDetails = new DataTable();
                    dtPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtPayDetails);
                    EmployeeDataHandling.InsertPayDetail(dtPayDetails);
                    HttpContext.Current.Session["AddPayDetails"] = dtPayDetails;
                }
                else
                {
                }
                int EmoloyeeID = 0; string EmpNo = "";
                if (dtMaster != null && master != null)
                {
                    int AdminID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    if (isNewEmp == "New")
                        EmoloyeeID = EmployeeDataHandling.SaveMaster(dtMaster, master, AdminID);

                    if (EmoloyeeID > 0)
                    {
                        if (dtPayDetails.Rows.Count > 0)
                        {
                            DataTable dt = DBHandler.GetResult("Get_EmpNobyEmployeeID", EmoloyeeID);
                            if (dt.Rows.Count > 0)
                            { 
                            EmpNo=dt.Rows[0]["EmpNo"].ToString();
                            emp.EmpNo = EmpNo;
                            }
                             EmployeeDataHandling.SavePayDetails(dtPayDetails, EmoloyeeID, AdminID);

                            try
                            {
                                smsResult = "SUCCESS";
                            }
                            catch (Exception exp)
                            {

                            }
                            emp.SMSResult = smsResult;
                        }
                    }
                }
                JSONVal = emp.ToJSON();
                dtMaster = null;
                payDetails = null;

                HttpContext.Current.Session["EmployeeMaster"] = null;
                HttpContext.Current.Session["PayDetails"] = null;
            }
            else
            {
                string result = "Not";
                JSONVal = result.ToJSON();
            }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateEmployee(string employeeForm, string SecID)
    {
        string JSONVal = "";
        //try
        //{
            DataTable dtEncodeValue = DBHandler.GetResult("Get_SalPayMonth", SecID, HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);
            if (dtEncodeValue.Rows.Count > 0)
            {
                string EncValue = dtEncodeValue.Rows[0]["EncodeValue"].ToString();
                if (EncValue == "" || EncValue == null)
                {
            string noNewLines = employeeForm.Replace("\n", "");
            string smsResult = "";
            Employee emp = JSONJavascriptHelper.FromJSON<Employee>(noNewLines.ToString());
            EmployeeMasters master = emp.employeeMaster;
            AddEmployeePayDetails[] payDetails = emp.empAddPayDetails;

            DataTable dtMaster = (DataTable)HttpContext.Current.Session["EmployeeMaster"];
            if (dtMaster == null)
            {
                dtMaster = new DataTable();
                dtMaster = EmployeeDataHandling.CreateEmpMasterTable(dtMaster);
                EmployeeDataHandling.InsertMasterforUpdate(dtMaster, master);
                HttpContext.Current.Session["EmployeeMaster"] = dtMaster;
            }
            else
            {
                if (master != null)
                {
                    if (master.EmployeeID > 0)
                    {
                        EmployeeDataHandling.InsertMasterforUpdate(dtMaster, master);
                    }
                }
            }
            DataTable dtPayDetails = (DataTable)HttpContext.Current.Session["AddPayDetails"];
            if (dtPayDetails == null)
            {
                dtPayDetails = new DataTable();
                dtPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtPayDetails);
                EmployeeDataHandling.InsertPayDetail(dtPayDetails);
                HttpContext.Current.Session["AddPayDetails"] = dtPayDetails;
            }
            else { }

            int EmoloyeeID = 0;
            string Remarks = "";
            if (dtMaster != null && master != null)
            {
                int AdminID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                DataTable dtPopUpStatus = (DataTable)HttpContext.Current.Session["PopUpStatus"];
                if (dtPopUpStatus.Rows.Count > 0)
                {
                    string orderNo = dtPopUpStatus.Rows[0]["OrderNo"].ToString();
                    string orderDate = dtPopUpStatus.Rows[0]["OrderDate"].ToString();
                    string remarks = dtPopUpStatus.Rows[0]["Remarks"].ToString();
                    Remarks=orderNo+"#"+orderDate+"#"+remarks;
                }
                EmoloyeeID = EmployeeDataHandling.SaveMasterforUpdate(dtMaster, master, AdminID, Remarks);

                    if (dtPayDetails.Rows.Count > 0 || dtPayDetails.Rows.Count==0)
                    {
                       EmployeeDataHandling.SavePayDetails(dtPayDetails, EmoloyeeID, AdminID);
                        try
                        {
                            smsResult = "SUCCESS";
                        }
                        catch (Exception exp)
                        {

                        }
                        emp.SMSResult = smsResult;
                    
                }
            }
            JSONVal = emp.ToJSON();
            dtMaster = null;
            payDetails = null;

            HttpContext.Current.Session["EmployeeMaster"] = null;
            HttpContext.Current.Session["PayDetails"] = null;
            }
            else
            {
                string result = "Not";
                JSONVal = result.ToJSON();
            }
            }
        //}
        //catch (Exception ex)
        //{
        //    throw new Exception(ex.Message);
        //}
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeletePayDetail(string employeeForm, string EarnDeductionID, int noofrows)
    {
        string JSONVal = ""; double Total = 0; double PTax = 0;
        DataTable dtPayDetail;
        try
        {
            int DetID =Convert.ToInt32(EarnDeductionID);
            string noNewLines = employeeForm.Replace("\n", "");
            Employee emp = JSONJavascriptHelper.FromJSON<Employee>(noNewLines.ToString());
            EmployeeMasters master = emp.employeeMaster;
            AddEmployeePayDetails[] payDetails = emp.empAddPayDetails;
            DataTable dtMaster = (DataTable)HttpContext.Current.Session["EmployeeMaster"];

            DataTable dtPayDetails = (DataTable)HttpContext.Current.Session["AddPayDetails"];
            if (dtPayDetails == null)
            {
                dtPayDetails = new DataTable();
                dtPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtPayDetails);
                HttpContext.Current.Session["AddPayDetails"] = dtPayDetails;
            }
            else
            {
                if (dtPayDetails.Rows.Count > 0)
                {
                    EmployeeDataHandling.DeletePayDetail(dtPayDetails, DetID, noofrows);
                }

            }
            dtPayDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
            if (dtPayDetail.Rows.Count > 0)
            {
                for (int i = 0; i < dtPayDetail.Rows.Count; i++)
                {
                    string Edtype = dtPayDetail.Rows[i]["EarnDeductionType"].ToString();
                    if (Edtype == "E")
                    {
                        double t = Convert.ToDouble(dtPayDetail.Rows[i]["Amount"]);
                        Total = Total + t;
                    }
                }

                DataTable dtslab = DBHandler.GetResult("Get_Slab", Total);
                PTax = Convert.ToDouble(dtslab.Rows[0][2].ToString());

                var vals = dtPayDetail.Select("EarnDeductionID=" + 5);
                if (vals.Length == 0)
                {
                    DataRow dr4 = dtPayDetail.NewRow();

                    dr4["EarnDeductionID"] = 5;
                    dr4["EarnDeduction"] = "PTax-(D)";
                    dr4["Amount"] = PTax;
                    dr4["EarnDeductionType"] = "D";
                    dtPayDetail.Rows.Add(dr4);
                }
                else
                {
                    DataRow[] dr2 = dtPayDetail.Select("EarnDeductionID=" + 5);
                    foreach (DataRow row in dr2)
                    {
                        row["EarnDeductionID"] = 5;
                        row["EarnDeduction"] = "PTax-(D)";
                        row["Amount"] = PTax;
                        row["EarnDeductionType"] = "D";
                        row.AcceptChanges();
                    }
                }
                
                //var valsq = dtPayDetail.Select("EarnDeductionID=" + 39);
                //if (valsq.Length == 0)
                //{
                //    DataRow dr8 = dtPayDetail.NewRow();
                //    dr8["EarnDeductionID"] = 39;
                //    dr8["EarnDeduction"] = "IR-(E)";
                //    dr8["Amount"] = 0;
                //    dr8["EarnDeductionType"] = "E";
                //    dtPayDetail.Rows.Add(dr8);
                //}
                 
                //else
                //{
                //    DataRow[] dr2 = dtPayDetail.Select("EarnDeductionID=" + 5);
                //    foreach (DataRow row in dr2)
                //    {
                //        row["EarnDeductionID"] = 39;
                //        row["EarnDeduction"] = "IR-(E)";
                //        row["Amount"] = 0;
                //        row["EarnDeductionType"] = "E";
                //        row.AcceptChanges();
                //    }
                //}
            }

            List<AddEmployeePayDetails> emppd = new List<AddEmployeePayDetails>();
            if (HttpContext.Current.Session["AddPayDetails"] != null)
            {
                DataTable dtDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                foreach (DataRow dr1 in dtDetail.Rows)
                {
                    AddEmployeePayDetails bc = new AddEmployeePayDetails();
                    bc.EarnDeductionID = Int32.Parse(dr1["EarnDeductionID"].ToString());
                    bc.EarnDeduction = dr1["EarnDeduction"].ToString();
                    bc.Amount = dr1["Amount"].ToString();
                    bc.EarnDeductionType = dr1["EarnDeductionType"].ToString();
                    emppd.Add(bc);
                }
            }
            emp.employeeMaster = master;
            emp.empAddPayDetails = emppd.ToArray();

           JSONVal = emp.ToJSON();
           
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdatePayDetail(int EarnDeductionID, string EarnDeduction, string Amount, string PayScale, string PSType, int Pid, string DA, int ptaxval,
                                                string QA, string HRACat, int Houseid, string chkHRAFlag, string txtHRAValue, string hrafromddl, string LicFee,
                                                string health, string houseorbasic, string rentAmount, string SQ, string SQAmount, string pctBasic, string IR, string CpfRate, string GpfRate, string FathersName, string AddressRemarks, string BranchInCharge)
     {
        double TotlDeduction = 0; double Total = 0.00;
        string JSONVal = "";
        DataTable dtPayDetail;
        try
        {
            Employee emp = new Employee();
            DataTable dtPayDetails = (DataTable)HttpContext.Current.Session["AddPayDetails"];
            if (dtPayDetails == null)
            {
                dtPayDetails = new DataTable();
                dtPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtPayDetails);
                HttpContext.Current.Session["AddPayDetails"] = dtPayDetails;
            }
            else
            {
                dtPayDetails  = (DataTable)HttpContext.Current.Session["AddPayDetails"] ;
            }

            string a = EarnDeduction;
            string EarnDedType = a.Substring(a.Length - 2, 2 - 1);
             var val = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                    if (val.Length != 0)
                    {
                        string PayBand = ""; double minScale = 0; double maxScale = 0; double GradePay = 0; double BasicPay = 0.00;
                        double FinalDAAmount = 0.00; double FinalHRAAmount = 0.00;
                        double PTax = 0; double finalHRA = 0; double HRADeduction = 0; double FinalIR = 0.00; double IRAmt = 0; double CpfAmount = 0; double cpfAmountEar = 0; double GpfAmountDed = 0;


                        if (EarnDedType == "E")
                        {
                            if (EarnDeductionID == 2)
                            {
                                double amt = Convert.ToDouble(Amount);
                                string PayS = PayScale;
                                if (PayS != "0")
                                {
                                    if (PSType == "O")
                                    {
                                        string[] lines = PayS.Split('-');
                                        for (int i = 0; i <= lines.Length; i++)
                                        {
                                            minScale = Int32.Parse(lines[0].ToString());
                                            maxScale = Int32.Parse(lines[lines.Length - 1].ToString());
                                        }
                                        GradePay = 0;
                                    }
                                    else if (PSType == "K")
                                    {
                                        string[] lines = PayS.Split('-');
                                        for (int i = 0; i <= lines.Length; i++)
                                        {
                                            PayBand = lines[0].ToString();
                                            minScale = Int32.Parse(lines[1].ToString());
                                            string VAL = lines[2].ToString();
                                            maxScale = Int32.Parse(VAL.ToString().Substring(0, VAL.ToString().IndexOf('('))); ;
                                        }
                                        DataTable dtgdpay = DBHandler.GetResult("Get_GrayPay", Pid);
                                        GradePay = Convert.ToDouble(dtgdpay.Rows[0][0].ToString());

                                        IRAmt = Math.Round(((Convert.ToDouble(Amount)) * Convert.ToDouble(IR)) / 100);
                                    }
                                    else if (PSType == "S")
                                    {
                                        string[] lines = PayS.Split('-');
                                        for (int i = 0; i <= lines.Length; i++)
                                        {
                                            PayBand = lines[0].ToString();
                                            minScale = Int32.Parse(lines[1].ToString());
                                            string VAL = lines[2].ToString();
                                            maxScale = Int32.Parse(VAL.ToString().Substring(0, VAL.ToString().IndexOf('('))); ;
                                        }
                                        DataTable dtgdpay = DBHandler.GetResult("Get_GrayPay", Pid);
                                        GradePay = Convert.ToDouble(dtgdpay.Rows[0][0].ToString());

                                        IRAmt = Math.Round(((Convert.ToDouble(Amount)) * Convert.ToDouble(IR)) / 100);
                                    }
                                    else
                                    {
                                        string[] lines = PayS.Split('-');
                                        for (int i = 0; i <= lines.Length; i++)
                                        {
                                            //PayBand = lines[0].ToString();
                                            minScale = Int32.Parse(lines[0].ToString());
                                            string VAL = lines[1].ToString();
                                            maxScale = Int32.Parse(VAL.ToString().Substring(0, VAL.ToString().IndexOf('('))); ;
                                        }
                                        DataTable dtgdpay = DBHandler.GetResult("Get_GrayPay", Pid);
                                        GradePay = Convert.ToDouble(dtgdpay.Rows[0][0].ToString());

                                        IRAmt = Math.Round(((Convert.ToDouble(Amount)) * Convert.ToDouble(IR)) / 100);
                                    }
                                    if (amt >= minScale && amt <= maxScale)
                                    {
                                        BasicPay = amt + GradePay;
                                        double da = Convert.ToDouble(DA); double hra = Convert.ToDouble(hrafromddl);
                                        double finalDA = Convert.ToDouble((BasicPay * da) / 100);

                                        double ir = Convert.ToDouble(IR);
                                        double finalIR = Convert.ToDouble((amt * ir) / 100);
                                        FinalIR = Math.Round(finalIR, 0, MidpointRounding.AwayFromZero);
                                        //FinalIR = 0;
                                        if (QA == "Y")
                                        {
                                            if (HRACat == "G" || HRACat == "V")
                                            {
                                                finalHRA = 0;
                                            }
                                            else if (HRACat == "A")
                                            {
                                                DataTable dtHra = DBHandler.GetResult("Get_RentAmountforHRAHouse", Houseid);
                                                if (dtHra.Rows.Count > 0 && houseorbasic == "House")
                                                {
                                                    double HRAHouseRent = Convert.ToDouble(dtHra.Rows[0]["RentAmt"].ToString());
                                                    string HouseCat = dtHra.Rows[0]["HouseCategory"].ToString();
                                                    finalHRA = HRAHouseRent;
                                                    HRADeduction = HRAHouseRent;
                                                }
                                                else
                                                {

                                                    double basic = Convert.ToDouble((BasicPay * Convert.ToDouble(pctBasic.Equals("") ? "0" : pctBasic)) / 100);
                                                    finalHRA = Convert.ToDouble(basic);
                                                    HRADeduction = Convert.ToDouble(basic);
                                                }
                                            }
                                            else if (HRACat == "L")
                                            {
                                                DataTable dtHra = DBHandler.GetResult("Get_RentAmountforHRAHouse", Houseid);
                                                if (dtHra.Rows.Count > 0 && houseorbasic == "House")
                                                {
                                                    double HRAHouseRent = Convert.ToDouble(dtHra.Rows[0]["RentAmt"].ToString());
                                                    string HouseCat = dtHra.Rows[0]["HouseCategory"].ToString();
                                                    //finalHRA = HRAHouseRent;
                                                    HRADeduction = Convert.ToDouble(HRAHouseRent);
                                                }
                                                else
                                                {
                                                    double basic = Convert.ToDouble((BasicPay * Convert.ToDouble(pctBasic.Equals("") ? "0" : pctBasic)) / 100);
                                                    //finalHRA = Convert.ToDouble(rentAmount);
                                                    HRADeduction = Convert.ToDouble(basic);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (chkHRAFlag == "Y")
                                            {
                                                finalHRA = Convert.ToDouble(txtHRAValue);
                                            }
                                            else
                                            {
                                                if (SQ == "Y")
                                                {
                                                    double MaxAmounts = 0.0;
                                                    double SPAmt = Convert.ToDouble(SQAmount);
                                                    double HraCals = Convert.ToDouble((BasicPay * hra) / 100);
                                                    DataTable dts = DBHandler.GetResult("Get_MaxHRAAmount", hra);
                                                    if (dts.Rows.Count > 0)
                                                        MaxAmounts = Convert.ToDouble(dts.Rows[0]["MaxAmount"].ToString());
                                                    else
                                                        MaxAmounts = 0.0;

                                                    double valamt = SPAmt + HraCals;
                                                    if (valamt > MaxAmounts)
                                                    {
                                                        finalHRA = MaxAmounts - SPAmt;
                                                    }
                                                    else
                                                    {
                                                        finalHRA = HraCals;
                                                    }
                                                }
                                                else
                                                {
                                                    double MaxAmount = 0.0;
                                                    DataTable dt = DBHandler.GetResult("Get_MaxHRAAmount", hra);
                                                    if (dt.Rows.Count > 0)
                                                    MaxAmount = Convert.ToDouble(dt.Rows[0]["MaxAmount"].ToString());
                                                    else
                                                        MaxAmount = 0.0;

                                                    double HraCal = Convert.ToDouble((BasicPay * hra) / 100);
                                                    if (HraCal > MaxAmount)
                                                    {
                                                        finalHRA = MaxAmount;
                                                    }
                                                    else
                                                    {
                                                        finalHRA = HraCal;
                                                    }
                                                }
                                            }
                                        }

                                        FinalDAAmount = Math.Round(finalDA,0,MidpointRounding.AwayFromZero);
                                        FinalHRAAmount = Math.Round(finalHRA, 0, MidpointRounding.AwayFromZero);

                                    }
                                }
                                DataRow[] dr1 = dtPayDetails.Select("EarnDeductionID=" + 2);
                                foreach (DataRow row in dr1)
                                {
                                    row["EarnDeductionID"] = 2;
                                    row["EarnDeduction"] = "Band Pay-(E)";
                                    row["Amount"] = Convert.ToInt32(amt);
                                    row["EarnDeductionType"] = EarnDedType;
                                    row.AcceptChanges();
                                }

                                if (PSType == "O")
                                {
                                    DataRow[] dr2 = dtPayDetails.Select("EarnDeductionID=" + 9);
                                    foreach (DataRow row in dr2)
                                    {
                                        row.Delete();
                                        //row.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    var vals = dtPayDetails.Select("EarnDeductionID=" + 9);
                                    if (vals.Length == 0)
                                    {
                                        DataRow dr2 = dtPayDetails.NewRow();
                                        dr2["EarnDeductionID"] = 9;
                                        dr2["EarnDeduction"] = "Grade Pay-(E)";
                                        dr2["Amount"] = GradePay;
                                        dr2["EarnDeductionType"] = "E";
                                        dtPayDetails.Rows.Add(dr2);
                                    }
                                    else
                                    {
                                        DataRow[] dr2 = dtPayDetails.Select("EarnDeductionID=" + 9);
                                        foreach (DataRow row in dr2)
                                        {
                                            row["EarnDeductionID"] = 9;
                                            row["EarnDeduction"] = "Grade Pay-(E)";
                                            row["Amount"] = GradePay;
                                            row["EarnDeductionType"] = "E";
                                            row.AcceptChanges();
                                        }
                                    }
                                }
                               
//------------------------------------------------------------------------------------------------
                              var valDA = dtPayDetails.Select("EarnDeductionID=" + 3);
                                if (valDA.Length == 0)
                                {
                                    DataRow dr10 = dtPayDetails.NewRow();
                                    dr10["EarnDeductionID"] = 3;
                                    dr10["EarnDeduction"] = "Da-(E)";
                                    dr10["Amount"] = FinalDAAmount;
                                    dr10["EarnDeductionType"] = "E";
                                    dtPayDetails.Rows.Add(dr10);
                                }
                                else
                                {
                                    DataRow[] dr3 = dtPayDetails.Select("EarnDeductionID=" + 3);
                                    foreach (DataRow row in dr3)
                                    {
                                        row["EarnDeductionID"] = 3;
                                        row["EarnDeduction"] = "Da-(E)";
                                        row["Amount"] = FinalDAAmount;
                                        row["EarnDeductionType"] = "E";
                                        row.AcceptChanges();
                                    }
                                }
//----------------------------------------------------------------------------------------------
                                var valhra = dtPayDetails.Select("EarnDeductionID=" + 4);
                                   if (valhra.Length == 0)
                                   {
                                       DataRow dr9 = dtPayDetails.NewRow();
                                       dr9["EarnDeductionID"] = 4;
                                       dr9["EarnDeduction"] = "Hra-(E)";
                                       dr9["Amount"] = FinalHRAAmount;
                                       dr9["EarnDeductionType"] = "E";
                                       dtPayDetails.Rows.Add(dr9);
                                   }
                                   else
                                   {
                                       DataRow[] dr4 = dtPayDetails.Select("EarnDeductionID=" + 4);
                                       foreach (DataRow row in dr4)
                                       {
                                           row["EarnDeductionID"] = 4;
                                           row["EarnDeduction"] = "Hra-(E)";
                                           row["Amount"] = FinalHRAAmount;
                                           row["EarnDeductionType"] = "E";
                                           row.AcceptChanges();
                                       }
                                   }
//----------------------------------------------------------------------------------------------
                                   //*******************Sup*******************************************
                                   // "(band pay+ grade pay+ da)* @cpf";
                                   CpfAmount = Math.Round((Convert.ToDouble((amt + GradePay + FinalDAAmount) * Convert.ToDouble(CpfRate))) / 100);
                                   var valCpfAmount = dtPayDetails.Select("EarnDeductionID=" + 33);
                                   
                                   if (valCpfAmount.Length == 0)
                                   {
                                       DataRow dr11 = dtPayDetails.NewRow();
                                       dr11["EarnDeductionID"] = 33;
                                       dr11["EarnDeduction"] = "Company PF-(D)";
                                       dr11["Amount"] = CpfAmount;
                                       dr11["EarnDeductionType"] = "D";
                                       dtPayDetails.Rows.Add(dr11);
                                   }
                                   else {
                                       DataRow[] drcpf = dtPayDetails.Select("EarnDeductionID=" + 33);
                                       foreach (DataRow row in drcpf)
                                       {
                                           row["EarnDeductionID"] = 33;
                                           row["EarnDeduction"] = "Company PF-(D)";
                                           row["Amount"] = CpfAmount;
                                           row["EarnDeductionType"] = "D";
                                           row.AcceptChanges();
                                       }
                                   }

                                  

                                   //"(band pay+ grade pay+ da)* @gpf";

                                   GpfAmountDed = Math.Round((Convert.ToDouble((amt + GradePay + FinalDAAmount) * Convert.ToDouble(GpfRate))) / 100);
                                   var valGpfAmount = dtPayDetails.Select("EarnDeductionID=" + 14);

                                   if (valGpfAmount.Length==0)
                                   {
                                       DataRow dr12 = dtPayDetails.NewRow();
                                       dr12["EarnDeductionID"] = 14;
                                       dr12["EarnDeduction"] = "EPF-(D)";
                                       dr12["Amount"] = GpfAmountDed;
                                       dr12["EarnDeductionType"] = "D";
                                       dtPayDetails.Rows.Add(dr12);
                                   }
                                   else{
                                       DataRow[] drGpf = dtPayDetails.Select("EarnDeductionID=" + 14);
                                       foreach (DataRow row in drGpf)
                                       {
                                           row["EarnDeductionID"] = 14;
                                           row["EarnDeduction"] = "EPF-(D)";
                                           row["Amount"] = GpfAmountDed;
                                           row["EarnDeductionType"] = "D";
                                           row.AcceptChanges();
                                       }
                                   }
                                   

                                   cpfAmountEar = Math.Round((Convert.ToDouble((amt + GradePay + FinalDAAmount) * Convert.ToDouble(CpfRate))) / 100);
                                   var valcpfAmountEar = dtPayDetails.Select("EarnDeductionID=" + 41);

                                   if (valcpfAmountEar.Length==0)
                                   {
                                       DataRow dr13 = dtPayDetails.NewRow();
                                       dr13["EarnDeductionID"] = 41;
                                       dr13["EarnDeduction"] = "Company PF-(E)";
                                       dr13["Amount"] = cpfAmountEar;
                                       dr13["EarnDeductionType"] = "E";
                                       dtPayDetails.Rows.Add(dr13);
                                   }
                                   else{
                                       DataRow[] drGpf = dtPayDetails.Select("EarnDeductionID=" + 41);
                                       foreach (DataRow row in drGpf)
                                       {
                                           row["EarnDeductionID"] = 41;
                                           row["EarnDeduction"] = "Company PF-(E)";
                                           row["Amount"] = cpfAmountEar;
                                           row["EarnDeductionType"] = "E";
                                           row.AcceptChanges();
                                       }
                                   }
                                   

                                   //*****************************************************************
//----------------------------------------------------------------------------------------------
//This is for IR Calculation

                                   var valir = dtPayDetails.Select("EarnDeductionID=" + 39);
                                   if (valir.Length == 0)
                                   {
                                       DataRow dr10 = dtPayDetails.NewRow();
                                       dr10["EarnDeductionID"] = 39;
                                       dr10["EarnDeduction"] = "IR-(E)";
                                       dr10["Amount"] = FinalIR;
                                       dr10["EarnDeductionType"] = "E";
                                       dtPayDetails.Rows.Add(dr10);
                                   }
                                   //else
                                   //{
                                   //    DataRow[] dr5 = dtPayDetails.Select("EarnDeductionID=" + 39);
                                   //    foreach (DataRow row in dr5)
                                   //    {
                                   //        row["EarnDeductionID"] = 39;
                                   //        row["EarnDeduction"] = "IR-(E)";
                                   //        row["Amount"] = 0;
                                   //        row["EarnDeductionType"] = "E";
                                   //        row.AcceptChanges();
                                   //    }
                                   //}
                                 
//----------------------------------------------------------------------------------------------
                                   if (health == "N")   // 'C' MEANS NOT APPLICABLE
                                   {
                                       var valMediAllow = dtPayDetails.Select("EarnDeductionID=" + 7);
                                       if (valMediAllow.Length == 0)
                                       {
                                           DataTable dtMedicalAllow = DBHandler.GetResult("Get_ExactPayValbyEDID", 7);
                                           if (dtMedicalAllow.Rows.Count > 0)
                                           {
                                               double MedicalAllowAmount = Convert.ToDouble(dtMedicalAllow.Rows[0]["PayVal"].ToString());
                                               DataRow dr4 = dtPayDetails.NewRow();
                                               dr4["EarnDeductionID"] = 7;
                                               dr4["EarnDeduction"] = "MEDICAL ALLOWANCE-(E)";
                                               dr4["Amount"] = Math.Round(MedicalAllowAmount);
                                               dr4["EarnDeductionType"] = "E";
                                               dtPayDetails.Rows.Add(dr4);
                                           }
                                       }
                                       else
                                       {
                                           DataTable dtMedicalAllow = DBHandler.GetResult("Get_ExactPayValbyEDID", 7);
                                           if (dtMedicalAllow.Rows.Count > 0)
                                           {
                                               double MedicalAllowAmount = Convert.ToDouble(dtMedicalAllow.Rows[0]["PayVal"].ToString());
                                               DataRow[] dr4 = dtPayDetails.Select("EarnDeductionID=" + 7);
                                               foreach (DataRow row in dr4)
                                               {
                                                   row["EarnDeductionID"] = 7;
                                                   row["EarnDeduction"] = "MEDICAL ALLOWANCE-(E)";
                                                   row["Amount"] = Math.Round(MedicalAllowAmount);
                                                   row["EarnDeductionType"] = "E";
                                                   row.AcceptChanges();
                                               }
                                           }
                                       }
                                   }
                                   else 
                                   {
                                       var vals = dtPayDetails.Select("EarnDeductionID=" + 7);
                                       if (vals.Length == 1)
                                       {
                                           DataRow[] dr5 = dtPayDetails.Select("EarnDeductionID=" + 7);
                                           foreach (DataRow row in dr5)
                                           {
                                               if (row.RowState != DataRowState.Deleted)
                                               {
                                                   row.Delete();
                                               }
                                           }
                                           dtPayDetails.AcceptChanges();
                                       }
                                   }
//------------------------------------------------------------------------------------------------
                                   if (HRACat == "G" || HRACat == "V")
                                   {
                                       var vals = dtPayDetails.Select("EarnDeductionID=" + 18);
                                       if (vals.Length == 1)
                                       {
                                           DataRow[] dr5 = dtPayDetails.Select("EarnDeductionID=" + 18);
                                           foreach (DataRow row in dr5)
                                           {
                                               if (row.RowState != DataRowState.Deleted)
                                               {
                                                   row.Delete();
                                               }
                                           }
                                           dtPayDetails.AcceptChanges();
                                       }
                                   }
//------------------------------------------------------------------------------------------------
                                   else if (HRACat == "A")
                                   {
                                    var vals = dtPayDetails.Select("EarnDeductionID=" + 18);
                                    if (vals.Length == 0)
                                    {
                                        DataRow dr5 = dtPayDetails.NewRow();
                                        dr5["EarnDeductionID"] = 18;
                                        dr5["EarnDeduction"] = "HRA Deduction-(D)";
                                        dr5["Amount"] = HRADeduction;
                                        dr5["EarnDeductionType"] = "D";
                                        dtPayDetails.Rows.Add(dr5);
                                    }
                                    else
                                    {
                                        DataRow[] dr5 = dtPayDetails.Select("EarnDeductionID=" + 18);
                                        foreach (DataRow row in dr5)
                                        {
                                            row["EarnDeductionID"] = 18;
                                            row["EarnDeduction"] = "HRA Deduction-(D)";
                                            row["Amount"] = HRADeduction;
                                            row["EarnDeductionType"] = "D";
                                            row.AcceptChanges();
                                        }
                                    }

                                    var valsss = dtPayDetails.Select("EarnDeductionID=" + 4);
                                    if (valsss.Length == 0)
                                    {
                                        DataRow dr6 = dtPayDetails.NewRow();
                                        dr6["EarnDeductionID"] = 4;
                                        dr6["EarnDeduction"] = "Hra-(E)";
                                        dr6["Amount"] = FinalHRAAmount;
                                        dr6["EarnDeductionType"] = "E";
                                        dtPayDetails.Rows.Add(dr6);
                                    }
                                    else
                                    {
                                        DataRow[] dr7 = dtPayDetails.Select("EarnDeductionID=" + 4);
                                        foreach (DataRow row in dr7)
                                        {
                                            row["EarnDeductionID"] = 4;
                                            row["EarnDeduction"] = "Hra-(E)";
                                            row["Amount"] = FinalHRAAmount;
                                            row["EarnDeductionType"] = "E";
                                            row.AcceptChanges();
                                        }
                                    }
                                }
                                   else if (HRACat == "L")
                                   {
                                       var vals = dtPayDetails.Select("EarnDeductionID=" + 18);
                                       if (vals.Length == 0)
                                       {
                                           DataRow dr6 = dtPayDetails.NewRow();
                                           dr6["EarnDeductionID"] = 18;
                                           dr6["EarnDeduction"] = "HRA Deduction-(D)";
                                           dr6["Amount"] = HRADeduction;
                                           dr6["EarnDeductionType"] = "D";
                                           dtPayDetails.Rows.Add(dr6);
                                       }
                                       else
                                       {
                                           DataRow[] dr6 = dtPayDetails.Select("EarnDeductionID=" + 18);
                                           foreach (DataRow row in dr6)
                                           {
                                               row["EarnDeductionID"] = 18;
                                               row["EarnDeduction"] = "HRA Deduction-(D)";
                                               row["Amount"] = HRADeduction;
                                               row["EarnDeductionType"] = "D";
                                               row.AcceptChanges();
                                           }
                                       }
                                   }
                                   else
                                   {
                                       var vals = dtPayDetails.Select("EarnDeductionID=" + 18);
                                       if (vals.Length == 1)
                                       {
                                           DataRow[] dr5 = dtPayDetails.Select("EarnDeductionID=" + 18);
                                           foreach (DataRow row in dr5)
                                           {
                                               if (row.RowState != DataRowState.Deleted)
                                               {
                                                   row.Delete();
                                               }
                                           }
                                           dtPayDetails.AcceptChanges();
                                       }
                                   }

                            }
                            else
                            {
                                DataRow[] dr7 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                                foreach (DataRow row in dr7)
                                {
                                    row["EarnDeductionID"] = EarnDeductionID;
                                    row["EarnDeduction"] = EarnDeduction;
                                    row["Amount"] = Amount;
                                    row["EarnDeductionType"] = EarnDedType;
                                    row.AcceptChanges();
                                }

                            }
                        }
                        else
                        {
                            DataRow[] dr8 = dtPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                            foreach (DataRow row in dr8)
                            {
                                row["EarnDeductionID"] = EarnDeductionID;
                                row["EarnDeduction"] = EarnDeduction;
                                row["Amount"] = Amount;
                                row["EarnDeductionType"] = EarnDedType;
                                row.AcceptChanges();
                            }
                        }

                        for (int i = 0; i < dtPayDetails.Rows.Count; i++)
                        {
                            string Edtype = dtPayDetails.Rows[i]["EarnDeductionType"].ToString();
                            double t = Convert.ToDouble(dtPayDetails.Rows[i]["Amount"]);
                            if (Edtype == "E")
                            {
                                Total = Total + t;
                            }
                            else
                            {
                                TotlDeduction = TotlDeduction + t;
                            }
                        }


                        if (ptaxval == 1)
                        {
                            DataTable dtslab = DBHandler.GetResult("Get_Slab", Total);
                            PTax = Convert.ToDouble(dtslab.Rows[0][2].ToString());
                        }
                        else
                        {
                            PTax = 0;
                        }

                        var valss = dtPayDetails.Select("EarnDeductionID=" + 5);
                        if (valss.Length == 0)
                        {
                            DataRow dr4 = dtPayDetails.NewRow();
                            dr4["EarnDeductionID"] = 5;
                            dr4["EarnDeduction"] = "PTax-(D)";
                            dr4["Amount"] = PTax;
                            dr4["EarnDeductionType"] = "D";
                            dtPayDetails.Rows.Add(dr4);
                        }
                        else
                        {
                            DataRow[] dr2 = dtPayDetails.Select("EarnDeductionID=" + 5);
                            foreach (DataRow row in dr2)
                            {
                                row["EarnDeductionID"] = 5;
                                row["EarnDeduction"] = "PTax-(D)";
                                row["Amount"] = PTax;
                                row["EarnDeductionType"] = "D";
                                row.AcceptChanges();
                            }
                        }



                    }

                dtPayDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                if (dtPayDetail.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;

                    foreach (DataRow r in dtPayDetail.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtPayDetail.Columns)
                        {
                            row.Add(col.ColumnName, r[col]);
                        }
                        rows.Add(row);
                    }

                    JSONVal = rows.ToJSON();
                }

                //if (TotlDeduction > Total)
                //{
                //    var value = dtPayDetail.Select("EarnDeductionID=" + EarnDeductionID);
                //    if (value.Length != 0)
                //    {
                        
                //        DataRow[] dr2 = dtPayDetail.Select("EarnDeductionID=" + EarnDeductionID);
                //        foreach (DataRow row in dr2)
                //        {
                //            row.Delete();
                //            dtPayDetail.AcceptChanges();

                //        }
                //    }
                //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ExactPayValbyEDID(string EarnDeductionID)
    {
        string JSONVal = ""; double Amt = 0;
        try
        {
            int DetID = Convert.ToInt32(EarnDeductionID);
            DataTable dt = DBHandler.GetResult("Get_ExactPayValbyEDID", DetID);
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow r in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }

                JSONVal = rows.ToJSON();
                //JSONVal = Amt.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetHouseRent(string HouseID)
    {
        string JSONVal = ""; double rentAmount = 0;
        try
        {
            int HID = Convert.ToInt32(HouseID);

            DataTable dt = DBHandler.GetResult("Get_RentAmountforHRAHouse", HID);
            //rentAmount = Convert.ToDouble(dt.Rows[0]["RentAmt"].ToString());
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow r in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }
                JSONVal = rows.ToJSON();
            }


        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GetEmpCodeWithName(string prefix, string Sectorid)
    {
        List<string> CodewithName = new List<string>();
        //customers.Add(string.Format("{0}-{1}", sdr["ContactName"], sdr["CustomerId"]));
        
        try
        {
            DataTable dtCodeName = DBHandler.GetResult("Get_SearchCodewithName", prefix, Sectorid==""?0:Convert.ToInt32(Sectorid));
            if (dtCodeName.Rows.Count > 0)
            {
                foreach (DataRow row in dtCodeName.Rows)
                {
                    CodewithName.Add(string.Format("{0}|{1}", row["EmpName"], row["EmpNo"]));
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return CodewithName.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Validate_DOB_DOJ(string DOB, string DOJ)
    {
        DataTable dt = DBHandler.GetResult("Validate_DOB_DOJ_DOJDept", DOB, DOJ);
        object obj = new  object();
        if (dt.Rows.Count>0)
        {
            var objdata = new {
                MSg = dt.Rows[0]["MSg"],
                MsgCode = dt.Rows[0]["MsgCode"]
            };
            obj=objdata;
        }
        return obj.ToJSON();
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Validate__DOJ_DOJDept(string DOJ, string DOJDept)
    {
        DataTable dt = DBHandler.GetResult("Validate_DOJ_DOJDept", DOJ, DOJDept);
        object obj = new object();
        if (dt.Rows.Count > 0)
        {
            var objdata = new
            {
                MSg = dt.Rows[0]["MSg"],
                MsgCode = dt.Rows[0]["MsgCode"]
            };
            obj = objdata;
        }
        return obj.ToJSON();
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string CheckEmpNoAvailable(string EmpNo)
    {
        string JSONVal = ""; double rentAmount = 0;
        try
        {
            DataTable dt = DBHandler.GetResult("Check_EmpNoAvailable", EmpNo);
            //rentAmount = Convert.ToDouble(dt.Rows[0]["RentAmt"].ToString());
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow r in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }
                JSONVal = rows.ToJSON();
            }
            //else
            //{
            //    JSONVal = "{Available: " + 0 + "}";
            //}

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetEmpTransferDetails(string txtEffeFrom, string txtEffeTo)
    {
        string JSONVal = ""; 
        try
        {
            DataTable dt = DBHandler.GetResult("Get_TransferEmployee", txtEffeFrom, txtEffeTo);

            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow r in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }
                JSONVal = rows.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AddSuspendedPayDetails(int EarnDeductionID, string EarnDeduction, string Amount)
    {
        DataTable dtAddPayDetails; double TotlDeduction = 0; double Total = 0.00;
        string JSONVal = "";
        try
        {
            Employee emp = new Employee();
            if (HttpContext.Current.Session["AddPayDetails"] == null)
            {
                dtAddPayDetails = new DataTable();
                dtAddPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtAddPayDetails);
                HttpContext.Current.Session["AddPayDetails"] = dtAddPayDetails;
            }
            else
            {
                dtAddPayDetails = (DataTable)HttpContext.Current.Session["AddPayDetails"];
            }

            var vals = dtAddPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
            if (vals.Length != 0)
            {
                DataRow[] dr2 = dtAddPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                foreach (DataRow row in dr2)
                {
                    //row["EarnDeductionID"] = 5;
                    //row["EarnDeduction"] = "PTax-(D)";
                    row["Amount"] = Amount;
                    //row["EarnDeductionType"] = "D";
                    row.AcceptChanges();
                }
            }
            else
            {
                string EarnDedType = EarnDeduction.Substring(EarnDeduction.Length - 2, 2 - 1);
                DataRow dr4 = dtAddPayDetails.NewRow();
                dr4["EarnDeductionID"] = EarnDeductionID;
                dr4["EarnDeduction"] = EarnDeduction;
                dr4["Amount"] = Amount;
                dr4["EarnDeductionType"] = EarnDedType;
                dtAddPayDetails.Rows.Add(dr4);
            }

            if (dtAddPayDetails.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow r in dtAddPayDetails.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtAddPayDetails.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }

                JSONVal = rows.ToJSON();

            }
            if (TotlDeduction > Total)
            {
                var value = dtAddPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                if (value.Length != 0)
                {
                    DataRow[] dr2 = dtAddPayDetails.Select("EarnDeductionID=" + EarnDeductionID);
                    foreach (DataRow row in dr2)
                    {
                        row.Delete();
                    }

                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeletePayDetailbySuspended(string employeeForm, string EarnDeductionID)
    {
        string JSONVal = ""; double Total = 0;
        DataTable dtPayDetail;
        try
        {
            int DetID = Convert.ToInt32(EarnDeductionID);
            string noNewLines = employeeForm.Replace("\n", "");
            Employee emp = JSONJavascriptHelper.FromJSON<Employee>(noNewLines.ToString());
            EmployeeMasters master = emp.employeeMaster;
            AddEmployeePayDetails[] payDetails = emp.empAddPayDetails;
            DataTable dtMaster = (DataTable)HttpContext.Current.Session["EmployeeMaster"];

            DataTable dtPayDetails = (DataTable)HttpContext.Current.Session["AddPayDetails"];
            if (dtPayDetails == null)
            {
                dtPayDetails = new DataTable();
                dtPayDetails = EmployeeDataHandling.CreatePayDetailsTable(dtPayDetails);
                HttpContext.Current.Session["AddPayDetails"] = dtPayDetails;
            }
            else
            {
                if (dtPayDetails.Rows.Count > 0)
                {
                    EmployeeDataHandling.DeletePayDetailbySuspended(dtPayDetails, DetID);
                }

            }
            dtPayDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
            if (dtPayDetail.Rows.Count > 0)
            {
                for (int i = 0; i < dtPayDetail.Rows.Count; i++)
                {
                    string Edtype = dtPayDetail.Rows[i]["EarnDeductionType"].ToString();
                    if (Edtype == "E")
                    {
                        double t = Convert.ToDouble(dtPayDetail.Rows[i]["Amount"]);
                        Total = Total + t;
                    }
                }

            }

            List<AddEmployeePayDetails> emppd = new List<AddEmployeePayDetails>();
            if (HttpContext.Current.Session["AddPayDetails"] != null)
            {
                DataTable dtDetail = (DataTable)HttpContext.Current.Session["AddPayDetails"];
                foreach (DataRow dr1 in dtDetail.Rows)
                {
                    AddEmployeePayDetails bc = new AddEmployeePayDetails();
                    bc.EarnDeductionID = Int32.Parse(dr1["EarnDeductionID"].ToString());
                    bc.EarnDeduction = dr1["EarnDeduction"].ToString();
                    bc.Amount = dr1["Amount"].ToString();
                    bc.EarnDeductionType = dr1["EarnDeductionType"].ToString();
                    emppd.Add(bc);
                }
            }
            emp.employeeMaster = master;
            emp.empAddPayDetails = emppd.ToArray();

            JSONVal = emp.ToJSON();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_StatusPopUpDetail(string Status, string OrderNo, string OrderDate, string Remarks)
    {
        string JSONVal = "";
        try
        {
            DataTable dtPopUpStatus = (DataTable)HttpContext.Current.Session["PopUpStatus"];
            if (dtPopUpStatus == null)
            {
                dtPopUpStatus = EmployeeDataHandling.CreatePopUpStatusTable(dtPopUpStatus);
                HttpContext.Current.Session["PopUpStatus"] = dtPopUpStatus;
            }
            else
            {
                dtPopUpStatus.Rows.Clear();
                DataRow dr = dtPopUpStatus.NewRow();
                dr["Status"] = Status;
                dr["OrderNo"] = OrderNo;
                dr["OrderDate"] = OrderDate;
                dr["Remarks"] = Remarks;
                dtPopUpStatus.Rows.Add(dr);

                HttpContext.Current.Session["PopUpStatus"] = dtPopUpStatus;
            }
            DataTable dt=(DataTable)HttpContext.Current.Session["PopUpStatus"];
            if (dt.Rows.Count > 0)
            {
                JSONVal = "Saved".ToJSON();
            }
            else 
            {
                JSONVal = "NotSaved".ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Check_Status(string Status, string SearchStatus)
    {
        string JSONVal = "";  string EmpStatus="";
        try
        {
            if (SearchStatus == "S")
                EmpStatus = "Suspended";
            else if(SearchStatus == "W")
                EmpStatus = "Withheld";
            else if (SearchStatus == "Y")
                EmpStatus = "Active";

            DataTable dtPopUpStatus = (DataTable)HttpContext.Current.Session["PopUpStatus"];

            if (dtPopUpStatus.Rows.Count > 0)
            {
                JSONVal = "Matched".ToJSON();
            }
            else
            {
                if (Status == EmpStatus || Status != EmpStatus)
                    JSONVal = "Matched".ToJSON();
                else 
                    JSONVal = "NotMatched".ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Admin_Search()
    {
        string JSONVal = ""; 
        try
        {
            string UserTypeCode = HttpContext.Current.Session[SiteConstants.SSN_USERTYPECODE].ToString();
            JSONVal = UserTypeCode.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Check_Exist(string BankAccNo)
    {
        string JSONVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Check_ExistBankAccountNo", BankAccNo);
            if (dt.Rows.Count > 0)
            {
                JSONVal = dt.Rows[0]["Value"].ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
     {
        try
        {
            if (dv.CurrentMode == FormViewMode.Edit)
            {
                Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
            }
            else
            {
                Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload(string str)
    {
        try
        {
            if (str == "Classification")
            {
                DataTable dt = DBHandler.GetResult("Load_Classification");
                return dt;
            }
            if (str == "Center")
            {
                DataTable dt = DBHandler.GetResult("Load_Center");
                return dt;
            }
            if (str == "Caste")
            {
                DataTable dt = DBHandler.GetResult("Load_Caste");
                return dt;
            }
            if (str == "Religion")
            {
                DataTable dt = DBHandler.GetResult("Load_Religion");
                return dt;
            }
            if (str == "Qualification")
            {
                DataTable dt = DBHandler.GetResult("Load_Qualification");
                return dt;
            }
            if (str == "Designation")
            {
                DataTable dt = DBHandler.GetResult("Load_Designation");
                return dt;
            }
            if (str == "Group")
            {
                DataTable dt = DBHandler.GetResult("Load_Group");
                return dt;
            }
            if (str == "Cooperative")
            {
                DataTable dt = DBHandler.GetResult("Load_Cooperative");
                return dt;
            }
            if (str == "Category")
            {
                DataTable dt = DBHandler.GetResult("Load_Category");
                return dt;
            }
            if (str == "Department")
            {
                DataTable dt = DBHandler.GetResult("Load_Department");
                return dt;
            }
            if (str == "PFRate")
            {
                DataTable dt = DBHandler.GetResult("Load_PFRate");
                return dt;
            }
            if (str == "PayScale")
            {
                DataTable dt = DBHandler.GetResult("Load_PayScale");
                return dt;
            }
            if (str == "Bank")
            {
                DataTable dt = DBHandler.GetResult("Load_Bank");
                return dt;
            }
            if (str == "EDName")
            {
                DataTable dt = DBHandler.GetDirectResult("Get_EarnDeduction");
                return dt;
            }
            if (str == "Employee")
            {
                DataTable dt = DBHandler.GetDirectResult("Get_EmployeeDdl");
                return dt;
            }

            else
            {
                return null;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //This is for Present Address 
    [WebMethod]     
    public static string BindState()
    {
        DataSet ds = DBHandler.GetResults("Load_State");
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<Student> liststudent = new List<Student>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Student objst = new Student();

                objst.StateId = Convert.ToInt32(dt.Rows[i]["StateId"]);
                objst.State = Convert.ToString(dt.Rows[i]["StateName"]);
                liststudent.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(liststudent);
    }
    public class Student
    {
        public int StateId { get; set; }
        public string State { get; set; }
    }
    [WebMethod]
    public static string CheckIsRound(int EdId)
    {
         int IsRound=0;
         DataTable dt= DBHandler.GetResult("CheckIsRoundByEdid", EdId);
         IsRound =Convert.ToInt32(dt.Rows[0][0].ToString());
         return IsRound.ToJSON();

    }
    [WebMethod]
    public static string BindDistricts(int StateID)
    {

        DataSet ds = DBHandler.GetResults("Load_District", StateID);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<Districts> listdistrict = new List<Districts>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Districts objst = new Districts();

                objst.DistID = Convert.ToInt32(dt.Rows[i]["DistID"]);
                objst.District = Convert.ToString(dt.Rows[i]["District"]);
                listdistrict.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listdistrict);
    }
    public class Districts
    {
        public int DistID { get; set; }
        public string District { get; set; }
    }

    //This is for Permanent address
    [WebMethod]                   
    public static string BindStateforPermanentAddress()
    {
        DataSet ds = DBHandler.GetResults("Load_State");
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<StateforPermanentAddress> liststudent = new List<StateforPermanentAddress>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                StateforPermanentAddress objst = new StateforPermanentAddress();

                objst.StateId = Convert.ToInt32(dt.Rows[i]["StateId"]);
                objst.State = Convert.ToString(dt.Rows[i]["StateName"]);
                liststudent.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(liststudent);
    }
    public class StateforPermanentAddress
    {
        public int StateId { get; set; }
        public string State { get; set; }
    }

    [WebMethod]
    public static string BindDistrictsforPermanentAddress(int StateID)
    {

        DataSet ds = DBHandler.GetResults("Load_District", StateID);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<DistrictsforPermanentAddress> listdistrict = new List<DistrictsforPermanentAddress>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DistrictsforPermanentAddress objst = new DistrictsforPermanentAddress();

                objst.DistID = Convert.ToInt32(dt.Rows[i]["DistID"]);
                objst.District = Convert.ToString(dt.Rows[i]["District"]);
                listdistrict.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listdistrict);
    }
    public class DistrictsforPermanentAddress
    {
        public int DistID { get; set; }
        public string District { get; set; }
    }

    //This is for Binding 'DA' on the Base of Employee Type
    [WebMethod]
    public static string BindDA(string EmpType)

    {

        DataSet ds = DBHandler.GetResults("Load_DA", EmpType);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<DA> listdA = new List<DA>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DA objst = new DA();

                objst.DAID = Convert.ToInt32(dt.Rows[i]["DAID"]);
                objst.DARate = Convert.ToString(dt.Rows[i]["DARate"]);
                listdA.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listdA);
    }
    public class DA
    {
        public int DAID { get; set; }
        public string DARate { get; set; }
    }

    //This is for Binding 'HRA' on the Base of Employee Type
    [WebMethod]
    public static string BindHRA(string EmpType)
    {

        DataSet ds = DBHandler.GetResults("Load_HRA", EmpType);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<HRAS> listHRAS = new List<HRAS>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HRAS objst = new HRAS();

                objst.HRAID = Convert.ToInt32(dt.Rows[i]["HRAID"]);
                objst.HRA = Convert.ToString(dt.Rows[i]["HRA"]);
                listHRAS.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listHRAS);
    }
    public class HRAS
    {
        public int HRAID { get; set; }
        public string HRA { get; set; }
    }

    //This is for Binding 'IR' on the Base of Employee Type
    [WebMethod]
    public static string BindIR(string EmpType)
    {

        DataSet ds = DBHandler.GetResults("Load_IR", EmpType);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<IR> listIR = new List<IR>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IR objst = new IR();

                objst.IRID = Convert.ToInt32(dt.Rows[i]["IRID"]);
                objst.IRRate = Convert.ToString(dt.Rows[i]["IRRate"]);
                listIR.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listIR);
    }
    public class IR
    {
        public int IRID { get; set; }
        public string IRRate { get; set; }
    }

    //This is for Binding 'House' on the Base of HRA Category
    [WebMethod]
    public static string BindHouse(string HouseCategory)
    {

        DataSet ds = DBHandler.GetResults("Load_House", HouseCategory);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<House> listHouse = new List<House>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                House objst = new House();

                objst.HouseID = Convert.ToInt32(dt.Rows[i]["HouseID"]);
                objst.HouseName = Convert.ToString(dt.Rows[i]["HouseName"]);
                listHouse.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listHouse);
    }
    public class House
    {
        public int HouseID { get; set; }
        public string HouseName { get; set; }
    }

    //This is for Binding 'Pay Scale' on the Base of Pay Scale Type
    [WebMethod]
    public static string BindPayScale(string EmpType)
    {

        DataSet ds = DBHandler.GetResults("Get_PayScalebyPayScaleType", EmpType);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<PayScales> listPayScale = new List<PayScales>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PayScales objst = new PayScales();

                objst.PayScaleID = Convert.ToInt32(dt.Rows[i]["PayScaleID"]);
                objst.PayScale = Convert.ToString(dt.Rows[i]["PayScale"]);
                listPayScale.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listPayScale);
    }
    public class PayScales
    {
        public int PayScaleID { get; set; }
        public string PayScale { get; set; }
    }

    //This is for Binding 'Pay Scale' on the Base of Employee Type
    [WebMethod]
    public static string GetPayScalebyEMPType(string EmpType)
    {

        DataSet ds = DBHandler.GetResults("get_PayScaleByEmpType", EmpType);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        List<payScalebyEmpType> listHRAS = new List<payScalebyEmpType>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                payScalebyEmpType objst = new payScalebyEmpType();

                objst.PayScaleID = dt.Rows[i]["PayScaleID"].ToString();
                objst.PayScale = dt.Rows[i]["PayScale"].ToString();
                listHRAS.Insert(i, objst);
            }

        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listHRAS);
    }
    public class payScalebyEmpType
    {
        public string PayScaleID { get; set; }
        public string PayScale { get; set; }
    }

    [WebMethod]
    public static string GetCenter(int SecID)
    {
        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataTable dt = ds.Tables[0]; 

        List<CenterbySector> listCenter = new List<CenterbySector>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CenterbySector objst = new CenterbySector();

                objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);

                listCenter.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listCenter);

    }
    public class CenterbySector
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
    }

    [WebMethod]
    public static string BindBranch(int BankID)
    {
        DataSet ds = DBHandler.GetResults("Get_BranchbyBankID", BankID);
        DataTable dt = ds.Tables[0];

        List<BranchbyBank> listbranch = new List<BranchbyBank>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                BranchbyBank objst = new BranchbyBank();

                objst.BranchID = Convert.ToInt32(dt.Rows[i]["BranchID"]);
                objst.Branch = Convert.ToString(dt.Rows[i]["Branch"]);

                listbranch.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listbranch);

    }
    public class BranchbyBank
    {
        public int BranchID { get; set; }
        public string Branch { get; set; }
    }

    [WebMethod]
    public static string GetIFSCandMICR(int BranchID)
    {
        DataSet ds = DBHandler.GetResults("Get_IFSCandMICR", BranchID);
        DataTable dt = ds.Tables[0];

        List<IfscMicr> listIfscMicr = new List<IfscMicr>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IfscMicr objst = new IfscMicr();

                objst.IFSC = Convert.ToString(dt.Rows[i]["IFSC"]);
                objst.MICR = Convert.ToString(dt.Rows[i]["MICR"]);

                listIfscMicr.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listIfscMicr);

    }
    public class IfscMicr
    {
        public string IFSC { get; set; }
        public string MICR { get; set; }
    }

    [WebMethod]
    public static string BindEarnDedbyStatus(string Status)
    {
        DataSet ds = DBHandler.GetResults("Get_EarnDeduction_ParamWise", Status);
        DataTable dt = ds.Tables[0];

        List<EarnDedbyStatus> listEarnDed = new List<EarnDedbyStatus>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                EarnDedbyStatus objst = new EarnDedbyStatus();

                objst.EDID = Convert.ToInt32(dt.Rows[i]["EDID"]);
                objst.EarnDeduction = Convert.ToString(dt.Rows[i]["EarnDeduction"]);

                listEarnDed.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listEarnDed);

    }
    public class EarnDedbyStatus
    {
        public int EDID { get; set; }
        public string EarnDeduction { get; set; }
    }

}