﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;



public partial class RPT_PaySlip : System.Web.UI.Page
{
    public string str1 = "";
    public static string Hdnsalmonth = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SetPageNoCache();
            if (!IsPostBack)
            {
            //  GenerateReport();
                if (Session[SiteConstants.SSN_INT_USER_ID] != null)
                {
                    DataTable dt = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
                    if (dt.Rows.Count > 0)
                    {
                        DropDownList ddlSector = (DropDownList)dv.FindControl("ddlSector");
                        ddlSector.DataSource = dt;
                        ddlSector.DataBind();
                    }
                }
                string Month="";
                string Today = DateTime.Now.ToString("dd/MM/yyyy");
                int year = Convert.ToInt32(Today.Substring(6, 4));
                int month = Convert.ToInt32(Today.Substring(3, 2));
                if (month < 10)
                {
                    Month = "0" + month;
                }
                else
                {
                    Month = Convert.ToString(month);
                }
                string payMonth = Convert.ToString(Month + "/" + year);
                TextBox txtPayMonth = (TextBox)dv.FindControl("txtPayMonth");
                txtPayMonth.Text = payMonth;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    //protected DataTable drpload()
    //{
    //    DataTable dt = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
    //    return dt;
    //}




    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                //Button cmdSave = (Button)dv.FindControl("cmdSave");
               // cmdSave.CommandName = "Edit";
               // cmdSave.ToolTip = "Update";
               // cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dtstr = DBHandler.GetResult("Get_Sector");
            tbl.DataSource = dtstr;
            tbl.DataBind();

            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    //protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList sec = (DropDownList)dv.FindControl("ddlSector");
    //        string SectorID = sec.SelectedValue;
    //        if (SectorID != "")
    //        {
    //            DataTable dtl = DBHandler.GetResult("Get_CenterbySector", SectorID);
    //            ((DropDownList)dv.FindControl("ddlLocation")).DataSource = dtl;
    //            ((DropDownList)dv.FindControl("ddlLocation")).DataTextField = "CenterName";
    //            ((DropDownList)dv.FindControl("ddlLocation")).DataValueField = "CenterID";
    //            ((DropDownList)dv.FindControl("ddlLocation")).Items.Clear();
    //            ((DropDownList)dv.FindControl("ddlLocation")).DataBind();
    //        }
    //        else
    //        { 
    //            ((DropDownList)dv.FindControl("ddlLocation")).Items.Clear();
    //            ((DropDownList)dv.FindControl("ddlLocation")).Items.Add("(Select Location)");
    //        }
    //    }
    //    catch(Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }

    //    PayMonth();
    //} 

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_DAByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlEmpType")).SelectedValue = dtResult.Rows[0]["EmpType"].ToString();
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_DAByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //protected void cmdSave_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        Button cmdSave = (Button)sender;
    //        if (cmdSave.CommandName == "Add")
    //        {
    //            TextBox txtDACode = (TextBox)dv.FindControl("txtDACode");
    //            DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
    //            TextBox txtDARate = (TextBox)dv.FindControl("txtDARate");
    //            TextBox txtMaxAmt = (TextBox)dv.FindControl("txtMaxAmt");
    //            TextBox txtEffFrm = (TextBox)dv.FindControl("txtEffFrm");
    //            TextBox txtEffTo = (TextBox)dv.FindControl("txtEffTo");

    //            DateTime EffFrm = DateTime.ParseExact(txtEffFrm.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
    //            DateTime EffTo = DateTime.ParseExact(txtEffTo.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

    //            DBHandler.Execute("Insert_DA", txtDACode.Text, ddlEmpType.SelectedValue, Convert.ToDouble(txtDARate.Text),
    //                Convert.ToDouble(txtMaxAmt.Text), EffFrm.ToShortDateString(), EffTo.ToShortDateString(),
    //                Session[SiteConstants.SSN_INT_USER_ID]);
    //            cmdSave.Text = "Add";
    //            dv.ChangeMode(FormViewMode.Insert);
    //            PopulateGrid();
    //            dv.DataBind();
    //            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
    //        }
    //        else if (cmdSave.CommandName == "Edit")
    //        {
    //            TextBox txtDACode = (TextBox)dv.FindControl("txtDACode");
    //            DropDownList ddlEmpType = (DropDownList)dv.FindControl("ddlEmpType");
    //            TextBox txtDARate = (TextBox)dv.FindControl("txtDARate");
    //            TextBox txtMaxAmt = (TextBox)dv.FindControl("txtMaxAmt");
    //            TextBox txtEffFrm = (TextBox)dv.FindControl("txtEffFrm");
    //            TextBox txtEffTo = (TextBox)dv.FindControl("txtEffTo");

    //            DateTime EffFrm = DateTime.ParseExact(txtEffFrm.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
    //            DateTime EffTo = DateTime.ParseExact(txtEffTo.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

    //            string ID = ((HiddenField)dv.FindControl("hdnID")).Value;

    //            DBHandler.Execute("Update_DA", ID, txtDACode.Text, ddlEmpType.SelectedValue, Convert.ToDouble(txtDARate.Text),
    //                Convert.ToDouble(txtMaxAmt.Text), EffFrm.ToShortDateString(), EffTo.ToShortDateString(),
    //                Session[SiteConstants.SSN_INT_USER_ID]);
    //            cmdSave.Text = "Create";
    //            dv.ChangeMode(FormViewMode.Insert);
    //            PopulateGrid();
    //            dv.DataBind();
    //            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
    //    }
    //}
    protected void cmdPrint_Click(object sender, EventArgs e)
    {
        GenerateReport();
        //Label lblrpt = (Label)dv.FindControl("lblrpt");

        
       
    }
    //protected void PayMonth()
    //{
    //    DropDownList sec = (DropDownList)dv.FindControl("ddlSector");
    //    string SectorID = sec.SelectedValue;
    //    TextBox A = (TextBox)this.Master.FindControl("txtSalFinYear");
    //    string SalFinYear = A.Text;
    //    if(SectorID != "")
    //    {
    //        DataTable dtpm = DBHandler.GetResult("Get_SalPayMonth", SectorID, SalFinYear);
    //        TextBox B = (TextBox)dv.FindControl("txtPayMonth");
    //        B.Text = dtpm.Rows[0][1].ToString();
            
    //    }
    //    else
    //    {
    //        TextBox B = (TextBox)dv.FindControl("txtPayMonth");
    //        B.Text = "";
    //    }
       
    //}


    [WebMethod]
    public static string GetCenter(int SecID, string SalFinYear)
    {


        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID, SalFinYear);
        DataTable dt = ds.Tables[0]; 
        DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonthID = ds1.Tables[0];
      



        List<CenterbySector> listCenter = new List<CenterbySector>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CenterbySector objst = new CenterbySector();

                objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);
                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                objst.PayMonthsID = Convert.ToInt32(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                Hdnsalmonth = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();
                listCenter.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listCenter);

    }
    public class CenterbySector
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
        public string PayMonths { get; set; }
        public int PayMonthsID { get; set; }
    }

    protected void GenerateReport()
    {
       
  
       // string SectorID = ((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue;
       // string SalMonthID = ((HiddenField)dv.FindControl("hdnSalMonthID")).Value;
      
       // DataTable dtcr = DBHandler.GetResult("get_payslip_new",SectorID,SalMonthID);
         
       // // getting value according to imageID and fill dataset

       // ReportDocument crystalReport = new ReportDocument(); // creating object of crystal report
       // crystalReport.Load(Server.MapPath("~/Reports/PaySlip_new.rpt")); // path of report
       // crystalReport.SetDatabaseLogon("kmda", "infotech", "saswati-pc", "db_kmda",true);
       // crystalReport.VerifyDatabase();
       //// crystalReport.SetDataSource(dtcr); // binding datatable
       // crystalReport.RecordSelectionFormula = "{tmp_payslip_monthly.SecID}=" +1+ "";
       // crystalReport.ReadRecords();
       // crystalReport.Refresh();

       // ExportOptions exportOpts = new ExportOptions();
       // DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
       //   //  crystalldeExportOptions.CreateDiskFileDestinationOptions();
       // PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();

       //// exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
       // //exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
       // string FOLDERNAME = "Reports_PDF/PS"+SectorID+SalMonthID+Session[SiteConstants.SSN_INT_USER_ID]+".pdf";
       // string PATH = Context.Server.MapPath("~/" + FOLDERNAME);
       // diskOpts.DiskFileName = PATH;
       // //exportOpts.ExportDestinationOptions = diskOpts;

       // //Report.Export(exportOpts);
       // exportOpts = crystalReport.ExportOptions;
       // {
       //     exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
       //     exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
       //     exportOpts.DestinationOptions = diskOpts;
       //     exportOpts.FormatOptions = CrFormatTypeOptions;
       // }
       // crystalReport.Export();
        
       // str1 += "<a target='_blank' href='" + FOLDERNAME + "'>Download</a>";
       
       
    }


    public void SetPageNoCache()
    {
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
        //Response.Cache.SetAllowResponseInBrowserHistory(false);
        //Response.Cache.SetNoStore();
        Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
        Response.AppendHeader("Expires", "0");

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.UtcNow.AddSeconds(-1));
        Response.Cache.SetAllowResponseInBrowserHistory(false);
        Response.Cache.SetNoStore();
        HttpResponse.RemoveOutputCacheItem("/RTP_PaySlip.aspx");
    }

}