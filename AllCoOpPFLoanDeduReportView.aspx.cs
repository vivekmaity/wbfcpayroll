﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;        
using CrystalDecisions.Shared;             

public partial class AllCoOpPFLoanDeduReportView : System.Web.UI.Page
{       
    static int ReportNameVal=0;             
    static int SectorID=0;              
    static int CenterID = 0;
    static int SalMonthId = 0;            
    static string EmpType="";
    static string EmpStatus = "";
    static string SalaryFinYear = "";
    static int DeduEdid = 0;
    static int LoanEdid = 0;
    static int UserId = 0;
    static string ExportFileName = "";
    static string ExportSectorName = "";
    static string SalMonth = "";
    String ConnString = "";
    static string EmpStatus1 = "";
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPaperSize();
        BindReport(crystalReport);
    }

    protected void ExportPDF(object sender, EventArgs e)
    {
        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);
      
        ExportFormatType formatType = ExportFormatType.NoFormat;
        switch (rbFormat.SelectedItem.Value)
        {
            case "Excel":
                formatType = ExportFormatType.Excel;
                break;
            case "PDF":
                formatType = ExportFormatType.PortableDocFormat;
                break;          
        }
        crystalReport.ExportToHttpResponse(formatType, Response, true, ExportFileName);
        Response.End();
    }

    private void BindReport(ReportDocument crystalReport)
    {
        ConnString = DataAccess.DBHandler.GetConnectionString();         
        SalaryFinYear = Convert.ToString(HttpContext.Current.Request.QueryString["SalaryFinYear"]);
        SalMonthId = Convert.ToInt32(HttpContext.Current.Request.QueryString["SalMonthID"]);
        SectorID = Convert.ToInt32(HttpContext.Current.Request.QueryString["SectorId"]);
        CenterID = Convert.ToInt32(HttpContext.Current.Request.QueryString["CenterID"]);
        EmpType = Convert.ToString(HttpContext.Current.Request.QueryString["EmpType"]);
        EmpStatus = Convert.ToString(HttpContext.Current.Request.QueryString["EmpStatus"]);
        DeduEdid = Convert.ToInt32(HttpContext.Current.Request.QueryString["DeduEdid"]);
        LoanEdid = Convert.ToInt32(HttpContext.Current.Request.QueryString["LoanEdid"]);
        UserId = Convert.ToInt32(HttpContext.Current.Request.QueryString["UserId"]);
        ReportNameVal = Convert.ToInt32(HttpContext.Current.Request.QueryString["ReportID"]);
        SalMonth = Convert.ToString(HttpContext.Current.Request.QueryString["SalMonth"]);
        if (EmpStatus.Length > 1)
        {
            EmpStatus1 = "[" + "'Y'" + "," + "'S'" + "]";        
            EmpStatus = EmpStatus1;
        }                     
        else
        {
            EmpStatus = "[" + "'" + EmpStatus + "'" + "]";
        }
        
        SqlConnection con = new SqlConnection(ConnString);
        DataTable dt = new DataTable();
        //SqlDataAdapter(Query,Connection)
        SqlDataAdapter adp = new SqlDataAdapter("Select SectorName from MST_Sector where SectorID=" + SectorID + "", con);
        adp.Fill(dt);
        // dt.Rows[index][ColumnName]
        if (dt.Rows.Count > 0)                
        {
            ExportSectorName = dt.Rows[0]["SectorName"].ToString();
            //ExportFileName=ExportSectorName + "_" + "Bank List" + "(" + SalMonth + ")";
        }

        if (ReportNameVal == 1)
        { 
            crystalReport.Load(Server.MapPath("~/Reports/Pay_Schedule_COOP_kmda.rpt"));
            //crystalReport.RecordSelectionFormula = "{MST_Sector.SectorID}=" + SectorID + " and {MST_SalaryMonth.SalMonthID}=" + SalMonthId + "  and {MST_Employee.Status}='Y'";
            crystalReport.RecordSelectionFormula = "{MST_Employee.Status} IN " + EmpStatus + "";
            ExportFileName = ExportSectorName + "_" + "Pay Schedule COOP" + "(" + SalMonth + ")";
        }
        if (ReportNameVal == 2 || ReportNameVal == 3)
        {
            crystalReport.Load(Server.MapPath("~/Reports/PaySchedule_CPF_all_PF_state.rpt"));
            //crystalReport.RecordSelectionFormula = "{MST_Sector.SectorID}=" + SectorID + " and {MST_SalaryMonth.SalMonthID}=" + SalMonthId + "  and {MST_Employee.Status}='Y'";
            crystalReport.RecordSelectionFormula = "{MST_Employee.Status} IN " + EmpStatus + "";
            ExportFileName = ExportSectorName + "_" + "All CPF And PF" + "(" + SalMonth + ")";
        }

        crystalReport.DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
        crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
        crystalReport.SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
        crystalReport.Refresh();
        crystalReport.SetParameterValue("@SalaryFinYear", SalaryFinYear);
        crystalReport.SetParameterValue("@SalMonth", SalMonthId);
        crystalReport.SetParameterValue("@SectorID", SectorID);
        crystalReport.SetParameterValue("@CenterID", CenterID);
        crystalReport.SetParameterValue("@EmpType", EmpType);
        crystalReport.SetParameterValue("@Status",EmpStatus);
        crystalReport.SetParameterValue("@Dedu_EDID", DeduEdid);
        crystalReport.SetParameterValue("@Loan_EDID", LoanEdid);
        crystalReport.SetParameterValue("@UserID", UserId);
        CrystalReportViewer1.DisplayToolbar = true;
        CrystalReportViewer1.Zoom(150);  // Page Width
        CrystalReportViewer1.Visible = true;
        CrystalReportViewer1.ReportSource = crystalReport;
    }

    protected void SetPaperSize()
    {
        ddlPaperSize.Items.Clear();
        ddlPaperSize.Items.Add("Page - A4");
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //if (this.crystalReport != null)
        //{
        this.crystalReport.Close();
        this.crystalReport.Dispose();
        //}
    }

    public void OnConfirmprint(object sender, EventArgs e)
    {
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {
            ReportDocument crystalReport = new ReportDocument();
            BindReport(crystalReport);
            ExportFormatType formatType = ExportFormatType.NoFormat;
            switch (rbFormat.SelectedItem.Value)
            {
                case "Excel":
                    formatType = ExportFormatType.Excel;
                    break;
                case "PDF":
                    formatType = ExportFormatType.PortableDocFormat;
                    break;
            }
            crystalReport.ExportToHttpResponse(formatType, Response, false, ExportFileName);
            Response.End();
        }
    }

}