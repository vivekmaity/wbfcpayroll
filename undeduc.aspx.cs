﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;

public partial class undeduc: System.Web.UI.Page{

    public static string hdnEDId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

   

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void PopulateGrid()
    {
        try
        {

            DataTable dt = DBHandler.GetResult("Get_Deduct");
            tbl.DataSource = dt;
            tbl.DataBind();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_DeductbyID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                hdnEDId= dtResult.Rows[0]["EDID"].ToString();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddletyp")).SelectedValue = dtResult.Rows[0]["EDType"].ToString();

                    ((DropDownList)dv.FindControl("ddldtyp")).SelectedValue = dtResult.Rows[0]["DeducType"].ToString();
                }



            }
            else if (flag == 2)
            {
                DBHandler.Execute("DELETE_DeducBYID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                //TextBox txtEDC = (TextBox)dv.FindControl("txtEDC");
                TextBox txtEDN = (TextBox)dv.FindControl("txtEDN");
                TextBox txtABBV = (TextBox)dv.FindControl("txtABBV");
                DropDownList ddletyp = (DropDownList)dv.FindControl("ddletyp");
                DropDownList ddldtyp = (DropDownList)dv.FindControl("ddldtyp");
                TextBox txtpval = (TextBox)dv.FindControl("txtpval");
                TextBox txtmxval = (TextBox)dv.FindControl("txtmxval");
                TextBox txtminval = (TextBox)dv.FindControl("txtminval");
                TextBox txtfncname = (TextBox)dv.FindControl("txtfncname");
                TextBox txtinodr = (TextBox)dv.FindControl("txtinodr");
                TextBox txtcalodr = (TextBox)dv.FindControl("txtcalodr");
                TextBox txtprntodr = (TextBox)dv.FindControl("txtprntodr");
                CheckBox check1 = (CheckBox)dv.FindControl("check1");
                CheckBox check2 = (CheckBox)dv.FindControl("check2");
                

               

                //TextBox txtDOR = (TextBox)dv.FindControl("txtDOR");
                // DateTime dt1 = DateTime.ParseExact(txtDOR.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                DBHandler.Execute("INSERT_Deduct",
                    DBNull.Value,
                    txtEDN.Text.Equals("") ? DBNull.Value : (object)(txtEDN.Text),
                    txtABBV.Text.Equals("") ? DBNull.Value : (object)(txtABBV.Text),
                    ddletyp.SelectedValue.Equals("") ? DBNull.Value : (object)ddletyp.SelectedValue,
                    ddldtyp.SelectedValue.Equals("") ? DBNull.Value : (object)ddldtyp.SelectedValue,
                    txtpval.Text.Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtpval.Text),
                    txtminval.Text.Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtminval.Text),
                    txtmxval.Text.Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtmxval.Text),
                    (check1.Checked ? "1" : "0"),
                    txtfncname.Text.Equals("") ? DBNull.Value : (object)(txtfncname.Text), 
                    (check2.Checked ? "1" : "0"),
                    txtinodr.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtinodr.Text),
                    txtcalodr.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtcalodr.Text),
                    txtprntodr.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtprntodr.Text),
                    Session[SiteConstants.SSN_INT_USER_ID]);

                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {

                //TextBox txtEDC = (TextBox)dv.FindControl("txtEDC");
                TextBox txtEDN = (TextBox)dv.FindControl("txtEDN");
                TextBox txtABBV = (TextBox)dv.FindControl("txtABBV");
                DropDownList ddletyp = (DropDownList)dv.FindControl("ddletyp");
                DropDownList ddldtyp = (DropDownList)dv.FindControl("ddldtyp");
                TextBox txtpval = (TextBox)dv.FindControl("txtpval");
                TextBox txtmxval = (TextBox)dv.FindControl("txtmxval");
                TextBox txtminval = (TextBox)dv.FindControl("txtminval");
                TextBox txtfncname = (TextBox)dv.FindControl("txtfncname");
                TextBox txtinodr = (TextBox)dv.FindControl("txtinodr");
                TextBox txtcalodr = (TextBox)dv.FindControl("txtcalodr");
                TextBox txtprntodr = (TextBox)dv.FindControl("txtprntodr");
                CheckBox check1 = (CheckBox)dv.FindControl("check1");
                CheckBox check2 = (CheckBox)dv.FindControl("check2");

                // DateTime dt1 = DateTime.ParseExact(txtDOR.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                string ID = hdnEDId;
                    //((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("UPDATE_Deduct", ID,
                    DBNull.Value,
                    txtEDN.Text.Equals("") ? DBNull.Value : (object)(txtEDN.Text),
                    txtABBV.Text.Equals("") ? DBNull.Value : (object)(txtABBV.Text),
                    ddletyp.SelectedValue.Equals("") ? DBNull.Value : (object)ddletyp.SelectedValue,
                    ddldtyp.SelectedValue.Equals("") ? DBNull.Value : (object)ddldtyp.SelectedValue,
                    txtpval.Text.Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtpval.Text),
                    txtminval.Text.Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtminval.Text),
                    txtmxval.Text.Equals("") ? DBNull.Value : (object)Convert.ToDouble(txtmxval.Text),
                    (check1.Checked ? "1" : "0"),
                    txtfncname.Text.Equals("") ? DBNull.Value : (object)(txtfncname.Text), 
                    (check2.Checked ? "1" : "0"),
                    txtinodr.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtinodr.Text),
                    txtcalodr.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtcalodr.Text),
                    txtprntodr.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtprntodr.Text),
                    Session[SiteConstants.SSN_INT_USER_ID]);

                    cmdSave.Text = "Create";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}