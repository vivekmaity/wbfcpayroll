﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;


public partial class EmpSalStatement : System.Web.UI.Page
{
    static string StrFormula="";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                GetSecByCent(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]));
                //DBHandler.Execute("Get_SalPayMonthHist", HttpContext.Current.Session[SiteConstants.SSN_SECTORID], HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);
                populateCenter();
                int CenterId =Convert.ToInt32(ddlCenter.SelectedValue);

                populateEmployee(CenterId);
            }
        }
        try
        {
           

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdReset_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    
    [WebMethod]
    public static string GetSecByCent(int SecID)
    {
        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataTable dt = ds.Tables[0];
        List<SectbyCent> listSectoCent = new List<SectbyCent>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SectbyCent objst = new SectbyCent();

                objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);

                listSectoCent.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSectoCent);

    }

    [WebMethod]
    public static string GetEmpnameByCent(int CenID)
    {
        DataSet ds = DBHandler.GetResults("Get_EmployeebyCenter", CenID);
        DataTable dt = ds.Tables[0];
        List<SectbyCent> listSectoCent = new List<SectbyCent>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SectbyCent objst = new SectbyCent();

                objst.CenterID = Convert.ToInt32(dt.Rows[i]["EmployeeID"]);
                objst.CenterName = Convert.ToString(dt.Rows[i]["EmpName"]);

                listSectoCent.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSectoCent);

    }
    public class SectbyCent
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(int StrSecID, int StrCenID, int StrEmpID, string ChkAllEmp, string StrFinYr, string FormName, string ReportName, string ReportType)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        String Msg = "";
        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable DTCheckReportVal1 = DBHandler.GetResult("Rpt_Create_DAT_MonthlySalary", StrFinYr, StrSecID, StrEmpID, UserID);

        DataTable DTCheckReportVal2 = DBHandler.GetResult("Rpt_Create_MST_MonthlySalary", StrFinYr, StrSecID, StrEmpID, UserID);
               

        if (ReportName == "Emp_salary_statement_details")
        {
            if (ChkAllEmp == "Y")
            {
                StrFormula = "{MST_Employee.Status}='Y' and {MST_Sector.SectorID}=" + StrSecID + " and {Rpt_DAT_MonthlySalary.InsertedBy}=" + UserID + "";

            }
            else
            {
                StrFormula = "{MST_Employee.Status}='Y' and {MST_Sector.SectorID}=" + StrSecID + " and {Rpt_DAT_MonthlySalary.InsertedBy}=" + UserID + " and {MST_Employee.EmployeeID}=" + StrEmpID + "";
            }
        }
        else{

            DataTable DTCheckReportVal3 = DBHandler.GetResult("getAllEarnDeducLoanType",UserID);
            DataTable DTCheckReportVal4 = DBHandler.GetResult("Generate_Emp_sal_details_statement_sid", StrSecID, StrFinYr, UserID, StrEmpID);
            if (ChkAllEmp == "Y")
            {
                StrFormula = "{RPT_Emp_DAT_Sal_statement.InsertedBy}=" + UserID + "";

            }
            else
            {
                StrFormula = "{RPT_Emp_DAT_Sal_statement.InsertedBy}=" + UserID + " and {MST_Employee.EmployeeID}=" + StrEmpID + "";
            }
        }


            
           

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

            //DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Update_RptParameterValue", UserID, FormName, ReportName, ReportType, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9, Parameter10);

            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(Msg);
        }


    public void populateCenter()
    {
        DataTable dsCenter = DBHandler.GetResult("Get_CenterbySector", HttpContext.Current.Session[SiteConstants.SSN_SECTORID]);
        if (dsCenter.Rows.Count > 0)
        {
            //for (int i = 0; i < dsmonth.Rows.Count; i++)
            //{
            //ClsMonth objst = new ClsMonth();
            //objst.SalMonthID = Convert.ToInt32(dsmonth.Rows[i]["SalMonthID"]);
            //objst.SalMonth = Convert.ToString(dsmonth.Rows[i]["SalMonth"]);

            //listmonth.Insert(i, objst);
            //}
            ddlCenter.DataTextField = "CenterName";
            ddlCenter.DataValueField = "CenterID";
            ddlCenter.DataSource = dsCenter;
            ddlCenter.DataBind();

        }
    }

    public void populateEmployee(int CenterId)
    {
        DataTable dsEmployee = DBHandler.GetResult("Get_EmployeebyCenter", CenterId);
        if (dsEmployee.Rows.Count > 0)
        {
            //for (int i = 0; i < dsmonth.Rows.Count; i++)
            //{
            //ClsMonth objst = new ClsMonth();
            //objst.SalMonthID = Convert.ToInt32(dsmonth.Rows[i]["SalMonthID"]);
            //objst.SalMonth = Convert.ToString(dsmonth.Rows[i]["SalMonth"]);

            //listmonth.Insert(i, objst);
            //}
            ddlEmployee.DataTextField = "EmpName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataSource = dsEmployee;
            ddlEmployee.DataBind();

        }
    }
    }


    

    





