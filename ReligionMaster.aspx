﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" ViewStateEncryptionMode="Always" AutoEventWireup="true" CodeFile="ReligionMaster.aspx.cs" Inherits="ClassificationMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            //$("#txtdc").rules("add", { required: true, messages: { required: "Please enter designation code"} })
            $("#txtd").rules("add", { required: true, messages: { required: "Please enter designation"} })
           
        }
        

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

//      function isNumberKey(evt)
//       {
//          var charCode = (evt.which) ? evt.which : evt.keyCode;
//          if (charCode != 46 && charCode > 31 
//            && (charCode < 48 || charCode > 57))
//             return false;

//          return true;
//       }
 
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Religion  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%"
                                                    AutoGenerateRows="False"
                                                    OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center"
                                                    GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <%--<td style="padding: 10px;"><span class="headFont">Religion Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtrc"  ClientIDMode="Static" autocomplete="off" MaxLength="3" runat="server" CssClass="textbox"></asp:TextBox>
                                                                </td>--%>
                                                                <td style="padding: 10px;"><span class="headFont">Religionn&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtr" ClientIDMode="Static" autocomplete="off" MaxLength="20" runat="server" CssClass="textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>



                                                        </table>

                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">


                                                            <tr>
                                                               <%-- <td style="padding: 10px;"><span class="headFont">Religion Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtrc" autocomplete="off" Text='<%# Eval("ReligionCode") %>' ClientIDMode="Static" MaxLength="3" runat="server" CssClass="textbox"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ReligionID") %>' />
                                                                </td>--%>
                                                                <td style="padding: 10px;"><span class="headFont">Religion &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtr" autocomplete="off" Text='<%# Eval("Religion") %>' ClientIDMode="Static" MaxLength="100" runat="server" CssClass="textbox"></asp:TextBox>
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 10px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                                <td style="padding: 10px;">&nbsp;</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"
                                                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();'
                                                                            OnClick="cmdSave_Click" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                                                            Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                                                    AutoGenerateColumns="false" DataKeyNames="ReligionID" OnRowCommand="tbl_RowCommand" AllowPaging="true"
                                                    PageSize="10" CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                                    <AlternatingRowStyle BackColor="#FFFACD" />
                                                    <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />

                                                    <Columns>

                                                        <asp:TemplateField HeaderText="Edit">
                                                            <HeaderStyle />
                                                            <ItemTemplate>

                                                                <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                    runat="server" ID="btnEdit" CommandArgument='<%# Eval("ReligionID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Delete">
                                                            <HeaderStyle />
                                                            <ItemTemplate>

                                                                <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                    OnClientClick='return Delete();'
                                                                    CommandArgument='<%# Eval("ReligionID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="ReligionID" Visible="false" HeaderText="ReligionID" />
                                                        <%--<asp:BoundField DataField="ReligionCode" HeaderText="Religion Code" ItemStyle-HorizontalAlign="Center" />--%>
                                                        <asp:BoundField DataField="Religion" HeaderText="Religion" />


                                                    </Columns>
                                                </asp:GridView>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>

        </div>
</div>  
</asp:Content>

