﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="FieldPermission.aspx.cs" Inherits="Field_Permission" %>

<%@ Register TagPrefix="art" TagName="EmpGPFandCPF" Src="GridviewTemplete.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
<link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {

        $(".DefaultButton").click(function (event) {
            event.preventDefault();
        });
        var professional = document.getElementById("<%=tabProfessional.ClientID %>");
        professional.style.display = "none";


        $("#ddlUser").change(function () {
            var UserID = $("#ddlUser").val();
            var E = "{UserID: '" + UserID + "'}";
            //alert(E);
                $.ajax({
                    type: 'POST',
                    url: 'FieldPermission.aspx/PopulateGridUserWise',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (D) {
                        $('#LockField').html(D.d);
                    }
                });
            });


        $('#worktype a').live('click', function () {
            flag = $(this).attr('id');
            FieldID = $(this).attr('itemid');
            var E = "{FieldID: '" + FieldID + "'}";

            $.ajax({
                type: 'POST',
                data: E,
                url: 'FieldPermission.aspx/DeleteLockPermission',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (D) {
                    var data = jQuery.parseJSON(D.d);
                    $.ajax({
                        type: 'POST',
                        url: 'FieldPermission.aspx/AjaxGetGridCtrl',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        success: function (D) {
                            $('#LockField').html(D.d);
                        }
                    });
                }
            });
        });

        $("#cmdLock").click(function (event) {
            var UserID = $("#ddlUser").val();
            var Tablename = $("#ddlTableName").val();
            var Fieldname = $("#ddlFieldname").val();
            var Fieldtype = $("#txtFieldName").val();

            var E = "{UserID: '" + UserID + "', Tablename: '" + Tablename + "',Fieldname: '" + Fieldname + "', Fieldtype: '" + Fieldtype + "'}";
            //alert(E);
            if (UserID != "") {
                //alert("eNTER");
                if (Tablename != "") {
                    if (Fieldname != "") {
                        if (Fieldtype != "") {

                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/LockEmployeePermissions',
                                data: E,
                                contentType: "application/json; charset=utf-8",
                                success: function (D) {
                                    var data = jQuery.parseJSON(D.d);
                                    // alert(data);
                                    if (data == "Success") {
                                        alert("Field wise Entry Permission is inserted Successfully.");
                                        $("#ddlUser").val('');
                                        $("#ddlTableName").val('');
                                        $("#ddlFieldname").val('');
                                        $("#txtFieldName").val('');
                                        return;
                                    }
                                    else {
                                        alert("Field wise Entry Permission is Not inserted.");
                                        return;
                                    }
                                },
                                error: function (response) {
                                    alert(response.d);
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        }
                        else {
                            $("#txtFieldName").focus();
                            return;
                        }
                    }
                    else {
                        $("#ddlFieldname").focus();
                        return;
                    }
                }
                else {
                    $("#ddlTableName").focus();
                    return;
                }
            }
            else {
                $("#ddlUser").focus();
                return;
            }
        });

        $(document).ready(function () {
            $("#ddlTableName").change(function () {
                var s = $('#ddlTableName').val();
                if (s != "") {
                    if ($("#ddlTableName").val() != "Please select") {
                        var E = "{TableName: '" + $('#ddlTableName').val() + "'}";
                        //alert(E);
                        var options = {};
                        options.url = "FieldPermission.aspx/BindFieldName";
                        options.type = "POST";
                        options.data = E;
                        options.dataType = "json";
                        options.contentType = "application/json";
                        options.success = function (listFiled) {
                            var t = jQuery.parseJSON(listFiled.d);
                            //alert(t);
                            var a = t.length;
                            $("#ddlFieldname").empty();
                            if (a > 0) {
                                $("#ddlFieldname").append("<option value=''>(Select Field/Column)</option>")
                                $.each(t, function (key, value) {
                                    $("#ddlFieldname").append($("<option></option>").val(value.COLUMN_NAMEid).html(value.COLUMN_NAME));
                                });
                            }
                            $("#ddlFieldname").prop("disabled", false);
                        };
                        options.error = function () { alert("Error retrieving Field Name!"); };
                        $.ajax(options);
                    }
                    else {
                        $("#ddlFieldname").empty();
                        $("#ddlFieldname").prop("disabled", true);
                    }
                }
                else {
                    $("#ddlFieldname").empty();
                    $("#ddlFieldname").append("<option value=''>(Select Field/Column)</option>")
                }
            });
        });

        $("#cmdSearch").click(function (event) {
            var EmpNo = $("#txtEmpNo").val();
            if (EmpNo != "") {
                var E = "{  EmpNo: " + EmpNo + "}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/GetEmployee',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var data = jQuery.parseJSON(D.d);
                        $("#txtEmpName").val(data);
                    },
                    error: function (response) {
                        alert(response.d);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
            else {
                $("#txtEmpNo").focus();
                return;
            }
        });

        $("#btnUpdate").click(function (event) {
            var EmpNo = $("#txtEmpNo").val();
            var EmpName = $("#txtEmpName").val();
            var Lockflag = $("#ddlLockFlag").val();

            if (EmpNo != "") {
                if (EmpName != "") {
                    if (Lockflag != "") {
                        var E = "{EmpNo: '" + EmpNo + "',Lockflag: '" + Lockflag + "'}";
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/UpdateLockFlag',
                            data: E,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                var data = jQuery.parseJSON(D.d);
                                if (data == 'Success') {
                                    alert("Employee LockFlag is Updated Successfully.");
                                    $("#txtEmpNo").val('');
                                    $("#txtEmpName").val('');
                                    $("#ddlLockFlag").val('');
                                }
                                else
                                    alert("There is Some Problem in Updating Employee LockFlag.");
                            },
                            error: function (response) {
                                alert(response.d);
                            },
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }
                    else {
                        $("#ddlLockFlag").focus();
                        return;
                    }
                }
                else {
                    alert("Please Enter EmpNo and then Search.");
                    $("#txtEmpNo").focus();
                    return;
                }
            }
            else {
                $("#txtEmpNo").focus();
                return;
            }
        });

        $("#txtEmpNo").keydown(function (event) {
            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            // (event.keyCode == 65 && event.ctrlKey == true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                    $("#errmsg").html("Digits Only").show().fadeOut(1500);
                    return false;

                }
            }
        });
    });

</script>
<script type="text/javascript">
        function callfunction() {
            var personal = document.getElementById("<%=tabPersonal.ClientID %>");
            var professional = document.getElementById("<%=tabProfessional.ClientID %>");

            if (document.getElementById('rdEmployee').checked) {

                professional.style.display = 'none';
                personal.style.display = 'block';
            }
            else if (document.getElementById('rdField').checked) {
                personal.style.display = "none";
                professional.style.display = "block";
            }
            else {
                personal.style.display = "none";
                professional.style.display = "none";
            }
        }

</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
<div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>User_Field_Permission</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" align="center" style="border: solid 2px lightblue;">
                         <tr>
                                <td>
                                    <table width="100%" style="margin-bottom: 10px;">
                                        <tr>
                                            <td style="padding: 5px;">
                                                <asp:RadioButton ID="rdEmployee" Checked="true" class="headFont" runat="server" onchange='callfunction();'
                                                    Font-Size="Medium" GroupName="a" Text="Employee Wise" />
                                                <asp:RadioButton ID="rdField" class="headFont" runat="server" onchange='callfunction();'
                                                    Font-Size="Medium" GroupName="a" Text="Field Wise" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6">
                                                <div align="center">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                         <tr>
                                <td style="padding: 2px; ">
                                    <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <div id="tabPersonal" runat="server" >
                                                <div id="tab" runat="server" clientidmode="Static">
                                                    <table align="center" width="100%">
                                                        <tr>
                                                            <td colspan="2" style="padding: 3.5px; background: #deedf7;" class="style1">
                                                                <span class="headFont" style="font-weight: bold;"><font size="3px">Update Employee Master (EmpNo Wise)</font></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;" align="left" class="style1">
                                                                <span class="headFont">Employee No. :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:TextBox ID="txtEmpNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                    MaxLength="6" Width="50"></asp:TextBox>
                                                                    <asp:Button ID="cmdSearch" runat="server" Text="Search" Width="70px" CssClass="Btnclassname DefaultButton"
                                                                           OnClientClick='javascript: return unvalidate();' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;"class="style1">
                                                                <span class="headFont">Employee Name :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:TextBox ID="txtEmpName" autocomplete="off" Width="1000" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                    Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;" class="style1">
                                                                <span class="headFont">Lock Flag :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:DropDownList ID="ddlLockFlag" autocomplete="off" Width="220px" runat="server" Height="24px" AppendDataBoundItems="true"
                                                                    ClientIDMode="Static">
                                                                    <asp:ListItem Text="(Select Flag)" Selected="True" Value=""></asp:ListItem>
                                                                    <asp:ListItem Text="Lock" Value="Y"></asp:ListItem>
                                                                    <asp:ListItem Text="UnLock" Value="N"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="70px" CssClass="Btnclassname DefaultButton" />
                                                            </td>
                                                        </tr>
                                                  </table>
                                                    </div>
                                                </div>
                                                <div id="tabProfessional" runat="server" style="float:left; width:100%;" >
                                                <div id="tabs" runat="server" clientidmode="Static" >
                                                    <table align="center" width="100%">
                                                        <tr>
                                                            <td colspan="2" style="padding: 3.5px; background: #deedf7;" class="style1">
                                                                <span class="headFont" style="font-weight: bold;"><font size="3px">Field Entry</font></span>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td style="padding: 2px;" class="style1">
                                                                <span class="headFont">User Name :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:DropDownList ID="ddlUser" autocomplete="off" Width="220px" runat="server" Height="24px" AppendDataBoundItems="true"
                                                                    ClientIDMode="Static" DataTextField="UserName" DataValueField="UserID">
                                                                    <asp:ListItem Text="(Select User)" Selected="True" Value=""></asp:ListItem>
                                                                   
                                                                </asp:DropDownList>
                                                               </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;" align="left" class="style1">
                                                                <span class="headFont">Table Name :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:DropDownList ID="ddlTableName" autocomplete="off" Width="220px" runat="server" Height="24px" AppendDataBoundItems="true"
                                                                    ClientIDMode="Static"  DataTextField="TABLE_NAME" DataValueField="TABLE_NAME">
                                                                    <asp:ListItem Text="(Select Table)" Selected="True" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;" class="style1">
                                                                <span class="headFont">Field Name :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:DropDownList ID="ddlFieldname" autocomplete="off" Width="220px" runat="server" Height="24px" AppendDataBoundItems="true"
                                                                    ClientIDMode="Static">
                                                                    <asp:ListItem Text="(Select Field/Column)" Selected="True" Value=""></asp:ListItem>
                                                                   
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                       <tr>
                                                            <td style="padding: 2px;" class="style1">
                                                                <span class="headFont">Field Name :&nbsp;&nbsp;</span><span class="require">*</span>
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:TextBox ID="txtFieldName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"
                                                                    ></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 2px;" class="style1">
                                                               
                                                            </td>
                                                            <td style="padding: 2px;" align="left">
                                                                <asp:Button ID="cmdLock" runat="server" autocomplete="off" Text="Lock" Width="70px" CssClass="Btnclassname DefaultButton" />
                                                            </td>
                                                        </tr>
                                                    
                                                    <table align="center" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div id="LockField" runat="server" clientidmode="Static">
                                                              
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                            
                                                    </table>
                                                    
                                                    </div>
                                                    
                                                </div>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

