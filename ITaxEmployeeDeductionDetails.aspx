﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" EnableEventValidation="false" CodeFile="ITaxEmployeeDeductionDetails.aspx.cs" Inherits="ITaxEmployeeDeductionDetails" %>

<%@ Register TagPrefix="art" TagName="ItaxGridView_Page" Src="ItaxGridView_Page.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" runat="Server">

    <script src="js/Itax.js?v2"></script>
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .grdCell {
            color: white;
        }

        .Grid1 {
            background-color: #fff;
            margin: 5px 0 10px 0;
            border: solid 1px #525252;
            border-collapse: collapse;
            font-family: Calibri;
            color: #474747;
            width: 100%;
        }

            .Grid1 td {
                padding: 2px;
                border: solid 1px #c1c1c1;
                font-family: Verdana;
            }

            .grid1 tr:nth-child(even) {
                background-color: #ffffff;
            }

            .grid1 tr:nth-child(odd) {
                background-color: #cccccc;
            }

            .Grid1 th {
                padding: 3px 2px;
                color: white;
                font-weight: bold;
                background-color: #6ad3f5;
                border-left: solid 1px #525252;
                font-size: 14px;
                height: 17px;
                font-family: Verdana;
            }


        .GridRowEven {
            background-color: #ffffcc;
        }

        .GridRowOdd {
            background-color: #FEDCBA;
        }
    </style>




</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" runat="Server">
    <link href="css/style.css" rel="stylesheet" />
    <div class="loading-overlay">
        <div class="loadwrapper">
            <div class="ajax-loader-outer">Loading...</div>
        </div>
    </div>

    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>iTax_Deduction_Master</h2>
                    </section>
                    <asp:HiddenField ID="hdnEmpID" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="HiddenCheckGrid" runat="server" ClientIDMode="Static" Value="0" />
                    <asp:HiddenField ID="hdnEmpGender" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalGrossPay" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalPtax" runat="server" ClientIDMode="Static" Value="" />

                    <asp:HiddenField ID="hdnTotalITax" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalGIS" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalGPF" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalHRA" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalGPFRec" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalIHBL" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalHBL" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalOtherDe" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalNetPay" runat="server" ClientIDMode="Static" Value="" />
                    <asp:HiddenField ID="hdnTotalArrearOT" runat="server" ClientIDMode="Static" Value="" />


                    <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" Value="" />


                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <%--//--%>
                                                                <td style="padding: 5px;"><span class="headFont">Salary Financial Year &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtSalaryFinYear" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Width="90px" Enabled="true"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>
                                                                <%--//--%>
                                                                <td style="padding: 5px;"><span class="headFont">Assessment Year &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtAssessmentYear" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Width="90px" Enabled="false"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Employee No &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtEmpNo"  ClientIDMode="Static" MaxLength="5" Style="text-transform: uppercase" AutoComplete="off" runat="server" CssClass="textbox" title="" Width="90px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    

                                                                    <asp:Button ID="cmdShow" runat="server" Text="Show" OnClick="cmdSearch_Click" CommandName="Show"
                                                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return unvalidate();' />
                                                                    <td style="padding: 5px;">Gender</td>
                                                                    <td style="padding: 5px;">:</td>
                                                                    <td style="padding: 5px;" align="left">
                                                                        <asp:TextBox ID="txtGender" autocomplete="off" ClientIDMode="Static" Enabled="false" MaxLength="6" runat="server" CssClass="textbox" Width="90px" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                    </td>
                                                                    <td></td>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 10px;"><span class="headFont">Category Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtCategoryCode" autocomplete="off" Text='<%# Eval("CategoryCode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" Enabled="false"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("CategoryID") %>' />
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Category Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtCategoryName" autocomplete="off" Text='<%# Eval("Category") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </EditItemTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <asp:GridView ID="grvItax" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                                    DataKeyNames="EmployeeID" AllowPaging="false" PageSize="10" ShowFooter="true"
                                                    CssClass="Grid FirstGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
                                                    <AlternatingRowStyle BackColor="#FFFACD" />
                                                    <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                    <Columns>
                                                        <asp:BoundField DataField="SalMonth" Visible="true" HeaderText="SalMonth" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="GrossPay" Visible="true" HeaderText="GrossPay" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Ptax" HeaderText="PTax" ItemStyle-HorizontalAlign="Right" />

                                                        <asp:BoundField DataField="Itax" HeaderText="ITax" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="GIS" HeaderText="GIS" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="HRA" HeaderText="HRA" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="GPF" HeaderText="GPF" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="GPFRecov" HeaderText="GPF Recovery" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="HBL" HeaderText="HBL" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="IHBL" HeaderText="IHBL" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="OtherDedu" HeaderText="Other Deduction" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="NetPay" HeaderText="NetPay" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="ArrearOT" HeaderText="Arrear / OT" ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                </asp:GridView>

                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center">

                                    <table id="tbldetails" style="border: 1px solid lightblue; width: 90%">
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">1 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Income From Salary&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px; width: 230px">
                                                <%-- <asp:Button ID="btnDetails1" runat="server" Text="Show Details" CommandName="Show"
                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return unvalidate();' />--%>
                                            </td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtIncome" ClientIDMode="Static" autocomplete="off" runat="server" Style="text-align: right" Enabled="false" CssClass="textbox cal2" MaxLength="10"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">2 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">P.Tax&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtTotalPTax" ClientIDMode="Static" autocomplete="off" runat="server" Style="text-align: right" Enabled="false" CssClass="textbox cal2" MaxLength="10"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">3 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">HRA&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtTotHRA" ClientIDMode="Static" autocomplete="off" runat="server" Style="text-align: right" Enabled="false" CssClass="textbox cal2" MaxLength="10"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">4 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Interest on House Building Loan&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtTotIHBL" ClientIDMode="Static" autocomplete="off" runat="server" Style="text-align: right" Enabled="false" CssClass="textbox cal2" MaxLength="10"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">5 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Add : Income From Other Source&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;">
                                                <asp:Button ID="btnDetails1" runat="server" Text="Show Details"
                                                    Width="100px" CssClass="Btnclassname DefaultButton" OnClientClick='javascript: return unvalidate();' />
                                            </td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtDetails1" ClientIDMode="Static" autocomplete="off" runat="server" Text="0" Style="text-align: right" CssClass="textbox cal2" MaxLength="3" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray; display: none" class="show1">
                                            <td colspan="4" align="right">
                                                <table width="55%" style="border: 1px solid lightblue" id="tblSourceFromOtherIncome">
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="show1"  style="border-bottom: 1px solid gray; display: none; background-color: lightblue">
                                            <td colspan="4" style="text-align:right" >
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 4px;" align="right"><span class="headFont">Add New Other Income Source&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                            <td style="padding: 4px;">
                                                                <asp:TextBox ID="txtAddNewOtherNewSourceIncome" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                                            </td>
                                                            <td style="padding: 4px; width: 100px" align="center">
                                                                <asp:Button ID="btnAddNewSrcIncome" runat="server" autopoastback="false" CssClass="Btnclassname DefaultButton" Width="80px" Text="Add"  />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">6 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Deduction Under Chapter VI-A&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;">
                                                <asp:Button ID="btnDetails2" runat="server" Text="Show Details"
                                                    Width="100px" CssClass="Btnclassname DefaultButton" OnClientClick='javascript: return unvalidate();' />
                                            </td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtTotalDeduction" ClientIDMode="Static" runat="server" Style="text-align: right" Text="0" Enabled="false" CssClass="textbox" MaxLength="3" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray; display: none" class="show2">
                                            <td colspan="4" align="right">
                                                <div id="tblBeneficary_CHDetail" clientidmode="Static" runat="server" style="height: auto; width: 100%;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="show2" style="border-bottom: 1px solid gray; display: none; background-color: lightblue">
                                            <td colspan="4">
                                                <table style="width: 100%;" align="center;">
                                                    <tr align="right">
                                                        <td style="padding: 4px;width:60px" align="right"><span class="headFont">Add New&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                        <td style="padding: 4px; width:130px">
                                                            <asp:TextBox ID="txtNewSection" Style="text-transform: uppercase;width:120px" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="padding: 4px;width:50px" align="right;"><span class="headFont">Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                        <td style="padding: 4px; margin-right: 5px;width:160px">
                                                            <asp:RadioButton ID="rbValue" runat="server" onchange="myFunction()" Checked="true" Text="Value" Style="color: navy" GroupName="V" />
                                                            <asp:RadioButton ID="rbPercent" runat="server" onchange="myFunction()" Text="Percentage" Style="color: navy; margin-right: 5px" GroupName="V" />
                                                        </td>
                                                        <td style="padding: 4px;width:90px" align="right"><span class="headFont Max">Max Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                        <td style="padding: 4px;width:110px">
                                                            <asp:TextBox ID="txtMaxAmount" ClientIDMode="Static" runat="server" Style="text-align: right;width:100px" CssClass="textbox allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="padding: 4px;width:90px" align="right"><span class="headFont PMax" style="display:none">Max Amount&nbsp;&nbsp;</span><span class="require mndtry" style="display:none">*</span> </td>
                                                        <td style="padding: 4px;width:110px" >
                                                            <asp:TextBox ID="txtPMaxAmount" ClientIDMode="Static" runat="server" Style="text-align: right;display:none;width:100px" CssClass="textbox allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="padding: 4px; width: 100px" align="center">
                                                            <asp:Button ID="btnSectionAdd" runat="server" autopoastback="false" CssClass="Btnclassname DefaultButton" Width="80px" Text="Add" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>

                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">7 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Taxable Income&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtTaxableIncome" ClientIDMode="Static" runat="server" Enabled="false" Style="text-align: right" CssClass="textbox cal2" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">8 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Tax On Net Income&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtOnNetIncome" ClientIDMode="Static" runat="server" Style="text-align: right" Enabled="false" CssClass="textbox cal2 allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">9 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Rebate [Under Section 87A]&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtRabate" ClientIDMode="Static" runat="server" onkeyup="CalEduCess($(this).val())" Style="text-align: right" Enabled="false" CssClass="textbox cal2 allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">10 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Surcharge &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtSurcharge" ClientIDMode="Static" runat="server" onkeyup="TotTaxLi()" Enabled="true" Style="text-align: right" CssClass="textbox cal2 allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">11 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Education Cess &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtEduCess" ClientIDMode="Static" runat="server" Enabled="false" Style="text-align: right" CssClass="textbox cal2 allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">12 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Secondary and Higher Secondary Educational Cess &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtHDCess" ClientIDMode="Static" runat="server" Enabled="false" Style="text-align: right" CssClass="textbox cal2" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">13 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Total Tax Liabilities &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtTaxLi" ClientIDMode="Static" runat="server" onkeyup="CalNetTaxPay()" Enabled="false" Style="text-align: right" CssClass="textbox cal2 allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">14 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Relief Under Section 89  &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtUS89" ClientIDMode="Static" runat="server" onkeyup="CalTaxPay()" Enabled="true" Style="text-align: right" CssClass="textbox cal2 allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">15 .</td>
                                            <td style="padding: 4px; width: 65%;"><span class="headFont">Tax Payable&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtNetTaxPay" ClientIDMode="Static" onkeyup="CalTaxPay()" runat="server" Style="text-align: right" CssClass="textbox allownumericwithoutdecimal" MaxLength="10" Enabled="false" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">16 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">Tax Deduction From Salary&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtTDSAmt" ClientIDMode="Static" onkeyup="CalNetTaxPay()" runat="server" Style="text-align: right" Enabled="false" CssClass="textbox cal2 allownumericwithoutdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">17 .</td>
                                            <td style="padding: 4px; width: 65%"><span class="headFont">ITax Paid From Other Source&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtOtherSource" ClientIDMode="Static" runat="server" Style="text-align: right" Enabled="false" CssClass="textbox cal2" MaxLength="10" autocomplete="off" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <table>
                                                    <tr>

                                                        <td style="padding: 4px;"><span class="headFont">Challan No.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                        <td style="padding: 4px;"></td>
                                                        <td style="padding: 4px;" align="center">
                                                            <asp:TextBox ID="txtChallanNo" Width="110" ClientIDMode="Static" runat="server" Style="text-align: left; text-transform: uppercase" Enabled="true" CssClass="textbox txtpress" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="padding: 4px;"><span class="headFont">BSR No.&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                        <td style="padding: 4px;"></td>
                                                        <td style="padding: 4px;" align="center">
                                                            <asp:TextBox ID="txtBSRNo" Width="110" ClientIDMode="Static" runat="server" Style="text-align: left; text-transform: uppercase" Enabled="true" CssClass="textbox txtpress" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="padding: 4px;"><span class="headFont">Challan Date&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                        <td style="padding: 4px;"></td>
                                                        <td style="padding: 4px;" align="center">
                                                            <asp:TextBox ID="txtDate" Width="110" ClientIDMode="Static" runat="server" Style="text-align: left; text-transform: uppercase" Enabled="true" CssClass="textbox txtpress" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="padding: 4px;"><span class="headFont">Amount&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                        <td style="padding: 4px;"></td>
                                                        <td style="padding: 4px;" align="center">
                                                            <asp:TextBox ID="txtAmount" Width="115" ClientIDMode="Static" runat="server" Style="text-align: right; text-transform: uppercase" Enabled="true" CssClass="textbox allownumericwithoutdecimal txtpress" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </td>
                                                        <td style="padding: 4px;"><span class="headFont">&nbsp;&nbsp;</span><span class="require"></span> </td>
                                                        <td style="padding: 4px;"></td>
                                                        <td style="padding: 4px;" align="center">
                                                            <asp:Button ID="btnOtherSource" runat="server" ClientIDMode="Static" CssClass="Btnclassname" Width="80px" Text="Add" />
                                                            <%--<asp:Button ID="btnOtherRefresh" runat="server" ClientIDMode="Static" Visible="true" CssClass="Btnclassname" Width="80px" Text="Refresh" />--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr style="border-bottom: 1px solid gray">
                                            <td colspan="4">
                                                <asp:GridView ID="GridViewOther" runat="server" Width="75%" align="center" GridLines="Both" Style="margin-left: 0.5%; text-align: center; text-transform: uppercase" AutoGenerateColumns="false"
                                                    AllowPaging="false" PageSize="10" ShowFooter="true"
                                                    CssClass="Grid1" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
                                                    <AlternatingRowStyle BackColor="#FFFACD" />
                                                    <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                    <Columns>
                                                        <asp:BoundField DataField="ChallanNo" Visible="true" HeaderText="Challan No." ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="BSRNo" Visible="true" HeaderText="BSR No." ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="ChallanDate" HeaderText="Date" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="TaxAmount" HeaderText="Amount" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="Delete" HeaderText="Delete" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid gray">
                                            <td style="padding: 4px; background-color: #6ad3f5">18 .</td>
                                            <td style="padding: 4px; width: 65%; font-size: 2em; font-weight: bolder"><span style="font-size: .6em; letter-spacing: 1px; font-weight: 500" class="headFont balance">Balance Tax&nbsp;&nbsp;</span><span class="require"></span> </td>
                                            <td style="padding: 4px;"></td>
                                            <td style="border-left: 1px solid gray; padding: 4px;" align="center">
                                                <asp:TextBox ID="txtBalTax" ClientIDMode="Static" runat="server" Style="text-align: right" CssClass="textbox cal2 " MaxLength="3" Enabled="false" autocomplete="off"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right">
                                                <br />
                                            </td>
                                            <tr style="border-bottom: 1px solid gray">
                                                <td colspan="4" align="right">
                                                    <asp:Button ID="btnSave" runat="server" ClientIDMode="Static" CssClass="Btnclassname" Height="26" Width="100px" Text="Save" />
                                                    <asp:Button ID="btnRefresh" runat="server" Style="margin-left: 10px" ClientIDMode="Static" CssClass="Btnclassname" Height="26" Width="100px" Text="Refresh" />
                                                    <br />
                                                    &nbsp;
                                                </td>
                                            </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

