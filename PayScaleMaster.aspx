﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="PayScaleMaster.aspx.cs" Inherits="PayScaleMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        function beforeSave() {
            $("#form1").validate();
            //$("#txtPayScaleCode").rules("add", { required: true, messages: { required: "Please enter Loan Type Code"} });
            $("#ddlEmpType").rules("add", { required: true, messages: { required: "Please select Loan Type"} });
            $("#txtPayScale").rules("add", { required: true, messages: { required: "Please enter Loan Abbreviation"} });
            $("#txtGradePay").rules("add", { required: true, messages: { required: "Please enter Loan Description"} });
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
       <style type="text/css">           
             
              .cssPager td
            {
                  padding-left: 5px;     
                  padding-right: 5px;    
              }
        </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>PayScale Master</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <%--<td style="padding:5px;" ><span class="headFont">PayScale Code &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtPayScaleCode" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3"></asp:TextBox>                            
                                        </td>--%>
                                        <td style="padding:5px;"><span class="headFont">Employee Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmpType" Width="200px" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpType_SelectedIndexChanged" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                <asp:ListItem Text="WBFC Employee" Value="K"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">PayScale &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtPayScale" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="25"></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Grade Pay&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtGradePay" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>

                            <EditItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                        <%--<td style="padding:10px;"> <span class="headFont">PayScale Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtPayScaleCode" Text='<%# Eval("PayScaleCode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" autocomplete="off" Enabled="true"></asp:TextBox>                            
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("PayScaleID") %>'/>
                                        </td>--%>
                                        <td style="padding:5px;"><span class="headFont">Employee Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmpType" Width="200px" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEmpType_SelectedIndexChanged" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="State Employee" Value="S"></asp:ListItem>
                                                <asp:ListItem Text="Central Employee" Value="C"></asp:ListItem>
                                                <asp:ListItem Text="CDMA Employee" Value="K"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">PayScale&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtPayScale" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="25" Text='<%# Eval("PayScale") %>'></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Grade Pay&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtGradePay" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("GradePay") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:auto;width:100%">
                            <asp:GridView ID="tbl" runat="server" Width="100%"  align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="PayScaleID" OnRowCommand="tbl_RowCommand" AllowPaging="true" OnPageIndexChanging="tbl_PageIndexChanging" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <PagerSettings FirstPageText="First" LastPageText="Last"
                                  Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                  <PagerStyle CssClass="cssPager" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("PayScaleID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                OnClientClick='return Delete();' CommandArgument='<%# Eval("PayScaleID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PayScaleID" Visible="false" ItemStyle-CssClass="labelCaption"
                                        HeaderStyle-CssClass="TableHeader" HeaderText="PayScaleID">
                                        <HeaderStyle CssClass="TableHeader"></HeaderStyle>
                                        <ItemStyle CssClass="labelCaption"></ItemStyle>
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="PayScaleCode" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader"
                                        HeaderText="PayScale Code">
                                        <HeaderStyle CssClass="TableHeader"></HeaderStyle>
                                        <ItemStyle CssClass="labelCaption"></ItemStyle>
                                    </asp:BoundField>--%>
                                    <asp:BoundField DataField="EmpType" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader"
                                        HeaderText="Employee Type">
                                        <HeaderStyle CssClass="TableHeader"></HeaderStyle>
                                        <ItemStyle CssClass="labelCaption"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PayScale" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader"
                                        HeaderText="Pay Scale">
                                        <HeaderStyle CssClass="TableHeader"></HeaderStyle>
                                        <ItemStyle CssClass="labelCaption"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GradePay" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader"
                                        HeaderText="Grade Pay">
                                        <HeaderStyle CssClass="TableHeader"></HeaderStyle>
                                        <ItemStyle CssClass="labelCaption"></ItemStyle>
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

