﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="undeduc.aspx.cs" Inherits="undeduc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">





    
    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            //$("#txtEDC").rules("add", { required: true, messages: { required: "Please enter code"} })
            $("#txtEDN").rules("add", { required: true, messages: { required: "Please enter name"} })
            $("#txtABBV").rules("add", { required: true, messages: { required: "Please enter avreviation"} });
           

        }
        
       

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

      function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
 
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						
						<section>
							<h2>Earn Deduct Master</h2>						
						</section>

                    <table width="98%"   style="border:solid 2px lightblue;  " align="right" >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv"  runat="server"  Width="99%" 
                            AutoGenerateRows="False" 
                            OnModeChanging="dv_ModeChanging"                            
                            DefaultMode="Insert" HorizontalAlign="Center" 
                            GridLines="None"  >
                            <InsertItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                    <%--<td style="padding:10px;" ><span class="headFont">ED Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtEDC"  autocomplete="off" ClientIDMode="Static" MaxLength="3" runat="server" CssClass="textbox"></asp:TextBox>                            
                                    </td>    --%>                        
                             
                                    <td style="padding:10px;" ><span class="headFont">ED Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtEDN" autocomplete="off"  ClientIDMode="Static" MaxLength="50"  runat="server" CssClass="textbox"></asp:TextBox>                            
                                    </td>                            
                              </tr>
                               <tr>
                                    <td style="padding:10px;" ><span class="headFont">ED Abbreviation&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtABBV"  ClientIDMode="Static"  MaxLength="4" runat="server" CssClass="textbox"></asp:TextBox>                            
                                    </td>                            
                             
                                  <td style="padding:10px;" ><span class="headFont">Earn Type&nbsp;&nbsp;<span class="require">*</span> </td>
                                  <td style="padding:10px;" >:</td>
                                  <td style="padding:10px;" align="left" ><asp:DropDownList id="ddletyp"   
                                   runat="server" AppendDataBoundItems="true" Width="223px" Height="26px"  ClientIDMode="Static" CssClass="textbox"  >
                                   <asp:ListItem Text="Select from below" Value=""></asp:ListItem>
                                  <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                            
                                  <asp:ListItem Text="Value" Value="V"></asp:ListItem>
                                 </asp:DropDownList>                    
                               </td>                            
                           </tr>  
                           
                           <tr>
                                  <td style="padding:10px;" ><span class="headFont">Deduction Type&nbsp;&nbsp;<span class="require">*</span> </td>
                                  <td style="padding:10px;" >:</td>
                                  <td style="padding:10px;" align="left" ><asp:DropDownList id="ddldtyp"   
                                   runat="server" AppendDataBoundItems="true" Width="223px" Height="26px" ClientIDMode="Static" CssClass="textbox" autocomplete="off"  >
                                  <asp:ListItem Text="Select from below" Value=""></asp:ListItem>
                                  <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                            
                                  <asp:ListItem Text="Value" Value="V"></asp:ListItem>
                                 </asp:DropDownList>                            
                               </td>                            
                          
                                    <td style="padding:10px;" ><span class="headFont">Pay Value&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtpval"  ClientIDMode="Static"  MaxLength="18" onkeypress="return isNumberKey(event)"  runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>                            
                                    </td>                            
                           </tr>
                            <tr>
                                    <td style="padding:10px;" ><span class="headFont">Max Value&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtmxval"  ClientIDMode="Static"  MaxLength="18" onkeypress="return isNumberKey(event)"  runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>                            
                                    </td>                            
                            
                                    <td style="padding:10px;" ><span class="headFont">Minimum Value&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtminval"  ClientIDMode="Static" MaxLength="18" onkeypress="return isNumberKey(event)"   runat="server" CssClass="textbox"></asp:TextBox>                            
                                    </td>                            
                            </tr>
                            <tr>
                            <td style="padding:10px;" ><span class="headFont">Active&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" >
                                     <asp:CheckBox id="check1" ClientIDMode="Static"  runat="server" Checked="true"   />  
                                             </td>                      
                             
                                    <td style="padding:10px;" ><span class="headFont">Funcation Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtfncname"  ClientIDMode="Static"  runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>                            
                                    </td>                            
                            </tr>
                            <tr>
                                  <td style="padding:10px;" ><span class="headFont">Input Field&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" >
                                     <asp:CheckBox id="Check2" ClientIDMode="Static"  runat="server" Checked="true"   />  
                                             </td>             
                                                             
                             

                                    <td style="padding:10px;" ><span class="headFont">input Order&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtinodr"  ClientIDMode="Static" onkeypress="return isNumberKey(event)"   runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>                            
                                    </td>        
                                                           
                              </tr>
                              <tr>
                                    <td style="padding:10px;" ><span class="headFont">Calculate Order&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtcalodr"  ClientIDMode="Static" onkeypress="return isNumberKey(event)"  runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>                            
                                    </td>                            
                             
                                    <td style="padding:10px;" ><span class="headFont">Print Order&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtprntodr"  ClientIDMode="Static" onkeypress="return isNumberKey(event)"  runat="server" CssClass="textbox" autocomplete="off" ></asp:TextBox>                            
                                    </td>                            
                              </tr>

                                    
                                                                             
                                </table>

                            </InsertItemTemplate>
                           
                            <EditItemTemplate>
                                <table align="center"  width="100%">
                            

                              <%-- <tr>
                                    <td style="padding:10px;"  > <span class="headFont">ED Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtEDC" Text='<%# Eval("EDCode") %>' ClientIDMode="Static" Enabled="false" autocomplete="off"  runat="server" CssClass="textbox"></asp:TextBox>                            
                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("EDID") %>'/>
                                    </td>                            
                                    </tr>--%>

                                     <tr>
                                    <td style="padding:10px;"  > <span class="headFont"> ED Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtEDN" Text='<%# Eval("EDName") %>' ClientIDMode="Static"  MaxLength="50" autocomplete="off"  runat="server" CssClass="textbox"></asp:TextBox>   
                                    </td>


                                 </tr>
                                 <tr>

                                    <td style="padding:10px;"  > <span class="headFont"> ED Abbreviation &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtABBV" Text='<%# Eval("EDAbbv") %>' ClientIDMode="Static"  MaxLength="4" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>   
                                    </td>
                                      
                                  <td style="padding:10px;" ><span class="headFont">Earn Type&nbsp;&nbsp;<span class="require">*</span> </td>
                                  <td style="padding:10px;" >:</td>
                                  <td style="padding:10px;" align="left" ><asp:DropDownList id="ddletyp"   
                                   runat="server" AppendDataBoundItems="true" Width="223px" Height="26px" ClientIDMode="Static" CssClass="textbox"  autocomplete="off" >
                                 <asp:ListItem Text="Select from below" Value=""></asp:ListItem>
                                  <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                            
                                  <asp:ListItem Text="Value" Value="V"></asp:ListItem>
                                 </asp:DropDownList>    
                               </td>                            
                           </tr>  
                           
                           <tr>
                                  <td style="padding:10px;" ><span class="headFont">Deduction Type&nbsp;&nbsp;<span class="require">*</span> </td>
                                  <td style="padding:10px;" >:</td>
                                  <td style="padding:10px;" align="left" ><asp:DropDownList id="ddldtyp"   
                                   runat="server" AppendDataBoundItems="true" Width="223px" Height="26px" ClientIDMode="Static" CssClass="textbox" autocomplete="off" >
                                  <asp:ListItem Text="Select from below" Value=""></asp:ListItem>
                                  <asp:ListItem Text="percentage" Value="P"></asp:ListItem>                            
                                  <asp:ListItem Text="value" Value="V"></asp:ListItem>
                                 </asp:DropDownList>                            
                               </td>                            
                          
                                    <td style="padding:10px;"  > <span class="headFont"> Pay Value &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtpval" Text='<%# Eval("PayVal") %>' ClientIDMode="Static"  MaxLength="18" autocomplete="off"  runat="server" CssClass="textbox" onkeypress="return isNumberKey(event)"></asp:TextBox>   
                                    </td>

                         
                            </tr>
                             <tr>
                                    <td style="padding:10px;"  > <span class="headFont"> Max Value &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtmxval" Text='<%# Eval("MaxLimit") %>'  MaxLength="18" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" onkeypress="return isNumberKey(event)"></asp:TextBox>   
                                    </td>

                           
                                    <td style="padding:10px;"  > <span class="headFont"> Minimum Value &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtminval" Text='<%# Eval("MinLimit") %>'  MaxLength="18" ClientIDMode="Static" autocomplete="off"  runat="server" CssClass="textbox" onkeypress="return isNumberKey(event)"></asp:TextBox>   
                                    </td>

                             </tr>
                             
                              <tr>
                              <td style="padding:10px;" ><span class="headFont"> Active &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" >
                                     <asp:CheckBox id="Check1" ClientIDMode="Static"  runat="server" Checked='<%# Eval("Active").ToString().Equals("1") %>'/>  
                                       
                                     </td>  
                                   
                            
                                    <td style="padding:10px;"  > <span class="headFont"> Funcation Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtfncname" Text='<%# Eval("FuncName") %>'  MaxLength="100" ClientIDMode="Static" autocomplete="off"  runat="server" CssClass="textbox"></asp:TextBox>   
                                    </td>

                             </tr>
                              <tr>
                               <td style="padding:10px;" ><span class="headFont">Input Field&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" >
                                     <asp:CheckBox id="Check2" ClientIDMode="Static"  runat="server" Checked='<%# Eval("InputField").ToString().Equals("1") %>'  />  
                                             </td>     
                                  
                            
                                    <td style="padding:10px;"  > <span class="headFont"> Input Order &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtinodr" autocomplete="off" Text='<%# Eval("InputOrder") %>' ClientIDMode="Static"  runat="server" CssClass="textbox"></asp:TextBox>   
                                    </td>

                             </tr>
                              <tr>
                                    <td style="padding:10px;"  > <span class="headFont"> Calculate Order &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtcalodr" autocomplete="off" Text='<%# Eval("CalcOrder") %>' ClientIDMode="Static"  runat="server" CssClass="textbox"></asp:TextBox>   
                                    </td>

                            
                                    <td style="padding:10px;"  > <span class="headFont">Print Order &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                    <td style="padding:10px;" >:</td>
                                    <td style="padding:10px;" align="left" ><asp:TextBox ID="txtprntodr" autocomplete="off" Text='<%# Eval("PrintOrder") %>' ClientIDMode="Static"  runat="server" CssClass="textbox"></asp:TextBox>   
                                    </td>

                             </tr>

                                                                                 
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:10px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:10px;">&nbsp;</td>
                                    <td style="padding:10px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                    
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:300px ;width:100%">
                    
                            <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" 
                            AutoGenerateColumns="false" DataKeyNames="EDID" OnRowCommand="tbl_RowCommand"  AllowPaging="false" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                        
                                    <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                    
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("EDID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                   
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                            OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("EDID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                            
                                    <asp:BoundField DataField="EDID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText=" EDID" />
                                    <%--<asp:BoundField DataField="EDCode" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Code" />--%>
                                    <asp:BoundField DataField="EDName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Name" />
                                    <asp:BoundField DataField="EDAbbv" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="EDAbbv" />
                                    <asp:BoundField DataField="EDType" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="EDType" />
                                    <asp:BoundField DataField="DeducType" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="DeducType" />
                                    <asp:BoundField DataField="PayVal" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="PayVal" /> 
                                    <asp:BoundField DataField="MinLimit" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="MinLimit" />
                                    <asp:BoundField DataField="MaxLimit" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="MaxLimit" />
                                    <asp:BoundField DataField="Active" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Active" />
                                    <asp:BoundField DataField="FuncName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="FuncName" />
                                    <asp:BoundField DataField="InputField" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="InputField" />
                                    <asp:BoundField DataField="InputOrder" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="InputOrder" />                 
                                    <asp:BoundField DataField="CalcOrder" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="CalcOrder" />  
                                    <asp:BoundField DataField="PrintOrder" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="PrintOrder" />                                
                                </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
                           
                        
                        				
					</div>
				</div>
			</div>
		</div>
</asp:Content>

