﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;         
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class AdminSectorwiseGross_GPFReportView : System.Web.UI.Page
{
    static string ReportNameVal ="";
    static string ExportFileName = "";               
    static string SalMonth = "";
    String ConnString = "";
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPaperSize();
        BindReport(crystalReport);
    }

    protected void ExportPDF(object sender, EventArgs e)
    {
        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);
      
        ExportFormatType formatType = ExportFormatType.NoFormat;
        switch (rbFormat.SelectedItem.Value)
        {
            case "Excel":
                formatType = ExportFormatType.Excel;
                break;
            case "PDF":
                formatType = ExportFormatType.PortableDocFormat;
                break;          
        }
        crystalReport.ExportToHttpResponse(formatType, Response, true, ExportFileName);
        Response.End();
    }

    private void BindReport(ReportDocument crystalReport)
    {
        ConnString = DataAccess.DBHandler.GetConnectionString();
        SalMonth = Convert.ToString(HttpContext.Current.Request.QueryString["SalMonth"]);
        ReportNameVal = Convert.ToString(HttpContext.Current.Request.QueryString["ReportID"]);
        if (ReportNameVal == "1")
        {
            ExportFileName = "Sector Wise Salary With & Without GPF" + "(" + SalMonth + ")";
        }
        else {
            ExportFileName = "Sector Wise Salary With & Without CPF" + "(" + SalMonth + ")";
        }
        crystalReport.Load(Server.MapPath("~/Reports/SectorwiseGross_GPF.rpt"));
        crystalReport.Refresh();
        crystalReport.DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
        crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
        crystalReport.SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
        CrystalReportViewer1.DisplayToolbar = true;
        CrystalReportViewer1.Zoom(150);  // Page Width
        CrystalReportViewer1.Visible = true;
        crystalReport.VerifyDatabase(); 
        CrystalReportViewer1.DataBind();            
        CrystalReportViewer1.ReportSource = crystalReport;
    }

    public void OnConfirmprint(object sender, EventArgs e)
    {
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {
            ReportDocument crystalReport = new ReportDocument();
            BindReport(crystalReport);
            ExportFormatType formatType = ExportFormatType.NoFormat;
            switch (rbFormat.SelectedItem.Value)
            {
                case "Excel":
                    formatType = ExportFormatType.Excel;
                    break;
                case "PDF":
                    formatType = ExportFormatType.PortableDocFormat;
                    break;
            }
            crystalReport.ExportToHttpResponse(formatType, Response, false, ExportFileName);
            Response.End();
        }
        //string confirmValue = Request.Form["confirm_value"];
        //if (confirmValue == "Yes")
        //{
        //    crystalReport.Refresh();
        //    crystalReport.PrintOptions.PrinterName = ddlPrinterName.SelectedItem.Text;


        //    if (ddlPaperSize.SelectedItem.Text == "Page - A4")
        //    {
        //        crystalReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
        //        crystalReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
        //        crystalReport.PrintToPrinter(1, false, 1, crystalReport.FormatEngine.GetLastPageNumber(new CrystalDecisions.Shared.ReportPageRequestContext()));
        //    }
        //    crystalReport.Refresh();
        //}

    }

    protected void SetPaperSize()
    {
        ddlPaperSize.Items.Clear();
        ddlPaperSize.Items.Add("Page - A4");
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //if (this.crystalReport != null)
        //{
        this.crystalReport.Close();
        this.crystalReport.Dispose();
        //}
    }

}