﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Globalization;

public partial class SectorMaster : System.Web.UI.Page
{
    public static string hdnSectorId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected DataTable drpload()
    {
        DataTable dt = DBHandler.GetResult("show_sdropdown");
        return dt;
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dtstr = DBHandler.GetResult("Get_SectorMst");
            tbl.DataSource = dtstr;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_SmidByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                hdnSectorId= dtResult.Rows[0]["SectorID"].ToString();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlsmid")).SelectedValue = dtResult.Rows[0]["Smid"].ToString();
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_SectorByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtSectorCode = (TextBox)dv.FindControl("txtSectorCode");
                TextBox txtSectorName = (TextBox)dv.FindControl("txtSectorName");
                DropDownList ddlsmid = (DropDownList)dv.FindControl("ddlsmid");
                TextBox txtSmpsw = (TextBox)dv.FindControl("txtSmpsw");
                TextBox txtAddress = (TextBox)dv.FindControl("txtAddress");
                TextBox txtPhone = (TextBox)dv.FindControl("txtPhone");
                TextBox txtFAx = (TextBox)dv.FindControl("txtFAx");
                TextBox txtHeadOff = (TextBox)dv.FindControl("txtHeadOff");
                TextBox txtTanNo = (TextBox)dv.FindControl("txtTanNo");



            //    DBHandler.Execute("Insert_Sector", txtSectorCode.Text, txtSectorName.Text,  Session[SiteConstants.SSN_INT_USER_ID]);
            //    cmdSave.Text = "Add";
            //    dv.ChangeMode(FormViewMode.Insert);
            //    PopulateGrid();
            //    dv.DataBind();
            //    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
             
                DBHandler.Execute("Insert_Sector",
                    txtSectorCode.Text.Equals("") ? DBNull.Value : (object)(txtSectorCode.Text),
                    txtSectorName.Text.Equals("") ? DBNull.Value : (object)(txtSectorName.Text),

                    ddlsmid.SelectedValue.Equals("") ? DBNull.Value : (object)(ddlsmid.SelectedValue),
                    txtSmpsw.Text.Equals("") ? DBNull.Value : (object)(txtSmpsw.Text),
                    txtAddress.Text.Equals("") ? DBNull.Value : (object)(txtAddress.Text),
                    txtPhone.Text.Equals("") ? DBNull.Value : (object)(txtPhone.Text),
                    txtFAx.Text.Equals("") ? DBNull.Value : (object)(txtFAx.Text),
                    txtHeadOff.Text.Equals("") ? DBNull.Value : (object)(txtHeadOff.Text),
                    txtTanNo.Text.Equals("") ? DBNull.Value : (object)(txtTanNo.Text),

                    Session[SiteConstants.SSN_INT_USER_ID]);
                  

                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            
            }

            else if (cmdSave.CommandName == "Edit")
            {
                TextBox txtSectorCode = (TextBox)dv.FindControl("txtSectorCode");
                TextBox txtSectorName = (TextBox)dv.FindControl("txtSectorName");
                DropDownList ddlsmid = (DropDownList)dv.FindControl("ddlsmid");
                TextBox txtSmpsw = (TextBox)dv.FindControl("txtSmpsw");
                TextBox txtAddress = (TextBox)dv.FindControl("txtAddress");
                TextBox txtPhone = (TextBox)dv.FindControl("txtPhone");
                TextBox txtFAx = (TextBox)dv.FindControl("txtFAx");
                TextBox txtHeadOff = (TextBox)dv.FindControl("txtHeadOff");
                TextBox txtTanNo = (TextBox)dv.FindControl("txtTanNo");

                string ID = hdnSectorId; //;((HiddenField)dv.FindControl("hdnID")).Value;

                //DBHandler.Execute("Update_Sector", ID, txtSectorCode.Text, txtSectorName.Text,
                //    Session[SiteConstants.SSN_INT_USER_ID]);
                //cmdSave.Text = "Create";
                //dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                //dv.DataBind();
               // ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                DBHandler.Execute("Update_Sector", ID,
                   txtSectorCode.Text.Equals("") ? DBNull.Value : (object)(txtSectorCode.Text),
                   txtSectorName.Text.Equals("") ? DBNull.Value : (object)(txtSectorName.Text),

                   ddlsmid.SelectedValue.Equals("") ? DBNull.Value : (object)(ddlsmid.SelectedValue),
                   txtSmpsw.Text.Equals("") ? DBNull.Value : (object)(txtSmpsw.Text),
                    txtAddress.Text.Equals("") ? DBNull.Value : (object)(txtAddress.Text),
                    txtPhone.Text.Equals("") ? DBNull.Value : (object)(txtPhone.Text),
                    txtFAx.Text.Equals("") ? DBNull.Value : (object)(txtFAx.Text),
                    txtHeadOff.Text.Equals("") ? DBNull.Value : (object)(txtHeadOff.Text),
                    txtTanNo.Text.Equals("") ? DBNull.Value : (object)(txtTanNo.Text),

                   Session[SiteConstants.SSN_INT_USER_ID]);


                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tbl.PageIndex = e.NewPageIndex;
        PopulateGrid();
    }
}