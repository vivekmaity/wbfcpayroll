﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AdminPaySlipReport.aspx.cs" Inherits="AdminPaySlipReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
        <script type="text/javascript">
            var PayMonthId = '';
            //This is for 'Location' Binding on the Base of 'Sector'
            $(document).ready(function () {
                $("#ddlSector").change(function () {
                    var s = $('#ddlSector').val();
                    if (s != 0) {
                        if ($("#ddlSector").val() != "Please select") {
                            var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                            var options = {};
                            options.url = "PaySlipReport.aspx/GetSalMonth";
                            options.type = "POST";
                            options.data = E;
                            options.dataType = "json";
                            options.contentType = "application/json";
                            options.success = function (listSalMonth) {
                                var t = jQuery.parseJSON(listSalMonth.d);
                                var PayMon = t[0]["PayMonths"];
                                var PayMonID = t[0]["PayMonthsID"];
                                var a = t.length;
                                $("#ddlLocation").empty();
                                $('#txtPayMonth').val(PayMon);
                               // $('#hdnSalMonthID').val(PayMonID);
                                PayMonthId = PayMonID;
                                if (a >= 0) {
                                    $("#ddlLocation").append("<option value=''>Select Location</option>")
                                    $.each(t, function (key, value) {
                                        $("#ddlLocation").append($("<option></option>").val(value.CenterID).html(value.CenterName));
                                    });
                                }
                                if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != '0') {
                                    document.getElementById("ddlLocation").disabled = false;

                                }

                            };
                            options.error = function () { alert("Error in retrieving Location!"); };
                            $.ajax(options);
                        }

                    }
                    else {
                        $('#txtPayMonth').val('');
                        $("#ddlLocation").empty();
                        $("#ddlLocation").append("<option value=''>Select Location</option>")
                        $("#ddlLocation").val(0);
                    }

                });

            });

            $(document).ready(function () {

                $("#ddlReportType").change(function () {
                    if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != "0") {
                        document.getElementById("ddlLocation").disabled = false;

                    }
                    else {
                        $("#ddlLocation").val(0);
                        document.getElementById("ddlLocation").disabled = true;

                    }

                });
            });


            function opentab() {
                var SectorID;
                var SalMonth;
                var SalMonthID;
                var Centerid;
                var ReportType = $("#ddlReportType").val();
                SectorID = $("#ddlSector").val();
                SalMonthID = PayMonthId;//$('#hdnSalMonthID').val();
                SalMonth = $('#txtPayMonth').val();
                Centerid = $('#ddlLocation').val();
                var  AdminorSector="Admin";

                if (SectorID == "0") {
                    alert("Please select Sector");
                    $('#ddlSector').focus();
                    return false;
                }

                if (SalMonthID == "") {
                    alert("Please Select Sector");
                    $('#ddlSector').focus();
                    return false;
                }

                if (ReportType == "0") {
                    alert("Please select Report Type");
                    $('#ddlReportType').focus();
                    return false;
                }

                //            var E = "{SectorID:" + SectorID + ",SalMonth:" + SalMonth + "}";
                //          
                //            $.ajax({
                //                type: "POST",
                //                url: "PaySlipReport.aspx/Report_Paravalue",
                //                data: E,
                //                dataType: "json",
                //                contentType: "application/json; charset=utf-8"

                //            });           

                if (ReportType == "1") {
                    window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonth=" + SalMonth + "&AdminorSector=" + AdminorSector);
                    return false;
                }
                else if (ReportType == "2") {
                    window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonth=" + SalMonth + "&AdminorSector=" + AdminorSector);
                    return false;
                }
                else if (ReportType == "3") {
                    if ($("#ddlReportType").val() == "3" & $("#ddlLocation").val() != 0) {
                        window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&Centerid=" + Centerid + "&SalMonth=" + SalMonth + "&AdminorSector=" + AdminorSector);
                        return false;
                    }
                    else {

                        window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&SalMonthId=" + SalMonthID + "&SalMonth=" + SalMonth + "&AdminorSector=" + AdminorSector);
                        return false;
                    }
                }
                else if (ReportType == "4") {
                    window.open("PayReportView.aspx?ReportNameVal=" + ReportType + "&SectorId=" + SectorID + "&AdminorSector=" + AdminorSector);
                    return false;
                }

            }

            function unvalidate() {
                $("#form1").validate().currentForm = '';
                return true;
            }

            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
</script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
     <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>         
							<h2>Pay Report</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  " bgcolor="#669999"  >
                    <tr>
                        <td style="padding:15px;" bgcolor="White">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style1" bgcolor="White" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" class="style2" bgcolor="White" >
                                        <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" Width="100px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>

                                        </td>
                                        <td style="padding:5px;" class="style3" bgcolor="White"><span class="headFont">Report Type</span></td>
                                        <td style="padding:5px;" bgcolor="White">:</td>
                                        <td style="padding:5px;" align="left" bgcolor="White" >
                                        <asp:DropDownList ID="ddlReportType" Width="200px" Height="28px" CssClass="textbox"  Enabled="True" runat="server" autocomplete="off">
                                            <asp:ListItem Value="0">(Select Report Type)</asp:ListItem>
                                            <asp:ListItem Value="1">Pay Slip</asp:ListItem>
                                            <asp:ListItem Value="2">Pay Summary</asp:ListItem>
                                            <asp:ListItem Value="3">Pay Summary Location Wise</asp:ListItem>
                                             <asp:ListItem Value="4">Pay Register</asp:ListItem>
                                        </asp:DropDownList>   
                                        </td>

                                         <td style="padding:5px;" class="style4" bgcolor="White"><span class="headFont">Location </span></td>
                                         <td style="padding:5px;" bgcolor="White" >:</td>
                                        <td style="padding:5px;" align="left" bgcolor="White" >
                                        <asp:DropDownList ID="ddlLocation" runat="server" class="headFont" Width="230px" Height="28px" CssClass="textbox" DataValueField="CenterID" DataTextField="CenterName" 
                                        AppendDataBoundItems="true" Enabled="false" ClientIDMode="Static" autocomplete="off">
                                        <asp:ListItem Text="Select Location" Selected="True" Value="0"></asp:ListItem>
                                        </asp:DropDownList>   
                                        </td>
                                         
                                     <%--  <asp:HiddenField ID="hdnSalMonthID" runat="server"  />--%>
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" bgcolor="White" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;" /> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                        
                                    </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

