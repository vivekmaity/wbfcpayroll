﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register TagPrefix="art" TagName="TransferEmp" Src="TransferEmpDetail.ascx" %>


<%-- Add content controls here --%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <script src="js/accordion.js"></script>
    <link href="css/defaults.css" rel="stylesheet" />
    <script src="js/paccordion.js"></script>


    <style type="text/css">

         .GroupHeaderStyle{
            border:solid 1px Black;
            background-color:#81BEF7;
            font-weight:bold;
        }

    </style>



    <script type="text/javascript">
        var Flag = 0;
        $(document).ready(function () {
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });

            $('#worktype a').on('click', function () {
                flag = $(this).attr('id');
                SecID = $(this).attr('itemid');

                if (flag == "details")
                {
                    var E = "{SecID: '" + SecID + "',flag: '" + flag + "'}";

                    $.ajax({
                        type: 'POST',
                        data: E,
                        url: 'Default.aspx/AjaxGetGridCtrl',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        success: function (D) {
                            //alert(JSON.stringify(D.d));

                            $('#mainDiv').html(D.d);
                            $("#overlay").show();
                            $("#dialog").fadeIn(300);

                        },
                    });
                }
                else
                {
                    var E = "{SecID: '" + SecID + "',flag: '" + flag + "'}";

                    $.ajax({
                        type: 'POST',
                        data: E,
                        url: 'Default.aspx/AjaxGetGridCtrl',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        success: function (D) {
                            //alert(JSON.stringify(D.d));
                            PrintDiv();
                            $('#mainDiv').html(D.d);
                           
                        },
                    });
                }
            });
            /*---------------------------------------------------------*/
            /*---------------------------------------------------------*/
            $('*[id^="chkSelect"]').on('click', function () {
                var RemarksID = 0;
                $("input[id*='chkSelect']:checkbox").each(function (index) {
                    if ($(this).is(':checked')) {
                        RemarksID = $(this).closest('tr').find('td:eq(1)').text();
                    }
                });
            });
            /*---------------------------------------------------------*/
            /*---------------------------------------------------------*/
            $('#btnAccept').on('click', function () {
                var EmpNo = 0; var SecID = 0; var CenID = 0; var ArrayListEmp = []; var ArrayListSec = []; var ArrayListCen = [];
                $("input[id*='chkSelect']:checkbox").each(function (index) {

                    if ($(this).is(':checked')) {
                        EmpNo = $(this).closest('tr').find('td:eq(3)').text();
                        SecID = $(this).closest('tr').find('td:eq(1)').text();
                        CenID = $(this).closest('tr').find('td:eq(2)').text();

                        var lre = /^\s*/;
                        var rre = /\s*$/;
                        EmpNo = EmpNo.replace(lre, "");
                        EmpNo = EmpNo.replace(rre, "");

                        var lre = /^\s*/;
                        var rre = /\s*$/;
                        SecID = SecID.replace(lre, "");
                        SecID = SecID.replace(rre, "");

                        var lre = /^\s*/;
                        var rre = /\s*$/;
                        CenID = CenID.replace(lre, "");
                        CenID = CenID.replace(rre, "");

                        ArrayListEmp.push(EmpNo);
                        ArrayListSec.push(SecID);
                        ArrayListCen.push(CenID);

                    }
                });
                //alert(ArrayListEmp); alert(ArrayListSec); alert(ArrayListCen); 
                var a = ArrayListEmp.length;
                if (a != 0) {

                    var F = "{ArrayListEmp: '" + ArrayListEmp + "', ArrayListSec: '" + ArrayListSec + "', ArrayListCen: '" + ArrayListCen + "'}";
                    //alert(F);
                    $.ajax({
                        type: "POST",
                        url: pageUrl + '/AcceptEmployee',
                        data: F,
                        contentType: "application/json; charset=utf-8",
                        success: function (D) {
                            //alert(JSON.stringify(D));
                            var da = jQuery.parseJSON(D.d);
                            if (da == 'success') {
                                alert("Employee Transfered is acepted Successfully.");
                                $("#overlay").hide();
                            }
                            else {
                                alert("Employee Transfered is Not acepted.");
                            }
                        },
                        error: function (response) {
                            alert('');
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }
                else {
                    alert("Please Select CheckBox.");
                    return;
                }
            });
            /*---------------------------------------------------------*/
            /*---------------------------------------------------------*/
            $('#Close a').on('click', function () {
                $("#overlay").hide();
            });
            /*---------------------------------------------------------*/

        });
        //This is for Print 
        function PrintDiv() {
            var divContents = document.getElementById("accordion-1").innerHTML;
            var printWindow = window.open('', '', 'height=200,width=400');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }

        $(document).ready(function () {
            var boxWidth = $(".box").width();
            $(".box").animate({
                width: 0
            });

            $("#button").click(function () {
                if (Flag == 0) {
                    Flag = 1;
                    $(".box").animate({
                        width: boxWidth,
                        marginLeft: "=130px"
                    });
                }
                else {
                    Flag = 0;
                    $(".box").animate({
                        width: 0,
                        marginLeft: "=130px"
                    });
                }
            });
        });

        $(function () {
            $("#selectable").selectable();
        });

        $("#list1").click(function () {
            alert("lop");
            $("#listContednt").show();
        });

    </script>

    <style type="text/css">
        .box {
            float: right;
            overflow: hidden;
            background: #F2F6FC;
            top: 0;
            right: 0;
            height: 480px;
            width:35%;
        }
        /* Add padding and border to inner content
    for better animation effect */
        .box-inner {
            width: 400px;
            padding: 10px;
        }

        .sidePanel {
            float: right;
            height: 480px;
            width: 20px;
            background-image: #363670 url(../img/button_topnavi_selected.jpg) repeat-x top; /*url(images/loginonline.png) repeat-x top;*/
            vertical-align: middle;
            text-align: center;
            vertical-align: middle;
        }

        .vertical {
            writing-mode: tb-rl;
            vertical-align: middle;
            line-height: normal;
        }

        #feedback {
            font-size: 1.4em;
        }

        #selectable .ui-selecting {
            background: #FECA40;
        }

        #selectable .ui-selected {
            background: #F39814;
            color: white;
        }

        #selectable {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 100%;
        }

            #selectable li {
                margin: 3px;
                padding: 0.4em;
                font-size: 1.4em;
                height: 18px;
            }
    </style>

    <style>
        .ac-pane {
            margin-bottom: 0px;
            background-color: #16A085;
            color: #fff;
        }

            .ac-pane:last-child {
                margin-bottom: 0;
            }

        .ac-content {
            display: none;
        }

        .ac-title {
            border: 1px solid #fff;
            color: #fff;
            display: block;
            padding: 12px;
            background-color: #1ABC9C;
            font-family:Verdana;
            font-weight:bold;
            font-size:15px;
            text-decoration:none;
        }

            .ac-title i {
                float: right;
                font-size: 20px;
            }

                .ac-title i:before {
                    content: "\f107";
                }

        .active .ac-title i:before {
            content: "\f106";
        }

        .ac-content {
            border: 1px solid #fff;
            /*margin-top: -1px;
            padding: 15px;*/
            height:50px;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" runat="server">
    <div style="height:480px; background-color:#F2F6FC;">

        <asp:Panel ID="button" runat="server"
            CssClass="Btnclassname DefaultButton sidePanel" OnClientClick='javascript: return beforeSave();'>
            <span class="vertical">Click for Open</span>
        </asp:Panel>

        <div class="content-overlay" id='overlay'>
            <div class="wrapper-outer">
                <div class="wrapper-inner">
                    <div class="loadContent">
                        <div id="Close" class="close-content"><a href="javascript:void(0);" id="prev" style="color: red;">Close</a></div>
                        <div style="float: left;">
                            <h1 style="font-family: Verdana; font-size: 18px; font-weight: bold;">Details of Transfer Employee</h1>
                        </div>
                        <table cellpadding="5" style="border: solid 1px lightblue; padding-bottom: 10px; width: 650px; overflow: scroll;" cellspacing="0">
                            <tr>
                                <td>
                                    <table align="center" cellpadding="5">
                                        <tr>
                                            <td>
                                                <div class='divCaption' id='divCaption'></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="mainDiv" runat="server" clientidmode="Static" style="width: 650px; height: 440px; overflow: scroll;">
                                                </div>
                                                <div class="container">
                                                    <table>
                                                        <tr>
                                                            <td style="padding: 5px;"></td>
                                                            <td style="padding: 10px; align-items: center;">
                                                                <asp:Button ID="btnAccept" runat="server" Text="Accept Employee" CommandName="Add"
                                                                    Width="115px" CssClass="Btnclassname DefaultButton" OnClientClick='javascript: return beforeSave();' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="float: right;">
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="accordion-wrapper">
                <div class="ac-pane active">
                    <a href="#" class="ac-title">
                        <span>Employee(s) Transfered Details</span>
                        <i class="fa"></i>
                    </a>
                    <div class="ac-content" style="height:250px; overflow:scroll;">
                        
                        <asp:GridView ID="grdViewOrders" runat="server" AutoGenerateColumns="False" TabIndex="1" OnRowDataBound="grdViewOrders_RowDataBound"
                            Width="100%" DataKeyNames="SectorID" CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                            BorderStyle="None" BorderWidth="1px" ForeColor="Black">
                            <AlternatingRowStyle BackColor="#FFFACD" />
                            <Columns>
                                <asp:TemplateField HeaderText="UserID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserID" autocomplete="off" runat="server" Visible="false" Text='<%# Bind("UserID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SectorID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSectorID" autocomplete="off" runat="server" Visible="false" Text='<%# Bind("SectorID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sector Name" Visible="true">
                                    <ItemTemplate>
                                        &nbsp;&nbsp;<asp:Label ID="Label1" autocomplete="off" runat="server" Visible="true" Text="Employee(s) Transfered to "></asp:Label>
                                        <asp:Label ID="lblSectorName" autocomplete="off" runat="server" Visible="true" Text='<%# Bind("SectorName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="true">
                                    <ItemTemplate>
                                        <div id="worktype">
                                            <a href="javascript:void(0);" id="details" itemid='<%# Eval("SectorID") %>'>Accept</a> &nbsp;|&nbsp; 
                                        <a href="javascript:void(0);" id="view" itemid='<%# Eval("SectorID") %>'>View/Print</a>
                                        </div>

                                    </ItemTemplate>

                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                       
                    </div>
                </div>
                <div class="ac-pane">
                    <a href="#" class="ac-title">
                        <span>Others</span>
                        <i class="fa"></i>
                    </a>
                    <div class="ac-content" style="height: 250px; overflow: scroll;">
                        <asp:GridView ID="grvOther" runat="server" AutoGenerateColumns="False" TabIndex="1"
                            Width="100%" ForeColor="Black" GridLines="Both" BorderStyle="None" CssClass="Grid"
                            PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
                            <AlternatingRowStyle BackColor="#FFFACD" />
                            <Columns>
                                <asp:TemplateField HeaderText="Others" Visible="true">
                                    <ItemTemplate>
                                        &nbsp;&nbsp;<asp:Label ID="lblUserI" autocomplete="off" runat="server" Visible="true" Text='<%# Bind("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SectorID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSectorID" autocomplete="off" runat="server" Visible="false" Text=""></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SectorName" Visible="false">
                                    <ItemTemplate>
                                        &nbsp;&nbsp;
                                            <asp:Label ID="lblSectorNam" autocomplete="off" runat="server" Visible="false" Text=""></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SectorName" Visible="false">
                                    <ItemTemplate>
                                        &nbsp;&nbsp;
                                            <asp:Label ID="lblSectorNam" autocomplete="off" runat="server" Visible="true" Text=""></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="true">
                                    <ItemTemplate>
                                        <div id="worktype">
                                            <a href="javascript:void(0);" id="view">View/Print</a>

                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>



    </div>
</asp:Content>