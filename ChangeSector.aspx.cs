﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class ChangeSector : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtEmpNoSearch.Enabled = true;
        btnSave.Enabled = false;
        btnSave1.Enabled = false;
        //if (Session[SiteConstants.SSN_INT_USER_ID] != null)
        //{
        //    DataTable dt = DBHandler.GetResult("Get_SectorMst");
        //    if (dt.Rows.Count > 0)
        //    {
        //        ddlSec.DataSource = dt;
        //        ddlSec.DataBind();
        //    }
        //}
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (cmdSearch.CommandName == "Search")
            {
                string EmployeeNo = txtEmpNoSearch.Text;
                DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
                int Secid = Convert.ToInt32(Sectors.SelectedValue.ToString());

                if (EmployeeNo != "")
                {
                    if (Secid != 0)
                    {
                        DataSet ds = DBHandler.GetResults("Get_EmployeeDetails", EmployeeNo, Secid, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                        DataTable dtss = ds.Tables[0];  DataTable dtresult = ds.Tables[2];
                        if (dtss.Rows.Count == 0)
                        {
                            if (dtresult.Rows[0][0].ToString() == "Y")
                            {
                                string display = "Please select the appropriate Sector of this Employee.";
                                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                                return;
                            }
                            else
                            {
                                string display = "Sorry ! You are not authorised.";
                                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + display + "')</script>");
                                return;
                            }
                        }
                        else
                        {
                            DataSet dss = DBHandler.GetResults("get_empDetails_for_loan", EmployeeNo);
                            DataTable dt = dss.Tables[0];

                            if (dt.Rows.Count > 0)
                            {
                                if (Session[SiteConstants.SSN_INT_USER_ID] != null)
                                {
                                    DataTable d = DBHandler.GetResult("Get_SectorMst", EmployeeNo);
                                    if (d.Rows.Count > 0)
                                    {
                                        ddlSec.DataSource = d;
                                        ddlSec.DataBind();
                                    }
                                }


                                txtEmpNoSearch.Enabled = false;
                                btnSave.Enabled = true;
                                btnSave1.Enabled = true;
                                TextBox txtEmpName = (TextBox)empDetails.FindControl("txtEmpName");
                                TextBox txtDesignation = (TextBox)empDetails.FindControl("txtDesignation");
                                TextBox txtPayScale = (TextBox)empDetails.FindControl("txtPayScale");
                                TextBox txtNetPay = (TextBox)empDetails.FindControl("txtNetPay");
                                TextBox txtSector = (TextBox)empDetails.FindControl("txtSector");
                                TextBox txtLocation = (TextBox)empDetails.FindControl("txtLocation");
                                Label lblNetPay = (Label)empDetails.FindControl("lblNetPay");
                                txtEmpName.Text = dt.Rows[0]["EmpName"].ToString();
                                txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
                                txtPayScale.Text = dt.Rows[0]["PayScale"].ToString();
                                lblNetPay.Text = dt.Rows[0]["SalMonth"].ToString();
                                txtNetPay.Text = dt.Rows[0]["NetPay"].ToString();
                                txtSector.Text = dt.Rows[0]["SectorName"].ToString();
                                txtLocation.Text = dt.Rows[0]["CenterName"].ToString();

                                Session["DesignationID"] = dt.Rows[0]["DesignationID"].ToString();
                                Session["SectorID"] = dt.Rows[0]["SectorID"].ToString();
                                Session["CenterID"] = dt.Rows[0]["CenterID"].ToString();
                                Session["QuaterAllot"] = dt.Rows[0]["QuaterAllot"].ToString();
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('LPC for this Employee has been already generated.')</script>");
                            }
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Select the Sector.')</script>");
                        Sectors.Focus();
                        return;
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Enter Employee No.')</script>");
                    txtEmpNoSearch.Focus();
                    return;
                }
                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveSector(string SecId, string CenId, string EmpNo, string TransOrderNo, string TransDate, string ReleOrderNo, string ReleDate, string MemoNo, string MemoDate, string ProcHeadOffice, string Remarks,
                                    string From, string To,
                                    string ProceedingPost = "", string SancLeave = "", string NoofDays = "", string HouseRentUpto = "", string ParameterTypes = "")
    {
        string JSONVal = ""; string BeforeSecEncodedValue = ""; string AfterSecEncodedValue = "";
        try
        {
            DataTable dtEncodeValueAfter = DBHandler.GetResult("Get_EncodedValuebySectorID", SecId);
            if (dtEncodeValueAfter.Rows.Count > 0)
            {
                string EncValue = dtEncodeValueAfter.Rows[0]["EncodedValue"].ToString();
                AfterSecEncodedValue = EncValue;
            }

            DataTable dtEncodeValueBefore = DBHandler.GetResult("Get_EncodedValuebySectorID", HttpContext.Current.Session["SectorID"]);
            if (dtEncodeValueBefore.Rows.Count > 0)
            {
                string EncValue = dtEncodeValueBefore.Rows[0]["EncodedValue"].ToString();
                BeforeSecEncodedValue = EncValue;
            }

            if (BeforeSecEncodedValue != "" && AfterSecEncodedValue != "")
            {
                string res = "Generated";
                JSONVal = res.ToJSON();
           }
            else
            {
                if (BeforeSecEncodedValue == "" && AfterSecEncodedValue =="")
                {
                    DataTable dtavailable = DBHandler.GetResult("Get_CheckLPCisAvailable", EmpNo);
                    if (dtavailable.Rows.Count > 0 && dtavailable.Rows[0]["Result"].ToString() == "No")
                    {
                        DBHandler.Execute("Update_EmpMasterforLPC", EmpNo);

                        DataTable dt = DBHandler.GetResult("Insert_EmpMasterforLPC", EmpNo,
                                                                TransOrderNo.Equals("") ? DBNull.Value : (object)TransOrderNo, TransDate.Equals("") ? DBNull.Value : (object)TransDate,
                                                                ReleOrderNo.Equals("") ? DBNull.Value : (object)ReleOrderNo, ReleDate.Equals("") ? DBNull.Value : (object)ReleDate,
                                                                HttpContext.Current.Session["DesignationID"].Equals("") ? DBNull.Value : (object)HttpContext.Current.Session["DesignationID"],
                                                                HttpContext.Current.Session["SectorID"].Equals("") ? DBNull.Value : (object)HttpContext.Current.Session["SectorID"],
                                                                HttpContext.Current.Session["CenterID"].Equals("") ? DBNull.Value : (object)HttpContext.Current.Session["CenterID"],
                                                                SecId.Equals("") ? DBNull.Value : (object)SecId, CenId.Equals("") ? DBNull.Value : (object)CenId,
                                                                HttpContext.Current.Session["QuaterAllot"].Equals("") ? DBNull.Value : (object)HttpContext.Current.Session["QuaterAllot"],
                                                                MemoNo.Equals("") ? DBNull.Value : (object)MemoNo, MemoDate.Equals("") ? DBNull.Value : (object)MemoDate,
                                                                ProcHeadOffice.Equals("") ? DBNull.Value : (object)ProcHeadOffice,
                                                                Remarks.Equals("") ? DBNull.Value : (object)Remarks,

                                                                From.Equals("") ? DBNull.Value : (object)From,
                                                                To.Equals("") ? DBNull.Value : (object)To,
                                                                ProceedingPost.Equals("") ? DBNull.Value : (object)ProceedingPost,
                                                                SancLeave.Equals("") ? DBNull.Value : (object)SancLeave,
                                                                NoofDays.Equals("") ? DBNull.Value : (object)NoofDays,
                            //HouseRent.Equals("") ? DBNull.Value : (object)HouseRent,
                                                                HouseRentUpto.Equals("") ? DBNull.Value : (object)HouseRentUpto,
                                                                ParameterTypes.Equals("") ? DBNull.Value : (object)ParameterTypes,

                                                                HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                        if (dt.Rows.Count > 0)
                        {
                            string result = "sucess";
                            JSONVal = result.ToJSON();
                        }
                        else
                        {
                            string result = "fail";
                            JSONVal = result.ToJSON();
                        }
                    }
                    else
                    {
                        string result = "Transfered";
                        JSONVal = result.ToJSON();
                    }


                    //string beforSector = dtEncodeValueBefore.Rows[0]["SectorName"].ToJSON();
                    //string afterSector = dtEncodeValueAfter.Rows[0]["SectorName"].ToJSON();

                    //string bothSector = beforSector + ',' + afterSector;
                    //JSONVal = bothSector.ToJSON();

                }
                else if (BeforeSecEncodedValue == "")
                {
                    JSONVal = dtEncodeValueAfter.Rows[0]["SectorName"].ToJSON();
                }
                else if (AfterSecEncodedValue == "")
                {
                    JSONVal = dtEncodeValueBefore.Rows[0]["SectorName"].ToJSON(); 
                }
            }

       }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod]
    public static string GetCenter(int SecID)
    {
        DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
        DataTable dt = ds.Tables[0];

        List<CenterbySector> listCenter = new List<CenterbySector>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CenterbySector objst = new CenterbySector();

                objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);

                listCenter.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listCenter);

    }
    public class CenterbySector
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
    }
}