﻿<%@ Page Language="C#" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="OpenReport_b.aspx.cs" Inherits="OpenReport_b" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<%@ Register assembly="CrystalDecisions.Web" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="crystalreportviewers13/js/crviewer/crv.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
            Height="50px" Width="350px" GroupTreeStyle-ShowLines="False" 
            HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False" 
            HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False" 
            ShowAllPageIds="True" ReuseParameterValuesOnRefresh="true" 
            ToolPanelView="None" ViewStateMode="Enabled"   />
    </div>
    </form>
</body>
</html>
