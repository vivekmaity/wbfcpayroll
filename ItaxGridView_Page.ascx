﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItaxGridView_Page.ascx.cs"  Inherits="ItaxGridView_Page" %>
<link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .ModeAction {
        font-size: 14px;
        font-weight: bold;
        text-decoration: none;
    }

    .HideColumn {
        display: none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".DefaultButton").click(function (event) {
            event.preventDefault();
        });
    }); 
</script>


<form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridTaxDetails" runat="server" ShowFooter="false" Width="100%"
            AutoGenerateColumns="false" CssClass="Grid1" OnRowDataBound="GridView1_RowDataBound"
            CellSpacing="0" CellPadding="0" Font-Bold="false">
            <AlternatingRowStyle BackColor="#ccffff" />
            <Columns>
                <asp:TemplateField HeaderText="Serial number">
                    <ItemTemplate>
                        <asp:Label ID="lblSerial" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ID" Visible="true" HeaderStyle-CssClass="HideColumn" ItemStyle-CssClass="HideColumn">
                    <ItemTemplate>
                        <asp:TextBox ID="txtCheck" value="0" ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ID" Visible="true" HeaderStyle-CssClass="HideColumn" ItemStyle-CssClass="HideColumn">
                    <ItemTemplate>
                        <asp:TextBox ID="lblId" Text='<%# Eval("ItaxTypeID").ToString()%>' ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TypeDesc" Visible="true" HeaderText="Tax Type" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Button ID="btnDetails4" runat="server" ClientIDMode="Static" OnClientClick="checkButton(this)" CssClass="Btnclassname DefaultButton ddd" Width="90px" Text="Show Details" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Serial number" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lableMaxType" Text='<%# Eval("MaxType").ToString()%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Serial number" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lableMaxAmt" Text='<%# Eval("MaxAmount").ToString()%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="lableHAmount" Text='<%# Eval("lblAmount").ToString()%>' runat="server"></asp:Label>
                        <asp:TextBox ID="txtHAmount" ClientIDMode="Static" Text='<%# Eval("Amount").ToString()%>' Style="text-align: right" runat="server" onkeypress="return NumberOnly()" CssClass="textbox allownumericwithdecimal" MaxLength="10" autocomplete="off"></asp:TextBox>
                        <itemstyle horizontalalign="Right" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemTemplate>
                        <%--<table></table>--%>
                        <tr align="right" class=''>
                            <%--  <td></td>--%>
                            <td colspan="100%" align="right">
                                <div class='show4 <%# Eval("ItaxTypeID").ToString()%>' style="max-height: 200px; overflow: auto; display: none; width: 95%">
                                    <asp:GridView ID="gv80C80CCD" runat="server" ShowFooter="true" HeaderStyle-BackColor="#ffff99" ClientIDMode="Static"
                                        AutoGenerateColumns="false" CssClass="Grid1" OnRowDataBound="GridView_RowDataBound" Style="overflow: auto" HeaderStyle-CssClass="FixedHeader"
                                        CellSpacing="0" CellPadding="0" Font-Bold="false">
                                        <AlternatingRowStyle BackColor="Beige" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Serial number" ItemStyle-CssClass="HideColumn" HeaderStyle-CssClass="HideColumn">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="HideChildID" ClientIDMode="Static" Text='<%# Eval("ItaxSaveID").ToString()%>' Style="text-align: right; display: none" CssClass="textbox" runat="server" Width="100px" autocomplete="off"></asp:TextBox>
                                                </ItemTemplate>
                                                <FooterStyle CssClass="HideColumn" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Serial number">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSerial1" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="SavingTypeDesc" Visible="true" HeaderText="Saving Type" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderText="Amount" HeaderStyle-Font-Bold="false" ItemStyle-HorizontalAlign="Right" ControlStyle-Font-Bold="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="itaxSaveMaxAmount" CssClass="itaxSaveMaxAmount" Text='<%# Eval("lblAmount").ToString()%>' runat="server">0</asp:Label>
                                                    <asp:TextBox ID="txtGAmount" ClientIDMode="Static" Text='<%# Eval("Amount").ToString()%>' onkeypress="return NumberOnly()" onkeyup="Calcutate(this)" Style="text-align: right" CssClass="textbox DefaultButton allownumericwithdecimal numeric txtevent" runat="server" Width="100px" autocomplete="off"></asp:TextBox>
                                                    <asp:HiddenField ClientIDMode="Static" runat="server" ID="txtGAmounthid"/>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <%--CssClass=""--%>
                                                    <asp:Label ID="Label1" runat="server" Text="Total = " Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="lblTotal1" ClientIDMode="Static"  runat="server" Style="text-align: right" Text="0" Font-Bold="true"></asp:Label>
                                                </FooterTemplate>
                                                <FooterStyle BackColor="AntiqueWhite" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <table style="width:100%" id="tblNewSavingTypeAdd">
                                        <tr>
                                            <td style="padding: 4px;width:110px">
                                                <span class="headFont">Abbv&nbsp;&nbsp;</span><span class="require">*</span> 
                                                <asp:TextBox ID="txtAbbreviation" Style="text-transform: uppercase;width:100px" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                            </td>
                                            <td style="padding: 4px;width:150px">
                                                <span class="headFont">Desc&nbsp;&nbsp;</span><span class="require">*</span> 
                                                <asp:TextBox ID="txtDescription" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                            </td>
                                             <td style="padding: 4px;width:150px">
                                                 <input type="radio" id="rdSavingsValue" name="gender" onchange="rdValuePercentageNewSavingsType(this)" value="V"  class="rdSavingsValue"> Value<br>
                                                 <input type="radio" id="rdSavingsPercentage" name="gender" onchange="rdValuePercentageNewSavingsType(this)"  value="P" class="rdSavingsPercentage"> Percentage<br>
                                            </td>
                                            <td style="padding: 4px;width:130px;display:none" class="hidetdRdbWise">
                                                <span class="headFont chngClass">Percentage(%)&nbsp;&nbsp;</span><span class="require">*</span> 
                                                <asp:TextBox ID="txtPercentage" Style="text-transform: uppercase;" onkeypress="return NumberOnly()" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                            </td>
                                            <td style="padding: 4px;width:130px ;" >
                                                <span class="headFont">Max Amount&nbsp;&nbsp;</span><span class="require">*</span> 
                                                <asp:TextBox ID="txtMaxAmountsaving" Style="text-transform: uppercase;" onkeypress="return NumberOnly()" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="50" autocomplete="off"></asp:TextBox>
                                                <input type="hidden" id="IDTaxType" value='<%# Eval("ItaxTypeID").ToString()%>'/>
                                            </td>
                                            <td style="padding: 4px; width: 100px" align="center">
                                                <span> New</span>
                                                <asp:Button runat="server" autopoastback="false" CssClass='Btnclassname DefaultButton btnAddNewSavingType' Width="80px" Text="Add" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </ItemTemplate>

                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</form>
