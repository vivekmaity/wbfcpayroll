﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Management;
using System.Runtime.InteropServices;

public partial class FinalBankReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static String ISEncodedValueGenerated()
    {
        string JSONVal = "";
        try
        {
            DataTable dtResult = DBHandler.GetResult("Find_ISEncodedValueGenerated");
            if (dtResult.Rows.Count > 0)
            {
                int res = Convert.ToInt32(dtResult.Rows[0]["Result"].ToString());
                if (res > 0)
                {
                    string value = "NotGenerate";
                    JSONVal = value.ToJSON();
                }
                else
                {
                    string value = "Generate";
                    JSONVal = value.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }
}