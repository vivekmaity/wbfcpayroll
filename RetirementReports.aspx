﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="RetirementReports.aspx.cs" Inherits="RetirementReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var ParameterTypes = "P";
        $(document).ready(function () {
            $('#rdPDF').click(function () {
                if (document.getElementById('rdPDF').checked) {
                    ParameterTypes = "P";
                }
            });

            $('#rdExcel').click(function () {
                if (document.getElementById('rdExcel').checked) {
                    ParameterTypes = "E";
                }
            });
        });
//        $(document).ready(function () {
//            var StrRetirementDate = '';
//            if ($('#txtRetirementDate').val() != '') {
//                var fromDate = $("#txtRetirementDate").val().split("/");
//                StrRetirementDate = fromDate[1] + "-" + fromDate[2];
//            }
//            var E = '';
//            E = "{StrRetirementDate:'" + StrRetirementDate + "'}";
//            //            alert(E);
//            $.ajax({

//                type: "POST",
//                url: "RetirementReports.aspx/LastDay_CurrentMonth",
//                data: E,
//                dataType: "json",
//                contentType: "application/json; charset=utf-8",
//                success: function (listCenter) {
//                    var t = jQuery.parseJSON(listCenter.d);
//                    var CurrentMonth = t[0]["LastDay_CurrentMonth"];

//                    $('#txtRetirementDate').val(CurrentMonth);
//                }
//            });
//        });

        $(document).ready(function () {
            $('#txtRetirementDate').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
               
                if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57))
                    alert("Please Select Retirement Date");
                $('#txtRetirementDate').focus();
                $(this).dialog('close');
                return false;
            });
        });


//        $(document).ready(function () {
//            $('#txtRetirementDate').datepicker({
//                showOtherMonths: true,
//                selectOtherMonths: true,
//                closeText: 'X',
//                showAnim: 'drop',
//                showButtonPanel: true,
//                duration: 'slow'
//            });



        $(document).ready(function () {
            $('#txtRetirementDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                duration: 'slow',
                onSelect: function (dates) {
                    var aa = $('#txtRetirementDate').val();
                    var a = aa.split('/');
                    for (var i = 0; i < a.length; i++) {
                        var date = a[0];
                        var month = a[1];
                        var year = a[a.length - 1];
                    }
                   RetirementDate();

                }
            });

            $('#txtToDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow'

            });

            $("#btnFromDate").click(function () {
                $('#txtRetirementDate').datepicker("show");
                return false;
            });

            $("#btnToDate").click(function () {
                $('#txtToDate').datepicker("show");
                return false;
            });
        });


        // ==================== Start From Indent keypress Type Not Allowed Coding Here =========================//
        function RetirementDate() {
//            alert("year2");
            var strmonth = '';
            var strmonthname = '';


            var StrRetirementDate = '';
            if ($('#txtRetirementDate').val() != '') {
                var fromDate = $("#txtRetirementDate").val().split("/");
                StrRetirementDate = fromDate[1] + "/" + fromDate[2];
            }
            var E = '';
            E = "{StrRetirementDate:'" + StrRetirementDate + "'}";
            //            alert(E);
            $.ajax({

                type: "POST",
                url: "RetirementReports.aspx/LastDay_CurrentMonth",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (listCenter) {
                    var t = jQuery.parseJSON(listCenter.d);
                    var CurrentMonth = t[0]["LastDay_CurrentMonth"];

                    $('#txtRetirementDate').val(CurrentMonth);
                }
            });


            if ($('#txtRetirementDate').val() != '') {
                var fromDate = $("#txtRetirementDate").val().split("/");
                strmonth = fromDate[1];


                if (strmonth == '01') {
                    strmonthname = 'January';
                }
                if (strmonth == '02') {
                    strmonthname = 'February';

                }
                if (strmonth == '03') {
                    strmonthname = 'March';
                }
                if (strmonth == '04') {
                    strmonthname = 'April';
                }
                if (strmonth == '05') {
                    strmonthname = 'May';
                }
                if (strmonth == '06') {
                    strmonthname = 'June';
                }
                if (strmonth == '07') {
                    strmonthname = 'July';
                }
                if (strmonth == '08') {
                    strmonthname = 'August';
                }
                if (strmonth == '09') {
                    strmonthname = 'September';
                }
                if (strmonth == '10') {
                    strmonthname = 'October';
                }
                if (strmonth == '11') {
                    strmonthname = 'November';
                }
                if (strmonth == '12') {
                    strmonthname = 'December';
                }


                StrFromIndentDate = strmonthname + "-" + fromDate[2];
                $('#TxtRetirementMonth').val(StrFromIndentDate);
            }


        }
        // ==================== End From Indent keypress Type Not Allowed Coding Here =========================//
       

        function PrintOpentab() {
            var StrRetirementDate = '';
            var StrMonthYr = '';
           
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            StrMonthYr = $('#TxtRetirementMonth').val();
            FormName = "RetirementReports.aspx";
            ReportName = "RetirementList";
            ReportType = "Retirement";

            if ($('#txtRetirementDate').val() == 0) {
                alert("Please select Retirement Date");
                $('#txtRetirementDate').focus();
                return false;
            }

            if ($('#txtRetirementDate').val() != '') {
                var fromDate = $("#txtRetirementDate").val().split("/");
                StrRetirementDate = fromDate[2] + "-" + fromDate[1];
            }
            if (ParameterTypes == "P") {

                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";
                if (confirm("Do you want to view the report ?")) {
                    confirm_value.value = "Yes";
                    $(".loading-overlay").show();
           
            //alert(StrMonthYr);
            var E = '';
            E = "{StrRetirementDate:'" + StrRetirementDate + "',StrMonthYr:'" + StrMonthYr + "',FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
            //alert(E);
            $.ajax({

                type: "POST",
                url: "RetirementReports.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    window.open("ReportView1.aspx?");
                }
            });
                } else {
                    confirm_value.value = "No";

                }
            }
            else {
                //var ReportName = "Arrear_bank_slip";

                //var PaymentDate = $('#ddlArrearPayDate').find('option:selected').text();
                window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "&StrRetirementDate=" + StrRetirementDate + "&StrMonthYr=" + StrMonthYr);

            }


        }
        




        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        

</script>
    <style type="text/css">
        .style4
        {
            width: 232px;
        }
        .inputbox2
        {
            margin-bottom: 4px;
        }
        .calendar
        {}
        .style5
        {
            width: 427px;
        }
        .style6
        {
            width: 309px;
        }
        .style7
        {
            width: 209px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Retirement Report</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" class="style16" ><span class="headFont">Retirement Date &nbsp;&nbsp;</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" class="style4" >
                                        <asp:TextBox ID="txtRetirementDate" ClientIDMode="Static" autocomplete="off" 
                                                ToolTip="Please Enter From Indent Date" runat="server" Width="158px" 
                                                MaxLength="10"  Height="22px" 
                                               ></asp:TextBox>
                                            <asp:ImageButton ID="btnFromDate" ToolTip="Please Click For From Indent Date" 
                                                CausesValidation="false" ImageUrl="~/img/date.png"
                                                AlternateText="Click to show calendar" CssClass="dpDate calendar" 
                                                runat="server" Height="24px" Width="22px" />

                                        </td>
                                        <td style="padding:5px;" class="style18"><span class="headFont">Name of the Month & Year</span></td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" class="style6" >
                                        <asp:TextBox ID="TxtRetirementMonth" autocomplete="off" ClientIDMode="Static"  Enabled="false" runat="server" ToolTip="Please Enter To Indent Number"  Width="100px" MaxLength="50" ></asp:TextBox>
                                        
                                        </td>
                                       
                                           <td style="padding: 1px; vertical-align: top;">
                                            <asp:RadioButton ID="rdPDF" Checked="true" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="PDF" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdExcel" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="Excel" Checked="false" />
                                        </td>
 
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="PrintOpentab();return false;"/> 
                                       
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
 
                                      <%-- <asp:HiddenField ID="hdnSalMonthID" runat="server"  />--%>
                                    </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

