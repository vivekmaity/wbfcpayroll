﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AdminScheduleReport.aspx.cs" Inherits="AdminScheduleReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        var ParameterTypes1 = "D";
        var ParameterTypes = "P";
        $(document).ready(function () {
            $('#ddlBoth').hide();
            //$('#ddlEmpType').hide();
            //$('#LempType').hide();
            $('#Total').hide();
            $('#TotalValue').hide();
            $('#TotalEmp').hide();
            });
       
        $(document).ready(function () {
           
            $('#rdPDF').click(function () {
                if (document.getElementById('rdPDF').checked) {
                    ParameterTypes = "P";
                }
            });

            $('#rdExcel').click(function () {
                if (document.getElementById('rdExcel').checked) {
                    ParameterTypes = "E";
                }
            });

            $('#RadBoth').click(function () {
                if (document.getElementById('RadBoth').checked) {
                    document.getElementById("RadBDeduction").checked = false;
                    document.getElementById("RadBLoan").checked = false;
                    $('#ddlBoth').show();
                    $('#ddlSched').hide();
                    $('#ddlEmpType').show();
                    //$('#LempType').show();
                    $('#Total').show();
                    $('#TotalValue').show();
                    $('#TotalEmp').show();
                    ParameterTypes1 = "B";
                }
            });
        });
        function opentab() {       
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            var StrFinYr = '';
            var StrSecID;
            var StrSalMonth;
            var StrSalMonthID;
            var StrCenID;
            var StrSchedule;
            var StrEmpType;
            var StrSchedule1;
            FormName = "AdminScheduleReport.aspx";
            //if ($("#ddlReportType").val() == "1") {
            //    ReportName = 'GPFCPFAND_ITAX';
            //    ReportType = 'Sector Wise GPF,CPF And ITAX';
            //}
            //if ($("#ddlReportType").val() == "2") {
            //    ReportName = 'SectorwiseGross_gpf_with_ITAX_PTAX';
            //    ReportType = 'Sector wise GPF,ITAX,PTAX Summary';
            //}
            //if ($("#ddlReportType").val() == "3") {
            //    ReportName = 'Admin_ITAX';
            //    ReportType = 'Sector wise ITAX';
            //}

                                      
            SalMonth = $('#txtPayMonth').val();
            SalMonthID = 0; 
            ReportType = "Admin Schedule Report"
            SalFinYear = $("#txtSalFinYear").val();
            StrSchedule = $('#ddlSched').val();
            StrEmpType = $('#ddlEmpType').val();
            StrSchedule1 = $('#ddlBoth').val();
            //alert(StrEmpType);
            //alert(StrSchedule1);
            //if (SalMonth == "") {
            //    alert("Salary Month Invalid");
            //    return false;
            //}
         
            //if (ReportType == "0") {
            //    alert("Please select Report Type");
            //    $('#ddlReportType').focus();
            //    return false;
            //}

            if (ParameterTypes1 == "D") {
                if (StrSchedule == 1) {
                    ReportName = "Pay_Schedule_ITAX_Admin";

                }
                else if (StrSchedule == 5) {
                    ReportName = "Pay_Schedule_PTAX_Admin";

                }
                else {
                    ReportName = "Pay_Schedule_MICE_Admin";
                }
            }
            if (ParameterTypes1 == "L")
            {
                ReportName = "Pay_Schedule_LOAN_Admin";

            }
            if (ParameterTypes1 == "B") {
                StrSchedule = $('#ddlBoth').val();
                if (StrSchedule == 3) {
                    ReportName = "AdminAllsector_COOP";
                }
                else {
                    ReportName = "AdminAllsector_CPF_all_PF_state";
                }
                //alert(StrSchedule);
            }
            if (ParameterTypes == "P") {

                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";
                if (confirm("Do you want to view the report ?")) {
                    confirm_value.value = "Yes";
                    $(".loading-overlay").show();

              
              
                    var E = "{SalMonth:'" + SalMonth + "',SalMonthID:" + SalMonthID + ",StrSchedule:" + StrSchedule + ",SalFinYear:'" + SalFinYear + "',StrEmpType:'" + StrEmpType + "',FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
            //alert(E);
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",               
                url: "AdminScheduleReport.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    window.open("ReportView1.aspx?");
                    $(".loading-overlay").hide();
                }
            });   
                } else {
                    confirm_value.value = "No";

                }
            }
            else {
                E = "{SalMonth:'" + SalMonth + "',SalMonthID:" + SalMonthID + ",StrSchedule:" + StrSchedule + ",SalFinYear:'" + SalFinYear + "',StrEmpType:'" + StrEmpType +"',ReportName:'" + ReportName + "'}";
                //alert(E);
                $.ajax({

                    type: "POST",
                    url: "AdminScheduleReport.aspx/Count_Excel",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        //alert(JSON.stringify(jsmsg));
                        if (jsmsg > 0)
                            window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "&SalMonth=" + SalMonth + "&SalMonthID=" + SalMonthID + "&StrSchedule=" + StrSchedule + "&SalFinYear=" + SalFinYear + "&StrEmpType=" + StrEmpType );
                        else {
                            alert("No Records found");
                        }


                    }
                });
            }
        }
                
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function deduction() {
            if (document.getElementById('RadBDeduction').checked) {
                document.getElementById("RadBLoan").checked = false;
                document.getElementById("RadBoth").checked = false;
                $('#ddlBoth').hide();
                $('#ddlSched').show();
                $('#ddlEmpType').hide();
                //$('#LempType').hide();
                $('#TotalValue').hide();
                $('#TotalEmp').hide();
                $(document).ready(function () {
                    $.ajax({

                        type: "POST",
                        url: "AdminScheduleReport.aspx/GetDeduction",
                        data: {},

                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlSched").empty();
                            $("#ddlSched").append("<option value=''>Select Schedule Deduction</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlSched").append($("<option></option>").val(value.EDID).html(value.EDName));
                            });
                        },
                        error: function (data) {
                            //alert("error found");
                        }
                    });
                });
            }

            else {
                document.getElementById("RadBLoan").checked = true;
            }
        }
        function loan() {

            if (document.getElementById('RadBLoan').checked) {
                document.getElementById("RadBDeduction").checked = false;
                document.getElementById("RadBoth").checked = false;
                ParameterTypes1 = "L";
                $('#ddlBoth').hide();
                $('#ddlSched').show();
                $('#ddlEmpType').hide();
                // $('#LempType').hide();
                $('#TotalValue').hide();
                $('#TotalEmp').hide();
                $(document).ready(function () {
                    $.ajax({
                        type: "POST",
                        url: "AdminScheduleReport.aspx/GetLoan",
                        data: {},
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsdata = JSON.parse(data.d);
                            $("#ddlSched").empty();
                            $("#ddlSched").append("<option value=''>Select Schedule Loan</option>")
                            $.each(jsdata, function (key, value) {
                                $("#ddlSched").append($("<option></option>").val(value.EDID).html(value.EDName));
                            });
                        },
                        error: function (data) {
                        }
                    });
                });

            }


            else {
                document.getElementById("RadBDeduction").checked = true;
            }
        }

</script>
    <style type="text/css">
        .style1
        {
            width: 77px;
        }
        .style2
        {
            width: 129px;
        }
        .style3
        {
            width: 88px;
        }
        .style4
        {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Admin Schedule Report</h2>						
						</section>
                                                                        <asp:HiddenField ID="hdnSalMonthID" runat="server" />
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                           
                                <table align="left" width="100%">
                                    <tr>
                                        <td style="padding: 5px; width:100px;" ><span class="headFont">Sal Month &nbsp;&nbsp;</span> </td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left" >
                                            <asp:TextBox ID="txtPayMonth" autocomplete="off" ClientIDMode="Static" runat="server" Width="180px" CssClass="textbox" MaxLength="10" Enabled="false"></asp:TextBox>
                                        </td>

                                        <td style="padding: 5px;" ><span class="headFont">Schedule Type&nbsp;&nbsp;</span> </td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left" >
                                            <asp:RadioButton ID="RadBDeduction" CssClass="headFont" runat="server"
                                                onchange='deduction();' Checked="false" ClientIDMode="Static" AutoPostBack="false"
                                                Text="Deduction"></asp:RadioButton>
                                            <asp:RadioButton ID="RadBLoan" CssClass="headFont" runat="server" AutoPostBack="false"
                                                onchange='loan();' Checked="false" ClientIDMode="Static" Text="Loan"></asp:RadioButton>
                                            <asp:RadioButton ID="RadBoth" CssClass="headFont" runat="server" AutoPostBack="false"
                                                Checked="false" ClientIDMode="Static" Text="Both"></asp:RadioButton>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px; width:100px;" ><span class="headFont">Select Schedule &nbsp;&nbsp;</span></td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:DropDownList ID="ddlSched" Width="217px" Height="25px" runat="server" DataValueField="EDID"
                                                DataTextField="EDName" Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="Select Schedule" Selected="True" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlBoth" Width="217px" Height="25px" runat="server" DataValueField="EDID" autocomplete="off"
                                                DataTextField="EDName" Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                ClientIDMode="Static">
                                                <asp:ListItem Value="0">Select Report Type</asp:ListItem>
                                                <asp:ListItem Value="1"> PF</asp:ListItem>
                                                <asp:ListItem Value="2"> CPF</asp:ListItem>
                                                <asp:ListItem Value="3"> CO-OP</asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                    </tr>
                                    <tr>
                                                <td style="padding: 5px; width: 200px;" id="TotalEmp"><span class="headFont">Emplyee Type &nbsp;&nbsp;</span></td>
                                                <td style="padding: 5px;" id="TotalValue">:</td>
                                                <td style="padding: 5px;" align="left" id="Total">
                                                    <asp:DropDownList ID="ddlEmpType" Width="217px" Height="25px" runat="server" DataValueField="EDID" autocomplete="off" DataTextField="EDName" Enabled="true" AppendDataBoundItems="true" CssClass="textbox"
                                                        ClientIDMode="Static">
                                                        <asp:ListItem Value="N">Select Employee Type</asp:ListItem>
                                                        <asp:ListItem Value="K"> WBFC</asp:ListItem>
                                                        <asp:ListItem Value="S"> State</asp:ListItem>
                                                        <asp:ListItem Value="C"> Central</asp:ListItem>
                                                        <%--<asp:ListItem Value="A"> ALL</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </td>
                                    </tr>

                                    <tr>
                                        <td style="padding: 5px; vertical-align: top; margin-left: 150px; width:150px;">
                                            <asp:RadioButton ID="rdPDF" Checked="true" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="PDF" />&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdExcel" class="headFont" runat="server" Font-Size="Medium" GroupName="a" Text="Excel" Checked="false" />
                                        </td>
                                    </tr>
                                </table>
                           
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        
                                        <asp:Button ID="cmdPrint" runat="server" Text="Show"   
                                        Width="100px" CssClass="Btnclassname" OnClientClick="opentab(); return false;" /> 
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>

                                      
                                        
                                    </div>
                                        <div class="loading-overlay">
                                        <div class="loadwrapper">
                                        <div class="ajax-loader-outer">Loading...</div>
                                        </div>
                                        </div>
                                    </td>
                                   
                                </tr>
                                
                                </table>
                           
                    
                        </td>
                    </tr>
                      
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

