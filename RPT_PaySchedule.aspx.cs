﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;             
using System.Web.Script;
using System.Web.Script.Serialization;          
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;

public partial class RPT_PaySchedule : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            RadBDeduction.Checked = true;         

            if (!IsPostBack)          
            {
                if (Session[SiteConstants.SSN_INT_USER_ID] != null)
                {
                    if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
                    {
                        PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
                    }

                }
          
            }


        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

        protected void cmdReset_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetSalMonth(string SalFinYear, int SectorId)
    {
        //int SectorID = 1;
        //DataSet ds1 = DBHandler.GetResults("Get_SalPayYear", SalFinYear); --> This coding is current Month display from Get_SalPayYear
        DataSet ds1 = DBHandler.GetResults("Get_SalPayYear", SalFinYear, SectorId);
        DataTable dt = ds1.Tables[0]; DataTable dtSalMonth = ds1.Tables[0];

        List<CenterbySector> listCenter = new List<CenterbySector>();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CenterbySector objst = new CenterbySector();
                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                listCenter.Insert(i, objst);
            }
        }
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listCenter);

    }

    public class CenterbySector
    {
        public string PayMonths { get; set; }   
    }

    protected void GenerateReport()
    {
        string SectorID = ((DropDownList)this.Master.FindControl("ddlSector")).SelectedValue;

        string SalMonthID = hdnSalFinYear.Value;  //hdnSalMonthID.Value;
        DataTable dtcr = DBHandler.GetResult("get_payslip_new",SectorID,SalMonthID);
         
        // getting value according to imageID and fill dataset

        ReportDocument crystalReport = new ReportDocument(); // creating object of crystal report
        crystalReport.Load(Server.MapPath("~/Reports/PaySlip_new.rpt")); // path of report
        crystalReport.SetDatabaseLogon("kmda", "infotech", "saswati-pc", "db_kmda",true);
        crystalReport.VerifyDatabase();
       // crystalReport.SetDataSource(dtcr); // binding datatable
        crystalReport.RecordSelectionFormula = "{tmp_payslip_monthly.SecID}=" +1+ "";
        crystalReport.ReadRecords();
        crystalReport.Refresh();
        //crystalReport.Load();

        ExportOptions exportOpts = new ExportOptions();
        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
          //  crystalldeExportOptions.CreateDiskFileDestinationOptions();
        PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();

       // exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
        //exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
        string FOLDERNAME = "Reports_PDF/PS"+SectorID+SalMonthID+Session[SiteConstants.SSN_INT_USER_ID]+".pdf";
        string PATH = Context.Server.MapPath("~/" + FOLDERNAME);
        diskOpts.DiskFileName = PATH;
        //exportOpts.ExportDestinationOptions = diskOpts;

        //Report.Export(exportOpts);
        exportOpts = crystalReport.ExportOptions;
        {
            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
            exportOpts.DestinationOptions = diskOpts;
            exportOpts.FormatOptions = CrFormatTypeOptions;
        }
        crystalReport.Export();
        
        //str1 += "<a target='_blank' href='" + FOLDERNAME + "'>Download</a>";
             
    }
    
    [WebMethod]
    public static string GetCenter()
    {
        DataSet ds = DBHandler.GetResults("Get_Center");
        DataTable dt = ds.Tables[0];

        List<Center> listCenter = new List<Center>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Center objst = new Center();

                objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);

                listCenter.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listCenter);

    }
    public class Center
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
    }

         [WebMethod]
    public static string GetLoan()
    {
         
        //String SchType;
        //SchType = "Loan";
        String ReportType;
        int parentID;
        parentID = 2;
        ReportType = "L";
        //DataSet ds = DBHandler.GetResults("Get_ScheduleType", SchType);
        DataSet ds = DBHandler.GetResults("Load_ChildPayrollReport", parentID, ReportType);
        DataTable dt = ds.Tables[0];

        List<ScheduleLoan> listLoan = new List<ScheduleLoan>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ScheduleLoan objst = new ScheduleLoan();

                objst.EDID = dt.Rows[i]["ReportID"].ToString();
                objst.EDName = Convert.ToString(dt.Rows[i]["ReportName"]);

                listLoan.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listLoan);

    }
    public class ScheduleLoan
    {
        public string EDID { get; set; }
        public string EDName { get; set; }
    }


    [WebMethod]
    public static string GetDeduction()
    {
        String ReportType;
        int parentID;
        parentID = 2;
        ReportType = "D";
        //DataSet ds = DBHandler.GetResults("Get_ScheduleType", SchType);
        DataSet ds = DBHandler.GetResults("Load_ChildPayrollReport", parentID, ReportType);
        DataTable dt = ds.Tables[0];

        List<ScheduleDeductionChi> listDeduction = new List<ScheduleDeductionChi>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ScheduleDeductionChi objst = new ScheduleDeductionChi();

                objst.ReportID = dt.Rows[i]["ReportID"].ToString();
                objst.ReportName = Convert.ToString(dt.Rows[i]["ReportName"]);

                listDeduction.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listDeduction);

    }
    public class ScheduleDeduction
    {
        public string EDID { get; set; }
        public string EDName { get; set; }
    }
    public class ScheduleDeductionChi
    {
        public string ReportID { get; set; }
        public string ReportName { get; set; }
    }
    
        [WebMethod]
        public static string GetSecByCent(int SecID)
        {
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID);
            DataTable dt = ds.Tables[0];
            List<SectbyCent> listSectoCent = new List<SectbyCent>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SectbyCent objst = new SectbyCent();

                    objst.CenterID = Convert.ToInt32(dt.Rows[i]["CenterID"]);
                    objst.CenterName = Convert.ToString(dt.Rows[i]["CenterName"]);

                    listSectoCent.Insert(i, objst);
                }
            }
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(listSectoCent);

        }
        public class SectbyCent
        {          
            public int CenterID { get; set; }
            public string CenterName { get; set; }
         }


        protected void PopulateCenter(int SecID, string SalFinYear)
        {
            try
            {
                string SecId = "";
                DataSet dsLocation = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
                DataTable dtSector = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
                SecId = dtSector.Rows[0]["SectorId"].ToString();
                if (dtSector.Rows.Count == 1)
                {
                    ddlSector1.Enabled = true;
                    ddlSector1.DataSource = dtSector;
                    ddlSector1.DataBind();
                    ddlSector1.SelectedIndex = 1;
                    ChkSectorAll.Checked = false;

                    ddlCenter.DataSource = dsLocation.Tables[0];
                    ddlCenter.DataTextField = "CenterName";
                    ddlCenter.DataValueField = "CenterID";
                    ddlCenter.DataBind();
                    ddlCenter.SelectedValue = "0";
                    
                    DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth", Convert.ToInt32(SecId), SalFinYear);
                    if (dtSalMonth.Rows.Count > 0)
                    {
                        txtCurrMonth.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
                        hdnCurMonth.Value = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();                        
                    }
                    
                }
                else
                {
                    ChkSectorAll.Checked = true;
                    ddlSector1.SelectedIndex =0;
                    ddlSector1.Enabled = false;
                    ddlCenter.Text = "Select Location";
                    if (dtSector.Rows.Count > 0)
                    {
                        ddlSector1.DataSource = dtSector;
                        ddlSector1.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }          

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string Report_Paravalue(int SalmonthID,string SalaryFinYear, string SalMonth, int SectorID, int CenterID, string EmpType, string Status, int EDID, int ChildreportID, string reportHisCur, int isPDFExcel)
        {
            String Msg = "";
            int UserID;
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
           // DataSet dtcr = DBHandler.GetResults("Get_PaySchedule", SalaryFinYear, SalMonth, SectorID, CenterID, EmpType, Status, EDID, SectorGroup, CenterGroup, EmpTypeGroup, StatusGroup, SchType, UserID);
            DataSet dtcr = DBHandler.GetResults("Load_reportDynamic", SalmonthID, SalMonth, SalaryFinYear, SectorID, UserID, ChildreportID, CenterID, EmpType, Status, EDID, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, isPDFExcel, reportHisCur);
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(Msg);

        }
       

}