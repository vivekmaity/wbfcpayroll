﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;           
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;          
using System.IO;                                     
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class AdminScheduleReport : System.Web.UI.Page
{
    public string str1 = "";
    public string strSalFin = "";
    static string StrFormula = "";
    public static string  StrSalMonID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        strSalFin = Convert.ToString(Session[SiteConstants.SSN_SALFIN]);
        //ddlReportType.SelectedIndex = 1;
        DataSet ds = DBHandler.GetResults("Get_SalPayYear_WithoutSec_GPF5",strSalFin);
        DataTable dt = ds.Tables[0];
        DataTable dtSalMonth = ds.Tables[0];
        DataTable dtSalMonthID = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            txtPayMonth.Text = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
            StrSalMonID = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonthID"]);
        }
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)                   
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string GetDeduction()
    {
        String SchType;
        SchType = "Deduction";
        DataSet ds = DBHandler.GetResults("Get_ScheduleType5", SchType);
        DataTable dt = ds.Tables[0];

        List<ScheduleDeduction> listDeduction = new List<ScheduleDeduction>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ScheduleDeduction objst = new ScheduleDeduction();

                objst.EDID = Convert.ToInt32(dt.Rows[i]["EDID"]);
                objst.EDName = Convert.ToString(dt.Rows[i]["EDName"]);

                listDeduction.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listDeduction);

    }
    public class ScheduleDeduction
    {
        public int EDID { get; set; }
        public string EDName { get; set; }
    }

    [WebMethod]
    public static string GetLoan()
    {

        String SchType;
        SchType = "Loan";
        DataSet ds = DBHandler.GetResults("Get_ScheduleType5", SchType);
        DataTable dt = ds.Tables[0];

        List<ScheduleLoan> listLoan = new List<ScheduleLoan>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ScheduleLoan objst = new ScheduleLoan();

                objst.EDID = Convert.ToInt32(dt.Rows[i]["EDID"]);
                objst.EDName = Convert.ToString(dt.Rows[i]["EDName"]);

                listLoan.Insert(i, objst);
            }
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listLoan);

    }
    public class ScheduleLoan
    {
        public int EDID { get; set; }
        public string EDName { get; set; }
    }

    [WebMethod(EnableSession = true)]       
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(string SalMonth, int SalMonthID, int StrSchedule, string SalFinYear, string StrEmpType, string FormName, string ReportName, string ReportType)
    {
        String Msg = "";            
        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        if (ReportName == "Pay_Schedule_PTAX_Admin")
        {
            StrFormula = "{DAT_MonthlySalary.EDID}= 5 and {MST_SalaryMonth.SalMonthID}=" + StrSalMonID;
        }
        else if (ReportName == "Pay_Schedule_MICE_Admin")
        {

            StrFormula = "{DAT_MonthlySalary.EDID}=" + StrSchedule + " and {MST_SalaryMonth.SalMonthID}=" + StrSalMonID;
        }
        else if (ReportName == "Pay_Schedule_ITAX_Admin")
        {
            StrFormula = "{DAT_MonthlySalary.EDID}= 1 and {MST_SalaryMonth.SalMonthID}=" + StrSalMonID;
        }
        else if (ReportName == "Pay_Schedule_LOAN_Admin")
        {

            StrFormula = "{DAT_MonthlySalary.EDID}=" + StrSchedule + " and {MST_SalaryMonth.SalMonthID}=" + StrSalMonID;
        }
        if (ReportName == "AdminAllsector_COOP") {
             int Dedu_EDID=22;
             int Loan_EDID=315;

             DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF", SalFinYear, StrSalMonID, StrEmpType, Dedu_EDID, Loan_EDID, UserID);
             StrFormula = "{RPT_AdminAllsector_COOP_PF_CPF.UserID}=" + UserID;
        }
        if (ReportName == "AdminAllsector_CPF_all_PF_state")
        {
            int Dedu_EDID = 14;
            int Loan_EDID = 311;

            //if (StrSchedule == 1)
            //{
            //    Dedu_EDID = 14;
            //    Loan_EDID = 311;
            //}
            if (StrSchedule == 2)
            {
                Dedu_EDID = 33;
                Loan_EDID = 311;
            }

            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF", SalFinYear, StrSalMonID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            StrFormula = "{RPT_AdminAllsector_COOP_PF_CPF.UserID}=" + UserID;
        }
        

        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Count_Excel(string SalMonth, int SalMonthID,int StrSchedule,string SalFinYear,string StrEmpType,string ReportName)
    {

        int UserID;
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        string jsval = ""; string rowscount = "";
        if (ReportName == "Pay_Schedule_ITAX_Admin")
        {
            DataTable dt = DBHandler.GetResult("Load_AdminScheduleItaxExcel", StrSalMonID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }
        if (ReportName == "Pay_Schedule_PTAX_Admin")
        {
            DataTable dt = DBHandler.GetResult("Load_AdminSchedulePtaxExcel", StrSalMonID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }
        if (ReportName == "Pay_Schedule_MICE_Admin")
        {
            DataTable dt = DBHandler.GetResult("Load_AdminSchedulereportExcel", StrSalMonID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }
        if (ReportName == "Pay_Schedule_LOAN_Admin")
        {
            DataTable dt = DBHandler.GetResult("Load_AdminScheduleLoanExcel", StrSalMonID, StrSchedule);
            rowscount = dt.Rows.Count.ToJSON();
        }
        if (ReportName == "AdminAllsector_COOP")
        {
            int Dedu_EDID = 22;
            int Loan_EDID = 315;

            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF", SalFinYear, StrSalMonID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCOOP", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            rowscount = dt.Rows.Count.ToJSON();
        }
        if (ReportName == "AdminAllsector_CPF_all_PF_state")
        {
            int Dedu_EDID = 14;
            int Loan_EDID = 311;


            if (StrSchedule == 2)
            {
                Dedu_EDID = 33;
                Loan_EDID = 311;
            }

            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_AdminAllsector_COOP_PF_CPF", SalFinYear, StrSalMonID, StrEmpType, Dedu_EDID, Loan_EDID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable dt = DBHandler.GetResult("Load_AdminAllsectorCPFPF", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            rowscount = dt.Rows.Count.ToJSON();
        }
        jsval = rowscount.ToJSON();
        return jsval;
    }

}