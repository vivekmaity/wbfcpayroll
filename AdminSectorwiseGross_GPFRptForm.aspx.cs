﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;           
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;          
using System.IO;                                     
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class AdminSectorwiseGross_GPFRptForm : System.Web.UI.Page
{
    public string str1 = "";
    public string strSalFin = "";
    public static string strSalmonth = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        strSalFin = Convert.ToString(Session[SiteConstants.SSN_SALFIN]);
        DataSet ds = DBHandler.GetResults("Get_SalPayYear_WithoutSec_GPF",strSalFin);
        DataTable dt = ds.Tables[0];
        DataTable dtSalMonth = ds.Tables[0];
        DataTable dtSalMonthID = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            txtPayMonth.Text = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
            strSalmonth = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonthID"]);
        }
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)                   
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]       
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(int SalMonthID, string SalMonth, string CPFORGPF, string PFCode)
    {
        String Msg = "";
        DataTable dtcr = DBHandler.GetResult("Get_GPF_Summary", SalMonth, PFCode, CPFORGPF, strSalmonth);
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);
    }

}