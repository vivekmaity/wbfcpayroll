﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="EmployeeWisePayReport.aspx.cs" Inherits="EmployeeWisePayReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link type="text/css" href="css/Gridstyle.css" rel="stylesheet" />
    <script src="js/ValidationTextBox.js"></script>
   <%-- <link type="text/css" href="css/GridvewColorchange.css" rel="stylesheet" />--%>
    
    <script type="text/javascript">
        var ParameterTypes = "P";
        var final = '';
        $(document).ready(function () {
            $("#ddlSector").change(function () {
                $('#lbEmpName').text("");
                $('#lDOB').text("");
                $('#lSector').text("");
                $('#lDesignation').text("");
                $('#lPfNo').text("");
                $('#lLocation').text("");
                $('#lPayScale').text("");
                $('#txtEmpCodeSearch').val("");
                $("#tbl").empty();
                $("#tbl").hide();
                $('#lbEmpNo').text("");
                $('#lPAN').text("");
                $('#Radi').hide();
                $('#lDOR').text("");
                

            });
        });
        $(document).ready(function () {
            $('#kmdaLogo').hide();
            $('#KmdaSign').hide();
            $('#Gen').hide();
            $('#btnProjected').hide();
            $('#btnOrginal').hide();
            $('#ldate').hide();
            $('#Radi').hide();
            


            
            //var today = new Date();
            //alert(today);
            //$('#ldate').text(today);

                
            
        });
        $(document).ready(function () {
            $('#rdPDF').click(function () {
                if (document.getElementById('rdPDF').checked) {
                    ParameterTypes = "P";
                }
            });

            $('#rdExcel').click(function () {
                if (document.getElementById('rdExcel').checked) {
                    ParameterTypes = "E";
                }
            });
        });

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        function beforeSave_Search() {
            $("#form1").validate();
            $("#txtEmpCodeSearch").rules("add", { required: true, messages: { required: "Please enter Employee No." } });
            $('#btnProjected').hide();
            $('#btnOrginal').hide();
           
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        $(document).ready(function () {
            $("#txtEmpCodeSearch").keydown(function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                        $("#Span1").html("Digits Only").show().fadeOut(1500);
                        return false;
                    }
                }
            });
        });
        function last()
        {
            var t = final;
            $("#tbl").empty();
            $("#tbl").append("<tr><th>Month</th><th>Gross Pay</th><th>Ptax</th><th>Itax</th><th>GIS</th><th>HRA</th><th>GPF</th><th>GPF Recover</th><th>HBL(Principal)</th><th>HBL(Interest)</th><th>Other Deduction</th><th>Net Pay</th><th>Arrear/OT/Bonus</th><th>Status</th><th>Pay Slip</th></tr>");
            $("#gridDiv").show();
            var Gross = 0;
            var Ptax = 0;
            var Itax = 0;
            var GPF = 0;
            var Netpay = 0;
            var IHBL = 0;
            var GPFRecov = 0;
            var HBL = 0;
            var ArrearBonus = 0;
            var HRA = 0;
            var OtherDedu = 0;
            var GIS = 0;
            a = t.length;
            //alert(a);
            for (var i = 0; i < t.length; i++) {
                var status = t[i].Status;
                var sal = t[i].SalMonth;
                var empID = t[i].EmployeeID;

                //var Status = t[i].Status;
                if (status == "Projection") {
                    var re = "Unrealised";
                    $("#tbl").append("<tr style='background-color:#D3F9FA; height:30px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td></td></tr>");
                }
                else if (status == "H") {
                    var re = "Realised";
                    $("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                }
                else if (status == "C") {
                    // alert(status);
                    var re = "Completed and Bank Generated";
                    $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                }
                else if (status = "B") {
                    var re = 'Bank Not Yet Generated';
                    //alert(re);
                    //$("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GPF + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].IHBL + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                    $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                }
                if (t[i].GrossPay != "") {
                    Gross = parseInt(Gross) + parseInt(t[i].GrossPay);
                }
                if (t[i].Ptax != "") {
                    Ptax = parseInt(Ptax) + parseInt(t[i].Ptax);
                }

                if (t[i].Itax != "") {
                    Itax = parseInt(Itax) + parseInt(t[i].Itax);
                }
                if (t[i].GPF != "") {
                    GPF = parseInt(GPF) + parseInt(t[i].GPF);
                }
                if (t[i].NetPay != "") {
                    Netpay = parseInt(Netpay) + parseInt(t[i].NetPay);
                }
                if (t[i].IHBL != "") {
                    IHBL = parseInt(IHBL) + parseInt(t[i].IHBL);
                }
                if (t[i].GPFRecov != "") {
                    GPFRecov = parseInt(GPFRecov) + parseInt(t[i].GPFRecov);
                }
                if (t[i].HBL != "") {
                    HBL = parseInt(HBL) + parseInt(t[i].HBL);
                }
                if (t[i].ArrearOT != "") {
                    ArrearBonus = parseInt(ArrearBonus) + parseInt(t[i].ArrearOT);
                }
                if (t[i].HRA != "") {
                    HRA = parseInt(HRA) + parseInt(t[i].HRA);
                }
                if (t[i].OtherDedu != "") {
                    OtherDedu = parseInt(OtherDedu) + parseInt(t[i].OtherDedu);
                }
                if (t[i].GIS != "") {
                    GIS = parseInt(GIS) + parseInt(t[i].GIS);
                }


                if (i == t.length - 1) {
                    $("#tbl").append("<tr style='background-color:lightpink; height:30px;Font-Bold=true'><td><h2>Total</h2></td><td>" + Gross + "</td><td>" + Ptax + "</td><td>" + Itax + "</td><td>" + GIS + "</td><td>" + HRA + "</td><td>" + GPF + "</td><td>" + GPFRecov + "</td><td>" + HBL + "</td><td>" + IHBL + "</td><td>" + OtherDedu + "</td><td>" + Netpay + "</td><td>" + ArrearBonus + "</td><td></td><td></td></tr>");
                }
            }
        }

        function search() {
            //==========================
            //$('#lbEmpName').text("");
            //$('#lDOB').text("");
            ////$('#lSector').text("");
            //$('#lDesignation').text("");
            //$('#lPfNo').text("");
            //$('#lLocation').text("");
            //$('#lPayScale').text("");
            ////$('#txtEmpCodeSearch').val("");
            //$("#tbl").empty();
            //$("#tbl").hide();
            //$('#lbEmpNo').text("");
            //$('#lPAN').text("");
            //$('#Radi').hide();
            //$('#lSector').text("");
            //$('#lDOR').text("");
            //$("#gridDiv").hide();




            //====================

            $("#tbl").show();
            $('#btnProjected').show();
            $('#btnOrginal').show();
            $('#Radi').show();
            var EmpNo = $('#txtEmpCodeSearch').val().trim();
            var SecID = $('#ddlSector').val().trim();
            var isPrint = 'Yes';
            $('#txtEmpCodeSearch').attr('disabled', 'disabled');
          
            var type1 = '<%= Session["SiteConstants.SSN_SALACC"] %>';
            
            //alert(type1);
            if (SecID == "0") {
                alert("Please select Sector");
                $('#btnProjected').hide();
                $('#btnOrginal').hide();
                $('#ddlSector').focus();
                $('#rdPDF').hide();
                $('#rdExcel').hide();
                return false;
            }
            last();

            var E = "{SecID: " + $('#ddlSector').val() + ",EmpNo:'" + EmpNo + "'}";
            //alert(E);
            var options = {};
            options.url = "EmployeeWisePayReport.aspx/EmployeeDetails";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (Empdetails) {
                var t = jQuery.parseJSON(Empdetails.d);
                var a = t.length;
               
                if (a > 0) {
                    var EmpName = t[0]["EmpName"];
                    var DOB = t[0]["DOB"];
                    var SectorName = t[0]["SectorName"];
                    var Designation = t[0]["Designation"];
                    var PFCode = t[0]["PFCode"];
                    var CenterName = t[0]["CenterName"];
                    var PayScale = t[0]["PayScale"];
                    //alert(EmpName);
                    var printdate = t[0]["printdate"];
                    var DOR = t[0]["DOR"];
                    var PAN = t[0]["PAN"];

                    //alert(printdate);

                    $('#lbEmpName').text(EmpName);
                    $('#lDOB').text(DOB);
                    $('#lSector').text(SectorName);
                    $('#lDesignation').text(Designation);
                    $('#lPfNo').text(PFCode);
                    $('#lLocation').text(CenterName);
                    $('#lPayScale').text(PayScale);
                    $('#lbEmpNo').text(EmpNo);
                    $('#ldate').text(printdate);
                    $('#lDOR').text(DOR);
                    $('#lPAN').text(PAN);
                    PopulateGrid(SecID, EmpNo, isPrint);
                }
                else {
                    alert("Please select right sector or Emp No");
                    $('#txtEmpCodeSearch').val("");
                    $('#ddlSector').val("");
                    $('#txtEmpCodeSearch').attr('disabled', false);
                }
            };
            options.error = function () { alert("Error in  Location!"); };
            $.ajax(options);
        }

        function PopulateGrid(SecID, EmpNo, isPrint) {
           
            var a = "";
            var E = "{SecID: " + SecID + ",EmpNo:'" + EmpNo + "'}";
       
            $.ajax({
                type: "POST",
                url: "EmployeeWisePayReport.aspx/PopulateEmpGrid",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    var t = jQuery.parseJSON(msg.d);
                    final = t;
                    if (isPrint == "Yes") {
                        $("#tbl").empty();
                        $("#tbl").append("<tr><th>Month</th><th>Gross Pay</th><th>Ptax</th><th>Itax</th><th>GIS</th><th>HRA</th><th>GPF</th><th>GPF Recover</th><th>HBL(Principal)</th><th>HBL(Interest)</th><th>Other Deduction</th><th>Net Pay</th><th>Arrear/OT/Bonus</th><th>Status</th><th>Pay Slip</th></tr>");
                        $("#gridDiv").show();
                        var Gross = 0;
                        var Ptax = 0;
                        var Itax = 0;
                        var GPF = 0;
                        var Netpay = 0;
                        var IHBL = 0;
                        var GPFRecov = 0;
                        var HBL = 0;
                        var ArrearBonus = 0;
                        var HRA = 0;
                        var OtherDedu = 0;
                        var GIS = 0;
                        a = t.length;
                        //alert(a);
                        for (var i = 0; i < t.length; i++) {
                            var status = t[i].Status;
                            var sal = t[i].SalMonth;
                            var empID = t[i].EmployeeID;
                            if (status == "Projection") {
                                var re = "Unrealised";
                                $("#tbl").append("<tr style='background-color:#D3F9FA; height:30px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td></td></tr>");
                            }
                            else if (status == "H") {
                                var re = "Realised";
                                $("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                            }
                            else if (status == "C") {
                               // alert(status);
                                var re = "Completed and Bank Generated";
                                $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                            }
                            else if (status = "B") {
                                var re = 'Bank Not Yet Generated';
                                //alert(re);
                                //$("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GPF + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].IHBL + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                                $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                            }

                            if (t[i].GrossPay != "") {
                                Gross = parseInt(Gross) + parseInt(t[i].GrossPay);
                            }
                            if (t[i].Ptax != "") {
                                Ptax = parseInt(Ptax) + parseInt(t[i].Ptax);
                            }

                            if (t[i].Itax != "") {
                                Itax = parseInt(Itax) + parseInt(t[i].Itax);
                            }
                            if (t[i].GPF != "") {
                                GPF = parseInt(GPF) + parseInt(t[i].GPF);
                            }
                            if (t[i].NetPay != "") {
                                Netpay = parseInt(Netpay) + parseInt(t[i].NetPay);
                            }
                            if (t[i].IHBL != "") {
                                IHBL = parseInt(IHBL) + parseInt(t[i].IHBL);
                            }
                            if (t[i].GPFRecov != "") {
                                GPFRecov = parseInt(GPFRecov) + parseInt(t[i].GPFRecov);
                            }
                            if (t[i].HBL != "") {
                                HBL = parseInt(HBL) + parseInt(t[i].HBL);
                            }
                            if (t[i].ArrearOT != "") {
                                ArrearBonus = parseInt(ArrearBonus) + parseInt(t[i].ArrearOT);
                            }
                            if (t[i].HRA != "") {
                                HRA = parseInt(HRA) + parseInt(t[i].HRA);
                            }
                            if (t[i].OtherDedu != "") {
                                OtherDedu = parseInt(OtherDedu) + parseInt(t[i].OtherDedu);
                            }
                            if (t[i].GIS != "") {
                                GIS = parseInt(GIS) + parseInt(t[i].GIS);
                            }
                            if (i == t.length - 1) {
                                $("#tbl").append("<tr style='background-color:lightpink; height:30px;Font-Bold=true'><td style='font-weight:bold;' height='30px'>Total</td><td>" + Gross + "</td><td>" + Ptax + "</td><td>" + Itax + "</td><td>" + GIS + "</td><td>" + HRA + "</td><td>" + GPF + "</td><td>" + GPFRecov + "</td><td>" + HBL + "</td><td>" + IHBL + "</td><td>" + OtherDedu + "</td><td>" + Netpay + "</td><td>" + ArrearBonus + "</td><td></td><td></td></tr>");
                            }
                        }
                    }
                    else
                    {
                        $("#tbl").empty();
                        $("#tbl").append("<tr><th>Month</th><th>Gross Pay</th><th>Ptax</th><th>Itax</th><th>GIS</th><th>HRA</th><th>GPF</th><th>GPF Recover</th><th>HBL(Principal)</th><th>HBL(Interest)</th><th>Other Deduction</th><th>Net Pay</th><th>Arrear/OT/Bonus</th><th>Status</th></tr>");
                        $("#gridDiv").show();
                        var Gross = 0;
                        var Ptax = 0;
                        var Itax = 0;
                        var GPF = 0;
                        var Netpay = 0;
                        var IHBL = 0;
                        var GPFRecov = 0;
                        var HBL = 0;
                        var ArrearBonus = 0;
                        var HRA = 0;
                        var OtherDedu = 0;
                        var GIS = 0;
                        a = t.length;
                        //alert(a);
                        for (var i = 0; i < t.length; i++) {
                            var status = t[i].Status;
                            var sal = t[i].SalMonth;
                            var empID = t[i].EmployeeID;
                            if (status == "Projection") {
                                var re = "Unrealised";
                                $("#tbl").append("<tr style='background-color:#D3F9FA; height:30px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td></td></tr>");
                            }
                            else if (status == "H") {
                                var re = "Realised";
                                $("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                            }
                            else if (status == "C") {
                                // alert(status);
                                var re = "Completed and Bank Generated";
                                $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                            }
                            else if (status = "B") {
                                var re = 'Bank Not Yet Generated';
                                //alert(re);
                                //$("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GPF + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].IHBL + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                                $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                            }
                            if (t[i].GrossPay != "") {
                                Gross = parseInt(Gross) + parseInt(t[i].GrossPay);
                            }
                            if (t[i].Ptax != "") {
                                Ptax = parseInt(Ptax) + parseInt(t[i].Ptax);
                            }

                            if (t[i].Itax != "") {
                                Itax = parseInt(Itax) + parseInt(t[i].Itax);
                            }
                            if (t[i].GPF != "") {
                                GPF = parseInt(GPF) + parseInt(t[i].GPF);
                            }
                            if (t[i].NetPay != "") {
                                Netpay = parseInt(Netpay) + parseInt(t[i].NetPay);
                            }
                            if (t[i].IHBL != "") {
                                IHBL = parseInt(IHBL) + parseInt(t[i].IHBL);
                            }
                            if (t[i].GPFRecov != "") {
                                GPFRecov = parseInt(GPFRecov) + parseInt(t[i].GPFRecov);
                            }
                            if (t[i].HBL != "") {
                                HBL = parseInt(HBL) + parseInt(t[i].HBL);
                            }
                            if (t[i].ArrearOT != "") {
                                ArrearBonus = parseInt(ArrearBonus) + parseInt(t[i].ArrearOT);
                            }
                            if (t[i].HRA != "") {
                                HRA = parseInt(HRA) + parseInt(t[i].HRA);
                            }
                            if (t[i].OtherDedu != "") {
                                OtherDedu = parseInt(OtherDedu) + parseInt(t[i].OtherDedu);
                            }
                            if (t[i].GIS != "") {
                                GIS = parseInt(GIS) + parseInt(t[i].GIS);
                            }


                            if (i == t.length - 1) {
                                $("#tbl").append("<tr style='background-color:lightpink; height:30px;Font-weight:bold'><td style='font-weight:bold'>Total</td><td>" + Gross + "</td><td>" + Ptax + "</td><td>" + Itax + "</td><td>" + GIS + "</td><td>" + HRA + "</td><td>" + GPF + "</td><td>" + GPFRecov + "</td><td>" + HBL + "</td><td>" + IHBL + "</td><td>" + OtherDedu + "</td><td>" + Netpay + "</td><td>" + ArrearBonus + "</td><td></td><td></td></tr>");
                            }
                        }

                        $('#kmdaLogo').show();
                        $('#KmdaSign').show();
                        $('#Gen').show();
                        $('#ldate').show();
                        $('#Radi').hide();
                        document.getElementById("spnEmpNo").style.visibility = "hidden";
                        document.getElementById("spnRequire").style.visibility = "hidden";
                        document.getElementById("spnColon").style.visibility = "hidden";
                        document.getElementById("txtEmpCodeSearch").style.visibility = "hidden";
                        document.getElementById("cmdSearch").style.visibility = "hidden";
                        //document.getElementById("btnViewTotCalculation").style.visibility = "hidden";
                        //$("#Itaxtype a").hide();
                        document.getElementById("btnProjected").style.visibility = "hidden";
                        document.getElementById("btnOrginal").style.visibility = "hidden";
                       
                        var divContents = document.getElementById("tblEmp").innerHTML;
                        search();
                        var printWindow = window.open('', '', 'height=500,width=950');
                        printWindow.document.write('<html><head><title>DIV Contents</title>');
                        printWindow.document.write('</head><body >');
                        printWindow.document.write(divContents);
                        printWindow.document.write('</body></html>');
                        printWindow.document.close();
                       
                       
                        $('#kmdaLogo').hide();
                        $('#KmdaSign').hide();
                        $('#Gen').hide();
                        $('#ldate').hide();
                        document.getElementById("spnEmpNo").style.visibility = "visible";
                        document.getElementById("spnRequire").style.visibility = "visible";
                        document.getElementById("spnColon").style.visibility = "visible";
                        document.getElementById("txtEmpCodeSearch").style.visibility = "visible";
                        document.getElementById("cmdSearch").style.visibility = "visible";
                        //document.getElementById("btnViewTotCalculation").style.visibility = "hidden";
                        //$("#Itaxtype a").hide();
                        document.getElementById("btnProjected").style.visibility = "visible";
                        document.getElementById("btnOrginal").style.visibility = "visible";


                       
                        printWindow.print();
                        

                    }
                   
                }
            });
        }
        
  
        function PopulateTotal(SecID, EmpNo) {
            var E = "{SecID: " + SecID + ",EmpNo:'" + EmpNo + "'}";
            //alert(E);
            $.ajax({
                type: "POST",
                url: "EmployeeWisePayReport.aspx/PopulateTotalGrid",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (EmpGrdTotal) {
                    var t = jQuery.parseJSON(EmpGrdTotal.d);
                    var a = t.length;
                    //alert(a);
                    if (a > 0) {
                        var TotalGross = t[0]["TotalGross"];
                        var TotalPtax = t[0]["TotalPtax"];
                        var TotalItax = t[0]["TotalItax"];
                        var TotalGPF = t[0]["TotalGPF"];
                        var TotalNetPay = t[0]["TotalNetPay"];
                        var TotalIHBL = t[0]["TotalIHBL"];
                        var TotalHBL = t[0]["TotalHBL"];
                        var TotalGPFRecover = t[0]["TotalGPFRecover"];
                        var TotalArrearOT = t[0]["TotalArrearOT"];
                       

                        $('#lTotalGross').text(TotalGross);
                        $('#lTotalPtax').text(TotalPtax);
                        $('#lTotalItax').text(TotalItax);
                        $('#lTotalGPF').text(TotalGPF);
                        $('#lTotalNetPay').text(TotalNetPay);
                        $('#lTotalIHBL').text(TotalIHBL);
                       
                    }
                    else {
                       
                    }
                }
            });
        }

       
        function GenReport(EmpID, SalMonth, status) {
           
                var FormName = '';
                var ReportName = '';
                var ReportType = '';
                var StrFinYr = '';
                var StrSecID;
                var StrSalMonth;
                var StrSalMonthID;
                var StrCenID;
                var StrReportTyp = '';
                ReportName = "History_PaySlip_new_LocationWISE_A5";
                var EmployeeID = EmpID;
                var SalMonth = SalMonth;
                var Status = status;
                var SecID = $('#ddlSector').val()
                FormName = "EmployeeWisePayReport.aspx";

                ReportType = "PaySlipReport";
                $(".loading-overlay").show();
                var E = '';
                E = "{EmployeeID:'" + EmployeeID + "',SalMonth:'" + SalMonth + "',Status:'" + Status + "',SecID:'" + SecID + "', FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                //alert(E);
                $.ajax({

                    type: "POST",
                    url: "EmployeeWisePayReport.aspx/Report_Paravalue_Payslip",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        $(".loading-overlay").hide();
                        var left = ($(window).width() / 2) - (900 / 2),
                       top = ($(window).height() / 2) - (600 / 2),
                       popup = window.open("ReportView1.aspx", "popup", "width=900, height=600, top=" + top + ", left=" + left);
                        popup.focus();
                    }
                });

            }


        

        function projected() {
            var SecID = $('#ddlSector').val().trim();
            var EmpNo = $('#txtEmpCodeSearch').val().trim();
            var FinYR = $("#txtSalFinYear").val();
            var PenMonth;
            var CurrMonth;
            var Reporttype;
            var ReportName = '';
            var ReportType = '';
            var StrReportTyp = '';
            FormName = "EmployeeWisePayReport.aspx";
            ReportName = "Salary_Projection_Report";
            ReportType = "Salary Projection Report";
            StrReportTyp = 1;
            if (ParameterTypes == "P") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";
                if (confirm("Do you want to view the report ?")) {
                    confirm_value.value = "Yes";
                    $(".loading-overlay").show();
                    var E = "{EmpNo:'" + EmpNo + "',SecID:" + SecID + ",StrReportTyp:" + StrReportTyp + ",FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                    // alert(E);
                    $.ajax({
                        type: "POST",
                        url: "EmployeeWisePayReport.aspx/Report_Paravalue",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            jsmsg = JSON.parse(msg.d);
                            $(".loading-overlay").hide();
                            //window.open("ReportView1.aspx?");
                            var left = ($(window).width() / 2) - (900 / 2),
                            top = ($(window).height() / 2) - (600 / 2),
                            popup = window.open("ReportView1.aspx", "popup", "width=900, height=600, top=" + top + ", left=" + left);
                            popup.focus();
                        }

                    });
                } else {
                    confirm_value.value = "No";

                }
            }
            else {
                //StrPaymentDate = $("#ddlPaymentDate").val();
                
                //ReportName = "ProjectedSalStatement";
               var E = "{SecID:" + SecID + ",EmpNo:'" + EmpNo + "',FinYR:'" + FinYR + "',ReportName:'" + ReportName + "'}";
               // alert(E);
                $.ajax({

                    type: "POST",
                    url: "EmployeeWisePayReport.aspx/Count_Excel",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        //alert(JSON.stringify(jsmsg));
                        //alert("HI");
                        if (jsmsg > 0)
                            window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "&SecID=" + SecID + "&EmpNo=" + EmpNo +"&FinYr=" +FinYR);
                        else {
                            alert("No Records found");
                        }
                    }
                });
            }
           
        }

       
        function Orginal() {
            var SecID = $('#ddlSector').val().trim();
            var EmpNo = $('#txtEmpCodeSearch').val().trim();
            var FinYR = $("#txtSalFinYear").val();
            var PenMonth;
            var CurrMonth;
            var Reporttype;
            var ReportName = '';
            var ReportType = '';
            var StrReportTyp = '';
            FormName = "EmployeeWisePayReport.aspx";
            ReportName = "EMP_Orginal_Sal_Statement";
            ReportType = "Emp Orginal Sal Satement";
            StrReportTyp = 2;
            if (ParameterTypes == "P") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";
                if (confirm("Do you want to view the report ?")) {
                    confirm_value.value = "Yes";
                    $(".loading-overlay").show();
                    var E = "{EmpNo:'" + EmpNo + "',SecID:" + SecID + ",StrReportTyp:" + StrReportTyp + ",FormName:'" + FormName + "',ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
                    //alert(E);
                    $.ajax({
                        type: "POST",
                        url: "EmployeeWisePayReport.aspx/Report_Paravalue",
                        data: E,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            jsmsg = JSON.parse(msg.d);
                            $(".loading-overlay").hide();
                            //window.open("ReportView1.aspx?");
                            var left = ($(window).width() / 2) - (900 / 2),
                            top = ($(window).height() / 2) - (600 / 2),
                            popup = window.open("ReportView1.aspx", "popup", "width=900, height=600, top=" + top + ", left=" + left);
                            popup.focus();
                        }

                    });
                } else {
                    confirm_value.value = "No";

                }
            }
            else {
               //alert('hi')
               var E = "{SecID:" + SecID + ",EmpNo:'" + EmpNo + "',FinYR:'" + FinYR + "',ReportName:'" + ReportName + "'}";
               // alert(E);
                $.ajax({

                    type: "POST",
                    url: "EmployeeWisePayReport.aspx/Count_Excel",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        jsmsg = JSON.parse(msg.d);
                        //alert(JSON.stringify(jsmsg));
                        //alert("HI");
                        if (jsmsg > 0)
                            window.open("AllReportinExcel.aspx?ReportName=" + ReportName + "&SecID=" + SecID + "&EmpNo=" + EmpNo + "&FinYr=" + FinYR);
                        else {
                            alert("No Records found");
                        }
                    }
                });
            }
            
        }
        function PopulateGridForPrint() {
            var SecID = $('#ddlSector').val().trim();
            var EmpNo = $('#txtEmpCodeSearch').val().trim();
           // alert('print');
            var a = "";
            var E = "{SecID: " + SecID + ",EmpNo:'" + EmpNo + "'}";
           // alert(E);
            $.ajax({
                type: "POST",
                url: "EmployeeWisePayReport.aspx/PopulateEmpGrid",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    var t = jQuery.parseJSON(msg.d);
                    $("#tbl").empty();
                    $("#tbl").append("<tr><th>Month</th><th>Gross Pay</th><th>Arrear/OT/Bonus</th><th>Ptax</th><th>Itax</th><th>GIS</th><th>HRA</th><th>GPF</th><th>GPF Recover</th><th>HBL(Principal)</th><th>HBL(Interest)</th><th>Other Deduction</th><th>Net Pay</th><th>Status</th><th>Pay Slip</th></tr>");
                    $("#gridDiv").show();
                    
                    for (var i = 0; i < t.length; i++) {
                        var status = t[i].Status;
                        var sal = t[i].SalMonth;
                        var empID = t[i].EmployeeID;
                        if (status == "Projection") {
                            var re = "Unrealised";
                            $("#tbl").append("<tr style='background-color:#D3F9FA; height:30px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td></td></tr>");
                        }
                        else if (status == "H") {
                            var re = "Realised";
                            $("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                        }
                        else if (status == "C") {
                            // alert(status);
                            var re = "Completed and Bank Generated";
                            $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                        }
                        else if (status = "B") {
                            var re = 'Bank Not Yet Generated';
                            //alert(re);
                            //$("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GPF + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].IHBL + "</td><td>" + re + "</td><td><img src='img/print.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                            $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td><img src='img/print_new.jpg' onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                        }
                    }
                    //HideandShow();
                    PopulateTotal(SecID, EmpNo);

                }
            });
        }

        //This is for Print 
        function PrintDiv() {
           
            var EmpNo = $('#txtEmpCodeSearch').val().trim();
            var SecID = $('#ddlSector').val().trim();
            var isPrint = 'No';
            PopulateGrid(SecID, EmpNo, isPrint);
         
        }
    </script>
    <style type="text/css">
        .style1 {
            width: 77px;
        }

        .style2 {
            width: 129px;
        }

        .style3 {
            width: 88px;
        }

        .style4 {
            width: 59px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">
                    <section>
                        <h2>Employee Wise Pay Report</h2>
                    </section>

                    <table id="tblEmp" width="100%" style="border: solid 2px lightblue;" bgcolor="#669999">
                        <tr>
                            <td style="padding: 15px;" bgcolor="White">

                                <table align="center" width="98%">
                                    <tr>
                                        <td style="padding: 5px;"><span class="headFont" style="font-size: 16px;" id="spnEmpNo">Employee No.&nbsp;&nbsp;</span><span id="spnRequire" class="require">*</span> </td>
                                        <td style="padding: 5px;" id="spnColon">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:TextBox ID="txtEmpCodeSearch" ClientIDMode="Static" oncopy="return true" onpaste="return true" oncut="return true"
                                                runat="server" MaxLength="6" CssClass="textbox" Width="60px" autocomplete="off"></asp:TextBox>
                                        </td>
                                        <td style="padding: 5px;">

                                            <div style="float: left;" align="left">
                                                <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="Search" OnClick="cmdSearch_Click" 
                                                    Width="100px" CssClass="Btnclassname" OnClientClick="search();return false;" />
                                            </div>
                                           </td>
                                       
                                    </tr>

                                    <tr>
                                        <td colspan="6" id="kmdaLogo" align="middle">
                                           <h1>KOLKATA METROPOLITAN DEVELOPMENT AUTHORITY</h1>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="6">
                                            <hr style="border: solid 1px lightblue" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px;"align="right"><span class="headFont">Employee Name&nbsp;&nbsp;</span> </td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:Label ID="lbEmpName" ClientIDMode="Static" autocomplete="off" class="headFont" runat="server" Width="400px"></asp:Label>
                                        </td>

                                        <td style="padding: 5px;" align="right"><span class="headFont">Employee No.&nbsp;&nbsp;</span> </td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:Label ID="lbEmpNo" ClientIDMode="Static" autocomplete="off" class="headFont" oncopy="return true" onpaste="return true" oncut="return true"
                                                runat="server" Width="100px"></asp:Label>
                                        </td>
                                        </tr>
                                     <tr>
                                        <td style="padding: 5px;" align="right"><span class="headFont">PF No.&nbsp;&nbsp;</span> </td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:Label ID="lPfNo" ClientIDMode="Static" autocomplete="off" class="headFont" oncopy="return true" onpaste="return true" oncut="return true"
                                                runat="server" Width="100px"></asp:Label>
                                        </td>
                                        <asp:HiddenField ID="hdnSalMonthID" runat="server" />
                                    
                                   
                                        <td style="padding: 5px;" align="right"><span class="headFont">Designation&nbsp;&nbsp;</span> </td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:Label ID="lDesignation" ClientIDMode="Static" autocomplete="off" class="headFont" oncopy="return true" onpaste="return true" oncut="return true"
                                                runat="server" Width="400px"></asp:Label>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="padding: 5px;" align="right"><span class="headFont">DOB&nbsp;&nbsp;</span> </td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:Label ID="lDOB" ClientIDMode="Static" class="headFont" autocomplete="off" oncopy="return true" onpaste="return true" oncut="return true"
                                                runat="server" Width="100px"></asp:Label>
                                        </td>
                                    
                                
                                        <td style="padding: 5px;"align="right"><span class="headFont">DOR&nbsp;&nbsp;</span> </td>
                            <td style="padding: 5px;">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:Label ID="lDOR" ClientIDMode="Static" class="headFont" autocomplete="off" oncopy="return true" onpaste="return true" oncut="return true"
                                    runat="server"  Width="400px"></asp:Label>
                            </td>
                            </tr>
                             <tr>
                                 <td style="padding: 5px;"align="right"><span class="headFont">Sector&nbsp;&nbsp;</span> </td>
                            <td style="padding: 5px;">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:Label ID="lSector" ClientIDMode="Static" class="headFont" autocomplete="off" oncopy="return true" onpaste="return true" oncut="return true"
                                    runat="server" Width="400px"></asp:Label>
                            </td>
                            <td style="padding: 5px;" align="right"><span class="headFont">Location&nbsp;&nbsp;</span> </td>
                            <td style="padding: 5px;">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:Label ID="lLocation" ClientIDMode="Static" class="headFont" autocomplete="off" oncopy="return true" onpaste="return true" oncut="return true"
                                    runat="server" Width="200px"></asp:Label>
                            </td>
                        
                       
                            

                        </tr>
                                    <tr>
                                        <td style="padding: 5px;" align="right"><span class="headFont">Pay Scale&nbsp;&nbsp;</span> </td>
                            <td style="padding: 5px;">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:Label ID="lPayScale" ClientIDMode="Static" class="headFont" autocomplete="off" oncopy="return true" onpaste="return true" oncut="return true"
                                    runat="server" Width="200px"></asp:Label>
                            </td>
                                         <td style="padding: 5px;" align="right"><span class="headFont">PAN&nbsp;&nbsp;</span> </td>
                            <td style="padding: 5px;">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:Label ID="lPAN" ClientIDMode="Static" class="headFont" autocomplete="off" oncopy="return true" onpaste="return true" oncut="return true"
                                    runat="server" Width="200px"></asp:Label>
                            </td>
                                    </tr>

                        <div id="gridDiv" style="width: 100%; display: none;"  >
                        <table id="tbl" border="1" style="width: 100%; text-align:center;" class="Grid" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_RowDataBound">
                            <%--                            <tr>
                                <th>asd</th>
                                <th>asd</th>
                                <th>asd</th>
                                <th>asd</th>
                                <th>asd</th>
                                <th>asd</th>
                                <th>asd</th>
                                <th>asd</th>
                            </tr>--%>
                        </table>
                        <table align="center" width="100%">
                            <tr>
                                <td width="1000"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                           
                            <td></td>
                                <td style="padding: 5px; vertical-align: top;" >
                                <div id="Radi">
                                            <asp:RadioButton ID="rdPDF" Checked="true" class="headFont" runat="server"  GroupName="a" Text="PDF" />
                                            <asp:RadioButton ID="rdExcel" class="headFont" runat="server" GroupName="a" Text="Excel" Checked="false" />
                                    </div>
                                        </td>

                            </tr>
                        <tr>
                            <td width="400"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            
                            <td style="padding-left: 800px;" align="left">
                                 <asp:Button ID="btnProjected" runat="server" Text="Projected Sal Statement"
                                        Width="150px" CssClass="Btnclassname" OnClientClick="projected(); return false;" />
                                      </td>
                                 <td style="padding-left: 20px;"  align="left">
                                 <asp:Button ID="btnOrginal" runat="server"  Text="Orginal Sal Statement"
                                        Width="150px" CssClass="Btnclassname" OnClientClick="Orginal(); return false;" />
                                      </td>
                            </tr>
                            
                         
                            <tr>
                                <td style="padding: 5px;" id="Gen" width="400" align="left"><span class="headFont">Generated on</span> 
                               <%--<td style="padding: 5px;" align="left">--%>
                                <asp:Label ID="ldate" ClientIDMode="Static" autocomplete="off" class="headFont" oncopy="return true" onpaste="return true" oncut="return true"
                                    runat="server" Width="100px"></asp:Label>
                            </td>
                                <td></td>
                                   </tr>
                            <tr>         
                                 <td colspan="7" id="KmdaSign"  style="padding: 5px;" align="right">
                                        <br /><br /> <br />     
                                           <h4>Signature</h4>
                                        </td>
                                    </tr>
                       
                        </table>
                    </div>
                    </table>

                    

                    <table align="center" width="100%">
                        <%--<tr>
                            <td colspan="4">
                                <hr style="border: solid 1px lightblue" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="padding: 5px;"><span class="require">*</span> indicates Mandatory Field</td>
                            <td style="padding: 5px;">&nbsp;</td>
                            <td style="padding: 5px;" align="left" bgcolor="White">
                                <div style="float: left; margin-left: 200px;">

                                    <asp:Button ID="cmdPrint" runat="server" Text="Print"
                                        Width="100px" CssClass="Btnclassname" OnClientClick="PrintDiv(); return false;" />   <%--PopulateGridForPrint--%>

                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />

                                </div>
                            </td>

                        </tr>

                    </table>


                </div>
            </div>
        </div>
    </div>
</asp:Content>

