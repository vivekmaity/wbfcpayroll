﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class ItaxGridView_Page : System.Web.UI.UserControl
{
    //==================================================================
    /* For ITax Type*/
    public DataTable dtAllTax { get; set; }
    //=====================================================================
    public string EmpID { get; set; }
    //===================================================================//
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                //========================================================================================================================
                if (dtAllTax == null)
                { dtAllTax = new DataTable(); DataRow dr = dtAllTax.NewRow(); }
                if (dtAllTax.Rows.Count != 0)
                    BindGridTaxType(dtAllTax);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void BindGridTaxType(DataTable dtAllTax)
    {
        try
        {
            if (dtAllTax.Rows.Count > 0)
            {

                GridTaxDetails.DataSource = dtAllTax;
                GridTaxDetails.DataBind();
                foreach (GridViewRow grdRow in GridTaxDetails.Rows)
                {
                    Button btnDetailsTax = (Button)(GridTaxDetails.Rows[grdRow.RowIndex].Cells[3].FindControl("btnDetails4"));
                    TextBox txtHAmount = (TextBox)(GridTaxDetails.Rows[grdRow.RowIndex].Cells[4].FindControl("txtHAmount"));
                    TextBox lblId = (TextBox)(GridTaxDetails.Rows[grdRow.RowIndex].Cells[1].FindControl("lblId"));
                    string srlno = lblId.Text;
                    //if (srlno == "1")
                    //{
                        btnDetailsTax.Visible = true;
                        txtHAmount.Enabled = false;

                    //}
                    //else
                    //{
                    //    btnDetailsTax.Visible = false;
                    //}
                }


            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    int i = 1;
    int j = 1;
    int GlobItaxId = 0;
    string ItaxSavingTotalAmount = "";
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            
            string txtcss = "txtHAmountChk" + i;
            Label lblSerial = (Label)e.Row.FindControl("lblSerial");
            Label lableMaxType = (Label)e.Row.FindControl("lableMaxType");
            Label lableMaxAmt = (Label)e.Row.FindControl("lableMaxAmt");
            TextBox txtHAmount = (TextBox)e.Row.FindControl("txtHAmount");
            Label lableHAmount = (Label)e.Row.FindControl("lableHAmount");
            TextBox ItxTypeId = (TextBox)e.Row.FindControl("lblId");
            int ITaxTypeId =Convert.ToInt32(ItxTypeId.Text);
            //TextBox txtCheck = (TextBox)e.Row.FindControl("txtCheck");

            GlobItaxId = ITaxTypeId;


            lableHAmount.ID = "lblchk" + ITaxTypeId.ToString();
            txtHAmount.CssClass = txtHAmount.CssClass + " txtHAmountChk" + ITaxTypeId.ToString();
            lableHAmount.CssClass = lableHAmount.CssClass + "chklbl";
            txtHAmount.Attributes.Add("onKeyup", "CkeckAmount($(this).val(),'" + lableMaxType.Text + "'," + lableMaxAmt.Text + ",'" + lableHAmount.ID + "')");
            txtHAmount.Attributes.Add("onKeydown", "CkeckMaxAmount($(this).val(),'" + lableMaxType.Text + "','" + lableMaxAmt.Text + "','" + txtcss + "')");
            //if (i == 1)
            //{
                DataTable dtSavingType = DBHandler.GetResult("Load_AllSavingType", EmpID, ITaxTypeId);
                if (dtSavingType.Rows.Count>0)
                {
                    ItaxSavingTotalAmount = dtSavingType.Rows[0]["TotlblAmount"].ToString();
                }
              
                    GridView gv = (GridView)e.Row.FindControl("gv80C80CCD");
                    gv.DataSource = dtSavingType;
                    gv.DataBind();
                    gv.CssClass = gv.CssClass + " " + ITaxTypeId.ToString();
                    //e.Row.Attributes["class"] += ITaxTypeId;
                    if (dtSavingType.Rows.Count > 0)
                    {
                       
                        Label lblTotal1 = gv.FooterRow.FindControl("lblTotal1") as Label;
                        lblTotal1.CssClass = "lblTotal" + ITaxTypeId.ToString();
                    }
                    else {
                        dtSavingType.Rows.Add(dtSavingType.NewRow());
                        gv.DataSource = dtSavingType;
                        gv.DataBind();
                        int columncount = gv.Rows[0].Cells.Count;
                        gv.Rows[0].Cells.Clear();
                        gv.Rows[0].Cells.Add(new TableCell());
                        gv.Rows[0].Cells[0].ColumnSpan = columncount;
                        gv.Rows[0].Cells[0].Text = "No Records Found";
                        Label lblTotal1 = gv.FooterRow.FindControl("lblTotal1") as Label;
                        lblTotal1.CssClass = "lblTotal" + ITaxTypeId.ToString();
                        gv.Height = new Unit(gv.RowStyle.Height.Value * gv.Rows.Count);
                    }

            //}
            lblSerial.Text = i.ToString();
            i++;
            //trhideClass.Attributes.Add();
            
        }
        j = 1;
    }
    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView gv = (GridView)e.Row.FindControl("gv80C80CCD");
            Label lblSerial1 = (Label)e.Row.FindControl("lblSerial1");
            lblSerial1.Text = j.ToString();
            HiddenField txtGAmounthid = (HiddenField)e.Row.FindControl("txtGAmounthid");
            txtGAmounthid.Value = GlobItaxId.ToString();
            TextBox txtGAmount = (TextBox)e.Row.FindControl("txtGAmount");
            txtGAmount.CssClass = txtGAmount.CssClass + " " + "atextbox" + GlobItaxId.ToString();

            Label itaxSaveMaxAmount = (Label)e.Row.FindControl("itaxSaveMaxAmount");
            itaxSaveMaxAmount.CssClass = itaxSaveMaxAmount.CssClass + GlobItaxId.ToString();
            j++;

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label AmountTotal = (Label)e.Row.FindControl("lblTotal1");
            AmountTotal.Text = String.Format("{0:C2}", ItaxSavingTotalAmount);
        }
    }

   
}