﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;

public partial class UserMaster : System.Web.UI.Page
{

    public static string hdnUsereId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_UserMaster");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_UserMasterByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                hdnUsereId= dtResult.Rows[0]["UserID"].ToString();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlEmployee")).SelectedValue = dtResult.Rows[0]["EmployeeID"].ToString();
                    ((DropDownList)dv.FindControl("ddlUserType")).SelectedValue = dtResult.Rows[0]["TypeCode"].ToString();
                    ((DropDownList)dv.FindControl("ddlSector")).SelectedValue = dtResult.Rows[0]["SectorID"].ToString();
                }
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_UserMasterByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);

        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtUserName = (TextBox)dv.FindControl("txtUserName");
                TextBox txtPass = (TextBox)dv.FindControl("txtPass");
                TextBox txtUserCode = (TextBox)dv.FindControl("txtUserCode");
                TextBox txtSMID = (TextBox)dv.FindControl("txtSMID");
                CheckBox chkLock = (CheckBox)dv.FindControl("chkLock");
                DropDownList ddlEmployee = (DropDownList)dv.FindControl("ddlEmployee");
                DropDownList ddlUserType = (DropDownList)dv.FindControl("ddlUserType");
                DropDownList ddlSector = (DropDownList)dv.FindControl("ddlSector");

                DBHandler.Execute("Insert_UserMaster", txtUserName.Text, Convert.ToInt32(ddlUserType.SelectedValue),
                    Convert.ToInt32(ddlEmployee.SelectedValue), txtPass.Text, Convert.ToInt32(ddlSector.SelectedValue),
                    txtUserCode.Text, txtSMID.Text, (chkLock.Checked ? "Y" : "N"), Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {
                TextBox txtUserName = (TextBox)dv.FindControl("txtUserName");
                TextBox txtPass = (TextBox)dv.FindControl("txtPass");
                TextBox txtUserCode = (TextBox)dv.FindControl("txtUserCode");
                TextBox txtSMID = (TextBox)dv.FindControl("txtSMID");
                CheckBox chkLock = (CheckBox)dv.FindControl("chkLock");
                DropDownList ddlEmployee = (DropDownList)dv.FindControl("ddlEmployee");
                DropDownList ddlUserType = (DropDownList)dv.FindControl("ddlUserType");
                DropDownList ddlSector = (DropDownList)dv.FindControl("ddlSector");

                string ID = hdnUsereId;

                DBHandler.Execute("Update_UserMaster", ID, txtUserName.Text, Convert.ToInt32(ddlUserType.SelectedValue),
                    Convert.ToInt32(ddlEmployee.SelectedValue), Convert.ToInt32(ddlSector.SelectedValue),
                    txtSMID.Text, (chkLock.Checked ? "Y" : "N"), Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload(string name)
    {
        try
        {
            DataTable dt = new DataTable();
            if (name == "Employee")
            {
                dt = DBHandler.GetResult("Get_EmployeeDdl");
            }
            else if (name == "UserType")
            {
                dt = DBHandler.GetResult("Get_UserTypeDdl");
            }
            else if (name == "Sector")
            {
                dt = DBHandler.GetResult("Get_SectorDdl");
            }
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}