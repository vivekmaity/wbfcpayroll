﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class error_PageNotFound1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session[SiteConstants.SSN_INT_USER_ID] = null;
        Session[SiteConstants.SSN_DR_USER_DETAILS] = null;
        Session.RemoveAll();
        Session.Abandon();
    }

    protected void Exit_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }
}