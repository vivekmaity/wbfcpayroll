﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageNotFound1.aspx.cs" Inherits="error_PageNotFound1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>WEST BENGAL FINANCIAL CORPORATION</title>
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" type="image/ico" href="favicon.ico" />
<link href="css/homestyle.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.8.1.min.js" type="text/javascript"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>

    <style>
        #Exit {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>

</head>

<body>

    <form id="form1" runat="server">
        <div>
            <table style="width:100%;text-align:center;">
                <tr>
                    <td>
                        <img src="~/images/logo.gif" alt="banner" style="width:47%;text-align:center;" runat="server" autocomplete="off" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table style="width:100%;text-align:center;">
                <tr>
                    <td>
                        <img src="~/images/PNF.jpg" alt="banner" runat="server" autocomplete="off" />
                    </td>
                </tr>
            </table>
        </div>
         <div>
            <table style="width:100%;text-align:center;">
                <tr>
                    <td>
                        <asp:Button ID="Exit"  runat="server" autocomplete="off" ClientIDMode="Static" style="width:20%;"
                         OnClick="Exit_Click" class="submit_bg" Text="Exit" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
