﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;      
using System.Configuration;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DataAccess;

public partial class PayReportView : System.Web.UI.Page
{               
    static int ReportNameVal=0;
    static int SectorID=0;                      
    static int SalMonthId = 0;
    static string SalMonth = "";
    static int Centerid = 0;
    static string ExportFileName = "";
    static string ExportSectorName = "";
    static string AdminorSector = "";
    static string  UserID = "";
    String ConnString = "";  
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindReport(crystalReport);
        SetPaperSize();
    }

    protected void ExportPDF(object sender, EventArgs e)
    {
        mydiv.Style.Add("display", "block");
        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);        
      
        ExportFormatType formatType = ExportFormatType.NoFormat;
        switch (rbFormat.SelectedItem.Value)
        {
            case "Excel":
                formatType = ExportFormatType.Excel;
                break;
            case "PDF":
                formatType = ExportFormatType.PortableDocFormat;
                break;          
        }
        crystalReport.ExportToHttpResponse(formatType, Response, true, ExportFileName);
        Response.End();
    }

    private void BindReport(ReportDocument crystalReport)
    {
        ConnString = DataAccess.DBHandler.GetConnectionString();
         ReportNameVal = Convert.ToInt32(HttpContext.Current.Request.QueryString["ReportNameVal"]);
         SectorID = Convert.ToInt32(HttpContext.Current.Request.QueryString["SectorId"]);
         SalMonthId = Convert.ToInt32(HttpContext.Current.Request.QueryString["SalMonthId"]);
         Centerid = Convert.ToInt32(HttpContext.Current.Request.QueryString["Centerid"]);
         SalMonth = Convert.ToString(HttpContext.Current.Request.QueryString["SalMonth"]);
         AdminorSector = Convert.ToString(HttpContext.Current.Request.QueryString["AdminorSector"]);
         mydiv.Style.Add("display", "block");

         SqlConnection con = new SqlConnection(ConnString);
         DataTable dt = new DataTable();
         //SqlDataAdapter(Query,Connection)
         SqlDataAdapter adp = new SqlDataAdapter("Select SectorName from MST_Sector where SectorID=" + SectorID + "", con);
         adp.Fill(dt);
         // dt.Rows[index][ColumnName]
         if (dt.Rows.Count > 0)
         {
             ExportSectorName = dt.Rows[0]["SectorName"].ToString();
         }


         if (AdminorSector == "" || AdminorSector==null)
             UserID = HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID].ToString();
         else
         {
             DataTable dtUserID = DBHandler.GetResult("Get_UserIDbyAdmin", SectorID);
             if (dtUserID.Rows.Count > 0)
             {
                 UserID = dtUserID.Rows[0]["DefaultUserId"].ToString();
             }
         }

        if (ReportNameVal == 1)      
        {
            crystalReport.Load(Server.MapPath("~/Reports/PaySlip_new_LocationWISE_A5.rpt"));
            crystalReport.RecordSelectionFormula = "{MST_Employee.SectorID}=" + SectorID + " and {rpt_tmp_payslip_monthly.inserted_By}=" + UserID;
            ExportFileName = ExportSectorName + "_" + "Pay Slip" + "(" + SalMonth + ")";
        }
        else if (ReportNameVal == 2)
        {
            crystalReport.Load(Server.MapPath("~/Reports/PaySummarySecWise.rpt"));
            crystalReport.RecordSelectionFormula = "{tmp_Summary_PayDetails_sectorWise.Secid}=" + SectorID + " and {tmp_Summary_PayDetails_sectorWise.insertedBy}=" + UserID;
            ExportFileName = ExportSectorName + "_" + "Pay Slip Summary Sector Wise" + "(" + SalMonth + ")";
        }
        else if (ReportNameVal == 3)
        {
            crystalReport.Load(Server.MapPath("~/Reports/PaySummarySecWiseLocationWise.rpt"));
            ExportFileName = ExportSectorName + "_" +  "Pay Slip Summary Location Wise" + "(" + SalMonth + ")";
            if (Centerid > 0)
            {
                crystalReport.RecordSelectionFormula = "{tmp_Summary_PayDetails_sectorWise_centerWise.Secid}=" + SectorID + " and {tmp_Summary_PayDetails_sectorWise_centerWise.MonthID}=" + SalMonthId + "  and {tmp_Summary_PayDetails_sectorWise_centerWise.Centerid}=" + Centerid + " and {tmp_Summary_PayDetails_sectorWise_centerWise.InsertedBy}=" + UserID;
            }
            else 
            {
                crystalReport.RecordSelectionFormula = "{tmp_Summary_PayDetails_sectorWise_centerWise.Secid}=" + SectorID + " and {tmp_Summary_PayDetails_sectorWise_centerWise.MonthID}=" + SalMonthId + " and {tmp_Summary_PayDetails_sectorWise_centerWise.InsertedBy}=" + UserID;
            }           
 
        }
        else if (ReportNameVal == 4)
        {
            crystalReport.Load(Server.MapPath("~/Reports/Pay Register.rpt"));
            crystalReport.RecordSelectionFormula = "{MST_Employee.SectorID}=" + SectorID + " and {MST_Employee.Status} in ['Y','S']";
            ExportFileName = "Pay Register" + "(" + SalMonth + ")";
        }
        else if (ReportNameVal == 41)
        {
            crystalReport.Load(Server.MapPath("~/Reports/NewPayRegister.rpt"));
            //crystalReport.RecordSelectionFormula = "{MST_Employee.SectorID}=" + SectorID + " and {MST_Employee.Status} in ['Y','S']";
            ExportFileName = "Pay Register" + "(" + SalMonth + ")";
        }

        //crystalReport.Refresh();

        //crystalReport.DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
        //crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
        //crystalReport.SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
       // System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString);
        crystalReport.Refresh();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        crConnectionInfo.ServerName = Utility.ServerName(ConnString);
        crConnectionInfo.DatabaseName = Utility.DatabaseName(ConnString);
        crConnectionInfo.UserID = Utility.UserID(ConnString);
        crConnectionInfo.Password = Utility.Password(ConnString);
        TableLogOnInfo crTableLogoninfo = new TableLogOnInfo();
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in crystalReport.Database.Tables)
        {
            crTableLogoninfo = CrTable.LogOnInfo;
            crTableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crTableLogoninfo);
        }
        foreach (ReportDocument subreport in crystalReport.Subreports)
        {
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in subreport.Database.Tables)
            {
                crTableLogoninfo = CrTable.LogOnInfo;
                crTableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crTableLogoninfo);
            }
        }
        crystalReport.VerifyDatabase();
        CrystalReportViewer1.DisplayToolbar = true;
        CrystalReportViewer1.Zoom(150);  // Page Width
        CrystalReportViewer1.Visible = true;
        CrystalReportViewer1.ReportSource = crystalReport;
        crystalReport.Refresh();
    }       

    public void OnConfirmprint(object sender, EventArgs e)
    {
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {

            ReportDocument crystalReport = new ReportDocument();
            BindReport(crystalReport);

            ExportFormatType formatType = ExportFormatType.NoFormat;
            switch (rbFormat.SelectedItem.Value)
            {
                case "Excel":
                    formatType = ExportFormatType.Excel;
                    break;
                case "PDF":
                    formatType = ExportFormatType.PortableDocFormat;
                    break;
            }

            crystalReport.ExportToHttpResponse(formatType, Response, false, ExportFileName);

            Response.End();

        }
    }

    protected void SetPaperSize()
    {
        ddlPaperSize.Items.Clear();
        ddlPaperSize.Items.Add("Page - A4");
        //if (ReportNameVal == 1)
        //{
        //    ddlPaperSize.Items.Add("Page - A5");
        //}
        //else
        //{
        //    ddlPaperSize.Items.Add("Page - A4");
        //}
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        //if (this.crystalReport != null)
        //{
        this.crystalReport.Close();
        this.crystalReport.Dispose();
        //}
    }

}