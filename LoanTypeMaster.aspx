﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LoanTypeMaster.aspx.cs" Inherits="LoanTypeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            $("#txtLoanTypeCode").rules("add", { required: true, messages: { required: "Please enter Loan Type Code"} });
            $("#ddlLoanType").rules("add", { required: true, messages: { required: "Please select Loan Type"} });
            $("#txtLoanAbbv").rules("add", { required: true, messages: { required: "Please enter Loan Abbreviation"} });
            $("#txtLoanDesc").rules("add", { required: true, messages: { required: "Please enter Loan Description"} });
        }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Loan Type  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr style="display:none;">
                                        <td style="padding:5px;" ><span class="headFont">Loan Type Code &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanTypeCode" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" value="i"></asp:TextBox>                            
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Loan Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlLoanType" Width="217px" runat="server" AppendDataBoundItems="true" Height="22px" autocomplete="off">
                                                <asp:ListItem Text="(Select Loan Type)" Value="h"></asp:ListItem>
                                                <asp:ListItem Text="Government Loan" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="Earning Recovery" Value="B"></asp:ListItem>
                                                <asp:ListItem Text="Out of Account Loan" Value="O"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Abbreviation &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanAbbv" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="4"></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Loan Description&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanDesc" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Active &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:CheckBox ID="chkActiveFlag" runat="server" ClientIDMode="Static" Checked="true" />
                                        </td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>

                            <EditItemTemplate>
                                <table align="center"  width="100%">
                                    <tr style="display:none;">
                                        <%--<td style="padding:5px;"> <span class="headFont">Loan Type Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanTypeCode" Text='<%# Eval("LoanTypeCode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" Enabled="false"></asp:TextBox>                            
                                            
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Loan Type&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlLoanType" Width="217px" runat="server" AppendDataBoundItems="true" Height="23px">
                                                <asp:ListItem Text="(Select Loan Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Government Loan" Value="N"></asp:ListItem>
                                                <asp:ListItem Text="Earning Recovery" Value="B"></asp:ListItem>
                                                <asp:ListItem Text="Out of Account Loan" Value="O"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Abbreviation &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("LoanTypeID") %>'/>
                                            <asp:TextBox ID="txtLoanAbbv" ClientIDMode="Static" runat="server" autocomplete="off" CssClass="textbox" MaxLength="4" Text='<%# Eval("LoanAbbv") %>'></asp:TextBox>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Loan Description&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtLoanDesc" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox" MaxLength="100" TextMode="MultiLine" Text='<%# Eval("LoanDesc") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Loan Active &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:CheckBox ID="chkActiveFlag" runat="server" ClientIDMode="Static" Checked='<%# Eval("LoanActive").ToString().Equals("Y")  %>' />
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                                                    AutoGenerateColumns="false" DataKeyNames="LoanTypeID" OnRowCommand="tbl_RowCommand"
                                                    AllowPaging="true"
                                                    PageSize="10" CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                                     <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                        <AlternatingRowStyle BackColor="#FFFACD" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Edit">
                                                            <HeaderStyle  />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                    runat="server" ID="btnEdit" CommandArgument='<%# Eval("LoanTypeID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Delete">
                                                            <HeaderStyle  />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                    OnClientClick='return Delete();' CommandArgument='<%# Eval("LoanTypeID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="LoanTypeID" Visible="false" ItemStyle-CssClass="labelCaption"
                                                            HeaderStyle-CssClass="TableHeader" HeaderText="LoanTypeID" />
                                                        <%--<asp:BoundField DataField="LoanTypeCode" 
                                                            HeaderText="Loan Type Code" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="LoanType"
                                                            HeaderText="Loan Type" />--%>
                                                        <asp:BoundField DataField="LoanAbbv" 
                                                            HeaderText="Loan Abbreviation" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="LoanDesc"
                                                            HeaderText="Loan Description" />
                                                        <asp:BoundField DataField="LoanActive" 
                                                            HeaderText="Loan Active" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                    </div>

                </div>
            </div>

        </div>
</div>  
</asp:Content>

