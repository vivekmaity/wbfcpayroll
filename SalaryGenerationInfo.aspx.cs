﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class SalaryGenerationInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Load_SalGenInfo");
            if (dt.Rows.Count > 0)
            {
                txtPayMonth.Text=dt.Rows[0]["Pay Month"].ToString();
                grvSalInfo.DataSource = dt;
                grvSalInfo.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}