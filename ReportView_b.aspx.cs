﻿using System;
using System.Configuration;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using DataAccess;

public partial class ReportView_b : System.Web.UI.Page
{
    string ParentID_b = "";
    string ReportID_b = "";
    string SalMonth = "";
    string ExportFileName = "";
    String ConnString = "";
    ExportFormatType formatType = ExportFormatType.NoFormat;
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();

    
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!SessionContext.IsAuthenticated)
        //{
        //    Response.Redirect("/Error/Error401/Index", true);
        //    return;
        //}

        //user = SessionContext.CurrentUser as IInternalUser;

        if (HttpContext.Current.Request.QueryString["ReportTypeID"] != null)
            ParentID_b = HttpContext.Current.Request.QueryString["ReportTypeID"].ToString();

        if (HttpContext.Current.Request.QueryString["ReportID"] != null)
            ReportID_b = HttpContext.Current.Request.QueryString["ReportID"].ToString();

        if (HttpContext.Current.Request.QueryString["SalMonth"] != null)
            SalMonth = HttpContext.Current.Request.QueryString["SalMonth"].ToString();

       
        BindReport(crystalReport);
    }

    private void BindReport(ReportDocument crystalReport)
    {
        ConnString = DataAccess.DBHandler.GetConnectionString();
        SqlConnection con = new SqlConnection(ConnString);
        int UserID; int ParentID; int ReportID;
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        ParentID = Convert.ToInt32(ParentID_b);
        ReportID = Convert.ToInt32(ReportID_b);
        DataTable dt = DBHandler.GetResult("Get_Report", ParentID, ReportID, UserID);
       
        
        try
        {
            
            if (dt.Rows.Count > 0)
            {
                string rptName = dt.Rows[0]["rptName"].ToString();
                string ReportName = dt.Rows[0]["ReportName"].ToString();
                string Formula = dt.Rows[0]["SelectionFormula"].ToString();

                crystalReport.Load(Server.MapPath("~/Reports/" + rptName.Trim()));
                ExportFileName = ReportName + "_" + SalMonth;

                if (Formula != "")
                {
                    crystalReport.RecordSelectionFormula = Formula + UserID;
                }

                string server = Utility.ServerName(ConnString);
                string database = Utility.DatabaseName(ConnString);
                string userid = Utility.UserID(ConnString);
                string password = Utility.Password(ConnString);


                crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
                crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                crystalReport.SetDatabaseLogon(server, database, userid, password);

                crystalReport.VerifyDatabase();
                crystalReport.Refresh();
                crystalReport.ReadRecords();

                for (int i = 0; i < crystalReport.Subreports.Count; i++)
                {
                    crystalReport.Subreports[i].DataSourceConnections[0].SetConnection(server, database, userid, password);
                    crystalReport.Subreports[i].DataSourceConnections[0].IntegratedSecurity = false;
                    crystalReport.Subreports[i].SetDatabaseLogon(server, database, userid, password);

                }

                //



                //CrystalReportViewer1.ReportSource = crystalReport;


                formatType = ExportFormatType.PortableDocFormat;
                crystalReport.ExportToHttpResponse(formatType, Response, false, ExportFileName);

                Response.End();
            }
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            con.Close();
        }
       
    }

    protected void ExportPDF(object sender, EventArgs e)
    {
        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);

        ExportFormatType formatType = ExportFormatType.NoFormat;
        formatType = ExportFormatType.PortableDocFormat;
        crystalReport.ExportToHttpResponse(formatType, Response, true, ExportFileName);
        Response.End();
    }

    protected void Page_Unload(object sender, EventArgs e)
        {
            this.crystalReport.Close();
            this.crystalReport.Dispose();
        }


}