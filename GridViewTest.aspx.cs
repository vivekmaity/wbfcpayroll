﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class GridViewTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string EmpNo = string.Empty;
        try
        {
            //mainDiv.InnerHtml = GetGridControlContent("~/GridViewTemplete.ascx", EmpNo);
            if (!IsPostBack)
            {
                PopulateGrid(1);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void PopulateGrid(int SecID)
    {
        try
        {
            DataTable dtsector = DBHandler.GetResult("Get_EmpdetailforacceptLPC", SecID);
            if (dtsector.Rows.Count > 0)
            {
                    grdEmp.DataSource = dtsector;
                    grdEmp.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grdEmp_OnRowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                e.Row.TabIndex = -1;
                var d = string.Format("javascript:SelectRow(this, {0});", e.Row.RowIndex);
                e.Row.Attributes["onclick"] = string.Format("javascript:SelectRow(this, {0});", e.Row.RowIndex);
                e.Row.Attributes["onkeydown"] = "javascript:return SelectSibling(event);";
                e.Row.Attributes["onselectstart"] = "javascript:return false;";
            }
        }
    }

    [WebMethod]
    public static string AjaxGetGridCtrl(string EmpNo)
    {
        return GetGridControlContent("~/GridViewTemplete.ascx", EmpNo);
    }

    private static String GetGridControlContent(String controlLocation, string EmpNo)
    {
        var page = new Page();

        var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

        userControl.EmpNo = EmpNo;

        page.Controls.Add(userControl);
        String htmlContent = "";

        using (var textWriter = new StringWriter())
        {
            HttpContext.Current.Server.Execute(page, textWriter, false);
            htmlContent = textWriter.ToString();
        }
        return htmlContent;
    }
}