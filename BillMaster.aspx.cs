﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class BillMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Insert_BillMaster(string SecID, string BillNo, string BillDate, string PayMontID)
    {
        string JSONVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Insert_BillMaster", SecID, BillNo, BillDate, PayMontID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (dt.Rows.Count > 0)
            {
                int resut = Convert.ToInt32(dt.Rows[0]["BillSl"].ToString());
                if (resut > 0)
                {
                    string val = "success";
                    JSONVal = val.ToJSON();
                }
                else 
                {
                    string val = "fail";
                    JSONVal = val.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Update_BillMaster(string SecID, string SalMonth, string BillNo, string BillDate, string BillSl, string PayMontID)
    {
        string JSONVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Update_BillMaster", SecID, SalMonth, BillNo, BillDate, BillSl, PayMontID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (dt.Rows.Count > 0)
            {
                string resut = dt.Rows[0]["Result"].ToString();
                JSONVal = resut.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod]
    public static string GetCenter(int SecID, string SalFinYear)
    {
        DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID, SalFinYear);
        string salMonthID = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();
        DataTable dtBill = DBHandler.GetResult("Get_BillMasterDetails", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID, salMonthID);

        List<SalaryMonth> listSalMonth = new List<SalaryMonth>();

        if (dtSalMonth.Rows.Count > 0)
        {
            for (int i = 0; i < dtSalMonth.Rows.Count; i++)
            {
                SalaryMonth objst = new SalaryMonth();

                objst.PayMonthsID = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                if (dtBill.Rows.Count > 0)
                {
                    objst.BillSl = Convert.ToString(dtBill.Rows[0]["BillSl"]);
                    objst.BillNo = Convert.ToString(dtBill.Rows[0]["BillNo"]);
                    objst.BillDate = Convert.ToString(dtBill.Rows[0]["BillDate"]);
                    objst.SaveorUpdate = "Update";
                }
                else
                {
                    objst.BillSl = "";
                    objst.BillNo = "";
                    objst.BillDate = "";
                    objst.SaveorUpdate = "Save";
                }

                listSalMonth.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class SalaryMonth
    {
        public string PayMonthsID { get; set; }
        public string PayMonths { get; set; }
        public string BillSl { get; set; }
        public string BillNo { get; set; }
        public string BillDate { get; set; }
        public string SaveorUpdate { get; set; }
    }
}