﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true"  ViewStateEncryptionMode="Always" CodeFile="MonthelyReport.aspx.cs" Inherits="MonthelyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            fin_year();
            $('#txtSalFinalYear').change(function () {
                var sectorid = $('#ddlSector').val();
                if (sectorid == 0) {
                    alert('Plese Select Sector'); return;
                }
                var fn_year = $('#txtSalFinalYear').val();
                if (fn_year == '') {
                    alert('Plese Final year'); return;
                }
                var E = "{sectorid: " + $('#ddlSector').val() + ", finYear: '" + fn_year + "'}";
                var options = {};
                options.url = "PaySlipReport.aspx/GetSalMonth";
                options.type = "POST";
                options.data = E;
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (salmonth) {

                    var t = jQuery.parseJSON(salmonth.d);
                    var a = t.length;
                    if (a >= 0) {
                        //$("#txtPayMonth").append("<option value=''>Select</option>")
                        $.each(t, function (key, value) {
                            $("#txtCurrMonth").append($("<option></option>").val(value.Salmonthid + '#' + value.SalaryMonth).html(value.SalaryMonth));
                        });
                        var myVal = $('#txtCurrMonth option:last').val();
                        $('#txtCurrMonth').val(myVal);
                    }
                };
                options.error = function () { alert("Error in retrieving SalMonth!"); };
                $.ajax(options);
            });
        });
        function PrintOpentab() {
            //$(".loading-overlay").show();
            var SalaryFinYear;
            var SalMonth;
            var SectorID;
            var CenterID;
            var EmpType;
            var Status;
            var SchType;
            var SectorGroup;
            var CenterGroup;
            var EmpTypeGroup;
            var StatusGroup;
            var EDID;
            var UserID;

            SalaryFinYear = $('#txtSalFinalYear').val();
            SalMonth = $('#txtCurrMonth').val().split("#")[2];
            var salmonthID = $('#txtCurrMonth').val().split('#')[0];
            var E = "{SalmonthID: " + salmonthID + ",SalaryFinYear:'" + SalaryFinYear + "',SalMonth:'" + SalMonth + "',SectorID: " + $('#ddlSector').val() + ",CenterID: 0,EmpType:'K',Status:'0',EDID:101,ChildreportID:39,reportHisCur:'" + $('#txtCurrMonth').val().split('#')[1] + "',isPDFExcel:1}";
            //console.log(E); return false;
            $(".loading-overlay").show();
            $.ajax({

                type: "POST",
                url: "MonthelyReport.aspx/Report_Paravalue",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    jsmsg = JSON.parse(msg.d);
                    window.open("ReportView_b.aspx?ReportID=39&SalMonth=" + SalMonth + "&ReportTypeID=3");
                    $(".loading-overlay").hide();
                }
            });


        }
        function fin_year() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var E = "{sectorid: " + $('#ddlSector').val() + "}";
            var options = {};
            options.url = "PaySlipReport.aspx/FinYearForReport";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (FinYear) {
                var t = jQuery.parseJSON(FinYear.d);
                var a = t.length;
                if (a >= 0) {
                    //$("#ddlfinyear").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#txtSalFinalYear").append($("<option></option>").val(value.SalaryFinYear).html(value.SalaryFinYear));
                    });
                    var myVal = $('#txtSalFinalYear option:last').val();
                    $('#txtSalFinalYear').val(myVal);
                    sal_month();
                }
            };
            options.error = function () { alert("Error in retrieving Ina year!"); };
            $.ajax(options);
        }
        function sal_month() {
            var sectorid = $('#ddlSector').val();
            if (sectorid == 0) {
                alert('Plese Select Sector'); return;
            }
            var fn_year = $('#txtSalFinalYear').val();
            if (fn_year == '') {
                alert('Plese Final year'); return;
            }
            var E = "{sectorid: " + $('#ddlSector').val() + ", finYear: '" + fn_year + "'}";
            var options = {};
            options.url = "PaySlipReport.aspx/GetSalMonth";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (salmonth) {

                var t = jQuery.parseJSON(salmonth.d);
                var a = t.length;
                if (a >= 0) {
                    //$("#txtPayMonth").append("<option value=''>Select</option>")
                    $.each(t, function (key, value) {
                        $("#txtCurrMonth").append($("<option></option>").val(value.Salmonthid + '#' + value.SalaryMonth).html(value.SalaryMonth));
                    });
                    var myVal = $('#txtCurrMonth option:last').val();
                    $('#txtCurrMonth').val(myVal);
                }
            };
            options.error = function () { alert("Error in retrieving SalMonth!"); };
            $.ajax(options);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Monthely Report</h2>						
						</section>
                        <table width="98%" style="border:solid 2px lightblue;  "  >
                            <tr>
                                <td style="padding:15px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td style="padding:5px;" class="style17" ><span class="headFont">Salary Financial Year &nbsp;&nbsp;</span> </td>
                                            <td style="padding:5px;">:</td>
                                            <td style="padding:5px;" align="left" class="style19" >
                                                <asp:DropDownList ID="txtSalFinalYear" Width="200px" Height="25px" CssClass="textbox" Enabled="true" runat="server" autocomplete="off"></asp:DropDownList>
                                            </td>
                                            <td style="padding:5px;" class="style18"><span class="headFont" >Current Month &nbsp;&nbsp;</span> </td>
                                            <td style="padding:5px;">:</td>
                                            <td class="style22" >
                                                <asp:DropDownList ID="txtCurrMonth" Width="200px" Height="25px" autocomplete="off" CssClass="textbox" Enabled="true" runat="server"></asp:DropDownList>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <table align="center"  width="100%">
                                        <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                        <tr>
                                            <td style="padding:5px;" class="style1"><span class="headFont">* indicates Mandatory Field &nbsp;&nbsp;</span> </td>
                                            <td style="padding:5px;">&nbsp;</td>
                                            <td style="padding:5px;" align="left" >
                                                <div style="float:left;margin-left:200px;">
                                                    <asp:Button ID="cmdShow" runat="server" Text="Show"   
                                                    Width="100px" CssClass="Btnclassname" OnClientClick="PrintOpentab(); return false;" 
                                                        BorderColor="#669999"/> 
                                                    <asp:Button ID="cmdCancel" runat="server" Text="Reset"   
                                                    Width="100px" CssClass="Btnclassname"/>
                                                </div>
                                                <div class="loading-overlay">
                                                    <div class="loadwrapper">
                                                    <div class="ajax-loader-outer">Loading...</div>
                                                    </div>
                                                </div>    
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

