﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MonthlySalaryDetails.aspx.cs" Inherits="MonthlySalaryDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        function beforeSave() {
            $("#form1").validate();
            $("#txtEDAmt").rules("add", { required: true, messages: { required: "Please enter ED Amount"} });
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>Monthly Salary Details</h2>						
						</section>
                    <table width="98%" style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployee" Width="200px" runat="server" DataSource='<%#drpload("Employee") %>'
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Salary Month&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlSalMonth" Width="200px" runat="server" DataSource='<%#drpload("SalMonth") %>'
                                                DataValueField="SalMonthID" DataTextField="SalMonth" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="(Select Salary Month)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">ED Type &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEDType" Width="200px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlEDTypeChanged" autocomplete="off">
                                                <asp:ListItem Text="(Select ED Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>
                                                <asp:ListItem Text="Value" Value="V"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">ED Abbv&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEDAbbv" Width="200px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">ED Amount &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtEDAmt" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18" onkeypress="return isNumberKey(event)" autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>
                            <EditItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Employee Name &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEmployee" Width="200px" runat="server" DataSource='<%#drpload("Employee") %>'
                                                DataValueField="EmployeeID" DataTextField="EmpName" AppendDataBoundItems="true" ClientIDMode="Static" Enabled="false" autocomplete="off">
                                                <asp:ListItem Text="(Select Employee)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("SalaryDetailsID") %>'/>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Salary Month&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlSalMonth" Width="200px" runat="server" DataSource='<%#drpload("SalMonth") %>'
                                                DataValueField="SalMonthID" DataTextField="SalMonth" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                <asp:ListItem Text="(Select Salary Month)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">ED Type &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEDType" Width="200px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlEDTypeChanged" autocomplete="off">
                                                <asp:ListItem Text="(Select ED Type)" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>
                                                <asp:ListItem Text="Value" Value="V"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">ED Abbv&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:DropDownList ID="ddlEDAbbv" Width="200px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">ED Amount &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtEDAmt" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="18" onkeypress="return isNumberKey(event)" Text='<%# Eval("EDAmount") %>' autocomplete="off"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdSave_Click" /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            <div style="overflow-y:scroll;height:auto;width:100%">
                            <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                DataKeyNames="SalaryDetailsID" OnRowCommand="tbl_RowCommand" AllowPaging="false" >
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("SalaryDetailsID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                         <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete" 
                                                OnClientClick='return Delete();' 
                                            CommandArgument='<%# Eval("SalaryDetailsID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SalaryDetailsID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SalaryDetailsID" />
                                    <asp:BoundField DataField="EmployeeID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="EmployeeID" />
                                    <asp:BoundField DataField="SalMonthID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SalMonthID" />
                                    <asp:BoundField DataField="EmpName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Employee Name" />
                                    <asp:BoundField DataField="SalMonth" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Salary Month" />
                                    <asp:BoundField DataField="EDAbbv" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="ED Abbv" />
                                    <asp:BoundField DataField="EDType" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="ED Type" />
                                    <asp:BoundField DataField="EDAmount" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="ED Amount" />
                                </Columns>
                            </asp:GridView> 
                            </div>
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

