﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

public partial class Log : System.Web.UI.Page
{
    IEncryptDecrypt chiperAlgorithm;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                string QueryStringData = Request.QueryString["Data"];
                chiperAlgorithm = new AES_Algorithm("EIIPL@201^");
                string decryptedURL = chiperAlgorithm.Decrypt(QueryStringData);
                string urlDecodedData = HttpUtility.UrlDecode(decryptedURL);
                JavaScriptSerializer js = new JavaScriptSerializer();
                var serializedData = js.Deserialize<QueryData>(urlDecodedData);

                string QueryStringUserID = serializedData.UserID;
                string QueryStringMID = serializedData.MID;
                string QueryStringKMS = serializedData.KMS;
                string QueryStringUserName = serializedData.UserName;
                string QueryStringPassword = serializedData.Password;
                if (QueryStringUserID != null && QueryStringMID != null && QueryStringKMS != null && QueryStringUserName != null && QueryStringPassword != null)
                {
                    HttpContext.Current.Session["QUERY_USERID"] = QueryStringUserID;
                    HttpContext.Current.Session["QUERY_MID"] = QueryStringMID;
                    HttpContext.Current.Session["QUERY_KMS"] = QueryStringKMS;
                    HttpContext.Current.Session["QUERY_UserName"] = QueryStringUserName;
                    HttpContext.Current.Session["QUERY_Password"] = QueryStringPassword;
                    //Response.Redirect("/Default.aspx");
                    checkLogin(QueryStringUserName, QueryStringPassword);
                }
            }
            catch (FormatException ex)
            {
                Session.Clear();
                //Response.Redirect("http://admin.wbecscegovernance.com/LoginManager/RedirectfromOtherDomain");
                //Response.Redirect("http://admin.wbecscegovernance.com/LoginManager/RedirectfromOtherDomain");
            }
            catch (ArgumentException ex)
            {
                Session.Clear();
                //Response.Redirect("http://admin.wbecscegovernance.com/LoginManager/RedirectfromOtherDomain");
            }
            //catch (Exception ex)
            //{
            //    Session.Clear();
            //    Response.Redirect("http://localhost:28883/LoginManager/RedirectfromOtherDomain");
            //}


            //var queryData = Request.QueryString["Data"];
            //if (queryData != null)
            //{
            //    Dictionary<string, string> dcs = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(queryData);
            //    if (dcs.ContainsKey("sessionCookie"))
            //    {
            //        if (Request.Cookies["ASP.NET_SessionId"] == null)
            //        {
            //            HttpCookie myCookie = new HttpCookie("ASP.NET_SessionId");
            //            myCookie.Value = dcs["sessionCookie"];
            //            Response.Cookies.Add(myCookie);
            //        }else
            //        {
            //            Request.Cookies["ASP.NET_SessionId"].Value= dcs["sessionCookie"];
            //        }
            //    }

            //    if (dcs.ContainsKey("logOutRedirection"))
            //    {
            //        Session["logOutRedirection"] = dcs["logOutRedirection"];
            //    }
            //    if (dcs.ContainsKey("redirectTo"))
            //    {
            //        redirection.Value=dcs["redirectTo"];
            //    }
            //}
        }
    }
    protected void checkLogin(string UserID,string Password)
    {
        /*
        string UserName = Page.Request["txtUserName"];
        string Password = Page.Request["txtPass"];
        //string strQuery = "select USERID, USERNAME, PASSWORD from bellevuepayhr.MST_USER where USERNAME ='" + UserName + "' and PASSWORD= '" + Password + "' ";
        DataTable dt = DBHandler.GetResult("Get_LoginCredentials", UserName, Password);

        //DataRow dr = DBHandler.GetResult("GET_LoginStatus", UserID, Password).Rows[0];
        int cnt = dt.Rows.Count;
        switch (cnt)
        {
            case 1://Successful
                //Session[SiteConstants.SSN_DR_USER_DETAILS] = dr;
                Session[SiteConstants.SSN_INT_USER_ID] = dt.Rows[0]["UserID"].ToString();
                Session[SiteConstants.SSN_USERTYPECODE] = dt.Rows[0]["TypeCode"].ToString();
                Response.Redirect("Default.aspx");
                break;
            case 2://Locked
                //this.Page.RegisterStartupScript("Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                break;
            case 0://Failed
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Failed; Please provide correct User Name and Password')</script>");
                break;
            //return "Login Failed; Please provide correct User Name and Password";
        }
          */
        try
        {
            //string UserID = Page.Request["txtUserName"];
            //string Password = Page.Request["txtPass"];
            DataRow dr = DBHandler.GetResult("Get_LoginCredentials", UserID, Password).Rows[0];

            switch ((int)dr[0])
            {
                case 1://Successful
                    Session[SiteConstants.SSN_DR_USER_DETAILS] = dr;
                    Session[SiteConstants.SSN_INT_USER_ID] = dr["UserID"];
                    Session[SiteConstants.SSN_USERTYPECODE] = dr["TypeCode"];
                    //Session[SiteConstants.SSN_SECTOR_ID] = 1;

                    Response.Redirect("Default.aspx");
                    break;
                case 2://Locked
                    Response.Redirect("http://localhost:28883/LoginManager/RedirectfromOtherDomain");
                    //this.Page.RegisterStartupScript("Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    break;
                case 3://Failed
                    Response.Redirect("http://localhost:28883/LoginManager/RedirectfromOtherDomain");
                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Failed; Please provide correct User Name and Password')</script>");
                    break;
                case 4://Failed
                    Response.Redirect("http://localhost:28883/LoginManager/RedirectfromOtherDomain");
                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('The User is Blocked Due to Non Payment of Due Amount')</script>");
                    break;
                    //return "Login Failed; Please provide correct User Name and Password";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        /*
        string UserName = Page.Request["txtUserName"];
        string Password = Page.Request["txtPass"];
        //string strQuery = "select USERID, USERNAME, PASSWORD from bellevuepayhr.MST_USER where USERNAME ='" + UserName + "' and PASSWORD= '" + Password + "' ";
        DataTable dt = DBHandler.GetResult("Get_LoginCredentials", UserName, Password);

        //DataRow dr = DBHandler.GetResult("GET_LoginStatus", UserID, Password).Rows[0];
        int cnt = dt.Rows.Count;
        switch (cnt)
        {
            case 1://Successful
                //Session[SiteConstants.SSN_DR_USER_DETAILS] = dr;
                Session[SiteConstants.SSN_INT_USER_ID] = dt.Rows[0]["UserID"].ToString();
                Session[SiteConstants.SSN_USERTYPECODE] = dt.Rows[0]["TypeCode"].ToString();
                Response.Redirect("Default.aspx");
                break;
            case 2://Locked
                //this.Page.RegisterStartupScript("Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                break;
            case 0://Failed
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Failed; Please provide correct User Name and Password')</script>");
                break;
            //return "Login Failed; Please provide correct User Name and Password";
        }
          */
        try
        {
            string UserID = Page.Request["txtUserName"];
            string Password = Page.Request["txtPass"];
            DataRow dr = DBHandler.GetResult("Get_LoginCredentials", UserID, Password).Rows[0];

            switch ((int)dr[0])
            {
                case 1://Successful
                    Session[SiteConstants.SSN_DR_USER_DETAILS] = dr;
                    Session[SiteConstants.SSN_INT_USER_ID] = dr["UserID"];
                    Session[SiteConstants.SSN_USERTYPECODE] = dr["TypeCode"];
                    //Session[SiteConstants.SSN_SECTOR_ID] = 1;

                    Response.Redirect("Default.aspx");
                    break;
                case 2://Locked
                    Response.Redirect("http://localhost:28883/LoginManager/RedirectfromOtherDomain");
                    //this.Page.RegisterStartupScript("Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    break;
                case 3://Failed
                    Response.Redirect("http://localhost:28883/LoginManager/RedirectfromOtherDomain");
                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Failed; Please provide correct User Name and Password')</script>");
                    break;
                case 4://Failed
                    Response.Redirect("http://localhost:28883/LoginManager/RedirectfromOtherDomain");
                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('The User is Blocked Due to Non Payment of Due Amount')</script>");
                    break;
                //return "Login Failed; Please provide correct User Name and Password";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }
    public class QueryData
    {
        public string UserID { get; set; }
        public string MID { get; set; }
        public string KMS { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}