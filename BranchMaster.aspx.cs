﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class BranchMaster : System.Web.UI.Page
{
    public static Boolean falseLink = false;
    static string hdnBranchID = "";
    static string hdnBankID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
              //  int BankID = Convert.ToInt32(hdnBankID.Value = "0") ;
               // string BankName = hdnBankName.Value = string.Empty;
                string BranchCondition = "PageLoad";
           //     hdnPagingCondition.Value = "PageLoad";
               
               // gvtbl.InnerHtml = GetGridControlContent("~/GridviewTemplete.ascx", prefix, BankID, BankName, BranchCondition);
             //   gvtbl.InnerHtml = AjaxPaging(0, 10, BranchCondition, BankID, BankName);
                //PopulateGrid();
            }
           // hdnFalsePaging.Value = Convert.ToString(falseLink);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected DataTable drpload(string str)
    {
        try
        {
            if (str == "Bank")
            {
                DataTable dt = DBHandler.GetResult("Load_Bank");
                return dt;
            }
            else
            {
                return null;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dtstr = DBHandler.GetResult("Get_Branch");
            //tbl.DataSource = dtstr;
            //tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGridOnBankSelection()
    {
        try
        {
            DropDownList ddlbank = (DropDownList)dv.FindControl("ddlBank");
            string BankID = ddlbank.SelectedValue.ToString();
            string BankName = ddlbank.SelectedItem.Text;
            DataSet ds = DBHandler.GetResults("Get_BranchbyBankID", BankID);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                dt.Columns.Add("BankName");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["BankName"] = BankName;
                }
               // tbl.DataSource = dt;
                //tbl.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            PopulateGridOnBankSelection();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //tbl.PageIndex = e.NewPageIndex;
        PopulateGridOnBankSelection();
        if (!IsPostBack)
        {
            PopulateGrid();
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);

                hdnBranchID = e.CommandArgument.ToString();
                UpdateDeleteRecord(1, e.CommandArgument.ToString());

                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_BankandBranch", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                DropDownList ddlBank = (DropDownList)dv.FindControl("ddlBank");
                TextBox txtBranch = (TextBox)dv.FindControl("txtBranch");
                ddlBank.SelectedValue = dtResult.Rows[0]["BankID"].ToString();
                txtBranch.Text = dtResult.Rows[0]["Branch"].ToString();
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_BankByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtBankCode = (TextBox)dv.FindControl("txtBankCode");
                TextBox txtBankName = (TextBox)dv.FindControl("txtBankName");
                TextBox txtBranch = (TextBox)dv.FindControl("txtBranch");
                TextBox txtIFSC = (TextBox)dv.FindControl("txtIFSC");

                DBHandler.Execute("Insert_Bank", txtBankCode.Text, txtBankName.Text, txtBranch.Text,
                    txtIFSC.Text, Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Add";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {
                DropDownList ddlBank = (DropDownList)dv.FindControl("ddlBank");
                TextBox txtBranch = (TextBox)dv.FindControl("txtBranch");
                TextBox txtIFSC = (TextBox)dv.FindControl("txtIFSC");
                TextBox txtMICR = (TextBox)dv.FindControl("txtMICR");

                int ID =Convert.ToInt32(hdnBranchID);// Convert.ToInt32(hdnBranchID.Value);

                DBHandler.Execute("Update_Branch", ID, ddlBank.SelectedValue, txtBranch.Text,
                    txtIFSC.Text, txtMICR.Text, Session[SiteConstants.SSN_INT_USER_ID]);
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");

        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string AjaxGetGridCtrl(string prefix, string BankID, string BankName, string BranchCondition)
    {
        try
        {
            return GetGridControlContent("~/GridviewTemplete.ascx", prefix, BankID, BankName, BranchCondition);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private static String GetGridControlContent(String controlLocation, string prefix, string BankID, string BankName, string BranchCondition)
    {
        try
        {
            var page = new Page();

            var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

            userControl.Prefix = prefix;
            userControl.BankID = BankID;
            userControl.BankNam = BankName;
            userControl.BranchCondition = BranchCondition;

            page.Controls.Add(userControl);
            String htmlContent = "";

            using (var textWriter = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, textWriter, false);
                htmlContent = textWriter.ToString();
            }
            return htmlContent;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string AjaxPaging(int pgIndex, int pgSize, string PagingCondition, int BankID, string BankName)
    {
        return GetDetailsbyPaging("~/GridviewTemplete.ascx", pgIndex, pgSize, PagingCondition, BankID, BankName);
    }
    private static String GetDetailsbyPaging(String controlLocation, int pgIndex, int pgSize, string PagingCondition, int BankID, string BankName)
    {
        var page = new Page();

        var userControl = (GridviewTemplete)page.LoadControl(controlLocation);

        if (pgIndex < 0)
        {
            pgIndex = 0;
            falseLink = true;
        }

        userControl.PageSize = pgSize;
        userControl.PageIndex = pgIndex;
        userControl.PagingCondition = PagingCondition;
        userControl.PagingBankID = BankID;
        userControl.PagingBankName = BankName;

        page.Controls.Add(userControl);

        String htmlContent = "";

        using (var textWriter = new StringWriter())
        {
            HttpContext.Current.Server.Execute(page, textWriter, false);
            htmlContent = textWriter.ToString();
        }
        return htmlContent;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetBranchDetailbyBank(string BankID, string BankName)
    {
        try
        {
            string prefix = "0";
            string BranchCondition = "OnBankSelection";
            return GetGridControlContent("~/GridviewTemplete.ascx", prefix, BankID, BankName, BranchCondition);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateBranch(string BranchID, string BranchName, string IFSC, string MICR, string BankID)
    {
        string JSONval = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Update_Branch", BranchID, BankID, BranchName, IFSC, MICR, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                if (dt.Rows.Count > 0)
                {
                    string result = dt.Rows[0]["Result"].ToString();
                    if (result == "Success")
                        JSONval = result.ToString();
                    else
                        JSONval = result.ToString();
                }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONval;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_Baranch(string BankID, string BranchName, string IFSC, string MICR)
    {
        string JSONval = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Insert_BankBranch", BankID, BranchName, IFSC, MICR, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (dt.Rows.Count > 0)
            {
                string result = dt.Rows[0]["BranchID"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONval;
    }

    [WebMethod]
    public static string GET_SetBranchID(int type, string BranchID)
    {
        string msg = "";
        if (type == 0) { hdnBranchID = ""; }
        else { hdnBranchID = BranchID; }
        return msg;
    }
}