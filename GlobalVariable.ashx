﻿<%@ WebHandler Language="C#" Class="GlobalVariable" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public class GlobalVariable : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
{
    
    public void ProcessRequest (HttpContext context) {

        //if (context.Request.Files.Count > 0)
        //{
            string SalFinYear = context.Request.QueryString["salfinyear"];
            string AccFinYear = context.Request.QueryString["accfinyear"];
            string secid = context.Request.QueryString["secid"];
        
            HttpContext.Current.Session[SiteConstants.SSN_SALFIN] = SalFinYear;
            HttpContext.Current.Session[SiteConstants.SSN_SALACC] = AccFinYear;
           // HttpContext.Current.Session[SiteConstants.SSN_SECTORID] = secid;
       // }
        //context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}