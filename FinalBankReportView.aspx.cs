﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Management;
using System.Runtime.InteropServices;

public partial class FinalBankReportView : System.Web.UI.Page
{
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
    static string ExportFileName = "";

    String ConnString = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        GetPrinterList();
        SetPaperSize();
        BindReport(crystalReport);
    }

    protected void SetPaperSize()
    {
        ddlPaperSize.Items.Clear();
        ddlPaperSize.Items.Add("Page - A4");
    }

    protected void BindReport(ReportDocument crystalReport)
    {

        ConnString = DataAccess.DBHandler.GetConnectionString();
        //ExportFileName = "";
        //ReportNameVal = Convert.ToInt32(HttpContext.Current.Request.QueryString["ReportNameVal"]);
        //SectorID = Convert.ToInt32(HttpContext.Current.Request.QueryString["SectorId"]);
        //SalMonthId = Convert.ToInt32(HttpContext.Current.Request.QueryString["SalMonthId"]);
        //SalMonth = Convert.ToString(HttpContext.Current.Request.QueryString["SalMonth"]);
        //SqlConnection con = new SqlConnection(ConnString); //"server=192.168.1.25; database=db_kmda; uid= db_kmda;pwd=infotech"
        //DataTable dt = new DataTable();
        //SqlDataAdapter adp = new SqlDataAdapter("Select SectorName from kmda.MST_Sector where SectorID=" + SectorID + "", con);
        //adp.Fill(dt);
        //if (dt.Rows.Count > 0)
        //{
        //    ExportSectorName = dt.Rows[0]["SectorName"].ToString();
        //}


       // if (ReportNameVal == 1)
        //{
        crystalReport.Load(Server.MapPath("~/Reports/Admin_bank_slip_cum_list_running.rpt"));    //Bank Slip Final.rpt
            //crystalReport.RecordSelectionFormula = "{MST_Sector.SectorID}=" + SectorID + " and {MST_SalaryMonth.SalMonthID}=" + SalMonthId + "  and {MST_Employee.Status} in ['Y','S']";
            //ExportFileName = ExportSectorName + "_" + "Bank Slip " + "(" + SalMonth + ")";
            crystalReport.DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
            crystalReport.DataSourceConnections[0].IntegratedSecurity = false;

            crystalReport.SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
            crystalReport.Refresh();
            CrystalReportViewer1.DisplayToolbar = true;
            CrystalReportViewer1.Zoom(150);
            CrystalReportViewer1.Visible = true;
            CrystalReportViewer1.ReportSource = crystalReport;
        //}
    }

    private bool GetPrinterList()
    {
        //ddlPrinterName.Items.Clear();
        bool retVal = false;
        PrintDocument printtdoc = new PrintDocument();
        //prt.PrinterSettings.PrinterName returns the name of the Default Printer
        string strDefaultPrinterName = printtdoc.PrinterSettings.PrinterName;
        //this will loop through all the Installed printers and add the Printer Names to a ComboBox.
        foreach (String strPrinter in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
        {
            //ddlPrinterName.Items.Add(strPrinter);

            //This will set the ComboBox Index where the Default Printer Name matches with the current Printer Name returned by for loop
            if (strPrinter.CompareTo(strDefaultPrinterName) == 0)
            {
                //ddlPrinterName.Text = strDefaultPrinterName;
                retVal = true;
            }
        }

        return retVal;
    }

    protected void ExportPDF(object sender, EventArgs e)
    {

        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);

        ExportFormatType formatType = ExportFormatType.NoFormat;
        switch (rbFormat.SelectedItem.Value)
        {
            case "Excel":
                formatType = ExportFormatType.Excel;
                break;
            case "PDF":
                formatType = ExportFormatType.PortableDocFormat;
                break;
        }
        //if (PanNoReq == "0")
        //{
        crystalReport.ExportToHttpResponse(formatType, Response, true, ExportFileName);
        Response.End();
        //}
        //else
        //{
        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + display + "');", true);
        //}        
    }

    public void OnConfirmprint(object sender, EventArgs e)
    {
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "Yes")
        {
            ReportDocument crystalReport = new ReportDocument();
            BindReport(crystalReport);

            ExportFormatType formatType = ExportFormatType.NoFormat;
            switch (rbFormat.SelectedItem.Value)
            {
                case "Excel":
                    formatType = ExportFormatType.Excel;
                    break;
                case "PDF":
                    formatType = ExportFormatType.PortableDocFormat;
                    break;
            }

            //if (PanNoReq == "0")
            //{
            crystalReport.ExportToHttpResponse(formatType, Response, false, ExportFileName);
            Response.End();
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + display + "');", true);
            //}
        }
        //string confirmValue = Request.Form["confirm_value"];
        //if (confirmValue == "Yes")
        //{
        //    crystalReport.Refresh();
        //    crystalReport.PrintOptions.PrinterName = ddlPrinterName.SelectedItem.Text;


        //    if (ddlPaperSize.SelectedItem.Text == "Page - A4")
        //    {
        //        crystalReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
        //        crystalReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
        //        crystalReport.PrintToPrinter(1, false, 1, crystalReport.FormatEngine.GetLastPageNumber(new CrystalDecisions.Shared.ReportPageRequestContext()));
        //    }
        //    crystalReport.Refresh();
        //}

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        this.crystalReport.Close();
        this.crystalReport.Dispose();
    }
}