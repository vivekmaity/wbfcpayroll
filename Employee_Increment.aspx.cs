﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class Employee_Increment : System.Web.UI.Page
{
    public string strFrm1FA = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Button btnUpdate = (Button)dv.FindControl("btnUpdate");
                Button btnAll = (Button)dv.FindControl("btnAll");
                Button btnSearchEmp = (Button)dv.FindControl("btnSearchEmp");
                Button btncmdSave = (Button)dv.FindControl("cmdSave");
                //btnUpdate.Enabled = false;
                //btnAll.Enabled = false;
                btncmdSave.Enabled = false;

                btnUpdate.Visible = false;
                btnAll.Visible = false;
                //btncmdSave.Visible = false;

                btnSearchEmp.Enabled = false;

                HttpContext.Current.Session["EmployeeIncrement"] = null;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnViewIncr_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox txtEmpNoforSearch = (TextBox)dv.FindControl("txtEmpNoforSearch");
            txtEmpNoforSearch.Text = "";
            DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
            int Secid = Convert.ToInt32(Sectors.SelectedValue.ToString());
            if (Secid != 0)
            {
                string value = PopulateGrid();
                PopulateGridtoPrint();
                if (value == "Yes")
                {
                    DataTable dt = DBHandler.GetResult("Check_EmployeeAvailable", Secid);
                    if (dt.Rows.Count > 0)
                    {
                        string countValue = dt.Rows[0]["Count"].ToString();
                        //string countValue1 = dt.Rows[0]["Count1"].ToString();
                        if (countValue == "" || countValue == "0")
                        {
                            Button btnUpdate = (Button)dv.FindControl("btnUpdate");
                            Button btnAll = (Button)dv.FindControl("btnAll");
                            Button btncmdSave = (Button)dv.FindControl("cmdSave");

                            //btncmdSave.Enabled = false;
                            //btnUpdate.Enabled = true;
                            //btnAll.Enabled = true;

                            //btncmdSave.Visible = false;
                            btnUpdate.Visible = true;
                            btnAll.Visible = true;
                        }
                        else
                        {
                            Button btnUpdate = (Button)dv.FindControl("btnUpdate");
                            Button btnAll = (Button)dv.FindControl("btnAll");
                            Button btncmdSave = (Button)dv.FindControl("cmdSave");

                            //btncmdSave.Enabled = true;
                            //btnUpdate.Enabled = false;
                            //btnAll.Enabled = false;

                            btncmdSave.Visible = true;
                            btnUpdate.Visible = false;
                            btnAll.Visible = false;
                            btncmdSave.Enabled = true;
                            
                        }

                        
                    }


                    Button btnSearchEmp = (Button)dv.FindControl("btnSearchEmp");
                    btnSearchEmp.Enabled = true;
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No data for increment !')</script>");
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Select Sector.')</script>");
                Sectors.Focus();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnSearchEmp_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox txtEmpNoforSearch = (TextBox)dv.FindControl("txtEmpNoforSearch");
            string EmpNo = txtEmpNoforSearch.Text;
            if (EmpNo != "")
            {
                TextBox PayMonths = (TextBox)this.Master.FindControl("txtPayMonths");
                DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
                string Secid = Sectors.SelectedValue.ToString();

                string PayMonth = hdnPayMonth.Value.Equals("") ? PayMonths.Text.ToString() : hdnPayMonth.Value;
                //DateTime dtPayMonth = DateTime.ParseExact(PayMonth, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DataSet ds = DBHandler.GetResults("Get_DetailforEmpIncrementbyEmpNo", Secid.Equals("") ? Session[SiteConstants.SSN_SECTORID] : Secid, PayMonth, EmpNo);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string name = dt.Columns[0].ToString();
                    GridView grdDetails = (GridView)dv.FindControl("tbl");
                    grdDetails.DataSource = dt;
                    grdDetails.DataBind();
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No any Record Found.')</script>");
                    txtEmpNoforSearch.Text = "";
                    txtEmpNoforSearch.Focus();

                }
            }
            else
            {
                txtEmpNoforSearch.Focus();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
          DataTable dt=(DataTable)HttpContext.Current.Session["EmployeeIncrement"];
          if (dt !=null)
          {
              if (dt.Rows.Count > 0)
              {
                  GridView grdDetails = (GridView)dv.FindControl("tbl");
                  grdDetails.DataSource = dt;
                  grdDetails.DataBind();
              }
          }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridView grdDetails = (GridView)dv.FindControl("tbl");
            GridViewRow grdrow = (GridViewRow)((CheckBox)sender).NamingContainer;
            int rowIndex = grdrow.RowIndex;
            CheckBox chkSelect;
            int i = 0;
            foreach (GridViewRow grdRow in grdDetails.Rows)
            {

                chkSelect = (CheckBox)(grdDetails.Rows[i].Cells[0].FindControl("chkSelect"));
                if (chkSelect.Checked == false)
                    chkSelect.Checked = true;
                i++;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridView grdDetails = (GridView)dv.FindControl("tbl");
            GridViewRow grdrow = (GridViewRow)((CheckBox)sender).NamingContainer;
            int rowIndex = grdrow.RowIndex;
            CheckBox chkSelt = (CheckBox)(grdDetails.Rows[rowIndex].Cells[0].FindControl("chkSelect"));
            if (chkSelt.Checked == true)
            {
                CheckBox chkSelect;
                int i = 0;
                foreach (GridViewRow grdRow in grdDetails.Rows)
                {

                    chkSelect = (CheckBox)(grdDetails.Rows[i].Cells[0].FindControl("chkSelect"));
                    if (chkSelect.Checked == true)
                        chkSelect.Checked = false;
                    i++;
                }
                chkSelect = (CheckBox)(grdDetails.Rows[rowIndex].Cells[0].FindControl("chkSelect"));
                chkSelect.Checked = true;
                Label lblEmployeeID = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblEmployeeID"));
                Label lblEmpNo = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblEmpNo"));
                Label lblEmpName = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblEmpName"));
                Label lblOldBasic = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblOldBasic"));
                Label lblIncrPer = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblIncrPer"));
                Label lblPayScaleType = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblPayScaleType"));
                Label lblPayScaleID = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblPayScaleID"));
                Label lblPayBand = (Label)(grdDetails.Rows[rowIndex].Cells[0].FindControl("lblPayBand"));
                hdnPayBand.Value = lblPayBand.Text;
                hdnEmployeeID.Value = lblEmployeeID.Text;
                DataSet ds = DBHandler.GetResults("Get_PayScalebyPayScaleType", lblPayScaleType.Text);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DropDownList ddlNewPayScale = (DropDownList)dv.FindControl("ddlNewPayScale");
                    ddlNewPayScale.DataSource = dt;
                    ddlNewPayScale.DataBind();
                    ddlNewPayScale.SelectedValue = lblPayScaleID.Text;
                }

                string EmpNo = lblEmpNo.Text;
                string EmpName = lblEmpName.Text;
                string OldBasic = lblOldBasic.Text;
                string IncrPer = lblIncrPer.Text;
                TextBox txtEmpNo = (TextBox)dv.FindControl("txtEmpNo");
                TextBox txtEmpName = (TextBox)dv.FindControl("txtEmpName");
                TextBox txtCurrBasic = (TextBox)dv.FindControl("txtCurrBasic");
                TextBox txtIncrPer = (TextBox)dv.FindControl("txtIncrPer");
                TextBox txtNewBasic = (TextBox)dv.FindControl("txtNewBasic");
                DropDownList ddlDecline = (DropDownList)dv.FindControl("ddlDecline");

                txtEmpNo.Text = EmpNo;
                txtEmpName.Text = EmpName;
                txtCurrBasic.Text = OldBasic;
                txtIncrPer.Text = IncrPer;
                ddlDecline.SelectedValue = "N";

                int incr = Convert.ToInt32(IncrPer); int oldbasic = Convert.ToInt32(OldBasic);
                int NewBasic = ((incr * oldbasic) / 100) + oldbasic;
                txtNewBasic.Text = Convert.ToString(NewBasic);
            }
            else
            {
                Clear();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void UpdateDeleteRecord(int flag, string SanDate, string EmployeeID)
    {
        try
        {
            if (flag == 1)
            {
                    DateTime dtSanc = DateTime.ParseExact(SanDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DataTable dtResult = DBHandler.GetResult("Update_LoanSanction", dtSanc.ToShortDateString(), EmployeeID);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView grdDetails = (GridView)dv.FindControl("tbl");
        grdDetails.PageIndex = e.NewPageIndex;
        PopulateGrid();
    }
    protected void tbl_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //((CheckBox)e.Row.FindControl("chkAll")).Attributes.Add("OnClick", "javascript:return SelectAll('" + ((CheckBox)e.Row.FindControl("chkAll")).ClientID + "')");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void Clear()
    {
        try
        {
            TextBox txtEmpNo = (TextBox)dv.FindControl("txtEmpNo");
            TextBox txtEmpName = (TextBox)dv.FindControl("txtEmpName");
            TextBox txtCurrBasic = (TextBox)dv.FindControl("txtCurrBasic");
            DropDownList ddlNewPayScale = (DropDownList)dv.FindControl("ddlNewPayScale");
            TextBox txtIncrPer = (TextBox)dv.FindControl("txtIncrPer");
            TextBox txtNewBasic = (TextBox)dv.FindControl("txtNewBasic");
            TextBox txtIncrOn = (TextBox)dv.FindControl("txtIncrOn");
            DropDownList ddlDecline = (DropDownList)dv.FindControl("ddlDecline");
            TextBox txtRemarks = (TextBox)dv.FindControl("txtRemarks");
            txtEmpNo.Text = "";
            txtEmpName.Text = "";
            txtCurrBasic.Text = "";
            ddlNewPayScale.SelectedValue = "";
            txtIncrPer.Text = "";
            txtNewBasic.Text = "";
            txtIncrOn.Text = "";
            ddlDecline.SelectedValue = "";
            txtRemarks.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string PopulateGrid()
    {
        string Flag = "";
        try
        {
            TextBox PayMonths = (TextBox)this.Master.FindControl("txtPayMonths");
            DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
            string Secid = Sectors.SelectedValue.ToString();

            string PayMonth = hdnPayMonth.Value.Equals("") ? PayMonths.Text.ToString() : hdnPayMonth.Value;   //"01/" + hdnPayMonth.Value;
            DataSet ds = DBHandler.GetResults("Get_DetailforEmpIncrement", Secid, PayMonth);
            DataTable dt = ds.Tables[0]; 
            if (dt.Rows.Count > 0)
            {
                string Result = dt.Rows[0]["EmployeeID"].ToString();
                if (Result!="")
                {
                    HttpContext.Current.Session["EmployeeIncrement"] = dt;
                    string name = dt.Columns[0].ToString();
                    GridView grdDetails = (GridView)dv.FindControl("tbl");
                    grdDetails.DataSource = dt;
                    grdDetails.DataBind();
                    Flag = "Yes";
                }
                else
                {
                    Flag = "No";
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Flag;
    }
    private void PopulateGridtoPrint()
    {
        try
        {
            TextBox PayMonths = (TextBox)this.Master.FindControl("txtPayMonths");
            DropDownList Sectors = (DropDownList)this.Master.FindControl("ddlSector");
            string Secid = Sectors.SelectedValue.ToString();

            string PayMonth = hdnPayMonth.Value.Equals("") ? PayMonths.Text.ToString() : hdnPayMonth.Value;
            DataSet ds = DBHandler.GetResults("Get_DetailforEmpIncrement", Secid, PayMonth);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                string name = dt.Columns[0].ToString();
                GridView grdDetail = (GridView)dv.FindControl("tbltoPrint");
                grdDetail.DataSource = dt;
                grdDetail.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateIncrement(string EmpID, string OldBasic, string NewPayscaleID, string NewBasic, string IncrPer, string IncrOn, string Decline, string Remarks, string SectorID)
    {
        string JSONVal = ""; string B = "B";
        try
        {
            DataTable dt = DBHandler.GetResult("Update_EmployeeIncrement", EmpID, OldBasic, NewPayscaleID, NewBasic, IncrPer, IncrOn, Decline, Remarks, B, SectorID);

                if (dt.Rows.Count > 0)
                {
                    string result = "success";
                    JSONVal = result.ToJSON();
                }
                else
                {
                    string result = "fail";
                    JSONVal = result.ToJSON();
                }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateIncrementAll(string EmpID, string OldBasic, string NewPayscaleID, string NewBasic, string IncrPer, string IncrOn, string Decline, string Remarks, string SectorID)
    {
        string JSONVal = ""; string A = "A";
        try
        {
            if (Decline != "Y")
            {
                DataTable dt = DBHandler.GetResult("Update_EmployeeIncrement", EmpID.Equals("") ? DBNull.Value : (object)Convert.ToInt32(EmpID),  
                     OldBasic.Equals("") ? DBNull.Value : (object)Convert.ToDouble(OldBasic),
                     NewPayscaleID,
                     NewBasic.Equals("") ? DBNull.Value : (object)Convert.ToDouble(NewBasic),
                     IncrPer.Equals("") ? DBNull.Value : (object)Convert.ToInt32(IncrPer),
                    IncrOn, Decline, Remarks, A, SectorID);

                if (dt.Rows.Count > 0)
                {
                    string result = "success";
                    JSONVal = result.ToJSON();
                }
                else
                {
                    string result = "fail";
                    JSONVal = result.ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ApplyIncrement(string SecID)
    {
        string JSONVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("updt_emp_details", SecID);

               string result = "success";
               JSONVal = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Check_EmployeeLPC(string SectorID)
    {
        string JSONVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Check_EmployeeAvailable", SectorID);
            if (dt.Rows.Count > 0)
            {
                string countValue1 = dt.Rows[0]["Count1"].ToString();
                if (countValue1 == "" || countValue1 == "0")
                {
                    string result = "success";
                    JSONVal = result.ToJSON();
                }
                else
                {
                    string result = "Exist";
                    JSONVal = result.ToJSON();
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Find_EmployeeDetail(string EmpNo)
    {
        string jsonVal = "";
        try
        {

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return jsonVal;
    }

    [WebMethod]
    public static string GetPayMonth(int SecID, string SalFinYear)
    {
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID, SalFinYear);
        DataTable dtSalMonth = ds1.Tables[0];

        List<PayMonth> listPayMonth = new List<PayMonth>();

        if (dtSalMonth.Rows.Count > 0)
        {
            for (int i = 0; i < dtSalMonth.Rows.Count; i++)
            {
                PayMonth objst = new PayMonth();

                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);

                listPayMonth.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listPayMonth);

    }
    public class PayMonth
    {
        public string PayMonths { get; set; }
    }
}