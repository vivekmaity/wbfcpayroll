﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always"  CodeFile="SalaryProcess.aspx.cs" Inherits="GroupMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            $("#txtGroupCode").rules("add", { required: true, messages: { required: "Please enter Group Code"} });
            $("#txtGroupName").rules("add", { required: true, messages: { required: "Please enter Group Name"} });
        }           
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
    <asp:ScriptManager ID="pop" runat="server"></asp:ScriptManager>
			<div class="container">
				<div class="row main-row">
					<div class="12u">
						<section>
							<h2>SALARY PROCESS</h2>						
						</section>
                    <table width="98%"   style="border:solid 2px lightblue;  "  >
                    <tr>
                        <td style="padding:15px;">
                            <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                            <InsertItemTemplate>
                                <table align="center" width="100%">
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Salary Month &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtSalMonth"  autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="7" Enabled="false" Text="01/2015"></asp:TextBox>                            
                                        </td>
                                        
                                    </tr>
                                   <tr>
                                        <td style="padding:5px;"><span class="headFont">Final Process&nbsp;&nbsp; </span><span class="require">*</span> </td>
                                                    <td style="padding:5px;" >:</td>
                                                    <td style="padding:5px;" align="left"><asp:DropDownList ID="ddlFPFlag"  
                                                        CssClass="textbox" runat="server" 
                                                        AppendDataBoundItems="true"  autocomplete="off" >
                                                       <asp:ListItem Text="(Select Process)" Value=""></asp:ListItem> 
                                                       <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                       <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                       </asp:DropDownList>                            
                                                   </td>
                                        <td>
                                            <asp:Button ID="cmdGenerate" runat="server" Text="Generate" Width="100px" CssClass="Btnclassname" />
                                            <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="p1" CancelControlID="cmdcel"
                                            OkControlID="butOk" TargetControlID="cmdGenerate" BackgroundCssClass="ModalPopBg">

                                            </asp:ModalPopupExtender>
                                            <asp:Panel ID="p1" runat="server" style="display:none;" CssClass="modalPopup">
                                            <div class="MyPopUp">
                                                <table>
                                                    <tr>
                                                        <td colspan="2"><asp:Label ID="Label1" runat="server">This page is used at the time of increment in the month of july</asp:Label></td>
                                                        <%--<td></td>--%>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp</td>
                                                        <td>&nbsp</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp</td>
                                                        <td>&nbsp</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp</td>
                                                        <td>&nbsp</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp</td>
                                                        <td>&nbsp</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp</td>
                                                        <td>&nbsp</td>
                                                    </tr>
                                                    <tr>
                                                        <td><asp:Button ID="cmdcel" runat="server" Text="EXIT"  /></td>
                                                        <td><asp:Button ID="butOk" runat="server" Text="DONE"  /></td>
                                                    </tr>
                                                </table>
                                                
                                                

                                                
                                                
                                            </div>
                                            </asp:Panel>
                            <div id="modal_dialog" style="display: none">
                            This is a Modal Background popup
                            </div>
                                        </td>
                                   </tr>
                                </table>
                            </InsertItemTemplate>

                            <%--<EditItemTemplate>
                                <table align="center"  width="100%">
                                    <tr>
                                        <td style="padding:10px;"> <span class="headFont">Group Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtGroupCode" Text='<%# Eval("GroupCode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="1" Enabled="false"></asp:TextBox>                            
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("GroupID") %>'/>
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Group Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:10px;">:</td>
                                        <td style="padding:10px;" align="left" >
                                            <asp:TextBox ID="txtGroupName" Text='<%# Eval("Group") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10"></asp:TextBox>                            
                                           
                                        </td>
                                        
                                    </tr>
                                </table>
                            </EditItemTemplate>--%>
                            <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                <tr>
                                    <td style="padding:5px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td style="padding:5px;">&nbsp;</td>
                                    <td style="padding:5px;" align="left" >
                                    <div style="float:left;margin-left:100px;">
                                        
                                         
                                        <asp:Button ID="cmdProcess" runat="server" Text="Process" CommandName="Add"  
                                        Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' 
                                        OnClick="cmdProcess_Click" /> 

                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"   
                                        Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                            </asp:FormView>
                    
                        </td>
                    </tr>
                    <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                    <tr>
                        <td>
                            
                                
                        </td>
                    </tr>
                        </table>
					</div>
				</div>
			</div>
		</div>
</asp:Content>

