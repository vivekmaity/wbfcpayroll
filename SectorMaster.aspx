﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="SectorMaster.aspx.cs" Inherits="SectorMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function beforeSave() {
            $("#form1").validate();
            $("#txtSectorCode").rules("add", { required: true, messages: { required: "Please enter Sector Code"} });
            $("#txtSectorName").rules("add", { required: true, messages: { required: "Please enter Sector Name"} });
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
        $(function () {
            $("#txtPhone").keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 9) || (key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Sector  Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Sector Code &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtSectorCode" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3"></asp:TextBox>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Sector Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtSectorName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Smid &nbsp;&nbsp;<span class="require">*</span> </td>
                                                                <td style="padding: 5px;" class="labelCaption">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlsmid" DataSource='<%# drpload() %>'
                                                                        DataValueField="Smid" Height="23px"
                                                                        DataTextField="Smid" Width="217px" runat="server"
                                                                        AppendDataBoundItems="true" autocomplete="off">
                                                                        <asp:ListItem Text="(Select Smid)" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Smpsw &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtSmpsw" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <%-- **************************ADDED LATER*********************************** --%>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Address &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtAddress" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100" Text='<%# Eval("Address") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>

                                                                <td style="padding: 5px;"><span class="headFont">Phone No. &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPhone" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="11" Text='<%# Eval("Phone") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">FAX &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtFAx" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="11" Text='<%# Eval("Fax") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>

                                                                <td style="padding: 5px;"><span class="headFont">Head Office &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtHeadOff" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100" Text='<%# Eval("HeadOff") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                                <%--***************************************************************************** --%>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">TAN No. &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtTanNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" maxlength="100"  ></asp:TextBox>
                                                                </td>
                                                                <%--***************************************************************************** --%>
                                                            </tr>
                                                        </table>
                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 10px;"><span class="headFont">Sector Code&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 10px;">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:TextBox ID="txtSectorCode" autocomplete="off" Text='<%# Eval("SectorCode") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="3" Enabled="false"></asp:TextBox>
                                                                    <%--<asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("SectorID") %>' />--%>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Sector Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtSectorName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100" Text='<%# Eval("SectorName") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 10px;"><span class="headFont">Smid &nbsp;&nbsp;<span class="require">*</span> </td>
                                                                <td style="padding: 10px;" class="labelCaption">:</td>
                                                                <td style="padding: 10px;" align="left">
                                                                    <asp:DropDownList ID="ddlsmid" DataSource='<%# drpload() %>'
                                                                        DataValueField="Smid" Height="23px"
                                                                        DataTextField="Smid" Width="217px" runat="server"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Text="(Select Smid)" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">Smpsw &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtSmpsw" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="10" Text='<%# Eval("Smpsw") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <%--**********************Added later ***************************************************************** --%>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">Address &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtAddress" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100" Text='<%# Eval("Address") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>

                                                                <td style="padding: 5px;"><span class="headFont">Phone No. &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtPhone" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="11" Text='<%# Eval("Phone") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">FAX &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtFAx" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="11" Text='<%# Eval("Fax") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>

                                                                <td style="padding: 5px;"><span class="headFont">Head Office &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtHeadOff" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="100" Text='<%# Eval("HeadOff") %>' onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">TAN No. &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtTanNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("TanNo") %>'></asp:TextBox>
                                                                </td>
                                                                <%--***************************************************************************** --%>
                                                            </tr>
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                                <td style="padding: 5px;">&nbsp;</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"
                                                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();'
                                                                            OnClick="cmdSave_Click" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                                                            Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                                    DataKeyNames="SectorID" OnRowCommand="tbl_RowCommand" AllowPaging="true" PageSize="10"
                                                    CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                                    <AlternatingRowStyle BackColor="#FFFACD" />
                                                    <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Edit">
                                                            <HeaderStyle />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                    runat="server" ID="btnEdit" CommandArgument='<%# Eval("SectorID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                        <%--<asp:TemplateField HeaderText="Delete">
                                                            <HeaderStyle />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                    OnClientClick='return Delete();'
                                                                    CommandArgument='<%# Eval("SectorID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>--%>
                                                        <asp:BoundField DataField="SectorID" Visible="false" HeaderText="SectorID" />
                                                        <asp:BoundField DataField="SectorCode" HeaderText="Sector Code" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="SectorName"  HeaderText="Sector Name" />
                                                        <asp:BoundField DataField="Smid" HeaderText="Smid" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="Smpsw" HeaderText="Smpsw" ItemStyle-HorizontalAlign="Center"/>
                                                        <asp:BoundField DataField="Address" HeaderText="Address" />
                                                        <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                                        <asp:BoundField DataField="Fax" HeaderText="Fax" />
                                                        <asp:BoundField DataField="HeadOff" HeaderText="HeadOff" />
                                                        <asp:BoundField DataField="TanNo" HeaderText="TAN No" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

