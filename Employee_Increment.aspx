﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Employee_Increment.aspx.cs" Inherits="Employee_Increment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" />

    <link href="css/GridviewScroll.css" rel="stylesheet" />
    <script src="js/ValidationTextBox.js"></script>

<script type="text/javascript">
    function PrintDiv() {
        var divToPrint = $('#Print').html().trim();

        if (divToPrint != "") {
            var popupWin = window.open('', '_blank', '');
            popupWin.document.open();
            popupWin.document.write('<html><head><title>Print</title>');
            popupWin.document.write("<link href='css/style_form.css' rel='stylesheet' type='text/css' />");
            popupWin.document.write('</head><body onload="window.print()"><center>' + divToPrint + '</center></body></html>');
            popupWin.document.close();
        } else {
            alert('No Data To Print');
        }
    }
    $(document).ready(function () {
        $('#txtIncrOn').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            showButtonPanel: true,
            duration: 'slow'
        });

        $("#imgIncrOn").click(function () {
            $('#txtIncrOn').datepicker("show");
            return false;
        });
        $(function () {
            $("[id$=txtIncrOn]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
        });
        $("#ddlSector").change(function () {
            var s = $('#ddlSector').val();
            if (s != 0) {
                if ($("#ddlSector").val() != "Please select") {
                    var E = "{SecID: " + $('#ddlSector').val() + ", SalFinYear: '" + $('#txtSalFinYear').val() + "'}";
                    //alert(E);
                    var options = {};
                    options.url = "Employee_Increment.aspx/GetPayMonth";
                    options.type = "POST";
                    options.data = E;
                    options.dataType = "json";
                    options.contentType = "application/json";
                    options.success = function (listPayMonth) {
                        var t = jQuery.parseJSON(listPayMonth.d);
                        var PayMon = t[0]["PayMonths"];
                        var a = t.length;
                        $('#hdnPayMonth').val(PayMon);
                    };
                    $.ajax(options);
                }
            }
        });
        $("#ddlNewPayScale").change(function () {
            var gp = 0; var incrPer = $('#txtIncrPer').val();
            var P = $('#ddlNewPayScale').find('option:selected').text();
            if (P != "") {
                var a = P.split('-');
                for (var i = 0; i < a.length; i++) {
                    var min = a[1]; var V = a[2];
                    var max = V.substring(0, V.indexOf('('));
                    gp = V.substring(V.indexOf('(') + 1, V.indexOf(')'));
                }
                var PayBand = $('#hdnPayBand').val();
                var gpANDpayBand = parseInt(gp) + parseInt(PayBand);
                var NetgpANDpayBand = ((parseInt(gpANDpayBand) * 3) / 100) + gpANDpayBand;

                $("#txtNewBasic").val(Math.round(NetgpANDpayBand));
                alert(NetgpANDpayBand);
            }
        });
        $(document).ready(function () {
            //$("#txtEmpNoforSearch").keydown(function (event) {
            //    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            //(event.keyCode == 65 && event.ctrlKey === true) ||
            //(event.keyCode >= 35 && event.keyCode <= 39)) {

            //        return;
            //    }
            //    else {
            //        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            //            event.preventDefault();
            //            $("#Span6").html("Digits Only").show().fadeOut(1500);
            //            return false;
            //        }
            //    }
            //});
        });
        $("#btnUpdate").click(function (e) {
            e.preventDefault();
            var EmpID = $('#hdnEmployeeID').val();
            var OldBasic = $('#txtCurrBasic').val();
            var NewPayscaleID = $('#ddlNewPayScale').val();
            var NewBasic = $('#txtNewBasic').val();
            var IncrPer = $('#txtIncrPer').val();
            var IncrOn = $('#txtIncrOn').val();
            var Decline = $('#ddlDecline').val();
            var Remarks = $('#txtRemarks').val();
            var E = "{EmpID: '" + EmpID + "', OldBasic: '" + OldBasic + "', OldBasic: '" + OldBasic + "', NewPayscaleID: '" + NewPayscaleID + "' , NewBasic: '" + NewBasic + "',  IncrPer: '" + IncrPer + "', IncrOn: '" + IncrOn + "', Decline: '" + Decline + "', Remarks: '" + Remarks + "', SectorID: '" + $('#ddlSector').val() + "'}";
            $(".loading-overlay").show();
            if (EmpID != "" && EmpID != null) {
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/UpdateIncrement',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        $(".loading-overlay").hide();
                        var t = jQuery.parseJSON(D.d);
                        //alert(t);
                        if (t == "success") {
                            alert("Increment of this Employee is successfully Updated.");
                            $('#txtEmpNo').val('');
                            $('#txtEmpName').val('');
                            $('#txtCurrBasic').val('');
                            $('#ddlNewPayScale').val('');
                            $('#txtNewBasic').val('');
                            $('#txtIncrPer').val('');
                            $('#txtIncrOn').val('');
                            $('#ddlDecline').val('');
                            $('#txtRemarks').val('');
                        }
                    },
                    error: function (response) {
                        alert(response.d);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
            else {
                alert("Please Select Employee. Use CheckBox to Select the Employee.");
                return;
            }
        });

        $("#btnAll").click(function (e) {
            e.preventDefault();
            var EmpID = $('#hdnEmployeeID').val();
            var OldBasic = $('#txtCurrBasic').val();
            var NewPayscaleID = $('#ddlNewPayScale').val();
            var NewBasic = $('#txtNewBasic').val();
            var IncrPer = $('#txtIncrPer').val();
            var IncrOn = $('#txtIncrOn').val();
            var Decline = $('#ddlDecline').val();
            var Remarks = $('#txtRemarks').val();
            

           // *********************************************************************
            var E = "{SectorID: '" + $('#ddlSector').val() + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/Check_EmployeeLPC',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var tt = jQuery.parseJSON(D.d);
                    if (tt == "Exist") {
                        alert("Sorry ! You can not Increment for all Employee.\nBecause your one employee has been Transferred which is not accepted by the Other Sector.");
                        return false;
                    }
                    else {
                        var E = "{EmpID: '" + EmpID + "', OldBasic: '" + OldBasic + "', OldBasic: '" + OldBasic + "', NewPayscaleID: '" + NewPayscaleID + "', NewBasic: '" + NewBasic + "', IncrPer: '" + IncrPer + "',  IncrOn: '" + IncrOn + "', Decline: '" + Decline + "', Remarks: '" + Remarks + "', SectorID: '" + $('#ddlSector').val() + "'}";
                        $(".loading-overlay").show();
                        $.ajax({
                            type: "POST",
                            url: pageUrl + '/UpdateIncrementAll',
                            data: E,
                            contentType: "application/json; charset=utf-8",
                            success: function (D) {
                                $(".loading-overlay").hide();
                                var t = jQuery.parseJSON(D.d);
                               
                                alert("Increment of all the Employee(s) is/are successfully Updated.");
                                $("#btnAll").hide();
                                //$("#cmdSave").show();
                               
                                $("#cmdSave").prop("disabled", false);
                                return;
                            }
                        });
                    }
                }
            });
           //***********************************************************************
        });
        $("#cmdSave").click(function (e) {
            e.preventDefault();
            var S = $('#ddlSector').val();
            var E = "{SecID: '" + S + "'}";
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: pageUrl + '/ApplyIncrement',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    $(".loading-overlay").hide();
                    var t = jQuery.parseJSON(D.d);
                     var rslt=confirm("Increment of all the Employee(s) is/are successfully Updated.");
                     if (rslt == true || rslt == false)
                     {
                         window.location.href = "Employee_Increment.aspx";
                     }
                }
            });
        });


    });

</script>
       <script>
           $(document).ready(function () {
               /*Code to copy the gridview header with style*/
               var gridHeader = $('#tbl').clone(true);
            /*Code to remove first ror which is header row*/
            $(gridHeader).find("tr:gt(0)").remove();
            $('#tbl tr th').each(function (i) {
                /* Here Set Width of each th from gridview to new table th */
                $("th:nth-child(" + (i + 1) + ")", gridHeader).css('width', ($(this).width()).toString() + "px");
            });
            $("#controlHead").append(gridHeader);
            $('#controlHead').css('position', 'absolute');
            $('#controlHead').css('top', $('#tbl').offset().top);

        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">
<asp:HiddenField ID="hdnPayMonth" runat="server" Value="" ClientIDMode="Static" />
<asp:HiddenField ID="hdnPayBand" runat="server" Value="" ClientIDMode="Static" />
<asp:HiddenField ID="hdnEmployeeID" runat="server" Value="" ClientIDMode="Static" />
<div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Employee  Increment</h2>
                    </section>



                    <div class="loading-overlay">
                        <div class="loadwrapper">
                            <div class="ajax-loader-outer">Loading...</div>
                        </div>
                    </div>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 0px;">
                                    <table align="center" width="100%">
                                    <tr>
                                    <td style="padding:15px;">
                                    <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False"                            
                                DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" ClientIDMode="Static" >
                                <InsertItemTemplate>
                                <table align="center" width="100%">
                                <tr>
                                <td>
                                <asp:Button ID="btnViewIncr" runat="server" Text="View Increment-1" CommandName="View"  
                                        Width="115px" CssClass="Btnclassname" OnClick="btnViewIncr_Click" />
                                </td>
                                <td>
                                <a onclick="javascript:PrintDiv();" style="font-size:12px;color:Black; font-family:Segoe UI;font-weight:bold;" href="javascript:void()"><b>Print</b></a>
                                </td>
                                </tr>
                                    <tr>
                                        <td style="padding:3px;" ><span class="headFont">Employee No&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;" >:</td>
                                        <td style="padding:3px;" align="left">
                                            <asp:TextBox ID="txtEmpNo" autocomplete="off"  ClientIDMode="Static" runat="server" Enabled="false" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                        <td style="padding:3px;" ><span class="headFont">Employee Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;" >:</td>
                                        <td style="padding:3px;" align="left">
                                            <asp:TextBox ID="txtEmpName" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="false" CssClass="textbox"></asp:TextBox>                            
                                        </td>

                                    </tr>
                                    <tr>


                                    </tr>
                                    <tr>
                                        <td style="padding:3px;" ><span class="headFont">PayScale&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;" >:</td>
                                        <td style="padding:3px;" align="left" >
                                            <asp:DropDownList ID="ddlNewPayScale" autocomplete="off" Height="21px" Width="217px" runat="server" 
                                                                                DataValueField="PayScaleID" DataTextField="PayScale" AppendDataBoundItems="true" ClientIDMode="Static">
                                                                                <asp:ListItem Text="(Select Pay Scale)" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>                            
                                        </td>
                                        <td style="padding:3px;"><span class="headFont">Increment Percentage&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;">:</td>
                                       <td style="padding:3px;" align="left" >
                                            <asp:TextBox ID="txtIncrPer" autocomplete="off" ClientIDMode="Static" Width="80px" Enabled="false"  runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:3px;"><span class="headFont">Old Basic&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;">:</td>
                                       <td style="padding:3px;" align="left" >
                                            <asp:TextBox ID="txtCurrBasic" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                      <td style="padding:3px;"><span class="headFont">New Basic&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;">:</td>
                                       <td style="padding:3px;" align="left" >
                                            <asp:TextBox ID="txtNewBasic" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:3px;"><span class="headFont">DNI&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;">:</td>
                                       <td style="padding:3px;" align="left" >
                                            <asp:TextBox ID="txtIncrOn" ClientIDMode="Static"  Width="80px" autocomplete="off"  runat="server" CssClass="dpDate textbox" Text='<%# Bind("DOB", "{0:dd/MM/yyyy}") %>'></asp:TextBox>
                                            <asp:ImageButton ID="imgIncrOn" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                             AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />                           
                                        </td>
                                        <td style="padding:3px;"><span class="headFont">Remarks&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;">:</td>
                                       <td style="padding:3px;" align="left" >
                                            <asp:TextBox ID="txtRemarks" autocomplete="off" ClientIDMode="Static" runat="server"  CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                        <td>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Add"  
                                        Width="60px" CssClass="Btnclassname DefaultButton" OnClientClick='javascript: return beforeSave();' />

                                        <asp:Button ID="btnAll" runat="server" Text="Increment All-2" CommandName="All"  
                                        Width="100px" CssClass="Btnclassname DefaultButton" OnClientClick='javascript: return beforeSave();' />
                                        </td>
                                    </tr>
                                    <tr>
                                     <td style="padding:3px;" ><span class="headFont">Decline&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:3px;" >:</td>
                                        <td style="padding:3px;" align="left" >
                                        <asp:DropDownList ID="ddlDecline"  Height="21px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                 <asp:ListItem Text="(Select Decline)" Value=""></asp:ListItem>
                                                 <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                 <asp:ListItem Text="No" Value="N"></asp:ListItem>                            
                                        </asp:DropDownList>                            
                                        </td>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>
                                <EditItemTemplate>
                                <table align="center" width="100%">
                                <tr><td>
                                <asp:Button ID="btnViewIncr" runat="server" Text="View Increment" CommandName="View"  
                                        Width="105px" CssClass="Btnclassname" OnClick="btnViewIncr_Click" />
                                </td></tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Employee Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtEmpName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Text='<%# Eval("EmpName") %>'></asp:TextBox>                            
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Current Basic&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                       <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtCurrBasic" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Increment PayScale&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtIncrPayScale" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Increment Percentage&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                       <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtIncrPer" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Increment Basic&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtIncrBasic" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Incremented On&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                       <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtIncrOn" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;" ><span class="headFont">Decline&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;" >:</td>
                                        <td style="padding:5px;" align="left" >
                                        <asp:DropDownList ID="ddlDecline"  Height="22px" runat="server" AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                                 <asp:ListItem Text="(Select Decline)" Value=""></asp:ListItem>
                                                 <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                 <asp:ListItem Text="No" Value="N"></asp:ListItem>                            
                                        </asp:DropDownList>                            
                                        </td>
                                        <td style="padding:5px;"><span class="headFont">Remarks&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                        <td style="padding:5px;">:</td>
                                       <td style="padding:5px;" align="left" >
                                            <asp:TextBox ID="txtRemarks" autocomplete="off" ClientIDMode="Static" runat="server"  CssClass="textbox"></asp:TextBox>                            
                                        </td>
                                        <td>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Add"  
                                        Width="80px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();' />
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                                <FooterTemplate>
                                <table align="center"  width="100%">
                                <tr><td colspan="4" ><hr style="border:solid 1px lightblue" /></td></tr>
                                    <table>
                                    <tr>
                                        <td style="padding: 5px;"><span class="headFont">Employee No&nbsp;&nbsp;</span><span class="require">*</span></td>
                                        <td style="padding: 5px;">:</td>
                                        <td style="padding: 5px;" align="left">
                                            <asp:TextBox ID="txtEmpNoforSearch" autocomplete="off" ClientIDMode="Static" Width="70px" runat="server" MaxLength="6" CssClass="textbox"></asp:TextBox>&nbsp;&nbsp;
                                          
                                              <asp:Button ID="btnSearchEmp" runat="server" ClientIDMode="Static" Text="Search" CommandName="Search"  
                                        Width="70px" CssClass="Btnclassname" OnClick="btnSearchEmp_Click" />

                                          <%--  <asp:Button ID="btnReset" runat="server" ClientIDMode="Static" Text="Reset" CommandName="Reset"  
                                        Width="70px" CssClass="Btnclassname" OnClick="btnReset_Click" />--%>
                                        </td>
                                        </tr>
                                    </table>
                                    <tr>
                                        <td style="width:100%;">
                                            <div id="controlHead">
                                            </div>
                                            <div style="height: 200px; overflow: auto;">
                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="None"
                                                    AutoGenerateColumns="false" DataKeyNames="EmployeeID"
                                                    OnRowDataBound="tbl_RowDataBound" OnPageIndexChanging="tbl_PageIndexChanging" ClientIDMode="Static">
                                                    <AlternatingRowStyle BackColor="Honeydew" />
                                                    <HeaderStyle CssClass="GridviewScrollHeader" />
                                                    <RowStyle CssClass="GridviewScrollItem" />
                                                    <PagerStyle CssClass="GridviewScrollPager" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="EmployeeID" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmployeeID" runat="server" Visible="false" Text='<%# Bind("EmployeeID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="EmpNo" HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EmpNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="EmployeeName" HeaderStyle-Width="180px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Designation" HeaderStyle-Width="140px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PayScaleType" HeaderStyle-Width="10px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayScaleType" runat="server" Visible="false" Text='<%# Bind("PayScaleType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField >
                                                        <asp:TemplateField HeaderText="PayScaleID" HeaderStyle-Width="10px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayScaleID" runat="server" Visible="false" Text='<%# Bind("PayScaleID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField >
                                                        <asp:TemplateField HeaderText="PayBand" HeaderStyle-Width="10px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayBand" runat="server" Visible="false" Text='<%# Bind("PayBand") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PayScale" HeaderStyle-Width="200px"  Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayScale" runat="server" Visible="true" Text='<%# Bind("PayScale") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Old Basic (PB + GP)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOldBasic" runat="server" Text='<%# Bind("OldBasic") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Increment Percentage (%)" HeaderStyle-Width="150px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIncrPer" runat="server" Text='<%# Bind("IncrementPercentage") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-Width="50px" HeaderText="Edit">
                                                            <ItemTemplate>
                                                                <asp:CheckBox runat="server" ID="chkSelect" CommandArgument='<%# Eval("EmployeeID") %>'
                                                                     ClientIDMode="static" AutoPostBack="true"   OnCheckedChanged="chkSelect_CheckedChanged"   />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                <tr>
                                        <td style="width:100%;">
                                            <div id="Print" style="visibility:hidden; height:0px; width: 100%">
                                                <asp:GridView ID="tbltoPrint"  runat="server" Width="100%" align="center" GridLines="Both"
                                                    AutoGenerateColumns="false" DataKeyNames="EmployeeID" CssClass="Grid" PagerStyle-CssClass="pgr"
                                                    AlternatingRowStyle-CssClass="alt" PageSize="10" 
                                                     ClientIDMode="Static">
                                                    <AlternatingRowStyle BackColor="Honeydew" />
                                                   
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="EmployeeID" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmployeeID" runat="server" Visible="false" Text='<%# Bind("EmployeeID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="EmpNo" HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EmpNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="EmployeeName" HeaderStyle-Width="180px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Designation" HeaderStyle-Width="140px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PayScaleType" HeaderStyle-Width="10px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayScaleType" runat="server" Visible="false" Text='<%# Bind("PayScaleType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField >
                                                        <asp:TemplateField HeaderText="PayScaleID" HeaderStyle-Width="10px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayScaleID" runat="server" Visible="false" Text='<%# Bind("PayScaleID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField >
                                                        <asp:TemplateField HeaderText="PayBand" HeaderStyle-Width="10px" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayBand" runat="server" Visible="false" Text='<%# Bind("PayBand") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PayScale" HeaderStyle-Width="200px"  Visible="true">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayScale" runat="server" Visible="true" Text='<%# Bind("PayScale") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Old Basic (PB + GP)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOldBasic" runat="server" Text='<%# Bind("OldBasic") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Increment Percentage (%)" HeaderStyle-Width="150px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIncrPer" runat="server" Text='<%# Bind("IncrementPercentage") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table align="center"  width="100%">
                                <tr>
                                    <td style="padding:3px;" ><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>
                                    <div style="float:left;margin-left:200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Apply Increment-3" CommandName="Add"  
                                        Width="115px" CssClass="Btnclassname DefaultButton" OnClientClick='javascript: return beforeSave();' 
                                         /> 
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"  OnClick="cmdCancel_Click" 
                                        Width="80px" CssClass="Btnclassname"  OnClientClick='javascript: return unvalidate();'/>
                                    </div>
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                                </asp:FormView>
                                    </td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
</div> 
</asp:Content>

