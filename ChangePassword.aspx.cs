﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;


public partial class ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_INT_USER_ID] == null)
            {
                Response.Redirect("Default.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void dv_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != DetailsViewMode.Insert && e.NewMode != DetailsViewMode.Edit)
            {

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void Submit_Click(object sender, System.EventArgs e)
    {
        try
        {
            //DataTable dt = DBHandler.GetResult("Get_LoaneeCode", Session[SiteConstants.SSN_INT_WEBSITE_USER_ID].ToString());
            //string LCode = dt.Rows[0][0].ToString();
            //TextBox txtold = (TextBox)dv.FindControl("txtold");
            TextBox txtnew = (TextBox)dv.FindControl("txtnew");
            TextBox txtconfirm = (TextBox)dv.FindControl("txtconfirm");
            //DataTable dt = DBHandler.GetResult("Get_PasswordByCode", Session[SiteConstants.SSN_INT_USER_ID].ToString());
            //string pass = dt.Rows[0][0].ToString();
            //pass = pass.Replace(" ",string.Empty);
            if (txtnew.Text == txtconfirm.Text)
            {
                DBHandler.Execute("Update_PassByCode", txtnew.Text, Session[SiteConstants.SSN_INT_USER_ID].ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Password Updated Successfully.')</script>");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Enter  Password Correctly.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void btnreset_Click(object sender, System.EventArgs e)
    {
        try
        {
            TextBox txtnew = (TextBox)dv.FindControl("txtnew");
            TextBox txtconfirm = (TextBox)dv.FindControl("txtconfirm");
            txtnew.Text = "";
            txtconfirm.Text = "";
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] CheckUserPassword(string prefix)
    {
        List<string> CodewithName = new List<string>();
        //customers.Add(string.Format("{0}-{1}", sdr["ContactName"], sdr["CustomerId"]));
        try
        {
            DataTable dtCodeName = DBHandler.GetResult("Get_UserByID", prefix, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (dtCodeName.Rows.Count > 0)
            {
                foreach (DataRow row in dtCodeName.Rows)
                {
                    CodewithName.Add(string.Format("{0}", row["Password"]));
                    //CodewithName.Add(string.Format("{0}|{1}", row["Password"], row["UserID"]));
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return CodewithName.ToArray();
    }

   // [WebMethod(EnableSession = true)]
   // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public static string CheckUserPassword(string oldpwd)
    //{
    //    string JSONVal = ""; string result = string.Empty;
    //    try
    //    {
    //        DataTable dt = DBHandler.GetResult("Get_UserByID", HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
    //        if (dt.Rows.Count > 0)
    //        { 
    //        string password = dt.Rows[0]["Password"].ToString();
    //        if (oldpwd == password)
    //        {
    //            result = "Right";
    //            JSONVal = result.ToJSON();
    //        }
    //        else
    //        {
    //            result = "Wrong";
    //            JSONVal = result.ToJSON();
    //        }

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //    return JSONVal;
    //}
}