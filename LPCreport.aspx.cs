﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;


public partial class LPCreport : System.Web.UI.Page
{
    static string StrFormula="";
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                //GetSecByCent(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]));
                //DBHandler.Execute("Get_SalPayMonthHist", HttpContext.Current.Session[SiteConstants.SSN_SECTORID], HttpContext.Current.Session[SiteConstants.SSN_SALFIN]);
                //populateCenter();
                //int CenterId =Convert.ToInt32(ddlCenter.SelectedValue);

                //populateEmployee(CenterId);
            }
        }
        try
        {
           

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string AjaxGetGridCtrl(string prefix, string ParameterTypes)
    {
        DataSet ds = DBHandler.GetResults("Get_TransferOrderByEmpNo", prefix, ParameterTypes);
        DataTable dt = ds.Tables[0];

        List<TranferOrder> listTranferOrder = new List<TranferOrder>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TranferOrder objst = new TranferOrder();

                objst.SerialNo = Convert.ToInt32(dt.Rows[i]["SerialNo"]);
                objst.TransferOrderNo = Convert.ToString(dt.Rows[i]["TransferOrderNo"]);

                listTranferOrder.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listTranferOrder);

    }
    public class TranferOrder
    {
        public int SerialNo { get; set; }
        public string TransferOrderNo { get; set; }
    }



    protected void cmdReset_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    
   
    public class SectbyCent
    {
        public int CenterID { get; set; }
        public string CenterName { get; set; }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(string StrEmpNo,string TransOrder, string FormName, string ReportName, string ReportType)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        String Msg = "";
        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable DTCheckReportVal1 = DBHandler.GetResult("Load_DAT_EmpLPC", StrEmpNo, UserID);

        DataTable DTCheckReportVal2 = DBHandler.GetResult("Load_EmpLPC", StrEmpNo, UserID);

        StrFormula = "{RPT_MST_EmployeeLPC.EmpNo}='" + StrEmpNo + "' and {RPT_MST_EmployeeLPC.TransferOrderNo}='" + TransOrder + "'";
        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

            JavaScriptSerializer jscript = new JavaScriptSerializer();
            return jscript.Serialize(Msg);
        }


   

   
    }


    

    





