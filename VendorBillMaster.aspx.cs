﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;

public partial class VendorBillMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PopulateGrid();
        
    }

    protected DataTable drpload(string str)
    {
        try
        {
            //if (str == "Vendor")
            //{
            //    DataTable dt = DBHandler.GetResult("Load_Vendor");
            //    return dt;
            //}
            if (str == "Bank")
            {
                DataTable dt = DBHandler.GetResult("Load_Bank");
                return dt;
            }
            else
            {
                return null;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GET_AutoComplete_VendorName(string VenName)
    {
        List<string> Detail = new List<string>();
        DataTable dtGetData = DBHandler.GetResult("AutoComplete_VendorName", VenName);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
           // Detail.Add(dtRow["InvestmentNumber"].ToString() + "|" + dtRow["InvestmentID"].ToString());
            Detail.Add(dtRow["VendorName"].ToString() + "|" + dtRow["VendorID"].ToString());
        }
        return Detail.ToArray();
    }


    [WebMethod]
    public static string BindBranch(int BankID)
    {
        DataSet ds = DBHandler.GetResults("Get_BranchbyBankID", BankID);
        DataTable dt = ds.Tables[0];

        List<BranchbyBank> listbranch = new List<BranchbyBank>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                BranchbyBank objst = new BranchbyBank();

                objst.BranchID = Convert.ToInt32(dt.Rows[i]["BranchID"]);
                objst.Branch = Convert.ToString(dt.Rows[i]["Branch"]);

                listbranch.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listbranch);

    }
    public class BranchbyBank
    {
        public int BranchID { get; set; }
        public string Branch { get; set; }
    }

    [WebMethod]
    public static string GetIFSCandMICR(int BranchID)
    {
        DataSet ds = DBHandler.GetResults("Get_IFSCandMICR", BranchID);
        DataTable dt = ds.Tables[0];

        List<IfscMicr> listIfscMicr = new List<IfscMicr>();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IfscMicr objst = new IfscMicr();

                objst.IFSC = Convert.ToString(dt.Rows[i]["IFSC"]);
                objst.MICR = Convert.ToString(dt.Rows[i]["MICR"]);

                listIfscMicr.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listIfscMicr);

    }
    public class IfscMicr
    {
        public string IFSC { get; set; }
        public string MICR { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DuplicatePannumber_Checkings(string panno)
    {
        string htmlContent = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Get_VendorALLBYCompanyPan", panno);
            if (dt.Rows.Count > 0)
            {
                htmlContent = "Available".ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;
    }
    

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string FindLocationNumber(string LocationNo, string SecID)
    {
        string htmlContent = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Find_LocationAvailability", SecID, LocationNo);
            if (dt.Rows.Count > 0)
            {
                string Value = dt.Rows[0]["Value"].ToString();
                if (Value != "NotAvailable")
                {
                    string LocationName = Value;
                    htmlContent = LocationName.ToJSON();
                }
                else
                {
                    htmlContent = "NotAvailable".ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_BillMaster(string LocationCode, string BillNo, string BillDate, string VendorID, string VendorBillNo, string VendorBillDate, string sectorid)
    {
        string htmlContent = "";

        try
        {
            DataTable dt = DBHandler.GetResult("INSERT_VendorBillMast", HttpContext.Current.Session[SiteConstants.SSN_SALFIN], sectorid,
            LocationCode,
                        VendorID, BillNo, BillDate,
                        VendorBillNo, VendorBillDate, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);


            if (dt.Rows.Count > 0)
            {
                string Value = dt.Rows[0]["Value"].ToString();
                if (Value != "0")
                {
                    htmlContent = "Saved".ToJSON();
                }
                else
                {
                    htmlContent = "NotSaved".ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent; 
    
    }




    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_VendorMaster(string VendorName, string Address, string CompanyName, string PanNo, string Email, string Phone, string BranchDrp, string BankCode, string ActiveFlag)
    {
        string htmlContent = "";
        
       
        
        try
        {
            DataTable dt = DBHandler.GetResult("INSERT_VendorMast", VendorName, Address,
                            CompanyName, PanNo, Email.Equals("") ? DBNull.Value : (object)Email,
                            Phone.Equals("") ? DBNull.Value : (object)Phone,
            Convert.ToInt32(BranchDrp), Convert.ToInt32(ActiveFlag), BankCode, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);

          
            if (dt.Rows.Count > 0)
            {
                htmlContent = dt.Rows[0]["Value"].ToString();
                //if (Value == "A")
                //{
                //    htmlContent = "A".ToJSON();
                //}
                //else if (Value == "S")
                //{
                //    htmlContent = "S".ToJSON();
                //}

                //else if (Value == "D")
                //{
                //    htmlContent = "Duplicate PanCard Number.Data Not Saved".ToJSON();
                //}
                //else
                //{
                //    htmlContent = "Unknown Error".ToJSON();
                //}
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        
        return htmlContent.ToJSON(); 
       
       
    }




    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Update_BillMaster(string  VendorBillID, string LocationCode, string BillNo, string BillDate, string VendorID, string VendorBillNo, string VendorBillDate, string sectorid)
    {
        string htmlContent = "";

        try
        {
            DataTable dt = DBHandler.GetResult("Update_VendorBIllMast", VendorBillID, 
            LocationCode,
                        VendorID, BillNo, BillDate,
                        VendorBillNo, VendorBillDate, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);


            if (dt.Rows.Count > 0)
            {
                string Value = dt.Rows[0]["Value"].ToString();
                if (Value != "0")
                {
                    htmlContent = "Updated".ToJSON();
                }
                else
                {
                    htmlContent = "Not Updated".ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;

    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Get_PanandCompanyName(string VendorID)
    {
        string htmlContent = "";

        try
        {
            DataTable dt = DBHandler.GetResult("Get_VendorID", VendorID);
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow r in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, r[col]);
                    }
                    rows.Add(row);
                }

                htmlContent = rows.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string BindVendorBillMaster()
    {
        string htmlContent = "";

        try
        {
            DataTable dt = DBHandler.GetResult("Get_Grid_VendorBillMAst");
          //  if (dt.Rows.Count > 0)
          //  {
          //      List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
          //      Dictionary<string, object> row;

          //      foreach (DataRow r in dt.Rows)
          //      {
          //          row = new Dictionary<string, object>();
          //          foreach (DataColumn col in dt.Columns)
          //          {
          //              row.Add(col.ColumnName, r[col]);
          //          }
          //          rows.Add(row);
          //      }

          //      htmlContent = rows.ToJSON();
          //  }
            
          }
        
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        
        return htmlContent;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Update_VendorDetail(string VendorBillID)
    {
        string htmlContent = "";

        try
        {
            DataTable dt = DBHandler.GetResult("Update_VendorBillMasterId", VendorBillID);
              if (dt.Rows.Count > 0)
              {
                  List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                  Dictionary<string, object> row;

                  foreach (DataRow r in dt.Rows)
                  {
                      row = new Dictionary<string, object>();
                      foreach (DataColumn col in dt.Columns)
                      {
                          row.Add(col.ColumnName, r[col]);
                      }
                      rows.Add(row);
                  }

                  htmlContent = rows.ToJSON();
              }

        }

        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return htmlContent;

    }



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Delete_VendorDetail(string VendorBillID)
    {
        string jsonVal = "";
        try
        {
            DataTable dtPayDetails = DBHandler.GetResult("Delete_VendorBillMastByID", VendorBillID);
           
            //if (dtPayDetails.Rows.Count > 0)
            //{
            //    string result = dtPayDetails.Rows[0]["Result"].ToString();
            //    if (result == "Deleted")
            //        jsonVal = result.ToJSON();
            //    else
            //        jsonVal = result.ToJSON();
           // }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return jsonVal;
    }

    protected void PopulateGrid()
    {
        DataTable dt = DBHandler.GetResult("Get_Grid_VendorBillMAst");
        if (dt.Rows.Count > 0)
        {
            tbl.DataSource = dt;
            tbl.DataBind();
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
            //    dv.ChangeMode(FormViewMode.Edit);
            //    hdnBankID.Value = e.CommandArgument.ToString();
            //    UpdateDeleteRecord(1, e.CommandArgument.ToString());

            //    Button cmdSave = (Button)dv.FindControl("cmdSave");
            //    cmdSave.CommandName = "Edit";
            //    cmdSave.ToolTip = "Update";
            //    cmdSave.Text = "Update";
            //}
            //else if (e.CommandName == "Del")
            //{
            //    UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    


    public static string FindLocationNumberByGrid(string LocationNo)
    {
        string htmlContent = "";

        try
        {
            DataTable dt = DBHandler.GetResult("LocationCode_textBOx_autoComplete", LocationNo);
            if (dt.Rows.Count > 0)
            {
                string Value = dt.Rows[0]["Value"].ToString();
                if (Value != "NotAvailable")
                {
                    string LocationName = Value;
                    htmlContent = LocationName.ToJSON();
                }
                else
                {
                    htmlContent = "NotAvailable".ToJSON();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return htmlContent;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ArrayList populatePS()
    {
        ArrayList psJSON = new ArrayList();
        try
        {
            DataTable dtProperty = DBHandler.GetResult("Load_Vendor");
            foreach (DataRow dr in dtProperty.Rows)
            {
                psJSON.Add(new ListItem(dr["VendorName"].ToString(), dr["VendorID"].ToString()));
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return psJSON;
    }
    

}
 


