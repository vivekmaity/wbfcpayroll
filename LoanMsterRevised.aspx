﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LoanMsterRevised.aspx.cs" Inherits="LoanMsterRevised" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="js/LoanMasterRevised.js?v2"></script>
    <script src="jquery.ui.position.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" runat="Server">
    <style type="text/css">
        #AutoComplete-SanctionNo, #Autocomplete-SanctionNoExisting {
            display: block;
            position: relative;
        }
        .buttonbig:hover {
                 background-color: #808080;
                 color:black;
                }
         .buttonbig1:hover {
                 background-color: #808080;
                 color:black;
                }
          .buttonbig2:hover {
                 background-color: #808080;
                 color:black;
                }
        .ui-autocomplete {
            height: 200px;
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #cbc7c7;
            font-size: 13px;
            font-weight: normal;
            color: #242424;
            background: #fff;
            -moz-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            -webkit-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            box-shadow: inset 0.9px 1px 3px #e4e4e4;
            width: 350px;
            /*height:30px;*/
            padding: 5px;
            font-family: Segoe UI, Lucida Grande, Arial, Helvetica, sans-serif;
            margin: 3px 0;
        }

        h2 {
            background-color: #384e72;
            font-size: 17px;
            color: white;
            padding: 2px;
            font-family: Vrinda;
        }

        .SpanClass {
            background-color: #384e72;
            font-size: 17px;
            color: white;
            padding: 2px;
            font-family: Vrinda;
        }

        .button {
            display: inline-block;
            border-radius: 4px;
            background-color: #f4511e;
            border: none;
            color: #FFFFFF;
            text-align: center;
            font-size: 15px;
            padding: 10px;
            width: 142px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

        .buttonbig {
            display: inline-block;
            border-radius: 4px;
            background-color: #0094ff;
            border: none;
            color: #f2f2ef;
            text-align: center;
            font-size: 15px;
            padding: 10px;
            width: 390px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

        .buttonbig1 {
            display: inline-block;
            border-radius: 4px;
            background-color: #0094ff;
            border: none;
            color: #f2f2ef;
            text-align: center;
            font-size: 15px;
            padding: 10px;
            width: 390px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

        .buttonbig2 {
            display: inline-block;
            border-radius: 4px;
            background-color: #0094ff;
            border: none;
            color: #f2f2ef;
            text-align: center;
            font-size: 15px;
            padding: 10px;
            width: 390px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

        u {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" runat="Server">
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">
                    <section>
                        <h2 style="font: 400px">Loan Master</h2>
                    </section>
                    <div>
                        <table align="center" style="border: solid 2px lightblue; width: 100%;">
                            <thead style="border: solid 2px lightblue">
                                <tr>
                                    <th colspan="9" style="font-family: 'Bell MT'; font-size: x-large">Employee Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: large">Employee No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left" width="150px">
                                        <asp:TextBox ID="txtEmpNoSearch" autocomplete="off" ClientIDMode="Static" Width="200px" runat="server" CssClass="textbox"></asp:TextBox>
                                        <%--<asp:HiddenField ID="hdnEmployeeId" ClientIDMode="Static" runat="server" />--%>
                                    </td>
                                    <%--Width="100px" Height="20PX"--%>
                                    <%--<td style="text-align: left" colspan="6">
                                        <asp:Button ID="btnSearchEployee" runat="server" Text="Search"
                                            Class="button" />
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: large">Employee Name&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left" colspan="7">
                                        <asp:TextBox ID="txtEmpName" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="false" Width="400px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: large">Location&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="txtLocation" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="false" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right; width: 220px"><span class="headFont" style="font-family: 'Bell MT'; font-size: large">Date Of Retirement(DOR)&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="txtDateofRetirement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="false" Width="200px" CssClass="textbox" placeholder="DD/MM/YYYY"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: large">No of months left From DOR&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <span id="NoOfMonths" runat="server" style="color: red; font-family: Verdana; font-size: x-large"></span>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <td style="text-align: left"><%--Existing Loan Details--%>
                                    <asp:Button ID="btnExistingLoanDetails" runat="server" Text="Existing Loan Details"
                                        Class="buttonbig" />
                                </td>
                                <td style="text-align: left"><%--Add New Loan--%>
                                    <asp:Button ID="btnAddNewLoan" runat="server" Text="Add New Loan"
                                        Class="buttonbig1" />
                                </td>
                                <td style="text-align: left"><%--Disbursement Against Existing Loan--%>
                                    <asp:Button ID="btnDisAdinExtLoan" runat="server" Text="Disbursement Against Existing Loan"
                                        Class="buttonbig2" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="DescriptionBodyAddNewLoan" style="display: none">
                        <table align="center" style="border: solid 2px #0a2754; width: 100%;">
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Description&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <%-- DataValueField="LoanTypeID" DataTextField="LoanDesc"--%>
                                <td style="padding: 3px;" align="left" colspan="7">
                                    <asp:DropDownList ID="ddlLoanDesc" Width="300px" Height="22px" runat="server"
                                        AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                        <asp:ListItem Text="(Select Loan Description)" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                            </tr>
                            <%--<tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Sanction Reference No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtSanctionReferenceNo" ClientIDMode="Static" runat="server" Enabled="true" Width="300px" CssClass="textbox"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Sanction Reference Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <nobr>
                                                <asp:TextBox ID="txtSanctionReferenceDate" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                                <asp:ImageButton ID="btnLDSDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                                    AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                    </nobr>
                                </td>
                            </tr>--%>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Sanction No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtNewSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Sanction Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <nobr>
                                        <asp:TextBox ID="txtLoanSanctionDate" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox" placeholder="DD/MM/YYYY"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" CausesValidation="false" ImageUrl="~/images/date.png" 
                                          AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                    </nobr>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Sanction Amount:&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtLoanSanctionAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Order No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtDisbursementOrderNo" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Order Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <nobr>
                                        <asp:TextBox ID="txtDisbursementOrderDate" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox" placeholder="DD/MM/YYYY"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton3" CausesValidation="false" ImageUrl="~/images/date.png" 
                                          AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                    </nobr>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Disbursement Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtLoanDisbursementAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Deduction Start Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <nobr>
                                        <asp:TextBox ID="txtDeductionStartDate" autocomplete="off" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox" placeholder="DD/MM/YYYY"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" CausesValidation="false" ImageUrl="~/images/date.png" 
                                          AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                    </nobr>
                                </td>
                                <td style="padding: 3px; text-align: right" colspan="4"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Interest on Disbursement Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtInterestOnDisbursementAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <%--<tr>
                                <td style="padding: 3px; text-align: right" ><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Reference No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtDisbursementReferenceNo" ClientIDMode="Static" runat="server" Enabled="true" Width="300px" CssClass="textbox"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Reference Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <nobr>
                                        <asp:TextBox ID="txtDisbursementReferenceDate" ClientIDMode="Static" Width="190px" runat="server" CssClass="dpDate textbox"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton4" CausesValidation="false" ImageUrl="~/images/date.png" 
                                          AlternateText="Click to show calendar" CssClass="dpDate calendar" runat="server" />
                                    </nobr>
                                </td>
                            </tr>--%>
                        </table>
                        <table align="center" style="border: solid 2px #0a2754; width: 100%;">
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Total Loan Installment No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtTotalLoanInstall" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="120px" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Current Loan Install&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtCurrentLoanInstall" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="120px" CssClass="textbox"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Principal Amount Paid&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtLoanAmountPaid" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="120px" CssClass="textbox"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Interest Amount Paid&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtLoanIntAmountPaid" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="120px" CssClass="textbox"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table align="center" style="border: solid 2px #0a2754; width: 100%;">
                            <tr>
                                <td colspan="9"></td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    <h2 style="font: 400px">First Installment Slab</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FrstInstallmentNo" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FrstInstallmentPrincipleAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FrstInstallmentInterestAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    <h2 style="font: 400px">Second Installment Slab</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="secndInstallmentNo" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="secndInstallmentPrincipleAmount" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="secndInstallmentInterestAmount" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    <h2 style="font: 400px">Third Installment Slab</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="thirdInstallmentNo" ClientIDMode="Static" runat="server" autocomplete="off" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="thirdInstallmentPrincipleAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="thirdInstallmentInterestAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    <h2 style="font: 400px">Fourth Installment Slab</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FourthInstallmentNo" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FourthInstallmentPrincipleAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FourthInstallmentInterestAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                           
                            <tr>
                                <td colspan="9">
                                    <h2 style="font: 400px">Fifth Installment Slab</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FifthInstallmentNo" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FifthInstallmentPrincipleAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="FifthInstallmentInterestAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="9">
                                    <h2 style="font:400px"> Retirement Adjustment</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Principle Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtRetPrincipleAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Interest Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtRetirementAdjustmentAmount" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Adjustment Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtRetirementAdjustmentDate" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="DD/MM/YYYY" CssClass="textbox" ></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table align="center" style="border: solid 2px #0a2754; width: 100%;">
                            <tr>
                                <td colspan="3">
                                    <hr style="border: solid 1px lightblue" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;"><span class="require">*</span> indicates Mandatory Field</td>
                                <td style="padding: 5px;">&nbsp;</td>
                                <td style="padding: 5px;" align="left">
                                    <div style="float: left; margin-left: 200px;">
                                        <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add"
                                            Width="150px" Height="30px" CssClass="Btnclassname" />
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                            Width="150px" Height="30px" CssClass="Btnclassname" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="DescriptionBodyDisbursementAgainstExistingLoan" style="display: none">
                        <table align="center" style="border: solid 2px  #0a2754; width: 100%;">
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Description&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <%-- DataValueField="LoanTypeID" DataTextField="LoanDesc"--%>
                                <td style="padding: 3px;" align="left" colspan="4">
                                    <asp:DropDownList ID="ddlLoanDescription" Width="400px" Height="22px" runat="server"
                                        AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                        <asp:ListItem Text="(Select Loan Description)" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Sanction No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left" colspan="4">
                                    <asp:TextBox ID="txtSanctionNoAutoComplete" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="300px" Enabled="true"></asp:TextBox>
                                    <asp:HiddenField ID="hdnLoanId" runat="server" Value="" ClientIDMode="Static" />
                                    <div id="AutoComplete-SanctionNo"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Sanction Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtLoanSanctionAmountDisExistingLoan" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="300px" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; width: 200px"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Total Disbursed Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <span id="totalDisbursementAmount" runat="server" clientidmode="Static" style="font-family: Verdana; color: green; font-size: larger">XXXXXXXX</span>
                                </td>
                            </tr>
                            <tr>

                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Sanction Reference Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <asp:TextBox ID="txtAlreadySanctionReferenceDate" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="300px" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; width: 200px"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Left Disbursement Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 5px;" align="left">
                                    <span id="LeftDisbursementAmount" runat="server" autocomplete="off" clientidmode="Static" style="font-family: Verdana; color: green; font-size: larger">XXXXXXXX</span>
                                </td>
                            </tr>

                        </table>
                        <section>
                            <h2 style="font: 400px">
                                <img src="/estb.wbfcerp.co.in/images/triangle.png" /><u id="ListofAlreadyDisbursementLoans">List of Already Disbursement Loans</u></h2>
                        </section>
                        <div id="DisbursementLoanEdit" style="display: none">
                            <table align="center" style="border: solid 2px  #0a2754; width: 100%;">
                                <%-- <tr>
                                    <td colspan="2" class="SpanClass"><span style="font: 400px">
                                        <img src="/estb.wbfcerp.co.in/images/triangle.png" /><u id="ListofAlreadyDisbursementLoans">List of Already Disbursement Loans</u></span></td>
                                    <td colspan="2" class="SpanClass" style="text-align: right"><span style="font: 400px">Total Amount</span></td>
                                    <td colspan="2" class="SpanClass"><span></span></td>
                                </tr>--%>
                                <tr>
                                    <td style="padding: 3px; text-align: right; width: 150px"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Search By Order No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px; width: 50px">:</td>
                                    <td style="padding: 5px;" align="left" width="200px">
                                        <asp:TextBox ID="TextBox33" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true"></asp:TextBox>
                                    </td>
                                    <td style="padding: 5px;" align="left" colspan="">
                                        <div style="float: left; margin-left: 50px;">
                                            <asp:Button ID="Button3" runat="server" Text="Search"
                                                Width="100px" Height="20px" CssClass="Btnclassname" />
                                            <asp:Button ID="Button4" runat="server" Text="Refresh"
                                                Width="100px" Height="20px" CssClass="Btnclassname" />
                                        </div>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="4">
                                        <div style='overflow-y: scroll; width: 100%; height: 200px;'>
                                            <table id="tblDetails" border="1px" class="Grid" style="width: 100%; border-collapse: collapse;" align="center">
                                                <thead>
                                                    <tr>
                                                        <th>Sl No</th>
                                                        <th>Order No</th>
                                                        <th>Order Date</th>
                                                        <th>Disbursement Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot></tfoot>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <section>
                            <h2 style="font: 400px">
                                <img src="/estb.wbfcerp.co.in/images/triangle.png" /><u id="AddNewLoan">Add New Loan</u></h2>
                        </section>
                        <div id="DisbursementAddNewLoan" style="display: none">
                            <table align="center" style="border: solid 2px  #0a2754; width: 100%;">

                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Order No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="DisbursementNoAgainstAlreadtSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="150px" Enabled="true"></asp:TextBox>
                                    </td>
                                   <%--<td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Order Date&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="SanctionDateAgainstAlreadtSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="150px" Enabled="true"></asp:TextBox>
                                    </td>--%>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Order Date&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="DisbursementDateAgainstAlreadtSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="150px" Enabled="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="DisbursementAmountAgainstAlreadtSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" onkeypress="return isNumber(event)" Width="300px" Enabled="true"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Interest Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="DisbursementIneterestAgainstAlreadtSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" onkeypress="return isNumber(event)" CssClass="textbox UpperCase" Width="300px" Enabled="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Deduction Start Date&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 5px;" align="left" colspan="4">
                                        <asp:TextBox ID="DisbursementDeductionStartDtAgainstAlreadtSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="300px" Enabled="true"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table align="center" style="border: solid 2px #0a2754; width: 100%;">
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Total Loan Installment No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="DisbursementTotalLoanInstallAgainstAlreadtSanctionNo" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="120px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Current Loan Install&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="TextBox7" ClientIDMode="Static" runat="server" autocomplete="off" Enabled="true" Width="120px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <%--<td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Amount Paid&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="TextBox8" ClientIDMode="Static" runat="server" autocomplete="off" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>--%>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Principal Amount Paid&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="DisbursementPrincipalAmountPaidAgainstAlreadtSanctionNo" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="120px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Interest Amount Paid&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="DisbursementInterestAmountPaidAgainstAlreadtSanctionNo" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="true" Width="120px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table align="center" style="border: solid 2px #0a2754; width: 100%;">
                                <tr>
                                    <td colspan="9"></td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <h2 style="font: 400px">First Installment Slab</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FirstInstallmentNoAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" onkeypress="return isNumber(event)" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FirstPrincipleAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" onkeypress="return isNumber(event)" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FirstInterestAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" onkeypress="return isNumber(event)" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <h2 style="font: 400px">Second Installment Slab</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="SecondInstallmentNoAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" placeholder="0" onkeypress="return isNumber(event)" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="SecondPrincipleAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" placeholder="0" onkeypress="return isNumber(event)" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="SecondInterestAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" placeholder="0" onkeypress="return isNumber(event)" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <h2 style="font: 400px">Third Installment Slab</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="ThirdInstallmentNoAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" placeholder="0" onkeypress="return isNumber(event)" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="ThirdPrincipleAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="ThirdInterestAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <h2 style="font: 400px">Fourth Installment Slab</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FourthInstallmentNoAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FourthPrincipleAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FourthInterestAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <h2 style="font: 400px">Fifth Installment Slab</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment No&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FifthInstallmentNoAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Principle Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FifthPrincipleAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Installment Interest Amount&nbsp;&nbsp;</span></td>
                                    <td style="padding: 3px;">:</td>
                                    <td style="padding: 3px;" align="left">
                                        <asp:TextBox ID="FifthInterestAmountAgainstDisbursement" autocomplete="off" ClientIDMode="Static" runat="server" placeholder="0" onkeypress="return isNumber(event)" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                <td colspan="9">
                                    <h2 style="font:400px"> Retirement Adjustment</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Principle Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtRetPrincipleAgainstAlreadyDisburse" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger"> Interest Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtRetAdjIntrstAmountAgainstAlreadyDisburse" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="0" CssClass="textbox" onkeypress="return isNumber(event)"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Adjustment Date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left">
                                    <asp:TextBox ID="txtAsdujtmentDateAgainstAlreadyDisburse"  autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" placeholder="DD/MM/YYYY" CssClass="textbox" ></asp:TextBox>
                                </td>
                            </tr>
                            </table>

                            <table align="center" style="border: solid 2px #0a2754; width: 100%;">
                                <tr>
                                    <td colspan="3">
                                        <hr style="border: solid 1px lightblue" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px;"><span class="require">*</span> Indicates Mandatory Field</td>
                                    <td style="padding: 5px;">&nbsp;</td>
                                    <td style="padding: 5px;" align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="btnSaveAgainstAlreadySanctionNo" runat="server" Text="Save" CommandName="Add"
                                                Width="150px" Height="30px" CssClass="Btnclassname" />
                                            <asp:Button ID="btnCancelAgainstAlreadySanctionNo" runat="server" Text="Cancel"
                                                Width="150px" Height="30px" CssClass="Btnclassname" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="DescriptionBodyExistingLoanDetails" style="display: none">
                        <table align="center" style="border: solid 2px  #0a2754; width: 100%;">
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Description&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <%-- DataValueField="LoanTypeID" DataTextField="LoanDesc"--%>
                                <td style="padding: 3px;" align="left" colspan="4">
                                    <asp:DropDownList ID="ddlLoanDescriptionExisting" Width="400px" Height="22px" runat="server"
                                        AppendDataBoundItems="true" ClientIDMode="Static" autocomplete="off">
                                        <asp:ListItem Text="(Select Loan Description)" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Sanction No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px;">:</td>
                                <td style="padding: 3px;" align="left" colspan="1" width="200px">
                                    <asp:TextBox ID="txtSanctionNoAutoCompleteExisting" autocomplete="off" ClientIDMode="Static" runat="server" Enabled="true" Width="200px" CssClass="textbox"></asp:TextBox>
                                    <asp:HiddenField ID="hdnAutocompleteExistingLoanId" runat="server" Value="" ClientIDMode="Static" />
                                    <div id="Autocomplete-SanctionNoExisting"></div>
                                </td>
                              
                                <td style="padding: 5px;" align="left" colspan="3">
                                    <div style="float: left; margin-left: 50px;">
                                        <asp:Button ID="btnSearchByExistingSanctionNo" runat="server" Text="Search"
                                            Width="150px" Height="25px" CssClass="Btnclassname" />
                                        <asp:Button ID="btnCancelExistngSanctionNo" runat="server" Text="Reset"
                                            Width="150px" Height="25px" CssClass="Btnclassname" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                               <td style="padding: 3px; text-align: right" colspan=""><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Loan Sanction Amount&nbsp;&nbsp;</span></td>
                               <td style="padding: 3px;">:</td>
                               <td style="padding: 3px;" align="left" colspan="4">
                                     <span id="SpanLoanSanctionAmount" runat="server" style="color: blue; font-family: Verdana; font-size: larger"></span>
                               </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <hr style="border: solid 1px lightblue" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td style="padding: 3px; text-align: right; width: 150px"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Amount&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px; width: 50px">:</td>
                                <td style="padding: 5px;" align="left" width="200px" colspan="4">
                                    <asp:TextBox ID="txtDisbursementAmountExisting" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px; text-align: right; width: 150px"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Order No&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px; width: 50px">:</td>
                                <td style="padding: 5px;" align="left" width="200px">
                                    <asp:TextBox ID="txtDisburseOrderNoExisting" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true"></asp:TextBox>
                                </td>
                                <td style="padding: 3px; text-align: right; width: 150px"><span class="headFont" style="font-family: 'Bell MT'; font-size: larger">Disbursement Order date&nbsp;&nbsp;</span></td>
                                <td style="padding: 3px; width: 50px">:</td>
                                <td style="padding: 5px;" align="left" width="200px">
                                    <asp:TextBox ID="txtDisbursementOrderDateExisting" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; text-align: center" colspan="6">
                                    <div style="text-align: center">
                                        <asp:Button ID="Button5" runat="server" Text="Update" CommandName="Add"
                                            Width="100px" Height="25px" CssClass="Btnclassname" />
                                        <asp:Button ID="Button6" runat="server" Text="Cancel"
                                            Width="100px" Height="25px" CssClass="Btnclassname" />
                                    </div>
                                </td>
                            </tr>--%>
                            <tr>
                                <td colspan="6">
                                    <div style='overflow-y: scroll; width: 100%; height: 200px;'>
                                        <table id="tblExistingLoanDetailsbySanctionNo" border="1px" class="Grid" style="width: 100%; border-collapse: collapse;" align="center">
                                            <thead>
                                                <tr>
                                                    <th>Sl No.</th>
                                                    <th>Disbursement Order No</th>
                                                    <th>Disbursement Order Date</th>
                                                    <th>Disbursement Amount</th>
                                                    <th>Pre-Payment</th>
                                                    <th>Retirement Adjustment</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <%-- /****************************************************************Wrap PpoUp Re-Payment***************************************************************/--%>
                        <div class="content-overlay" id='overlay'>
                            <div class="wrapper-outer">
                                <div class="wrapper-inner">
                                    <%--style="height:400px"--%>
                                    <div class="loadContent" >
                                        <div id="Close" class="close-content"><a href="javascript:void(0);" id="prev" style="color: red;">Close</a></div>
                                        <table style="width: 100%;" align="center" visible="false">
                                            <tr>
                                                <td style="padding: 5px; background: #deedf7; width: 100px" colspan="2"><span class="headFont" style="font-weight: bold;"><font size="3px"></font></span></td>
                                            </tr>
                                            <tr>
                                             <td style="padding: 5px; width: 100px"><span class="headFont" style="font-weight: bold;"><font size="3px">Pre-Payment Date :</font></span></td>
                                              <td style="padding: 5px; width: 100px"><span class="headFont" style="font-weight: bold;"><font size="3px">Pre-Payment Principle  Amount :</font></span></td>


                                            </tr>
                                            <tr>
                                                <td style="padding: 5px; width: 100px">
                                                    <asp:TextBox ID="txtRepaymentDate" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox UpperCase" Width="200px" onkeypress="return isNumber(event)" Enabled="true"></asp:TextBox></td>
                                                 <td style="padding: 5px; width: 100px">
                                                    <asp:TextBox ID="txtRepaymentPrincipleAmount" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true" onkeypress="return isNumber(event)"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                 <td style="padding: 5px; width: 100px"><span class="headFont" style="font-weight: bold;"><font size="3px">Pre-Payment Interest Amount :</font></span></td>
                                                 <td style="padding: 5px; width: 100px"><span class="headFont" style="font-weight: bold;"><font size="3px">Pre-Payment Interest adjustment Amount :</font></span></td>


                                            </tr>
                                            <tr>
                                               
                                                <td style="padding: 5px; width: 100px">
                                                    <asp:TextBox ID="txtRepaimentInterestAmount" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true" onkeypress="return isNumber(event)"></asp:TextBox></td>
                                                <td style="padding: 5px; width: 100px">
                                                    <asp:TextBox ID="txtRepaymentInerestAdjustment" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true" onkeypress="return isNumber(event)"></asp:TextBox></td>

                                            </tr>
                                           
                                            <tr>
                                                <td colspan="2">
                                                    <div style="text-align: center">
                                                    <asp:Button ID="btnSaveRepaymentDetails" runat="server" Text="Save" CommandName="Add"
                                                        Width="100px" Height="25px" CssClass="Btnclassname" />
                                                    <asp:Button ID="btnRefresh" runat="server" Text="Cancel"
                                                        Width="100px" Height="25px" CssClass="Btnclassname" />
                                                </div>
                                                </td>
                                                
                                            </tr>
                                             
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- /*******************************************************************************************************************************/--%>
                         <%-- /****************************************************************Wrap PpoUp Retirement Adjustment***************************************************************/--%>
                        <div class="content-overlay" id='overlay1'>
                            <div class="wrapper-outer">
                                <div class="wrapper-inner">
                                    <%--style="height:400px"--%>
                                    <div class="loadContent" >
                                        <div id="closeRet" class="close-content"><a href="javascript:void(0);" id="prev1" style="color: red;">Close</a></div>
                                        <table style="width: 100%;" align="center" visible="false">
                                            <tr>
                                                <td style="padding: 5px; background: #deedf7; width: 100px" colspan="2"><span class="headFont" style="font-weight: bold;"><font size="3px"></font></span></td>
                                            </tr>
                                            <tr>
                                              <td style="padding: 5px; width: 100px" colspan="2"><span class="headFont" style="font-weight: bold;"><font size="3px">Retirement Adjustment Date :</font></span></td>

                                            </tr>
                                            <tr>
                                                <td style="padding: 5px; width: 100px" colspan="2">
                                                    <asp:TextBox ID="txtRetAdjDate" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="200px" onkeypress="return isNumber(event)" Enabled="true"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                              <td style="padding: 5px; width: 100px"><span class="headFont" style="font-weight: bold;"><font size="3px">Retirement Adjustment Principle  Amount :</font></span></td>
                                              <td style="padding: 5px; width: 100px"><span class="headFont" style="font-weight: bold;"><font size="3px">Retirement Adjustment Interest Amount :</font></span></td>

                                            </tr>
                                            <tr>
                                                <td style="padding: 5px; width: 100px">
                                                    <asp:TextBox ID="txtRetAdjPrinAmt" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true" onkeypress="return isNumber(event)"></asp:TextBox></td>
                                                <td style="padding: 5px; width: 100px">
                                                    <asp:TextBox ID="txtRetAdjIntAmt" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox UpperCase" Width="200px" Enabled="true" onkeypress="return isNumber(event)"></asp:TextBox></td>
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="2">
                                                    <div style="text-align: center">
                                                    <asp:Button ID="btnSaveRetirementAdjustment" runat="server" Text="Save" CommandName="Add"
                                                        Width="100px" Height="25px" CssClass="Btnclassname" />
                                                    <asp:Button ID="btnCancelRetirementAdjustment" runat="server" Text="Cancel"
                                                        Width="100px" Height="25px" CssClass="Btnclassname" />
                                                </div>
                                                </td>
                                            </tr>
                                             
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- /*******************************************************************************************************************************/--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

