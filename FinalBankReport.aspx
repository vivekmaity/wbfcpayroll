﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="FinalBankReport.aspx.cs" Inherits="FinalBankReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_Menu" Runat="Server">

    <script type="text/javascript">
        function PrintOpentab() {
            $.ajax({
                type: "POST",
                url: "FinalBankReport.aspx/ISEncodedValueGenerated",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = JSON.parse(D.d); //alert(t);
                    if (t == "Generate") {
                        var popUp = window.open("FinalBankReportView.aspx");
                        return (popup) ? false : true;
                    }
                    else {
                        alert("Sorry ! You can not Generate Final Bank Report Because.\nAll Sector Final Report has not Generated Yet.");
                        return true;
                    }
                }

            });


        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Cont1" Runat="Server">

    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>Final Bank Report</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td style="text-align: center;">
                                                <asp:Button ID="btnNarration" runat="server" Text="Final Bank Report" ClientIDMode="Static"
                                                    Width="115px" CssClass="Btnclassname DefaultButton" OnClientClick='PrintOpentab();' />
                                            </td>
                                        </tr>
                                       
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>

</asp:Content>

