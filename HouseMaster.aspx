﻿<%@ Page Title="" Language="C#" MasterPageFile="~/KMDAMaster.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="HouseMaster.aspx.cs" Inherits="HouseMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function beforeSave() {
            $("#form1").validate();
            $("#txtHouseCode").rules("add", { required: true, messages: { required: "Please enter House Code"} });
            $("#txtHouseName").rules("add", { required: true, messages: { required: "Please enter House Name"} });
        }
        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#form1").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }
        function unvalidate() {
            $("#form1").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cont1" Runat="Server">
<asp:HiddenField ID="hdnHouseID" runat="server" ClientIDMode="Static" Value="" />
    <div id="main">
        <div class="container">
            <div class="row main-row">
                <div class="12u">

                    <section>
                        <h2>House Master</h2>
                    </section>

                    <div id="tabEmpdetail" runat="server">
                        <table width="100%" style="border: solid 2px lightblue;">
                            <tr>
                                <td style="padding: 3px;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td>
                                                <asp:FormView ID="dv" runat="server" Width="99%" AutoGenerateRows="False" OnModeChanging="dv_ModeChanging"
                                                    DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                                                    <InsertItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">House Category &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlHouseCat" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true"
                                                                                ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Category)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="WBFC Quarter" Value="G"></asp:ListItem>
                                                                                <asp:ListItem Text="Assessed Rent" Value="A"></asp:ListItem>
                                                                                <asp:ListItem Text="Licence Type" Value="L"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">House Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtHouseName" ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="20" autocomplete="off"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                                <td style="padding: 5px;"><span class="headFont">House Rent &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtRent" ClientIDMode="Static" Width="190px" runat="server" CssClass="textbox" autocomplete="off" ></asp:TextBox>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>
                                                    </InsertItemTemplate>

                                                    <EditItemTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="headFont">House Category&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:DropDownList ID="ddlHouseCat" Width="200px" Height="24px" runat="server" AppendDataBoundItems="true"
                                                                                ClientIDMode="Static" autocomplete="off">
                                                                                <asp:ListItem Text="(Select Category)" Value=""></asp:ListItem>
                                                                                <asp:ListItem Text="WBFC Quarter" Value="G"></asp:ListItem>
                                                                                <asp:ListItem Text="Assessed Rent" Value="A"></asp:ListItem>
                                                                                <asp:ListItem Text="Licence Type" Value="L"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                </td>
                                                                <td style="padding: 5px;"><span class="headFont">House Name&nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtHouseName" Text='<%# Eval("HouseName") %>' ClientIDMode="Static" runat="server" CssClass="textbox" MaxLength="20" autocomplete="off"></asp:TextBox>

                                                                </td>

                                                            </tr>
                                                             <tr>
                                                                <td style="padding: 5px;"><span class="headFont">House Rent &nbsp;&nbsp;</span><span class="require">*</span> </td>
                                                                <td style="padding: 5px;">:</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <asp:TextBox ID="txtRent" ClientIDMode="Static" Text='<%# Eval("RentAmt") %>' Width="190px" runat="server" CssClass="textbox" autocomplete="off" ></asp:TextBox>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>
                                                    </EditItemTemplate>
                                                    <FooterTemplate>
                                                        <table align="center" width="100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <hr style="border: solid 1px lightblue" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 5px;"><span class="require">*</span> indicates Mandatory Field</td>
                                                                <td style="padding: 5px;">&nbsp;</td>
                                                                <td style="padding: 5px;" align="left">
                                                                    <div style="float: left; margin-left: 200px;">
                                                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add"
                                                                            Width="100px" CssClass="Btnclassname" OnClientClick='javascript: return beforeSave();'
                                                                            OnClick="cmdSave_Click" />
                                                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
                                                                            Width="100px" CssClass="Btnclassname" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate();' />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:FormView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="border: solid 1px lightblue" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" AutoGenerateColumns="false"
                                                    DataKeyNames="HouseCode" OnRowCommand="tbl_RowCommand"  AllowPaging="true" PageSize="5"
                                                    CssClass="Grid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="tbl_PageIndexChanging">
                                                     <AlternatingRowStyle BackColor="#FFFACD" />
                                                        <PagerSettings FirstPageText="First" LastPageText="Last"
                                                        Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Previous" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle  />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Select' ImageUrl="img/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                                                    runat="server" ID="btnEdit" CommandArgument='<%# Eval("HouseID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle  />
                                                            <ItemTemplate>
                                                                <asp:ImageButton CommandName='Del' ImageUrl="img/Delete.gif" runat="server" ID="btnDelete"
                                                                    OnClientClick='return Delete();'
                                                                    CommandArgument='<%# Eval("HouseID") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="HouseID" Visible="false"  HeaderText="House ID" />
                                                        <asp:BoundField DataField="HouseCategory"  HeaderText="House Category" />
                                                        <asp:BoundField DataField="HouseName"  HeaderText="House Name" />
                                                        <asp:BoundField DataField="RentAmt"  HeaderText="House Rent" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>

        </div>
</div>  
</asp:Content>

