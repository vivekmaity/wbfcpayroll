﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;                  
using System.Web.Script.Services;          
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;              
using System.IO;
using System.Reflection;    
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
    
public partial class SalaryGeneration : System.Web.UI.Page
{
    public string str1 = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_SECTORID] != "" && HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString() != "")
            {
                PopulateCenter(Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTORID]), HttpContext.Current.Session[SiteConstants.SSN_SALFIN].ToString());
            }
        }
        //string message = "Message from server side";
        //ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + message + "');", true);
    }  

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }



    [WebMethod]
    public static string GetSalMonth(int SecID, string SalFinYear)
    {
        DataSet ds1 = DBHandler.GetResults("Get_SalPayMonth", SecID, SalFinYear);
        DataTable dtSalMonth = ds1.Tables[0];
        DataTable dtSalMonthID = ds1.Tables[0];

        List<MaxSalMonth> listSalMonth = new List<MaxSalMonth>();

        if (dtSalMonth.Rows.Count > 0)
        {
            for (int i = 0; i < dtSalMonth.Rows.Count; i++)
            {
                MaxSalMonth objst = new MaxSalMonth();

                
                objst.PayMonths = Convert.ToString(dtSalMonth.Rows[0]["MaxSalMonth"]);
                objst.PayMonthsID = Convert.ToInt32(dtSalMonth.Rows[0]["MaxSalMonthID"]);
                listSalMonth.Insert(i, objst);
            }
        }


        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(listSalMonth);

    }
    public class MaxSalMonth
    {
       public string PayMonths { get; set; }
       public int PayMonthsID { get; set; }
    }
   
    

    [WebMethod]
    public static string Report_Paravalue(int SalMonth,int SectorID)
    {
        DataSet ds1 = DBHandler.GetResults("SalaryProcess", SalMonth, SectorID, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        String Msg = "Salary Generation has been Completed Successfully";
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(Msg);

    }

    protected void PopulateCenter(int SecID, string SalFinYear)
    {
        try
        {

            string SecId = "";
            DataSet ds = DBHandler.GetResults("Get_CenterbySector", SecID.Equals("") ? HttpContext.Current.Session[SiteConstants.SSN_SECTORID] : (object)SecID);
            DataTable dtSector = DBHandler.GetResult("Get_Sector", Session[SiteConstants.SSN_INT_USER_ID]);
            SecId = dtSector.Rows[0]["SectorId"].ToString();
            if (dtSector.Rows.Count == 1)
            {
                DataTable dtSalMonth = DBHandler.GetResult("Get_SalPayMonth", Convert.ToInt32(SecId), SalFinYear);
                if (dtSalMonth.Rows.Count > 0)
                {
                    txtPayMonth.Text = dtSalMonth.Rows[0]["MaxSalMonth"].ToString();
                    hdnSalMonthID.Value = dtSalMonth.Rows[0]["MaxSalMonthID"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

}